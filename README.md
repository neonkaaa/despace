# despace

Just another spacecraft components & analytics database which you’re able to take anywhere even without an internet connection.

The proposed starting point is **[the Index page](db/index.md)**.

Or you may look at the [FAQ](db/faq.md).

<br>

You may use your favorite markdown‑editor/viewer to use this database on your PC or mobile device. Currently the proposed editors  with a [custom themes ❐](js/themes.zip) are:

1. **ghostwriter** (<https://wereturtle.github.io/ghostwriter>)
2. **VNote** (<https://github.com/vnotex/vnote>)
3. **Zettlr** (https://www.zettlr.com/)

<br>

The DB is licensed under [CC0](LICENSE) (see [FAQ](db/faq.md) for clarification).
