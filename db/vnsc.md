# VNSC
> 2022.03.31 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/v/vnsc_logo1t.webp)](f/c/v/vnsc_logo1.webp)|<info@vnsc.org.vn>, 0084 4 37917675, Fax …;<br> *18 Hoàng Quốc Việt, Nghĩa Đô, Cầu Giấy, Hà Nội, Vietnam*<br> <https://vnsc.org.vn/en>|
|:--|:--|
|**Business**|Vietnam Space Agency|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Vietnam National Space Center (VNSC)** is a Vietnam Space Agency aimed to manage & implement space & satellite projects for the government of Vietnam. A part of Vietnam Academy of Science & Technology. Founded 2011.09.16.

<p style="page-break-after:always"> </p>

## The End

end of file
