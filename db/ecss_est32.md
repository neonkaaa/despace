# ecss_est32

[TOC]

---


## Annex I (normative) Mathematical model description & delivery (MMDD) — DRD

**I.1 DRD identification.** *Requirement ID & source* — ECSS‑E‑ST‑32, requirement 1.1.1.1.1. *Purpose & objective* — a mathematical model is associated with its **mathematical model description & delivery document (MMDD)**, which contributes to a correct use of the model & to the understanding of its results. The MMDD is fundamental for traceability of the mathematical models & indicates & lists all the changes of the delivered model. The MMDD provides a description of the structural mathematical model (named below “mathematical model” or simply “model”) & of the performed quality checks.

**I.2 Expected response**

*Special remarks* — None.

*Scope & content:*

1. **Introduction.** The purpose, objective, applicability, content & the reason prompting its preparation.
1. **Applicable & reference documents, Definitions & abbreviations.** The applicable & reference documents in support to the generation of the document. The terms & definitions, abbreviated terms, & symbols used.
1. **Structure (or structure component) description**
   1. *Unit system.* Indicate the consistent unit system of measures used. The units for mass, force, length, time, temperature & angles shall be explicitly documented.
   1. *General description, drawings.* Describe the structure & introduce to the terminology for major structure components. Reference shall be made to the available drawings. Reference the set of the applied drawings in order to explain the actual status of the design & to define the structure components to be analyzed.
1. **Coordinate system.** Describe all the coordinate systems used in the model, by giving, for each coordinate system, the following information; include a brief description to explain the use of the coordinate system (e.g. by listing the model items using the coordinate system): ➀ the label number, ➁ the type (rectangular, cylindrical, spherical), ➂ origin position & axes orientation, ➃ data card used to define it.
1. **Mathematical model outline**
   1. *Assumption, idealizations & limitations.* Summarize & include a justification of the modelling assumptions & methodology; include the following: ➀ justification of used element types, spring & rigid connections, rigid body  & relationships, ➁ model adequacy to study specific structure behaviour (e.g. non‑linear phenomena, local‑global buckling, & contact), ➂ model limitations (e.g. limitation on type of analyses can be performed or on output can be found), ➃ significant model output: stress, frequencies
   1. *Numbering.* If any special rule is applied for model numbering, it shall be reported, with reference to: ➀ nodes, ➁ elements, ➂ element properties, ➃ materials, ➄ constraints, ➅ forces, ➆ analysis cases
   1. *Mathematical model summary.* Include a mathematical model summary as a table that summarizes model data, showing the total number of each type of data.
   1. *Analysis code compatibility.* Indicate the analysis code, (e.g. NASTRAN, SAMCEF, ABAQUS.), which the model is designed for. If the model can be used with more than a specific code, these shall be indicated.
   1. *Recommended analysis parameters.* If special parameters are required to correctly use the model with a specific code, then these parameters shall be specified.
   1. *Pre‑ & post processors compatibility.* Indicate the pre & post‑processor used for modelling, by underlying software limitations & recommendations to properly handle the model.
   1. *Compliance with model requirements.* Summarize the requirements, if any, that the model is compliant with, e.g. maximum number of nodes, numbering ranges, & recommendations in using specific elements.
1. **Finite element modelling**
   1. *General & information for each major structural item.*
      1. Give the detailed description of the mathematical model.
      1. Each of the major structural items into which the product can be split shall be described independently.
      1. The description of each of the major structural items into which the product can be split shall include the information in <7.2> until <7.7>.
      1. The MMDD shall give a brief description of the each item, with reference to the previous clause "General description of the structure" & to available project documents.
      1. A figure or drawing shall be included, showing the physical structural item.
   1. *Modelling assumption.* Underline if any assumptions of relevant significance have been introduced. If no significant assumptions have been used in the modelling of the item, this shall be explicitly written, by introducing the following sentence: “No special assumptions are to be underlined”.
   1. *Idealization.* The mathematical model of each structural item shall be described in detail, indicating the: ➀ type of the used elements, ➁ number of nodes & elements, ➂ rigid connections, ➃ interfaces with other items. Include a figure reproducing the each idealized item, in order to direct compare the idealized & the physical structural item.
   1. *Model: nodes & elements.* Indicate label ranges of nodes k elements of each item model. Declare logic applied for node & element labelling, if any. Produce plots of each element & model, showing node & element labels.
   1. *Model: properties & material.* Indicate properties & materials related to the item k. Reproduce code input card (e.g. property & material input), including relevant comments.
   1. *Critical parameters.* Highlight & comment any data parameter specific of each item k if deemed critical for model performance & reliability (e.g. damping coefficient in the frequency response analysis, large mass in transient analysis).
   1. *Interfaces with other items.* Describe the interfaces between each item k & other items, in terms of common nodes & connecting elements.
1. **Masses**
   1. *Density of structural masses.* Indicate structural masses included in the model & related mass density (Some plots can be used to show the parts of the structure with the same mass density). If no structural mass is included in the model, declare this explicitly.
   1. *Lumped masses.* Describe lumped masses & reproduce related input data. Indicate the position of the masses also by plotting the model & labelling the masses. If lumped masses are not included in the model, declare this explicitly.
   1. *Distributed non‑structural masses.* If distributed non‑structural masses are present in the model, a description shall be provided. Report the value of the distributed mass. Use model plots to indicate where the distributed non‑structural masses are smeared. If distributed non structural masses are not included in the model, declare this explicitly.
   1. *Global inertia properties.* Report Centre of Gravity (COG) position, total mass & other inertia properties computed by the analysis code.
   1. *Source documents of mass distribution.* Provide reference to the documents used to establish the mass distribution of the mathematical model.
1. **Loads.** Describe the model load sets. Each load set shall be independently described, including the following: ➀ List types of load included in each load set k (e.g. forces, line distributed loads, pressure, gravity) & related values shall be indicated, ➁ Give total resultant forces & moment (w.r.t. a specified point), in code output format.
1. **Multi‑point constraints & single‑point constraints**
   1. *General.* Describe the set of point constraints & relationships. Describe each set independently, including the information from below.
   1. *Set information*
      1. Multi‑point constraint (relationships between DOFs): Describe each multi‑point constraint set in terms of connected nodes & degrees of freedom. Compare the model to the physical structure & explain modelling assumptions. Use model plots & comparison with structure drawings, as a mean of presentation.
      1. Single‑point constraint: Describe each single‑point constraint set in terms of constrained nodes & degrees of freedom. Compare the model to the physical structure & explain modelling assumptions. Use model plots & comparison with structure drawings, as a mean of presentation.
1. **Analysis cases.** Describe the analysis cases (An analysis case is defined by associating a constraint set to an analysis set).
1. **Miscellaneous model topics.** Collect any other topic of interest.
1. **Model checks**
   1. *Model geometry checks.* Report the results of dedicated checks performed to assess the geometry correctness.
   1. *Elements topology checks.* Report the results of dedicated checks performed to assess the elements topology correctness.
   1. *Rigid‑body‑motion strain energy check.* Report the results of dedicated checks performed in order to ensure that neither strain energy nor nodal residual forces arise due to rigid body motions of the model. Report value of strain energy & residual forces due to rigid body motions at different set of DOFs:
      1. At the set including all model DOFs.
      1. At the set obtained by removing all dependent DOFs in the multi‑point constraints.
      1. At the set obtained by removing also DOFs constrained by single‑point constraints.
   1. *Static analysis check.* Report the results of dedicated checks performed to assess model adequacy to perform static analysis.
   1. *Thermal‑elastic analysis check.* Report the results of dedicated checks performed to assess adequacy to perform thermal stress analysis.
   1. *Normal mode analysis check.* Report the results of dedicated checks performed to assess the adequacy of the model to perform normal mode related dynamic analyses.
1. **Mathematical model changes.** If a model changes, describe the changes. The clauses affected by model changes shall be indicated & updated. If additional model checks have been performed on the delivered model, then document, number & address the check results.
1. **Conclusion**
   1. *Mathematical model use in structural analysis.* On the basis of the model checks that have been performed, include a clarification if the model satisfies the purposes for which it has been created, by indicating the analysis types the model is capable of performing.
   1. *Mathematical model limitations.* A concluding remark on the limits of the model shall underlined the: ➀ analyses that cannot be performed, ➁ behaviour of the structure that cannot be studied, ➂ responses that cannot be given.
   1. *Suggested future implementation.* Provide suggestions to improve the responses of the model, including: ➀ how to modify the model to study other effects, ➁ how to increase the accuracy of the analysis modifying particular areas.
