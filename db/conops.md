# CONOPS
> 2019.12.19 [🚀](../../index/index.md) [despace](index.md) → **[](.md)** <mark>NOCAT</mark>  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Концепция эксплуатации** — русскоязычный термин. **Concept of operations (CONOPS)** — англоязычный эквивалент.</small>

**Концепция эксплуатации** *(Concept of operations, CONOPS)* — документ, предназначенный для пользователя и содержащий информацию о рабочих характеристиках системы с точки зрения пользователя (IEEE 1362). В отличии от концепции функционирования (operational concept) она как правило описывает один сценарий или несколько взаимосвязанных сценариев. Концепция эксплуатации разрабатывается перед началом разработки системы на стадии планирования. Она служит основой для формирования функциональных требований к системе.

A **concept of operations** (abbreviated CONOPS, CONOPs, or ConOps) is a document describing the characteristics of a proposed system from the viewpoint of an individual who will use that system such as a business requirements specification or stakeholder requirements specification (StRS). It is used to communicate the quantitative & qualitative system characteristics to all stakeholders. CONOPS are widely used in the military, governmental services & other fields.

The main standard is ISO/IEC/IEEE 15288:2015 Systems & software engineering — System life cycle processes.

A CONOPS generally evolves from a concept & is a description of how a set of capabilities may be employed to achieve desired objectives or end state. In the field of joint military operations, a CONOPS in DoD terminology is a verbal or graphic statement that clearly & concisely expresses what the joint force commander intends to accomplish & how it will be done using available resources. CONOPS may also be used or summarized in system acquisition DODAF descriptions such as the OV-1 High Level Operational Concept Graphic.

The main question is how do we maneuver and operate a spacecraft during the mission?



## Описание
В состав CONOPS обычно включают описание:

1. Целей и задач создания системы вместе с критериями успешности
1. Взаимосвязей с другими системами и объектами
1. Источников и адресатов информации
1. Прочих связей и ограничений

Concept of Operations documents can be developed in many different ways, but usually share the same properties. In general, a CONOPS will include the following:

1. Statement of the goals & objectives of the system
1. Strategies, tactics, policies, & constraints affecting the system
1. Organizations, activities, & interactions among participants & stakeholders
1. Clear statement of responsibilities & authorities delegated
1. Specific operational processes for fielding the system
1. Processes for initiating, developing, maintaining, & retiring the system

A CONOPS should relate a narrative of the process to be followed in implementing a system. It should define the roles of the stakeholders involved throughout the process. Ideally, it offers a clear methodology to realize the goals & objectives for the system, while not intending to be an implementation or transition plan itself.

A CONOPS Standard is available to guide the development of a CONOPS document. The Institute of Electrical & Electronics Engineers (IEEE) Standard is structured around information systems, but the standard may be applied to other complex systems as well.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[](.md)】**<br> <mark>NOCAT</mark>|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Concept_of_operations>
1. <http://sewiki.ru/CONOPS>


## The End

end of file
