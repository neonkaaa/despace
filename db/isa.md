# ISA
> 2019.08.05 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/i/isa_logo1t.webp)](f/c/i/isa_logo1.webp)|<mark>noemail</mark>, +972(3)764-96-00, Fax +972(3)764-96-22;<br> *Derech Menachem Begin 52, Tel Aviv, Israel*<br> <http://space.gov.il> ~~ [FB ⎆](https://he-il.facebook.com/IsraelSpaceAgency) ~~ [X ⎆](https://twitter.com/ILSpaceAgency?lang=he) ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/Israel_Space_Agency)|
|:--|:--|
|**Business**|Coordinates all Israeli space research programs|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

The **Israel Space Agency** (**ISA**; Hebrew: סוכנות החלל הישראלית, Sokhnut heKhalal haYisraelit) is a governmental body, a part of Israel’s Ministry of Science & Technology, that coordinates all Israeli space research programs with scientific & commercial goals. The agency was founded by the theoretical physicist Yuval Ne'eman in 1983 to replace the National Committee for Space Research which was established in 1960 to set up the infrastructure required for space missions.



## The End

end of file
