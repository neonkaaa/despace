# Events
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [Events](event.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Events** — EN term. **События** — RU analogue.</small>



## Contests
1. [Google Lunar X Prize](google_lunar_x_prize.md)



## Meetings
**The points for participation in meetings:** an advertising for a company or for you in person, expanding the business network, demonstration of possibilities, definition of perspectives.

1. Major: [AGU](agu.md), [CGMS](cgms.md), [COSPAR](cospar.md), [DPS](dps.md), [EGU](egu.md), [EPSC](epsc.md), [FHS](fhs.md), [GLEX](glex.ru), [IAC](iac.md), [IPDW](ipdw.md), [IVC](ivc.md), [JpGU](jpgu.md), [LPSC](lpsc.md), [MAKS](maks.md), [MSSS](msss.md), [NIAC](niac_program.md), [PMCS](pmcs.ru), [VEXAG](vexag.md), [WSI](wsi.md).
1. 1 time in 2 years: [COSPAR](cospar.md), [IVC](ivc.md), [MAKS](maks.md), [WSI](wsi.md); jointed 1 time in 3 years: [DPS](dps.md), [EPSC](epsc.md).
1. Preparations & absttract submission in common begins 6 months before the meeting; in case of [WSI](wsi.md) — 1 ‑ 1.5 y.
1. **Major event calendars:**
   1. <https://www.hou.usra.edu/meetings/calendar>
   1. <http://planetarynews.org/meetings.html>
   1. <http://spaceagenda.com>

**Some notable events.**

|**Date**|**Abbr.**|**Meeting**|
|:--|:--|:--|
|2020.05.26‑27|[PMCS](pmcs.md)|[Planetary Mission Concept Studies 2020](pmcs2020.md) (Virtual, US, Colorado)|
|2019.05.19‑24|[CGMS](cgms.md)|[Coordination Group for Meteorological Satellites, 47th](cgms_47.md) (RU, Sochi)|
|2019.05.31‑04|[IVC](ivc.md)|[International Venus Conference. Fujihara Seminar 74](ivc_2019.md) (JP, Hokkaido, Niseko)|
|2019.10.02‑05|…|[Venera-D: Landing Sites & Cloud Layer Habitability Workshop](vdws2019.md) (RU, Moscow)|
|2019.10.07‑11|[MSSS](msss.md)|[Moscow Solar System Symposium, 10th](msss_10.md) (RU, Moscow)|
|2019.11.06‑08|[VEXAG](vexag.md)|[Venus Exploration Analysis Group, 17th](vexag_2019.md) (US, Colorado, Bowlder)|
|2013.11.19‑21|[VEXAG](vexag.md)|[Venus Exploration Analysis Group, 11th](vexag_11.md) (US, Maryland, Laurel)|



## Docs/Links
|**Sections & pages**|
|:--|
|**【[Events](event.md)】**<br> **Meetings:** [AGU](agu.md) ~~ [CGMS](cgms.md) ~~ [COSPAR](cospar.md) ~~ [DPS](dps.md) ~~ [EGU](egu.md) ~~ [EPSC](epsc.md) ~~ [FHS](fhs.md) ~~ [IPDW](ipdw.md) ~~ [IVC](ivc.md) ~~ [JpGU](jpgu.md) ~~ [LPSC](lpsc.md) ~~ [MAKS](maks.md) ~~ [MSSS](msss.md) ~~ [NIAC](niac_program.md) ~~ [VEXAG](vexag.md) ~~ [WSI](wsi.md) ┊ ··•·· **Contests:** [Google Lunar X Prize](google_lunar_x_prize.md)|

1. Docs:
   1. [События, template](templates.md)
   1. [Командировка зарубеж, расчёт стоимости ❐](f/event/meeting_calc.ods)
1. <…>


## The End

end of file
