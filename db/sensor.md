# Sensor
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [IU](iu.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Датчик** — русскоязычный термин. **Sensor** — англоязычный эквивалент.</small>

**Датчик** — средство измерений, предназначенное для выработки сигнала измерительной информации в форме, удобной для передачи, дальнейшего преобразования, обработки и (или) хранения, но не поддающейся непосредственному восприятию наблюдателем. Датчики, выполненные на основе электронной техники, называются электронными датчиками. Отдельно взятый датчик может быть предназначен для измерения (контроля) и преобразования одной физической величины или одновременно нескольких физических величин.

В состав датчика входят чувствительные и преобразовательные элементы. Основными характеристиками электронных датчиков являются чувствительность и погрешность.

Датчики широко используются в научных исследованиях, испытаниях, контроле качества, телеметрии, системах автоматизированного управления и в других областях деятельности и системах, где требуется получение измерительной информации.


|**Датчики**|**Аналоговый**|**Цифровой**|
|:--|:--|:--|
|Давления|[2МД-Т (ТО и ТУ) ❐](f/sensor/2md-t_docs.7z)| |
|Температуры|[ТМ 293 (ТО и ТУ) ❐](f/sensor/tm_293_docs.7z)| |

**Общая информация.**

|**Тип датчика**|**Масса, г**|<small>*Электро‑<br> потребление, А*</small>|<small>*Напря&shy;жение,<br> В*</small>|<small>*Диапазон<br> измерений*</small>|<small>*Погреш&shy;ность<br> измерений*</small>|
|:--|:--|:--|:--|:--|:--|
|**Давления,<br> аналоговый**|180 ‑ 250 (L<sub>кабеля</sub> 400 ㎜)|0.01 ‑ 0.046|6.5|0 ‑ 140 ㎏f/㎝²|2.5 %|
|**Давления,<br> цифровой**| | | | | |
|**Температурный,<br> аналоговый**|9 (L<sub>кабеля</sub> 400 ㎜);<br> 12 (L<sub>кабеля</sub> 1 000 ㎜);<br> 22 (L<sub>кабеля</sub> 2 000 ㎜)|0.0035 за 10 ㎳|12|–199 ‑ +200 ℃|1 %|
|**Температурный,<br> цифровой**|9.3 (L<sub>кабеля</sub> 300 ㎜);<br> 13.2 (L<sub>кабеля</sub> 800 ㎜);<br> 24 (L<sub>кабеля</sub> 1 500 ㎜)|0.0015 за  750 ㎳|12|–120 ‑ +150 ℃|0.1 %|

【**Table.** Manufacturers】

| | |
|:--|:--|
|**AE**|…|
|**AU**|…|
|**CA**|・[GHGSat](ghgsat.md) — gas sensors for Cubesats|
|**CN**|…|
|**EU**|…|
|**IL**|…|
|**IN**|…|
|**JP**|・[Meisei](meisei.md) — spectrometers|
|**KR**|…|
|**RU**|…|
|**SA**|…|
|**SG**|…|
|**US**|…|
|**VN**|…|



## Earth sensor
> <small>**Датчик Земли** — русскоязычный термин. **Earth sensor (ES)** — англоязычный эквивалент.</small>

| | |
|:--|:--|
|**AE**|…|
|**AU**|…|
|**CA**|…|
|**CN**|…|
|**EU**|…|
|**IL**|…|
|**IN**|…|
|**JP**|・[Mitsubishi](mitsubishi.md) — temperature sensors<br> ・[NEC](nec.md)<br> ・[Sumitomo PP](sumitomo_pp.md)|
|**KR**|…|
|**RU**|…|
|**SA**|…|
|**SG**|…|
|**US**|…|
|**VN**|…|



## Magnetometer
> <small>**Магнитометр** — русскоязычный термин. **Magnetometer** — англоязычный эквивалент.</small>

**Магнито́метр** — *(от гр. μαγνητό — магнит + гр. μετρεω измеряю)*, прибор для измерения характеристик магнитного поля и магнитных свойств материалов. В зависимости от измеряемой величины различают приборы для измерения напряжённости поля (эрстедметры), направления поля (инклинаторы и деклинаторы), градиента поля (градиентометры), магнитной индукции (тесламетры), магнитного потока (веберметры или флюксметры), коэрцитивной силы (коэрцитиметры), магнитной проницаемости (мю‑метры), магнитной восприимчивости (каппа‑метры), магнитного момента.

Магнитометры применяются в геологии, археологии, магнитной геохронологии, навигации на море, в космосе и авиации, разведке и сейсмологии, биологии и медицине.

Магнитометры используются для навигации КА по магнитному полю Земли, а также для определения высоты. Впервые магнитометр был использован на КА 「Спутник‑3」 (1958 г).

Современные магнитометры для навигации сделаны в виде трёх перпендикулярных друг другу трубок, с помощью которых может быть построена 3‑осная ориентация и определено направление магнитного поля. В случае применения одной или двух трубок вместо трёх измерения могут проводиться путём поворота КА или помещения магнитометра на привод.

|В космосе применяются<br> индукционные магнитометры.|Магнитометр КА [THEMIS](themis.md).|
|:--|:--|
|[![](f/sensor/lemi-120t.webp)](f/sensor/lemi-120.webp)|[![](f/sensor/search_coil_magnetometert.webp)](f/sensor/search_coil_magnetometer.webp)|

**Разновидности**

| |**Произв.**|**Актуальные (масса, г)**|**Исторические (масса, г)**|
|:--|:--|:--|:--|
|**RU**|[Спутникс](sputnix.md)|[SX-MAGWR](mtm_lst.md) (100)| |

【**Table.** Manufacturers】

| | |
|:--|:--|
|**AE**|…|
|**AU**|…|
|**CA**|…|
|**CN**|…|
|**EU**|…|
|**IL**|…|
|**IN**|…|
|**JP**|・[Meisei](meisei.md) — spectrometers|
|**KR**|…|
|**RU**|・[Спутникс](sputnix.md)|
|**SA**|…|
|**SG**|…|
|**US**|…|
|**VN**|…|



## Star tracker
> <small>**Звёздный датчик (ЗД)** — русскоязычный термин. **Star tracker (ST)** — англоязычный эквивалент.</small>

**Звёздный датчик (ЗД)**, также **блок определения координат звёзд (БОКЗ)** — прибор в составе [космического аппарата](sc.md), предназначенный для определения ориентации КА относительно звёздного неба. Является чувствительным элементом системы ориентации КА. В конце 1980‑х начали применяться широкопольные датчики на основе ПЗС‑матриц, которые сравнивают полученное изображение звёздного неба с имеющимся в памяти звёздным каталогом. Датчик может быть как автономным прибором, содержащим блок обработки данных, так и использовать для этой цели вычислительные мощности [бортовой ЭВМ](obc.md).

| |**[Фирма](contact.md)**|**Модели ЗД (масса, ㎏)**|
|:--|:--|:--|
|**EU**|[Leonardo](leonardo.md)|[AA-STR](st_lst.md) (2.6) ~~ [A-STR](st_lst.md) (3.55)|
| |[Jena‑Optronik](jenaoptronik.md)|[ASTRO APS](st_lst.md) (2) ~~ [ASTRO 10](st_lst.md) (3.8) ~~ [ASTRO 15](st_lst.md) (6.15)|
| |[Sodern](sodern.md)|[Auriga](st_lst.md) (0.56) ~~ [Horus](st_lst.md) (1.6) ~~ [Hydra](st_lst.md) (4.6)<br> *Исторические:* *[SED26](st_lst.md) (3.3)**|
| |[Terma A/S](terma.md)|[T1](st_lst.md) (0.6 ‑ 1) ~~ [T2](st_lst.md) (0.8) ~~ [HE-5AS](st_lst.md) (2.2)|
|•|• • • • • • • • • • • •|~ ~ ~ ~ ~ |
|**RU**|[Geofizika‑Cosmos](geofizika_s.md)|[360К](st_lst.md) () ~~ [348К](st_lst.md) (3.45)|
| |[IKI RAS](iki_ras.md)|[МикроБОКЗ](st_lst.md) (0.4) ~~ [мБОКЗ-2](st_lst.md) (1.5) ~~ [БОКЗ-МФ](st_lst.md) (2.8)|
| |[Mars MOKB](mars_mokb.md)|[АД-1](st_lst.md) (3.8)|
| |[Sputnix](sputnix.md)|[SX-SR-MicroBOKZ](st_lst.md) (0.5)|
|•|• • • • • • • • • • • •|~ ~ ~ ~ ~ |
|**US**|[Ball A&T](ball_at.md)|[CT-2020](st_lst.md) (3) ~~ [HAST](st_lst.md) (7.7)|
| |[Space Micro](space_micro.md)|[MIST](st_lst.md) (0.55) ~~ [µSTAR](st_lst.md) (2.1)|

【**Table.** Manufacturers】

| | |
|:--|:--|
|**AE**|…|
|**AU**|…|
|**CA**|・[Macfab](macfab.md)|
|**CN**|…|
|**EU**|…|
|**IL**|…|
|**IN**|…|
|**JP**|・[Hamamatsu Photonics](hamamatsu_phot.md)|
|**KR**|・[Satrec Initiative](satreci.md)|
|**RU**|…|
|**SA**|…|
|**SG**|…|
|**US**|…|
|**VN**|…|

**Кубсаты:**

1. <https://www.bluecanyontech.com/components>
1. <https://www.adcolemai.com/attitude-systems>
1. <https://www.berlin-space-tech.com>



## Spectrometer
A **spectrometer** is a tool used to separate & measure spectral components (chemical composition) of a physical phenomenon. See also [spectrometry](spectroscopy.md)

1. <https://en.wikipedia.org/wiki/Spectrometer>
1. <https://en.wikipedia.org/wiki/Spectroscopy>



## Sun sensor
> <small>**Солнечный датчик (СД)**,  **Солнечный датчик положения (СДП)** — русскоязычные термины. **Sun sensor (SS)** — англоязычный эквивалент.</small>

**Солнечный датчик (СД)**, также **солнечный датчик положения (СДП)** предназначен для:

1. первоначального построения **постоянной солнечной ориентации (ПСО)** после отделения от [РБ](lv.md);
1. построения и поддержания ПСО на [КА](sc.md), где условия работы [научной аппаратуры](sc.md) предполагают использование ПСО в качестве рабочей ориентации;
1. построения и поддержания ПСО в нештатных ситуациях, в случаях, когда парирование нештатной ситуации в режиме инерциальной ориентации невозможно.

There are various types of sun sensors, which differ in their technology and performance characteristics. Sun presence sensors provide a binary output, indicating when the Sun is within the sensor’s field of view. Analog and digital sun sensors, in contrast, indicate the angle of the Sun by continuous and discrete signal outputs, respectively.

In typical sun sensors, a thin slit at the top of a rectangular chamber allows a line of light to fall on an array of photodetector cells at the bottom of the chamber. A voltage is induced in these cells, which is registered electronically. By orienting 2 sensors perpendicular to each other, the direction of the Sun can be fully determined. Often, multiple sensors will share processing electronics.

| |**[Фирма](contact.md)**|**Модели СД (масса, ㎏)**|
|:--|:--|:--|
|**EU**|[Bradford](bradford_eng.md)|[FSS](ss_lst.md) (0.05 ‑ 0.375) ~~ [CSS](ss_lst.md) (0.275) ~~ [CoSS](ss_lst.md) (0.015 ‑ 0.024)|
| |[Jena‑Optronik](jenaoptronik.md)|[FSS](ss_lst.md) (0.65)|
| |[Solar MEMS](solarmems.md)|[NanoSSOC-A60](nssoc_a60.md) () ~~ [NanoSSOC-D60](nssoc_d60.md) () ~~ [SSOC-A60](ssoc_a60.md) () ~~ [SSOC-D60](ssoc_d60.md) ()|
|•|• • • • • • • • • • • • •|~ ~ ~ ~ ~ |
|**RU**|[ВНИИЭМ](vniiem.md)| |
| |[Геофизика‑Космос](geofizika_s.md)|[347К](ss_lst.md) (0.7)|
| |[ИКИ РАН](iki_ras.md)|[ОСД](ss_lst.md) (0.65)|
| |[МОКБ Марс](mars_mokb.md)|[ТДС](ss_lst.md) (2.3) ~~ [СДП-1](ss_lst.md) (0.4)|
| |[Спутникс](sputnix.md)|[SX-SUNR-01](ss_lst.md) (0.04)| |

【**Table.** Manufacturers】

| | |
|:--|:--|
|**AE**|…|
|**AU**|…|
|**CA**|・[Macfab](macfab.md)<br> ・[Sinclair Interplanetary](sinclair_ip.md)|
|**CN**|…|
|**EU**|…|
|**IL**|…|
|**IN**|…|
|**JP**|・[Hamamatsu Photonics](hamamatsu_phot.md)|
|**KR**|・[Satrec Initiative](satreci.md)|
|**RU**|…|
|**SA**|…|
|**SG**|…|
|**US**|…|
|**VN**|…|

Etc:

1. Solar MEMS <http://www.solar-mems.com/space-equipment>
1. <https://www.adcole.com/aerospace/analog-sun-sensors>
1. <https://www.adcolemai.com/sun-sensors>
1. <https://spacetech-i.com/products/satellite-equipment/cess>
1. <https://lens-rnd.com/sun-sensors>
1. <https://www.cgsatellite.com>
1. <http://lasp.colorado.edu/home/wp-content/uploads/2018/07/700.Fine-Sun-Sensor.pdf>
1. <http://www.vniiem.ru/ru/index.php?:option=com_content&view=article&id=705:2015-08-04-07-29-07&catid=98:2015-08-04-07-15-09&Itemid=62>

**Кубсаты:**

| |**[Фирма](contact.md)**|**Модели СД для кубсатов (масса, ㎏)**|
|:--|:--|:--|
|**US**|[Space Micro](space_micro.md)|[MSS](ss_lst.md) (0.036) ~~ [CSS](ss_lst.md) (0.02)|

1. <http://www.newspaceglobal.com>
1. <https://sunsensor.eu>



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**`Датчик:`**<br> …|
|**`Звёздный датчик (ЗД):`**<br> [Видимая звёздная величина](app_mag.md) ~~ [ПЗр](fov.md)<br>~ ~ ~ ~ ~<br> **Европа:** [ASTRO 15](st_lst.md) (6.15) ~~ [Hydra](st_lst.md) (4.6) ~~ [ASTRO 10](st_lst.md) (3.8) ~~ [A-STR](st_lst.md) (3.55) ~~ [AA-STR](st_lst.md) (2.6) ~~ [HE-5AS](st_lst.md) (2.2) ~~ [ASTRO APS](st_lst.md) (2) ~~ [Horus](st_lst.md) (1.6) ~~ [T2](st_lst.md) (0.8) ~~ [T1](st_lst.md) (0.6 ‑ 1) ~~ [Auriga](st_lst.md) (0.21)  ▮  **РФ:** [348К](st_lst.md) (3.45) ~~ [360К](st_lst.md) () ~~ [АД-1](st_lst.md) (3.8) ~~ [БОКЗ-МФ](st_lst.md) (2.8) ~~ [мБОКЗ-2](мбокз_2.md) (1.5) ~~ [SX-SR-MicroBOKZ](st_lst.md) (0.5)  ▮  **США:** [HAST](st_lst.md) (7.7) ~~ [CT-2020](st_lst.md) (3) ~~ [µSTAR](st_lst.md) (2.1) ~~ [MIST](st_lst.md) (0.55) |
|**【[Structures, gears, materials (SGM)](sc.md)】**<br> [Гермоконтейнер](гермоконтейнер.md) ~~ [Датчик](sensor.md) ~~ [Задел](margin.md) ~~ [Изделие](unit.md) ~~ [Испарение материалов](matc.md) ~~ [Кавитация](cavitation.md) ~~ [КЗУ](cinu.md) (ВБУ КТ) ~~ [КХГ](cgs.md) ~~ [Контейнеры для транспортировки](ship_contain.md) ~~ [Крейцкопф](crosshead.md) ~~ [Номинал](nominal.md) ~~ [ПУС](lag.md) ~~ [ПНА, ПОНА, ПСНА](devd.md) ~~ [Резерв](reserve.md) ~~ [Слайс](слайс.md) ~~ [ТСП](tsp.md) ~~ [Типичные формы КА](sc.md) ~~ [Толкатель](толкатель.md) ~~ [Унификация](commonality.md)|
|**`Магнитометр:`**<br> … <br>~ ~ ~ ~ ~<br> **РФ:** [SX-MAGWR](mtm_lst.md) (100)|
|**`Солнечный датчик (СД):`**<br> [ПЗр](fov.md) <br>~ ~ ~ ~ ~<br> (КА) **Европа:** [FSS](ss_lst.md) (650) ~~ [FSS](ss_lst.md) (50 ‑ 375) ~~ [CSS](ss_lst.md) (275) ~~ [CoSS](ss_lst.md) (15 ‑ 24)  ▮  **РФ:** [ТДС](ss_lst.md) (2 300) ~~ [347К](ss_lst.md) (700) ~~ [ОСД](ss_lst.md) (650) ~~ [СДП-1](ss_lst.md) (400) ~~ [SX-SUNR-01](ss_lst.md) (40)<br> *(Кубсаты) **США:** [MSS](ss_lst.md) (0.036) ~~ [CSS](ss_lst.md) (0.02)**|

1. Docs:
   1. [ЗД, template](templates.md) / [СД, template](templates.md)
1. Sensor:
   1. <https://en.wikipedia.org/wiki/Sensor>
1. ЗД:
   1. <https://en.wikipedia.org/wiki/Star_tracker>
   1. <https://www.ball.com/aerospace/markets-capabilities/capabilities/technologies-components/star-trackers>
   1. <https://www.terma.com/space/space-segment/star-trackers>
1. СД:
   1. <https://en.wikipedia.org/wiki/Sun_sensor>
   1. <https://www.jvejournals.com/article/17518>
1. <https://en.wikipedia.org/wiki/Magnetometer>
1. <https://en.wikipedia.org/wiki/Spacecraft_magnetometer>


## The End

end of file
