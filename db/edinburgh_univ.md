# Edinburgh Univ.
> 2019.08.09 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/e/edinburgh_univ_logo1t.webp)](f/c/e/edinburgh_univ_logo1.webp)|<mark>noemail</mark>, +44(131)650-1000, Fax …;<br> *WRV6+R8 Edinburgh, UK*<br> <https://www.ed.ac.uk> ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/University_of_Edinburgh)|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Эдинбургский университет (The University of Edinburgh (abbreviated as Edin.)** — государственный университет в столице Шотландии городе Эдинбурге. Шестой по старшинству в Великобритании, открылся в 1583 году. Ему принадлежат многие здания в старой части города. Структурно состоит из трёх колледжей, включающих 20 школ. Член группы 「Расселл」 и Лиги европейских исследовательских университетов.



## The End

end of file
