# NGSL
> 2020.11.19 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/n/ngsl_logo1t.webp)](f/c/n/ngsl_logo1.webp)|<info@ngsl.or.jp>, <mark>nophone</mark>, Fax …;<br> *1-5-3 Nihonbashi Muromachi, Chuo-ku, Tokyo, Japan*<br> <https://ngsl.or.jp>|
|:--|:--|
|**Business**|Solving global issues utilizing Japanese space technology|
|**Mission**|Solving global issues utilizing Japanese space technology. Contributing to 「World Safety & Security」 through collaboration between JP, the US, & EU in the field of global commons (space, ocean, electromagnetic cyber).|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**New Space Global Strategy Lab (NGSL)** from New Space International Institute for Strategic Studies is a JP non‑profit company aimed to promote solving global issues utilizing Japanese space technology.

- *Purpose.* For the development & utilization of new universes & the expansion of their players forming a community from an international & strategic perspective, establish this corporation for the purpose of working on it.
   - Purpose 1. Utilizing Japanese space technology, we will contribute to 「solving global issues」.
   - Purpose 2. In the field of global commons such as space, ocean, & cyberspace, we will develop the results of joint activities in space by Japan, the United States, Europe, etc., share data & knowledge, & contribute to 「safety & security of the world」.
   - Purpose 3. We aim to 「create a new space business」 by fusing conventional space development with new space & different industries.



## The End

end of file
