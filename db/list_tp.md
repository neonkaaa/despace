# Перечень методик испытаний
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [Док.](doc.md), **[Test](test.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Перечень методик испытаний (ПМИ)** — русскоязычный термин, не имеющий аналога в английском языке. **List of test procedures** — дословный перевод с русского на английский.</small>

**Перечень методик проведения наземных и лётных испытаний и методики оценки их результатов** или в обиходе **Перечень методик испытаний (ПМИ)** — документ с перечнем методик проведения наземных и лётных испытаний и методики оценки их результатов.

Разрабатывается на этапе [ЭП](rnd_ep.md) на основании требований [Положения РК‑11‑КТ](const_rk.md) п.3.1.5.

Разрабатывается в соответствии с требованиями [ГОСТ 15.210](гост_15_210.md).



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Test](test.md)】**<br> [JTAG](jtag.md) ~~ [Proto fligt model](pfm.md) ~~ [Безэховая камера](ach.md) ~~ [Валидация](vnv.md) ~~ [Класс чистоты](clean_lvl.md) ~~ [КПЭО](ctpr.md) ~~ [Перечень методик испытаний](list_tp.md) ~~ [Программа и методика испытаний](pmot.md) ~~ [Опытный образец](pilot_sample.md) ~~ [Циклограмма](obc.md) ~~ [Штатный образец](flight_unit.md) ~~ [ЭО](test.md) ~~ [Экспериментально‑теоретический метод](etetm.md)|

1. Docs: …
1. <…>


## The End

end of file
