# Xplore
> 2020.01.12 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/x/xplore_logo1t.webp)](f/c/x/xplore_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *7683 SE 27th St. #308, Mercer Island, WA 98040, US*<br> <https://www.xplore.com> ~~ [LI ⎆](https://www.linkedin.com/company/xplore-inc)|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Xplore** 「Space as a Service」 — an agile space company dedicated to space exploration. Production: [spacecraft](sc.md).

We believe the opportunity space presents is without limit. Embracing this opportunity will reveal mysteries never before known and give us a deeper understanding of the universe and our place within it.  
We are an agile space company dedicated to science. We are infinitely curious about our solar system, and we believe in pushing the boundaries of human knowledge.  
Our mission is unfettered exploration. We are reaching for the stars and will push technology to its limits — leveraging every possible angle to achieve efficiencies in capability and cost.  
It’s time to explore our solar system. Our plan is ambitious — yet grounded in the reality that space is hard. We are building the architecture that will enable new discoveries.

Spacecraft [Xcraft](xcraft.md).

- **Executive Team:**
   - Allison 「Allie」 Hannigan — Relationship Manager
   - Jeff Rich — Chief Executive Officer
   - Dr. Darren Garber — Chief Technology Officer
   - Lisa Rich — Chief Operating Officer
   - Adam Schilffarth — Strategist
   - Gina Amanatullah — Chief Engineer
   - Adam Rotman — Communications
- **Advisory Team:**
   - Dr. Louis D. Friedman
   - Dr. Darren Garber
   - Raymond 「Bud」 Fraze
   - Mike Burger
   - Eileen Dukes



## The End

end of file
