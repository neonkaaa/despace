# OHB
> 2022.03.25 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/o/ohb_logo1t.webp)](f/c/o/ohb_logo1.webp)|<info@ohb.de>, +49 421 2020 8, Fax +49 421 2020 700;<br> *Manfred-Fuchs-Platz 2-4, 28359 Bremen, Germany*<br> <https://www.ohb.de> ~~ [IG ⎆](https://www.instagram.com/ohb_se) ~~ [LI ⎆](https://www.linkedin.com/company/255297) ~~ [X ⎆](https://twitter.com/OHB_SE)|
|:--|:--|
|**Business**|Sats, exploration, human spaceflight|
|**Mission**|We bring together passionate people to deliver complex space systems. As a Large System Integrator, we develop smart & innovative solutions to maximize value for our customers. Profitable & sustainable growth is the basis for our future. Entrepreneurial spirit & agility provide perspectives for all of us.|
|**Vision**|We simplify your life through smart space solutions|
|**Values**|・**Trust.** We recognize & respect each other. We trust in our individual characters & competences.<br> ・**Versatility.** We embrace our diversity & multidisciplinary capabilities to shape the future.<br> ・**Collaboration.** We work together, we support each other & we communicate proactively & transparently to achieve our objectives.<br> ・**Responsibility.** We act responsibly & reliably with each other, with our clients, our partners & society.<br> ・**Courage.** We accept & master challenges & take initiative.<br> ・**Solution Orientation.** We apply our creativity, flexibility, passion & performance-orientation towards the development of outstanding solutions.|
|**[MGMT](mgmt.md)**|・CEO — Marco R. Fuchs|

**OHB SE** is a European multinational technology corporation. Headquartered in Bremen, Germany, the corporation consists of the two business divisions Space Systems & Aerospace + Industrial Products. A key product of the corporation is fully integrated spacecraft. At present OHB is the third largest corporation in Europe's space sector. Founded in 1958.

Space business unit’s activities encompass satellites, manned spaceflight, exploration & security/reconnaissance technologies. Another focus is developing & implementing payloads, scientific equipment & devices for aeronautics/aerospace, research institutes & industry. Thus, OHB System AG develops, builds, launches & operates low‑orbiting & geostationary small satellites for scientific applications, communications & terrestrial observation.

The OHB SE group encompasses subsidiaries across the European Union.

|**Subsidiary**|**Location**|**Legal Form**|**Share**|
|:--|:--|:--|:--|
|**【Space System】**| | | |
|OHB System|Bremen & Oberpfaffenhofen, Germany|AG|100%|
|OHB Italia|Milan, Italy|S.p.A.|100%|
|LuxSpace|Betzdorf, Luxembourg|Sàrl|100%|
|Antwerp Space|Antwerp, Belgium|N.V.|100%|
|[OHB Sweden ⎆](https://www.ohb-sweden.se)|Stockholm, Sweden|AB|100%|
|OHB Czechspace|Klatovy & Brno, Czech Republic|s.r.o.|100%|
|OHB Hellas|Athens, Greece|mon.E.P.E|100%|
|**【Aerospace + Industrial Products】**| | | |
|MT Aerospace|Augsburg, Germany|AG|70%|
|MT Mechatronics|Mainz, Germany|GmbH|70%|
|MT Aerospace Guyane|Kourou, French Guiana|S.A.S.|70%|
|OHB Teledata|Bremen, Germany|GmbH|100%|
|OHB Digital Services|Bremen, Germany|GmbH|74.9% |

<p style="page-break-after:always"> </p>

## The End

end of file
