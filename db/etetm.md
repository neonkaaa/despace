# Экспериментально‑теоретический метод
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [Project](project.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Экспериментально‑теоретический метод (ЭТМ)** — русскоязычный термин, не имеющий аналога в английском языке. **Experimental-theoretical (experimental-theoretical) method (ETETM)** — дословный перевод с русского на английский.</small>

**Экспериментально‑теоретический (опытно‑теоретический) метод** — метод отработки, проверки и подтверждения характеристик [комплекса](sc.md) и [его изделий](unit.md), основанный на совместном применении математического и имитационного моделирования, наземных испытаний, специально организованных [автономных натурных испытаний](rnd_e.md) отдельных изделий.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Test](test.md)】**<br> [JTAG](jtag.md) ~~ [Proto fligt model](pfm.md) ~~ [Безэховая камера](ach.md) ~~ [Валидация](vnv.md) ~~ [Класс чистоты](clean_lvl.md) ~~ [КПЭО](ctpr.md) ~~ [Перечень методик испытаний](list_tp.md) ~~ [Программа и методика испытаний](pmot.md) ~~ [Опытный образец](pilot_sample.md) ~~ [Циклограмма](obc.md) ~~ [Штатный образец](flight_unit.md) ~~ [ЭО](test.md) ~~ [Экспериментально‑теоретический метод](etetm.md)|

1. Docs:
   1. [РК‑11](const_rk.md), стр.22.
1. <…>


## The End

end of file
