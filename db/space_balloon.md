# Space Balloon
> 2021.12.13 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/s/space_balloon_logo1t.webp)](f/c/s/space_balloon_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *…, Mito, Ibaraki, Japan*<br> <https://www.spaceballoon.co.jp> ~~ [IG ⎆](https://www.instagram.com/spaceballoon_official)|
|:--|:--|
|**Business**|Earth observation using high altitude balloons|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Space Balloon Co., Ltd.** is a Japanese company aimed to provide a photo service from a high altitude using low cost balloons. Founded 2020.

<p style="page-break-after:always"> </p>

## The End

end of file
