# Boeing
> 2019.12.24 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](file/cooperation//_logo1t.webp)](file/cooperation//_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *…*<br> …|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

…



2003 ‑ 2019. Ненадёжны. Жадны. Выносят на аутсорс всё подряд, увольняют квалифицированных работников. (<https://lazy-flyer.livejournal.com/811084.html>, архив [от 2019.12.24 ❐](f/c/b/boeing_doc001.pdf))


## The End

end of file
