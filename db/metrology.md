# Метрология
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [QM](qm.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Метрология** — русскоязычный термин. **Metrology** — англоязычный эквивалент.</small>

**Метроло́гия** *(от греч. μέτρον — мера, + др.-греч. λόγος — мысль, причина)* — наука об измерениях, методах и средствах обеспечения их единства и способах достижения требуемой точности. Предметом метрологии является извлечение количественной информации о свойствах объектов с заданной точностью и достоверностью; нормативная база для этого — метрологические стандарты.

Метрология состоит из 3 основных разделов:

1. **Теоретическая.** Рассматривает общие теоретические проблемы (разработка теории и проблем измерений физических величин, их единиц, методов измерений).
1. **Прикладная.** Изучает вопросы практического применения разработок теоретической метрологии. В её ведении находятся все вопросы метрологического обеспечения.
1. **Законодательная.** Устанавливает обязательные технические и юридические требования по применению единиц физической величины, методов и средств измерений.

**Цели и задачи метрологии:**

1. создание общей теории измерений;
1. образование единиц физических величин и систем единиц;
1. разработка и стандартизация методов и средств измерений, определения точности измерений, основ обеспечения единства измерений и единообразия средств измерений (т.н. 「законодательная метрология」);
1. создание эталонов и образцовых средств измерений, поверка мер и средств измерений. Приоритетной подзадачей данного направления является выработка системы эталонов на основе физических констант;
1. также метрология изучает развитие системы мер, денежных единиц и счёта в исторической перспективе.

**Аксиомы метрологии:**

1. Любое измерение есть сравнение.
1. Любое измерение без априорной информации невозможно.
1. Результат любого измерения без округления значения является случайной величиной.



## Термины и определения метрологии

1. **Единство измерений** — состояние измерений, характеризующееся тем, что их результаты выражаются в узаконенных единицах, размеры которых в установленных пределах равны размерам единиц, воспроизводимым первичными эталонами, а погрешности результатов измерений известны и с заданной вероятностью не выходят за установленные пределы.
1. **Физическая величина** — одно из свойств физического объекта, общее в качественном отношении для многих физических объектов, но в количественном отношении индивидуальное для каждого из них.
1. **Измерение** — совокупность операций по применению технического средства, хранящего единицу физической величины, обеспечивающих нахождение соотношения измеряемой величины с её единицей и получения значения этой величины.
1. **Средство измерений** — техническое средство, предназначенное для измерений и имеющее нормированные метрологические характеристики воспроизводящие и (или) хранящие единицу величины, размер которой принимается неизменным в пределах установленной погрешности в течение известного интервала времени.
1. **Поверка** — совокупность операций, выполняемых в целях подтверждения соответствия средств измерений метрологическим требованиям.
1. **Погрешность измерения** — отклонение результата измерения от истинного значения измеряемой величины.
1. **Погрешность средства измерения** — разность между показанием средства измерений и действительным значением измеряемой физической величины.
1. **Точность средства измерений** — характеристика качества средства измерений, отражающая близость его погрешности к нулю.
1. **Лицензия** — это разрешение, выдаваемое органам государственной метрологической службы на закрепленной за ним территории физическому или юридическому лицу на осуществление ему деятельности по производству и ремонту средств измерения.
1. **Эталон единицы величины** — техническое средство, предназначенное для передачи, хранения и воспроизведения единицы величины.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**`Качество:`**<br> [Bus factor](bus_factor.md) ~~ [Way](faq.md) ~~ [АВПКО](fmeca.md) ~~ [Авторский надзор](des_spv.md) ~~ [Бережливое производство](lean_man.md) ~~ [Валидация, верификация](vnv.md) ~~ [Класс чистоты](clean_lvl.md) ~~ [Конструктивное совершенство](con_vel.md) ~~ [Крит. технологии](kt.md) ~~ [Крит. элементы](sens_elem.md) ~~ [Метрология](metrology.md) ~~ [Надёжность](qm.md) ~~ [Нештатная ситуация](emergency.md) ~~ [Номинал](nominal.md) ~~ [Ошибки](faq.md) ~~ [Система менеджмента качества](qms.md) ~~ [УГТ](trl.md)/[TRL](trl.md)|

1. Docs: …
1. [ГСИ](sseum.md)
1. [Ошибки](faq.md) — частые ошибки.
1. <https://en.wikipedia.org/wiki/Metrology>


## The End

end of file
