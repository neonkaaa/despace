# NAOJ
> 2020.07.18 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/n/naoj_logo1t.webp)](f/c/n/naoj_logo1.webp)|<mark>noemail</mark>, +81 422-34-3600, Fax …;<br> *2-21-1 Osawa, Mitaka, Tokyo 181-8588, Japan*<br> <https://www.nao.ac.jp> ~~ [LI ⎆](https://www.linkedin.com/company/national-astronomical-observatory-of-japan) ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/National_Astronomical_Observatory_of_Japan)|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**National Astronomical Observatory of Japan (NAOJ)** is the national center for astronomy research in Japan. It provides observational facilities to researchers throughout the country & promotes joint research programs. It also utilizes the development of astronomy & other related fields as opportunities for international cooperation. Founded in 1988.

In 2004, NAOJ, in alliance with four other national institutes — the National Institute for Basic Biology, the National Institute for Fusion Science, the National Institute for Physiological Sciences, & the Institute for Molecular Science — established the National Institutes of Natural Sciences (NINS) to promote collaboration among researchers of the five constituent institutes.

Facilities:

1. **Mitaka Campus (Mitaka, Tokyo. 35°40′31″N 139°32′17″E)**
   - HQ, Astronomy Data Center, Advanced Technology Center, Public Relations Center
   - Solar Flare Telescope, Sunspot Telescope, TAMA 300 gravitational wave detector
   - Tokyo Photoelectric Meridian Circle
   - Historical instruments: Solar Tower Telescope, 65 ㎝ refractor dome, 20 ㎝ refractor dome
1. **Nobeyama Radio Observatory & Nobeyama Solar Radio Observatory (Minamimaki, Nagano, 35°56′28″N 138°28′13″E)**
   - 45 m Millimeter Radio Telescope, Nobeyama Millimeter Array, Nobeyama Radio Heliograph
1. **Mizusawa VERA Observatory (Ōshū, Iwate. 39°08′06″N 141°08′00″E)**
   - 20 m radio telescope, 10 m VLBI radio telescope
   - Historical building: Dr. Kimura Museum
1. **Okayama Astrophysical Observatory (Mt. Chikurinji in Asakuchi, Okayama. 34°34′34″N 133°35′39″E)**
   - 188 ㎝ telescope, 91 ㎝ telescope, 65 ㎝ Coude-Type solar telescope
1. **VERA 20 m radio telescopes**
   - Mizusawa
   - Ogasawara. 27°05′30″N 142°13′00″E
   - Iriki. 31°44′52″N 130°26′24″E
   - Ishigakijima. 24°24′43.83″N 124°10′15.58″E
1. **Hawaii Observatory (Hawaii)**
   - Subaru 8 m telescope (Mt. Mauna Kea. 19°49′33″N 155°28′35″W)
   - Hilo Base Facility (Hilo, Hawaii. 19°42′10″N 155°05′25″W)



## The End

end of file
