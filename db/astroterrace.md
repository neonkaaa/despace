# AstroTerrace
> 2022.12.29 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/a/astroterrace_logo1t.webp)](f/c/a/astroterrace_logo1.svg)|<h_tsuji@astroterrace.co.jp>, +81-80-5181-6905, Fax …;<br> *Uchisaiwaicho 2-2-2 Fukoku Seimei Building 16F, Chiyoda-ku, Tokyo, 〒100-0011, Japan*<br> <https://astroterrace.co.jp>|
|:--|:--|
|**Business**|Space development, marketing, research, consulting|
|**Mission**|Space technologies for better society. We will contribute to the realization of a prosperous society by connecting the world’s technologies & people & contributing to space development.|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO — Hisanori Tsuji|

**AstroTerrace** is a Japanese space company aimed to provide solutions for space development programs & business as 「Soft Prime」, incl. commercial engineering support & project management services for space research & development organizations & satellite companies, with a specialty in advanced optical systems for complex satellite networks. Founded 2016.02.06.

Business:

- Development & dev. support for equipment mounted on space equipment (satellites & rockets)
- Space Project Management
- Space business development, marketing, research, consulting
- Providing solutions in the space development business

**「Soft Prime」 Business Model for Space Equipment Development.** Participated in the project from the early stages of concept in the development of satellite‑mounted systems & equipment. From planning to completion of systems & equipment & on‑track demonstrations, we realize consistent project management & project integration from the customer’s perspective & standpoint. It is a service model that provides the best solution. Features of 「Soft Prime」 Business Model

1. Customer Perspective. From the initial stage of the concept, we repeated dialogue not only with customers but also with users & stakeholders, & conducted feasibility studies & concept studies from the customer’s perspective. After starting the development of systems & devices, our own team of engineers examines requirements for systems & equipment, examines specifications, & performs conceptual design. It enables the creation of output that fully incorporates the customer’s perspective & intentions.
1. Consistency. We provide consistent project management & technical support according to the project life cycle. It is possible to build a project framework & system that realizes optimal performance in each phase, & always provide the best solution.
1. Own engineer team. We flexibly build the optimal system according to the phase of the project & make the most of the knowledge, technology, network, etc. gathered at our company. Astro Terrace’s own team of experienced engineers enables us to provide the optimal technology according to the project life cycle.
1. Independence. The selection of manufacturing sites for systems & equipment eliminates certain restrictions & influences. As a result, we have built an optimal project system with domestic & overseas space equipment manufacturers from the customer’s perspective. It enables the manufacture of reliable systems & equipment.
Workflow using the 「soft prime」 bus.



## The End

end of file
