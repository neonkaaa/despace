# Контрольная сборка (КСБ)
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [R&D](rnd.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Контрольная сборка (КСБ)** — русскоязычный термин. **Controlled assembly** — англоязычный эквивалент.</small>

**Контрольная сборка (КСБ)** — сборка (монтаж, регулировка) [изделия комплекса](unit.md) (составной части, сборочной единицы) в организации‑изготовителе, назначаемая и предъявляемая на приёмочный контроль непосредственно в процессе проведения указанных работ с целью проверки требований технологической документации, а также [конструкторской документации](doc.md) на технологичность для подтверждения работоспособного состояния изделия комплекса (составной части, сборочной единицы).



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[](.md)】**<br> <mark>NOCAT</mark>|

1. Docs:
   1. [РК‑11](const_rk.md), стр.17.
1. <…>


## The End

end of file
