# ecss_qst10


---

## Annex A  (normative) Product Assurance Plan (PAP) — DRD

**A.1 DRD identification.** *Requirement ID & source* — ECSS‑Q‑ST‑10, requirement 5.1.3.1b. *Purpose & objective.* The objective of the **Product Assurance Plan (PAP)** is to describe the activities to be performed by the supplier to assure the quality of the space product with regard to the specified mission objectives & to demonstrate compliance to the applicable PA requirements.

**A.2 Expected response**

*Special remarks.* The response to this DRD **may be combined with the Project Management Plan**, as per ECSS‑M‑ST‑10. The response to this DRD may be performed by reference to separate discipline plans addressing some of the above clauses of this DRD.

*Scope & content:*

1. **Introduction.** The purpose, objective & the reason prompting its preparation. (For example: programme or project reference & phase)
2. **Applicable & reference documents.** The applicable & reference documents in support of the generation of the document.
3. **Product assurance management**
   1. *PA Planning.* Describe the organization (including responsibilities & authorities), the activities, processes & procedures to be applied by the supplier to fulfil the applicable product assurance planning requirements defined in clause 5.1 of ECSS‑Q‑ST‑10.
   2. *PA implementation.* Describe the activities, processes & procedures to be applied by the supplier to fulfil the applicable product assurance implementation requirements defined in clause 5.2 of ECSS‑Q‑ST‑10.
4. **Quality assurance.** Describe the activities, processes & procedures to be applied by the supplier to fulfil the applicable quality assurance requirements.
5. **Dependability.** Describe the activities, processes & procedures to be applied by the supplier to fulfil the applicable dependability requirements.
6. **Safety.** Describe the activities, processes & procedures to be applied by the supplier to fulfil the applicable safety requirements.
7. **EEE components.** Describe the activities, processes & procedures to be applied by the supplier to fulfil the applicable EEE Component requirements.
8. **Materials & processes.** Describe the activities, processes & procedures to be applied by the supplier to fulfil the applicable Material & Processes requirements.
9. **Software product assurance.** Describe the activities, processes & procedures to be applied by the supplier to fulfil the applicable Software product assurance requirements.
10. **Other PA requirements.** Describe the activities, processes & procedures to be applied by the supplier to fulfil all other applicable PA requirements not covered in the clause A.2.1.1.<3> to clause A.2.1.1.<9>. (The order of the sections is not mandatory. For example: Security, Planetary protection, Off‑The‑Shelf, Customer furnished equipment, Integrated logistic support, Production preparation, Launch system exploitation, Involvement of relevant Surveillance Authority Representatives, National Surveillance Organizations for Launch Segment)
