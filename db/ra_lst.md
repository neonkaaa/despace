# Robotic arm (a list)
> 2023.10.28 [🚀](../../index/index.md) [despace](index.md) → [Robotics](robot.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

A list of [Robotic arms](robot.md).

## Current

### PIAP Titan   ［EU］

**Titan** — robotic arm for precise manipulations. Designed by [PIAP](piap_space.md) in 2024.

|**Characteristics**|**(Titan)**|
|:--|:--|
|Composition|2 units: RA & Control Box|
|Consumption, W|20 ‑ 45 (idle, w/o TCS), 45 (worst case); 20 ‑ 150 (nominal); 300 ‑ 360 (max);<br> 10 – for Control Box|
|Dimensions, ㎜|1079 × 873 × 565 stowed, 95 × 94 × 50 Control Box|
|[Interfaces](interface.md)|HOTDOCK (end effector), …|
|[Lifetime](lifetime.md), h(y)| |
|Mass, ㎏|45 (6 DoF), 55 (7 DoF), 0.5 (Control Box)|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃|−20 ‑ +60 (operational), −40 ‑ +80 (survival), 10 W RA heat emission|
|[TRL](trl.md)|6|
|[Voltage](sps.md), V|22 ‑ 28 ‑ 34 DC|
|**【Specific】**|~ ~ ~ ~ ~ |
|DoF|6, 7|
|Dust tolerance| |
|FDIR concept|Health monitor., Collision detection, Abort, Data check, Redundancy mgmt|
|Grippers| |
|Joint rotation, °| |
|Max payload, ㎏|0g: 1 000 (up to 2 000 with worse characteristics), 1g: 6|
|Preciseness|Between mounting point and End point:<br> ±0.1° & ±5 ㎜ on each axis;<br> Open loop absolute accuracy: 1° rotational, 5 ㎜ translational;<br> knowledge of the position is 4 times better (w/o loads)|
|RAMS concept|Redundancy: DoF by 7th joint, FTS, Data bus, TCS (heaters, sensors)|
|Reachability, m|1g (self-supporting): cylindrical workspace of R=0.6 m;<br> 0g: 2 m (precise manipulations for R=1 m)|
|Sensors| |
|Speed|100 ㎜/s & 5 °/s (tip);<br> 0.25 rad/s (large joints), 0.45 rad/s (small joints);<br> accel: 0.25 rad/s² & 0.45 rad/s²;<br> velocity control accuracy is 0.0035 rad/s|
| |[![](f/ra/p/piap_titan_pic1t.webp)](f/ra/p/piap_titan_pic1.webp)|


**Notes:**

1. The mentioned knowing of the position after loads is performed via reading the sensors inside joints.
1. **Applicability:** …



### Redwire Staark

**Staark** — robotic arm for precise manipulations. Designed by [Redwire](redwire.md) in 2021.

|**Characteristics**|**(Staark LEO-S10)**|
|:--|:--|
|Composition|2 units: RA & Control Unit|
|Consumption, W| |
|Dimensions, ㎜|2000 × 250 × 350|
|[Interfaces](interface.md)|LAR Gripper, Vision Sensors, Custom end effectors available (end effector), RS-422, Spacewire|
|[Lifetime](lifetime.md), h(y)| |
|Mass, ㎏|25 ‑ 35|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)|300 (30 000)|
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃|−30 ‑ +70 (operational)|
|[TRL](trl.md)| |
|[Voltage](sps.md), V|28 VDC|
|**【Specific】**|~ ~ ~ ~ ~ |
|DoF|4 ‑ 6|
|Dust tolerance| |
|FDIR concept|Dust tolerant mechanisms|
|Grippers| |
|Joint rotation, °| |
|Max payload, ㎏|0g: 2 200, 1g: |
|Preciseness| |
|RAMS concept| |
|Reachability, m|0.96 ‑ 1.96|
|Sensors| |
|Speed| … ‑ … |
| | |


**Notes:**

1. **Applicability:** …



## Archive

### Template

**…** — robotic arm for …. Designed by … in ….

|**Characteristics**|**(…)**|
|:--|:--|
|Composition| |
|Consumption, W| |
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)| |
|Mass, ㎏| |
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)| |
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|DoF| |
|Dust tolerance| |
|FDIR concept| |
|Grippers| |
|Joint rotation, °| |
|Max payload, ㎏|0g: , 1g: |
|Preciseness| |
|RAMS concept| |
|Reachability, m| |
|Sensors| |
|Speed| … ‑ … |
| | |


**Notes:**

1. **Applicability:** …



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|…|

1. Docs:


## The End

end of file
