# Конкурс на ОКР (СЧ ОКР)
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [R&D](rnd.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Конкурс на ОКР (СЧ ОКР)** — русскоязычный термин. **Public procurement competition** — англоязычный эквивалент.</small>

**Конкурс на ОКР (СЧ ОКР)** — самый сложный способ размещения заказа вообще и в рамках [R&D](rnd.md) в частности. Он достаточно затратный, и победитель в нём определяется на основании ряда критериев, которые оговорены заранее в конкурсной документации. Критериями могут быть квалификация разработчика, качество товаров и услуг, сроки и т.д. Конкурс проводит заказчик, либо его уполномоченный. Извещение о проведении торгов размещается в специализированных изданиях и на сайте заказчика.

Конкурс может быть **открытым** и **закрытым**. В открытом конкурсе может участвовать любое лицо, в закрытом — только лица, специально приглашённые для этой цели.
1. Участники **открытого конкурса** направляют свои заявки, оформленные в соответствии с требованиями. Если подана лишь одна заявка, конкурс считается несостоявшимся. Вскрытие предложений участников производится публично, при этом оглашаются участники и их ценовые предложения. Все заявки оценивает конкурсная комиссия, которая и определяет победителя. С победителем открытого конкурса проводятся переговоры о заключении контракта. В случае, если переговоры с этим участником заходят в тупик, организатор приглашает к переговорам о заключении контракта поставщика, сделавшего следующее по выгодности предложение.
1. **Закрытый конкурс** проводится на таких же правилах, что и открытый. Отличие в том, что в закрытом конкурсе могут участвовать только поставщики, которых приглашает сам заказчик. Информация об итогах закрытого конкурса также не разглашается.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[](.md)】**<br> <mark>NOCAT</mark>|

1. Docs: …
1. <http://www.consultant.ru/law/podborki/zakrytyj_konkurs>
1. <http://www.my-tender.ru/?:_escaped_fragment_=/content/otkrytyy_i_zakrytyy_konkurs#!/content/otkrytyy_i_zakrytyy_konkurs|my-tender.ru>
1. <https://ru.wikipedia.org/wiki/Открытый_конкурс>


## The End

end of file
