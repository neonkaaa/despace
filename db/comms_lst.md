# Comms (a list)
> 2019.07.31 [🚀](../../index/index.md) [despace](index.md) → [Sensor](sensor.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

A list of [Communication systems](comms.md).

## Current



### AddValue IDRS i100   ［SG, RxTx］
**IDRS i100** — IDRS transceiver. Designed by [AddValue](addvalue.md) in ….

|**Characteristics**|**(IDRS i100)**|
|:--|:--|
|Composition|1 unit|
|Consumption, W|Rx: 8;<br> Average TT&C: 10.3 (LGA), 8.8 (MGA), 9.5 (HGA);<br> Tx: 40 (LGA), 25 (MGA), 32 (HGA)|
|Dimensions, ㎜|125 × 96 × 70|
|[Interfaces](interface.md)|RS-422, CAN, Ethernet (FTP, SSH, Telnet, HTTP, etc.)|
|[Lifetime](lifetime.md), h(y)|26 280 (3)|
|Mass, ㎏|1|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃|–25 ‑ +55 (operational), –40 ‑ +80 (storage)|
|[TRL](trl.md)|9|
|[Voltage](sps.md), V|27.5|
|**【Specific】**|~ ~ ~ ~ ~ |
|[Bitrate](bitrate.md), bit/s|200 000|
|Continuous work, h| |
|Distance, ㎞| |
|Frequency instability| |
|[Frequency range](comms.md), ㎐| |
|[Modulation type](comms.md)| |
|Out. power, W| |
|Signal structure| |
| |[![](f/comms/a/addvalue_idrs_i100_pic1t.webp)](f/comms/a/addvalue_idrs_i100_pic1.webp)|

**Notes:**

1. Inmarsat BGAN network
1. Antennas:
   1. LGA — ISOFLUX 200 × ⌀20 ㎜, 1.2 ㎏, hemisphere, fixed, no pointing required, 100 % coverage above 20° elevation
   1. MGA — patch 200 × 100 ㎜, 0.15 ㎏, fixed, pointing required, pointing accuracy ±5°
   1. HGA — 7 segment switched, 150 × ⌀270 ㎜ or attached to top/side panels, 2.8 ㎏, no pointing required, 100 % coverage above 5° elevation
1. <https://www.addvaluetech.com/wp-content/uploads/2020/07/Introduction-to-Addvalue-and-IDRS.pdf>, <https://www.addvaluetech.com/wp-content/uploads/2019/01/Addvalue_IDRS_brochure_AS210104011900_en.pdf>
1. **Applicability:** …



### NPOIT PRIZYV‑1   ［RU, RxTx］
> **ПРИЗЫВ‑1** — русскоязычный термин, не имеющий аналога в английском языке. **PRIZYV‑1** — дословный перевод с русского на английский.

**ПРИЗЫВ‑1** — приёмо‑передающая радиоаппаратура, радиомаяк коротковолнового диапазона. Предназначен для радиопеленгации подвижных и стационарных объектов. Приём сигнала, излучаемого радиомаяком, может вестись радиопоисковыми самолётными (вертолётными) системами АРК‑УД, ЮР‑46 и другими подобными системами.  
Разработчик [НПО ИТ](npoit.md). Разработано  Покупное изделие (на 2018.03.19)

|**Characteristics**|**(ПРИЗЫВ-1)**|
|:--|:--|
|Composition|1 unit|
|Consumption, W|0.765 для 9 В; 2.3 для 27 В|
|Dimensions, ㎜|70 × 34 × 24|
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / 105 120 (12)|
|Mass, ㎏|0.075|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)|0.99 за 2 000 часов|
|[Thermal](tcs.md), ℃|–50 ‑ +65|
|[TRL](trl.md)|9|
|[Voltage](sps.md), V|9 или 27|
|**【Specific】**|~ ~ ~ ~ ~ |
|[Bitrate](bitrate.md), bit/s| |
|Continuous work, h|определяется ресурсом батареи питания|
|Distance, ㎞| |
|Frequency instability|± 0.00005|
|[Frequency range](comms.md), ㎐|100 ‑ 250|
|[Modulation type](comms.md)| |
|Out. power, W|0.3 (при [КСВ](swr.md) ≤ 2)|
|Signal structure|посылка непрерывной несущей в течение 0.5 ± 0.3 s и посылка АИМ сигнала с частотой следования импульсов 1 500 Гц в течение 0.8 ± 0.2 с|
| |[![](f/comms/p/prizyv-1_pic1t.webp)](f/comms/p/prizyv-1_pic1.webp)|

**Notes:**

1. <http://www.npoit.ru/products/telemetricheskie-sistemy/radiomayak-metrovogo-diapazona-prizyv-1>
1. **Applicability:** …



### NPOIT PRIZYV‑3   ［RU, RxTx］
> **ПРИЗЫВ‑3** — русскоязычный термин, не имеющий аналога в английском языке. **PRIZYV‑3** — дословный перевод с русского на английский.

**ПРИЗЫВ‑3** — приёмо‑передающая радиоаппаратура, радиомаяк коротковолнового диапазона. Предназначен для радиопеленгации подвижных и стационарных объектов. Приём излучаемого радиомаяком ПРИЗЫВ‑3 сигнала может вестись радиопоисковыми самолётными (вертолётными) системами ЮР‑26, ЮР‑46 и другими подобными системами.  
Разработчик [НПО ИТ](npoit.md). Разработано  Покупное изделие (на 2018.03.19)

|**Characteristics**|**(ПРИЗЫВ-3)**|
|:--|:--|
|Composition|1 unit|
|Consumption, W|1.8 для 9 В; 5.4 для 27 В|
|Dimensions, ㎜|36 × 84 × 40|
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|0.17|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃|–50 ‑ +65|
|[TRL](trl.md)|9|
|[Voltage](sps.md), V|9 или 27|
|**【Specific】**|~ ~ ~ ~ ~ |
|[Bitrate](bitrate.md), bit/s| |
|Continuous work, h|определяется ресурсом батареи питания|
|Distance, ㎞|180|
|Frequency instability| |
|[Frequency range](comms.md), ㎐|10 000 000 (10 ㎒)|
|[Modulation type](comms.md)| |
|Out. power, W|2 (при [КСВ](swr.md) ≤ 2)|
|Signal structure|посылка АИМ сигнала с частотой следования импульсов 1 500 Гц в течении 0.5 ± 0.2 s с паузами между посылками 1.5 с|
| |[![](f/comms/p/prizyv-3_pic1t.webp)](f/comms/p/prizyv-3_pic1.webp)|

**Notes:**

1. [Чертёж ❐](f/comms/p/prizyv-3_sketch1.webp)
1. <http://www.npoit.ru/products/telemetricheskie-sistemy/radiomayak-korotkovolnovogo-diapazona-prizyv-3>
1. **Applicability:** …



### Sait CSXBT   ［RU, Tx］
> **SAIT Cubesat X‑band Transmitter (CSXBT)** — англоязычный термин, не имеющий аналога в русском языке. **Радиопередатчик для кубсатов X-диапазона НПП Саит (CSXBT)** — дословный перевод с английского на русский.

**SAIT Cubesat X‑band Transmitter (CSXBT)** — радиопередатчик.  
Разработчик [НПП Саит](sait_ltd.md). Разработано ранее 2016 года, активное применение.

|**Characteristics**|**(CSXBT)**|
|:--|:--|
|Composition| |
|Consumption, W|16|
|Dimensions, ㎜|87 × 93 × 28|
|[Interfaces](interface.md)|Customizable [LVDS](lvds.md) or CMOS. 5 pairs for serial data (clock in/out, data, optional enable). RS-485 or I2C or CAN-2B. Micro-D (MIL-DTL-83513) female (9-pin power, 21-pin data/control). SMA female 50 Ω RF output|
|[Lifetime](lifetime.md), h(y)|26 280 (3) / …|
|Mass, ㎏|0.38|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)|60 (6 000) при Al 1.5 g/㎝², ≥40 МэВ·㎝²/㎎|
|[Reliability](qm.md)|0.9392|
|[Thermal](tcs.md), ℃|–20 ‑ +50, 1 датчик температуры DS18S20|
|[TRL](trl.md)|9|
|[Voltage](sps.md), V|12 (11 ‑ 16)|
|**【Specific】**|~ ~ ~ ~ ~ |
|[Bitrate](bitrate.md), bit/s|(внутр. до 1 Гбит/с)|
|Continuous work, h| |
|Distance, ㎞| |
|Frequency instability|±10 ppm|
|[Frequency range](comms.md), ㎐|X: 8 200 000 000 (настройка 8.1 ‑ 8.5 ㎓)|
|[Modulation type](comms.md)|QPSK, 8PSK, 16APSK, 32APSK|
|Out. power, W|2.5|
|Signal structure| |
| |[![](f/comms/c/csxbt_pic1t.webp)](f/comms/c/csxbt_pic1.webp)|

**Notes:**

1. [CSXBT Datasheet ❐](f/comms/c/csxbt_datasheet.pdf)
1. **Applicability:** Аист‑2Д



### Sait HSXBDT   ［RU, Tx］
> **SAIT High‑speed X‑band Downlink Transmitter (HSXBDT)** — англоязычный термин, не имеющий аналога в русском языке. **Высокоскоростной радиопередатчик X-диапазона НПП Саит (HSXBDT)** — дословный перевод с английского на русский.

**SAIT High‑speed X‑band Downlink Transmitter (HSXBDT)** — радиопередатчик.  
Разработчик [НПП Саит](sait_ltd.md). Разработано ранее 2016 года, активное применение.

|**Characteristics**|**(HSXBDT)**|
|:--|:--|
|Composition| |
|Consumption, W|70|
|Dimensions, ㎜|230 × 154 × 45|
|[Interfaces](interface.md)|Customizable [LVDS](lvds.md). Two ports with 4 pairs each (clock in/out, data, optional enable). [RS-422](rs_xxx.md) or [MIL-STD-1553](mil_std_1553.md)|
|[Lifetime](lifetime.md), h(y)|61 320 (7) / …|
|Mass, ㎏|1.8|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)|60 (6 000) при Al 1.5 g/㎝², ≥40 МэВ·㎝²/㎎|
|[Reliability](qm.md)|0.9392|
|[Thermal](tcs.md), ℃|–20 ‑ +50, 2 датчика температуры|
|[TRL](trl.md)|9|
|[Voltage](sps.md), V|27 (23 ‑ 34)|
|**【Specific】**|~ ~ ~ ~ ~ |
|[Bitrate](bitrate.md), bit/s|до 520 Мб/с (CCSDS), до 880 Мб/с (DVB-S2)|
|Continuous work, h| |
|Distance, ㎞| |
|Frequency instability|±4 ppm|
|[Frequency range](comms.md), ㎐|X: 8 225 000 000 (настройка 8.1 ‑ 8.5 ㎓)|
|[Modulation type](comms.md)|QPSK, 8PSK, 16APSK и 32APSK|
|Out. power, W|8|
|Signal structure| |
| |[![](f/comms/h/hsxbdt_pic1t.webp)](f/comms/h/hsxbdt_pic1.webp)|

**Notes:**

1. [HSXBDT Datasheet ❐](f/comms/h/hsxbdt_datasheet.pdf)
1. **Applicability:** Аист‑2Д



## Archive

### Template   ［, ］
**…** — tranceiver. Designed by … in ….

|**Characteristics**|**(…)**|
|:--|:--|
|Composition| |
|Consumption, W| |
|Dimensions, ㎜|… × …|
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)| |
|Mass, ㎏| |
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)| |
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Bitrate](bitrate.md), bit/s||
|Continuous work, h| |
|Distance, ㎞| |
|Frequency instability| |
|[Frequency range](comms.md), ㎐| |
|[Modulation type](comms.md)| |
|Out. power, W| |
|Signal structure| |
| |[![](f/comms//_pic1t.webp)](f/comms//_pic1.webp)|

**Notes:**

1. **Applicability:** …



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Communications](comms.md)】**<br> [CCSDS](ccsds.md) ~~ [Антенна](antenna.md) ~~ [АФУ](afdev.md) ~~ [Битрейт](bitrate.md) ~~ [ВОЛП](ofts.md) ~~ [ДНА](дна.md) ~~ [Диапазоны частот](comms.md) ~~ [Зрение](view.md) ~~ [Интерферометр](interferometer.md) ~~ [Информация](info.md) ~~ [КНД](directivity.md) ~~ [Код Рида‑Соломона](rsco.md) ~~ [КПДА](antenna.md) ~~ [КСВ](swr.md) ~~ [КУ](ку.md) ~~ [ЛКС, АОЛС, FSO](fso.md) ~~ [Несущий сигнал](carrwave.md) ~~ [ПНА, ПОНА, ПСНА](devd.md) ~~ [Помехи](emi.md) (EMI, RFI) ~~ [Последняя миля](last_mile.md) ~~ [Регламент радиосвязи](comms.md) ~~ [СИТ](etedp.md) ~~ [Фидер](feeder.md) <br>~ ~ ~ ~ ~<br> **РФ:** [БА КИС](ба_кис.md) (21) ~~ [БРК](brk_lav.md) (12) ~~ [РУ ПНИ](ру_пни.md) () ~~ [HSXBDT](comms_lst.md) (1.8) ~~ [CSXBT](comms_lst.md) (0.38) ~~ [ПРИЗЫВ-3](comms_lst.md) (0.17) *([ПРИЗЫВ-1](comms_lst.md) (0.075))**|

1. Docs: …
1. <…>


## The End

end of file
