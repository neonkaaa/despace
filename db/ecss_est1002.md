# ecss_est1002

[TOC]

---

## Annex A (normative) Verification plan (VP) — DRD

**A.1 DRD identification.** *Requirement ID & source* — ECSS‑E‑ST‑10‑02, requirement 5.2.8.1b. *Purpose & objective* — the **Verification Plan (VP)** contains the overall verification approach, the model philosophy, the product matrix, the verification strategies for the requirements (the interrelation between different methods/levels/stages of verification to be used to demonstrate status of compliance to requirements), the test, inspection, analysis & review‑of‑design programme with the relevant activity sheets & planning, the verification tools, the verification control methodology, the involved documentation, the verification management & organization.

**A.2 Expected response**

*Special remarks.* The Verification Plan **may be combined with the AIT Plan** in one single AIV Plan; in this case VP & AIT plans do not exist anymore as single entities.

*Scope & content:*

1. **Introduction.** The purpose, objective, content & the reason prompting its preparation. State & describe open issues, assumptions & constraints relevant to this document.
1. **Applicable & reference documents, Definitions & abbreviations.** The applicable & reference documents in support to the generation of the document. The applicable dictionary or glossary & the meaning of specific terms or abbreviations utilized in the document.
1. **Verification subject.** Briefly describe the subject of the verification process.
1. **Verification approach.** Describe the basic verification concepts & definitions (methods, levels & stages).
1. **Model philosophy.** Describe the selected models & the associated model philosophy, product matrix.
1. **Verification Strategy.** Describe the selected combination of the different verification methods at the applicable verification levels & stages, in general & for each requirement type/group (including software). Provide the allocation of the requirements to the specific verification tasks.
1. **Verification programme.** Document the verification activities & associated planning in the applicable verification stages. Detail the analysis, review ­of ­design, inspection & test programmes through dedicated activity sheets, or through reference to the AIT Plan.
1. **Verification tools.** Describe high level definitions of the verification tools to be used, such as S/W facilities, special tools, simulators, analytical tools.
1. **Verification control methodology.** Describe the proposed methodology to be utilized for verification monitoring & control including the use of a verification data base.
1. **Documentation.** List the involved verification documents & describe their content.
1. **Organization & management**
   1. Describe the responsibility & management tools applicable to the described verification process.
   1. Describe the responsibilities within the project team, the relation to product assurance, quality control & configuration control (including anomaly handling & change control) as well as the responsibility sharing with external partners.
   1. Describe & plan the relevant reviews & responsibilities.



## Annex B (normative) Verification control document (VCD) — DRD

**B.1 DRD identification.** *Requirement ID & source* — ECSS‑ST‑E‑10‑02, requirement 5.2.8.2b. & 5.4.4.1b. *Purpose & objective* — the **Verification Control Document (VCD)** lists the requirements to be verified with the selected methods in the applicable stages at the defined levels. It includes the **Verification Matrix (VM)**. The VCD is a living document & provides traceability during the phase C, D & E, how & when each requirement is planned to be verified & is actually verified. The VCD becomes part of the EIDP, as detailed in ECSS‑Q‑ST‑20.

**B.2 Expected response**

*Special remarks* — None.

*Scope & content:*

1. **Introduction.** The purpose, objective, content & the reason prompting its preparation. State & describe open issues, assumptions & constraints relevant to this document. Phase the VCD content with the product life‑cycle such that the initial issue contains the verification matrix, intermediate issues cover the planned on‑ground verifications & their executions evidence (in particular for qualification & acceptance completion), the in‑orbit & post landing activities; final issue provides evidence of the close‑out of the overall verification process.
1. **Applicable & reference documents, Definitions & abbreviations.** The applicable & reference documents in support to the generation of the document. List the applicable dictionary or glossary & the meaning of specific terms or abbreviations utilized in the document.
1. **Verification subject.** Describe the verification control approach applied to the product, the involved documentation & the computerized tool used to support the process. Include the requirements to be verified (with reference to the specifications involved), call up the verification methods, levels & stages definitions & explain the verification close­out criteria.
1. **Verification summary status.** Summarize the current Verification Close‑out status for each issue of the VCD.
1. **Verification control data**
   1. Collect in the form of a matrix, for each requirement, the following  verification information:
      1. Requirement identifier
      1. Requirement text
      1. Traceability between requirement
      1. Levels & stages of verification
      1. Methods
      1. Link to the relevant section of the verification plan & any planning document (For example, test specification)
      1. References to any documentation that demonstrates compliance to the requirements (For example, report, analysis, waivers, RFD, NCR, NRB, customer closeout records
      1. Status of Compliance (yes, no, partial)
      1. Close‑out status (open/closed)
      1. Reasons of the close‑out status
   1. The initial issue of the VCD shall contain a verification matrix limited to:
      1. Requirement identifier
      1. Requirement text
      1. Traceability between requirement
      1. Levels & stages of verification
      1. Methods
      1. Link to the relevant section of the verification plan



## Annex C (normative) Test report — DRD

**C.1 DRD identification.** *Requirement ID & source* — ECSS‑E‑ST‑10‑02, requirement 5.3.2.1b. *Purpose & objective* — the **Test Report (TR)** describes test execution, test & engineering assessment of results & conclusions in the light of the test requirements (including pass‑fail criteria). The test report contains the scope of the test, the test description, the test article & set‑up configuration, & the test results including the as­run test procedures, the considerations & conclusions with particular emphasis on the close­out of the relevant verification requirements including deviations.

**C.2 Expected response**

*Special remarks* — None.

*Scope & content:*

1. **Introduction.** The purpose, objective, content & the reason prompting its preparation. State & describe open issues, assumptions & constraints relevant to this document.
1. **Applicable & reference documents, Definitions & abbreviations.** The applicable & reference documents in support to the generation of the document. Include as applicable reference documents the corresponding test procedure & test specification as specified in the DRDs in ECSS‑E‑ST‑10‑03. The applicable dictionary or glossary & the meaning of specific terms or abbreviations utilized in the document.
1. **Test results.**
   1. The test results with supporting data (including the test execution dates, the as run procedure, & the test facility results).
   1. The analysis of test data & the relevant assessment.
   1. Provide a synthesis of the test results.
1. **Anomalies.** Include the list of deviations to the test procedure, the nonconformance including failures & the problems.
1. **Conclusions**
   1. Summarize:
      1. the test results, including: ➀ the list of the requirements to be verified (in correlation with the VCD), ➁ traceability to used documentation, ➂ conformance or deviation including references & signature & date)
      1. the comparison with the requirements
      1. the verification close‑out judgment
   1. Clearly state & describe open issues
   1. Cross‑conference aeparate test analyses



## Annex D (normative) Review‑of‑design report — DRD

**D.1 DRD identification.** *Requirement ID & source* — ECSS‑E‑ST‑10‑02, requirement 5.3.2.3b. *Purpose & objective* — the **review‑­of‑­design report (RDR)** describes each verification activity performed for reviewing documentation. The RDR contains proper evidence that the relevant requirements are verified & the indication of deviations.

**D.2 Expected response**

*Special remarks* — None.

*Scope & content:*

1. **Introduction.** The purpose, objective, content & the reason prompting its preparation. State & describe open issues, assumptions & constraints relevant to this document.
1. **Applicable & reference documents, Definitions & abbreviations.** The applicable & reference documents in support to the generation of the document. The applicable dictionary or glossary & the meaning of specific terms or abbreviations utilized in the document with the relevant meaning.
1. **Review‑of‑design summary.** Describe the review‑of‑design activity in terms of method & procedures used.
1. **Conclusions.** Summarize the review‑of‑design results, including:
   1. the list of the requirements to be verified (in correlation with the VCD)
   1. traceability to used documentation
   1. conformance or deviation including references & signature & date
   1. the comparison with the requirements
   1. the verification close‑out judgment
   1. clearly state & describe open issues



## Annex E (normative) Inspection report — DRD

**E.1 DRD identification.** *Requirement ID & source* — ECSS‑E‑ST‑10‑02, requirement 5.3.2.4b. *Purpose & objective* — the **inspection report (IR)** describes each verification activity performed for inspecting hardware or software. The IR contains proper evidence that the relevant requirements are verified & the indication of deviations.

**E.2 Expected response**

*Special remarks.* The IR **may be a part of the Test Report if** the verification by Inspection is carried‑out in combination with Testing.

*Scope & content:*

1. **Introduction.** The purpose, objective, content & the reason prompting its preparation. State & describe open issues, assumptions & constraints relevant to this document.
1. **Applicable & reference documents, Definitions & abbreviations.** The applicable & reference documents in support to the generation of the document. The applicable dictionary or glossary & the meaning of specific terms or abbreviations utilized in the document with the relevant meaning.
1. **Inspection summary.** Describe the product configuration data of the inspected item.
1. **Conclusions.** Summarize the following (clearly state & describe open issues):
   1. inspection results, including: ➀ the list of the requirements to be verified (in correlation with the VCD), ➁ traceability to used documentation, ➂ inspection event location & date, ➃ expected finding, ➄ conformance or deviation including proper references & signature & date
   1. comparison with the requirements
   1. verification close‑out judgment



## Annex F (normative) Verification report — DRD

**F.1 DRD identification.** *Requirement ID & source* — ECSS‑E‑ST‑10‑02, requirement 5.3.2.5b. *Purpose & objective* — the **Verification Report (VR)** is prepared when more than one of the defined verification methods are utilized to verify a requirement or a specific set of requirements. It reports the approach followed & how the verification methods were combined to achieve the verification objectives. The positive achievement constitutes the completion of verification for the particular requirement.

**F.2 Expected response**

*Special remarks* — None.

*Scope & content:*

1. **Introduction.** The purpose, objective, content & the reason prompting its preparation. State & describe open issues, assumptions & constraints relevant to this document.
1. **Applicable & reference documents, Definitions & Abbreviations.** The applicable & reference documents in support to the generation of the document. The applicable dictionary or glossary & the meaning of specific terms or abbreviations utilized in the document with the relevant meaning Verification subject.
1. **Verification results.** Describe the verification approach, the associated problems & results with reference to the relevant test, analysis, review­ of­ design & inspection reports. Identify the deviations from the verification plan.
1. **Conclusions.** List the requirements to be verified (in correlation with the VCD). Summarize verification results, the comparison with the requirements & the verification close­out judgement. Clearly state & describe open issues.


## Annex G (deleted)
