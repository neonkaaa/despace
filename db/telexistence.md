# Telexistence Inc.
> 2021.10.21 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/t/telexistence_inc_logo1t.webp)](f/c/t/telexistence_inc_logo1.webp)|<info@tx-inc.com>, +81(3)6427-4001, Fax …;<br> *Cross Dock Harumi 1st Floor, 4-7-4 Harumi, Chuo-ku, Tokyo, 104-0053, Japan*<br> <https://tx-inc.com> ・ [FB ⎆](https://www.facebook.com/telexistence) ・ [LI ⎆](https://www.linkedin.com/company/telexistenceinc) ・ [X ⎆](https://twitter.com/telexistenceinc)|
|:--|:--|
|**Business**|Design, manufacture, & operate robots|
|**Mission**|To create the world where every single person on every planet will benefit from the robotic revolution|
|**Vision**|Grasping every object with our hands, from anywhere|
|**Values**|➀ Freedom & Responsibility, ➁ Move Fast, Deliver 1st, ➂ Systematic Innovator of Scale, ➃ Begin with the End, ➄ Interdependence|
|**[MGMT](mgmt.md)**|・Co‑CEO, Founder — Jin Tomioka|

**Telexistence Inc.** was founded in 2017 to design, manufacture, & operate robots in anywhere we see an opportunity of pushing the fundamental state of art forward. Founders: Charith Fernando, Ferunando Charisu, Susumu Tachi.

<p style="page-break-after:always"> </p>

**Principles at TX.** TX envisions to grasp every object with our hands from everywhere. We want to create a world where every single person on every planet benefits from the robotic revolution. Our core principles are the guideline to our employees to help us get there. Unlike some of the companies who define corporate values but get ignored, our core principles genuinely resonate with our team. We break them down into specific behaviors & skills we would expect every TX employee to demonstrate. Our principles consist of below five components:

1. **Freedom & Responsibility.** Freedom is an essential principle at TX. As we live in modern democratic society, we have the right to choose our own actions. As long as we live in such a society, our actions & choices are free. Thus, we also need to be responsible for our own decisions about what to do & what not to do. We believe this is the best state of happiness. At the same time, freedom could be deemed 「Heavy」. Because of this heaviness, people do not exercise their freedom & act as instructed by society & organizations. Many people can’t endure the heaviness of freedom. At TX, employees are expected to exercise freedom & take full responsibility for their own choices. Great people don’t need to be managed. Once a direction is set, they make their own choices to achieve goals. Behavioral Indicators:
   - Acts autonomously to best leverage his/her strengths; is not shy about being unique & unconventional; is not confined in generally accepted social expectations & norms.
   - Fully exercises his/her authority within the scope of his/her work & makes his/her own decision rather than following others’. Takes responsibility for the outcome.
   - Is personally invested & demonstrates the highest standard in his/her role to represent TX’s purpose; leads others by examples.
1. **Move Fast, Deliver 1st.** Fast delivery is a key thing at TX. When developing new features, we do not waste too much time on conceptualizing & planning.  Think of the minimum requirements to realize the base product 1st & deliver it as quickly as possible. It is faster to improve off of a working prototype than a design drawing.  The same concept applies to the business team; we don’t get caught up in the process. We take the shortest path to success; execution is everything. Behavioral Indicators:
   - Emphasizes actions & problem solving rather than making plans; proactively seeks & develops creative ways to solve puzzles.
   - Accepts & embraces the unknown & ambiguous rather than being threatened; can stay focused on the goals & choose the fastest way to reach them.
   - Thinks in idealistic terms & demonstrates a 「can‑do」 attitude; exhibits courage to take smart risks to challenge the status quo.
1. **Systematic Innovator of Scale.** We do things like a systematic innovator of scale does. Innovator creates new things. Scale means big (at least 10x cause). Ideas at TX should be systematic & scalable. Systematic means making sure the idea is reproducible & recurring, & scalability makes sure that seeds of scalability are built in that idea. Every idea should be provable, explainable, & have a reason to make sure we are always clear on what, how, & why we are doing what we are. Behavioral Indicators:
   - Owns long‑term goals & a sense of purpose; takes calculated risks, makes decisions, & takes actions based on purposes; holds himself/herself responsible for proving & explaining the reasons for actions.
   - Seeks for holistic & integrated approach rather than occasional, haphazard, or unplanned activities.
   - Owns analytic view on current problems as well as outside the box thinking to solve real‑life problems in a repeatable manner.
1. **Begin with the End.** It’s a method we use at TX when conceptualizing how to realize new & scalable ideas. When planning for the goal, the timeline should be challenging to a point where it seems almost impossible to achieve at 1st glance. We believe that the 「Begin with the end」(in other words, backcasting) method coupled with a challenging timeline pushes people to think out of the box & develop imaginative solutions to meet that goal. Suppose we start from the beginning & slowly work ourselves up when trying to achieve things (forecasting). In that case, you will most likely end up planning on a normal path with an average time frame giving you mediocre results. Behavioral Indicators:
   - Identifies the desired future state 1st, looks back to assess requirements to reach the future state, seeks imaginative new ideas to meet the requirements.
   - Seeks for greater future opportunities behind current actions; is willing to put in extra legwork to see his/her vision achieved in the future.
   - Invites others to articulate a shared intention about the future state to find new ways of working together to achieve it.
1. **Interdependence.** People working at TX should be at least independent. But this is not enough. Our goal is to create a company where people are interdependent on each other.  This is a state in which 2 or more people with different skillsets support one another in a way where 1 + 1 = 4.  When 2 people of the same skillset work with each other they are independent of each other & will usually create a situation where 1 + 1 = 2. Behavioral Indicators:
   - Exhibits trust in others & generate support amongst coworkers. Emphasizes team effort & provides a safe environment for others to take risks.
   - Enables an environment where issues are addressed openly & immediately; is comfortable hearing & saying uncomfortable truths when necessary.
   - Generates a strong sense of belongings amongst coworkers; reinforces shared beliefs to move forward as a team; is comfortable reaching out to others for help in order to reach goals.


## The End

end of file
