# Параллакс
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [Space](index.md), **[СИ, формулы](si.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Параллакс** — русскоязычный термин. **Parallax** — англоязычный эквивалент.</small>

**Паралла́кс** *(греч. παραλλάξ, от παραλλαγή, 「смена, чередование」)* — изменение видимого положения объекта относительно удалённого фона в зависимости от положения наблюдателя.

Зная расстояние между точками наблюдения **L** (база) и угол смещения **α**, можно определить расстояние до объекта:  
`D = L / (2·sinα/2)`

Для малых углов (α — в радианах):  
`D = L / α`

Параллакс используется в геодезии и астрономии для измерения расстояния до удалённых объектов (в частности в [[глоссарий:международная_система_единиц_си|специальных единицах> — [парсеках](parsec.md)). На явлении параллакса основано бинокулярное зрение.

|Схема параллакса|Измерение расстояния при помощи параллакса|
|:--|:--|
|[![](f/si/parallax_example_rut.webp)](f/si/parallax_example_ru.webp)|[![](f/si/parallax_telemetre_parallaxe_principet.webp)](f/si/parallax_telemetre_parallaxe_principe.webp)|



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**`СИ, формулы:`**<br> [Атмосфера](atmosphere.md) ~~ [Квази](quasi.md) ~~ [Параллакс](parallax.md) ~~ [Парсек](parsec.md) ~~ [Ускорение свободного падения](g.md)|
|**【[Space](index.md)】**<br> [Apparent magnitude](app_mag.md) ~~ [Astro.object](aob.md) ~~ [Blue Marble](earth.md) ~~ [Cosmic rays](ion_rad.md) ~~ [Ecliptic](ecliptic.md) ~~ [Escape velocity](esc_vel.md) ~~ [Health](health.md) ~~ [Hill sphere](hill_sphere.md) ~~ [Information](info.md) ~~ [Lagrangian points](l_points.md) ~~ [Near space](near_space.md) ~~ [Pale Blue Dot](earth.md) ~~ [Parallax](parallax.md) ~~ [Point Nemo](earth.md) ~~ [Silver Snoopy award](silver_snoopy_award.md) ~~ [Solar constant](solar_const.md) ~~ [Terminator](terminator.md) ~~ [Time](time.md) ~~ [Wormhole](wormhole.md) ┊ ··•·· **Solar system:** [Ariel](ariel.md) ~~ [Callisto](callisto.md) ~~ [Ceres](ceres.md) ~~ [Deimos](deimos.md) ~~ [Earth](earth.md) ~~ [Enceladus](enceladus.md) ~~ [Eris](eris.md) ~~ [Europa](europa.md) ~~ [Ganymede](ganymede.md) ~~ [Haumea](haumea.md) ~~ [Iapetus](iapetus.md) ~~ [Io](io.md) ~~ [Jupiter](jupiter.md) ~~ [Makemake](makemake.md) ~~ [Mars](mars.md) ~~ [Mercury](mercury.md) ~~ [Moon](moon.md) ~~ [Neptune](neptune.md) ~~ [Nereid](nereid.md) ~~ [Nibiru](nibiru.md) ~~ [Oberon](oberon.md) ~~ [Phobos](phobos.md) ~~ [Pluto](pluto.md) ~~ [Proteus](proteus.md) ~~ [Rhea](rhea.md) ~~ [Saturn](saturn.md) ~~ [Sedna](sedna.md) ~~ [Solar day](solar_day.md) ~~ [Sun](sun.md) ~~ [Titan](titan.md) ~~ [Titania](titania.md) ~~ [Triton](triton.md) ~~ [Umbriel](umbriel.md) ~~ [Uranus](uranus.md) ~~ [Venus](venus.md)|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Parallax>



## The End

end of file
