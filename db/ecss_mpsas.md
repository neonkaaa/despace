# ECSS Margin philosophy for science assessment studies (15‑Jun‑2012)
> 2012.06.15 [🚀](../../index/index.md) [despace](index.md) → [Doc](doc.md), [ECSS](ecss.md), [SC](sc.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

## 1. Introduction

This document is intended to establish a common margin philosophy for the Cosmic Vision 2015‑2025 & the Mars & Robotic Exploration Program (MREP) assessment studies within SRE‑PA. This philosophy is consistent with the relevant ECSS recommendations ([ECSS‑E‑ST‑10‑02C](ecss_est1002.md), [ECSS‑E‑ST‑31C](ecss_est31.md), & [ECSS‑E‑ST‑50‑05C](ecss_est5005.md) rev.1); & based on the previous version of the technical note on Margin Requirements applied to previous assessment studies of science missions (Bepi‑Colombo, Solar Orbiter, & CDF studies & industrial activities, namely: SCI‑A/2003.302/AA & SCI‑PA/2007/022/). It includes an improved section concerning temperature & cooling power margins (cryogenic system).


## 2. Margins requirements & management

### 2.1. Mass margins

The next 2 sections describe 2 different approaches to propellant estimation & sizing of the propulsion system. **A clear agreement with the Agency is required before selecting one approach.**

1. 2.1.1 uses the Nominal Dry Mass plus the 20 % system level margin to calculate the propellant mass. For pre‑assessment studies, when no/little detail is available of the **spacecraft (SC)** design.
1. 2.1.2 uses Maximum Separated Mass to calculate the propellant mass. For studies for which a higher level of detail is available (e.g. advanced assessment or definition phase studies).

|**2.1.1. Margin on total dry mass**|**2.1.2. Margin on maximum separated mass**|
|:--|:--|
|[R‑M1‑1] The total dry mass at launch of the SC shall include an ESA system level mass margin of ≥ 20 % of the nominal dry mass at launch (this shall include the equipment level margin as per R‑M1‑4).|[R‑M2‑1] Same|
|[R‑M1‑2] The nominal dry mass at launch does not include the ESA system level mass margin, but shall include the design maturity mass margins to be applied at equipment level (R‑M1‑4).|[R‑M2‑2] Same|
|[R‑M1‑3] This ESA system level mass margin shall:|[R‑M2‑3] Same|
|・Be explicitly visible & traceable in the overall mass budget of the SC|Same|
|・Be uniformly distributed among the dry mass of all modules or elements of the SC|Same|
|・Not include any propellant residuals or unused propellant|Same|
|[R‑M1‑4] At equipment level, the following design maturity mass margins shall be applied:|[R‑M2‑4] Same|
|・[R‑M1‑41] ≥ 5 % for 「Off‑The‑Shelf」 items (ECSS Category: A/B, see [ECSS‑E‑ST‑10‑02C](ecss_est1002.md))|Same|
|・[R‑M2‑42] ≥ 10 % for 「Off‑The‑Shelf」 items requiring minor mods (ECSS Category: C, see [ECSS‑E‑ST‑10‑02C](ecss_est1002.md))|Same|
|・[R‑M1‑43] ≥ 20 % for new designed/developed items, or items requiring major mods or re‑design (ECSS Category: D, see [ECSS‑E‑ST‑10‑02C](ecss_est1002.md))|Same|
|[R‑M1‑5] The propellant calculation shall be based on the total dry mass at launch (as per R‑M1‑1).|[R‑M2‑5] The propellant mass shall be calculated using the Maximum Separated Mass (this one shall be defined for each mission in agreement with ESA).|
|[R‑M1‑6] A 2 % of propellant residuals shall be added to the propellant calculated in R‑M1‑5. The residuals are not part of the other margins.|[R‑M2‑6] Same|
|[R‑M1‑7] Harness mass shall be ≥ 5 % of nominal dry mass.|[R‑M2‑7] Harness mass shall be ≥ 5 % of nominal dry mass at launch (as per R‑M2‑1).|
|[R‑M1‑9] The **propulsion modules (PM)** of the SC shall be designed & sized for the total wet mass, including the previously mentioned margins.|[R‑M2‑8] The PMs of the SC shall be designed & sized for the Maximum Separated Mass.|
|[R‑M1‑10] The volume of the tanks of the PMs shall be sized for the total propellant mass (including margins) plus ≥ 10 %.|[R‑M2‑9] The volume of the tanks of the PMs shall be sized for the Maximum Separated Mass plus ≥ 10 %.|
|[R‑M1‑8] Launcher margins (if any) are mission dependant & shall be defined case by case, in accordance with ESA. They shall be explicitly visible, applied on actual launcher performance after subtraction of payload adapter mass.|—|


### 2.2. Volume margins

During the assessment activities (Phase 0/A) uncertainties on actual units size induce the risk of underestimating the SC volume, with consequences on system level & configuration trade‑off decisions. In order to mitigate this risk, the level of volume growth available to the different units will be quantified & monitored through‑out the study activities, with specific attention to elements with larger dimensional uncertainty.

[R‑V‑1] The potential for equipment volume growth associated to a given SC design shall be assessed via a suitable Figure of Merit (used volume vs available volume) & monitored throughout the study work.

[R‑V‑2] All equipment (including both platform & payload items) subject to potential volume growth shall be identified & potential volume drivers clearly indicated.

[R‑V‑3] Volume allocations shall be defined to cover potential growth of equipment (according to maturity margin), in order to ensure that equipment with full used margins can still be accommodated.

[R‑V‑4] Equipment where a potential volume growth is already considered by other margins (e.g. propellant tanks covered via dV margins, navigation margins; solar panel growth covered via power margin; mass memory covered via data budget margins, etc.) shall be exempted.


### 2.3. Delta‑V margins

[R‑DV‑1] The following margins (covering uncertainties in mission design & system performance) shall be applied to the Effective dV manoeuvres:

1. [R‑DV‑11] 5 % for accurately calculated manoeuvres (trajectory ones as well as detailed orbit maintenance ones)
1. [R‑DV‑12] 100 % for general (not analytically derived) orbit maintenance manoeuvres, over the lifetime (maintenance ones calculated in detail shall be handled as per R‑DV‑11)
1. [R‑DV‑13] 100 % for the attitude control & angular momentum management manoeuvres

[R‑DV‑2] When manoeuvres budgets concern theoretical values, & do not take into account gravity losses (e.g.: impulsive manoeuvres by chemical thrusters), such losses shall be quantified & added to the specified Effective dV.

[R‑DV‑3] Launcher dispersion for each launch vehicle shall be assessed & included.

[R‑DV‑4] The Launcher performance penalty shall be assessed over the entire applicable launch window, to avoid using ideal performance only.

[R‑DV‑5] In case of **Gravity Assist Manoeuvres (GAM)**, an allocation of either 15 ㎧ (planetary GAMs) or 10 ㎧ (GAMs of planetary moons) shall be added for chemical propulsion to the dV budget for each GAM, to account for preparation & correction of these manoeuvres. For electrical propulsion, 35 ㎧ shall be applied per GAM. (These numbers are based on previous SolO analysis for Venus Gravity Assist Manoeuvres. Should the applicability be in doubt, these numbers shall be verified for the relevant mission profile)

[R‑DV‑6] For electric propulsion, a 5 % dV margin shall be foreseen for navigation during the cruise phase, to compensate for trajectory inaccuracies.

[R‑DV‑7] At destination, the SC shall be able to perform orbit & attitude control manoeuvres, necessary for orbit maintenance during the lifetime.


### 2.4. Power margins

[R‑P‑1] At equipment level & for conventional electronic units, the following design maturity power margins shall be applied:

1. [R‑P‑11] ≥ 5 % for 「Off‑The‑Shelf」 items (ECSS Category: A/B)
1. [R‑P‑12] ≥ 10 % for 「Off‑The‑Shelf」 items requiring minor mods (ECSS Category: C)
1. [R‑P‑13] ≥ 20 % for new designed/developed items, or items requiring major mods or re‑design (ECSS Category: D)

[R‑P‑2] Should electric propulsion be considered, the design maturity power margins specified in R‑P‑1 of the electronic equipment of the electric propulsion system (e.g. power conditioners, ion thruster drivers, etc.) shall only be applied to the dissipated power.

[R‑P‑3] The total power budget of the SC shall include an ESA system level power margin of ≥ 20 % of the nominal power requirements of the SC.

[R‑P‑4] The ESA system level power margin shall be explicitly visible & traceable in the overall power budget of the SC composite.

[R‑P‑5] The nominal power requirements include the power requirements of all SC elements (payload & platform, including their respective design maturity power margins to be applied at equipment level), & does not include the ESA system level power margin.

[R‑P‑6] Solar arrays & batteries shall be sized to provide the SC required power, including all specified margins, at **End Of Life (EOL)**.


### 2.5. Data processing margins

[R‑SW‑1] Any on‑board memory (Random Access Memory RAM used for code &/or data) shall include a memory margin of ≥ 50 %.

[R‑SW‑2] Any on‑board processor peak usage shall be ≤ 50 % of its maximum processing capability.


### 2.6. Communications margins

[R‑C‑1] Links (up‑ & down‑link) budgets & associated margins, for all phases of the mission, shall be computed as per [ECSS‑E‑ST‑50‑05C](ecss_est5005.md) rev.1, including: nominal, adverse, favourable, mean – 3 sigma & worst RSS (Root Sum Square) cases.

[R‑C‑2] Telecommand & telemetry data rates shall be satisfied with minimum margins as per [ECSS‑E‑ST‑50‑05C](ecss_est5005.md) rev.1, for all mission phases, under all cases specified in R‑C‑1.


### 2.7. Thermal control margins

[R‑T‑1] The different temperature ranges (calculated, predicted, design, acceptance, qualification) shall be in accordance with [ECSS‑E‑ST‑31C](ecss_est31.md).

**Cryogenic systems**

This section describes 2 different approaches to heat load estimation & corresponding sizing of the cryogenic cooling system. A clear agreement with the Agency is required before selecting one approach. R‑T‑3 is more detailed & shall be favoured.

1. R‑T‑2 uses larger cooling power margin requirements. For pre‑assessment studies, when no or little detail is available on the cooling system design
1. R‑T‑3 uses a sensitivity analysis to calculate the heat load uncertainties & lower margins. For studies for which a higher level of detail is available (e.g. advanced assessment or definition phase studies)

[R‑T‑2] The thermal control system shall provide 50 % margin on heat rejection capabilities:

1. For radiators, this can be achieved by margins in available area &/or temperature. More specifically for temperatures, Tdesign ≤ Trequired / ∜1.5 with T in K.
1. For active coolers, 50 % margins shall be available on the max cooling power.

[R‑T‑3] **Uncertainties.** A sensitivity analysis on the following parameters shall be carried out to assess the uncertainties of the design:

1. Electrical dissipation ±30 %
1. Conductance of Kevlar, Composite/Plastic materials — ±30 %
1. Conductance of Metallic Material (except Harness) — ±15 %
1. Conductance of Harness — ±30 %
1. Multi Layer Insulation (MLI) efficiency — ±50 %
1. Contact Resistance — ±50 %
1. Uncertainties on Emissivities:
   1. High Emissivity (> 0.2) — ±0.03
   1. Low Emissivity (< 0.2) — ±0.02
1. Uncertainties on Interface Temperatures:
   1. ≥ 270 K — ±5 K
   1. 80 ‑ 270 K — ±3 K
   1. 20 ‑ 80 K — ±1 K
   1. 10 ‑ 20 K — ±0.5 K
1. Uncertainties on aging effects: if aging effects are identified (e.g. know contamination), ±10 % shall be applied on the effect of the aging (e.g. if a cooler provides 0.01 W less cooling power in EOL than in BOL, the aging uncertainty is ±0.001 W).

This sensitivity analysis campaign will permit to determine 2 kinds of uncertainties (equivalent to design maturity margins):

1. Uncertainties on Temperatures in case of passive cooling
1. Uncertainties on Heat Loads & Temperatures if active cooling

**Thermal Control System Margins.** To make the design of the Cryogenic Subsystem robust, the following system margins are recommended (equivalent to system level margins):

1. Passive cooling — +15 % on Radiator Surfaces to be considered for accommodation
1. Active cooling: the design of the Cryogenics Subsystem must demonstrate 15 % margin on the available cooling power (e.g. if the coolers can provide 1 W cooling power, the system must demonstrate 0.85 W heat loads, including uncertainties).

**Qualification Margin for Mechanical Coolers.** To guarantee the heat lift capabilities of a Mechanical Cooler, the following Qualification margin on the maximum stroke shall be applied:

1. For Coolers with TRL 3 ‑ 5 — ≥ 20 %
1. For Coolers with TRL 5+ — ≥ 10 % (i.e. the guaranteed maximum heat lift of the Cooler corresponds to the heat lift at 90 % of the stroke)


## The End

end of file
