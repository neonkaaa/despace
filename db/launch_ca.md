# Launch Canada
> 2021.06.29 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c//_logo1t.webp)](f/c//_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *…*<br> <http://www.launchcanada.org>|
|:--|:--|
|**Business**|Student community, LV & small sats building|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

<p style="page-break-after:always"> </p>

## The End

end of file
