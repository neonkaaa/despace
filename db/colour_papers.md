# Colour papers
> 2019.11.04 [🚀](../../index/index.md) [despace](index.md) → **[Док](doc.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Цветные книги** — русскоязычный термин. **Colour papers** — англоязычный эквивалент.</small>

**Colour papers** — a common name for a set of documents describing a technology or a result of an activity.

**Grey literature** (or **gray literature**) is materials & research produced by organizations outside of the traditional commercial or academic publishing & distribution channels. Common grey literature publication types include reports (annual, research, technical, project, etc.), working papers, government documents, white papers & evaluations. Organizations that produce grey literature include government departments & agencies, civil society or non-governmental organizations, academic centres & departments, & private companies & consultants. **Grey literature** may be made available to the public, or distributed privately within organizations or groups, & may lack a systematic means of distribution & collection. The standard of quality, review & production of grey literature can vary considerably. Grey literature may be difficult to discover, access, & evaluate, but this can be addressed through the formulation of sound search strategies.

**Цветные книги** (Colour papers) — общее название для ряда документов, описывающих технологию или результат деятельности.

1. **Blue paper** (синяя книга) sets out technical specifications of a technology or item of equipment.
1. **Green paper** (зелёная книга) is a proposal or consultative document rather than being authoritative or final.
1. **White paper** (белая книга) is an authoritative report or guide that informs readers concisely about a complex issue & presents the issuing body’s philosophy on the matter. It is meant to help readers understand an issue, solve a problem, or make a decision.
1. **Yellow paper** (жёлтая книга) is a document containing research that has not yet been formally accepted or published in an academic journal. It is synonymous to the more widely used term preprint.



## Blue book
A **blue book** or **bluebook** is an almanac, buyer’s guide or other compilation of statistics & information. The term dates back to the 15th century, when large blue velvet‑covered books were used for record‑keeping by the Parliament of the United Kingdom. The Oxford English Dictionary 1st records such a usage in 1633. The term has a variety of other meanings.

| | |
|:--|:--|
|**Academia & education**|・The Yale College Programs of Study, referred to as the Blue Book<br> ・Blue book exam, a type of test involving writing an essay, typically into a pamphlet — traditionally blue colored — called a 「blue book」|
|**Construc-<br>tion**|The Blue Book of Building & Construction, was a yellow pages‑like buyers guide of company information targeted towards commercial construction, 1st published in 1913. The guide contains information on architects, contractors, manufacturers, suppliers, vendors, & other companies relevant to the commercial bidding & build process. The hardbound book was retired in 2016 & subsequently replaced by a bi‑annual regional magazine called the Who’s Who in Building & Construction. The prior content of the hard bound book is now completely online & called The Blue Book Network. The company which publishes the information is Jefferson Valley, New York‑based Contractor’s Register.|
|**Computing & technology**|・Blue Book (CD standard), the name of one format from the Rainbow Book standards for compact discs, defining the Enhanced CD standard<br> ・Blue Book (CCITT), a set of telecommunications recommendations issued by the International Telecommunication Union Standardisation Sector in 1988<br> ・A common name of the book Smalltalk‑80: The Language & its Implementation, & any virtual machine implementation based on Smalltalk<br> ・Blue Book protocol, the file transfer protocol of the Coloured Book protocols<br> ・The Java Virtual Machine specification<br> ・Blue Books, spacecraft data & telemetry recommendations made by the [Consultative Committee for Space Data Systems](ccsds.md)<br> ・Ingo Wegener’s Blue Book, an influential textbook in circuit complexity formally titled The Complexity of Boolean Functions (1987)|
|**Country — UK**|・Colonial Blue Books, yearly collection of details wide range of matters from each colonial governor in the British Empire beginning in 1822<br> ・Blue Book (Office for National Statistics), published annually by the Office for National Statistics; contains the estimates of the domestic & national product, income & expenditure for the United Kingdom<br> ・Any official report in the UK of Parliament or the Privy Council, which in the 19th & early 20th centuries were standardly issued in a dark blue paper cover<br> ・「Treachery of the Blue Books」, the name given in Wales to the 1847 parliamentary report on the state of education in the country<br> ・A weekly digest of signals intelligence reports by the British intelligence agency GCHQ<br> ・「The Blue Book」, genocide scholars' nickname for The Treatment of Armenians in the Ottoman Empire, 1915 ‑ 1916<br>      ・The Blue Book, Political Truth or Historical Fact, a 2009 documentary film about this report|
|**Country — US**|・Blue Book (FCC), a nickname for a report on Public Service Responsibility of Broadcast Licensees issued on March 7, 1946 by the Federal Communications Commission of the United States<br> ・Blue Book (United States Marine Corps), a bulletin listing the lineal precedence & seniority of Marine Corps officers<br> ・Project Blue Book, a U.S. Air Force study on UFOs<br> ・Regulations for the Order & Discipline of the Troops of the United States, a Revolutionary War drill manual written by Baron von Steuben for the Continental Army, colloquially referred to as the Blue Book<br> ・Various information registers from U.S. state governments:<br>      ・Official Manual State of Missouri (「Missouri Blue Book」)<br>      ・Oregon Blue Book<br>      ・Tennessee Blue Book<br>      ・Wisconsin Blue Book<br> ・Blue Book of Denver society, 1st called the Who’s Who in Denver Society by Louise Sneed Hill<br> ・The Blue Book of the John Birch Society, a transcript of the founding meeting of the John Birch Society|
|**Govern-<br>ment & finance**|・In the Traineeship scheme of the European Commission, internship candidates who pass a preselection are added to a Blue Book, from which the final successful candidates are chosen<br> ・The publications of the European Central Bank describing the main payment & securities settlement systems in the EU Member States.<br> ・A publication of the United Nations Protocol & Liaison Service containing listings relating to Permanent Missions to the United Nations.|
|**Transpor-<br>tation**|・Aircraft bluebook, a digest that covers the price & condition of used general aviation aircraft in the U.S; the Aircraft Bluebook Rating Scale (or 「Bluebook scale」) is used in the aviation industry to rate the condition of used aircraft.<br> ・Automobile Blue Book, a route guide to American intercity transportation published between 1901 ‑ 1929<br> ・Kelley Blue Book, an automotive appraisal guide from the company of the same name; it is the United States' largest automotive vehicle valuation company|



## Green Book
In the European Union, the Commonwealth countries, Hong Kong & the United States, a **green paper** is a tentative government report & consultation document of policy proposals for debate & discussion. A green paper represents the best that the government can propose on the given issue, but, remaining uncommitted, it is able without loss of face to leave its final decision open until it has been able to consider the public reaction to it. Green papers may result in the production of a white paper. They may be seen as grey literature.

| | |
|:--|:--|
|**Canada**|A green paper in Canada, like a white paper, is an official government document. Green papers tend to be statements not of policy already determined, but of propositions put before the whole nation for discussion. They are produced early in the policy‑making process, while ministerial proposals are still being formulated. Many white papers in Canada have been, in effect, green papers, while at least one green paper — that on immigration & population in 1975 — was released for public debate after the government had already drafted legislation.|
|**United Kingdom**|Similarly, in the UK, green papers are official consultation documents produced by the government for discussion both inside & outside Parliament, for instance when a government department is considering introducing a new law.|
|**European Union**|A green paper released by the European Commission is a discussion document intended to stimulate debate & launch a process of consultation, at European level, on a particular topic. A green paper usually presents a range of ideas & is meant to invite interested individuals or organizations to contribute views & information. It may be followed by a white paper, an official set of proposals that is used as a vehicle for their development into law.|



## White paper
> <small>**Белая книга** — русскоязычный термин. **White paper** — англоязычный эквивалент.</small>

A **white paper** is an authoritative report or guide that informs readers concisely about a complex issue & presents the issuing body’s philosophy on the matter. It is meant to help readers understand an issue, solve a problem, or make a decision.

The initial British term concerning a type of government‑issued document has proliferated, taking a somewhat new meaning in business. In business, a white paper is closer to a form of marketing presentation, a tool meant to persuade customers & partners & promote a product or viewpoint. White papers may be considered grey literature.

**Белая книга** (англ. **white paper**) — официальное сообщение в письменном виде, обычно этот термин применяется в США, Великобритании, Ирландии и других англоговорящих странах. Это может быть:

1. государственное сообщение, поясняющее политику;
1. документация, описывающая новый технологический процесс или алгоритм;
1. мини‑книга, гибрид брошюры и большой статьи (вайт пейпер, инструмент контент‑маркетинга);
1. официальная документация, содержащая описание решения;
1. справочный документ для корпоративных клиентов.

Выделяют три основных типа WP:

1. 「Бэкграундер」 — подробно рассказывает про преимущества продукта и (или) услуги или методологии.
1. 「Проблема‑решение」 — фокусирует внимание аудитории на насущной проблеме и предлагает решение.
1. 「Результаты исследования」 — приводит обобщённую статистику о состоянии дел в индустрии или отдельном её сегменте.

Руководство [Как писать white paper ❐](f/doc/white_paper_howto_write.pdf).



### In government
The term white paper originated with the British government, & many point to the Churchill White Paper of 1922 as the earliest well‑known example under this name. Gertrude Bell, the British explorer & diplomat, was the 1st woman to author a White Paper. Her 149 page report was entitled, 「Review of the Civil Administration of Mesopotamia,」 & was presented to Parliament in 1920. In British government it is usually the less extensive version of the so‑called blue book, both terms being derived from the colour of the document’s cover.

White papers are a 「… tool of participatory democracy … not [an] unalterable policy commitment.」 「White papers have tried to perform the dual role of presenting firm government policies while at the same time inviting opinions upon them.」

In Canada, a white paper is 「…a policy document, approved by Cabinet, tabled in the House of Commons & made available to the general public.」 The 「provision of policy information through the use of white & green papers can help to create an awareness of policy issues among parliamentarians & the public & to encourage an exchange of information & analysis. They can also serve as educational techniques.」

White papers are a way the government can present policy preferences before it introduces legislation. Publishing a white paper tests public opinion on controversial policy issues & helps the government gauge its probable impact.

By contrast, green papers, which are issued much more frequently, are more open‑ended. Also known as consultation documents, green papers may merely propose a strategy to implement in the details of other legislation, or they may set out proposals on which the government wishes to obtain public views & opinion.

Examples of governmental white papers include, in Australia, the White Paper on Full Employment and, in the United Kingdom, the White Paper of 1939 & the 1966 Defence White Paper.

In Israeli history, the White Paper of 1939 — marking a sharp turn against Zionism in British policy & at the time greeted with great anger by the Jewish Yishuv community in Mandatory Palestine — is remembered as 「The White Paper」 (in Hebrew Ha'Sefer Ha'Lavan הספר הלבן — literally 「The White Book」).



### In business‑to‑business marketing
Since the early 1990s, the term 「white paper」, or 「whitepaper」, has been applied to documents used as marketing or sales tools in business. These white papers are long‑form content designed to promote the products or services from a specific company. As a marketing tool, these papers use selected facts & logical arguments to build a case favorable to the company sponsoring the document. B2B (business‑to‑business) white papers are often used to generate sales leads, establish thought leadership, make a business case, or inform & persuade prospective customers, channel partners, journalists, analysts, or investors.

White papers are considered to be a form of content marketing or inbound marketing; in other words, sponsored content available on the web with or without registration, intended to raise the visibility of the sponsor in search engine results & thus build web traffic. Many B2B white papers argue that one particular technology, product or method is superior to others for solving a specific business problem. They may also present research findings, list a set of questions or tips about a certain business issue, or highlight a particular product or service from a vendor.

There are, essentially, three main types of commercial white papers:

1. Backgrounder: Describes the technical or business benefits of a certain vendor’s offering; either a product, service, or methodology. This type of white paper is best used to supplement a product launch, argue a business case, or support a technical evaluation at the bottom of the sales funnel.
1. Numbered list: Presents a set of tips, questions, or points about a certain business issue. This type is best used to get attention with new or provocative views, or cast aspersions on competitors.
1. Problem/solution: Recommends a new, improved solution to a nagging business problem. This type is best used to generate leads at the top of the sales funnel, build mind share, or inform & persuade stakeholders, building trust & credibility in the subject.

While a numbered list may be combined with either other type, it is not workable to combine the detailed product information of a backgrounder with the industry‑wide perspective of a problem/solution white paper.



## Docs/Links
|**Sections & pages**|
|:--|
|**【[](.md)】**<br> <mark>NOCAT</mark>|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Blue_book>
1. <https://en.wikipedia.org/wiki/Green_paper>
1. <https://en.wikipedia.org/wiki/White_paper>
1. <https://habr.com/ru/post/450244> — Как писать и продвигать White Paper?


## The End

end of file
