# Payload Definition Document
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [Doc](doc.md), [OE](sc.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Payload Definition Document (PDD)** — EN term. **Описание полезной нагрузки** — literal RU translation.</small>

**Payload Definition Document (PDD)** — the document used by [ESA](esa.md) during the [R&D](rnd.md) process which contains a description, requirements & characteristics of scientific tasks of the mission & payload, as well as an analysis of the possibility of implementing the mission with this payload.

**Typical table of contents:**

1. 1 INTRODUCTION
   1. 1.1 Scope
   1. 1.2 Reference documentation
      1. 1.2.1 Applicable documents
      1. 1.2.2 Reference documents
   1. 1.3 Acronyms
   1. 1.4 Definitions
1. 2 SYSTEM ARCHITECTURE OVERVIEW
   1. 2.1 Spacecraft Architecture
   1. 2.2 Reference Payload complement
1. 3 INSTRUMENT PERFORMANCE REQUIREMENTS
1. 4 PAYLOAD INTERFACE REQUIREMENTS
1. 5 REFERENCE PAYLOAD COMPLEMENT
   1. 5.1 Payload unit #1
      1. 5.1.1 Payload Overview and Design Justification
      1. 5.1.2 Instrument electronics/Data handling
      1. 5.1.3 Budgets/ Interfaces
         - 5.1.3.1 Mass budget
         - 5.1.3.2 Thermal/Cryogenic Budget
         - 5.1.3.3 Power Budget
         - 5.1.3.4 Data Rate Budget
      1. 5.1.4 Volume
         - 5.1.4.1 Configuration
         - 5.1.4.2 Mass
      1. 5.1.5 Data
      1. 5.1.6 Power



## Docs/Links
|**Sections & pages**|
|:--|
|**`Documents:`**<br> …|
|**`Бортовая аппаратура (БА):`**<br> [PDD](pdd.md) ~~ [Антенна](antenna.md) ~~ [АПС](hns.md) ~~ [БУ](eas.md) ~~ [ЗУ](ds.md) ~~ [Изделие](unit.md) ~~ [КЛЧ](clean_lvl.md) ~~ [ПЗР](fov.md) ~~ [ПО](soft.md) ~~ [Прототип](prototype.md) ~~ [Радиосвязь](comms.md) ~~ [СКЭ](elmsys.md) ~~ [ССИТД](tsdcs.md) ~~ [СИТ](etedp.md) ~~ [УГТ](trl.md) ~~ [ЭКБ](elc.md) ~~ [EMC](emc.md)|

1. Docs: [Solar Orbiter PDD ❐](f/doc/pdd_solar_orbiter_esa_2004.pdf) (2004 г) ~~ [EChO PDD ❐](f/doc/pdd_echo_esa_2013.pdf) (2013 г)
1. <…>


## The End

end of file
