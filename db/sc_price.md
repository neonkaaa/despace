# Стоимость грамма КА
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [Проекты](project.md), **[ТЭО](fs.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Стоимость грамма КА** — русскоязычный термин, не имеющий аналога в английском языке. **Price for 1 g of a spacecraft** — дословный перевод с русского на английский.</small>

Средняя годовая стоимость 1 грамма некоторых металлов на международном рынке.  
Тро́йская у́нция — единица измерения массы, равная 31.1034768 грамма.



## Цены

|**Год**|**Проект**|**Стоимость, ₽<br> КА**|**Стоимость, ₽<br> 1 гр золота**|**Стоимость, ₽<br> 1 гр КА**|**Стоимость, ₽<br> 1 Мб (инф. на Землю)**|
|:--|:--|:--|:--|:--|:--|
|2014|[Луна‑25-27](луна_26.md)|205 000 000 000|2 395|35 300|78 ‑ 162|

**О деньгах**

|**·     ·     ·     Металл<br>Год, цена за 1 ㎏**|**Золото**|**Платина**|**Калифорний‑252**|**Осмий**|**Осмий‑187**|**Палладий**|**Родий**|
|:--|:--|:--|:--|:--|:--|:--|:--|
|2019, ₽<br>$<br>€|2 875 400<br> 44 237<br> 40 255|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|
|2018, ₽<br>$<br>€|…<br> 40 589<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|
|2017, ₽<br>$<br>€|…<br> 40 431<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|
|2016, ₽<br>$<br>€|…<br> 40 302<br> …|1 050<br> …<br> …|635 000 000<br> …<br> …|860<br> …<br> …|1 270 000<br> …<br> …|2 300<br> …<br> …|730<br> …<br> …|
|2015, ₽<br>$<br>€|…<br> 37 252<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|
|2014, ₽<br>$<br>€|…<br> 40 716<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|
|2013, ₽<br>$<br>€|…<br> 45 309<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|
|2012, ₽<br>$<br>€|…<br> 53 659<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|
|2011, ₽<br>$<br>€|…<br> 50 526<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|
|2010, ₽<br>$<br>€|…<br> 39 370<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|
|2009, ₽<br>$<br>€|…<br> 31 262<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|
|2008, ₽<br>$<br>€|…<br> 28 035<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|
|2007, ₽<br>$<br>€|…<br> 22 358<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|
|2006, ₽<br>$<br>€|…<br> 19 402<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|
|2005, ₽<br>$<br>€|…<br> 14 299<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|
|2004, ₽<br>$<br>€|…<br> 13 173<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|
|2003, ₽<br>$<br>€|…<br> 11 683<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|
|2002, ₽<br>$<br>€|…<br> 9 959<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|
|2001, ₽<br>$<br>€|…<br> 8 715<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|
|2000, ₽<br>$<br>€|…<br> 8 974<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|…<br> …<br> …|



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Project](project.md)】**<br> [Interferometer](interferometer.md) ~~ [NASA open](nasa_open.md) ~~ [NASA STI](nasa_sti.md) ~~ [NIH](nih.md) ~~ [Past, future and everything](pfaeverything.md) ~~ [PSDS](us_psds.md) [MGSC](mgsc.md) ~~ [Raman spectroscopy](spectroscopy.md) ~~ [SC price](sc_price.md) ~~ [SC typical forms](sc.md) ~~ [Spectrometry](spectroscopy.md) ~~ [Tech derivative laws](td_laws.md) ~~ [View](view.md)|
|**`Технико‑экономическое обоснование (ТЭО):`**<br> [NICM](nicm.md) ~~ [Невозвратные затраты](sunk_cost.md) ~~ [Номинал](nominal.md) ~~ [Оценка стоимости работ на НПОЛ](lav.md) ~~ [Секвестр](budget_seq.md) ~~ [Стоимость аппарата в граммах](sc_price.md)|

1. Docs: …
1. <http://goldomania.ru/menu_003_003.html> — [archived ❐](f/archive/20191021_1.pdf) 2019.10.21
1. 2017.10.09 [Сколько стоит космос](https://habr.com/ru/post/373875) — [archived ❐](f/archive/20171009_1.pdf) 2019.01.28



## The End

end of file
