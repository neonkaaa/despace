# CONAE
> 2019.08.05 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/c/conae_logo1t.webp)](f/c/c/conae_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *…*<br> <http://www.conae.gov.ar> ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/Comisión_Nacional_de_Actividades_Espaciales)|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Национальная комиссия по космической деятельности (исп. Comisión Nacional de Actividades Espaciales, CONAE)** — гражданское аргентинское государственное космическое агентство, отвечающее за космическую программу страны. Появилось в 1991 году в результате реорганизации Национальной комиссии по исследованию космоса (исп. Comisión Nacional de Investigaciones Espaciales, CNIE), существовавшей в Аргентине с 1960 года. Основано 28 января 1960 (как CNIE) и 28 мая 1991 (как CONAE).



## The End

end of file
