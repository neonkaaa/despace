# UNASTELLA
> 2022.04.04 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/u/unastella_logo1t.webp)](f/c/u/unastella_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *25, Sinchon-ro 12-gil, Seoul, KR*<br> <https://www.unastella.co> ~~ [FB ⎆](http://www.facebook.com/unastellaco) ~~ [IG ⎆](https://www.instagram.com/unastella.co) ~~ [LI ⎆](https://www.linkedin.com/company/unastella)|
|:--|:--|
|**Business**|Small LV for space tourism|
|**Mission**|Unastella provides a sub‑orbital space travel service by developing a launch vehicle that can reach space beyond 100 ㎞ with a total of 6 people on board|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO, Founder — Jae Park|

**UNASTELLA Corporation** means 「one star」 in Latin, it’s a startup that wants to become one of the brightest stars in the 「new space era」.

Starting with sub‑orbital space tourism (~100 ㎞) in 2027, we will expand the manned space flight service to low‑orbit manned space transport (~400 ㎞) & deep space manned space exploration (Earth geostationary orbit, Moon, Mars).

Unastella aims to develop Korea’s 1st & Asia’s 1st manned space launch vehicle system based on the expertise of the members gathered around the representatives from the German Space Center & the network of related industries such as rocket developers & parts suppliers.

**Vision.** Unastella, which dreams of becoming the 1st in Asia to dream of commercial manned space flight beyond Korea, wants to become Asia’s largest 「manned space flight platform」 company through the passionate passion of professionals with rich experience & know‑how & technology management based on domestic & foreign launch vehicle industry networks. Unastella will present a new paradigm of ESG management in the new space era by actively contributing to solving global warming, utilizing space solar energy, developing new resource infrastructure outside the Earth, & coexisting with space industry partners.

<p style="page-break-after:always"> </p>

## The End

end of file
