# Venera 2, 3
> 2019.12.10 [🚀](../../index/index.md) [despace](index.md) → [Venus](venus.md), **[Project](project.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Венера-2, Венера-3** — Russian terms with no analogues in English. **Venera 2 (3MV-4 No.4), Venera 3 (3MV-3 No.1)** — literal EN translation.</small>

**Venera 2** (**Венера-2**) and **Venera 3** (**Венера-3**) were a Soviet spacecrafts intended to explore [Venus](venus.md). A 3MV-4 spacecraft launched as part of the Venera programme, it failed to return data after flying past Venus. Venera 2 flew at a distance of 24 000 ㎞ from Venus, Venera 3, probably landed on Venus. In addition to data on space and near‑planet space in the year of the calm Sun, scientific data on Venus itself was not received.

[![](f/project/v/venera_2_3/pic01t.webp)](f/project/v/venera_2_3/pic01.webp)



|**Type**|**[Param.](si.md)**|
|:--|:--|
|**【Mission】**|~ ~ ~ ~ ~ |
|Cost|… or … ㎏ of [gold](sc_price.md)|
|Development|…|
|Duration|**Venera 2** 3 m 15 d;<br>**Venera 3** 3 m 13 d|
|Launch|**Venera 2** 1965.11.12 05:02 UTC, [Baikonur](spaceport.md), Rocket: Molniya 8K78M;<br> **Venera 3** 1965.11.16 04:19 UTC, [Baikonur](spaceport.md), Rocket: Molniya 8K78M|
|Operator|O㎅-1|
|Programme|Venera programme|
|Similar to|・Proposed: [Venera 4](venera_4.md), [Vega 1 and 2](vega_1_2.md)<br>  • Current: …<br> ・Past: Luna 2, [Mariner 2](mariner_2.md)|
|Target|[Venus](venus.md) exploration|
|[Type](sc.md)|Orbiter; lander|
|**【Spacecraft】**|~ ~ ~ ~ ~ |
|Composition|**Venera 2** Orbiter, **Venera 3** Lander, orbiter|
|Comms|2 m parabolic high‑gain antenna (decimeter range)|
|Contractor|…|
|[ID](spaceid.md)|**Venera 2** NSSDC ID (COSPAR ID): 1965-091A (<https://nssdc.gsfc.nasa.gov/nmc/spacecraft/display.action?:id=1965-091A>), SCN: 1730<br> **Venera 3** NSSDC ID (COSPAR ID): 1965-092A (<https://nssdc.gsfc.nasa.gov/nmc/spacecraft/display.action?:id=1965-092A>), SCN: 1733|
|Manufacturer|O㎅-1|
|Mass|**Venera 2** 963 ㎏ ([medium satellite](sc.md), [EVN‑072](venus.md))<br> **Venera 3** 960 ㎏ ([medium satellite](sc.md), [EVN‑072](venus.md))|
|Orbit / Site|**Venera 2** Heliocentric 0.72 AU × 1.2 AU, T = 341 days.<br> **Venera3** Heliocentric 0.68 AU × 0.99 AU; T = 277 days|
|Payload|**Venera 2** Camera, spectrometers, detectors, magnitometer;<br> **Venera 3**: *Orbiter*: Magnitometers, Discharge Counters and Semiconductor Detector<br> *Lander*: Photometer, Gas Analyzer, Temperature, pressure and density sensors, Movement detector, Gamma Ray Counter|
|Power|…|

Achieved targets & investigations:

1. **T** — technical; **C** — contact research; **D** — distant research; **F** — fly‑by; **H** — manned; **S** — soil sample return; **X** — technology demonstration
1. **Sections of measurement and observation:**
   1. Atmospheric/climate — **Ac** composition, **Ai** imaging, **Am** mapping, **Ap** pressure, **As** samples, **At** temperature, **Aw** wind speed/direction.
   1. General — **Gi** planet’s interactions with outer space.
   1. Soil/surface — **Sc** composition, **Si** imaging, **Sm** mapping, **Ss** samples.

<small>

|**EVN‑XXX**|**T**|**EN**|**Section of m&o**|**D**|**C**|**F**|**H**|**S**|
|:--|:--|:--|:--|:--|:--|:--|:--|:--|
|EVN‑005|T|Exploration: from Venusian orbit.| |D| |F| | |
|EVN‑072|T|Exploration with [satellites](sc.md): medium satellites.| |D| |F| | |

</small>



## Mission

**Venera 2**

Venera 2 was launched by a Molniya carrier rocket, flying from Site 31/6 at the Baikonur Cosmodrome. The launch occurred 1965.11.12 at 05:02 UTC.

The spacecraft made its closest approach to Venus at 1966.02.27 02:52 UTC, at a distance of 23 810 ㎞ ([EVN‑005](venus.md)).

During the fly‑by, all of Venera 2’s instruments were activated, requiring that radio contact with the spacecraft be suspended. The spacecraft was to have stored data using onboard recorders, and then transmitted it to Earth once contact was restored. Following the fly‑by the spacecraft failed to reestablish communications with the ground. It was declared lost on 1966.05.04. An investigation into the failure determined that the spacecraft had overheated due to a radiator malfunction.

**Venera 3**

Venera 3 was launched 1965.11.16 at 04:19 UTC from Baikonur.

The spacecraft’s initial trajectory missed Venus by 60 550 ㎞ and a course correction manoeuvre was carried out on 1965.12.26 which brought the spacecraft onto a collision course with the planet ([EVN‑005](venus.md)). Contact with the spacecraft was lost on 1966.02.15 probably due to overheating.

The lander crashed on Venus on 1966.03.01, making Venera 3 the 1st lander to hit the surface of another planet.

Venera 2 and 3 were not able to transmit data on Venus itself, but scientific data on space and near‑planetary space in the year of the calm Sun were obtained. The large volume of measurements during the flight was of great value for studying the problems of ultra‑long‑range communication and interplanetary flights. Magnetic fields, cosmic rays, low‑energy charged particle fluxes, solar plasma fluxes and their energy spectra, cosmic radio emissions and micrometeorites were studied.



## Science goals & payload
**Venera 2**

1. The mission of Venera 2 was to take pictures and to explore the planet Venus.
1. The Venera 2 spacecraft was equipped with cameras, as well as a magnetometer, solar and cosmic x‑ray detectors, piezoelectric detectors, ion traps, a Geiger counter and receivers to measure cosmic radio emissions, etc.
1. **Orbiter:**
   1. Charged particles detectors;
   1. Gas‑discharge and solid‑state cosmic ray detectors; Cosmic ray detectors were of two types and consisted of gas‑discharge meters and solid‑state silicon detectors. The parabolic antenna of the decimeter range radiometer was mounted on a ring between the control compartment and the instrument compartment;
   1. Micrometeorite detector;
   1. Receivers of space radio emission in the range 20 ‑ 2 000 ㎑;
   1. Solar plasma detector in decimeter radio range;
   1. Spectrometer on the oxygen and hydrogen line in Lyman‑alpha;
   1. Three‑axis fluxgate magnetometer.
1. **Fly‑by compartment:**
   1. Facsimile system of photo and image transmission. The camera was equipped with a 200 ㎜ lens;
   1. Infrared spectrometer. It was designed to measure the thermal radiation of the atmosphere and clouds. It captured two bands, each split into 150 intervals, with a receiver from *InAs* used for measurements in the 1st band and a receiver from *LiF* in the second. The weight of the device was in the range of 13 ‑ 15 ㎏, its characteristic size was 50 ㎝. The device was mounted on the outside of the instrument compartment coaxially with the photography system and also included a visible range photometer as a reference signal source. The infrared spectrometer could also perform a spatial scan of the planet at two wavelengths: 9.5 and 18.5 microns.
   1. Ultraviolet spectrometer in the range 285 ‑ 355 nm, built into the photosystem;
   1. Ultraviolet spectrometer for ozone detection in the range 190 ‑ 275 nm;

**Venera 3**

1. The mission of Venera 3 was to land on the night side of Venus.
1. The lander of Venera 3 contained a radio communication system, scientific instruments, electrical power sources, and it was sterilised before launch.
1. **Orbiter:**
   1. Atomic hydrogen detector in the Lyman‑alpha line;
   1. Gas‑discharge and solid‑state cosmic ray detectors.;
   1. Three flux‑gate magnetometer to measure interplanetary magnetic fields;
   1. Solar plasma detector in decimeter radio range;
   1. Special sensors (traps) to measure the flow of charged particles and determination of low energy consumption of the amounts of solar plasma flows and their energy spectra. The instrument for measuring cosmic rays on Venera 3 was equipped with an additional gas‑discharge meter, while the micrometeorite detector and radio receiver were excluded from the scientific equipment.
1. **Lander:**
   1. Devices for determining the composition of the atmosphere, acidity and conductivity;
   1. Device for determining the inclination of the apparatus for measuring the level of mercury;
   1. Gamma ray detector to determine the composition of the soil and cosmic ray detector;
   1. Photometer for measuring the intrinsic glow of the atmosphere;
   1. Temperature, pressure and density sensors.



## Spacecraft
The Venera 2 and 3 spacecraft were built on the basis of 3MV.

Venera 3 comprised an lander, designed to enter the Venus atmosphere and parachute to the surface, and a carrier/fly‑by spacecraft, which carried the lander to Venus and also served as a communications relay for the lander.

**Orbital Compartment**

The core of the stack was a pressurized compartment called the Orbital Compartment. This part housed the spacecraft’s control electronics, radio transmitters and receivers, batteries, astro‑orientation equipment, and so on. The compartment was pressurized to around 100 ㎪ and thermally controlled to simulate earth‑like conditions, which removed the need for special electronic components that could reliably operate in extreme conditions (on [Zond 1](zond_1.md) the module depressurized in flight, severely damaging the spacecraft’s systems).

Mounted on the outside of the Orbital Compartment were two solar panels which supplied power to the spacecraft. They were folded against the body of the spacecraft during launch and were only deployed when the spacecraft was already on its interplanetary trajectory. On the ends of each solar panel was a hemispherical radiator which radiated excess heat from the orbital compartment into through a coolant loop.

Also mounted on the Orbital Compartment was a 2 m parabolic high‑gain antenna, used for long‑range communications. Depending on the mission, the spacecraft also used other antennas (for example, for communications with probes on the planet’s surface).

**Planetary Compartment**

Below the Orbital Compartment was a second pressurized compartment called the Planetary Compartment. Depending on the mission the Planetary Compartment either housed scientific equipment for orbital observation of the planet or was designed to detach and land on the planet’s surface.

**Engine**

Course correction capabilities were provided by a KDU 414 engine attached to the top of the Orbital Compartment. It provided a maximum thrust of around 2 kN used [UDMH](udmh.md) and nitric acid as propellants. Attitude control was achieved by several small cold gas thrusters.

The whole stack was 3.6 m high and weighted around 1 000 ㎏.



## Community, library, links

**PEOPLE:**

<mark>TBD</mark>

1. [Vladimir Dolgopolov](person.md) (Владимир Павлович Долгополов) Венера-3

**COMMUNITY:**

<mark>TBD</mark>



## Docs/Links
|**Sections & pages**|
|:--|
|**【[](.md)】**<br> <mark>NOCAT</mark>|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Venera_2>
1. <https://en.wikipedia.org/wiki/Venera_3>
1. <https://galspace.spb.ru/index495.html>


## The End

end of file
