# HOTDOCK
> 2024.06.09 [🚀](../../index/index.md) [despace](index.md) → [Interface](interface.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

**HOTDOCK** is a product line of mating interfaces for robotic manipulation providing an androgynous coupling to transfer mechanical loads, electrical power, data and (optionally) thermal loads through a single interface.

It allows assembly and reconfiguration of spacecraft and payloads on-orbit and on planetary surfaces. It is designed to support launch loads and makes it straightforward to replace failed modules, or to swap payloads and provides chainable data interfaces for multiple module configurations. Mounted on the tip of robotic manipulators, HOTDOCK performs as a quick connect /disconnect interface for end-effectors and tools.

Created by Space Applications Services, BE.

[HOTDOCK 2 pager ❐](f/if/hotdock_2pager.pdf)



## Docs/Links
|**Sections & pages**|
|:--|
|**【[Spacecraft (SC)](sc.md)】**<br> [Cleanliness level](clean_lvl.md) ~~ [Communication SC](sc.md) ~~ [Cubesat](sc.md) ~~ [FSS](sc.md) ~~ [HTS](sc.md) ~~ [Interface](interface.md) ~~ [Manned SC](sc.md) ~~ [Satellite](sc.md) ~~ [Sub-item](sui.md) ~~ [Typical forms](sc.md)|

1. Docs: …


## The End

end of file
