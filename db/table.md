# Таблица
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → **[КД](doc.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Таблица** — русскоязычный термин. **Table** — англоязычный эквивалент.</small>

**Таблица** — по [ГОСТ 2.102](гост_2_102.md) — документ, содержащий в зависимости от его назначения соответствующие данные, сведённые в таблицу.  
В соответствии с ГОСТ 2.102 входит в состав [конструкторской документации](doc.md).



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[](.md)】**<br> <mark>NOCAT</mark>|

1. Docs:
   1. [ГОСТ 2.102](гост_2_102.md)
1. <…>


## The End

end of file
