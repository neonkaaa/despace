# TOWING
> 2021.12.10 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/t/towing_logo1t.webp)](f/c/t/towing_logo1.webp)|<mark>noemail</mark>, +81(50)-5849-1414, Fax …;<br> *7 Chome-1, Maehamatori, Minami Ward, Nagoya, 〒457-0058 Aichi, Japan*<br> <https://towing.co.jp> ~~ [FB ⎆](https://www.facebook.com/towing.sorano)|
|:--|:--|
|**Business**|Space food, soil & cultivation|
|**Mission**|…|
|**Vision**|We guide & inspire people in the world for the future with full of smiles. Our services bring joys of eating with precious experience of making foods. For the people living on the Earth. And also for who will live in space.|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO — Kohei Nishida<br> ・COO & CTO — Ryoya Nishida|

**TOWING Inc.** We develop cultivation recipes with the next‑generation artificial soil & the algorithm for optimizing the cultivation process. Founded 2020.02.

Business:

1. Circular agriculture system (SORANO) service
1. Circular agriculture consulting service

<p style="page-break-after:always"> </p>

## The End

end of file
