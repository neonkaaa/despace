# ISC
> 2022.06.29 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/i/inclusive_sc_logo1t.webp)](f/c/i/inclusive_sc_logo1.webp)|<pr@inclusive.co.jp>, <mark>nophone</mark>, Fax …;<br> *5-10-2 Minami Aoyama, Minato-ku, Tokyo 2nd September Building 3F*<br> <https://inclusive.co.jp>|
|:--|:--|
|**Business**|Artificial sat data utilization consulting|
|**Mission**|Inclusive the world with excitement|
|**Vision**|Create new value with the power of DX & planning|
|**Values**|**Everything starts with emotional empathy.** Embrace the people & things in front of you. You can't make the world smile unless you can make a single smile. **Make Shigoto more interesting.** Let's challenge to make 「work」 & 「private matters」 more interesting. Because difficult things & new things are valuable. **Become a sincere & sincere person.** Let's face everything & become a person who is loved & depended on by others. The connection you get will be a treasure.|
|**[MGMT](mgmt.md)**|・CEO, President — Makoto Fujita|

**ISC (Inclusive Space consulting Co., Ltd.)** is a Japanese business development company aimed to comprehensively support the utilization of artificial satellites in the business, centering on business consulting & solution development utilizing data obtained from artificial satellites. In addition to the advertising‑related business, we are developing the personal billing business, SaaS for media‑related services, & regional revitalization‑related services. Founded 2007.04.03.

<p style="page-break-after:always"> </p>

## The End

end of file
