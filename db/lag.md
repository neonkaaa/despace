# Landing gear
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [LAG](lag.md), [SGM](sc.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Landing gear (LAG)** — EN term. **Посадочное устройство (ПУС)** — RU analogue.</small>

**Landing gear (LAG)** — [structural](sc.md) element of the [spacecraft](sc.md), designed to ensure the landing of a spacecraft on the surface of a planet or other celestial body. Variants of name (execution):

1. crumpled (or squash) landing gear (LAGS)
1. inflatable (or pneumatic) landing gear (LAGP)
1. parachute
1. [rotor](iu.md)
1. spring (pneumatic, hydraulic) landing device
1. torus landing gear (LAGT)



## Description
...


### Crumpled PUS (LAGS)

1. The LAGS on the Soviet Veneras were abandoned in the [Vega](vega_1_2.md) project: the landing speed was so low that there was no need for a crumpled LAG.
1. LAGS on crushable crash supports developed by MSTU Bauman 2020.02.11 was placed in the [NPOL incubator](project.md).



## Docs/Links
|**Sections & pages**|
|:--|
|**【[Structures, gears, materials (SGM)](sc.md)】**<br> [Гермоконтейнер](гермоконтейнер.md) ~~ [Датчик](sensor.md) ~~ [Задел](margin.md) ~~ [Изделие](unit.md) ~~ [Испарение материалов](matc.md) ~~ [Кавитация](cavitation.md) ~~ [КЗУ](cinu.md) (ВБУ КТ) ~~ [КХГ](cgs.md) ~~ [Контейнеры для транспортировки](ship_contain.md) ~~ [Крейцкопф](crosshead.md) ~~ [Номинал](nominal.md) ~~ [ПУС](lag.md) ~~ [ПНА, ПОНА, ПСНА](devd.md) ~~ [Резерв](reserve.md) ~~ [Слайс](слайс.md) ~~ [ТСП](tsp.md) ~~ [Типичные формы КА](sc.md) ~~ [Толкатель](толкатель.md) ~~ [Унификация](commonality.md)|
|**【[Landing gear (LAG)](lag.md)】**<br> [Rotor](iu.md)|

1. Docs:
   1. [О выборе количества ног ПУ ❐](f/lag/pust_o_vibore_kolichestva_nog.djvu) (2012)
   1. [Баумана, ПУСМ с краш-опорами ❐](f/lag/lags_2020baumana.pdf) (2020)
1. <…>


## The End

end of file
