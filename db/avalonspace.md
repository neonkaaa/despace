# AvalonSpace Ltd.
> 2020.07.31 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/a/avalonspace_logo1t.webp)](f/c/a/avalonspace_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *Bristol, Avon BS11AA, GB*<br> <http://www.avalon-space.co.uk> ~~ [LI ⎆](https://www.linkedin.com/company/avalonspace)|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**AvalonSpace Ltd.** is a startup intending to vertically launch low-cost orbital payloads from the UK. AvalonSpace's mission is to provide efficient, green and cost-effective UK launches utilising emerging technologies. Founded 2019.07.15.



## The End

end of file
