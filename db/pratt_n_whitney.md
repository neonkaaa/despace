# Pratt & Whitney
> 2021.07.09 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/p/pratt_n_whitney_logo1t.webp)](f/c/p/pratt_n_whitney_logo1.webp)|<help24@prattwhitney.com>, +1(800)565-0140, Fax …;<br> *400 Main Street, East Hartford, CT 06118, USA*<br> <https://prattwhitney.com>・ <https://www.pwc.ca> ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/Pratt_%26_Whitney)|
|:--|:--|
|**Business**|[Engines](ps.md), gas turbines|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Pratt & Whitney** is an American & Canadian aerospace manufacturer with global service operations. It is a subsidiary of Raytheon Technologies. Pratt & Whitney's aircraft & rocket engines are widely used in both civil aviation (especially airlines) & military aviation. As one of the "big three" aero-engine manufacturers, it competes with General Electric & Rolls-Royce, although it has also formed joint ventures with both of these companies. In addition to aircraft engines, Pratt & Whitney manufactures gas turbines for industrial & power generation, & marine turbines.

- **Canadian division:** 1000 Marie-Victorin Blvd., Longueuil, Quebec, J4G 1A1 Canada. <c1st@pwc.ca>, +1(800)268-8000

<p style="page-break-after:always"> </p>

## The End

end of file
