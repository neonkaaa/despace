# Odysseus
> 2022.04.04 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/o/odysseus_logo1t.webp)](f/c/o/odysseus_logo1.webp)|<info@odysseus.space>, +352 545 580 201, Fax …;<br> *9 Av. des Hauts-Fourneaux, L-4362 Esch-sur-Alzette, Luxembourg*<br> <https://odysseus.space> ~~ [FB ⎆](https://www.facebook.com/odysseus.space) ~~ [LI ⎆](https://www.linkedin.com/company/10673884) ~~ [X ⎆](https://twitter.com/ODYSSEUS_SPACE)|
|:--|:--|
|**Business**|Support for R&D, operations, launches, purchase, logistics|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO — Jordan Vannitsen<br> ・COO — Julien Hennequin<br> ・CTO — Marco Agnan|

**Odysseus** provides innovative products & services to facilitate the flow of goods & information anywhere in the Solar System:

- **Purchase & Logistics** — active in both Asia & Europe, ODYSSEUS supports its customers in finding the best‑fitting solution for their mission.
- **Launches** — ODYSSEUS makes sure its customers get the perfect launch for their mission & the required support for their satellite integration & delivery to the launch site.
- **Earth Observation** — ODYSSEUS provides 2.5m resolution images from its partner’s constellation. With a high revisit frequency of the satellites, the images will ultimately be updated daily.
- **Mission Operations** — with its partners S‑band & X‑band Ground Stations located at the heart of Asia, ODYSSEUS helps customers to communicate with their satellite at any given time of their satellite mission, from commissioning to decommissioning.
- **Satellite Design, Assembly, Integration & Testing** — ODYSSEUS offers ad‑hoc support to its customers for their Satellite Design, Assembly, Integration & Testing activities with a complete access to dedicated environmental testing facilities.
- **Mission Analysis** — ODYSSEUS helps to identify the needs & requirements of your space missions. Our team can review & support early mission design phases by delivering complete feasibility studies or mission analysis reports.

**Offices:**

- Tainan Office — Dongning Rd, 508, 6F, Tainan City 70165, +886 6 200 8281
- Taipei Office — Xinyi Rd, 170, 3F, Taipei City 10681

<p style="page-break-after:always"> </p>

## The End

end of file
