# Heat bank
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [TCS](tcs.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Storage heater (SH) / Heat bank** — EN term. **Тепловой аккумулятор (ТА)** — RU analogue.</small>

**Heat accumulator (TA)** — a device for accumulating heat/cold for the purpose of its further use. It is used in individual houses, apartments and in industry (for example, for the supply of thermal energy at a thermal power plant), as well as for starting engines.

There are heat accumulators with solid or melting heat storage material; liquid; steam; thermochemical; with electric heating element.



## Docs/Links
|**Sections & pages**|
|:--|
|**【Thermal control system (TCS)】**<br> [Thermal characteristics](thermal_chars.md) ~~ [Гермоконтейнер](гермоконтейнер.md) ~~ [Насосы для СОТР](сотр_насос.md) ~~ [Покрытия для СОТР](сотр_покрытия.md) ~~ [Радиатор](радиатор.md) ~~ [РИТ (РИТЭГ)](rtg.md) ~~ [Стандартные условия](sctp.md) ~~ [Тепловая труба](hp.md) ~~ [ТЗП](hs.md) ~~ [Тепловой аккумулятор](heat_bank.md) ~~ [ТСП](tsp.md) ~~ [Шторка](thermal_curtain.md) ~~ [ЭВТИ](mli.md)|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Storage_heater>


## The End

end of file
