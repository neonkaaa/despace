# SSC GGSN
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [Swedish Space Corporation](swedish_sc.md), **[НС](sc.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Swedish Space Corporation’s Global Ground Station Network (SSC GGSN)** — англоязычный термин, не имеющий аналога в русском языке. **Глобальная сеть наземных станций Шведской Космической Корпорации (SSC GGSN)** — дословный перевод с английского на русский.</small>

**Swedish Space Corporation’s Global Ground Station Network (SSC GGSN)** — глобальная сеть НС [Swedish Space Corporation](swedish_sc.md).

|**Расположение станций**|**И с указанием диапазона**|
|:--|:--|
|[![](f/gs/ssc_ggsnt.webp)](f/gs/ssc_ggsn.webp)|[![](f/gs/ssc_ggsn_bandt.webp)](f/gs/ssc_ggsn_band.webp)|

Основные станции:

1. **Clewiston.** USA, Florida, Clewiston, 26°44' N, 81°02' W. [Bands](comms.md): **S** (♁↗), **dual X** (🚀↘).
1. **Dongara.** Australia, Dongara, 29° 03' S, 115°21' E. [Bands](comms.md): **S** (♁↗ 🚀↘), **X** (♁↗ 🚀↘), **Ku** (♁↗ 🚀↘), **Ka** (♁↗ 🚀↘).
1. **Esrange.** Sweden, Kiruna, 67° 53' N, 21° 04' E. [Bands](comms.md): **UHF** (🚀↘), **S** (♁↗ 🚀↘), **X** (🚀↘).
1. **Inuvik.** Canada, Inuvik, 68° 24' N, 133° 30' W. [Bands](comms.md): **S** (♁↗ 🚀↘), **X** (♁↗ 🚀↘).
1. **North Pole.** USA, Alaska, North Pole, 64° 48' N, 147° 39' W. [Bands](comms.md): **S** (♁↗ 🚀↘), **X** (🚀↘).
1. **Punta Arenas.** Chile, Punta Arenas, 52°56' S, 70°51' W. [Bands](comms.md): **S** (♁↗ 🚀↘), **X** (♁↗ 🚀↘).
1. **Santiago.** Chile, Santiago de Chile, 33° 08' S, 70° 40' W. [Bands](comms.md): **S** (♁↗ 🚀↘), **C** (♁↗ 🚀↘), **Ka** (♁↗ 🚀↘).
1. **Siracha.** Thailand, Siracha, 13° 6' N, 100° 55' E. [Bands](comms.md): **S** (♁↗ 🚀↘), **X** (🚀↘).
1. **South Point.** USA, Hawaii, South Point, 19°1' N, 155°40' W. [Bands](comms.md): **S** (♁↗ 🚀↘), **X** (♁↗ 🚀↘), **Ku** (♁↗ 🚀↘).
1. **Yatharagga.** Australia, Dongara, 29° 05' S, 115°35' E. [Bands](comms.md): **S** (♁↗ 🚀↘), **X** (🚀↘), **Ka** (🚀↘).

Станции с кооперацией:

1. O'Higgins, Antarctica
1. Fucino, Italy
1. Weilheim, Germany
1. Madrid, Spain
1. Hartebesthook, South Africa
1. Bengaluru, India
1. Hokkaido, Japan
1. Okinawa, Japan



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**`Наземная станция (НС):`**<br> … <br><br> [CDSN](cdsn.md) ~~ [DSN](dsn.md) ~~ [ESTRACK](estrack.md) ~~ [IDSN](idsn.md) ~~ [SSC_GGSN](ssc_ggsn.md) ~~ [UDSC](udsc.md)|

1. Docs: …
1. <https://www.sscspace.com/ssc-worldwide/ground-station-network>


## The End

end of file
