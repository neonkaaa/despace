# Wind turbines
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [SPS](sps.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Wind turbines (WT)** — EN term. **Ветроэлектрическая установка (ВЭУ)** — RU analogue.</small>

**Wind turbine (WT)** — a device for converting the kinetic energy of the wind flow into mechanical energy of the rotor rotation with its subsequent transformation into electrical energy.

**Application area**

|**[Mercury](mercury.md)**|**[Venus](venus.md)**|**[Earth](earth.md)**|**[Moon](moon.md)**|**[Mars]( mars.md)**|**[And further](index.md)**|
|:--|:--|:--|:--|:--|:--|
|—|on the surface|on the surface|+|+|+|

**Specifications**

The power of the wind generator depends on the power of the air flow (**N**), determined by the wind speed and the swept area:  
`N = ρ × S × V³ / 2`,  
where: V — wind speed, ρ — air density, S — swept area.

【**Table.** Manufacturers】

1. **RU:**
   1. <mark>TBD</mark>



## Docs/Links
|**Sections & pages**|
|:--|
|**【[Spacecraft power system (SPS)](sps.md)】**<br> [Charge eff.](charge_eff.md) ~~ [EAS](eas.md) ~~ [EB](eb.md) ~~ [EMI, RFI](emi.md) ~~ [NR](nr.md) ~~ [Rotor](iu.md) ~~ [RTG](rtg.md) ~~ [Solar cell](sp.md) ~~ [SP](sp.md) ~~ [SPB/USPB](suspb.md) ~~ [Voltage](sps.md) ~~ [WT](wt.md)<br>~ ~ ~ ~ ~<br> **RF/CIF:** [BAK‑01](eas_lst.md) ~~ [KAS‑LOA](eas_lst.md)|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Wind_turbine>


## The End

end of file
