# Wisconsin University
> 2019.08.08 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/w/wisconsin_univ_logo1t.webp)](f/c/w/wisconsin_univ_logo1.webp)|<guide-help@lists.wisc.edu>, +1(608)263-24-00, Fax …;<br> *3HGQ+J2 Madisone, Wisconsin, USA*<br> <https://www.wisc.edu> ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/University_of_Wisconsin–Madison)|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Висконсинский университет в Мадисоне (англ. University of Wisconsin–Madison)** — государственный исследовательский университет, расположенный в Мадисоне, штат Висконсин, США. Ведущий кампус Висконсинского университета (англ.)русск., а также член‑основатель Ассоциации американских университетов. Основан в 1848 году.

The University maintains almost 100 research centers & programs, ranging from agriculture to arts, from education to engineering. It has been considered a major academic center for embryonic stem cell research ever since UW–Madison professor James Thomson became the 1st scientist to isolate human embryonic stem cells. This has brought significant attention & respect for the University’s research programs from around the world. The University continues to be a leader in stem cell research, helped in part by the funding of the Wisconsin Alumni Research Foundation & promotion of WiCell.



## The End

end of file
