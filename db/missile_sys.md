# Ракетный комплекс
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → **[СхД](draw.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Ракетный комплекс** — русскоязычный термин. **Missile system** — примерный англоязычный эквивалент.</small>

**Ракетный комплекс** (**РКпл**, иногда **РК**) — обобщающее понятие, включающее ракетные комплексы военного назначения и космические ракетные комплексы.



## Описание



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[](.md)】**<br> <mark>NOCAT</mark>|

1. Docs:
   1. [РК‑11](const_rk.md), стр.20.
1. <…>


## The End

end of file
