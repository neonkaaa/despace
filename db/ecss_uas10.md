# ECSS‑U‑AS‑10C (3 December 2019)
> 2023.12.12 [🚀](../../index/index.md) [despace](index.md) → [Doc](doc.md), [ECSS](ecss.md), [SC](sc.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

## 1. Introduction & Scope

**Space sustainability. Adoption Notice of ISO 24113: Space systems - Space debris mitigation requirements**

This Adoption Notice is one document of the series of ECSS Standards intended to be applied together for the management, engineering & product assurance in space projects & applications. ECSS is a cooperative effort of the European Space Agency, national space agencies & European industry associations for the purpose of developing & maintaining common standards. Requirements in this Standard are defined in terms of what shall be accomplished, rather than in terms of how to organize & perform the necessary work. This allows existing organizational structures & methods to be applied where they are effective, & for the structures & methods to evolve as necessary without rewriting the standards. This Adoption Notice has been prepared by the ECSS Space Debris Working Group, reviewed by the ECSS Executive Secretariat & approved by the ECSS Technical Authority.

ECSS does not provide any warranty whatsoever, whether expressed, implied, or statutory, including, but not limited to, any warranty of merchantability or fitness for a particular purpose or any warranty that the contents of the item are error-free. In no respect shall ECSS incur any liability for any damages, including, but not limited to, direct, indirect, special, or consequential damages arising out of, resulting from, or in any way connected to the use of this document, whether or not based upon warranty, business agreement, tort, or otherwise; whether or not injury was sustained by persons or property or otherwise; & whether or not loss was sustained from, or arose out of, the results of, the item, or any services that may be provided by ECSS.

This document identifies the clauses & requirements (including notes & clarifications) modified or added with respect to the standard ISO 24113, Space systems — Space debris mitigation requirements, Third edition 2019-07 (referred to as ISO 24113:2019) for application in ECSS.


## 2. Context information

The standard ISO 24113, Space systems — Space debris mitigation requirements has been developed by ISO TC20/SC14. The key space debris mitigation requirements have been thoroughly discussed at international level, agreed by the ISO members & published as standard ISO 24113.

Aiming at the development of world wide implementation standards dealing with space debris mitigation, ECSS has proactively contributed to the preparation of ISO 24113.

ECSS decided to adopt & apply ISO 24133 with a minimum set of modifications, identified in the present document, to account for the reference & applicable space debris mitigation documents existing in Europe & of the needs of the ECSS members.

In 2012, ECSS adopted ISO 24113:2011 with a minimum set of modifications (as per ECSS-U-AS-10C), which have been mostly incorporated in ISO 24113:2019. Moreover, ISO 24113:2019 represents a significant improvement with respect to the previous ISO 24113:2011. Therefore, ECSS decided to adopt & apply ISO 24113:2019 as it is, without any modifications of the requirements. However, in the present document a few clarifications with respect to ISO 24113:2019 & its application are provided to account for the needs of the ECSS members.

A major clarification addressed in the present document is to stress that space debris mitigation requirements apply to space objects in any bounded Earth orbit & apply also to space objects in unbounded Earth orbits in case there is a risk for interference with the LEO & GEO protected regions. Moreover, the present document provides clarifications about the evaluation of the probability of successful disposal based on reliability analyses & about verification methods to be agreed with the approving agents, accounting for existing ECSS implementation practices.


## 3. Application

ISO 24113:2019, Space systems — Space debris mitigation requirements, Third edition 2019-07 shall apply with the modifications & clarifications listed in Table 3-1.

【**Table 3-1:** Applicability table for ISO 24113:2019】

|**Clause or requirement number**|**Applicability**|**Applicable text (the new/added text is underlined)**|**Comments**|**Text as in the original document (deleted text with strikethrough)**|
|:--|:--|:--|:--|:--|
|3.8 - Earth orbit|Modified|Note 1 to entry: The requirements in this document do not apply to space objects (3.24) in an unbounded Earth orbit if, for at least 100 years after the space objects enter the unbounded Earth orbit:<br> ・the assessed risk of the space objects interference with the LEO & GEO (3.11) protected regions (3.21), or<br> ・the assessed risk of the space objects re-entry (3.22)<br> ・is less or equal to the corresponding threshold set by the approving agent.<br> ・To clarify better the text of Note 1 to ISO 24113:2019 3.8 (definition of Earth orbit)	Note 1 to entry: For the purposes of this document, it is not necessary to consider space objects (3.24) in unbounded Keplerian orbits if their probability of interference with the LEO & GEO (3.11) protected regions (3.21) is negligible.
3.20 – Probability of successful disposal	Added	Note 5 to entry: The calculation of this probability can be based on reliability analyses performed according to “ECSS-Q-ST-30 - Space product assurance – Dependability”, “ECSS-Q-HB-30-08 - Space product assurance - Components reliability data sources & their use”, or any other methods set by the approving agent.
	To clarify & add information on the methods to evaluate the probability of successful disposal	
7.2.2	Added	Note 1 to entry: The listed contents include specific requirements, methods, tools & guidelines for which: 
- either detailed verification & validation means are not specified, or 
- a risk assessment is required but a threshold is not specified. 
These would be set by the approving agent dealing with requirements in this document	To clarify the content of the SDMP with reference to the assessments of quantitative requirement.



## Annex A (informal) Space Debris Mitigation Plan (SDMP) — DRD

**A.1. DRD identification.** *Requirement ID & source* — ECSS‑U‑AS‑10. *Purpose & objective* — to define the approach, methods, procedures, resources & organization to co‑ordinate & manage all technical activities necessary to mitigate/eliminate debris generation.

**A.2. Expected response**

*Special remarks* — None.

*Scope & content:*

1. **Introduction.** The purpose, objective, content & the reason prompting its preparation (e.g. logic, organization, process or procedure).
2. **Applicable & reference documents.** The applicable & reference documents in support to the generation of the document.
3. **Plan.** Describe the approach, events & risks leading to debris generation, & plan of their mitigation.


## Annex B (informal) Cites from the ISO

TBD



## The End

end of file
