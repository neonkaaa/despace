# Blender
> 2019.09.15 [🚀](../../index/index.md) [despace](index.md) → **[Soft](soft.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Блендер** — русскоязычный термин. **Blender** — англоязычный эквивалент.</small>

**Blender** — профессиональное cвободное и открытое программное обеспечение для создания трёхмерной компьютерной графики, включающее в себя средства моделирования, скульптинга, анимации, симуляции, рендеринга, постобработки и монтажа видео со звуком, компоновки с помощью 「узлов」 (Node Compositing), а также создания 2D анимаций. В настоящее время пользуется большой популярностью среди бесплатных 3D‑редакторов в связи с его быстрым и стабильным развитием, тех‑поддержкой, потому что Blender имеет профессиональную команду разработчиков.

[Open-source, GNU GPL](soft.md).



## Возможности
Характерной особенностью пакета Blender является его небольшой размер по сравнению с другими популярными пакетами для 3D‑моделирования. Документация в поставку не входит, но доступна онлайн. Демонстрационные сцены можно скачать на официальном сайте или на сайте открытых проектов 「Blender Cloud」.

Функции пакета:

1. Поддержка разнообразных геометрических примитивов, включая полигональные модели, систему быстрого моделирования в режиме subdivision surface (SubSurf), кривые Безье, поверхности NURBS, metaballs (метасферы), скульптурное моделирование и векторные шрифты.
1. Универсальные встроенные механизмы рендеринга и интеграция с внешними рендерерами YafRay, LuxRender и многими другими.
1. Инструменты анимации, среди которых инверсная кинематика, скелетная анимация и сеточная деформация, анимация по ключевым кадрам, нелинейная анимация, редактирование весовых коэффициентов вершин, ограничители.
1. Динамика мягких тел (включая определение коллизий объектов при взаимодействии), динамика твёрдых тел на основе физического движка Bullet.
1. Система частиц включающая в себя систему волос на основе частиц.
1. Модификаторы для применения неразрушающих эффектов.
1. Язык программирования Python используется как средство определения интерфейса, создания инструментов и прототипов, системы логики в играх, как средство импорта/экспорта файлов (например, COLLADA), автоматизации задач.
1. Базовые функции нелинейного видео и аудио монтажа.
1. Композитинг видео, работа с хромакеем.
1. Трекинг камеры и объектов.
1. Real‑time контроль во время физической симуляции и рендеринга.
1. Процедурное и node‑based текстурирование, а также возможность рисовать текстуру прямо на модели.
1. Grease Pencil — инструмент для 2D анимации в полном 3D пайплайне.
1. Blender Game Engine — подпроект Blender, предоставляющий интерактивные функции, такие как определение коллизий, движок динамики и программируемая логика. Также он позволяет создавать отдельные real‑time‑приложения начиная от архитектурной визуализации до видео игр. Удалён в версии 2.8.



## Использование
НАСА разработало интерактивное веб‑приложение к третьей годовщине со дня посадки марсохода Кьюриосити, используя Blender и Blend4Web, позже переделанного на Verge3D for Blender. В приложении реализовано движение ровера, управление камерами и манипулятором, а также воспроизведены некоторые известные события миссии. Приложение было представлено в начале секции WebGL на конференции SIGGRAPH 2015.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Software](soft.md)】**<br> [ASP](asp.md) ~~ [Blender](blender.md) ~~ [C](plang.md) ~~ [Cosmographia](cosmographia.md) ~~ [DOORS](doors.md) ~~ [DWG](cad_f.md) ~~ [GIMP](gimp.md) ~~ [Git](git.md) ~~ [IGES](cad_f.md) ~~ [ISIS](isis.md) ~~ [JT](cad_f.md) ~~ [NGT](neogeography_toolkit.md) ~~ [NX](nx.md) ~~ [Octave](gnu_octave.md) ~~ [OS](os.md) ~~ [PDF](pdf.md) ~~ [Python](plang.md) ~~ [R](plang.md) ~~ [SPICE](spice.md) ~~ [STEP](cad_f.md) ~~ [STL](stk.md) ~~ [SVG](cad_f.md) ~~ [Syncthing](syncthing.md) ~~ [SysML](sysml.md) ~~ [Teamcenter](teamcenter.md) ~~ [Valispace](valispace.md) ~~ [Система управления версиями](vcs.md) ~~ [ХРИП](adra.md)|

1. Docs: …
1. <https://www.blender.org>


## The End

end of file
