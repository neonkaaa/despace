# Copper
> 2019.04.30 [🚀](../../index/index.md) [despace](index.md) → [SGM](sc.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Copper** — EN term. **Медь** — RU analogue.</small>

<mark>TBD</mark>


|**Characteristic**|**[Value](si.md) (incl. [comparison](matc.md))**|
|:--|:--|
|**Space sector**|・The main applications for copper are in electrical & electronic subsystems (wiring, terminals in soldered assemblies) & plating (e.g. electronics, thermal control & corrosion protection). Copper is also used as a metallizing coating & as an additive in other materials.<br> ・Vacuum presents no special problem for copper‑based materials, although copper‑zinc alloys are generally plated. All metals in contact under vacuum conditions or in inert gas have a tendency to cold weld. This phenomenon is enhanced by mechanical rubbing or any other process which removes or disrupts surface oxide layers.<br> ・[Radiation](ion_rad.md) at the level existing in space does not modify the properties of copper alloys. Temperature problems are similar to those encountered in technologies other than space, but are complicated by the difficulty of achieving good thermal contact in vacuum & the absence of any convective cooling.<br> ・Copper shall not be used on the external surfaces of spacecraft in LEO. Atomic oxygen in LEO attacks copper.  Copper shows discoloration under atomic oxygen attack.|
|**General Information**|Copper & copper‑based alloys are established materials in electrical, electronic & also in more general engineering applications (e.g. bearing assemblies). Not all are acceptable for space, so discussion is limited to those alloys which were evaluated & to specific comments relating to their use in space|
|**Main Categories**|Copper materials are generally grouped as follows:<br> ・Commercially pure grades, of which there are many different "named" varieties that indicate the manufacturing method & the level of control of impurities, including oxygen.<br> ・Alloys in which the alloying additions affect the metallurgical microstructure & consequently their characteristics (mechanical, electrical & thermal properties, environmental resistance). The main alloying addition generally provides the named classifications:brass: copper‑zinc alloys, often containing other alloying elements, such as lead which acts as a 「lubricant」 for machining operations — so‑called 「free‑machining」; ‑bronze: copper‑[tin](tin.md) alloys, often containing other alloying elements.<br> Electronic assemblies use wires made of high‑purity copper or copper alloy & terminals of copper alloy. Beryllium‑copper (also known as copper‑beryllium) is a copper alloy with small additions of Be. Alloys form two groups: one with less than Be 1 % content & the other with approximately Be 2 %. Cobalt & [nickel](nickel.md) additions (present for heat treatment purposes) tend to vary inversely with Be content. These alloys, depending on their condition, offer combined mechanical performance & electrical conductivity for electrical & electronic applications (e.g. spring contacts); for low temperature applications; for high‑strength corrosion resistant components & in safety applications in hazardous environments (no sparks produced when impacted).<br> Copper is also used as a matrix phase in some reinforced metals.|
|**Processing & Assembly**|In electronic assembly operations, copper wires are soldered to terminals (either manually or automatically). The correct selection & use of process materials (e.g. approved solders & fluxes for space hardware & solvents) is a controlling factor in making reliable soldered connections. Beryllium‑copper alloys are heat treated to optimize mechanical performance. Fabrication processes (e.g. forming, machining & joining) are generally performed in a softened condition & the material subsequently solution treated & aged.|
|**Precautions**|Heating brass in an oxidizing atmosphere or under corrosive conditions can cause dezincification of the alloy (loss of zinc from the exposed surface layer). This alters the surface properties & reduces fatigue & bending resistance.<br> Cold worked brass alloys are sensitive to stress corrosion cracking. Annealing heat treatments are used to remove the cold work, but care shall be taken to avoid any dezincification.<br> Natural atmospheres containing the pollutants sulphur dioxide, oxides of nitrogen & ammonia are reported to cause stress corrosion cracking of some copper alloys. Chlorides present in marine atmospheres can cause stress corrosion problems, but to a lesser extent than the above pollutants, indicating that industrial areas are probablymore aggressive to copper‑based alloys than marine sites.<br> Many copper alloys containing over 20 % zinc are susceptible to SCC even in the presence of alloying additions that normally impart resistance to stress corrosion. In electronic assemblies, terminals fabricated from bronze should be used. Brass terminals need a barrier layer (plating), to prevent diffusion & surface oxidation of zinc, prior to applying a tin‑lead coating. Some constituents of potting compounds & sealants (catalysts) are corrosive to copper, & other metals.|
|**Hazardous & Precluded**|The toxicity of copper‑beryllium alloys (less than Be 4 %) is not known. Brass (Cu‑Zn alloys) used in electronic connections shall be plated with a barrier layer to prevent zinc diffusion to the surface|



<p style="page-break-after:always"> </p>

## Docs/Links
|**Sections & pages**|
|:--|
|**【[](.md)】**<br> <mark>NOCAT</mark>|

1. Docs: …
1. <https://www.spacematdb.com/spacemat/datasearch.php?name=02:%20Copper%20and%20its%20Alloys>


## The End

end of file
