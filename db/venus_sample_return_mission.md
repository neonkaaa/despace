# Venus Sample Return Mission
> 2019.10.10 [🚀](../../index/index.md) [despace](index.md) → [Venus](venus.md), **[Project](project.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Venus Sample Return Mission (VSRM)** — EN term. **Венерианская миссия по возврату грунта (ВМВГ)** — literal RU translation.</small>

**Venus Sample Return Mission** — proposed EU/US mission to return venusian surface samples to Earth.

1. 1st report dated 1986.10.13 by JPL/CNES.
1. Revisited by ESA in 2019.



|**Type**|**[Param.](si.md)**|
|:--|:--|
|**【Mission】**|~ ~ ~ ~ ~ |
|Cost|… or … ㎏ of [gold](sc_price.md) in … prices|
|[CML](trl.md) / [TRL](trl.md)|CML: <mark>TBD</mark>, TRL: <mark>TBD</mark>|
|Development|… ‑ …|
|Duration|…|
|Launch|…, …, …|
|Operator|…|
|Programme|…|
|Similar to|・Proposed: …<br> ・Current: …<br> ・Past: …|
|Target|…|
|[Type](sc.md)|…|
|**【Spacecraft】**|~ ~ ~ ~ ~ |
|Composition|…|
|Contractor|…|
|Manufacturer|…|
| |**`…`**|
|Comms|…|
|[ID](spaceid.md)|NSSDC ID (COSPAR ID): <mark>TBD</mark>, SCN: <mark>TBD</mark>|
|Mass|… ([…satellite](sc.md))|
|Orbit / Site|…|
|Power|…|
|Payload|…|

Achieved targets & investigations:

1. **T** — technical; **C** — contact research; **D** — distant research; **F** — fly‑by; **H** — manned; **S** — soil sample return; **X** — technology demonstration
1. **Sections of measurement and observation:**
   1. Atmospheric/climate — **Ac** composition, **Ai** imaging, **Am** mapping, **Ap** pressure, **As** samples, **At** temperature, **Aw** wind speed/direction.
   1. General — **Gi** planet’s interactions with outer space.
   1. Soil/surface — **Sc** composition, **Si** imaging, **Sm** mapping, **Ss** samples.

<small>

|**EVN‑XXX**|**T**|**EN**|**Section of m&o**|**D**|**C**|**F**|**H**|**S**|
|:--|:--|:--|:--|:--|:--|:--|:--|:--|
|…| |…| | | | | | |

</small>



## Mission
<mark>TBD</mark>

A plan to pick up sample of the surface of Venus and return it to Earth is under study for NASA by scientists and engineers at Jet Propulsion Laboratory.

The mission would begin in 1999 and require two years for sample to be returned to Earth orbit.

The basic mission idea is to use small samplers parachuted to the surface of the planet where they would take up bit of soil or rock. The material would then be carried aloft by balloons which would be retrieved by small robotic airplanes and transferred to an ascent rocket.

(The remotely piloted airplane could be of the 「Aquila」 type, developed by Lockheed and earlier demonstrated by the military.)

The ascent rocket would take the samples to Venus orbit where they would be transported by return vehicle to Earth orbit.

The objective of the mission would be to return to Earth at least 1 ㎏ (2.2 pounds) sample of the Venus surface for study.

The mission is considered to be similar in size and challenge to Mars or comet sample return mission, also currently under study by NASA, the team members said.

The nearly 900 ℉ temperature of the Venusian surface and atmospheric pressure 90 times that of Earth added to the team’s problems in designing the mission.

After considering several approaches, the team settled on mission that would use small balloons, dropped by parachute. The balloons would self‑inflate and rise to high stable altitude where they could be captured by robotic aircraft and transferred to rocket carried by large hot 「air」 balloon. The 「air」 in this case means the Venus ambient atmosphere.

Small balloons have already been flown in the Venus atmosphere during the Soviet Vega missions.

The strategy, the team members said, is to make the systems which must operate in the hostile surface environment as simple as possible.

The concept trades simple systems on the surface for complex systems at high altitude where the temperature and pressure are more Earth‑like.

In phase one of the mission, the combination of vehicles that make up the Venus Sample Return (VSR) would be assembled at the space station or low Earth orbit. Then the 20 000 ㎏ (44 000 pounds) spacecraft would be fired into 140‑day trajectory to Venus.

At Venus, the spacecraft would go into 300 kilometer (186 miles) circular orbit around the equator of the planet.

The Venus Sample Return spacecraft would include four elements: They are the Venus Orbiting Vehicle (VOV); two Venus Descent Modules (VDM1 and VDM2) which, respectively, carry the sampler packages, and the hot air balloon, airplanes and ascent rocket; and the Earth Return Vehicle (ERV).

In phase two, the 1st descent module, similar to the entry probes that were earlier used in the Venus atmosphere, would be released from the orbiting vehicle. Close to the surface it would drop several sampler packages that descend on small parachutes.

The packages would have about an hour to acquire soil sample. Several different mechanisms for picking up sample are being evaluated. By the end of an hour, liquid ammonia in the balloons would vaporize and expand in the Venusian heat and the balloon would begin to rise carrying the samples.

Each balloon would carry either transponder or radio frequency reflector to aid in its detection. The balloons would rise to about 50 − 60 ㎞ (31 − 37 miles) altitude over four hours. Winds would carry them about 500 ㎞ (310 miles) from their starting point.

In the third phase, the second descent module would be released and perform deorbit burn. The vehicles that make up the module — the hot air balloon, the ascent rocket and the Venus airplanes — would be packaged in an aeroshell that allows the module to maneuver close to the sampler balloons.

Once at the target, the descent module deploys parachute that allows the aeroshell to be jettisoned and assists in the deployment of the hot air balloon. The hot air balloon was chosen because of its weight‑carrying capability. It gets its gas from the atmosphere and it is kept aloft by gas expansion caused by heating from sunlight.

Attached to the hot air balloon would be the ascent rocket and two airplanes — one for backup. An airplane would be deployed and search, using radar, to locate the sampler balloons.

In the fourth phase, after the airplane locates and retrieves the sampler balloons, it would return to the hot air balloon and be captured itself by net and the samples transferred to the ascent rocket.

The airplanes would have about 50 hours, the estimated lifetime of the hot air balloon, to find and retrieve the samples.

In the final two phases, the ascent vehicle would carry the samples back to the Venus Orbiting Vehicle where they would be transferred to the Earth Return Vehicle.

At Earth orbit, an Earth Orbiting Canister (EOC) containing the samples would be ejected into 280 ㎞ (173.6 miles) by 40 000 ㎞ (24 800 miles) orbit around Earth. The canister would be retrieved by an orbit transfer vehicle.

The Aquila‑type remote aircraft has the capability to be launched from truck, fly pre‑programmed course and autonomously maneuver back to the launcher for recovery into net. The remotely‑piloted airplane engine, which burns hydrazine, was developed in 1978. Other technologies, including navigation, rendezvous, docking and snaring tethered balloon, were more recently developed.

The VSR team noted that the Venus Sample Return mission concept would be potentially good candidate for international cooperation, and could become part of sample return program that includes Mars, comets and asteroids since they have many technology requirements in common.



## Science goals & payload
<mark>TBD</mark>



## Spacecraft
<mark>TBD</mark>



## Community, library, links

**PEOPLE:**

1. **Concept of 1986:**
   1. [Blamont, Jacques](person.md) — CNES
   1. Jones, Ross M. — JPL
   1. [Nock, Kerry T.](person.md) — JPL
1. **Review of 2003:**
   1. Gershman, Bob
   1. Nilsen, Erik — CalTech
   1. [Peterson, Craig E.](person.md) — Independent Researcher
   1. Sweetser, Theodore H. — NASA
1. **Review of 2019:**
   1. Koppel, Christophe — AAAF (Association Aéronautique et Astronautique de France), EOS (stands for: Exploration and Observation of Space)
   1. Mairet, Loup — AAAF (Association Aéronautique et Astronautique de France), EOS (stands for: Exploration and Observation of Space)
   1. Mairet, Philippe — AAAF (Association Aéronautique et Astronautique de France), EOS (stands for: Exploration and Observation of Space)
   1. Valentian, Dominique — AAAF (Association Aéronautique et Astronautique de France), EOS (stands for: Exploration and Observation of Space)

**COMMUNITY:**

<mark>TBD</mark>



## Docs/Links
|**Sections & pages**|
|:--|
|**【[](.md)】**<br> <mark>NOCAT</mark>|

1. Docs:
   1. [Venus sample return mission revisited ❐](f/project/v/venus_sample_return_mission/valentiand_venus_sample_return_mission_revisited_r2.pdf)
1. <https://www.jpl.nasa.gov/news/news.php?feature=5902>
1. <https://www.cosmos.esa.int/documents/1866264/3219248/ValentianD_Venus+sample+return+mission+revisited_r2.pdf>
1. <https://forum.kerbalspaceprogram.com/index.php?/topic/113010-venus-surface-sample-return-mission>
1. <https://forum.nasaspaceflight.com/index.php?topic=29296.0>
1. <https://www.researchgate.net/publication/245139929_Venus_sample_return_missions-a_range_of_science_a_range_of_costs>


## The End

end of file
