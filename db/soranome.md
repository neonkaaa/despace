# sorano me
> 2022.06.17 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/s/soranome_logo1t.webp)](f/c/s/soranome_logo1.webp)|<contact@soranome.com>, <mark>nophone</mark>, Fax …;<br> *3-3-13 Nishi-Shinjuku, Shinjuku-ku, Tokyo, Japan, 〒160-0023*<br> <https://soranome.com> ~~ [LI ⎆](https://www.linkedin.com/company/soranomeofficial) ~~ [X ⎆](https://twitter.com/sorano_me)|
|:--|:--|
|**Business**|Space media, business consulting, HR|
|**Mission**|Enrich with space business|
|**Vision**|The universe is a challenging & unknown field. We at sorano me believe that the perspective from space updates the lives of the Earth. By lowering the threshold of the space field, fostering talented human resources, & rotating the challenge cycle at high speed, we will nurture the buds of numerous space businesses that update our lives.|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO — Ayano Kido|

**sorano me** is a Japanese space media & consulting company. Founded in 2019.

Business content:

- Space-related content creation & marketing
- Space business related consulting
- Media development & operation
- Human resources development / consulting

Currently, we are developing two main businesses. It is a companion-type support business that accompanies companies and individuals who challenge the space business and creates services together while utilizing their specialized knowledge, and a human resources infrastructure development business that builds the foundation of human resources that support it.

<p style="page-break-after:always"> </p>

## The End

end of file
