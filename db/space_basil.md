# Space Basil Inc.
> 2021.12.02 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/s/space_basil_logo1t.webp)](f/c/s/space_basil_logo1.webp)|<info@spacebasil.co.jp>, +81(3)6228-1352, Fax +81(3)6733-7870 ;<br> *4-2-15 Ginza Chuo-ku, Tokyo, 104-0061, Japan*<br> <https://spacebasil.co.jp>|
|:--|:--|
|**Business**|Outer space advertising & entertainment|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO — Katsuji Amazaki|

**Space Basil Inc.** is a Japanese company aimed for outer space advertising & entertainment. Founded 2019.10.

Business overview:

1. **Outer Space Advertisement Enterprise.** In Outer Space Advertisement enterprise, we will work on branding & marketing of client companies by advertising, PR, sales promotion using the outer space & most advanced micro satellite. Through the business, we are certain to create a topic that is a 「world’s 1st.」
1. **Outer Space Entertainment Enterprise.** In outer space entertainment enterprise, with collaboration with various entertainment companies such as TV stations, movie companies, record companies & game companies, we will provide users with a completely new experience such as images, videos, music & games utilizing space.

**Message from CEO.**

> Space Basil was established in October, 2019 with the aim to create outer space advertising business & outer space entertainment business.<br> Recently, space business has begun to attract attention all over the world, but business utilizing artificial satellites takes the most part. In reality there are still only few businesses that utilize the outer space itself.<br> With lead of technical adviser Shinichi Nakasuka, a professor at Tokyo Universityin the Department of Aeronautics & Astronautics who is the world’s greatest authority on micro‑satellite, we take the initiative in the new undeveloped field.<br> Advertising & entertainment have the power to excite & enthuse the general public.<br> Using this dreamful power, we will evolve outer space into something familiar to humanity.<br> We ask for your continued support & cooperation in our activities.

<p style="page-break-after:always"> </p>

## The End

end of file
