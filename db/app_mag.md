# Видимая звёздная величина
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → **[ЗД](sensor.md)**, [Space](index.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Видимая звёздная величина** — русскоязычный термин. **Apparent magnitude** — англоязычный эквивалент.</small>

**Видимая звёздная величина (m)** — мера яркости небесного тела (точнее, освещённости, создаваемой этим телом) с точки зрения земного наблюдателя. Обычно используют величину, скорректированную до значения, которое она имела бы при отсутствии атмосферы. Чем ярче объект, тем меньше его звёздная величина.

Уточнение 「видимая」 указывает только на то, что эта звёздная величина наблюдается с [Земли](earth.md); это уточнение нужно, чтобы отличить её от абсолютной. Оно не указывает на видимый диапазон: видимыми называют и величины, измеренные в инфракрасном или каком‑либо другом диапазоне. Величина, измеренная в видимом диапазоне, называется визуальной.

В [видимой части спектра](comms.md) самая яркая звезда на ночном небе — Сириус, а в инфракрасном J‑диапазоне — Бетельгейзе.



## История
|**<small>Видны<br> невооружённым<br> глазом</small>**|**<small>Видимая<br> величина</small>**|**<small>Яркость<br> относительно<br> Веги</small>**|**<small>Число звёзд<br> ярче этой<br> видимой величины</small>**|
|:--|:--|:--|:--|
|Да|−1,0|250 %|1|
|Да|0,0|100 %|4|
|Да|1,0|40 %|15|
|Да|2,0|16 %|48|
|Да|3,0|6,3 %|171|
|Да|4,0|2,5 %|513|
|Да|5,0|1,0 %|1 602|
|Да|6,0|0,40 %|4 800|
|Да|6,5|0,25 %|9 096|
|**Нет**|7,0|0,16 %|14 000|
|**Нет**|8,0|0,063 %|42 000|
|**Нет**|9,0|0,025 %|121 000|
|**Нет**|10,0|0,010 %|340 000|

Современная шкала звёздных величин берёт начало в Древней Греции. Её предложил во II веке до н.э. Гиппарх, разделив звезды, видимые невооруженным глазом, по шести величинам. Самые яркие из них он назвал звёздами первой величины (m = 1), а самые слабые — звёздами шестой величины (m = 6). Современная астрономия не ограничивается шестью величинами или только видимым светом. Очень яркие объекты имеют отрицательную величину.



## Вычисление
Видимая звёздная величина объектов 1 и 2 определяется как

`m₁ − m₂ = −2.5 · lg(L₁ / L₂)`  
где m — звёздные величины объектов, L — освещённости от этих объектов.

Таким образом, разница в 5 звёздных величин соответствует отношению освещённостей в 100 раз, а разница в одну звёздную величину — в 100<sup>1/5</sup> ≈ 2.512 раза.

**Примеры:**

Видимая звёздная величина полной Луны равна −12.7; яркость Солнца составляет −26.7.

Разница звёздных величин Луны (m₁) и Солнца (m₂):  
`m₁ − m₂ = (−12.7) − (−26.7) = 14.0`

Отношение освещённостей от Солнца и Луны:  
`L₁ / L₂ = 2.512^(m₁ − m₂) = 2.512^14.0 = 400000`

Таким образом, Солнце примерно в 400 000 раз ярче полной Луны.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**`Звёздный датчик (ЗД):`**<br> [Видимая звёздная величина](app_mag.md)|
|**【[Space](index.md)】**<br> [Apparent magnitude](app_mag.md) ~~ [Astro.object](aob.md) ~~ [Blue Marble](earth.md) ~~ [Cosmic rays](ion_rad.md) ~~ [Ecliptic](ecliptic.md) ~~ [Escape velocity](esc_vel.md) ~~ [Health](health.md) ~~ [Hill sphere](hill_sphere.md) ~~ [Information](info.md) ~~ [Lagrangian points](l_points.md) ~~ [Near space](near_space.md) ~~ [Pale Blue Dot](earth.md) ~~ [Parallax](parallax.md) ~~ [Point Nemo](earth.md) ~~ [Silver Snoopy award](silver_snoopy_award.md) ~~ [Solar constant](solar_const.md) ~~ [Terminator](terminator.md) ~~ [Time](time.md) ~~ [Wormhole](wormhole.md) ┊ ··•·· **Solar system:** [Ariel](ariel.md) ~~ [Callisto](callisto.md) ~~ [Ceres](ceres.md) ~~ [Deimos](deimos.md) ~~ [Earth](earth.md) ~~ [Enceladus](enceladus.md) ~~ [Eris](eris.md) ~~ [Europa](europa.md) ~~ [Ganymede](ganymede.md) ~~ [Haumea](haumea.md) ~~ [Iapetus](iapetus.md) ~~ [Io](io.md) ~~ [Jupiter](jupiter.md) ~~ [Makemake](makemake.md) ~~ [Mars](mars.md) ~~ [Mercury](mercury.md) ~~ [Moon](moon.md) ~~ [Neptune](neptune.md) ~~ [Nereid](nereid.md) ~~ [Nibiru](nibiru.md) ~~ [Oberon](oberon.md) ~~ [Phobos](phobos.md) ~~ [Pluto](pluto.md) ~~ [Proteus](proteus.md) ~~ [Rhea](rhea.md) ~~ [Saturn](saturn.md) ~~ [Sedna](sedna.md) ~~ [Solar day](solar_day.md) ~~ [Sun](sun.md) ~~ [Titan](titan.md) ~~ [Titania](titania.md) ~~ [Triton](triton.md) ~~ [Umbriel](umbriel.md) ~~ [Uranus](uranus.md) ~~ [Venus](venus.md)|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Apparent_magnitude>


## The End

end of file
