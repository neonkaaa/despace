# Hydrazine
> 2021.10.25 [🚀](../../index/index.md) [despace](index.md) → [](.md) <mark>NOCAT</mark>  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Hydrazine** — EN term. **Гидразин** — RU analogue.</small>

**Hydrazine** is an inorganic compound with the chemical formula N₂H₄. It is a simple pnictogen hydride, and is a colourless flammable liquid with an ammonia-like odour.

|**Characteristics**|**Description**|
|:--|:--|
|Систематическое наименование|Гидразин, диамид|
|[Токсичность](nfpa_704.md)|чрезвычайно токсичен, требуется нейтрализация<br> ![](f/fuel/n2h4_nfpa704.webp)|
|Хим. формула|N₂H₄|
|**【Физические свойства】**|—|
|Кинематическая вязкость| |
|Молярная масса|32.05 г/моль|
|Плотность|1 010 ㎏/m³|
|Состояние|жидкость|
|Энергия ионизации|8.93 ± 0.01 эВ|
|**【Термические свойства】**|—|
|Давление пара|10 ± 1 мм.рт.ст.|
|Т. кип.|114 ℃|
|Т. плав.|2 ℃|
|Энтальпия образования| |
|**【Топливо】**|—|
|Fuel — combustion products|H₂, N₂|
|Temper. — comb. chamber, К (℃)|653 ‑ 1 273 (380 ‑ 1 000)|



## Docs/Links
|**Sections & pages**|
|:--|
|**【[](.md)】**<br> <mark>NOCAT</mark>|

1. Docs: …
1. <…>


## The End

end of file
