# Electric battery
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [EB](eb.md), [SPS](sps.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Chemical source of electricity (CSE), Electric battery (EB), Primary cell (PC)** — EN term. **Химический источник тока (ХИТ)** — RU analogue.</small>

**Chemical source of electricity (CSE), Electric battery (EB)** — a source of electromotive force (EMF), in which the energy of chemical reactions taking place in it is directly converted into electrical energy. Divided into:

1. **galvanic cells** (primary EB), which, due to the irreversibility of the reactions occurring in them, cannot be recharged.
1. **electric batteries** (secondary EB), they are **rechargeable batteries (RB)** — rechargeable galvanic cells that can be recharged using an external current source (charger);
1. **fuel cells** (electrochemical generators) — devices similar to a galvanic cell, but differing from it in that substances for an electrochemical reaction are supplied to it from the outside, & reaction products are removed from it, which allows it to function continuously:
   1. direct methanol fuel cell;
   1. solid oxide fuel cell;
   1. alkaline fuel cell.

The division of cells into galvanic & accumulators is somewhat arbitrary, since some galvanic cells, for example alkaline batteries, can be recharged, but the efficiency of this process is extremely low.

| |**[Company](contact.md)**|**Actual (capacity, A·h)**|
|:--|:--|:--|
|**EU**|[Saft](saft.md)|[8S8P](eb_lst.md) (30)|
|•|• • • • • • • • •|~ ~ ~ ~ ~ |
|**JP**|[GS Yuasa](gs_yuasa.md)| |
| |[Mitsubishi Electric](mitsubishi.md)| |
|•|• • • • • • • • •|~ ~ ~ ~ ~ |
|**RU**| [VNIIEM](vniiem.md) | |
| | [Orion‑Hit](orion_hit.md) | |
| | [PAO Saturn](pao_saturn.md) | [8LI‑70](eb_lst.md) (70) ~~ [12LI‑120](eb_lst.md) (120) |

【**Table.** Manufacturers】

| | |
|:--|:--|
|**AE**|…|
|**AU**|…|
|**CA**|・[Canadensys](canadensys.md)|
|**CN**|…|
|**EU**|…|
|**IL**|…|
|**IN**|…|
|**JP**|・[GS Yuasa](gs_yuasa.md) — EB cells<br> ・[Mitsubishi Electric](mitsubishi.md) — Assembly|
|**KR**|…|
|**RU**|…|
|**SA**|…|
|**SG**|…|
|**US**|…|
|**VN**|…|



## Notes

|**Type of sealed battery**|**Vol-<br>tage,<br> V**|**Specific<br> energy <br> (mass), <br> W·h / ㎏**|**Specific<br> energy <br> (volume), <br> W·h / dm³**|**Self-<br> dischar-<br>ge /day,<br>%**|**[Output]( charge_eff.md), <br>%**|**Service<br> life,<br> years / <br> cycles**|**Temp., <br> ℃**|
|:--|:--|:--|:--|:--|:--|:--|:--|:--|
|**Lithium-Iron-Phosphate** (LiFePO₄, LFP) | 3.3 | 90 ‑ 130 | 320 ‑ 400 | 0.16 | 90 | 15 / 2 000 <br> 15 / 7 000 | −30 ‑ +55 |
|**Lithium-Ion**| 3.6 | 130 | 260 | 15 (28) | 98 | 5 / 500 |−… ‑ +… |
|**Lithium-Ion** (SIS, silicon) | | | | | |… / … | −… ‑ +300 |
|**Lithium-Cobalt** (LiCoO₂, LCO) | 3.6 | 200 | 560 | | |… / 1 000 | −… ‑ +… |
|**Lithium-Manganese** (LiMn₂O₄, LMO) | 3.7 | 150 | 420 | | 90 |… / 700 | −… ‑ +… |
|**Lithium-Metallic**| | | | | |… / … | −… ‑ +… |
|**Lithium-Nickel-Cobalt-Alum.-Oxide** (LiNiCoAlO₂, NCA) | 3.6 | 260 | 600 | | |… / 500 | −… ‑ +… |
|**Lithium-Nickel-Manganese-Cobalt** (LiNiMnCoO₂, NMC) | 3.7 | 220 | | | |… / 2 000 | −… ‑ +… |
|**Lithium-Sulfur** (LiS) | 2.1 | 500 | 350 | | |… / 1400 | −… ‑ +… |
|**Lithium-Thionyl-Chloride** (Li ‑ SOCl₂) | 3.6 | 37 | | 0.002 7 | |… /… | −40 ‑ +145 |
|**Lithium-Titanate** (Li₄Ti₅O₁₂, SCiB / LTO) | 2.4 | 60 ‑ 110 | 177 | | 85 ‑ 90 | 10 / 6 000 <br> 20 / 20 000 | −… ‑ +… |
|•••|•••|•••|•••|•••|•••|•••|•••|
|**Nickel-Hydrogen** (NiH₂) | 1.25 | 50 ‑ 55 | 100 ‑ 120 | 40 (3) | 90 ‑ 92 | 7 / 1 000 | −… ‑ +… |
|**Nickel-Cadmium** (NiCd) | 1.2 | 40 ‑ 45 | 80 ‑ 100 | 20 (28) | 72 | 5 / 500 | −… ‑ +… |
|**Nickel-Metal Hydride** (NiMH) | 1.25 | 60 ‑ 75 | 180 ‑ 200 | 20 (28) | 96 ‑ 98 | 5 / 500 | −… ‑ +… |
|•••|•••|•••|•••|•••|•••|•••|•••|
|**Sulfur-Magnesium**| | 1 720 | | | |… / 110 | −… ‑ +… |

Comparison of rechargeable & disposable batteries

| |**disposable (70 A·h)**|**rechargeable 8LI‑70 (70 A·h)**|
|:--|:--|:--|
|**Weight, ㎏**| 15.5 | 18.8 |
|**Dimensions, ㎜**| 328 × 342 × 150 | 268 × 232 × 270 |



### Converting A·h to W·h
Often battery manufacturers indicate in the technical specifications only the stored charge in mA·h (mA·h), others — only the stored energy in W·h. Both characteristics can be called 「capacity」 (not to be confused with electrical capacity as a measure of a conductor's ability to store a charge, measured in farads). In general, it is not easy to calculate the stored energy from the stored charge: it is required to integrate the instantaneous power delivered by the battery during the entire discharge time. If greater accuracy is not needed, instead of integration, you can use the average values ​​of voltage & current consumption & use the formula following from the fact that  
`1W = 1V × 1A` & `1W·h = 1V × 1A·h`

That is, the stored energy (W·h) is approximately equal to the product of the stored charge (V·A·h) by the average voltage (in Volts):  
`E = q × U`

**Example**  
The technical specification of the device indicates that the 「capacity」 (stored charge) of the battery is 56 A·h, the operating voltage is 15 V. Then the 「capacity」 (stored energy) is: 56 × 15 = 840 W·h (≈3 MJ)  
When the batteries are connected in series, the 「capacity」 remains the same, when connected in parallel, it is added.  
3.3v 1 000 mA·h + 3.3v 1 000 mA·h = 6.6v 1 000 mA·h — serial connection.  
3.3v 1 000 mA·h + 3.3v 1 000 mA·h = 3.3v 2 000 mA·h — parallel connection.



### Calculating battery capacity
Produced according to the formula:  
**Battery capacity (A·h) = load power (kW) × time (h) × 100**



### Some types of EB

**Galvanic cell** — a chemical source of electrical current named after Luigi Galvani. The principle of operation is based on the interaction of two metals through an electrolyte, leading to the emergence of an electric current in a closed circuit.

**Electric accumulator**, also **accumulator battery (AB)** is a reusable chemical current source (that is, unlike a galvanic cell, chemical reactions directly converted into electrical energy are reversible many times). Electric accumulators are used for energy storage & autonomous power supply of various devices.

|**Type of galvanic cell**|**Cathode**|**Electrolyte**|**Anode**|**Voltage, V**|
|:--|:--|:--|:--|:--|
| Manganese-Magnesium element | MnO₂ | MgBr₂ | Mg | 2.00 |
| Manganese-Tin element | MnO₂ | KOH | Sn | 1.65 |
| Manganese-Zinc element | MnO₂ | KOH | Zn | 1.56 |
| Oxide-Mercury-Tin element | HgO₂ | KOH | Sn | 1.30 |
| Mercury-Cadmium element | HgO₂ | KOH | Cd | 1.92 |
| Mercury-Zinc element | HgO | KOH | Zn | 1.36 |
| Lead-Cadmium cell | PbO₂ | H₂SO₄ | Cd | 2.42 |
| Lead-Chlorine element | PbO₂ | HClO₄ | Pb | 1.92 |
| Lead-Cinc cell | PbO₂ | H₂SO₄ | Zn | 2.55 |
| Chromium-Zinc element | K₂Cr₂O₇ | H₂SO₄ | Zn | 1.8 ‑ 1.9 |

List of galvanic cells & electric accumulators.

|**Galvanic cell**|**Electric accumulator**|
|:--|:--|
| ・Bismuth-Magnesium element<br> ・Bromine-Silver element<br> ・Calcium chromate element<br> ・Chloride-Copper-Magnesium element<br> ・Chlorine-Silver element<br> ・Copper oxide galvanic cell<br> ・Dioxysulfate-Mercury element<br> ・Iodine-Silver element<br> ・Lead chloride-Magnesium element<br> ・Lead-Fluoric element<br> ・Lithium bismuth cell<br> ・Lithium-Chrome silver cell<br> ・Lithium-Copper oxide cell<br> ・Lithium-dioxide-Sulfur cell<br> ・Lithium-Fluoride cell<br> ・Lithium Iodine Cell<br> ・Lithium-Iodine-lead cell<br> ・Lithium-Thionyl chloride cell<br> ・Lithium Vanadium Oxide Cell<br> ・Magnesium-M‑DNB element<br> ・Magnesium perchlorate element<br> ・Magnesium vanadium element<br> ・Mercury-Bismuth-Indium element<br> ・Silver chloride-Magnesium element<br> ・Sulfur-Magnesium element<br> ・Zinc Iodate element<br> ・Zinc-Silver chloride element | ・Iron-Air battery<br> ・Iron-Nickel battery<br> ・Lanthanum-Fluoride battery<br> ・Lead-Acid battery<br> ・Lead-Hydrogen battery<br> ・Lead-Tin battery<br> ・Li-Ion battery<br> ・Lithium-chlorine battery<br> ・Lithium-fluorine battery<br> ・Lithium-Iron-Sulfide battery<br> ・Lithium-polymer battery<br> ・Lithium-Sulfur battery<br> ・Manganese-Tin element<br> ・Nickel-Cadmium battery<br> ・Nickel-Metal-hydride battery<br> ・Nickel-Zinc battery<br> ・Silver-Cadmium battery<br> ・Silver-Zinc battery<br> ・Sodium-Nickel-chloride battery<br> ・Sodium Sulfur battery<br> ・Zinc-Air accumulator<br> ・Zinc-Bromine battery<br> ・Zinc-Chlorine accumulator |



## Docs/Links
|**Sections & pages**|
|:--|
|**【[Spacecraft power system (SPS)](sps.md)】**<br> [Charge eff.](charge_eff.md) ~~ [EAS](eas.md) ~~ [EB](eb.md) ~~ [EMI, RFI](emi.md) ~~ [NR](nr.md) ~~ [Rotor](iu.md) ~~ [RTG](rtg.md) ~~ [Solar cell](sp.md) ~~ [SP](sp.md) ~~ [SPB/USPB](suspb.md) ~~ [Voltage](sps.md) ~~ [WT](wt.md)<br>~ ~ ~ ~ ~<br> **RF/CIF:** [BAK‑01](eas_lst.md) ~~ [KAS‑LOA](eas_lst.md)|
|**【[Chemical source of electricity (CSE), Electric battery (EB)](eb.md)】**<br> [Charge efficiency](charge_eff.md) <br>~ ~ ~ ~ ~<br> **EU:** [8S8P](eb_lst.md) (30)  ▮  **RU:** [8LI-70](eb_lst.md) (70) ~~ [12LI-120](eb_lst.md) (120)|

1. Docs: …
1. 2019.08.02 Есть ли альтернатива литий‑ионному аккумулятору (<https://habr.com/ru/company/toshibarus/blog/462185>) — [archived ❐](f/sps/20190802_1_01.pdf) 2019.08.04
1. <https://en.wikipedia.org/wiki/Rechargeable_battery>
1. <http://niai.ru/catalog.php?:id=6>
1. <http://at-systems.ru/quest/new-quest/capacity-count-easy-y.shtml>
1. <https://en.wikipedia.org/wiki/Peukert’s_law>


## The End

end of file
