# КА к 9 планете (гипотетический)
> 2019.11.30 [🚀](../../index/index.md) [despace](index.md) → **[9‑я планета](planet_9.md)**, [Проекты](project.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**КА к 9 планете** — русскоязычный термин, не имеющий аналога в английском языке. **SC to the Ninth Planet** — дословный перевод с русского на английский.</small>

**КА к 9 планете** — гипотетический проект по созданию КА для подтверждения/опровержения существования [9‑й планеты](planet_9.md). Начат 2019.11.27.



## Описание и условия
На конец 2019 года наличие 9‑й планеты Солнечной системы заявлено на основе наблюдений траекторий движения объектов Солнечной системы и транснептуновых объектов, однако планета до сих пор не обнаружена в связи с отсутствием серьёзных целевых наблюдений, а также из‑за огромных расчётных масштабов (орбита 200 × 1200 а.е., период 15 000 земных лет, большие разбросы по значениям).

Исходные данные:

1. скорость Вояджер‑2 принимается в 62 100 ㎞/h (17.25 ㎞/s)
1. орбита 200 × 1200 а.е. (30 ‑ 180 млрд ㎞)
1. <mark>TBD</mark>



## CML 1
|**Параметр**|**[Value](si.md)**|
|:--|:--|
|[CML](trl.md)|**1** — Cocktail Napkin, Idea Generation, Overview & Advocacy (Записи на салфетках, Генерация идей)|
|[TRL](trl.md)| |

Салфетки:

1. КА типа Вояджера (2 или более) с БА для радиосканирования сектора неба. Первый — в сторону перицентра, второй — апоцентра. Перелёт на достаточное расстояние (пальцем в небо, 100 а.е. через 27 лет) и наблюдение неба. (МАБ)
1. КА типа Вояджера (2 или более) с БА для радиосканирования сектора неба. Оба — в сторону апоцентра, создают интерферометр. (МАБ)
1. Вылететь за орбиту Плутона, разогнавшись быстрее, чем Вояджер и Новые Горизонты (например, до 30 ‑ 170 ㎞/s). (ГАС)



## CML 2
|**Параметр**|**[Value](si.md)**|
|:--|:--|
|[CML](trl.md)|**2** — Initial Feasibility (Оценка осуществимости)|
|[TRL](trl.md)| |



## CML 3
|**Параметр**|**[Value](si.md)**|
|:--|:--|
|[CML](trl.md)|**3** — Trade Space (Определение границ/лимитов)|
|[TRL](trl.md)| |



## CML 4
|**Параметр**|**[Value](si.md)**|
|:--|:--|
|[CML](trl.md)|**4** — Point Design, Architecture selected within Trade Space (Выборочное проектирование)|
|[TRL](trl.md)| |



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[](.md)】**<br> <mark>NOCAT</mark>|

1. Docs: …
1. <…>


## The End

end of file
