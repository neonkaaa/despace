# Global Aerospace Corporation
> 2019.01.18 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/g/gac_logo1t.webp)](f/c/g/gac_logo1.webp)|<info@gaerospace.com>, +1(626)960-83-00, Fax +1(626)960-83-37;<br> *12981 Ramona Blvd, Suite E, Irwindale, CA 91706-3750, US.*<br> <http://www.gaerospace.com> ~~ [FB ⎆](https://www.facebook.com/pages/Global-Aerospace-Corporation/1649147128674779) ~~ [LI ⎆](https://www.linkedin.com/company/global-aerospace-corp) ~~ [X ⎆](https://twitter.com/gaerospacecorp)|
|:--|:--|
|**Business**|…|
|**Mission**|・We’re made up of creative engineers/scientists that have a passion for aerospace research.<br> ・We enjoy circumventing challenges that most people treat as insurmountable.<br> ・We produce honest, data‑driven research & engineering analysis for Government & commercial customers.|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Global Aerospace Corporation (GAC)** is a small, innovative aerospace research & development company with its main corporate office in Irwindale, CA (near the foothills northeast of Los Angeles). Global Aerospace Corporation does cutting edge aerospace R&D in the areas of space, undersea, lighter‑than‑air, re‑entry, defense, & power technologies; new concepts development; & software.

- **Core Competencies:**
   - Solutions for harsh/extreme environments w/ emphasis on buoyant & hypersonic systems
   - Modeling & simulation of complex systems
- **Areas of Expertise:**
   - Aeroelasticity
   - Aerothermodynamics
   - Buoyancy
   - Mechanical design & engineering
   - Orbital mechanics
   - Systems engineering & modeling



## The End

end of file
