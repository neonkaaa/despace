# JSGA
> 2022.04.15 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/j/jsga_logo1t.webp)](f/c/j/jsga_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *…, Tokyo, Japan*<br> <https://www.spaceguard.or.jp> ~~ [FB ⎆](https://www.facebook.com/spaceguard.japan) ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/Japan_Spaceguard_Association)|
|:--|:--|
|**Business**|Observations, protects Earth from space objects|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

The **Japan Spaceguard Association** (日本スペースガード協会, nihon supēsugādo kyōkai, abbreviated **JSGA**) is Japanese company aimed to protect the Earth’s environment from a disastrous near‑Earth object (NEO) collision by studying & observing the NEOs.

JSGA operates the Bisei Spaceguard Center, owned by the Japan Space Forum, to achieve their goal. It is located near Bisei town in the western Japan. In addition to the search for NEOs, this facility will be used to track debris in Earth's orbit. The Minor Planet Center credits JSGA with the discovery of the main‑belt asteroid (21309) 1996 XH5, made at Kiso Observatory 1996.12.06.

<p style="page-break-after:always"> </p>

## The End

end of file
