# Open Univ.
> 2019.08.09 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/o/open_univ_logo1t.webp)](f/c/o/open_univ_logo1.webp)|<mark>noemail</mark>, +44(300)303-02-66, Fax …;<br> *The Open University, PO Box 197, Milton Keynes, MK7 6BJ, UK*<br> <http://www.open.ac.uk> ~~ [FB ⎆](https://www.facebook.com/theopenuniversityinternational) ~~ [X ⎆](https://twitter.com/theouglobal) ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/Open_University)|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**The Open University (OU)** is a public research university, & the biggest university in the UK for undergraduate education. The majority of the OU’s undergraduate students are based in the United Kingdom & principally study off‑campus; many of its courses (both undergraduate & postgraduate) can also be studied anywhere in the world.



## The End

end of file
