# Eunoia Space
> 2022.07.19 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/e/eunoia_space_logo1t.webp)](f/c/e/eunoia_space_logo1.webp)|<info@eunoiaspace.com>, +81(46)732-88-40, Fax …;<br> *…, Kanagawa, Japan*<br> <https://eunoiaspace.com> ~~ [IG ⎆](https://www.instagram.com) ~~ [LI ⎆](https://www.linkedin.com/company/eunoia-space) ~~ [X ⎆](https://twitter.com/EunoiaJapan)|
|:--|:--|
|**Business**|JP & overseas space industry bridging|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO — Ruth Thomas|

**Eunoia Space** is a Japanese space business consulting company.

With combined experience working in both Japan’s space industry & facilitating cross‑cultural business projects, Eunoia Space is well placed to:

- Help Japan’s space industry identify & gain access to more varied & lucrative international business partners & sales prospects
- Help overseas space industry companies unlock & navigate/research/target/develop the Japan market

We also support space‑industry related educational endeavours (domestic & internationally) as part of our CSR efforts.


<p style="page-break-after:always"> </p>

## The End

end of file
