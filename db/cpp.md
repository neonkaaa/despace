# CPP
> 2019.08.09 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/c/cpp_logo1t.webp)](f/c/c/cpp_logo1.webp)|<mark>noemail</mark>, +1(909)869-76-59, Fax …;<br> *3801 West Temple Avenue,Pomona, CA 91768, USA*<br> <https://www.cpp.edu> ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/California_State_Polytechnic_University,_Pomona)|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**California State Polytechnic University, Pomona (Cal Poly Pomona, CPP, or Cal Poly)** is a public polytechnic university in Pomona, California in the Los Angeles metropolitan area. It is one of two polytechnics in the California State University system.

Cal Poly Pomona is among the best public universities in the West & is nationally ranked for helping students achieve economic success. As an inclusive polytechnic university, we cultivate success through experiential learning, discovery & innovation. Our graduates are ready to succeed in the professional world on Day 1. Faculty in all disciplines put theory to practice, providing students with opportunities to apply their knowledge in hands-on projects, research collaborations, & valuable internship & service-learning programs. Our history & geography are unlike any other university in the region. Nowhere else can students ride an Arabian horse, practice on a Steinway piano, bring a new product to market, & build a liquid‑fueled rocket



## The End

end of file
