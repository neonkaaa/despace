# Exotrail
> 2022.03.29 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/e/exotrail_logo1t.webp)](f/c/e/exotrail_logo1.webp)|<website@exotrail.com>, +33 630663332, Fax …;<br> *3 Rue Galvani, Massy, 91300, France*<br> <https://exotrail.com> ~~ [FB ⎆](https://www.facebook.com/exotrail) ~~ [LI ⎆](https://www.linkedin.com/company/exotrail) ~~ [X ⎆](https://twitter.com/exotrailspace)|
|:--|:--|
|**Business**|Electric propulsion, simulation/operation software, space tugs|
|**Mission**|To enhance small satellite capabilities by optimizing deployment strategies & improving performance|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO, Founder — David Henri|

**Exotrail SA** is a FR company aimed to provide space mobility solutions for on‑orbit agility. Founded in 2015.

Exotrail's product portfolio covers a variety of solutions, from the ExoMG™ range of propulsion systems for small satellites, to the ExoOPS™ mission simulation & operation software package, & the SpaceVan™ orbital transfer vehicles. Additionally, the company takes special care to provide sustainable solutions to mitigate space debris. Exotrail's solutions enable satellite operators to achieve the following:

- Altitude transfers: lower altitude, to gain data downlink capability & imaging resolution, & raise altitude, to gain coverage, revisit rate & lifespan.
- Inclination correction: correct inclination to maintain specific positions such as Sun Synchronous Orbits (SSO) after launch and/or throughout the whole mission.
- Single‑plane phasing: optimise satellite formations in a few days with phasing manoeuvres within a single plane.
- Multi‑plane phasing: change the local time of orbits in order to phase constellations globally.
- Station keeping: compensate atmospheric drag, extend lifetime for constellations, & develop new high downlink/high imaging resolution missions in Very Low Earth Orbit (VLEO).
- Deorbiting & collision avoidance: deorbit spacecraft at the end of mission to prevent further space pollution & protect spacecraft with collision avoidance capability.

<p style="page-break-after:always"> </p>

## The End

end of file
