# CONTEC
> 2021.11.08 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/c/contec_logo1t.webp)](f/c/c/contec_logo1.webp)|<info@contec.kr>, +82(42)863-45-23, Fax …;<br> *169-84 Gwahak-ro, Yuseong-gu, Daejeon, Republic of Korea, 34133*<br> <http://contec.kr> ~~ [LI ⎆](https://www.linkedin.com/company/contec-co-ltd-%EC%A3%BC-%EC%BB%A8%ED%85%8D)|
|:--|:--|
|**Business**|[GS Services](gs.md), Satellite image processing & application services|
|**Mission**|…|
|**Vision**|…|
|**Values**|**Value** — pursue new value for space industry. **Innovation** — promote the novel technology for innovative business. **Collaboration** — collaboration with our cooperation companies. **Social Responsibility** — reason why we have a business.|
|**[MGMT](mgmt.md)**|・CEO — Sunghee Lee|

**CONTEC** is offering Space Ground Station Services & Satellite Image Processing & Application Services as well as a whole ground integration solution. Established in January of 2015.

<p style="page-break-after:always"> </p>

## The End

end of file
