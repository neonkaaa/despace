# EPSC
> 2020.07.02 [🚀](../../index/index.md) [despace](index.md) → [EPSC](epsc.md), **[Events](event.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**European Planetary Science Congress (EPSC)** — EN term. **Когресс по европейской науке о планетах (EPSC)** — literal RU translation.</small>

The **European Planetary Science Congress (EPSC)** is the major European meeting on planetary science.

Originally known as the European Planetary Science Congress, the 1st EPSC was held in Berlin in 2006. EPSC is the main dissemination platform for the European planetary science community (hosted by the European Science Foundation (ESF) in Strasbourg, France) and is the annual meeting of the Europlanet Society.  EPSC meetings cover the entire scope of planetary sciences and have a distinctively interactive style, with an extensive mix of talks, workshops and posters, intended to provide a stimulating environment for the community to meet.

<small>

|**Year**|**Location**|**Attendees**|
|:--|:--|:--|
|2019|Geneva, Switzerland (Joint with DPS)|1731|
|2018|Berlin, Germany|1018|
|2017|Riga, Latvia|808|
|2016|Pasadena, California, USA (Joint with DPS)|1437|
|2015|Nantes, France|706|
|2014|Cascais, Portugal|591|
|2013|London, UK|960|
|2012|Madrid, Spain|705|
|2011|Nantes, France (Joint with DPS)|1532|
|2010|Rome, Italy|687|
|2009|Potsdam, Germany|550|
|2008|Muenster, Germany|461|
|2007|Potsdam, Germany|400+|
|2006|Berlin, Germany|554|

</small>



## Docs/Links
|**Sections & pages**|
|:--|
|**【[Events](event.md)】**<br> **Meetings:** [AGU](agu.md) ~~ [CGMS](cgms.md) ~~ [COSPAR](cospar.md) ~~ [DPS](dps.md) ~~ [EGU](egu.md) ~~ [EPSC](epsc.md) ~~ [FHS](fhs.md) ~~ [IPDW](ipdw.md) ~~ [IVC](ivc.md) ~~ [JpGU](jpgu.md) ~~ [LPSC](lpsc.md) ~~ [MAKS](maks.md) ~~ [MSSS](msss.md) ~~ [NIAC](niac_program.md) ~~ [VEXAG](vexag.md) ~~ [WSI](wsi.md) ┊ ··•·· **Contests:** [Google Lunar X Prize](google_lunar_x_prize.md)|

1. Docs: …
1. <https://www.europlanet-society.org/european-planetary-science-congress>


## The End

end of file
