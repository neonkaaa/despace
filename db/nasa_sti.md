# Scientific and technical information program
> 2019.12.18 [🚀](../../index/index.md) [despace](index.md) → **[NASA](nasa.md)**, [Project](project.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Scientific and technical information (STI) program** — англоязычный термин, не имеющий аналога в русском языке. **Программа научной и технической информации (ПНТИ)** — дословный перевод с английского на русский.</small>

The **NASA scientific and technical information (STI) program** collects, organizes, provides for archiving, and disseminates NASA’s scientific and technical information. The NASA STI program provides access to the NASA Aeronautics and Space Database and its public interface, the NASA Technical Report Server, thus providing one of the largest collections of aeronautical and space science STI in the world.



## Описание
The NASA Scientific and Technical Information (STI) Program was established to support the objectives of NASA’s missions and research. The Mission of the STI Program is to support the advancement of aerospace knowledge and contribute to U.S. competitiveness in aerospace research and development. This program is essential to help NASA avoid duplication of research by sharing information and to ensure that the U.S. maintains its preeminence in aerospace-related industries and education. The NASA STI Program acquires, processes, archives, announces, and disseminates NASA STI and acquires worldwide STI of critical importance to NASA and the Nation.

The STI Program is a critical component in the worldwide activity of scientific and technical aerospace research and development. Collected from U.S. and international sources, STI is organized according to content prior to being added to the NASA Technical Reports Server. The NTRS is a world-class collection of STI that includes over a million bibliographic records and a growing number of full-text documents. A public interface is available through the NASA Technical Reports Server (NTRS).

NASA STI Program

1. Helps provide the widest appropriate dissemination of NASA research results
1. Is essential in helping the United States maintain leadership in aerospace
1. Provides a mechanism to help NASA avoid duplication of research, saving time, effort and research funds

STI Program as a critical component in the aerospace community

1. Acquires STI from sources around the world
1. Maintains the largest collection of aerospace STI in the world
1. Provides worldwide access to STI that increases productivity and minimizes duplication of research

STI Program Mission

1. Supports the advancement of aerospace knowledge
1. Contributes to U.S. competitiveness in aerospace research and development (R&D) by helping to show the value of R&D funding and accomplishment
1. Implement R&D policy and align procedures with U.S. laws and NASA organizational requirements
1. Collects STI from all NASA centers


## STI
STI (scientific and technical information) is defined as a paper, abstract, journal article, presentation, etc. that contains and delivers the results (the analyses of the data, facts, and resulting conclusions) of basic and applied scientific, technical and related engineering research and development.

Types of STI:

1. Abstracts  extended abstracts
1. Accepted manuscripts & hournal articles
1. Books & book chapters
1. Final contractor/grantee reports
1. Meeting/conference papers
1. STI documents on public websites
1. STI report series
1. Theses & dissertations



## NTRS
**NASA Technical Reports Server (NTRS)** <https://ntrs.nasa.gov> provides access to aerospace-related citations, full-text online documents, images, and videos. The types of information included are: conference papers, journal articles, meeting papers, patents, research reports, images, movies, and technical videos — scientific and technical information (STI) created or funded by NASA.

By maintaining the NTRS collection, the STI Program supports NASA’s strategic goals to:

1. Expand the frontiers of knowledge, capability, and opportunity in space.
1. Advance understanding of Earth and develop technologies to improve the quality of life on our home planet.
1. Serve the American public and accomplish our Mission by effectively managing our people, technical capabilities, and infrastructure.

The NTRS integrates the following three NASA collections and enables search and retrieval through a common interface:

1. **NACA Collection:** Citations and reports from the National Advisory Committee for Aeronautics period lasting from 1915 to 1958, with information dating back to 1900.
1. **NASA Collection:** Citations and documents created or sponsored by NASA starting in 1958 and continuing to the present.
1. **NIX Collection:** Citations and links to the images, photos and movies once contained in the NASA Image eXchange as well as other collections currently curated by NASA and its various centers.

Please see NASA Disclaimers, Copyright Notice, and Terms and Conditions of Use for information on how to use NASA’s scientific and technical information.



## NTRS-R
The NTRS-R (NASA Technical Reports Server Registered) contains:

1. The complete STI collection of NASA and non-NASA aerospace materials and is available by registration only to U.S. Government civil servants, contractors, and grantees
1. Over 4.3 million metadata records
1. Over 500K full-text PDF images
1. Saved search capabilities

U.S. Civil Service, Contractors, and Grantees may fill out [the NTRS-R Registration form ❐](https://www.sti.nasa.gov/ntrs-registration) for access to the complete collection of NASA STI.



## Disclaimers, Copyright Notice, and Terms and Conditions of Use

**DISCLAIMERS**

**Disclaimer of Liability:** With respect to materials (e.g., documents, photographs, audio recordings, video recordings, tools, data products, or services) on or available through download from this Web site, neither the U.S. Government, NASA, nor any of its employees or contractors make any representations or warranties, express, implied, or statutory, as to the validity, accuracy, completeness, or fitness for a particular purpose; nor represent that use would not infringe privately owned rights; nor assume any liability resulting from the use of such materials and shall in no way be liable for any costs, expenses, claims, or demands arising out of the use of such materials.

**Disclaimer of Endorsement:** Neither the U.S. Government nor NASA endorse or recommend any commercial products, processes, or services. Reference to or appearance of any specific commercial products, processes, or services by trade name, trademark, manufacturer, or otherwise, in NASA materials does not constitute or imply its endorsement, recommendation, or favoring by the U.S. Government or NASA. The views and opinions of authors expressed on NASA Web sites or in materials available through download from this site do not necessarily state or reflect those of the U.S. Government or NASA, and they may not be used for advertising or product endorsement purposes.

**COPYRIGHT NOTICE**

**General**

Generally, United States government works (works prepared by officers and employees of the U.S. Government as part of their official duties) are not protected by copyright in the U.S. (17 U.S.C. §105) and may be used without obtaining permission from NASA. However, U.S. government works may contain privately created, copyrighted works (e.g., quote, photograph, chart, drawing, etc.) used under license or with permission of the copyright owner. Incorporation in a U.S. government work does not place the private work in the public domain.

Moreover, not all materials on or available through download from this Web site are U.S. government works. Some materials available from this Web site may be protected by copyrights owned by private individuals or organizations and may be subject to restrictions on use. For example, contractors and grantees are not considered Government employees; generally, they hold copyright to works they produce for the Government. Other materials may be the result of joint authorship due to collaboration between a Government employee and a private individual wherein the private individual will hold a copyright to the work jointly with U.S. Government. The Government is granted a worldwide license to use, modify, reproduce, release, perform, display, or disclose these works by or on behalf of the Government.

While NASA may publicly release copyrighted works in which it has government purpose licenses or specific permission to release, such licenses or permission do not necessarily transfer to others. Thus, such works are still protected by copyright, and recipients of the works must comply with the copyright law (Title 17 United States Code). Such copyrighted works may not be modified, reproduced, or redistributed without permission of the copyright owner.

If a recognizable person appears in a photograph or video recording, or the talent (such as a performance or narration) of a known individual is included in an audio or video recording, use for commercial purposes may infringe on a right of privacy or publicity. Permission should be obtained from the recognizable person or known individual. However, if the intended use of NASA material is primarily for educational or informational purposes (e.g., books, newspapers, and magazines reporting facts of historical significance), then such uses will generally not be considered an infringement of such personal rights.

Additional copyright information concerning copyright status is given in the metadata for the documents and other STI.

**Documents**

Documents available from this Web site are not protected by copyright unless noted. If not copyrighted, documents may be reproduced and distributed, without further permission from NASA. However, some documents or portions of documents available from this site may have been contributed by private individuals or organizations and may be copyrighted. If copyrighted, permission should be obtained from the copyright owner prior to use (e.g., modification, reproduction, or redistribution).

**Photography**

Photographs available from this Web site are not protected by copyright unless noted. If not copyrighted, photographs may be reproduced and distributed without further permission from NASA. If copyrighted, permission should be obtained from the copyright owner prior to use. If a recognizable person appears in a photograph, use for commercial purposes may infringe a right of privacy or publicity and permission should be obtained from the recognizable person.

**Audio Recordings**

Audiotape recordings available from this Web site are not protected by copyright unless noted. If not copyrighted, audio recordings may be excerpted or reproduced and distributed, without further permission from NASA. If copyrighted, permission should be obtained from the copyright owner prior to use. Use for a commercial purpose of a portion or segment of an audio recording that incorporates the talent (such as narration or music) of a known individual, may infringe a right of publicity and permission should be obtained from the known individual.

**Video Recordings**

Video recordings (videotape and motion picture recordings) available from this Web site, including audio recordings therein, are not protected by copyright unless noted. If not copyrighted, video recordings may be reproduced and distributed, without further permission from NASA. If a video recording is copyrighted in its entirety or a portion of a recording contains copyrighted material, such as music or video footage, permission should be obtained from the copyright owner prior to use. Use for a commercial purpose of any portion or segment of a video recording that displays a recognizable person, or the talent (such as a performance or narration) of a known individual, may infringe on a right of privacy or publicity, and permission should be obtained from the recognizable person or known individual.

**TERMS AND CONDITIONS OF USE**

Unless additional terms and conditions are provided with specific materials, the following information specifies the terms and conditions under which the materials on or available from this Web site may be used.

1. NASA does not grant exclusive use rights with respect to NASA material.
1. NASA should be acknowledged as the source of its material.
1. It is unlawful to falsely claim copyright or other rights in NASA material.
1. Material available from this Web site may include visible NASA identifiers (NASA name and initials, NASA Insignia, NASA Logotype, NASA Seal, and NASA Program Identifiers). Use of such material is generally non-objectionable, provided the NASA identifiers appear in their factual context. Other uses of the NASA Insignia, Seal, Logotype, or Program Identifiers require approval under 14 C.F.R. Part 1221. Use of the NASA name and initials is protected under 42 U.S.C. 2459b.
1. NASA material may not be used to state or imply the endorsement by NASA or by any NASA employee of commercial products, processes, or services, or used in any other manner that might mislead.
1. Use of NASA identifiers which would express or imply such an endorsement is strictly prohibited.
1. Use of the NASA name or initials as an identifying symbol by organizations other than NASA is strictly prohibited.
1. NASA does not indemnify nor hold harmless users of NASA material, nor release such users from copyright infringement.
1. NASA personnel are not authorized to sign indemnity or hold harmless statements, releases from copyright infringement, or documents granting exclusive use rights.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Project](project.md)】**<br> [Interferometer](interferometer.md) ~~ [NASA open](nasa_open.md) ~~ [NASA STI](nasa_sti.md) ~~ [NIH](nih.md) ~~ [Past, future and everything](pfaeverything.md) ~~ [PSDS](us_psds.md) [MGSC](mgsc.md) ~~ [Raman spectroscopy](spectroscopy.md) ~~ [SC price](sc_price.md) ~~ [SC typical forms](sc.md) ~~ [Spectrometry](spectroscopy.md) ~~ [Tech derivative laws](td_laws.md) ~~ [View](view.md)|

1. Docs: …
1. <http://www.sti.nasa.gov>
1. <https://ntrs.nasa.gov> — NASA Technical Reports Server (NTRS)



## The End

end of file
