# IMU (a list)
> 2019.07.31 [🚀](../../index/index.md) [despace](index.md) → [Sensor](sensor.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

A list of [Inertial measurement units](sensor.md).

## Current



### ADS Astrix 120   ［EU］
**Astrix 120** — optical [gyroscope](iu.md). Designed by [ADS](ads.md) in …. ITAR free.

|**Characteristics**|**(Astrix 120)**|
|:--|:--|
|Composition|2 units — ICU & GEU|
|Consumption, W|24 (6 per ON channel)|
|Dimensions, ㎜|180 × ⌀215 (ICU), 270 × 150 × 145 (GEU)|
|[Interfaces](interface.md)|[1553](mil_std_1553.md), RS-422|
|[Lifetime](lifetime.md), h(y)|… (15)|
|Mass, ㎏|6.5 (2 ICU, 4.5 GEU)|
|[Overload](vibration.md), Grms|25 g sine, 16 grms in plane (23 out), 1 200 g for 1.2 ‑ 10 ㎑|
|[Radiation](ion_rad.md), ㏉(㎭)|500 (50 000), SEP tolerant, latchup immune|
|[Reliability](qm.md)|0.9999 (0.995 after 5 y)|
|[Thermal](tcs.md), ℃|–10 ‑ +50 (full performance), –20 ‑ +60 (operation)|
|[TRL](trl.md)|9|
|[Voltage](sps.md), V|22 ‑ 50|
|**【Specific】**|~ ~ ~ ~ ~ |
|Impulse price (scale coefficient), ≤|—|
|・in Acc.channel| |
|・in Angle channel| |
|・stability in Acc.channel| |
|・stability in Angle channel| |
|Instability of the ° pos. of axes, ≤|—|
|・in Acc.channel|3σ: 10 ppm (200 ppm after 5 y)|
|・in Angle channel| |
|Noise in output information, ≤|—|
|・in Acc.channel| |
|・in Angle channel| |
|Quantity|—|
|・№ of accelerometers in the device|4|
|・№ of devices in GNC|1|
|・№ of gyroscopes in the device|4|
|Random error of the zero signal, ≤|—|
|・from start to start in Acc.channel| |
|・from start to start in Angle channel|3σ: 0.01 °/h|
|・BoL after calibr. in Acc.channel| |
|・BoL after calibr. in Angle channel|0.005 °/√h|
|Range of measurement|…, ± 140 °/s (max)|
| |[![](f/iu/a/ads_astrix_120_pic1t.webp)](f/iu/a/ads_astrix_120_pic1.webp)|

**Notes:**

1. **Applicability:** 27 satellites on orbit flying Astrix 120/200: 104 channels cumulating
500 years ON (as of January 2022)



### ADS Astrix 1090   ［EU］
> **Astrix 1090** — англоязычный термин, не имеющий аналога в русском языке. **Астрикс 1090** — дословный перевод с английского на русский.

**Astrix 1090** — волоконно‑оптический [гироскоп](iu.md) (ВОГ) в составе [КА](sc.md), предназначенный для определения положения КА в инерциальной системе отсчёта.  
*Разработчик:* [ADS](ads.md). Разработано в 2016 году компанией [Astrium](astrium.md). Прибор прошёл сертификацию. Покупное изделие.

|**Characteristics**|**(Astrix 1090)**|
|:--|:--|
|Composition|1 unit|
|Consumption, W|15|
|Dimensions, ㎜|261 × 171|
|[Interfaces](interface.md)|[1553](mil_std_1553.md)|
|[Lifetime](lifetime.md), h(y)|26 280 (3) / 131 400 (15)|
|Mass, ㎏|4.8 (1 прибор)|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)|1 000 (100 000)|
|[Reliability](qm.md)|0.98|
|[Thermal](tcs.md), ℃|–25 ‑ +60|
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|Range of measurement|± 200 ㎧²|
|Range of measurement of angular velocity|± 30 °/s|
|Impulse price (scale coefficient), ≤|—|
|・in Acc.channel|0.0001 ㎧|
|・in Angle channel|0.0000083 °|
|・stability in Acc.channel|0.005 % (3σ)|
|・stability in Angle channel|0.01 % (3σ)|
|Instability of the ° pos. of axes, ≤|—|
|・in Acc.channel|0.005555 °|
|・in Angle channel|0.011111 °|
|Noise in output information, ≤|—|
|・in Acc.channel|0.001 ㎧ (1σ)|
|・in Angle channel|0.0000833 ° (1σ)|
|Quantity|—|
|・№ of accelerometers in the device|3|
|・№ of devices in GNC|2|
|・№ of gyroscopes in the device|3|
|Random error of the zero signal, ≤|—|
|・from start to start in Acc.channel|0.00049 ㎧² (3σ)|
|・from start to start in Angle channel|0.15 °/h (3σ)|
|・BoL after calibr. in Acc.channel|0.0002 ㎧² (3σ)|
|・BoL after calibr. in Angle channel|0.05 °/h|

**Notes:**

1. Цена изготовления одного прибора: 2.5 млн €
1. **Applicability:** КА [ЭкзоМарс‑2020](экзомарс_2020.md)



### ADS Astrix NS   ［EU］
**Astrix NS** — optical [gyroscope](iu.md). Designed by [ADS](ads.md) in …. ITAR free.

|**Characteristics**|**(Astrix NS)**|
|:--|:--|
|Composition|1 unit|
|Consumption, W|6|
|Dimensions, ㎜|100 × 100 × 100|
|[Interfaces](interface.md)|2 × RS-422 / RS-485 full duplex|
|[Lifetime](lifetime.md), h(y)|… (15)|
|Mass, ㎏|1.1 ‑ 1.4 (depending on required shielding)|
|[Overload](vibration.md), Grms|25 g sine, 20 grms random, 2 000 g for 1 ‑ 10 ㎑|
|[Radiation](ion_rad.md), ㏉(㎭)|250 (25 000) |
|[Reliability](qm.md)|0.9999 (0.995 after 5 y)|
|[Thermal](tcs.md), ℃|–20 ‑ +65|
|[TRL](trl.md)|9|
|[Voltage](sps.md), V|20 ‑ 52|
|**【Specific】**|~ ~ ~ ~ ~ |
|Impulse price (scale coefficient), ≤|—|
|・in Acc.channel| |
|・in Angle channel| |
|・stability in Acc.channel| |
|・stability in Angle channel| |
|Instability of the ° pos. of axes, ≤|—|
|・in Acc.channel|500 ppm (3σ)|
|・in Angle channel| |
|Noise in output information, ≤|—|
|・in Acc.channel| |
|・in Angle channel| |
|Quantity|—|
|・№ of accelerometers in the device|3|
|・№ of devices in GNC|1|
|・№ of gyroscopes in the device|3|
|Random error of the zero signal, ≤|—|
|・from start to start in Acc.channel| |
|・from start to start in Angle channel|0.02 °/h (EoL: 0.2 °/h) (3σ)|
|・BoL after calibr. in Acc.channel| |
|・BoL after calibr. in Angle channel|0.005 °/√h|
|Range of measurement|…, ± 60 °/s|
| |[![](f/iu/a/ads_astrix_ns_pic1t.webp)](f/iu/a/ads_astrix_ns_pic1.webp)|

**Notes:**

1. **Applicability:** …



### Antares BIUS‑M   ［RU］
> **БИУС‑М** — русскоязычный термин, не имеющий аналога в английском языке. **BIUS‑M** — дословный перевод с русского на английский.

**БИУС‑М** — волоконно‑оптический [гироскоп](iu.md) (ВОГ) в составе [КА](sc.md), предназначенный для определения положения КА в инерциальной системе отсчёта.

**Прибор ДКШГ.402138.031.** Разработчик [НПП Антарес](npp_antares.md). Разработан в 2013 году, активное использование. Лётная квалификация в составе КА 「Барс‑М」 (запуск 27.02.2015). Покупное 

|**Characteristics**|**(БИУС-М)**|
|:--|:--|
|Composition|1 unit|
|Consumption, W|15|
|Dimensions, ㎜|200 × 159|
|[Interfaces](interface.md)|[1553](mil_std_1553.md)|
|[Lifetime](lifetime.md), h(y)|… / 50 000 (5.7)|
|Mass, ㎏|5.3|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)|500 (50 000)|
|[Reliability](qm.md)|0.999 за 5 лет|
|[Thermal](tcs.md), ℃|–10 ‑ +40|
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|Impulse price (scale coefficient), ≤|—|
|・in Acc.channel|0.000000306 ㎧|
|・in Angle channel|0.0001″|
|・stability in Acc.channel|0.02 %|
|・stability in Angle channel|0.001 % (3σ)|
|Instability of the ° pos. of axes, ≤|—|
|・in Acc.channel|1.0″ (3σ)|
|・in Angle channel|1.0″ (3σ)|
|Noise in output information, ≤|—|
|・in Acc.channel|0.004 ㎧ (3σ)|
|・in Angle channel|0.09″ (3σ)|
|Quantity|—|
|・№ of accelerometers in the device|3|
|・№ of devices in GNC|2|
|・№ of gyroscopes in the device|3|
|Random error of the zero signal, ≤|—|
|・from start to start in Acc.channel|0.00049 ㎧² (3σ)|
|・from start to start in Angle channel|0.018 °/h (3σ)|
|・BoL after calibr. in Acc.channel|0.0003 ㎧² (3σ)|
|・BoL after calibr. in Angle channel|0.006 °/h|
|Range of measurement|± 98 ㎧², ± 20 °/s|

**Notes:**

1. [Чертёж ❐](f/iu/b:bius_m_sketch.pdf)
1. Цена изготовления одного прибора: 9 млн р при централизованной поставке ЭРИ от заказчика.
1. В 2017 году поступили сообщения о не очень удачных ЛИ КА Барс‑М, в т.ч. по причине БИУС‑М.
1. **Applicability:** Барс‑М・ [Luna‑26](луна_26.md)



### Antares IUS VOA   ［RU］
> **ИУС ВОА** — русскоязычный термин, не имеющий аналога в английском языке. **IUS VOA** — дословный перевод с русского на английский.

**ИУС ВОА** — волоконно‑оптический [гироскоп](iu.md) (ВОГ) в составе [КА](sc.md), предназначенный для определения положения КА в инерциальной системе отсчёта. ИУС ВОА представляет собой четырёхосную (с неортогональным расположением осей чувствительности) отказоустойчивую систему измерения проекций абсолютной угловой скорости и линейного ускорения объекта на оси приборной системы координат.  
*Разработчик:* [НПП Антарес](npp_antares.md). Разработано  

|**Characteristics**|**(ИУС ВОА)**|
|:--|:--|
|Composition|1 unit|
|Consumption, W|21 ‑ 35|
|Dimensions, ㎜|380 × 350 × 300|
|[Interfaces](interface.md)|[1553](mil_std_1553.md)|
|[Lifetime](lifetime.md), h(y)|50 000 (5.7) / 50 000 (5.7)|
|Mass, ㎏|15.1|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)|0.998|
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V|23 ‑ 32|
|**【Specific】**|~ ~ ~ ~ ~ |
|Impulse price (scale coefficient), ≤|—|
|・in Acc.channel|0.02 %|
|・in Angle channel|0.02 %|
|・stability in Acc.channel|0.005 °/s|
|・stability in Angle channel|0.0003 °/s|
|Instability of the ° pos. of axes, ≤|—|
|・in Acc.channel| |
|・in Angle channel| |
|Noise in output information, ≤|—|
|・in Acc.channel| |
|・in Angle channel| |
|Quantity|—|
|・№ of accelerometers in the device| |
|・№ of devices in GNC| |
|・№ of gyroscopes in the device| |
|Random error of the zero signal, ≤|—|
|・from start to start in Acc.channel|0.00049 ㎧²|
|・from start to start in Angle channel|0.00005 °/s|
|・BoL after calibr. in Acc.channel| |
|・BoL after calibr. in Angle channel| |
|Range of measurement|± 98 ㎧², ± 30 °/s|
| |[![](f/iu/i/ius_voa_pic1t.webp)](f/iu/i/ius_voa_pic1.webp)|

**Notes:**

1. <http://npp-antares.ru/index.php/equipment.html> — [archived ❐](f/iu/i/ius_voa_npp-antares_ru.djvu) 2018.03.26
1. **Applicability:** Персона №1・ Лотос №1



### NPOIT BIB-FG   ［RU］
> **БИБ‑ФГ** — русскоязычный термин, не имеющий аналога в английском языке. **BIB-FG** — дословный перевод с русского на английский.

**БИБ‑ФГ** — волоконно‑оптический [гироскоп](iu.md) (ВОГ) в составе [КА](sc.md), предназначенный для определения положения КА в инерциальной системе отсчёта.

**Прибор БЫ2.529.001‑02.** Разработчик [НПО ИТ](npoit.md). Разработан в 2009 году Активное использование. Лётная квалификация в составе КА 「[Фобос‑Грунт](фобос_грунт.md)」. Покупное изделие.

|**Characteristics**|**(БИБ-ФГ)**|
|:--|:--|
|Composition|1 unit, sealed. Заполнен азотом с P = 30 ‑ 100 ㎩.|
|Consumption, W|11.5|
|Dimensions, ㎜|156 × 77.6|
|[Interfaces](interface.md)|[1553](mil_std_1553.md)|
|[Lifetime](lifetime.md), h(y)|0.9951 за 10 суток / 13 100 (1.5)|
|Mass, ㎏|1.35|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)|200 (20 000)|
|[Reliability](qm.md)|0.97 for 3y; 0.9992 for 10d; 0.985 for 3y in 2020|
|[Thermal](tcs.md), ℃|–15 ‑ +50 (on); –40 ‑ +50 (off)|
|[TRL](trl.md)|9|
|[Voltage](sps.md), V|27 (23 ‑ 34)|
|**【Specific】**|~ ~ ~ ~ ~ |
|Impulse price (scale coefficient), ≤|—|
|・in Acc.channel|0.005 ㎧|
|・in Angle channel|0.5″|
|・stability in Acc.channel|0.05 %|
|・stability in Angle channel|0.05 %|
|Instability of the ° pos. of axes, ≤|—|
|・in Acc.channel|30″|
|・in Angle channel|40″|
|Noise in output information, ≤|—|
|・in Acc.channel|0.01 ㎧ (1σ)|
|・in Angle channel|3″ (1σ)|
|Quantity|—|
|・№ of accelerometers in the device|3|
|・№ of devices in GNC|2|
|・№ of gyroscopes in the device|3|
|Random error of the zero signal, ≤|—|
|・from start to start in Acc.channel|0.003 ㎧²|
|・from start to start in Angle channel|3.0 °/h|
|・BoL after calibr. in Acc.channel|0.001 ㎧²|
|・BoL after calibr. in Angle channel|0.2 °/h|
|Range of measurement|± 10 ㎧², ± 60 °/s|

**Notes:**

1. Цена изготовления одного прибора: 4.5 млн р при централизованной поставке ЭРИ от заказчика.
1. **Applicability:**
   1. КА [Luna‑25](луна_25.md) (снят на этапе НЭО, т.к. В 2017 году в ходе испытаний КА [Luna‑25](луна_25.md) выяснилось, что при посадке от вибраций погрешности измерений БИБ‑ФГ становятся больше, чем измеряемые им величины.)
   1. КА [Luna‑27](луна_27.md) (снят на этапе РКД, т.к. В 2017 году в ходе испытаний КА [Luna‑25](луна_25.md) выяснилось, что при посадке от вибраций погрешности измерений БИБ‑ФГ становятся больше, чем измеряемые им величины.)
   1. КА [Фобос‑Грунт](фобос_грунт.md)



### NPOIT BIB‑IG   ［RU］
> **БИБ‑ИГ** — русскоязычный термин, не имеющий аналога в английском языке. **BIB‑IG** — дословный перевод с русского на английский.

**БИБ‑ИГ** — шестиосный волоконно‑оптический [гироскоп](iu.md) (ВОГ) в составе [КА](sc.md), предназначенный для определения положения КА в инерциальной системе отсчёта.  
Разработчик [НПО ИТ](npoit.md). Разработано в 2014 году в рамках ЭП КА 「[Интергелиозонд](интергелиозонд.md)」, но в дальнейшем использован не был.

|**Characteristics**|**(БИБ-ИГ)**|
|:--|:--|
|Composition|1 unit|
|Consumption, W|20|
|Dimensions, ㎜|323 × 136|
|[Interfaces](interface.md)|[1553](mil_std_1553.md)|
|[Lifetime](lifetime.md), h(y)|… / 70 000 (8)|
|Mass, ㎏|3.6|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)|500 (50 000)|
|[Reliability](qm.md)|0.999 за 5 лет|
|[Thermal](tcs.md), ℃|–50 ‑ +50|
|[TRL](trl.md)|2|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|Impulse price (scale coefficient), ≤|—|
|・in Acc.channel|0.0000012 ㎧|
|・in Angle channel|0.001″|
|・stability in Acc.channel|0.01 %|
|・stability in Angle channel|0.009 %|
|Instability of the ° pos. of axes, ≤|—|
|・in Acc.channel|8.0″|
|・in Angle channel|8.0″|
|Noise in output information, ≤|—|
|・in Acc.channel|Оценка не проводилась|
|・in Angle channel|0.09″ (3σ)|
|Quantity|—|
|・№ of accelerometers in the device|6|
|・№ of gyroscopes in the device|6|
|・№ of devices in GNC|1|
|Random error of the zero signal, ≤|—|
|・from start to start in Acc.channel|0,0009 ㎧²|
|・from start to start in Angle channel|0.15 °/h|
|・BoL after calibr. in Acc.channel|0.0004 ㎧²|
|・BoL after calibr. in Angle channel|0.005 °/h|
|Range of measurement|± 20 ㎧², ± 40 °/s|

**Notes:**

1. Цена изготовления одного прибора: 9 млн руб при централизованной поставке ЭРИ от заказчика
1. **Applicability:** Прибор нигде не применяется.### BIUS-L (RU)
> **БИУС‑Л** — русскоязычный термин, не имеющий аналога в английском языке. **BIUS-L** — дословный перевод с русского на английский.

**БИУС‑Л** — волоконно‑оптический [гироскоп](iu.md) (ВОГ) в составе [КА](sc.md), предназначенный для определения положения КА в инерциальной системе отсчёта.  
Разработчик: [НПЦАП](npcap.md). Разработано  

|**Characteristics**|**(БИУС-Л)**|
|:--|:--|
|Composition|1 unit|
|Consumption, W|25 (при +5 ‑ +40 ℃);<br> 40 (при –5 ‑ +5 ℃ и +40 ‑ +50 ℃)|
|Dimensions, ㎜|260 × 200|
|[Interfaces](interface.md)|[1553](mil_std_1553.md)|
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|10 (1 прибор)|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃|–20 ‑ +35 (on); –40 ‑ +50 (off)|
|[TRL](trl.md)|3|
|[Voltage](sps.md), V|27 (24 ‑ 32)|
|**【Specific】**|~ ~ ~ ~ ~ |
|Impulse price (scale coefficient), ≤|—|
|・in Acc.channel|0.5 "/имп|
|・in Angle channel| |
|・stability in Acc.channel| |
|・stability in Angle channel| |
|Instability of the ° pos. of axes, ≤|—|
|・in Acc.channel| |
|・in Angle channel|40"|
|Noise in output information, ≤|—|
|・in Acc.channel| |
|・in Angle channel|2" (3σ)|
|Quantity|—|
|・№ of accelerometers in the device|3|
|・№ of devices in GNC|2|
|・№ of gyroscopes in the device|3|
|Random error of the zero signal, ≤|—|
|・from start to start in Acc.channel| |
|・from start to start in Angle channel|0.3 %|
|・BoL after calibr. in Acc.channel| |
|・BoL after calibr. in Angle channel|0.05 %|
|Range of measurement| |
|Range of measurement of angular velocity|± 80.0 °/s|

**Notes:**

1. [Чертёж ❐](f/iu/b/bius-l_sketch1.pdf) ~~ [Инженерная записка ❐](f/iu/b/bius-l_iz_2018.djvu) (2018)
1. **Applicability:** …



### NPOIT MBINS   ［RU］
> **МБИНС** — русскоязычный термин, не имеющий аналога в английском языке. **MBINS** — дословный перевод с русского на английский.

**МБИНС** — волоконно‑оптический [гироскоп](iu.md) (ВОГ) в составе [КА](sc.md), предназначенный для определения положения КА в инерциальной системе отсчёта. По заявлению разработчика, не уступает [LN-200](imu_lst.md).  
*Разработчик:* [НПО ИТ](npoit.md). Разработано

|**Characteristics**|**(МБИНС)**|
|:--|:--|
|Composition|1 unit|
|Consumption, W| |
|Dimensions, ㎜| |
|[Interfaces](interface.md)|Манчестер‑2|
|[Lifetime](lifetime.md), h(y)|50 000 ‑ 100 000 (5.7 ‑ 11.4) / …|
|Mass, ㎏|1.34|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)| |
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|Impulse price (scale coefficient), ≤|—|
|・in Acc.channel| |
|・in Angle channel| |
|・stability in Acc.channel| |
|・stability in Angle channel| |
|Instability of the ° pos. of axes, ≤|—|
|・in Acc.channel| |
|・in Angle channel| |
|Noise in output information, ≤|—|
|・in Acc.channel| |
|・in Angle channel| |
|Quantity|—|
|・№ of accelerometers in the device|3|
|・№ of devices in GNC| |
|・№ of gyroscopes in the device|3|
|Random error of the zero signal, ≤|—|
|・from start to start in Acc.channel| |
|・from start to start in Angle channel| |
|・BoL after calibr. in Acc.channel| |
|・BoL after calibr. in Angle channel| |
|Range of measurement| |
| |[![](f/iu/m/mbins_pic1t.webp)](f/iu/m/mbins_pic1.webp)|

**Notes:**

1. <http://www.npoit.ru/products/telemetricheskie-sistemy/miniatyurnaya-besplatformennaya-inertsialnaya-navigatsionnaya-sistema-mbins> — [archived ❐](f/iu/m/mbins_npoit_ru.djvu) 2018.03.26
1. **Applicability:** …



### NPCAP BIUS-L   ［RU］
> **БИУС‑Л** — русскоязычный термин, не имеющий аналога в английском языке. **BIUS-L** — дословный перевод с русского на английский.

**БИУС‑Л** — волоконно‑оптический [гироскоп](iu.md) (ВОГ) в составе [КА](sc.md), предназначенный для определения положения КА в инерциальной системе отсчёта.  
Разработчик: [НПЦАП](npcap.md). Разработано  

|**Characteristics**|**(БИУС-Л)**|
|:--|:--|
|Composition|1 unit|
|Consumption, W|25 (при +5 ‑ +40 ℃);<br> 40 (при –5 ‑ +5 ℃ и +40 ‑ +50 ℃)|
|Dimensions, ㎜|260 × 200|
|[Interfaces](interface.md)|[1553](mil_std_1553.md)|
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|10 (1 прибор)|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃|–20 ‑ +35 (on); –40 ‑ +50 (off)|
|[TRL](trl.md)|3|
|[Voltage](sps.md), V|27 (24 ‑ 32)|
|**【Specific】**|~ ~ ~ ~ ~ |
|Impulse price (scale coefficient), ≤|—|
|・in Acc.channel|0.5 "/имп|
|・in Angle channel| |
|・stability in Acc.channel| |
|・stability in Angle channel| |
|Instability of the ° pos. of axes, ≤|—|
|・in Acc.channel| |
|・in Angle channel|40"|
|Noise in output information, ≤|—|
|・in Acc.channel| |
|・in Angle channel|2" (3σ)|
|Quantity|—|
|・№ of accelerometers in the device|3|
|・№ of devices in GNC|2|
|・№ of gyroscopes in the device|3|
|Random error of the zero signal, ≤|—|
|・from start to start in Acc.channel| |
|・from start to start in Angle channel|0.3 %|
|・BoL after calibr. in Acc.channel| |
|・BoL after calibr. in Angle channel|0.05 %|
|Range of measurement|…, ± 80.0 °/s|

**Notes:**

1. [Чертёж ❐](f/iu/b/bius-l_sketch1.pdf) ~~ [Инженерная записка ❐](f/iu/b/bius-l_iz_2018.djvu) (2018)
1. **Applicability:** …



### NG LN-200   ［US］
> **LN-200** — англоязычный термин, не имеющий аналога в русском языке. **ЛН-200** — дословный перевод с английского на русский.

**LN‑200** — волоконно‑оптический [гироскоп](iu.md) (ВОГ) в составе [КА](sc.md), предназначенный для определения положения КА в инерциальной системе отсчёта.  
*Разработчик:* [Northrop Grumman](northrop_grumman.md). Разработано в 1994 году 

|**Characteristics**|**(LN‑200)**|**(LN‑200c)**|**(LN‑200s)**|
|:--|:--|:--|:--|
|Composition|1 unit, sealed|1 unit, sealed|1 unit, sealed|
|Consumption, W|12 ‑ 16|12|12|
|Dimensions, ㎜|89 × 85|89 × 85|89 × 85|
|[Interfaces](interface.md)|[RS-485](rs_xxx.md)|[RS-485](rs_xxx.md)|[RS-422](rs_xxx.md)/[485](rs_xxx.md)|
|[Lifetime](lifetime.md), h(y)|… / 20 000 (2.3)|… / 20 000 (2.3)|… / 20 000 (2.3)|
|Mass, ㎏|0.75 ‑ 1.25|0.75|0.75|
|[Overload](vibration.md), Grms| | | |
|[Radiation](ion_rad.md), ㏉(㎭)| | |100 (10 000)|
|[Reliability](qm.md)| | | |
|[Thermal](tcs.md), ℃|–54 ‑ +71|–54 ‑ +71|–54 ‑ +71 (on);<br> –62 ‑ +85 (off)|
|[TRL](trl.md)|9|9|9|
|[Voltage](sps.md), V|+5, ± 15; +13 ‑ +35|+5, ± 15|+5, ± 15|
|**【Specific】**|~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |
|Impulse price (scale coefficient), ≤|—|—|—|
|・in Acc.channel| | | |
|・in Angle channel| | | |
|・stability in Acc.channel|0.0003 ‑ 0.005 % (1 σ)|0.001 ‑ 0.0003 %  (1 σ)|0.0003 %  (1 σ)|
|・stability in Angle channel|0.0001 ‑ 0.0005 % (1 σ)|0.0001 % (1 σ)|0.0001 % (1 σ)|
|Instability of the ° pos. of axes, ≤|—|—|—|
|・in Acc.channel| | |0.0057°|
|・in Angle channel| | | |
|Noise in output information, ≤|—|—|—|
|・in Acc.channel| |0.000035 ㎧²·㎐|0.000035 ㎧²·㎐|
|・in Angle channel| | | |
|Quantity|—|—|—|
|・№ of accelerometers in the device| | | |
|・№ of devices in GNC| | | |
|・№ of gyroscopes in the device| | | |
|Random error of the zero signal, ≤|—|—|—|
|・from start to start in Acc.channel|0.03 ㎧² (1 σ)|0.2 ‑ 0.0003 ㎧² (1 σ)| |
|・from start to start in Angle channel|3 °/ч (1 σ)|1 ‑ 2 °/ч (1 σ)| |
|・BoL after calibr. in Acc.channel|0.00003 ㎧² (1 σ)| |0.0003 ㎧² (1 σ)|
|・BoL after calibr. in Angle channel|1 °/ч (1 σ)|0.65 °/ч (1 σ)|1 °/ч (1 σ)|
|Range of measurement, ±|100 °/s²; 392 ㎧², 11.4 °/s|100 °/s²; 148 ㎧², 490 °/s|392 ㎧², 1 °/s|
| |[![](f/iu/l/ln-200_pic1t.webp)](f/iu/l/ln-200_pic1.webp)|

**Notes:**

1. …
1. **Applicability:** По состоянию на 2018.03.26 прибор LN‑200 применяется много где, выпущено более 30 000 LN‑200, например: [Falcon 9](falcon.md) ~~ [Dragon V1](dragon.md)



## Archive

### Template

### …   ［］
**…** — optical [gyroscope](iu.md). Designed by [](.md) in ….

|**Characteristics**|**(…)**|
|:--|:--|
|Composition| |
|Consumption, W| |
|Dimensions, ㎜|… × …|
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)| |
|Mass, ㎏| |
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)| |
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|Impulse price (scale coefficient), ≤|—|
|・in Acc.channel| |
|・in Angle channel| |
|・stability in Acc.channel| |
|・stability in Angle channel| |
|Instability of the ° pos. of axes, ≤|—|
|・in Acc.channel| |
|・in Angle channel| |
|Noise in output information, ≤|—|
|・in Acc.channel| |
|・in Angle channel| |
|Quantity|—|
|・№ of accelerometers in the device| |
|・№ of devices in GNC| |
|・№ of gyroscopes in the device| |
|Random error of the zero signal, ≤|—|
|・from start to start in Acc.channel| |
|・from start to start in Angle channel| |
|・BoL after calibr. in Acc.channel| |
|・BoL after calibr. in Angle channel| |
|Range of measurement| |

**Notes:**

1. **Applicability:** …



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**`Гироскоп:`**<br> …<br>~ ~ ~ ~ ~<br> **Европа:** [Astrix 1090](imu_lst.md) (4.8)  ▮  **РФ:** [ИУС-ВОА](imu_lst.md) (15.1) ~~ [БИУС-Л](imu_lst.md) (10) ~~ [БИУС-М](imu_lst.md) (5.1) ~~ [БИБ-ФГ](imu_lst.md) (1.36) ~~ [МБИНС](imu_lst.md) (1.34) ··· *([БИБ-ИГ](imu_lst.md) (3.6))*  ▮  **США:** [LN-200](imu_lst.md) (1.25)|

1. Docs:
   1. [Брошюры от Northrop Grumman ❐](f/iu/l/ln-200_doc1.djvu)
1. <http://www.northropgrumman.com/Capabilities/LN200FOG/Pages/default.aspx> — [archived ❐](f/iu/l/ln-200_northropgrumman_com.djvu) 2018.03.26


## The End

end of file
