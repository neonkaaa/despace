# ESTRACK
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [ESA](esa.md), **[НС](sc.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**European space tracking network (ESTRACK)** — англоязычный термин, не имеющий аналога в русском языке. **Европейская сеть космического слежения** — дословный перевод с английского на русский.</small>

**European space tracking network (ESTRACK)** — a number of ground‑based space‑tracking stations for the [European Space Agency](esa.md) (ESA).

| |
|:--|
|[![](f/gs/estrack_pic1t.webp)](f/gs/estrack_pic1.webp)|

**ESA stations**

1. Cebreros Station (Spain). [Bands](comms.md): <mark>TBD</mark>
1. Kiruna Station (Sweden). [Bands](comms.md): <mark>TBD</mark>
1. Kourou Station (French Guiana). [Bands](comms.md): <mark>TBD</mark>
1. Malargüe Station, (Argentina). [Bands](comms.md): <mark>TBD</mark>
1. [Maspalomas Station](maspalomas_station.md) (Gran Canaria, Spain). [Bands](comms.md): <mark>TBD</mark>
1. New Norcia Station (Australia). [Bands](comms.md): <mark>TBD</mark>
1. Redu Station (Belgium). [Bands](comms.md): <mark>TBD</mark>
1. Santa Maria Island Station (Azores, Portugal). [Bands](comms.md): <mark>TBD</mark>

**Cooperative stations**

1. Malindi Space Centre (Kenya). [Bands](comms.md): <mark>TBD</mark>
1. Santiago (Chile). [Bands](comms.md): <mark>TBD</mark>
1. Svalbard Satellite Station (Norway). [Bands](comms.md): <mark>TBD</mark>

**Former stations**

1. Villafranca Station (Spain). [Bands](comms.md): <mark>TBD</mark>

Each ESTRACK station is different, supporting multiple missions, some sharing one or more of the same missions. The ESTRACK network consists of at least:

1. Three 35-metre diameter antennas (Cebreros, Malargüe, New Norcia).
1. Six 15-metre antennas
1. One 13-metre antenna
1. One 12-metre antenna
1. One 5.5-metre antenna
1. Six GPS-TDAF antennas

There are also at least eleven smaller antennas with sizes of 9.3 to 2.5-metres. The antennas are remotely operated from the ESTRACK Control Centre (ECC) located at ESOC.

On 1 January 2013, the 35-metre station Malargüe became the newest station to join the ESTRACK Deep Space Network.

The station in Santa-Maria can be used to track Ariane launches and it is also capable of tracking Vega and Soyuz launchers operated from ESA’s Spaceport at Kourou, French Guiana.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**`Наземная станция (НС):`**<br> … <br><br> [CDSN](cdsn.md) ~~ [DSN](dsn.md) ~~ [ESTRACK](estrack.md) ~~ [IDSN](idsn.md) ~~ [SSC_GGSN](ssc_ggsn.md) ~~ [UDSC](udsc.md)|

1. Docs: …
1. <https://en.wikipedia.org/wiki/ESTRACK>



## The End

end of file
