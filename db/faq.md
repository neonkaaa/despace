# Despace Engineering Handbook
> 2019.05.05 [🚀](../../index/index.md) [despace](index.md) → [SCS](sc.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---



## 1. Basics

### 1.1. Basic principles

| |**1.1. Basic principles**|
|:--|:--|
|1| This Handbook is intended to help space engineers to create & operate a spacecraft. It consists of several facts, best practices, cases, & links for further readings. |
|2| Core principles:<br> ➊ **DO NOT PANIC!**<br> ➋ Do what you want.<br> ➌ There shall be a balance.<br> ➍ Brevity is the soul of wit.<br> ➎ Doubt is the key to success.<br> ➏ Everyone can create a spacecraft.<br> ➐ The best system is the one you can use to succeed.<br> ➑ Terminology & design shall be the same for everything. But remember words don’t matter.<br> ➒ Nothing is strict, or constant, or a rule. Flexibility & curiosity win most of the time.<br> ➓ Bureaucracy is slow, but you shall write down / justify the decisions & results — because everything will be forgotten. |
|TBD| Everything is relative. Everything can be seen through comparison. Bad is called good when worse happens. There is no point to tell how much 「something」 you obtain / lost if you do not compare it to 「anything」 previous. |
|TBD| Chaos is mostly dynamic & able to adapt. Use different approaches & combinations of them. Be flexible, have plans & a vision of how to achieve them. |
|TBD| When we do space engineering the only proper answer for questions like 「」who has to be in charge of something」 or 「who has to handle the situation」 is — 「the one who needs it」. If you build your habits around this approach you will succeed. |
|TBD| Almost everything can be broken down into 2 pieces (eventually 3) — that is how it’s done in nature. In most cases, everything else is just a combination of these 2 pieces. |
|TBD| For everything you plan & do there have to be:<br> ➊ A **Reason** (or need or target). Try not to use anything that has bad reasons. Change bad reasoned approaches you use as soon as possible. Examples of the bad reasons:<br> ・「It’s modern」<br> ・「I don’t know」<br> ・「It’s free / paid」<br> ・「Everyone uses it」<br> ・「The time is short」<br> ・「They told me to do that」<br> ・「This is a common path here」<br> ・「I didn’t look for good alternatives」<br> ・「I’m sure my way is correct, but I can’t prove it」<br> ➋ A **Consumer**. Try to avoid bad consumers — the ones, who:<br> ・doesn’t honor agreements<br> ・doesn’t pay<br> ・lies |
|TBD| When working with humans pay attention to what they do, not what they say. Yet, progress will be there, where you can make an agreement with those who have similar goals.<br> Everybody in a company is hired employee, even the CEO. Apply equality. You’re not better than them, & they’re no worse than you. There is no power or authority. Despite anything.<br> And try to:<br> ・always reply to them;<br> ・not make robots negatively reply to humans — robots shall make human’s life easier;<br> ・not make robots judge human effectiveness in performing robot’s duties. |
|TBD| Use alphabetical order; break it only in extreme cases. Number each section and, if possible, each paragraph of any document — that will lead to accurate reference & less error rate. |
|TBD| Use KISS (keep it simple, stupid) — it seems that perfection is attained not when there’s nothing more to add, but when there’s nothing more to remove.<br> Simplicity is a multifaceted concept. For some, a hammer is difficult, but for some, the P vs NP problem is quite simple. Avoid making documents larger than 150 pages while using large enough font. Use simple yet visible enough text markup. |
|TBD| Reduce your overhead while letting your people rest & think. Shorten the distances between every step, reduce the number of steps, & get rid of supporting structures that require time & people. Let authors defend their works. |
|TBD| Use the tools you need to achieve your goal, not just the ones everyone is used to. In IT, use universal standards that are shown to the user in the form to which he is accustomed.<br> The Despace’s technical part:<br> ・Bots — to perform robotic & routine duties, accept commands, remind, & track states<br> ・Chat — **XMPP or Mattermost or Rocket.chat** — to communicate<br> ・Git — **Gitea, GitLab** — to store them all with history & backups<br> ・Knowledge database<br> ・Markdown — to collaboratively write, parse, & remember everything<br> ・Office apps — **Word, Excel** — they look nice, can create math models, & everyone has them<br> ・What may be automated should be automated |
|TBD| No one should be punished for fails, one should be punished for 「not mentioned there can be fail」. |
|TBD| There’re 2 things — documents & words. Let the words complement the documents, not vice versa. |
|TBD| Terminology shall be the same for everyone. And the purpose shall prevail over the content. Don’t spend a lot of time on:<br> ・invent / change terms<br> ・reonrganize the documents. |
|TBD| When making decisions, it’s necessary to be clear now & then:<br> ・how the decision was made,<br> ・how it affects everything else,<br> ・what options were considered & why they were rejected. |
|TBD| Documents & processes shall be alive & up to date. If something requires periodic adjustment, then between adjustments it’s unadjusted. Changes shall be reported when ready, not at the next meeting. Changes to documents shall be made when ready, not when scheduled. |
|TBD| Content over form. 1st, you study the form, then the content. It’s impossible to create, knowing the form, but not knowing the content. It’s impossible to create, knowing the content, but not knowing the form. |
|TBD| The engineer is multifaceted & brave. An engineer is always sure of everything. An engineer is never sure of anything. The engineer seeks to create standards. The engineer seeks to bypass the standards. The engineer maintains the status quo. The engineer changes the status quo. All statements are correct. |
|TBD| If there are common design standards, try to stick to them. This will allow you to:<br> ➊ communicate with the rest of the community in the same language,<br> ➋ not waste time inventing bicycles,<br> ➌ avoid common mistakes,<br> ➍ focus on content, not form. |
|TBD| Don’t allow vendor lock.<br> ・Vendor lock is convenient, beautiful, integrated, & there are people who are willing to do good for your money. But if something happens (bankruptcy, mood change, fire, quarrel, etc.), then your business will stop.<br> ・Therefore, your information & the minimum tools to work with it shall be yours, but improvements & simplifications can be performed by others.<br> ・Therefore, prioritize ease of implementation & maintenance over ease of use.<br> ・The information you produce/use shall be as free as possible — that’s how you can avoid being influenced by someone else. |
|TBD| Bring in small details everywhere. Small details will not spoil the overall impression of the creation, but will please those who can see them:<br> ➊ If you make a big beautiful picture with spacecraft & space, do not forget to add a small image of a UFO, or a planet with the Little Prince, or something else.<br> ➋ In a large document (100+ pages), try to add phrases in the text like:<br> ・spacegraft<br> ・гениальный сетевой график<br> ・двигатели‑моховики<br> ・демоническая модель<br> ・комический аппарат<br> ・комический комплекс<br> ・ракетно‑комический комплекс<br> ・неземной комплекс управления<br> ・энергообречённость космического аппарата<br> ・etc.<br> ➌ Also, projects can have mascots. When preparing a presentation, on the 1st slide, stick a mascot & the slogan 「Keep moving forward」 or something else in an inconspicuous place. |
|TBD| Avoid momentary benefits, because often the cost of correcting mistakes made during achieving these benefits is many times higher than the benefits. |
|TBD| Processes should be built comfortably & bring joy to employees & customers. People shall strive for goals, not their substitutes. |
|TBD| Every choice, action, & incation leads to consequences. For every consequence there shall be responsible persons, & those who can handle this consequence. Everyone makes mistakes, especially people. Do what you can to:<br> ➊ avoid mistakes<br> ➋ minimize consequences<br> ➌ eliminate consequences |
|TBD| Help can come from anywhere. Involve everyone in the process, especially those who are higher in position than you. |
|TBD| In every aspect (career, process, project) you shall see the next step.  |
|TBD| Time estimates are mostly useless when it comes to people, & absolutely useless when it comes to people engaged in non‑standard tasks. You can estimate day & night, but not people. Time estimation shall agreed from both sides with responsibility & consequences. You can try & include regular communications between employees. |
|TBD| Share resources both technics & management. |
|TBD| When managing use deadlines & priorities. Both shall be real but not just someone’s will or promise. |
|TBD| Use what you promote & create. |
|TBD| You can't wake someone who pretends to be asleep. |
|TBD| Do not encourage stupidity |
|TBD| Plan widely, but let them decide small tasks. For tasks, as always, let them choose how to organize their working processes. The Despace use the following task list, trying to perform tasks from up to down:<br> ➊ Urgent<br> ➋ Important<br> ➌ Not‑urgent<br> ➍ Not‑important |
|TBD| Don’t trust analytics, trust facts. |
|TBD| Read a document in whole, not just 「your」 part. |
|TBD|  |



### 1.2. Obvious engineering

| |**1.2. Obvious engineering**|
|:--|:--|
|1| Engineering is simple — just make everything works in the way you want it to. **[System engineering](se.md)** is about making systems. A **system** is a set of balanced units that can achieve the target. A **balance** is when nothing overweights. A simple path:<br> ➊ Identify your [need & goals](rnd.md).<br> ➋ Describe major elements using approach described on the 「[unit page](unit.md)」.<br> ➌ Identify environments, objects, tasks, & requirements.<br> ➍ Choose people you would like to work with.<br> ➎ Desing, build, & test what you want.<br> ➏ [Make sure](vnv.md) your craft acts properly & survives on every stage of its lifetime.<br> ➐ Perform needed formal steps.<br> ➑ Create documents needed to build this very craft & possible future ones.<br> ➒ Mix & repeat everything in the way you feel good until you succeed. |
|2| Everything is a **environment**, **object**, & **task**. An engineer always perform tasks to (not)modify & investigate objects & environments, — it’s a nature of engineering.<br> ░╟ **Environment**<br> ░╟ **Object**<br> ░║░╟ (Specific) Object/Equipment<br> ░║░╟ Target<br> ░║░╙ Unit<br> ░║░░╙ modules<br> ░║░░░╙ assemblies<br> ░║░░░░╙ parts<br> ░╙ **Task**<br> ░░╟ Problem<br> ░░║░╙ challenge<br> ░░║░░╙ issue<br> ░░╙ Need<br> ░░░╙ goals<br> ░░░░╙ objectives |
|3| Environment is everything around objects & tasks, including their mutual influence. |
|4| There are 3 types of Objects:<br> ・A **(specific) Object/Equipment** — any object object, except for the 2 below<br> ・A **Target** — an object that is your target<br> ・A **Unit** — any part of a Space Segment |
|5| A spacecraft (SC) is a major unit of a Space Segment, it divides to the matters below:<br> ・**modules** can function separately, made of assemblies & parts<br> ・**assemblies** cannot function separately, made of parts<br> ・**parts**<br> ・**systems & subsystems** — specific combinations ot the above |
|6| There are 2 types of tasks:<br> ➊ the solution is known, execute it. This task can be precisely estimated & automated.<br> ➋ the solution is unknown, it must be found. It’s impossible to estimate it, because you can evaluate only what you know. It can be turned into the ➀ by a research. |
|7| Tasks can have different names — sometimes meaningfull, sometimes meaningless:<br> ・A **Problem** — something terrible that happens all of a sudden & you don’t know how to modify objects & environments. An engineer tries to transform it into a challenge.<br> ・A **Challenge** — you don’t know how to handle it, but, probably, there is a solution. An engineer tries to transform it into an issue.<br> ・An **Issue** — you probably know how to handle it. |
|8| Tasks can can be born out of **NGOs** (need, goals, objectives). See the logical steps on the 「[R&D page](rnd.md)」 |
|TBD| Define Spacecraft Systems structure as per 「[Spacecraft page](sc.md)」. |
|TBD| 1 lb = 0.453.592 ㎏<br> 1 g = 9.81 ㎧² (sometimes 9.806.650), but never 10. |
|TBD| The key areas of investigation for the unified description document are provided below.<br> <small>**1. Intro**<br> ・what is it, why we’re doing it, what is it for (1 page)<br> ・this table<br> ・abbreviations<br> ・lists (of authors, images, tables, literature)<br> **2. General description, consumers, NGOs, comparisons**<br> ・consumers — who will need that & what for<br> ・need, goals, objectives (NGOs)<br> ・general description<br> ・technical description<br> ・comparisons to analogs<br> **3. Requirements**<br> ・any requirement from any direction<br> ・one shall state where it came from & what are the reasons<br> ・also they shall be interconnected with everything else, so do V&V<br> ・from high to lower levels<br> **4. Structure, budgets, interfaces, CONOPS**<br> ・trees (system, subsystems, product)<br> ・interconnected budgets (materials, mass, power, thermal, tolerances, reliability, TRL, etc.)<br> ・interactions between components (from high level to lower levels)<br> ・CONOPS<br> **5. Technology description, key & critical technologies, TRL**<br> ・the list of technologies we need to buy/invent to achieve the NGOs<br> ・definition of how these techs are prioritized<br> ・high & deep‑level descriptions of how these techs are working as a system<br> ・description for TRLs<br> **6. Performance, V&V**<br> ・do the system meets the requirements & NGOs, & how good<br> **7. Quality (reliability, risks, safety)**<br> ・reliability<br> ・list of risks (risk management, if it’s possible)<br> ・safety<br> ・unification & standardization<br> **8. Cooperation, production timeline**<br> ・possible cooperation (preferably, 2+ for each component)<br> ・time estimation for suppliers, manufacturers, etc.<br> ・general timeline from now & up to disposal<br> **9. Cost model**<br> ・cost estimation for R&D, manufacturing, testing, launch, operations<br> ・plans for profit<br> **10. Patent research**<br> ・research for possible patent breaches<br> ・how to handle it<br> **11. Issues, future steps**<br> ・issues that were found<br> ・how we plan to handle them in the future<br> ・future steps for the R&D<br> **12. Conclusions**<br> ・what have been done<br> ・were the targets achieved<br> ・the next steps<br> **13. Annex A. Design variants**<br> ・ideas, sketches, proposals, etc.<br> ・why we didn’t choose them<br> ・what/why we chose<br> **14. Other annexes**<br> ・specific deep‑explored matters (structure, environment, thermal, vibrations, data, etc.)<br> ・calculations, drawings, models<br> ・IRD</small> |
|TBD| All models are wrong, but some are useful |



### 1.3. Links & books

| |**1.3. Links & books**|
|:--|:--|
|1| Proper non‑scientific books to read:<br> ・Antoine de Saint‑Exupéry — Le Petit Prince<br> ・Clifford D. Simak — City<br> ・Isaac Asimov — anything (especially the Foundation series)<br> ・Stanisław Lem — Solaris<br> ・Strugatsky — anything<br> ・Sun Tzu — The Art of War<br> ・Arthur C. Clarke — Childhood’s End<br> ・Lewis Carroll — Alice in Wonderland<br> ・Модель космоса, том 1 и 2<br> ・[NASA Systems engineering handbook](book_nasa_seh.md) |
|2| Some links:<br> ・<https://en.wikibooks.org/wiki/Space_Transport_and_Engineering_Methods><br> ・<https://en.wikibooks.org/wiki/Astrodynamics> |
|3|**Sources.** Pretty far from what they call the proper list of sources, but that’s all we have.<br>・<https://3dnews.ru/952315> <small>— [archive ❐](f/archive/20170524_1.pdf) of 2019.01.27</small><br>・<https://academia.edu><br>・<https://alemak.livejournal.com/1379.html> <small>— [archive ❐](f/archive/20140213_1.pdf) of 2019.01.27</small><br>・<http://astronautix.com><br>・<http://braeunig.us/space><br>・<https://britastro.org><br>・<http://ecoruspace.me><br>・<https://epizodyspace.ru><br>・<https://factoriesinspace.com> — In‑Space Manufacturing & Orbital Economy<br>・<https://incose.org> — International Council on Systems Engineering<br>・<https://trade.glavkosmos.com><br>・<https://globalspaceexploration.org><br>・<https://kosmolenta.com><br>・<https://multitran.com><br>・<https://nanosats.eu> — nanosats database<br>・<https://nasa.gov/offices/education/about/index.html><br>     ・<https://jpl.nasa.gov/missions/?type=current><br>     ・<https://nasa.gov/connect/ebooks/index.html> — NASA ebooks<br>     ・<https://forum.nasaspaceflight.com/index.php?topic=32901.0><br>     ・<https://ntrs.nasa.gov><br>     ・<https://pds.nasa.gov><br>     ・<https://spaceflight.nasa.gov/cgi-bin/acronyms.cgi?program=shuttle&searchall=true><br>・<https://newspace.im> — NewSpace index<br>・<https://forum.novosti-kosmonavtiki.ru><br>     ・<http://novosti-kosmonavtiki.ru/forum/forum14/topic8552><br>     ・<http://novosti-kosmonavtiki.ru/forum/forum14/topic14003><br>・<https://rocketengines.ru><br>・<https://satsearch.co><br>・<http://sewiki.ru> — Systems engineering thinking wiki<br>・<https://space.skyrocket.de/doc/acronyms.htm><br>・<https://spaceflightinsider.com><br>・<https://en.wikipedia.org>・ <https://ru.wikipedia.org>・ <https://ru.wiktionary.org><br>・[ГОСТ 16504-81](гост_16504.md)<br>・<https://www.spacematdb.com><br>・<https://spaceindustrydatabase.com><br>・**Unicode** — <https://compart.com/en/unicode/category/So>・ <https://htmlsymbols.xyz/unit-symbols>|



### 1.4. Common paths

| |**1.4. Common paths**|
|:--|:--|
|1|【**Anything failed**】<br><br> **Situation** — anything happened or can happen someday. (a common way to mitigate / avoid anything)<br><br> **Solution**:<br> ➊ Define the threat level.<br> ➋ Define possible risks, environment, requirements & available / possible resources.<br> ➌ Make a decision & implement it.<br> ➍ Make a solution to avoid this issue in the future.|
|2|【**A device broke down before the launch**】<br><br> **Situation** — a device can no longer be used, but you have time before the launch (e.g. broke, didn’t pass the tests, is no longer produced, etc.).<br><br> **Solution** — as usual, you need to balance resources (equipment, money, people, time, etc.). Typical solutions look like this (to choose any or a combination of them):<br> ➊ Repair the device.<br> ➋ Buy a new one, the same (but working) device.<br> ➌ Buy a device with similar characteristics, modify the SC & the mission in minor terms.<br> ➍ Adapt the device, SC & mission, albeit with some deterioration.<br> ➎ Buy a device that is not very close in characteristics, modify the SC & the mission.<br> ➏ Create/Order a device, taking additional risks & quickly going through the stages of R&D.|
|3|【**Requirements have not been verified**】<br><br> **Situation** — some requirements cannot be [verified](vnv.md) (digits are worse than expected).<br><br> **Solution** — a group that is in charge (e.g. for an SUI, ground equipment, etc.) shall describe the following issues to understand what mistake had been done, how to fix it & how to avoid it in the future. In case of failure that shall be done by that group with the help of the upper‑level designers.<br> ➊ What is the problem we try to solve.<br> ➋ How we got to this point & what will be done to prevent this from happening in the future.<br> ➌ Why what was described in the previous documents (approved by a customer) doesn’t work now.<br> ➍ What can be done so that what has already been approved will work.<br> ➎ A description of the pros & cons of the proposed options.<br> ➏ If it’s still impossible to satisfy the requirements, then what are the options for how the updated SUI will look like.<br> ➐ A description of the pros & cons of the proposed options.<br> ➑ What the developer ultimately proposes as the main option(s).|
|4|【**You need to design something new**】<br><br> **Situation** — there is an idea for a proposal, or a proposal for a R&D.<br><br> **Solution:**<br> ➊ Analyze/define the NGOs.<br> ➋ Define the possibilities to design it, including key technologies.<br> ➌ Describe several possible solutions & choose most suitable.<br> ➍ Define the requirements, build the system.|
|…|【**Template**】<br><br> **Situation** — ….<br><br> **Solution** — …|



### 1.5. Common mistakes & lessons learned

| |**1.5. Common mistakes & lessons learned**|
|:--|:--|
|1| This section contains a list of common errors in operation & [documentation](doc.md). The list is compiled based on the experience of working on various R&D projects, is almost universal & is not tied to any particular organization. Created for:<br> ➊ reduce the cost of subsequent fixes<br> ➋ improving the quality of work<br> ➌ focusing on the main, not the secondary |
|2| There is never enough time to do it right, but there’s always enough time to fix it. Errare humanum est. Time for work is usually scheduled, time for troubleshooting is usually not scheduled. |
|3|An **error** (from the Latin error, meaning 「wandering」) is an action which is inaccurate or incorrect. In some usages, an error is synonymous with a mistake. In statistics, 「error」 refers to the difference between the value which has been computed & the correct value. An error could result in failure or in a deviation from the intended performance or behavior.<br> Differences between an error & a mistake: an 「**error**」 — is a deviation from accuracy or correctness, a 「**mistake**」 — is an error caused by a fault: the fault being misjudgment, carelessness, or forgetfulness.<br> ・Docs: [Do not do their work for others ❐](f/doc/20191106_1.pdf)<br> ・<https://en.wikipedia.org/wiki/List_of_common_misconceptions><br> ・<https://en.wikipedia.org/wiki/Survivorship_bias>|
|4|**Error Types by xkcd (<https://xkcd.com/2303>):**<br> ➊ False positive<br> ➋ False negative<br> ➌ True positive for incorrect reasons<br> ➍ True negative for incorrect reasons<br> ➎ Incorrect result which leads you to a correct conclusion due to unrelated errors<br> ➏ Correct result which you interpret wrong<br> ➐ Incorrect result which produces a cool graph<br> ➑ Incorrect result which sparks further research & the development of new tools which reveal the flaw in the original result while producing novel correct results<br> ➒ The rise of Skywalker|
|5|【Mistake 1. **Implement an additional system for the same task**】<br> Leads to overhead & errors when rewriting from one to another.|
|6|【Mistake 2. **Stupidity**】<br> 「There are 2 infinities — the Universe & human stupidity. I’m not sure about the universe.」|
|7|【Mistake 3. **Game 「to get into the ass & get out of it heroically」**】<br> Classic. Make the wrong decision, & then clean up the consequences with sweat & blood.|
|8|【Mistake 4. **Change what works without learning why it was done this way**】<br> Leads to breakdowns / complications — often what looks awkward, but was done by smart people, was done that way with a reason. The Chernobyl nuclear power plant burned down precisely because of this.|
|9|【Mistake 5. **Small amount of time**】<br> 「Fast, high quality, cheap — choose any 2」. They often choose 「fast, cheap」.|
|10|【Mistake 6. **Non‑working atmosphere**】<br> Nerves, pressure, noise, calls, etc. distract, confuse thoughts, reduce concentration.|
|11|【Mistake 7. **Transfer of information by words. [Agreement](contract.md) in words**】<br> When a person speaks, & doesn’t write on paper & doesn’t sign, then the [end‑to‑end information path](etedp.md) looks something like this: 【What I thought → What I felt → What I said → What I decided about what I said → What my opponent heard → What he/she understood → What he/she remembered】; distortion of information is possible in each transition. In the human brain there’s a 「center of criticism」 through which incoming information passes, but not outgoing information. Thus, a person can say not what he wants, & the opponent can perceive not what he hears. Write protocols & sign them on the spot.|
|12|【Mistake 8. **Disconnection from the process, frequent switching**】<br> …|
|13|【Mistake 9. **Make sequential processes parallel**】<br> Leads to risks & overhead. The whole system can fail at once because of one small piece.|
|14|【Mistake 10. **Survivor Bias**】<br> **(ru. Систематическая ошибка выжившего)** is a type of selection bias, when there’s a lot of data on one group (「survivors」), & practically none on the other (「dead」). So researchers try to look for similarities among the 「survivors」 & lose sight of the fact that equally important information is hidden among the 「dead」. **Examples:**<br> ➊ In WW2, mathematician A. Wald from the New York SRG laboratory was instructed to find a solution to the problem: not all US bombers returned, & there were many holes on the returning ones, but they were unevenly distributed: many on the fuselage, fewer in the fuel system, & few — in the engine. So, more armor is needed in the holes? Wald replied: no, this shows that an aircraft with holes in these places can return. An aircraft that has been hit with an engine or gas tank will not be returned. Because hits in the 1st approximation are evenly distributed, it’s necessary to strengthen the places that the returnees have the most 「clean」.<br> ➋ There’s an opinion about the kindness of dolphins, based on the stories of swimmers who were pushed to the shore, but there’s no data from those who were pushed in the opposite direction.<br> ➌ Labor safety research is complicated by the fact that workers who are not adapted to harmful conditions quickly leave (the so‑called healthy worker effect).|
|15|【Mistake 11. **[Meetings](meeting.md) with a quick decision**】<br> Often, in meetings, decisions are made quickly without a detailed immersion in the issue. Subsequently, you have to spend resources on correcting the consequences of these decisions.|
|16|【Mistake 12. **Single‑handed decision making**】<br> There are always those who (not)deliberately distort the facts in their personal interests. Therefore there’s a scientific approach & peer review, & therefore consensus is important.|
|17|【Mistake 13. **(Not)consider everyone around as idiots**】<br> …|
|18|【Mistake 14. **Arphagraphy, gramar**】<br> Well, where can we go without them. MS Word can, of course, check something, but still check the document yourself. Also read fiction, improve your own literacy. |
|19|【Mistake 15. **A cursory reading of the documents**】<br> The devil is in the details, & in a hurry you can’t see the details. And sometimes it happens that there’s almost no water in the document, which can be skipped, & the written words are written for a reason.|
|20|【Mistake 16. **Duplication of information**】<br> If some information (composition, dimensions, mass, etc.) is indicated in more than one place, then during corrections it’s often corrected in one place, & in others it’s forgotten.|
|21|【Mistake 17. **Do not do your job**】<br> [Do not do their work for others❐](f/doc/20191106_1.pdf).|
|22|【Mistake 18. **Superfluous information**】<br> We write technical documentation, which, 1stly, is read by managers & generals who do not understand technology, & secondly, by technical specialists, who then use it to sharpen glands & carry out assembly. Therefore, there’s no need to spread thoughts along the tree — write briefly, clearly & to the point, follow the principle of Oссams razor.|
|23|【Mistake 19. **Using both [SI](si.md) & non‑SI units**】<br> Often, when converting one dimension to another, a size error will be made.|
|24|【Mistake 20. **Mythical 「generally known information」**】<br> What is known to everyone today may lose its relevance tomorrow. Therefore, try to refer (and it’s better to cite in the text) to generally accepted systems, constants, calculations, so that in 2, 10, 30 years you can understand how it all works & why. Some also think g = 10.|
|25|【Mistake 21. **Violation of alphabetical order**】<br> When compiling lists of jobs, documents, employees, etc., there’s a desire to sort them in non‑alphabetical order (by importance, significance, stages, etc.). This leads to the fact that, after agreeing on the document with the author of such a procedure, it’s required to explain this procedure to everyone who agrees later. And then everyone forgets the meaning of this logic & spends time looking for the right item. Alphabetical order is good for its speed & impartiality.|
|26|【Mistake 22. **Lack of numbers for documents**】<br> 「Mustache, paws & tail」 of any document is the name, number & signatures. There can be many documents with this name, documents with the same name, number & number — only one.|
|27|【Mistake 23. **Absence of an explicit choice**】<br> The choice should be spelled out clearly: 「based on the results of the given study, named matters were chosen」. No conventions, proposals, references like 「well, we wrote one thing in the conclusions, but in the footnotes on the hundredth page it’s written in small print that the conclusions should not be trusted.」|
|28|【Mistake 24. **Different names of the same matter**】<br> Often, within the framework of one document/project, devices are called either by name, then by name with a prefix, then by index, then by everyday name, then somehow else. So, is it really the same device? Sometimes — not & we are talking about different ones. The same applies to the names of documents, organizations, places, stages, etc.|
|29|【Mistake 25. **Links to non‑public documentation**】<br> If there’s a link to non‑public documentation (internal orders, letters, materials of other R&D, etc.), then they should be attached. There is no guarantee that they will be available to the Customer, & even more so when checked by the prosecutor in a couple of years.|
|30|【Mistake 26. **History does not tolerate the subjunctive mood**】<br> When evaluating history, it’s wrong to say something like: 「If Macedonian did not reach India, then…」 history went the way it went, & the rest is unfounded speculation.|
|31|【Mistake 27. **A single fool can confuse a thousand wise people**】<br> The owner 「doesn’t have to know all the technical details」 & for him it’s necessary to explain that a fool employee generates nonsense. To do this it’s necessary to spend a lot of expensive time explaning in detailes why these ideas are untenable. The owner sees 「an employee who is actively trying to solve problems」. It ends with everyone get tired & start to implement this nonsense; at the same time employees start looking for another employer. After a generation of employees has changed, the business of such an enterprise resembles a bicycle bed, which 「the grave will fix」 or a very robust investment in putting things in order, & then the grave.|
|32|【Mistake 28. **The judge & the prosecutor rolled into one**】<br> Equals should discuss/decide, a third party should judge, unless the equals agree.|
|33|【Mistake 29. **Implicit/unclear expressions & judgments**】<br> A person should not be attached to the document. Everything should be: written clearly, all numbers are available, links on the spot, etc.|
|34|【Mistake 30. **Figures without scatter**】<br> It’s usually considered that a number (dimensions, electricity, mass, etc.) is the worst boundary, i.e. there will be no worse value. But sometimes, in case of unscrupulous/negligent work, the performers consider the indicated figure to be the face value & subsequently give tolerances for it. Demand immediately either to write tolerances, or to indicate in words that the number is the limit.|
|35|【Mistake 31. **Pointing that something can be specified later**】<br> R&D process means that any document or unit at any stage can be specified. Actually, this is the principle of R&D — clarification through iterations, & then coordination of clarifications with stakeholders. However, in case of unfair work, such phrases in the documents lead to the fact that unreliable performers begin to demand the adoption of changes in fact. Therefore, such phrases shall be excluded from documents.|
|36|【Mistake 32. **Murphy’s Law & its variations**】<br> If something can go wrong, it will definitely go wrong. If something can be misunderstood, then they will definitely misunderstand. If 4 variants of malfunctions were envisaged & they were eliminated, then the 5th option will certainly appear.|
|37|【Mistake 33. **(Not‑to)doubt anything**】<br> …|
|38|【Mistake 34. **Not to use upper/lower UTF characters**】<br> Often, when copying from document to document, superscripts & subscripts (which were obtained by lifting numbers/letters using tags) are lost, and, say, 10⁴ ㎩ turn into 104 ㎩. And it happens the other way around, when 104 ㎩ is mistaken for a mistake & is regarded as 10 ㎪.|
|39|【Mistake 35. **Not knowing or understanding the contractor**】<br> Know the organizational structure, capabilities, goals, responsibilities, functions of the contractor, otherwise you will not be able to interact with him.|
|40|【Mistake 36. **Ignore deviation if you see it**】<br> Even if it is not in your competence or responsibility, not informing means dooming the whole company to inefficiency or marriage. If you see a deviation — tell about it, you know how to do it or fix it — teach them how.|
|41|Metrology.<br><br> **Allowed:**<br> ・use only SI units; non‑SI may be indicated in brackets;<br> ・use international or national designations, but not simultaneously;<br> ・apply decreasing indices up to 10⁻³ inclusive (㎝, ㎜, ㎳, ㎃, etc.), then — only numbers;<br> ・apply from 10⁻¹ from 10⁻⁶ inclusive zeros after the decimal point, then — only powers of 10;<br> ・apply increasing indices up to 10⁹ (㎞, ㎆, ㎬), then — only the power of 10.<br><br>**Forbidden:**<br> ・abbreviate the designations of quantities, if they are used without numbers, except for quantities in the heads & sides of tables & in the decoding of letter designations included in formulas & figures. (**ignored in this DB**)<br> ・separate the name from the number (or transfer them to different lines/pages).<br> ・apply turns of colloquial speech, technicalism, professionalism; (**ignored in this DB**)<br> ・apply for the same concept various terms close to the meaning, foreign terms in the presence of equivalent terms in the national language;<br> ・apply arbitrary word formations;<br> ・apply abbreviations of words, except for those established by the spelling rules, the corresponding state standards;<br> ・In the text of the document, with the exception of formulas, tables, figures:<br> ・・use the 「−」 sign (should be written with the word minus); (**ignored in this DB**)<br> ・・use the 「⌀」 sign (should be written with the word diameter); (**ignored in this DB**)<br> ・・apply ​​mat.signs w/o numerical values, e.g., \>, \<, =, ≤, ≥, ≠, №, %. (**ignored in this DB**)|

【**Table.** Размерности】

|**Правильно* |**Неправильно* |**Комментарии**|
|:--|:--|:--|
|г|гр| |
|кг|Кг| |
|мин|м| |
|ч|час| |
|с|сек| |
|рад|град| |
|5 градусов или 5°|5 град| |
|10 угловых минут или 10´|10 угл. мин| |
|40 угловых секунд или 40´´|40 угл. с| |
|10 угловых градусов или 10°|10 угл. град.| |
|окт/мин|октава/мин| |
|К|°К| |
|°/s или угловой градус в секунду|град/с| |
|°/ч|град/час| |
|Па (㎏f/㎝²)|㎏f/㎝²; кГс/㎝²; ㎏/㎝²| |
|Ом|Ом∙м| |
|Гр или Гр (рад)|рад| |
|Дж|дж| |
|В|в| |
|Гц|гц| |
|км/ч|км/час| |
|m/s² (g)|g, (ед)|<small>g не является единицей величины ускорения, а лишь символьное обозначение ускорения.</small>|
|Па∙с/м|Пас/м| |
|Па∙с/m³|Пас/m³| |
|(7 ± 2) Н·м [(70 ± 20) ㎏f·см]|7 Н·м| |
|70 ± 20 кгс·см| |
|бит<br> Б, байт<br> кбит<br> Кбайт|б<br> бт<br> Кбит<br> кбайт| |
|При нормальных условиях  m³|нm³| |
|Lр (исх. 20 м㎪) = 20 дБ или<br> 20 дБ (исх. 20 м㎪)<br> Не более 80 дБ (исх. 1 мкА)<br> Не менее 120 дБ (исх. 1 мкВ/м)|Не более 80 дБ мкА<br> Не менее 120 дБ мкВ/м|<small>Пример обозначения уровня звукового давления. Необходимо указывать исходную величину, её значение помещают в скобках  за обозначением<br> логарифмической величины. При краткой форме записи значение исходной величины указывают в скобках за значением уровня.</small>|
|100 ㎸т<br> 80 %<br> 20 ℃<br> 10 Ом<br> 20°<br> 1220×740 ㎜<br> 5,758°|100кВт<br> 80 %<br> 20℃<br> 10Ом<br> 20 °<br> 1220×740мм<br> 5°758| |
|㎪∙с/м|Па∙кс/м| |
|v = 3,6 s/t,<br> где v — скорость,км/ч;<br> s — путь, м;<br> t — время, с|v = 3,6 s/t км/ч,<br> где  s — путь, м;<br> t — время, с| |
|Вт/(м∙К)|Вт/м∙К| |
|Н∙м<br> А∙m²<br> Па∙с|НмНхм<br> Аm² Ахm²<br> Пас| |
|Вт∙м⁻²∙К⁻¹<br> Вт/(m²·K)|W/m²/К| |
|80 км/ч<br> 80 километров в час|80 км/час<br> 80 км в час| |
|Провести испытания пяти труб, каждая длиной 5 м.<br> Отобрать 15 труб для испытания на давление.|Провести испытания 5‑ти труб, каждая длиной 5 м.|<small>Числовые значения величин с обозначением единиц величин и единиц счёта следует писать цифрами, а числа без обозначения единиц величин и единиц счёта от единицы до девяти — словами.</small>|
|1,50; 1,75; 2,00 м|1,50 м; 1,75 м; 2,00 м| |
|Диаметр крепёжных отверстий прибора должен соответствовать М 4 (4,5 ㎜)|Диаметр крепёжных отверстий прибора должен соответствовать М 4 (⌀ 4,5)| |
|От 1 до 5 ㎜<br> От 10 до 100 кг<br> От плюс 10 до минус 40 ℃<br> От 8454,3 до 8464,3 ㎒|От 1 ㎜ до 5 ㎜<br> 10 … 100 ㎏<br> +10 ‑ −40 ℃<br> (8459,3 ± 5,0) ㎒| |
|Не более (если допустимы все значения меньше указанного значения).<br> Не менее (если допустимы все значения больше указанного значения).<br> Знак «±» не ставят перед не более, ≥|Больше, ниже, выше, меньше, хуже.<br> Не более ± 5 ℃.<br> Не менее ± 5 ℃| |
|Шероховатость поверхности.<br> Rₐ = 0,63 мкм<br> R<sub>z</sub>= 0,63 мкм|Чистота поверхности не хуже √ 0,63| |
|1,22∙10⁴|1,22Е+04| |
|Lo ≥ 15 МэВ/(мг∙см⁻²)<br> σо ≤ 10⁻² ㎝²|Lo ≥ 15 МэВ/(мг∙см⁻²)<br> σо ≤ 10⁻² ㎝²| |

【**Table.** Термины】

|**Правильно**|**Неправильно**|
|:--|:--|
|Температура точки росы ℃|Точка росы|
|Измерение|Замер, обмер|
|Точность, прецизионность|Верность измерений|
|Произвести измерение|Замерить, мерить, обмерить|
|Измерить давление|Померить давление|
|Измерить значение  напряжения или определить значение напряжения|Измерить  напряжение или<br> измерить величину напряжения|
|Результат измерений|Измеренные значения|
|Температура выражается в кельвинах|Температура измеряется в кельвинах|
|Единица скорости|Размерность скорости|
|Погрешность средств измерений|Погрешность показания прибора|
|Класс точности прибора указывается — 0,1; 0,2; 1, 2, 3.|Класс точности прибора ± 1,5 %|
|Погрешность измерения не должна быть  более 0,01 г<br> Погрешность измерения ±0,01 г|Ошибка измерения 0,01 г|
|Температура минус (60 ± 5) ℃|Температура — 60 ℃|
|Диапазон измерений от минус 50 до плюс 200 ℃|Предел измерений (−50 ‑ 200) ℃|
|Оси находились на одной высоте с допускаемыми отклонениями ± 1 ㎜|Оси находились на одной высоте с погрешностью ± 1 ㎜|
|Погрешность измерений не должна быть более (не менее) 1 %;<br> Погрешность измерения ± 1 %|Точность измерений 1 %|
|Погрешность измерений равна ± 5 %|Погрешность 3σ равна  ± 5 %|
|Единицы  величин|Единиц физических величин|
|Средство измерений или измерительный прибор|Мерительное средство|
|Средство измерений или измерительный прибор|Измерительная аппаратура|
|Средство измерений или измерительный прибор|Для измерения применялось средство контроля|
|Стандартный образец или измерительный прибор|Эталонная деталь|
|Нормальные климатические условия с указанием конкретных значений величин|Комнатная температура|
|Значение пусковой силы тока бортовой аппаратуры должна не превышать полуторократного номинального значения силы тока (указать значение или заменить термин номинального тока на потребляемый ток) электропотребления на время до 50 ㎳.|Величина пускового тока бортовой аппаратуры не должна превышать полуторократного значения номинального тока электро&shy;потребления на время до 50 ㎳.|
|Увеличение силы электрического тока до 23,5 А|Увеличение тока до 23,5 А|
|Значение силы электрического тока|Величина тока|
|Значение атмосферного давления 8,6∙10⁴ ㎩ (645 ㎜ рт. ст.)|Величина атмосферного давления 645 ㎜ рт. ст.|
|Ускорение|Перегрузка|
|Масса 3 ㎏|Груз 3 ㎏|
|Изделие массой 1 500 т|Изделие весом 1 500 т|
|Среднее квадратическое отклонение (стандартное отклонение) — параметр функции распределения измеренных значений или показаний, характеризующий их рассеивание и равный положительному корню квадратному из дисперсии этого распределения.|Не корректное употребление термина среднее квадратическое отклонение|



### 1.6. TRL

> [TRL definition](trl.md)  
> [CML definition](trl.md)

**TRL 0 — Ideas**

1. (Concept maturity level (CML) 1: Cocktail Napkin, Idea Generation, Overview & Advocacy. Spans to Phase 0 in common)
2. **Start date/event:**
3. **End date/event:**
4. **Responsible being:**
5. **Involved beings:** None.
6. **Inputs:** An initial spark in the dark.
7. **Tasks:**
    1. Generate the ideas (brainstorm).
    2. Identify the needs and possibilities.
    3. Define if the ideas are feasible in principle.
8. **Outputs:**
    1. A list of more‑or‑less refined and technically feasible ideas.
    2. Definition of possible market use.
9. **Risks:** None.

**TRL 1 — Basic principles observed & reported**

1. (Scientific research begins to be translated into applied R&D. Spans to Phase 0 in common. CML 2-4: Initial Feasibility, Trade Space, Point Design, Architecture selected within Trade Space)
2. **Start date/event:**
3. **End date/event:**
4. **Responsible being:**
5. **Involved beings:**
    1. Developers of the unit(s) or similar ones
    2. Possible customers
6. **Inputs:**
    1. Outputs from TRL 0
    2. Expectations of the unit
    3. Characteristics/expectations of the sub-units (or their alternatives)
    4. ConOps
7. **Tasks:**
    1. Identify the basic principles, requirements, NGOEs for the unit
    2. Define possible customers, perform market research
    3. Perform the literature review
    4. Identify & contact possible experienced companies
    5. Identify alternative units/approaches
    6. Define the possibility of using frameworks for alternative units/approaches
8. **Outputs:**
    1. Basic principles, requirements, NGOEs
    2. Matured ConOps
    3. Prove of ConOps possibility
    4. Use‑cases & market areas
    5. Prove the unit is needed
    6. Tools/frameworks/approaches to use
    7. Companies to contact
    8. Alternatives to study/use
    9. Similar prototypes
    10. Prove that we have a framework/tools/approaches to be adapted/developed for our needs.
9. **Risks:**
    1. Inability to study the market/technology
    2. TRL of sub‑units/technologies may be low or technologies may be not available
    3. TBD

**TRL 2 — Technology concept and/or application formulated**

1. (Once basic physical principles are observed, then practical applications of those characteristics can be invented/identified. At TRL 2, the application is speculative: there’s no experimental proof or detailed analysis to support the conjecture. Spans to Phase A and B1. CML 5-7: Baseline Concept, Integrated Concept, Preliminary Implementation Baseline)
2. **Start date/event:**
3. **End date/event:**
4. **Responsible being:**
5. **Involved beings:**
    1. Developers of the unit(s) or similar ones
    2. Possible customers
    3. Contractors
6. **Inputs:** Outputs from TRL 1
7. **Tasks:**
    1. Define the technologies (e.g., sub-units, tools, frameworks, approaches) needed to fulfill the NGOEs
    2. Define budgets (technical & management)
    3. Define Engineering plans & Schedule
    4. Mathematically model the ConOps and unit
    5. Contact companies (RFI, RFP)
    6. Update the market research
8. **Outputs:**
    1. Finalization of the high-level matters (requirements, tools to use, NGOEs, budgets & schedules)
    2. Mathematical models for the unit & ConOps (e.g., digital twin demo)
    3. RFIs/RFPs from companies
9. **Risks:**
    1. TBD

**TRL 3 — Analytical & experimental critical function and/or characteristic proof‑of‑concept**

1. (Active R&D is initiated. This must include both analytical studies to set the technology into an appropriate context & lab‑based studies to physically validate that the analytical predictions are correct. These studies & experiments should constitute 「proof‑of‑concept」 validation of the applications/concepts formulated at TRL 2. Spans to Phase B2. CML 8: Project Baseline)
2. **Start date/event:**
3. **End date/event:**
4. **Responsible being:**
5. **Involved beings:**
    1. Developers of the unit(s) or similar ones
    2. Possible customers
    3. Contractors
6. **Inputs:** Outputs from TRL 2
7. **Tasks:**
    1. Perform analytical/lab studies for key technologies
    2. Prove the TRL 2 matters (technologies, budgets, plans, models) are sufficient to achieve the NGOEs
8. **Outputs:**
    1. Fully functional model of the unit (and sub-units)
    2. Lab models for key technologies & ConOps
9. **Risks:**
    1. TBD

**TRL 4 — Component and/or breadboard validation in laboratory environment**

1. (Basic technological elements must be integrated to establish that the pieces’ll work together to achieve concept‑enabling levels of performance for a component/breadboard. This validation must be devised to support the concept formed earlier, & should be consistent with the requirements of potential system applications; it’s 「low‑fidelity」 compared to the eventual system: it could be composed of ad hoc discrete components in a lab. Spans to Phase C)
2. **Start date/event:**
3. **End date/event:**
4. **Responsible being:**
5. **Involved beings:**
    1. Developers of the unit(s) or similar ones
    2. Possible customers
    3. Contractors
6. **Inputs:** Outputs from TRL 3
7. **Tasks:**
    1. Perform the manufacture of the units & sub-units
    2. Integrate basic technological elements together to make them work as a system
    3. Test units/sub-units in labs
    4. Prove the parts you manufacture work as intended & can benefit to the final system
8. **Outputs:** Prove the components/units can work as intended in lab environment & meet NGOEs
9. **Risks:**
    1. TBD

**TRL 5 — Component and/or breadboard validation in relevant environment**

1. (The fidelity of the component/breadboard being tested has to increase significantly. The basic tech elements must be integrated with reasonably realistic supporting elements so that the total applications (component/system‑level) can be tested in a 「simulated」 or realistic environment. Spans to Phase D)
2. **Start date/event:**
3. **End date/event:**
4. **Responsible being:**
5. **Involved beings:**
    1. Developers of the unit(s) or similar ones
    2. Possible customers
    3. Contractors
6. **Inputs:** Outputs from TRL 4
7. **Tasks:**
    1. Perform tests from TRL 4 but in relevant environment
    2. Prove the parts you manufacture work as intended & can benefit to the final system
8. **Outputs:** Prove the components/units can work as intended in relevant environment & meet NGOEs
9. **Risks:**
    1. TBD

**TRL 6 — System model/prototype demonstration in a relevant environment (ground/space)**

1. (A representative model/prototype system or system — which would go well beyond ad hoc, 「patch‑cord」 or discrete component level breadboarding — would be tested in a relevant environment (if the only one is the space, then it must be demonstrated in space). Spans to Phase D)
2. **Start date/event:**
3. **End date/event:**
4. **Responsible being:**
5. **Involved beings:**
    1. Developers of the unit(s) or similar ones
   2. Possible customers
    3. Contractors
6. **Inputs:** Outputs from TRL 5
7. **Tasks:**
    1. Manufacture all the units
    2. Build a working representative system model/prototype
    3. Test it in a relevant environment (ground/space)
8. **Outputs:** Prove the representative system model/prototype can work as intended in relevant environment & meet NGOEs
9. **Risks:**

**TRL 7 — System prototype demonstration in a space environment**

1. (TRL 7 requiring an actual system prototype demonstration in a space environment. The prototype should be near or at the scale of the planned operational system & the demonstration must be in space. Spans to Phase E,F)
2. **Start date/event:**
3. **End date/event:**
4. **Responsible being:**
5. **Involved beings:**
    1. Developers of the unit(s) or similar ones
    2. Possible customers
    3. Contractors
6. **Inputs:** Outputs from TRL 6
7. **Tasks:**
    1. Manufacture & build an actual system (or prototype)
    2. Test it in space
8. **Outputs:** Prove the actual system (or prototype) can work as intended in space & meet NGOEs
9. **Risks:**

**TRL 8 — Actual system completed & Flight Qualified through test & demonstration (ground/space)**

1. (In almost all cases, the end of true 「system development」 for most technology elements. This might include integration of new technology into an existing system. Spans to Phase F)
2. **Start date/event:**
3. **End date/event:**
4. **Responsible being:**
5. **Involved beings:**
    1. Developers of the unit(s) or similar ones
    2. Possible customers
    3. Contractors
6. **Inputs:** Outputs from TRL 7
7. **Tasks:**
    1. Test the actual system in ground/space
    2. Achieve flight qualification through test & demonstration
8. **Outputs:** The flight qualified actual system
9. **Risks:**

**TRL 9 — Actual system Flight Proven through successful mission operations**

1. (In almost all cases, the end of last 「bug fixing」 aspects of true 「system development」. This might include the integration of new technology into an existing system. This TRL doesn’t include planned product improvement of ongoing or reusable systems. Spans to Phase F)
2. **Start date/event:**
3. **End date/event:**
4. **Responsible being:**
5. **Involved beings:**
    1. Developers of the unit(s) or similar ones
    2. Possible customers
    3. Contractors
6. **Inputs:** Outputs from TRL 8
7. **Tasks:**
    1. Test the actual system in ground/space
    2. Achieve flight proven qualification through successful mission operations
    3. Fix bugs if needed
8. **Outputs:** The flight proven actual system
9. **Risks:**



## 2. Ground & Space

### 2.1. General requirements

| |**2.1. General requirements**|
|:--|:--|
|TBD| Calculation of radiators (conceptual steps):<br> ➊ Determine the amount of heat that needs to be dumped, consisting of:<br> ・OE's heat production (in the 1st approximation, it is equal to electricity consumption)<br> ・heat received from the Sun & re-reflected from the celestial body (taking into account thermal protection & coatings)<br> ➋ Build a work cycle taking into account areas where radiators operate in the:<br> ・light & can receive outer heat (illumination, reflection) & their power may not be enough<br> ・shade & their power may be excessive<br> ➌ Select the material, shape & thickness of the radiator, taking into account:<br> ・heating & cooling rates<br> ・total heat capacity<br> ・capability of spacecraft to maintain heat balance in the shade |
|TBD|  |
|TBD|  |
|TBD|  |
|TBD|  |
|TBD|  |



### 2.2. Electrical & electro­magnetic requirements

| |**2.2. Electrical & electro­magnetic requirements**|
|:--|:--|
|TBD|  |



### 2.3. Mechanical & atmospheric requirements

| |**2.3. Mechanical & atmospheric requirements**|
|:--|:--|
|TBD|  |



### 2.4. Radiation & cosmic rays requirements

| |**2.4. Radiation & cosmic rays requirements**|
|:--|:--|
|TBD| There are 2 notable types of [radiation](ion_rad.md): ➊ heavy charged particles, ➋ all available spectra.<br> Living beings don’t mind heavy particles & suffer from the spectra; because it causes illnesses. For robots it’s vise versa; because it can damage their electric circuits. |
|TBD|  |
|TBD|  |
|TBD|  |



### 2.5. Thermal requirements

| |**2.5. Thermal requirements**|
|:--|:--|
|TBD|  |



## 3. Movement mechanics

| |**3. Movement mechanics**|
|:--|:--|
|TBD|  |



## 4. Addons

### 4.1. The Despace’s specific notes
**All data in this DB is under the CC0 license (<https://creativecommons.org/choose/zero>), except for data from numberless sources that may have other licenses. No classified data.** E.g. you can use it as you wish asking no one, but if you want to distribute/sell/whatever it then you better check if it’s legal. We try to follow the rules of fair use & to keep the data true, but sadly we’re only humans, we were born to die. Occasionally you may see Russian words — it was the 1st lang of this DB. The source is: <https://codeberg.org/neonkaaa/despace>

1. **For what?** Started as another handbook by a tiny spacecraft engineering group for some brief space/SC facts which have to be with you. Now it’s for science, planning & SC creating.
2. **For who?** For scientists, engineers & sympathetic essences. For those who want to create / plan / be in a stream.
3. **Who is?** Almost as at the start but a bit wider — a group of spacecraft engineers/scientists.
4. **Who is able?** In terms of git & CC0 — everybody can clone. In terms of this DB — anyone.
5. **Why not Wikipedia?** Because of the nuances in [SC](sc.md) design, & the ability to take the DB w/ you.

**Some technical issues.** There’re text files w/ markdown. To avoid vendor‑lock & use it anywhere in case you have a markdown editor/viewer. Notes & Requirements: (in historical order)

1. Fit pages for A4 or a 6" device. [The themes ❐](js/themes.zip) for proposed editors (VNote, ghostwriter) renders pages for A4. Liberation Mono 11.5.
2. Each database page has to be as self‑sufficient as possible.
3. Files / pages names — only with lower case Latin letters, digits, underlines.
4. Images ≤ 670 px wide. Photo ≤ 160×175 px (160×160 px body + 15 px year). [LV](lv.md)/[OE](sc.md) ≤ 120×120 px. Border 1px #ccc. Webp 75. Mini — ≤ 100×90 px (60×50 for companies), webp 69.
5. Fit [contact/company page](contact.md) & large logic blocks into 1 ‑ 2 A4.
6. Dates are used in YYYY.MM.DD format, e.g. — 1947.02.20. Time — 24h format, e.g. — 17:06.
7. Digits on the left are divided w/ the unbreakable space 「 」, on the right — dots, e.g.: 1 234 567.890.001.928.
8. Tables — try to avoid. Use the left align. Try to fit list points into a single line.
9. Write external links revealing the full path. You have to see where you’re about to go.
10. Prefer:
    1. a [text over images ❐](f/faq/text_pic_comparison.webp) — it’s searchable, editable, scalable, consumes less bytes;
    2. large pages full of topics over several tiny pages — easier to see the full picture;
    3. short lists — ≤ 10 points (for ordered ones) & no sub‑levels — it’s easier to remember.
11. Assume there are only 3 levels of headings (page title is included):
    1. 1 — Page title
    2. 1.1 — Sections
    3. 1.1.1 — Subsections
    4. 1.1.1.1 — Sub-subsections
12. Special symbols to be used: …°·•±×÷≤≥≈≠ ‑ − — ⎆ ↷✉ ❐“”’«»✔✘☐◪☑↪←↑→↓↔↕↖↗↘↙⟵⟶♁↗ 🚀 ªⁱⁿº⁺⁻⁼⁽⁾ ⁰¹²³⁴⁵⁶⁷⁸⁹₊₋₌₍₎ ₀₁₂₃₄₅₆₇₈₉ₐₑₒₓₔₕₖₗₘₙₚₛₜ ▂▃▅▇▓░┆╟║╙╓╱╲╳№©®™ ¼¾½⅓⅔⅕⅖⅗⅘⅙⅚⅛⅜⅝⅞ π§⌀∑∞√∛∜‰ ◯○⊙☀☁☂☃☄★☆$¢£¥€₽✓✕✖✗✉⌦ ｛｝（）［］【】・，、。「」『』 ➊➋➌➍➎➏➐➑➒➓ ➀➁➂➃➄➅➆➇➈➉ ⑪⑫⑬⑭⑮⑯⑰⑱⑲⑳ ⅠⅡⅢⅣⅤⅥⅦⅧⅨⅩⅪⅫⅬⅭⅮⅯ αβγδεζηθικλμνξο03C0πρςστυφχψω ΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟ03A0ΠΡΣΤΥΦΧΨΩ ⒶⒷⒸⒹⒺⒻⒼⒽⒾⒿⓀⓁⓂⓃⓄⓅⓆⓇⓈⓉⓊⓋⓌⓍⓎⓏ ♳♴♵♶♷♸♹ etc. ⒜⒝⒞⒟⒠⒡⒢⒣⒤⒥⒦⒧ ⒨⒩⒪⒫⒬⒭⒮⒯⒰⒱⒲⒳⒴⒵ 👌👍👎👏✋✌ ⑴⑵⑶⑷⑸⑹⑺⑻⑼⑽ ⑾⑿⒀⒁⒂⒃⒄⒅⒆⒇ ⒈⒉⒊⒋⒌⒍⒎⒏⒐⒑ ⒒⒓⒔⒕⒖⒗⒘⒙⒚⒛ №℡㏑㏒ K℉℃°㎭ ㎐㎑㎒㎓㎔ ㏈ ㎚㎜㎝㎞㎧㎳ ㍴ ㎅㎆㎇ ㎩㎪㎫㎬ ㏅㏐㏓ ㏙ ㎾㎿㎸㎹㎶㏁ ㎎㎏ ㎂㎃㎄ ㏉㋎㏜㏂㏘ 😷😵😳😲😱😰😭😫😪😩😨😥😤😣😢😡😠😞😝😜😚😘😖😔😓😒😏😍😌😋😊😉😆😅😄😃😂😁

~~~
<p style="page-break-after:always"> </p>
<ruby>A<rt>BCD</rt></ruby>
【**Table.** …】
【**Picture.** …】
<!--…-->
*(archived [2023.. ❐](f/archive))*
~~~

**Типографика** — (от греч. τύπος — отпечаток + γράφω — пишу, en. Typography) — искусство оформления при помощи наборного (не рисованного) текста посредством набора и вёрстки, базирующееся на правилах конкретного языка. Типографика — одна из отраслей графического дизайна, а также свод правил, определяющих использование шрифтов для создания наиболее понятного для восприятия текста.

1. <https://en.wikipedia.org/wiki/Typography>
2. <https://ru.wikipedia.org/wiki/Википедия:Отбивка_знака_процента_от_предшествующей_цифры>
3. <https://en.wikipedia.org/wiki/Box-drawing_character>
4. 2001.01.01 [Особенности набора ❐](http://web.archive.org/web/20080313061322/mamble.nm.ru/nabor.htm) — [archived ❐](f/archive/20010101_1.djvu) 2017.10.13
5. По ГОСТ 8.417‑2002 знак % отбивается от цифры, т.е., неправильно: 5%, правильно: 5 %.
6. Тире между цифрами в значении 「от‑до」 не отбивают (125‑199). Despace это игнорирует.
7. Кстати, на ЖЖ просто надо добавить ?format=light

**Примеры:**

1. 「 」 — короткий неразрывный пробел
2. 「 」 — длинный неразрывный пробел
3. 「‑」 — неразрывный дефис
4. Деньги — ₽ рупь ・ $ доллар США ・ € евро ・ ¥ японская иена


▇▅▃▂ **Archive** ▂▃▅▇

Just some ~~hysterical~~ historical pages:

[Astrium](astrium.md) ~~ [Canadian Space Commerce Association](csca.md) ~~ [Venus (2020.06.18)](faq_venus_20200618.md)


▇▅▃▂ **TBD (roadmap)** ▂▃▅▇

1. **MBSE**
   1. PP&C Project planning & control
2. **[Vibrations & shocks](vibration.md):**
   1. <https://www.google.com/search?q=grms+random+vibration>
   2. <https://www.google.com/search?q=SINE+structural>
   3. <https://en.wikipedia.org/wiki/Random_vibration>
   4. <https://femci.gsfc.nasa.gov/random/randomgrms.html>
   5. <https://vibrationresearch.com/blog/sine-on-random-application-testing>
   6. <https://sines.eimb.ru/Help.html>
   7. <https://en.wikipedia.org/wiki/Short_interspersed_nuclear_element>
3. **Companies & links:**
   1. <https://www.meicompany.com>
   2. <https://www.linkedin.com/company/enpulsion>
   3. <https://www.nasa.gov/directorates/spacetech/NASA_Technology_Enables_Precision_Landing_Without_a_Pilot>
   4. <https://www.inovor.com.au>
4. **Оформить и добавить методики расчёта, желательно в виде программ с комментариями:**
   1. баллистики:
      1. видимость наземных станций
      2. видимость Солнца
      3. коррекции траекторий в зависимости от космодрома
      4. окна старта
      5. перелёт на ЖРДУ
      6. перелёт на ЭРДУ
      7. перелёт с гравманёвром
      8. перелёт с повышением / понижением орбиты
      9. спуск в атмосфере
      10. цена изменения параметров орбиты
   2. мощности СЭС;
   3. надёжности;
   4. ПГС;
   5. прочности;
   6. радиации, РПЗ;
   7. радиолинии, в т.ч. связь с Землёй, диаграммы направленности:
      1. Orbiter ↔ Earth
      2. Orbiter ↔ Lander
      3. Lander ↔ Rover (payload on rover)
   8. СОТР;
      1. размер радиаторов
      2. эффективность радиаторов
   9. топлива;
   10. технического совершенства:
      1. антенн
      2. батарей аккумуляторных
      3. батарей солнечных, включая ФЭП
      4. двигателей
      5. двигателей-маховиков и гироскопов
      6. звёздных и солнечных датчиков
      7. КА
      8. кабельной сети
      9. конструкции
      10. РН
      11. <mark>TBD</mark>
5. **Научиться внятно работать и применять данные с:**<br> [ASP](ames_stereo_pipeline.md) ~~ [Blender](blender.md) ~~ [Cosmographia](cosmographia.md) ~~ [DOORS](doors.md) ~~ [NASA open](nasa_open.md) ~~ [NASA STI program](nasa_sti.md) ~~ [Octave](gnu_octave.md) ~~ [SPICE](spice.md) ~~ [STK](stk.md)
6. **Научиться визуализировать информацию.**
7. **В части накопления информации:**
   1. Аналитика по Луне
      1. Artemis
   2. Шары и конусы посадочных аппаратов Венер и Марсов
   3. Космические агентства — структура, подразделения, институты, их функции: [CNSA](03_cnsa.md) ~~ [CSA](03_csa.md)・ ± [ESA](03_esa.md) ~~ [ISRO](03_isro.md) ~~ [KARI](03_kari.md) ~~ [NASA](03_nasa.md)
   4. Тенденции по развитию космической отрасли.
   5. РН.
   6. Интеграция материалов <https://www.nasa.gov/offices/education/about/index.html>
   7. События.
   8. Добавить книги к описанию планет и прочих небесных объектов.
   9. Качество.
   10. Кооперация.
   11. БА отечественная / иностранная.
   12. НС.
   13. Схемы перелёта.
   14. Риск.
   15. Документы иностранные.
   16. Космические программы.
   17. Принципы разработки R&D (НИОКР): CH・ EU・ IN・ JP・ RU・ US
   18. Различные подходы к созданию СЧ КА.
   19. Инкубатор.
   20. Испытания.
   21. Качество.
   22. Научные исследования.
   23. Радиосвязь.
   24. Матрицы соответствия.
   25. Пилотируемый полёт.
   26. Планеты.
   27. Проекты.
   28. Экология.
   29. Эргономика.
   30. Удельный импульс топлива.
   31. Периодически патрулировать на предмет следующих пометок, добавлять / исправлять:<br> <mark>NOCAT</mark>・ <mark>TBD</mark>・ <mark>нетдаты</mark>・ <mark>нетин</mark>・ <mark>нетинсты</mark>・ <mark>нетмобильного</mark>・ <mark>нетподписи</mark>・ <mark>нетпочты</mark>・ <mark>нетрабочего</mark>・ <mark>неттви</mark>・ <mark>нетфб</mark>・ <mark>нетфото</mark>


▇▅▃▂ **Метафоры и мемы** ▂▃▅▇

**Мета́фора** — (от др.‑греч. μεταφορά — 「перенос」, 「переносное значение」) — слово или выражение, употребляемое в переносном значении, в основе которого лежит сравнение неназванного предмета с каким‑либо другим на основании их общего признака.

|**Метафора**|**Описание**|
|:--|:--|
|**Бритва Оккама**| **Бритва О́ккама** (иногда **лезвие Оккама**) — методологический принцип, получивший название от имени английского монаха‑францисканца, философа‑номиналиста Уильяма Оккама (ок. 1285‑1349). В кратком виде он гласит: **「Не следует множить сущее без необходимости」** (либо **「Не следует привлекать новые сущности без крайней на то необходимости」**). Сам Оккам писал: 「Что может быть сделано на основе меньшего числа [предположений], не следует делать, исходя из большего」 и 「Многообразие не следует предполагать без необходимости」. Этот принцип формирует базис методологического редукционизма, также называемый **принципом бережливости**, или **законом экономии** (лат. **lex parsimoniae**).<br> Принцип 「бритвы Оккама」 состоит в следующем: если некое явление может быть объяснено двумя способами: например, первым — через привлечение сущностей (терминов, факторов, фактов и проч.) А, В и С, либо вторым — через сущности А, В, С и D, — и при этом оба способа дают идентичный результат, то считать верным следует первое объяснение. Сущность D в этом примере — лишняя: и её привлечение избыточно.<br> Важно помнить, что бритва Оккама не аксиома, а презумпция, то есть она не запрещает более сложные объяснения в принципе, а лишь рекомендует **порядок рассмотрения** гипотез, который в большинстве случаев является оптимальным. |
|**Goose**| **Goose** is the only bird that can walk, swim, dive & fly, but does it all equally bad. |
|**Соотношение карты и территории**| **Соотношение карты и территории** — вопрос о соотношении между символом и объектом. Известное выражение Альфреда Коржибски — 「Карта не есть территория」 — означает, что абстракция, выведенная из чего‑нибудь, или реакция на неё не является самой вещью; иными словами, перст, указующий на предмет, не есть сам предмет; метафорическая репрезентация какого‑то концепта не является самим концептом; научная теория, описывающая 「объективную реальность」, не является самой 「объективной реальностью」 и т.д. То, что карта не территория, значит, что описание реальности не является самой реальностью. |
|**Шарики**| 「Представьте себе, что жизнь — это игра, построенная на жонглировании пятью шариками. Эти шарики — Работа, Семья, Здоровье, Друзья и Душа, и вам необходимо, чтобы все они постоянно находились в воздухе. Вскоре вы поймёте, что шарик Работа сделан из резины — если вы его невзначай уроните, он подпрыгнет и вернётся обратно. Но остальные четыре шарика — Семья, Здоровье, Друзья и Душа — стеклянные. И, если вы уроните один из них, он будет непоправимо испорчен, надколот, поцарапан, серьёзно повреждён или даже полностью разбит. Он никогда не будет таким, как раньше. Вы должны осознавать это и стараться, чтобы этого не случилось. Работайте максимально эффективно в рабочее время и уходите домой вовремя. Посвящайте необходимое время своей семье, друзьям и полноценному отдыху. Ценность ценна только если её ценят」.<br> — Брайан Дайсон, бывший СЕО Coca‑Cola|
|**Словоблудие**| Да тут аж [целая отдельная статья](словоблудие.md). |
|**Если можете не писать — тогда не пишите**| Говорят, однажды к Льву Толстому пришёл молодой писатель и просил прочесть его рукопись.<br> — Зачем? — спросил его Толстой.<br> — Мне важно узнать Ваше мнение, — ответил молодой литератор.<br> — Зачем Вам, милейший, знать моё мнение? А если, допустим, я прочту и скажу Вам: не пишите. Что будете делать? — насупил брови Лев Николаевич.<br> — Ну, как… Если Вы скажете не писать, то я не буду… — промямлило молодое дарование.<br> — Если можете не писать — тогда не пишите. — сказал граф и удалился. |
|**Тут так заведено**| Анекдот.<br> Клетка. В ней 5 обезьян. К потолку подвязаны бананы. Под ними лестница. Проголодавшись, одна из обезьян подошла к лестнице с намерением достать банан. Как только она дотронулась до лестницы, вы открываете кран и поливаете ВСЕХ обезьян очень холодной водой. Проходит немного времени, и другая обезьяна пытается полакомиться бананом. Те же действия с вашей стороны. Третья обезьяна, одурев от голода, пытается достать банан, но остальные хватают её, не желая холодного душа.<br> А теперь уберите одну обезьяну из клетки и замените ее новой обезьяной. Она сразу же, заметив бананы, пытается их достать. К своему ужасу, она увидела злые морды остальных обезьян, атакующих её. После третьей попытки она поняла, что достать банан ей не удастся. Теперь уберите из клетки ещё одну из первоначальных пяти обезьян и запустите туда новенькую. Как только она попыталась достать банан, все обезьяны дружно атаковали её, причем та, которую заменили первой, ещё и с энтузиазмом.<br> И так, постепенно заменяя всех обезьян, вы придёте к ситуации, когда в клетке окажутся 5 обезьян, которых водой вообще не поливали, но которые не позволят никому достать банан. Почему? Потому что тут так принято.|
|**Ищите во всём позитив**|Никогда не упускайте возможность сделать наш скучный мир хоть чуточку интереснее. Например, если Вас собьёт машина или ударит по башке сосулька, прежде чем потерять сознание / впасть в кому / умереть, успейте прошептать склонившемуся к Вам прохожему: 「Передайте членам Сопротивления, что Кальциноиды уже прибыли на Землю. Парапульсатор спрятан в сторожке у лесника. Пароль: 『В лесу, говорят, снова появились хромые лисицы』…」 При этом, конечно, ещё хорошо бы сунуть прохожему в ладонь заранее подготовленную флэшку со списком фамилий (желательно латышских) и кусочком видеозаписи, на которой какой‑то человек жадно ест корейскую морковь.|



### 4.2. Microblog entries (abandoned)

Below is a list of microblog (~1 000 chars) entries that are (have been) posted om different platforms. The aim is to promote space sciences, engineering approaches, obtain reviews & opinions, which can be included into this handbook.

<small>

1. 2022.06.06 — When we do #space #engineering the only proper answer for questions like 「who has to be in charge of something」 or 「who has to handle the situation」 is — 「the one who need it」.
2. 2022.06.07 — For the past 12 years, I was able to find & hire 11 engineers for my department (almost ¼ of the total headcount) & participated in #hiring ~25 people for other departments. So, 36 in total or 3 per year.<br> Not a big number, possibly, for an #HR but probably enough for an engineer during the 1st several years & the department’s deputy head during the last several years. There was only 1 candidate who had participated in an #interview & didn’t pass it. The HRs were involved only during the last stages — to prepare the needed documents.<br> Looking backward, I assume no one of those 11 could pass the 1st screening if we were using common advices 「How to look cool on an interview」 or 「How to stand out during your job‑seeking」 because:<br>・some weren’t able to have an eye contact;<br>・some weren’t able to smile properly;<br>・most of them weren’t able to answer simple questions & build strong logical sentences;<br>・all of them, except for a single one, weren’t able to discuss their passion for #space freely, except for short responses;<br>・they all were nervous;<br>・etc.<br>Yet, now these are good & brave employees that can handle complex issues.<br>Probably, that is because the focus was on are they able to perform the job they are hiring for & is there a mutual interest, but not about are they able to smile properly or not.
3. … — When you heading to your dreams no one can put strings on you, except yourself.<br> Because no one knows how to hurt you & what are your weak points. All they know is that they can try to push on random places waiting for your response. But why should you response then?<br> On the other hand, if that involves other people, then they still cannot put strings on you (until you let them) but they can block you if you’ll get close to them.
4. … — The #space nowadays is not a #rocket #science anymore. Some believe it’s because there are a lot of affordable technologies for low orbits (which I don’t classify as 「space」). Probably it never was a rocket science, — it was a hidden science. Or maybe a science that doesn’t worth spending time because there are more interesting things on Earth, or you cannot gain money out of fundamental space #explorations. Anyway, the space was & is simple. As any engineering matter.
5. … — There’s no big difference between the #space engineering & Earth one. Just some additions & complications for requirements & environment. Some notable points: fast evaporation & degradation of materials (metals, plastics, lubricants), lack of possibility of being maintained by a human, large temperature ranges, lesser gravity, own atmosphere, particles, radiation, vacuum. Basically, if a device can survive these conditions for a needed period then it can be used in space.
6. … — … где начинается космос
7. … — … problems
8. … — … почему земное неприменимо
9. … — … почему земное применимо
10. … — … почему космос важен
11. … — … инженерный подход, причины и следствия
12. … — … инженерный подход, определение необходимости
13. … — … инженерный подход, дуализм
14. … — … почему плохи MBSE, Agile, Scrum и иже с ними
15. … — … 
16. … — … 
17. … — … 
18. … — … 
19. … — … 

</small>

## The End

end of file
