# Future Launchers Preparatory Programme
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [LV](lv.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Future Launchers Preparatory Programme (FLPP)** — англоязычный термин, не имеющий аналога в русском языке. **Программа разработки перспективных пусковых установок** — дословный перевод с английского на русский.</small>

The **Future Launchers Preparatory Programme (FLPP)** is a technology development and maturation programme of the [European Space Agency (ESA)](esa.md). It develops technologies for the application in future European [launch vehicles (launchers)](lv.md) and in upgrades to existing launch vehicles. By this it helps to reduce time, risk and cost of launcher development programmes.



## Описание
Started in 2004, the programmes initial objective was to develop technologies for the Next Generation Launcher (NGL) to follow [Ariane 5](arian.md). With the inception of the [Ariane 6](arian.md) project, the focus of FLPP was shifted to a general development of new technologies for European launchers.

FLPP develops and matures technologies that are deemed promising for future application but currently do not have a sufficiently high [technology readiness level (TRL)](trl.md) to allow a clear assessment of their performance and associated risk. Those technologies typically have an initial TRL of 3 or lower. The objective is to raise the TRL up to about 6, thus creating solutions which are proven under relevant conditions and can be integrated into development programmes with reduced cost and limited risk.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[](.md)】**<br> <mark>NOCAT</mark>|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Future_Launchers_Preparatory_Programme>


## The End

end of file
