# Satrec Initiative
> 2020.07.24 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/s/satreci_logo1t.webp)](f/c/s/satreci_logo1.webp)|<inquiries@satreci.com>, +82-42-365-7474, Fax +82-42-365-7790;<br> *21, Yuseong-daero 1628 beon-gil, Yuseong-gu, Daejeon, 34054, Republic of Korea*<br> <https://www.satreci.com> ~~ [LI 1 ⎆](https://www.linkedin.com/company/satrec-initiative) & [LI 2 ⎆](https://www.linkedin.com/company/si-imaging-services) ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/Satrec_Initiative)|
|:--|:--|
|**Business**|	Earth observ., sats & their components, ground systems, analytics|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO — Ee-Eul Kim<br> ・Founder — Sungdong Park<br> ・Founder — Byungjin Kim<br> ・Founder, 1st Chairman — Professor Soondal Choi|

**Satrec Initiative Co., Ltd.** (Korean: 쎄트렉아이) or **SI** or **Satrec I** is a South Korean [satellite](sc.md) manufacturing company headquartered in Daejeon, South Korea. The company designs & builds Earth observation satellites platform called SpaceEye‑series, & SI provides various space components, incl. high resolution electro‑optical payloads & star‑trackers.

The company was founded in 1999 by the engineers who developed the 1st Korean satellite (KITSAT-1) at [KAIST](kaist.md) Satellite Technology Research Center (SaTRec). SI’s market entering began with a Malaysian Earth observation satellite, RazakSAT. SI has two subsidiaries, SI Imaging Service (SIIS), which is the main image data provider of KOMPSAT‑series & SI Detection (SID), which provides radiation monitoring solutions.

**[SI Imaging Services (SIIS)](siis.md)** is a leading Satellite Imagery Provider for Remote Sensing & Earth Observation. The SIIS has the Exclusive Worldwide Marketing & Sales Representative of KOMPSAT series which is KOMPSAT-2, KOMPSAT-3 & KOMPSAT-5. Also the SIIS serves DubaiSAT-2 satellite imagery worldwide.

Satellites & launches

- KOMPSAT-1 (1999): image receiving & processing station
- STSAT-1 (2003)
- KOMPSAT-2 (2006): image receiving & processing station
- RazakSAT (2009): satellite & ground systems
- DubaiSat-1 (2009): satellite & ground systems
- COMS-1 (2010): communication payload integration, electrical modules, sun sensor, image receiving & processing station, & mission control subsystems
- RASAT (2011): EO payload & attitude sensors
- X-Sat (2011): EO payload
- KOMPSAT-3 (2012): image receiving & processing station, mission control subsystems, & sun sensor
- Gokturk-2 (2012): EO payload
- KOMPSAT-5 (2013): fixed/mobile image receiving & processing station, mission control subsystems, SAR simulator, & sun sensor
- DubaiSat-2 (2013): satellite & ground systems
- Deimos-2 (2013): satellite & ground systems
- KOMPSAT-3A (2015): fixed/mobile image receiving & processing station & sun sensor
- TeLEOS-1 (2015): EO payload
- Velox-C1 (2015)
- KhalifaSat (2018): electric modules, telescope, & image receiving & processing station
- GEO-KOMPSAT-2A/B (2018/2019): electric modules, space weather sensor, image receiving & processing station, & mission control subsystems



Platforms

- **SpaceEye-X.** SpaceEye-X is a satellite bus, which was originally designed to carry very high resolution optical payload (≤0.5 m resolution). SpaceEye-X provides the capability to accommodate various payloads, incl. SAR antennas.
- **SpaceEye-1.** SpaceEye-1 is an improved model & advanced variant of the SI-300 satellite bus, which was the platforms of DubaiSat-2 & DEIMOS-2. Current model is optimized for Earth observation purposes (≤1 m resolution).
- **SpaceEye-W.** SpaceEye-W is a mini-satellite platform (100 ㎏ class). This platform has a very flexible configuration; it can support assorted missions from technical demonstration & science missions to Earth observation missions & telecommunications.

Subsystem-level Products

- **[Electric Propulsion Systems](ps.md).** SI provides Hall‑effect electric propulsion systems (HEPS) optimized for small satellite missions. They provide power processing units, propellant feeding units, & Hall effect thrusters with various power consumption range. HEPS has been acquired flight heritages; DubaiSat-2 & DEIMOS-2 equipped HEPS-200 (200 W power consumption).
- **[Star Tracker](sensor.md).** SI develops & manufactures star trackers that have enormous flight heritages.
- **[S-band Transceiver, X-band Transmitter](comms.md)**
- **Steerable X-band Antenna**
- **Command & Data Handling Unit**
- **[Solid-state Recorder](ds.md)**
- **[Sun Sensor](sensor.md)**



## The End

end of file
