# VEXAG
> 2019.11.08 [🚀](../../index/index.md) [despace](index.md) → **[Events](event.md)**, [Venus](venus.md), [VEXAG](vexag.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Venus Exploration Analysis Group (VEXAG)** — англоязычный термин, не имеющий аналога в русском языке. **Аналитическая группа исследования Венеры (ВЕКСАГ)** — дословный перевод с английского на русский.</small>

**Venus Exploration Analysis Group (VEXAG)** is a Venusian exploration community founded & managed by US.

VEXAG was established by NASA in 2005 (and funded since then) to identify scientific priorities & opportunities for the exploration of [Venus](venus.md), Earth’s sister planet. Possibly the world’s largest & most active group. The VEXAG:

1. composes a bunch of technical & directive documents,
1. monitors ongoing scientific & technical Venusian activities,
1. organize annual meetings,
1. provides findings to NASA Headquarters, but does not make recommendations,

The group has an open membership based on the listserv (<https://lists.psi.edu/mailman/listinfo/venus>) & a semi‑private membership (for US citizens only) — an 7‑person Executive Committee, 3 Focus Groups, & 2 Topical Analysis Groups. Input from the scientific community is actively sought.



## Description
The Venus Exploration Analysis Group is NASA’s community‑based forum designed to provide scientific input & technology development plans for planning & prioritizing the exploration of Venus over the next several decades, including a Venus surface sample return. VEXAG is chartered by NASA’s Solar System Exploration Division & reports its findings to NASA. Open to all interested scientists, VEXAG regularly evaluates Venus exploration goals, scientific objectives, investigations & critical measurement requirements, including especially recommendations in the NRC Decadal Survey & the Solar System Exploration Strategic Roadmap.

In addition to interested members of the scientific community, each focus group will include technology experts, NASA representatives, international partner representatives, EPO experts, & the chair. The current focus groups are listed below. Other focus groups may be added, as needed.

|**Steering Committe Member**|**Position/Roles**|**Tenure**|
|:--|:--|:--|
|[Darby Dyar](person.md) (Mt.Holyoke Coll.)|Chair|12.2018 ‑ 12.2021|
|[Noam Izenberg](person.md) (JHUAPL)|Deputy Chair|12.2018 ‑ 12.2021|
|[Gary Hunter](person.md) (GRC)|Lead, Technology Development<br> & Laboratory Measurements Focus Group|02.2017 ‑ 02.2020|
|[Allan Treiman](person.md) (LPI)|Lead, Venus Goals & Exploration Sites Focus Group|06.2018 ‑ 02.2020|
|[Candace Gray](person.md) (N.Mexico Univ.);<br> [Joseph O'Rourke](person.md) (Arizona Univ.)|Co‑Leads Lead,<br> Early‑Career Scholars Outreach Focus Group|09.2018 ‑ 09.2021|
|[James Cutts](person.md) (JPL)|Venus Exploration Roadmap Focus Group|11.2016 ‑ 11.2019|
|[Kevin McGouldrick](person.md) (LASP)|At‑Large Member|02.2017 ‑ 02.2020|
|[Lynn Carter](person.md) (Arizona Univ.)|At‑Large Member|02.2017 ‑ 02.2020|
|[Robert E. Grimm](person.md) (SWRI)|Past Chair|07.2016 ‑ 12.2018|
|[Colin Wilson](person.md) (Oxford Univ.)|At‑Large Member|07.2018 ‑ 07.2021|
|[Patrick McGovern](person.md) (LPI)|At‑Large Member|07.2018 ‑ 07.2021|
|[Emilie Royer](person.md) (LASP)|At‑Large Member|07.2018 ‑ 07.2021|
|[Giada Arney](person.md) (GFSC)|At‑Large Member|09.2018 ‑ 09.2021|

**Past VEXAG Co-Chairs (in chronological order):**

1. [Janet Luhmann](person.md), University of California, Berkeley, California
1. Sushil Atreya, University of Michigan, Ann Arbor, Michigan
1. [Ellen Stofan](person.md), Proxemy, Inc.
1. [Suzanne Smrekar](person.md), JPL, Pasadena, California
1. [Sanjay Limaye](person.md), University of Wisconsin, Madison, Wisconsin
1. [Lori Glaze](person.md), Goddard Space Flight Center
1. [Bob Grimm](person.md), Southwest Research Institute

Janet Luhmann, & Sushil Atreya, served as VEXAG Co-Chairs since VEXAG was formed in 2005 until November 2009. Ellen Stofan served as VEXAG Chair from November 2007 until spring 2009.

**NASA & LPI contacts:**

1. [Dr. Adriana Ocampo](person.md), NASA Executive Officer for VEXAG
1. [Dr. Allan Treiman](person.md), LPI



## VEXAG reports
1. [Venus Strategic Documents ❐](f/event/vexag/strategic_docs.pdf): Foreward & Acknowledgements, Goals, Objectives, & Investigations for Venus Exploration, Roadmap for Venus Exploration, Venus Technology Plan
1. [VEXAG GOI 2016 ❐](f/event/vexag/vexag_goi_2016.pdf) & [VEXAG GOI 2019 ❐](f/event/vexag/vexag_goi_2019.pdf)



## Meetings
<https://www.lpi.usra.edu/vexag/meetings/archive>

<small>

|**Event**|**Dates**|**Comments**|
|:--|:--|:--|
|[VEXAG 17](vexag_2019.md)|2019.03.20|Venus Town Hall at LPSC, The Woodlands, Texas|
|[VEXAG 16](vexag_2018.md)|2018.11.06 ‑ 08|Applied Physics Laboratory, Laurel, Maryland|
|[VEXAG 15](vexag_2017.md)|2017.11.14 ‑ 16|Applied Physics Laboratory, Laurel Maryland|
|[VEXAG 14](vexag_2016.md)|2016.11.29 ‑ 01|NASA Headquarters, Washington DC|
|[VEXAG 13](vexag_2015.md)|2015.10.27 ‑ 29|NASA Headquarters, Washington DC|
|[VEXAG 12](vexag_2014.md)|2015.04.09|National Institute for Aerospace, Hampton, Virginia|
|[VEXAG 11](vexag_2013.md)|2013.11.19 ‑ 21| |
|[VEXAG 10](vexag_2012.md)|2012.11.13 ‑ 15| |
|[VEXAG 9](vexag_2011.md)|2011.08.31 ‑ 01| |
|[VEXAG 8](vexag_2010.md)|2010.09.02| |
|[VEXAG 7](vexag_2009.md)|2009.10.28 ‑ 29| |
|[VEXAG 6](vexag_2008.md)|2009.02.25| |
|[VEXAG 5](vexag_2007.md)|2008.05.07 ‑ 08| |
|[VEXAG 4](vexag_20.md)|2007.11.04 ‑ 05| |
|[VEXAG 3](vexag_20.md)|2007.01.11 ‑ 12| |
|[VEXAG 2](vexag_20.md)|2006.05.01 ‑ 02| |
|[VEXAG 1](vexag_20.md)|2005.11.04| |

</small>



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Events](event.md)】**<br> **Meetings:** [AGU](agu.md) ~~ [CGMS](cgms.md) ~~ [COSPAR](cospar.md) ~~ [DPS](dps.md) ~~ [EGU](egu.md) ~~ [EPSC](epsc.md) ~~ [FHS](fhs.md) ~~ [IPDW](ipdw.md) ~~ [IVC](ivc.md) ~~ [JpGU](jpgu.md) ~~ [LPSC](lpsc.md) ~~ [MAKS](maks.md) ~~ [MSSS](msss.md) ~~ [NIAC](niac_program.md) ~~ [VEXAG](vexag.md) ~~ [WSI](wsi.md) ┊ ··•·· **Contests:** [Google Lunar X Prize](google_lunar_x_prize.md)|

1. Docs:
   1. [VEXAG GOI 2016 ❐](f/event/vexag/vexag_goi_2016.pdf)
   1. [VEXAG GOI 2019 ❐](f/event/vexag/vexag_goi_2019.pdf)
1. <https://www.lpi.usra.edu/vexag>
1. <https://www.lpi.usra.edu/vexag/reports>


## The End

end of file
