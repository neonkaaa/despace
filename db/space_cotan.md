# SPACE COTAN
> 2021.12.08 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/h/hospo_logo1t.webp)](f/c/h/hospo_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *Memu 183, Taiki-town, Hiroo-gun, Hokkaido, 089-2113, Japan*<br> <https://hokkaidospaceport.com> ~~ [FB ⎆](https://www.facebook.com/hokkaidospaceport) ~~ [X ⎆](https://twitter.com/@hospojapan)|
|:--|:--|
|**Business**|Sales & PR for Hokkaido Spaceport|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO — Yoshinori Odagiri<br> ・COO — Daisuke Ode<br> ・CTO — Yasuyuki Hoshiba<br> ・CMO — Mika Nakagami|

**SPACE COTAN Co., Ltd.** is a Japanese company for sales & PR for [Hokkaido Spaceport (HOSPO)](spaceport.md). Aiming to form a 「space version of Silicon Valley」 centered on Hokkaido Spaceport. Founded 2021.04.

Main Business:

- Design, construction, management, & operation of spaceports such as rocket launch sites & test sites
- Launch support for suborbital rockets, orbital rockets & spaceplanes
- Research, consulting , marketing & PR services related to the aerospace business

<p style="page-break-after:always"> </p>

## The End

end of file
