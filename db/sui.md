# Составная часть
> 2019.04.29 [🚀](../../index/index.md) [despace](index.md) → [SC](sc.md), [КК](sc.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Составная часть (СЧ)** — русскоязычный термин. **Sub-item (SUI)** — англоязычный эквивалент.</small>

**Составная часть** — общее название системы (подсистемы, компонента, блока, модуля и пр.), входящей во что‑то большее.



## Docs/Links (TRANSLATEME ALREADY)
|Navigation|
|:--|
|**[FAQ](faq.md)**【**[SCS](sc.md)**·КК, **[SC (OE+SGM)](sc.md)**·КА】**[CON](contact.md)·[Pers](person.md)**·Контакт, **[Ctrl](control.md)**·Упр., **[Doc](doc.md)**·Док., **[Drawing](draw.md)**·Чертёж, **[EF](ef.md)**·ВВФ, **[Error](faq.md)**·Ошибки, **[Event](event.md)**·Событ., **[FS](fs.md)**·ТЭО, **[HF&E](hfe.md)**·Эрго., **[KT](kt.md)**·КТ, **[N&B](nnb.md)**·БНО, **[Project](project.md)**·Проект, **[QM](qm.md)**·БКНР, **[R&D](rnd.md)**·НИОКР, **[SI](si.md)**·СИ, **[Test](test.md)**·ЭО, **[TRL](trl.md)**·УГТ|
|**Sections & pages**|
|**【[Spacecraft (SC)](sc.md)】**<br> [Cleanliness level](clean_lvl.md) ~~ [Communication SC](sc.md) ~~ [Cubesat](sc.md) ~~ [FSS](sc.md) ~~ [HTS](sc.md) ~~ [Interface](interface.md) ~~ [Manned SC](sc.md) ~~ [Satellite](sc.md) ~~ [Sub-item](sui.md) ~~ [Typical forms](sc.md)|
|**`Космический комплекс (КК):`**<br> [Выводимая масса](throw_weight.md) ~~ [ГО и ПхО](lv.md) ~~ [Класс чистоты](clean_lvl.md) ~~ [Контейнеры для транспортировки](ship_contain.md) ~~ [СЧ](sui.md)|

1. Docs: …
1. <…>


## The End

end of file
