# Meltin
> 2020.07.20 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/m/meltin_logo1t.webp)](f/c/m/meltin_logo1.webp)|<inquiry@metin.jp>, <mark>nophone</mark>, Fax …;<br> *NMF Kayabacho Bldg. 5F, 1-17-24 Shinkawa Chuo-ku, Tokyo 104-0033*<br> <https://www.meltin.jp>・ <https://aerospacebiz.jaxa.jp/en/spacecompany/meltin>|
|:--|:--|
|**Business**|Cyborg business by using bio‑signal & robot technology:<br> ・R&D — Commercialization of medical & welfare equipment<br> ・R&D — Commercialization of avatar robot<br> ・R&D — Commercialization of empowering robots & devices|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**MELTIN MMI** was established in July 2013. With forming initial members, [CEO](mgmt.md) Kasuya & [CTO](mgmt.md) Seki participated SRI International entrepreneur training program in Silicon Valley in 2014. MELTIN MMI is expertise in R&D & commercialization of medical devices & avatar robots.

Our ultimate goal is to realize Cyborg Technology that is well known as artificial body & brain‑machine interface (interface between brain & machine) for unleashing human’s physical limitation & maximizing our creativity for embodying a world where everyone can make their dreams come true.

Main Products:

- [Cyborg](robot.md) (MELTANT Series (MELTANT-α, MELT-ARM, MELT-HAND))
- Medical Equipment, R&D



## The End

end of file
