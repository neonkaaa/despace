# Гравитационный манёвр
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → **[БНО](nnb.md)**, [Control](control.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Гравитационный манёвр** — русскоязычный термин. **Gravity assist / Gravity turn / Zero-lift turn** — англоязычный эквивалент.</small>

**Гравитационный манёвр, гравманёвр** — разгон, замедление или изменение направления полёта космического аппарата под действием гравитационных полей небесных тел. Обычно используется для экономии топлива и дополнительного разгона автоматических межпланетных станций при полётах к дальним планетам Солнечной системы.



## Описание
Наиболее выгодны гравитационные манёвры у планет‑гигантов, но нередко используются манёвры у [Венеры](venus.md), [Земли](earth.md), [Марса](mars.md) и даже [Луны](moon.md). Возможно совмещение с [Эффектом Оберта](oberth_eff.md).

|<small>Гравитационный манёвр<br> для ускорения объекта<br> (гравитационная праща)</small>|<small>Гравитационный манёвр<br> для замедления объекта</small>|
|:--|:--|
|![](f/nav/gravity_assist_swingby_acc_anim.gif)|![](f/nav/gravity_assist_swingby_dec_anim.gif)|



**Примеры использования:**

1. Впервые в мире осуществлён в 1959 году во время полёта КА [Луна‑3](луна‑3.md). Изменение орбиты было рассчитано так, чтобы аппарат при возвращении к Земле снова пролетел над Северным полушарием, где были расположены советские наблюдательные станции.
1. В 1974 году гравитационный манёвр использовал космический аппарат [Маринер‑10](mariner-10.md) — было произведено сближение с [Венерой](venus.md), после которого аппарат направился к [Меркурию](mercury.md).
1. За счёт гравитационных манёвров скорость 「Вояджера‑1」 (≈17 ㎞/s) в марте 2011 года была выше, чем текущая скорость 「Новых горизонтов」 (≈15.9 ㎞/s), хотя после старта с Земли скорость последнего была самой высокой для рукотворных объектов (16.21 ㎞/s).
1. Сложную комбинацию гравитационных манёвров использовали АМС 「[Кассини](cassini_huygens.md)」 (для разгона аппарат использовал гравитационное поле трёх планет — Венеры (дважды), Земли и [Юпитера](jupiter.md)) и 「Розетта」 (четыре гравитационных манёвра около Земли и Марса).



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**`Баллистико‑навигационное обеспечение (БНО):`**<br> [SPICE](spice.md) ~~ [Апоцентр и перицентр](apopericentre.md) ~~ [Гравманёвр](gravass.md) ~~ [Кеплеровы элементы](keplerian.md) ~~ [Космическая скорость](esc_vel.md) ~~ [Сфера Хилла](hill_sphere.md) ~~ [Терминатор](terminator.md) ~~ [Точки Лагранжа](l_points.md) ~~ [Эффект Оберта](oberth_eff.md)|
|**【[Control](Control.md)】**<br> [Ad hoc](ad_hoc.md) ~~ [Business travel](business_travel.md) ~~ [Chief designers council](cocd.md) ~~ [CML](trl.md) ~~ [Competence](competence.md) ~~ [Confident](confident.md) ~~ [Consp.theory](consp_theory.md) ~~ [Control sys. (CS)](cs.md) ~~ [Coordinate system](coord_sys.md) ~~ [Curator](curator.md) ~~ [Designer’s supervision](des_spv.md) ~~ [E‑sig](esig.md) ~~ [Engineer](se.md) ~~ [Errand](errand.md) ~~ [Federal law](fed_law.md) ~~ [Federal TP](fed_tp.md) ~~ [Federal SP](fed_sp.md) ~~ [GNC](gnc.md) ~~ [Gravity assist](gravass.md) ~~ [Industrial archaeology](ind_arch.md) ~~ [Instruction](instruction.md) ~~ [Lean manuf.](lean_man.md) ~~ [Lifetime](lifetime.md) ~~ [Manager](manager.md) ~~ [MBSE](se.md) ~~ [Meeting](meeting.md) ~~ [MCC](sc.md) ~~ [MIC](mic.md) ~~ [MML](mml.md) ~~ [MoU](contract.md) ~~ [Nav. & ballistics (NB)](nnb.md) ~~ [Nonprofit org.](nonprof_org.md) ~~ [NX](nx.md) ~~ [Oberth effect](oberth_eff.md) ~~ [Org.structure](orgstruct.md) ~~ [Outcomes commission](outccom.md) ~~ [Patent](patent.md) ~~ [Peter prin.](peter_principle.md) ~~ [Plan](plan.md) ~~ [PMBok](pmbok.md) ~~ [Quorum](quorum.md) ~~ [R&D management](mgmt.md) ~~ [R&D support](rnd_support.md) ~~ [Recursion](recurs.md) ~~ [Schulze_method](schulze_method.md) ~~ [Sci'N'Tech activities](st_act.md) ~~ [Sci'N'Tech council](satc.md) ~~ [Single-window system](sw_sys.md) ~~ [Situ.leadership](situ_leadership.md) ~~ [Skunk works](se.md) ~~ [State arm. plan](plan_sa.md) ~~ [Swamp](swamp.md) ~~ [Teamcenter](teamcenter.md) ~~ [Tennis racket theorem](tr_theorem.md) ~~ [TRIZ](triz.md) ~~ [TRL](trl.md) ~~ [V‑model](v_model.md) ~~ [Veto](veto.md) ~~ [Workflow](workflow.md) ~~ [Workgroup](wg.md)|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Gravity_assist>


## The End

end of file
