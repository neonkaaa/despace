# Lunar Precursor Robotic Program
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [Луна](moon.md), **[Project](project.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Lunar Precursor Robotic Program (LPRP)** — англоязычный термин, не имеющий аналога в русском языке. **Предварительная программа по изучению Луны автоматами** — дословный перевод с английского на русский.</small>

**Lunar Precursor Robotic Program (LPRP)** — лунная программа НАСА по отправке лунных АМС, которые позволят подготовиться к будущим пилотируемым полётам на Луну. По программе LPRP осуществленно два запуска АМС, первая из которых 23 июня 2009 года достигла Луны:

1. [Lunar Reconnaissance Orbiter](lro.md) (LRO, Лунный КА);
1. [Lunar Crater Observation and Sensing Satellite](lcross.md) (LCROSS, Спутник для наблюдения и измерения лунных кратеров).



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[](.md)】**<br> <mark>NOCAT</mark>|

1. Docs: …
1. <…>


## The End

end of file
