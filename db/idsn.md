# Indian Deep Space Network
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [ISRO](isro.md), **[НС](sc.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Indian Deep Space Network (IDSN)** — англоязычный термин, не имеющий аналога в русском языке. **Индийская сеть дальней космической связи** — дословный перевод с английского на русский.</small>

**Indian Deep Space Network (IDSN)** is a network of large antennas and communication facilities operated by the [Indian Space Research Organisation](isro.md) to support the interplanetary spacecraft missions of India. Its hub is located at Byalalu, a village about 25 ㎞ from Bangalore, India. It was inaugurated on 17 October 2008 by the former ISRO chairman G. Madhavan Nair. The main tracking antenna was designed and commissioned by Hyderabad‑based Electronics Corporation of India Limited at a cost of ₹65 crore (about US$13 million).

| |
|:--|
|[![](f/gs/idsn_pic1t.webp)](f/gs/idsn_pic1.webp)|

The network consists of the ISRO Telemetry, Tracking and Command Network (ISTRAC), augmented by a fully steerable 18 m and a 32 m DSN antenna which improves the visibility duration when compared with the existing ISTRAC system. The Indian Deep Space Network implements a baseband system[clarification needed] adhering to [Consultative Committee for Space Data Systems (CCSDS)](ccsds.md) Standards, thus facilitating cross‑support among the Telemetry Tracking Command (TTC) agencies.

**Telescopes:**

1. 32‑meter DSN Antenna — Deep Space Tracking Antenna. [Bands](comms.md): **S** (♁↗ 🚀↘), **X** (♁↗ 🚀↘).
1. 18‑meter DSN Antenna — Deep Space Tracking Antenna. [Bands](comms.md): <mark>TBD</mark>
1. 11‑meter DSN Antenna — Terminal Tracking Antenna. [Bands](comms.md): <mark>TBD</mark>



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**`Наземная станция (НС):`**<br> … <br><br> [CDSN](cdsn.md) ~~ [DSN](dsn.md) ~~ [ESTRACK](estrack.md) ~~ [IDSN](idsn.md) ~~ [SSC_GGSN](ssc_ggsn.md) ~~ [UDSC](udsc.md)|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Indian_Deep_Space_Network>
1. <http://www.isro.gov.in/about-isro/isro-telemetry-tracking-and-command-network-istrac>



## The End

end of file
