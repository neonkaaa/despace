# Солнечные сутки
> 2019.09.19 [🚀](../../index/index.md) [despace](index.md) → **[Space](index.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Солнечные сутки** — русскоязычный термин. **Solar day** — англоязычный эквивалент.</small>

**Со́лнечные су́тки** — промежуток времени, за который небесное тело совершает 1 поворот вокруг своей оси относительно центра Солнца.

Более строго это промежуток времени между двумя одноимёнными (верхними или нижними) кульминациями (прохождениями через меридиан) центра Солнца в данной точке Земли (или иного небесного тела).



## Планеты
На газовых гигантах, не имеющих твёрдой поверхности, солнечные сутки зависят от широты — атмосфера вращается с разными скоростями на разных широтах.

|**Планета**|**Название<br> солн.суток**|**Длительность**|
|:--|:--|:--|
|[Venus](venus.md)| |243.023 d (5 832 h 33 min 7.2 s)|
|[Земля](earth.md)|Сутки| |
|[Луна](moon.md)| |29 d 12 h 44 min 2.82 с|
|[Марс](mars.md)|Сол|24 h 39 min 35.244 с|
|[Меркурий](mercury.md)| |58.646 d (1 407 h 30 min 14.4 s)|
|[Нептур](neptune.md)| |15 h 57 min 59 с|
|[Плутон](pluto.md)| |6 d 9 h 17 min 36 с|
|[Сатурн](saturn.md)| |10 h 34 min 13 с|
|[Уран](uranus.md)| |17 h 14 min 24 сек|
|[Юпитер](jupiter.md)| |9 h 55 min 40 с|



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Space](index.md)】**<br> [Apparent magnitude](app_mag.md) ~~ [Astro.object](aob.md) ~~ [Blue Marble](earth.md) ~~ [Cosmic rays](ion_rad.md) ~~ [Ecliptic](ecliptic.md) ~~ [Escape velocity](esc_vel.md) ~~ [Health](health.md) ~~ [Hill sphere](hill_sphere.md) ~~ [Information](info.md) ~~ [Lagrangian points](l_points.md) ~~ [Near space](near_space.md) ~~ [Pale Blue Dot](earth.md) ~~ [Parallax](parallax.md) ~~ [Point Nemo](earth.md) ~~ [Silver Snoopy award](silver_snoopy_award.md) ~~ [Solar constant](solar_const.md) ~~ [Terminator](terminator.md) ~~ [Time](time.md) ~~ [Wormhole](wormhole.md) ┊ ··•·· **Solar system:** [Ariel](ariel.md) ~~ [Callisto](callisto.md) ~~ [Ceres](ceres.md) ~~ [Deimos](deimos.md) ~~ [Earth](earth.md) ~~ [Enceladus](enceladus.md) ~~ [Eris](eris.md) ~~ [Europa](europa.md) ~~ [Ganymede](ganymede.md) ~~ [Haumea](haumea.md) ~~ [Iapetus](iapetus.md) ~~ [Io](io.md) ~~ [Jupiter](jupiter.md) ~~ [Makemake](makemake.md) ~~ [Mars](mars.md) ~~ [Mercury](mercury.md) ~~ [Moon](moon.md) ~~ [Neptune](neptune.md) ~~ [Nereid](nereid.md) ~~ [Nibiru](nibiru.md) ~~ [Oberon](oberon.md) ~~ [Phobos](phobos.md) ~~ [Pluto](pluto.md) ~~ [Proteus](proteus.md) ~~ [Rhea](rhea.md) ~~ [Saturn](saturn.md) ~~ [Sedna](sedna.md) ~~ [Solar day](solar_day.md) ~~ [Sun](sun.md) ~~ [Titan](titan.md) ~~ [Titania](titania.md) ~~ [Triton](triton.md) ~~ [Umbriel](umbriel.md) ~~ [Uranus](uranus.md) ~~ [Venus](venus.md)|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Synodic_day>


## The End

end of file
