# GISS
> 2019.08.09 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/n/nasa_logo1t.webp)](f/c/n/nasa_logo1.webp)|<webmaster@giss.nasa.gov>, +1(212)678-55-00, Fax …;<br> *2880 Broadway, New York, NY 10025, USA*<br> <https://www.giss.nasa.gov> ~~ [X ⎆](https://twitter.com/nasagiss) ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/Goddard_Institute_for_Space_Studies)|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

The **Goddard Institute for Space Studies (GISS)** is a laboratory in the Earth Sciences Division of NASA’s [Goddard Space Flight Center](gsfc.md) & a unit of the Columbia University Earth Institute. The institute is located at Columbia University in New York City. Founded in May 1961.

Research at the GISS emphasizes a broad study of Global Change; the natural & anthropogenic changes in our environment that affect the habitability of our planet. These effects may occur on greatly differing time scales, from one-time forcings such as volcanic explosions, to seasonal/annual effects such as El Niño, & on up to the millennia of ice ages.



## The End

end of file
