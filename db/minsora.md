# Minsora
> 2022.06.29 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/m/minsora_logo1t.webp)](f/c/m/minsora_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *302 Yuno Building, 1-3-4 Otemachi, Oita City, Oita Prefecture, 870-0022, Japan*<br> <https://minsora.jp> ~~ [FB ⎆]…) ~~ [IG ⎆]…) ~~ [LI ⎆]…) ~~ [TW ⎆]…) ~~ [Wiki ⎆]…)|
|:--|:--|
|**Business**|Space promotion|
|**Mission**|Changing the structure of the space industry from the region. Making space more fun & interesting|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO — Hisanobu Takayama|

**Minsora Co., Ltd.** is a Japanese company aimed to plan, implement & consult work related to promotion using space. Founded 2019.04.01.

<p style="page-break-after:always"> </p>

## The End

end of file
