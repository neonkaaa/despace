# Search & rescue complex
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [SARC](sarc.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Search & rescue complex (SARC)** — EN term. **Поисково‑спасательный комплекс (ПСК)** — RU analogue.</small>

The **Search & Rescue Complex (SARC)** — a part of [SCS](sc.md) — designed to search for a landing [unit](unit.md), detect, track during landing & evacuate it from the landing area & delivery to a given place.

1. **SARC tasks:**
   1. search, detection, tracking of the landing unit;
   1. evacuation of the unit from the landing area;
   1. delivery of the unit to the specified storage location, ensuring the transportation conditions;
   1. organization of storage of the unit, ensuring the storage conditions, until it is requested by a customer;
   1. providing free air & ground space in the specified landing areas for the unimpeded landing of the unit.
1. **The SARC usually includes:**
   1. ground stations for radio & video tracking;
   1. mobile radio & video tracking equipment;
   1. ground structures;
   1. mobile search & evacuation tools;
   1. landing areas & personnel.



## (RU) Поисково‑спасательный комплекс

**Поисково‑спасательный комплекс (ПСК)** — комплекс средств, предназначенный для поиска приземляющегося [изделия](unit.md), обнаружения, слежения в процессе посадки и эвакуации его из района посадки и доставки в заданное место.

1. **Задачи ПСК:**
   1. поиск, обнаружение, слежение за спускаемым изделием;
   1. эвакуация приземлившегося изделия из района посадки;
   1. доставка изделия в заданное место хранения с обеспечением условий транспортирования;
   1. организация хранения изделия с обеспечением условий хранения до его востребования заказчиком;
   1. обеспечение свободного воздушного и наземного пространства в заданных районах посадки для беспрепятственной посадки изделия.
1. **В состав ПСК обычно входят:**
   1. наземные станции радио‑ и видеонаблюдения;
   1. мобильные средства радио‑ и видеонаблюдения;
   1. наземные постройки;
   1. мобильные средства поиска и эвакуации;
   1. районы посадки и персонал.



## Docs/Links
|**Sections & pages**|
|:--|
|**【[Search & rescue complex (SARC)](sarc.md)】**<br> …|

1. Docs:
   1. [Вводный комплект документов ❐](f/sarc/2018_intro.7z) (2018)
   1. [ПСК КА 「Фобос‑Грунт」 ❐](f/sarc/2011_phobos-grunt.odt) (2011)
1. <…>


## The End

end of file
