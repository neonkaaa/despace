# Tradespace
> 2019.10.19 [🚀](../../index/index.md) [despace](index.md) → **[](.md)** <mark>NOCAT</mark>  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Tradespace** — EN term. **Определение границ/лимитов** — RU analogue.</small>

**Tradespace** has three related meanings:

1. As a multi-variant mathematical playspace used for identifying the optimal boundary spaces (the Pareto frontier) where the multiple variants have strong interdependencies. The term, Tradespace, is a combination of the words 「trade-off」 and 「playspace」, where 「trade-off」 indicates the method of traversing the Tradespace in search of the optimal boundary space (e.g., trading off a cost in one cost center (variant A) for a cost in another cost center (variant B)). Tradespace analysis is used in this context by NASA, DARPA and MIT for analyzing the complex resource, costs and provisioning involved in large projects with multiple stakeholders and multiple objectives.
1. As a collection of processes spanning multiple organizations. 「Tradespace」 in this context is used to distinguish the difference between intra-corporate process management and inter-corporate process management (although it may include (not exclusively) intra-corporate, inter-departmental processes). This type of Tradespace shares business processes and human resources across the system resulting in a collection of business processes spanning corporate or departmental boundaries. The term Tradespace was 1st applied in this context in 2003 by QBOS, Inc. as a way of signifying the relevance of the term in its above context to the search for equilibria (the Pareto frontier) in a collection of processes spanning multiple organizations where those organizations each have their own seven-sigma core objectives. The relevance of the term as a collection of inter-corporate processes to its meaning as a multi-variant interdependent playspace becomes very apparent when observing organizations working together in a Tradespace via processes put in place between the organizations. Their at-1st-glance independent objectives take on an interdependency similar to the multi-variant models in the mathematical use of the term. Thus, the very act of creating inter-corporate processes creates an interdependency that must be explored and managed in order to reach an equilibrium state that is stable, resilient and maximizes opportunity and ROI for the participant organizations (see Tradespace Management System). Multiple Tradespaces of this type may aggregate to form larger Tradespaces of this type.
1. In a financial sense as an environment for supporting inter-corporate financial exchange transactions. This form of the term was instantiated by NextSet Software, Inc. in 2000.



## Description
Multiple Tradespaces of context type 2 or 3, above, may aggregate to form larger Tradespaces of the same type. Merging Tradespaces of both types (2 and 3) results in an eco-system. Distinctions between Tradespaces and eco-systems include:

1. Tradespaces can give rise to strong eco-systems but not the other way around and
1. By focusing on either the analysis of inter-corporate processes (context 2 above) or the analysis of inter-corporate funds exchange (context 3 above), Tradespaces form natural boundary interfaces matching the cash flow / provisioning interface and thus protect against the anomalies that can arise in a poorly designed cash-flow/provisioning interface. Eco-systems that are not designed on a Tradespace foundation model 1st, run the risk of dissolution due to anomalies created by the cash flow / provisioning interface.

The term Tradespace has the following trademark registrations against it:

1. By QBOS, Inc.
   1. Serial Number: 77453425 (Filed: Apr 21, 2008)
   1. Registration Number: 3769751 (Apr 6, 2010)
   1. Goods and Services Description (Intl Class 042): 「Facilitating the exchange of needed information to provision of business goods and services via the Internet」
1. By NextSet Software Inc.
   1. Serial Number: 76119221 (Filed: Aug 30, 2000)
   1. Registration Number: 2714085 (May 6, 2003)
   1. Goods and Services Description (Intl Class 009): 「Computer e-commerce software which enables users to participate in electronic exchange transactions and auctions, via a global computer network」
   1. Goods and Services Description (Intl Class 042): 「Computer software development services for others in the field of electronic business exchange transactions and auctions」



<br><br><br>



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[](.md)】**<br> <mark>NOCAT</mark>|

**Documents:**

1. …

**Links:**

1. <https://en.wikipedia.org/wiki/Tradespace>


## The End

end of file
