# Космическая скорость
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → **[БНО](nnb.md)**, [Space](index.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Космическая скорость** — русскоязычный термин. **Escape velocity** — англоязычный эквивалент.</small>

**Космическая скорость** (первая v₁, вторая v₂, третья v₃ и четвёртая v₄) — это минимальная скорость, при которой какое‑либо тело в свободном движении с поверхности небесного тела сможет:

1. v₁ — круговая скорость — стать спутником небесного тела (то есть вращаться по круговой орбите вокруг небесного тела на нулевой или пренебрежимо малой высоте относительно поверхности);
1. v₂ — параболическая скорость, скорость убегания — преодолеть гравитационное притяжение небесного тела и уйти на бесконечность;
1. v₃ — покинуть звёздную систему, преодолев притяжение звезды;
1. v₄ — покинуть галактику.

Третья и четвёртая космические скорости используются довольно редко. Вторая космическая скорость обычно определяется в предположении отсутствия каких‑либо других небесных тел (например, для Луны скорость убегания равна 2.4 ㎞/s, несмотря на то, что в действительности для удаления тела на бесконечность с поверхности Луны необходимо преодолеть притяжение Земли, Солнца и Галактики). Ещё реже в некоторых источниках встречается понятие 「пятая космическая скорость」. Это скорость, позволяющая добраться до иной планеты звёздной системы вне зависимости от разности плоскостей эклиптики планет. Например, для Солнечной системы и, конкретно, для Земли, чтобы орбита межпланетного перелета была перпендикулярной к земной орбите, нужна скорость запуска 43.6 километра в секунду.

|**Небесное тело**|**Масса <small>(по отн. к M Земли)</small>**|**v₁, ㎞/s**|**v₂, ㎞/s**|**v₃, ㎞/s**|**v₄, ㎞/s**|
|:--|:--|:--|:--|:--|:--|
|[Луна](moon.md)|0.0123|1.680|2.375| | |
|[Меркурий](mercury.md)|0.055|3.05|4.3| | |
|[Марс](mars.md)|0.108|3.546|5.0| | |
|[Venus](venus.md)|0.82|7.356|10.22| | |
|[Земля](earth.md)|1|7.91|11.182|16.65| |
|[Уран](uranus.md)|14.5|15.6|21.3| | |
|[Нептун](neptune.md)|17.5|16.7|23.5| | |
|[Сатурн](saturn.md)|95.3|25|36.0| | |
|[Юпитер](jupiter.md)|318.3|43|61.0| | |
|[Солнце](sun.md)|333 000|437|617.7| | |
|[Сириус B](sirius.md)|325 675| |10 000| | |
|Нейтронная звезда|666 000| |200 000| | |
|Чёрная дыра|832 500 ‑ 5.6·10¹⁵| |\> 299 792| | |



## Вычисление
Между первой и второй космическими скоростями существует простое соотношение:  
**v₂** = √2 · v₁

Квадрат круговой скорости **(первой космической скорости)** с точностью до знака равен ньютоновскому потенциалу Φ на поверхности небесного тела (при выборе нулевого потенциала на бесконечности):  
**v₁²** = −Φ = G·M/R  
где M — масса планеты, R — радиус небесного тела, G — гравитационная постоянная.

Квадрат скорости убегания **(второй космической скорости)** равен удвоенному ньютоновскому потенциалу, взятому с обратным знаком:  
**v₂²** = −2Φ = 2·G·M/R

**Третья космическая скорость:**  
**v₃** = √( (√2−1)²·v₁² + v₂² )



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**`Баллистико‑навигационное обеспечение (БНО):`**<br> [SPICE](spice.md) ~~ [Апоцентр и перицентр](apopericentre.md) ~~ [Гравманёвр](gravass.md) ~~ [Кеплеровы элементы](keplerian.md) ~~ [Космическая скорость](esc_vel.md) ~~ [Сфера Хилла](hill_sphere.md) ~~ [Терминатор](terminator.md) ~~ [Точки Лагранжа](l_points.md) ~~ [Эффект Оберта](oberth_eff.md)|
|**【[Space](index.md)】**<br> [Apparent magnitude](app_mag.md) ~~ [Astro.object](aob.md) ~~ [Blue Marble](earth.md) ~~ [Cosmic rays](ion_rad.md) ~~ [Ecliptic](ecliptic.md) ~~ [Escape velocity](esc_vel.md) ~~ [Health](health.md) ~~ [Hill sphere](hill_sphere.md) ~~ [Information](info.md) ~~ [Lagrangian points](l_points.md) ~~ [Near space](near_space.md) ~~ [Pale Blue Dot](earth.md) ~~ [Parallax](parallax.md) ~~ [Point Nemo](earth.md) ~~ [Silver Snoopy award](silver_snoopy_award.md) ~~ [Solar constant](solar_const.md) ~~ [Terminator](terminator.md) ~~ [Time](time.md) ~~ [Wormhole](wormhole.md) ┊ ··•·· **Solar system:** [Ariel](ariel.md) ~~ [Callisto](callisto.md) ~~ [Ceres](ceres.md) ~~ [Deimos](deimos.md) ~~ [Earth](earth.md) ~~ [Enceladus](enceladus.md) ~~ [Eris](eris.md) ~~ [Europa](europa.md) ~~ [Ganymede](ganymede.md) ~~ [Haumea](haumea.md) ~~ [Iapetus](iapetus.md) ~~ [Io](io.md) ~~ [Jupiter](jupiter.md) ~~ [Makemake](makemake.md) ~~ [Mars](mars.md) ~~ [Mercury](mercury.md) ~~ [Moon](moon.md) ~~ [Neptune](neptune.md) ~~ [Nereid](nereid.md) ~~ [Nibiru](nibiru.md) ~~ [Oberon](oberon.md) ~~ [Phobos](phobos.md) ~~ [Pluto](pluto.md) ~~ [Proteus](proteus.md) ~~ [Rhea](rhea.md) ~~ [Saturn](saturn.md) ~~ [Sedna](sedna.md) ~~ [Solar day](solar_day.md) ~~ [Sun](sun.md) ~~ [Titan](titan.md) ~~ [Titania](titania.md) ~~ [Triton](triton.md) ~~ [Umbriel](umbriel.md) ~~ [Uranus](uranus.md) ~~ [Venus](venus.md)|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Escape_velocity>


## The End

end of file
