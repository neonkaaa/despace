# Крейцкопф
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [SGM](sc.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Крейцкопф** — русскоязычный термин. **Crosshead** — англоязычный эквивалент.</small>

**Крейцкопф** (нем. *Kreuzkopf*), **ползун** — деталь кривошипно‑ползунного механизма, совершающая возвратно‑поступательное движение по неподвижным направляющим.



## Назначение
Крейцкопф предназначен для соединения поршня и шатуна в крейцкопфном кривошипно‑шатунном механизме. При таком сочленении поршень жёстко связан с крейцкопфом с помощью штокa. Такое сочленение позволяет разгрузить поршень от нормальной силы, так как её действие в таком случае переносится на крейцкопф. Такая схема соединения позволяет создать вторую рабочую полость в цилиндре под поршнем. При этом шток проходит через уплотнение (сальник) в нижней крышке цилиндра, который обеспечивает необходимую герметичность.

| | |
|:--|:--|
|[![](f/sgm/kreuzkopf-01t.webp)](f/sgm/kreuzkopf-01.webp)|[![](f/sgm/kreuzkopf-02t.webp)](f/sgm/kreuzkopf-02.webp)|



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Structures, gears, materials (SGM)](sc.md)】**<br> [Гермоконтейнер](гермоконтейнер.md) ~~ [Датчик](sensor.md) ~~ [Задел](margin.md) ~~ [Изделие](unit.md) ~~ [Испарение материалов](matc.md) ~~ [Кавитация](cavitation.md) ~~ [КЗУ](cinu.md) (ВБУ КТ) ~~ [КХГ](cgs.md) ~~ [Контейнеры для транспортировки](ship_contain.md) ~~ [Крейцкопф](crosshead.md) ~~ [Номинал](nominal.md) ~~ [ПУС](lag.md) ~~ [ПНА, ПОНА, ПСНА](devd.md) ~~ [Резерв](reserve.md) ~~ [Слайс](слайс.md) ~~ [ТСП](tsp.md) ~~ [Типичные формы КА](sc.md) ~~ [Толкатель](толкатель.md) ~~ [Унификация](commonality.md)|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Crosshead>


## The End

end of file
