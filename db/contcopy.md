# Контрольный экземпляр документа
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → **[НД](doc.md#НД)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Контрольный экземпляр документа (КЭД)** — русскоязычный термин. **Controlled copy** — англоязычный эквивалент.</small>

**Контрольный экземпляр документа (КЭД)** — согласованный и утверждённый экземпляр [документа](doc.md), оформленный подлинными подписями, печатью организации, хранящийся в специально отведённом месте.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[](.md)】**<br> <mark>NOCAT</mark>|

1. Docs: …
1. <…>


## The End

end of file
