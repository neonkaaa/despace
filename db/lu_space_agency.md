# Luxembourg Space Agency
> 2022.03.30 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/l/lu_space_agency_logo1t.webp)](f/c/l/lu_space_agency_logo1.webp)|<info@space-agency.lu>, +352-288-482-10, Fax …;<br> *19-21, boulevard Royal, L-2449 Luxembourg*<br> <http://space-agency.public.lu> ~~ [FB ⎆](https://www.facebook.com/LuxembourgSpaceAgency) ~~ [IG ⎆](https://www.instagram.com/luxspaceagency/?igshid=t4ls3tg9cju1) ~~ [LI ⎆](https://www.linkedin.com/company/luxembourg-space-agency) ~~ [X ⎆](https://twitter.com/luxspaceagency) ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/Luxembourg_Space_Agency)|
|:--|:--|
|**Business**|Luxembourg Space Agency|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO — Marc Serres|

The **Luxembourg Space Agency (LSA)** is the national space agency of the Grand Duchy of Luxembourg. It was founded on September 12, 2018, by Luxembourg's Economy Minister Étienne Schneider.

The goal of the Luxembourg Space Agency is to use Luxembourg's state funds to provide private companies, start‑ups & organizations in the field of space exploration with financial support, especially those working on asteroid mining.

<p style="page-break-after:always"> </p>

## The End

end of file
