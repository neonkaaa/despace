# Maverick Space Systems
> 2022.04.07 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/m/maverick_ss_logo1t.webp)](f/c/m/maverick_ss_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *755 Fiero Lane, Suite E, San Luis Obispo, CA 93401, US*<br> <https://www.maverickspace.com> ~~ [LI ⎆](https://www.linkedin.com/company/maverick-space-systems)|
|:--|:--|
|**Business**|Sat deployment hardware, integration & launch brokering services|
|**Mission**|…|
|**Vision**|…|
|**Values**|**Credible** — from government to commercial, stateside or overseas, our team has the experience to get your mission integrated & on orbit. **Dedicated** — our customer’s satisfaction & their mission success is what drives us. **Responsive** — we tackle customized solutions every day & pick up the phone when you call.|
|**[MGMT](mgmt.md)**|・CEO — Roland Coelho<br> ・COO — Vidur Kaushish<br> ・CTO — Austin Kruggel|

**Maverick Space Systems, Inc.** offers satellite deployment hardware as well as integration & launch brokering services. Founded 2019.02.

- **Mission Engineering:**
   - **Mission Design** — perform & advise satellite developers on satellite mission design, incl. technical requirements, programmatic, regulatory, & financial planning for Nano to Micro class satellites.
   - **Hardware Development** — design & fabricate custom hardware solutions for launch adaptation structures, customized separation systems, & ground support equipment.
   - **Analysis & Testing** — perform satellite & launch vehicle analyses & testing, incl. structural, thermal, EMI/EMC compatibility, safety hazard, & launch environments.
- **Launch:**
   - **Launch Brokering** — broker launches with all major US & international LV providers to get your satellite to your preferred orbit, from LEO, GTO, GEO, & Interplanetary.
   - **Mission Management** — manage satellite development & launch campaigns for university programs, unique government missions, & commercial constellations.
   - **Launch Integration** — perform all required tasks to launch your satellite, incl. launch adapter hardware, interface control documents, requirement verifications, regulatory approvals, range safety, satellite to launch vehicle integration, & launch site support.

<p style="page-break-after:always"> </p>

## The End

end of file
