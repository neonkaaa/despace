# Некоммерческая организация
> 2019.08.27 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md), **[Control](control.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Некоммерческая организация** — русскоязычный термин. **Nonprofit organization** — англоязычный эквивалент.</small>

**Некомме́рческая организа́ция (НКО)** — организация, не имеющая в качестве основной цели своей деятельности извлечение прибыли и не распределяющая полученную прибыль между участниками. Некоммерческие организации могут создаваться для достижения социальных, благотворительных, культурных, образовательных, политических, научных и управленческих целей, в сферах охраны здоровья граждан, развития физической культуры и спорта, удовлетворения духовных и иных нематериальных потребностей граждан, защиты прав, законных интересов граждан и организаций, разрешения споров и конфликтов, оказания юридической помощи, а также в иных целях, направленных на достижение общественных благ. Некоммерческие организации вправе заниматься предпринимательской деятельностью, только если данная деятельность направлена на достижение целей организации. Фонд Викимедиа, осуществляющий поддержку инфраструктуры Википедии, относится именно к некоммерческим организациям.



## Виды некоммерческих организаций

1. Автономная некоммерческая организация
1. Университет
1. Адвокатское образование (коллегия адвокатов, адвокатское бюро и юридическая консультация)
1. Ассоциация и союз (в том числе биржевой)
1. Благотворительная организация
1. Гаражно‑строительный кооператив
1. Государственная корпорация
1. Государственная компания
1. Государственное и муниципальное автономное, бюджетное и казённое учреждение
1. Национальный парк, природный парк, государственный природный заповедник
1. Казачье общество
1. Жилищно‑строительный кооператив
1. Некоммерческое партнерство
1. Неправительственная организация
1. Кондоминиум, ТСЖ, ЖК (Жилищный кооператив), ГК
1. Общественное объединение (Политическая партия, общественная организация (в том числе инвалидов), общественное движение, общественный фонд, общественное учреждение, орган общественной самодеятельности, Профсоюз)
1. Общество взаимного страхования
1. Объединения юридических лиц
1. Объединение работодателей
1. Община коренных малочисленных народов
1. Потребительские кооперативы (в том числе Кредитный потребительский кооператив (граждан, первого и второго уровня), Сельскохозяйственный потребительский кооператив (перерабатывающий, сбытовой (торговый), обслуживающий, снабженческий, садоводческий, огороднический, животноводческий), Жилищный накопительный кооператив
1. Религиозная организация /объединение (религиозная организация (местная и централизованная), религиозная группа)
1. Садоводческое, огородническое или дачное некоммерческое объединение
1. Торгово‑промышленная палата
1. Территориальное общественное самоуправление.
1. Учреждение (в том числе частное)
   1. Автономное учреждение
1. Фонд



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Control](Control.md)】**<br> [Ad hoc](ad_hoc.md) ~~ [Business travel](business_travel.md) ~~ [Chief designers council](cocd.md) ~~ [CML](trl.md) ~~ [Competence](competence.md) ~~ [Confident](confident.md) ~~ [Consp.theory](consp_theory.md) ~~ [Control sys. (CS)](cs.md) ~~ [Coordinate system](coord_sys.md) ~~ [Curator](curator.md) ~~ [Designer’s supervision](des_spv.md) ~~ [E‑sig](esig.md) ~~ [Engineer](se.md) ~~ [Errand](errand.md) ~~ [Federal law](fed_law.md) ~~ [Federal TP](fed_tp.md) ~~ [Federal SP](fed_sp.md) ~~ [GNC](gnc.md) ~~ [Gravity assist](gravass.md) ~~ [Industrial archaeology](ind_arch.md) ~~ [Instruction](instruction.md) ~~ [Lean manuf.](lean_man.md) ~~ [Lifetime](lifetime.md) ~~ [Manager](manager.md) ~~ [MBSE](se.md) ~~ [Meeting](meeting.md) ~~ [MCC](sc.md) ~~ [MIC](mic.md) ~~ [MML](mml.md) ~~ [MoU](contract.md) ~~ [Nav. & ballistics (NB)](nnb.md) ~~ [Nonprofit org.](nonprof_org.md) ~~ [NX](nx.md) ~~ [Oberth effect](oberth_eff.md) ~~ [Org.structure](orgstruct.md) ~~ [Outcomes commission](outccom.md) ~~ [Patent](patent.md) ~~ [Peter prin.](peter_principle.md) ~~ [Plan](plan.md) ~~ [PMBok](pmbok.md) ~~ [Quorum](quorum.md) ~~ [R&D management](mgmt.md) ~~ [R&D support](rnd_support.md) ~~ [Recursion](recurs.md) ~~ [Schulze_method](schulze_method.md) ~~ [Sci'N'Tech activities](st_act.md) ~~ [Sci'N'Tech council](satc.md) ~~ [Single-window system](sw_sys.md) ~~ [Situ.leadership](situ_leadership.md) ~~ [Skunk works](se.md) ~~ [State arm. plan](plan_sa.md) ~~ [Swamp](swamp.md) ~~ [Teamcenter](teamcenter.md) ~~ [Tennis racket theorem](tr_theorem.md) ~~ [TRIZ](triz.md) ~~ [TRL](trl.md) ~~ [V‑model](v_model.md) ~~ [Veto](veto.md) ~~ [Workflow](workflow.md) ~~ [Workgroup](wg.md)|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Nonprofit_organization>



## The End

end of file
