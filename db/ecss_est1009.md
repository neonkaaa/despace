# ecss_est1009

[TOC]

---


## Annex A (normative) Coordinate Systems Document (CSD) — DRD

**A.1 DRD identification.** *Requirement ID & source* — ECSS‑E‑ST‑10‑09, requirement 5.2.2a. *Purpose & objective* — proper documentation & maintenance of coordinate systems & their inter‑relationships, throughout the product tree & the project/mission life, shall be initiated with sufficient detail. Standard requirements for the document & related database would help to regularise conventions even earlier.

**A.2 Expected response**

*Special remarks.* The CSD **can be part of the SEP** (as per ECSS‑E‑ST‑10).

*Scope & content:*

1. **Introduction.** The purpose, objective & the reason prompting its preparation (e.g. programme, project & phase).
1. **Applicable & reference documents.** The applicable & reference documents supporting the generation of the document.
1. **Convention & Notation**
   1. Specify the international conventions used within the project for coordinate systems & transformations.
   1. Define the naming, notation & acronym rules for coordinate systems, as well as for transformations.
   1. Define notation conventions for the transfer of coordinates & transformation data.
1. **Units.** Specify the units pertaining to the coordinate systems & parameterisations used within the project.
1. **Time standards.** Specify the time standards used within the project & the relationship between them.
1. **Coordinate system, overview.** The overview of the various coordinate systems. Refer to the theory & conventions applied in between the various coordinate systems (e.g. precession, nutation, polar motion). Include a brief description of at least the following coordinate systems, if applicable:
   1. Inertial Coordinate Systems
   1. Orbital Coordinate Systems
   1. Launcher Coordinate Systems
   1. Satellite‑fixed Coordinate System (generic platform & payload)
   1. Body‑fixed Rotation (planet) Coordinate Systems
   1. Topocentric Coordinate Systems
   1. Test facility Coordinate Systems
   1. Simulator Coordinate Systems
   1. Processing/Product Coordinate Systems
1. **Parameterisations.** Describe all parameterisations used within a coordinate system (e.g. azimuth, elevation), including the applicable type of coordinate systems where this parameterisation is allowed. Describe all parameterisations used within a transformation (e.g. quaternions, Euler angles), including the applicable type of coordinate systems where this parameterisation is allowed.
1. **Diagrams.**
   1. Describe in a graphical representation the top‑level chain of transformations for the project.
   1. The diagram (previous sentence) shall include any additional constraints between coordinate systems, as well as measurements.
   1. Describe in a graphical representation the lower level chains of transformations.
   1. Each individual coordinate system, identified in <9> below, shall be  graphically represented.
   1. These diagrams shall contain any additional constraints between coordinate systems, as well as measurements.
1. **Coordinate systems, details**
   1. Give the detailed description of all Coordinate Systems used in the project.
   1. The transformation between coordinate systems shall be defined in the one that is the parent coordinate system.
   1. Parameterisations within a coordinate system shall be identified & specified (e.g. right ascension & declination).
   1. The parameterisation of a transformation shall be identified & specified (e.g. quaternions).
   1. Any source document for a coordinate system shall be identified in the CSD.
