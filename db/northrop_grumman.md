# Northrop Grumman
> 2019.08.05 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/n/northrop_grumman_logo1t.webp)](f/c/n/northrop_grumman_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax +1(703)280-29-00;<br> *2980 Fairview ㎩rk Drive, West Falls Church, Virginia, United States*<br> <http://www.northropgrumman.com> ~~ [Wiki 1 ⎆](https://en.wikipedia.org/wiki/Northrop_Grumman) ~~ [Wiki 2 ⎆](https://en.wikipedia.org/wiki/Grumman_LLV)|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Northrop Grumman Corporation** — американская военно‑промышленная компания, работающая в области электроники и информационных технологий, авиакосмической отрасли, судостроении. Образована в 1994 году в результате слияния компаний 「Northrop Corporation」 и 「Grumman Corporation」. Деятельность компании сведена в основные группы:

1. Information & Services, с производственными секторами:
   - Информационные технологии
   - Mission Systems
   - Technical Services
1. Электроника
1. Авиация и космос
1. Судостроение

| | |
|:--|:--|
|[![](f/c/n/northrop_grumman_usaf_b_2_spiritt.webp)](f/c/n/northrop_grumman_usaf_b_2_spirit.webp)|[![](f/c/n/northrop_grumman_small_usps_truckt.webp)](f/c/n/northrop_grumman_small_usps_truck.webp)|
|Northrop Grumman manufactured<br> the B-2 Spirit strategic bomber.|Grumman Long Life Vehicle (LLV)|



## The End

end of file
