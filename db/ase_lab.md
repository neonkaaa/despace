# ASE-Lab
> 2022.04.18 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/a/ase_lab_logo1t.webp)](f/c/a/ase_lab_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *…*<br> <https://ase.cmd-moon.space> ~~ [X ⎆](https://twitter.com/ASE_lab_)|
|:--|:--|
|**Business**|Students community|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**ASE-Lab** — Aerospace Science & Engineering Laboratory — Is a community where students who are interested in the space field hold study sessions. Our activity style is the following 3 steps.

- Members decide the theme & plan a study session.
- Members who are interested in the same theme participate in the study session.
- Deepen learning among study group members.

Currently, study groups are planned to work on teaching materials related to space science & space engineering, study groups related to mathematics & physics, which are the basis of the study groups, & study groups such as liberal arts that allow you to participate more comfortably.

At ASE-Lab, there are science students, engineering students, & liberal arts students. If you want to deepen your learning about space, your title doesn't matter. Students who continue to face & challenge the study of space gather & study sessions organized by the members will become the academic foundation of young human resources. ASE-Lab continues to aim for a place for that.

<p style="page-break-after:always"> </p>

## The End

end of file
