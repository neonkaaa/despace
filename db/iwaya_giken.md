# Iwaya Giken
> 2022.06.29 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/i/iwaya_giken_logo1t.webp)](f/c/i/iwaya_giken_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *4-chōme-1-30 Kita 16 Jōnishi, Kita Ward, Sapporo, Hokkaido 001-0016, Japan*<br> <https://iwaya.biz> ~~ [FB ⎆](https://www.facebook.com/keisuke.iwaya.9) ~~ [IG ⎆](https://www.instagram.com/iwayagram) ~~ [X ⎆](https://twitter.com/iwayatweet)|
|:--|:--|
|**Business**|Develops & sells space video material, children edu|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Iwaya GIken** develops & sells space video material, as well as assisting & lecturing on educational issues for children. Founded 2016.04.

<p style="page-break-after:always"> </p>

## The End

end of file
