# Космический мусор
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [EF](ef.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Космический мусор (КМ)** — русскоязычный термин. **Space debris (SDEB)** — англоязычный эквивалент.</small>

**Космический мусор (КМ)** — все находящиеся на околоземной орбите космические объекты искусственного происхождения (включая фрагменты или части таких объектов), которые закончили своё активное существование. В некоторых случаях, крупные или содержащие на борту опасные (ядерные, токсичные и т. п.) материалы объекты космического мусора могут представлять прямую опасность и для [Земли](earth.md) — при их неконтролируемом сходе с орбиты, неполном сгорании при прохождении плотных слоёв атмосферы Земли и выпадении обломков на населённые пункты, промышленные объекты, транспортные коммуникации и т.п.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[External factors (EF)](ef.md)】**<br> [Astro.object](aob.md) ~~ [Atmosphere](atmosphere.md) ~~ [Atmosphere of Earth](earth.md) ~~ [Cosmic rays](ion_rad.md) ~~ [EMI](emi.md) ~~ [Grav.waves](gravwave.md) ~~ [Ion.radiation](ion_rad.md) ~~ [Radio frequency](comms.md) ~~ [Solar phenomena](solar_ph.md) ~~ [Space debris](sdeb.md) ~~ [Standart conditions](sctp.md) ~~ [Time](time.md) ~~ [VA radiation belts](ion_rad.md)|

1. Docs:
   1. [ГОСТ 53802](гост_53802.md)
1. <https://en.wikipedia.org/wiki/Space_debris>



## The End

end of file
