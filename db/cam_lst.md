# Camera (a list)
> 2019.12.16 [🚀](../../index/index.md) [despace](index.md) → [Camera](cam.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

A list of [Cameras](cam.md).



## Current



### IKI RAS ВТК
> <small>**Внешняя телевизионная камера (ВТК)** — русскоязычный термин, не имеющий аналога в английском языке. **Outer television camera (VTK)** — дословный перевод с русского на английский.</small>

**Внешняя телевизионная камера (ВТК)** — оптическая камера, предназначенная для фото‑видеосъёмки. Разработчик [ИКИ РАН](ики_ран.md). Разработано в <mark>TBD</mark> году.

|**Characteristics**|**(ВТК-10)**|**(ВТК-50)**|
|:--|:--|:--|
|Composition| | |
|Consumption, W|5|5|
|Dimensions, ㎜|117 × 90 × 116|117 × 90 × 113|
|[Interfaces](interface.md)|HD-SDI|HD-SDI|
|[Lifetime](lifetime.md), h(y)| | |
|Mass, ㎏|0.7|0.7|
|[Overload](vibration.md), Grms| | |
|[Radiation](ion_rad.md), ㏉(㎭)| | |
|[Reliability](qm.md)|… / …|… / …|
|[Thermal](tcs.md), ℃| | |
|[TRL](trl.md)|6|6|
|[Voltage](sps.md), V|27|27|
|**【Specific】**|~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |
|Back.bright., ㏅/m²| | |
|[FOV](fov.md), °|17 × 9.6|76 × 47.45|
|Frequency, fps|10 ‑ 25|10 ‑ 25|
|Lens|КМОП-матрица|КМОП-матрица|
|Resolution, px|1280 × 720|1280 × 720|
|Shooting modes| | |
| |[![](f/cam/v/vtk_1050_pic1t.webp)](f/cam/v/vtk_1050_pic1.webp)|[![](f/cam/v/vtk_1050_pic1t.webp)](f/cam/v/vtk_1050_pic1.webp)|

**Notes:**

1. <http://ofo.ikiweb.ru/razrabotki/VTK.html>
1. <http://ofo.ikiweb.ru/razrabotki/digital-sputnik-cams.html>
1. ВТК ставится на внешнюю поверхность ПТК 「Федерация」 для контроля сближения с другими КА и для обзора окружающего пространства. Оснащена двумя идентичными независимыми камерами: узкоугольная ВТК‑10 и широкоугольная ВТК‑50. Каждая позволяет получать цифровую видеоинформацию в видимом и ближнем ИК диапазонах с частотой 25 кадров/с. Цифровой поток видеоданных передается в КА с целью отображения на дисплее пилота ПТК или дальнейшей трансляции в системы КА. Конструктивно каждая камера представляет собой негерметичный моноблок, который состоит из видеоэлектронного блока и объектива. В ВТК‑10 используется узкоугольный объектив с фокусным расстоянием 23 ㎜, в ВТК‑50 — широкоугольный объектив с фокусным расстоянием 4.5 ㎜. Видеоэлектронный блок содержит источник вторичного питания и модуль электроники. Имеется встроенный процессор, обеспечивающий обработку видеопотока и автоматическую регулировку экспозиции, высокоскоростного интерфейса HD‑SDI по стандарту SMPTE ST 292‑1 и вторичного источника питания, обеспечивающего работу от бортовой сети +27В.
1. **Applicability:** ПТК 「Федерация」



### IKI RAS ЛСТК
> <small>**Лунная стереотопографическая камера (ЛСТК)** — русскоязычный термин, не имеющий аналога в английском языке. **Lunar stereotopographic camera (LSTK)** — дословный перевод с русского на английский.</small>

**Лунная стереотопографическая камера (ЛСТК)** — оптическая камера, предназначенная для фото‑видеосъёмки. Разработчик [ИКИ РАН](iki_ras.md). Разработано в <mark>TBD</mark> году разработка (на основе [ТСНН УТК и [ТСНН ШТК)

|**Characteristics**|**(ЛСТК)**|
|:--|:--|
|Composition| |
|Consumption, W|21|
|Dimensions, ㎜|ОБ 285 × 340 × 166, БЭ 254 × 80 × 150|
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|26 280 (3) / 10 000 (1.14)|
|Mass, ㎏|8 (6 ОБ, 2 БЭ)|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)|200 (20 000)|
|[Reliability](qm.md)|0.96|
|[Thermal](tcs.md), ℃|−20 ‑ +50|
|[TRL](trl.md)|4|
|[Voltage](sps.md), V|27|
|**【Specific】**|~ ~ ~ ~ ~ |
|Back.bright., ㏅/m²| |
|[FOV](fov.md), °|14.6 × 11|
|Frequency, fps|0.3|
|Lens| |
|Resolution, px|5120 × 3840 × 2|
|Shooting modes|фотосъёмка|
| | [![](f/cam/i/iki_ras_lstk_pic1t.webp)](f/cam/i/iki_ras_lstk_pic1.webp) [![](f/cam/i/iki_ras_lstk_pic2t.webp)](f/cam/i/iki_ras_lstk_pic2.webp) [![](f/cam/i/iki_ras_lstk_pic3t.webp)](f/cam/i/iki_ras_lstk_pic3.webp)|



### IKI RAS МСУ-50
> <small>**Многозональное сканирующее устройство (МСУ-50)** — русскоязычный термин, не имеющий аналога в английском языке. **Multizones scanning device (MSU-50)** — дословный перевод с русского на английский.</small>

**Многозональное сканирующее устройство (МСУ-50)** — оптическая камера, предназначенная для фото‑видеосъёмки. Разработчик [ИКИ РАН](iki_ras.md). Разработано в 2008 году активное применение

|**Characteristics**|**(МСУ-50)**|
|:--|:--|
|Composition| |
|Consumption, W|7|
|Dimensions, ㎜| |
|[Interfaces](interface.md)|[MIL-STD-1553](mil_std_1553.md)|
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|2.5|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|Back.bright., ㏅/m²| |
|[FOV](fov.md), °|58.5|
|Frequency, fps| |
|Lens| |
|Resolution, px|1 × 7926|
|Shooting modes| |
| | [![](f/cam/i/iki_ras_msu_50_pic1t.webp)](f/cam/i/iki_ras_msu_50_pic1.webp) |

**Notes:**

1. <http://ofo.ikiweb.ru/msu.php>
1. **Applicability:** Метеор‑М №1



### IKI RAS МСУ-100
> <small>**Многозональное сканирующее устройство (МСУ-100)** — русскоязычный термин, не имеющий аналога в английском языке. **Multizones scanning device (MSU-100)** — дословный перевод с русского на английский.</small>

**Многозональное сканирующее устройство (МСУ-100)** — оптическая камера, предназначенная для фото‑видеосъёмки. Разработчик [ИКИ РАН](iki_ras.md). Разработано в 2008 году активное применение

|**Characteristics**|**(МСУ-100)**|
|:--|:--|
|Composition| |
|Consumption, W|7|
|Dimensions, ㎜| |
|[Interfaces](interface.md)|[MIL-STD-1553](mil_std_1553.md)|
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|3.2|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|Back.bright., ㏅/m²| |
|[FOV](fov.md), °|31.3|
|Frequency, fps| |
|Lens| |
|Resolution, px|1 × 7926|
|Shooting modes| |
| | [![](f/cam/i/iki_ras_msu_100_pic1t.webp)](f/cam/i/iki_ras_msu_100_pic1.webp) |

**Notes:**

1. <http://ofo.ikiweb.ru/msu.php>
1. **Applicability:** Метеор‑М №1



### MCSE MCAMv3

**MCSE MCAMv3** — optical camera for close-range observations. Designed by MCSE (CH) in ….

|**Characteristics**|**(MCSE MCAMv3)**|
|:--|:--|
|Composition|1 unit|
|Consumption, W|1.8 (snapshot)|
|Dimensions, ㎜|63.1 × 45 × 85|
|[Interfaces](interface.md)|SpaceWire|
|[Lifetime](lifetime.md), h(y)| |
|Mass, ㎏|0.33 (incl. optics)|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)|250 (25 000), hardened by architecture against SEE|
|[Reliability](qm.md)|MTBF of 2 420 000 hr/failure|
|[Thermal](tcs.md), ℃|−30 ‑ +60|
|[TRL](trl.md)|9|
|[Voltage](sps.md), V|27.5|
|**【Specific】**|~ ~ ~ ~ ~ |
|Back.bright., ㏅/m²| |
|[FOV](fov.md), °|83|
|Frequency, fps|1 (full 4MPx res.), 5 (low res & monochrome)|
|Lens| |
|Resolution, px|4 000 000, APS sensor, 8 ‑ 12 bit/px|
|Shooting modes|Monochrome or colour, Snapshot & video operation, Embedded image compression, auto‑exposure, windowing, HDR etc.|
| | [![](f/cam/m/mcse_mcamv3_pic1t.webp)](f/cam/m/mcse_mcamv3_pic1.webp)|



### MDA SIRC-NAV

**MDA SIRC-NAV** — optical camera for IR measurements. Designed by [MDA](mda.md) in ….

|**Characteristics**|**(SIRC-NAV)**|
|:--|:--|
|Composition|1 unit|
|Consumption, W|5 nominal, 8 peak|
|Dimensions, ㎜|100 × 100 × 140|
|[Interfaces](interface.md)|Spacewire, RS-422|
|[Lifetime](lifetime.md), h(y)| |
|Mass, ㎏|1.2|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)|1 000 (100 000)|
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃|−10 ‑ +40|
|[TRL](trl.md)|9|
|[Voltage](sps.md), V|12 or 28 DC|
|**【Specific】**|~ ~ ~ ~ ~ |
|Back.bright., ㏅/m²| |
|[FOV](fov.md), °|28 (SSA), 10 (EO)|
|Frequency, fps|120|
|Lens| |
|Resolution, px|1024 × 768, 786 000, 8 ‑ 14 µM, 16 bit, 40 m at 450 ㎞|
|Shooting modes| |
| | |

**Notes:**

1. The camera is available in two variants, Space Situational Awareness (SSA) & Earth Observations (EO). The modular design allows selection of a lens to suit mission requirements while taking advantage of a common qualified structure & electronics.



### Space Micro 4K SC
**4K Space Camera** — optical camera for close-range observations. Designer: [Space Micro](space_micro.md). Designed in 2017.

|**Characteristics**|**(4K Space Camera)**|
|:--|:--|
|Composition|1 optical unit, 1 electronics unit|
|Consumption, W|60|
|Dimensions, ㎜|165 × 178 × 153|
|[Interfaces](interface.md)|100 Gb/s: CAUI, CAN|
|[Lifetime](lifetime.md), h(y)|LEO: 43 800 (5) / …|
|Mass, ㎏|1.75|
|[Overload](vibration.md), Grms|10|
|[Radiation](ion_rad.md), ㏉(㎭)|300 (30 000)|
|[Reliability](qm.md)|0.995, [NASA PSL](nasa_psl.md) Lv.1,2,3|
|[Thermal](tcs.md), ℃|−10 ‑ +50|
|[TRL](trl.md)|9|
|[Voltage](sps.md), V|28 (22 ‑ 34)|
|**【Specific】**|~ ~ ~ ~ ~ |
|Back.bright., ㏅/m²| |
|[FOV](fov.md), °|80|
|Frequency, fps|130|
|Lens| |
|Resolution, px|12MP: 4 096 × 3 076, 12 bit Bayer|
|Shooting modes|Still Image Capture, 4K Video|
| |[![](f/cam/nmb/4k_space_camera_pic1t.webp)](f/cam/nmb/4k_space_camera_pic1.webp)|

**Notes:**

1. <https://www.spacemicro.com/products/guidance-and-navigation.html>
1. [4K Space Camera datasheet ❐](f/cam/nmb/4k_space_camera_datasheet.pdf)
1. **Applicability:** …



### Space Micro 5MP SC
> <small>**5MP Space Camera** — англоязычный термин, не имеющий аналога в русском языке. **5МП космическая камера** — дословный перевод с английского на русский.</small>

**5MP Space Camera** — оптическая камера, предназначенная для фото‑видеосъёмки. Разработчик [Space Micro](space_micro.md). Разработано ранее 2017 года. Активное применение.

|**Characteristics**|**(5MP Space Camera)**|
|:--|:--|
|Composition| |
|Consumption, W|4|
|Dimensions, ㎜|107 × 84 × 238|
|[Interfaces](interface.md)|[SpaceWire](spacewire.md) 80 Mbps|
|[Lifetime](lifetime.md), h(y)|ГСО: 87 600 (10) / …|
|Mass, ㎏|1|
|[Overload](vibration.md), Grms|20|
|[Radiation](ion_rad.md), ㏉(㎭)|300 (30 000)|
|[Reliability](qm.md)|[NASA PSL](nasa_psl.md) Lv.1,2,3|
|[Thermal](tcs.md), ℃|−20 ‑ +55|
|[TRL](trl.md)|9|
|[Voltage](sps.md), V|5|
|**【Specific】**|~ ~ ~ ~ ~ |
|Back.bright., ㏅/m²| |
|[FOV](fov.md), °|29, 39, 80 (custom FOV available)|
|Frequency, fps|100 (Rolling Shutter), 50 (Global Shutter)|
|Lens| |
|Resolution, px|5MP: 2 560 × 2 160, RGB/Monochrome, 22 bits (2 × 11‑bit)|
|Shooting modes|Still Image Capture|
| |[![](f/cam/nmb/5mp_space_camera_pic1t.webp)](f/cam/nmb/5mp_space_camera_pic1.webp)|

**Notes:**

1. <https://www.spacemicro.com/products/guidance-and-navigation.html>
1. [5MP Space Camera datasheet ❐](f/cam/nmb/5mp_space_camera_datasheet.pdf)
1. **Applicability:** …



## Archive


<p style="page-break-after:always"> </p>

### Template

**…** — optical camera for …. Designer: …. Designed in ….

|**Characteristics**|**(4K Space Camera)**|
|:--|:--|
|Composition| |
|Consumption, W| |
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)| |
|Mass, ㎏| |
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)| |
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|Back.bright., ㏅/m²| |
|[FOV](fov.md), °| |
|Frequency, fps| |
|Lens| |
|Resolution, px| |
|Shooting modes| |
| | |



### ТСНН УТК
> <small>**Телевизионная система навигации и наблюдения, узкоугольная камера (ТСНН УТК)** — русскоязычный термин, не имеющий аналога в английском языке. **TV system for navigation & observations, small-angle camera (TSNN UTK)** — дословный перевод с русского на английский.</small>

**Телевизионная система навигации и наблюдения, узкоугольная камера (ТСНН УТК)** — оптическая камера, предназначенная для фото‑видеосъёмки. Разработчик [ИКИ РАН](iki_ras.md). Разработано в 2010 году архивное изделие

|**Characteristics**|**(ТСНН УТК)**|
|:--|:--|
|Composition| |
|Consumption, W|8|
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|2.7|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|Back.bright., ㏅/m²| |
|[FOV](fov.md), °|0.85|
|Frequency, fps| |
|Lens| |
|Resolution, px|1000 × 1000, 3.04″, ПЗС|
|Shooting modes| |
| |[![](f/cam/i/iki_ras_tsnn_utk_pic1t.webp)](f/cam/i/iki_ras_tsnn_utk_pic1.webp)|

**Notes:**

1. <http://ofo.ikiweb.ru/tsnn.php>
1. **Applicability:** Фобос‑Грунт



### ТСНН ШТК
> <small>**Телевизионная система навигации и наблюдения, широкоугольная камера (ТСНН ШТК)** — русскоязычный термин, не имеющий аналога в английском языке. **TV system for navigation & observations, wide-angle camera (TSNN SHTK)** — дословный перевод с русского на английский.</small>

**Телевизионная система навигации и наблюдения, широкоугольная камера (ТСНН ШТК)** — оптическая камера, предназначенная для фото‑видеосъёмки. Разработчик [ИКИ РАН](iki_ras.md). Разработано в 2010 году архивное изделие

|**Characteristics**|**(ТСНН ШТК)**|
|:--|:--|
|Composition| |
|Consumption, W|8|
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|1.7|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|Back.bright., ㏅/m²| |
|[FOV](fov.md), °|23.3|
|Frequency, fps| |
|Lens| |
|Resolution, px|1000 × 1000, 84.8″, ПЗС|
|Shooting modes| |
| |[![](f/cam/i/iki_ras_tsnn_shtk_pic1t.webp)](f/cam/i/iki_ras_tsnn_shtk_pic1.webp)|

**Notes:**

1. <http://ofo.ikiweb.ru/tsnn.php>
1. **Applicability:** Фобос‑Грунт



<p style="page-break-after:always"> </p>

## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Camera](cam.md)】**<br> [SAR](cam.md) <br>~ ~ ~ ~ ~<br> **РФ:** … () ┊ … ()  ▮  **США:** … () ┊ … ()|

1. Docs: …
1. <…>


## The End

end of file
