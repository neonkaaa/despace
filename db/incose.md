# INCOSE
> 2022.01.20 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/i/incose_logo1t.webp)](f/c/i/incose_logo1.svg)|<info@incose.org>, +1(858)541-17-25 , Fax …;<br> *7670 Opportunity Rd, Suite 220, San Diego, CA 92111-2222  USA*<br> <https://www.incose.org> ~~ [FB ⎆](https://www.facebook.com/INCOSE) ~~ [LI ⎆](https://www.linkedin.com/company/incose) ~~ [X ⎆](https://twitter.com/incose_org) ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/International_Council_on_Systems_Engineering)|
|:--|:--|
|**Business**|Conferences, publications, local chapters, certifications & technical working groups|
|**Mission**|To address complex societal & technical challenges by enabling, promoting & advancing systems engineering & systems approaches|
|**Vision**|A better world through a systems approach|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

The **International Council on Systems Engineering** (**INCOSE**; pronounced in‑co‑see) is a not‑for‑profit membership organization & professional society for [Systems engineering](se.md) (SE). The goals are focused on the creation & dissemination of SE information, promoting international collaboration & promoting the profession of SE.

- **Publications & products:**
   - INCOSE Systems Engineering Handbook
   - Journal of Systems Engineering
   - INSIGHT Practitioner Journal
   - Metrics Guidebook for Integrated Systems & Product Development
   - I-pub publication database
   - [Systems Engineering Tools Database ⎆](https://www.systemsengineeringtools.com)
- **Standards.** INCOSE’s International Council on Systems Engineering Standards Technical Committee (STC) is working to advance & harmonize systems engineering standards used worldwide. Some notable standards:
   - ECSS-E-10 Space Engineering — Systems Engineering Part 1B: Requirements & process, 18 Nov 2004
   - ECSS-E-10 Space Engineering — Systems Engineering Part 6A: Functional & technical specifications, 09 Jan 2004
   - ECSS-E-10 Space Engineering — Systems Engineering Part 7A: Product data exchange, 25 August 2004
   - ISO/IEC 15288: 2002 — System Life Cycle Processes
   - OMG [Systems Modeling Language](sysml.md) (OMG SysML), July 2006

<p style="page-break-after:always"> </p>

## The End

end of file
