# Sim.Space
> 2022.04.04 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/s/sim_space_logo1t.webp)](f/c/s/sim_space_logo1.webp)|<contact@sim.space>, (972)54-5679-222, Fax …;<br> *PO BOX 4104, Herzlia, 4614002 Israel*<br> <http://sim.space> ~~ [LI ⎆](https://www.linkedin.com/company/sim-space)|
|:--|:--|
|**Business**|Simulation software, Hardware-In-The-Loop (HIL) tests|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO, Founder — Asaf Lewin<br> ・CTO, Founder — Amir Notea|

**Sim.Space** is a boutique consulting company specializing in development & operation of full‑system simulations & hybrid labs for the aerospace industry.

<p style="page-break-after:always"> </p>

## The End

end of file
