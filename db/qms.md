# Система менеджмента качества
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [QM](qm.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Система менеджмента качества (СМК)** — русскоязычный термин. **Quality management system (QMS)** — англоязычный эквивалент.</small>

**Система менеджмента качества** — совокупность организационной структуры, методик, процессов и ресурсов, необходимых для осуществления политики в области [качества](qm.md) с помощью планирования, управления, обеспечения и улучшения качества.



## Описание

![](f/doc/sistema_menedjmenta_kachestva_01.webp)



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**`Качество:`**<br> [Bus factor](bus_factor.md) ~~ [Way](faq.md) ~~ [АВПКО](fmeca.md) ~~ [Авторский надзор](des_spv.md) ~~ [Бережливое производство](lean_man.md) ~~ [Валидация, верификация](vnv.md) ~~ [Класс чистоты](clean_lvl.md) ~~ [Конструктивное совершенство](con_vel.md) ~~ [Крит. технологии](kt.md) ~~ [Крит. элементы](sens_elem.md) ~~ [Метрология](metrology.md) ~~ [Надёжность](qm.md) ~~ [Нештатная ситуация](emergency.md) ~~ [Номинал](nominal.md) ~~ [Ошибки](faq.md) ~~ [Система менеджмента качества](qms.md) ~~ [УГТ](trl.md)/[TRL](trl.md)|

1. Docs:
   1. [РК‑11](const_rk.md), стр.21.
1. <…>


## The End

end of file
