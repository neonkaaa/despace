# 3D Printing Corp.
> 2020.07.16 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/0/3d_printing_corp_logo1t.webp)](f/c/0/3d_printing_corp_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *75-1 Onocho, Tsurumi Ward, Yokohama, Kanagawa 230-0046, Japan*<br> <https://www.3dpc.co.jp> ~~ [LI ⎆](https://www.linkedin.com/company/3d-printing-corporation)|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**3D Printing Corporation KK.** By integrating [3D printing technologies](sc.md), we establish alternative supply chains for the support of Japanese manufacturers.



## The End

end of file
