# PD AeroSpace
> 2020.07.18 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/p/pd_aerospace_logo1t.webp)](f/c/p/pd_aerospace_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *3519 Arimatsu, Midori-ku, Nagoya, Aichi 458-0924, JP*<br> <https://pdas.co.jp> ~~ [LI ⎆](https://www.linkedin.com/company/pd-aerospace-ltd.) ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/PD_AeroSpace)|
|:--|:--|
|**Business**|Suborbital spaceplane, space transportation, space tourism|
|**Mission**|**「Bringing Space Closer」**. There are so many amazing possibilities in space, waiting to be discovered & understood. Perhaps, a new kind of resources or energy source is just around the corner. Space also enables us to reflect upon ourselves & Earth, to deeply understand the preciousness of life & nature. There are & will be challenges never seen or predicted during our journey to reach the stars. However, we chose to take upon this challenge, because we believe humanity can earn so much from space exploration. In order to bring space closer to us & move towards a more peaceful & prosperous world, PDAS’ll continue to pursue the challenge in the field of space transportation.<br> ➀ Developing a safe & low‑cost transportation system.<br> ➁ Approaching two markets, one in space & one on Earth.<br> ➂ Expand the use of space for civilian purposes by converting the system.|
|**Vision**|Corporate Philosophy:<br> ・We contribute to society with technology.<br> ・We maintain harmony with space, the Earth, nature & humanity.<br> ・We become a company that society wants to exist, a company that clarifies the significance of itself through its activity.|
|**Values**|Motto. Never give up & keep an indomitable spirit of challenge. When there is no path, make one yourself. Innovation over improvement. Understand that time & space are limited & take action.|
|**[MGMT](mgmt.md)**|・CEO — Shuji Ogawa|

**PD Aerospace** (**PDAS**, ＰＤエアロスペース株式会社, Pī Dī Earosupēsu Kabushiki‑gaisha), is a Japanese space tourism company based in Nagoya founded in 2007.05.30 by Shuji Ogawa.

The 「PD」 in the company’s name stands for 「pulse detonation」. PDAS is developing a [suborbital spaceplane](sc.md) to carry 2 pilots & 6 passengers using a hybrid of jet & rocket power. Initial tickets are planned for ¥ 14 000 000 (~ $ 125 000 as of April 2017) eventually lowering to ¥ 400 000 (~ $ 3 600).

PDAS plans to develop a hybrid engine that produces jet & rocket thrust, using pulse detonation jet & pulse combustion rocket modes. To reduce the cost of development & keep the vehicle low‑cost, PDAS plans to use commercially available hardware, instead of custom‑designed parts. PDAS plans to launch an unmanned prototype in 2019, perform manned testing by 2020, & start commercial flights in 2023. H.I.S. & ANA own 10 % & 7 % of the company, respectively.

**SpacePlane Project.** To improve our understanding of our planet & to promote the utilization of near‑Earth space, we aim to establish a low cost & practical space transportation infrastructure. Startup companies in Europe & America are beginning to enter the space sector by offering unique means of transportation to space that reduce cost significantly. We aim to develop & operate a competitive space transportation system with our unique spaceplane concept.

【**Table.** Spaceplane development program】

|**Code name**|**Type**|**Length**|**Payload**|**Altitude**|**Main applications**|
|:--|:--|:--|:--|:--|:--|
|PDAS-X04|Unmanned|5 m|50 ㎏|~10 ㎞|Observation|
|PDAS-X07|Unmanned|12 m|100 ㎏|110 ㎞|Microgravity experiments, High altitude atmo observation|
|PDAS-X08|Manned|18 m|6 passengers + 2 pilots|110 ㎞|Commercial manned space flight|
|PDAS-X09|Unmanned|TBD|6 000 ㎏|110 ㎞|Air launch to orbit payload carrier|



**Message from CEO**

> In the 21st century, space has become indispensable to our daily life.<br> In the future, our activity in space will expand & diversify leading to medical, industrial & scientific research conducted in space. In order for such research & development to be fully effective, it’s necessary for industry, government & academia to promote individual research while maintaining unity & conducting it comprehensively & efficiently. For Japan to efficiently develop its activities in space, Japan needs its own space infrastructure. A space transportation system along with a general platform for utilizing space is essential to such infrastructure.<br><br> The achievement of such an infrastructure has been my goal for a long time & this passion is still strong in myself. Our company is not dependent on government policies or enormous funds; we seek to develop a manned spaceplane as one private company. Fully utilizing the experience gained over my career, I wish to bring a revolutionary technology to the field of aerospace & bring our challenge to life.<br><br> 「Everything stops once it is given up; the only path to success is to keep trying」<br><br> The simple idea of 「cannot」 is enough to make something impossible. Thinking of how to achieve something is the starting point. No matter how sizable a project is, what propels it forward is a collection such small starts.<br><br> At PD AeroSpace, we are collecting individual contributions to progress in our challenge within the space sector. I hope & believe that our company’s effort will be a shared passion among many & that among our numerous collaborators we will construct our future together.

【**Table.** Biography】

|**Date**|**Event**|
|:--|:--|
|May ’07|PD AeroSpace, LTD. Established|
|February ’08|Participated in funding matching event|
|August ’08|Noshiro space event/demo flight|
|April ’09|R & D organization launched (3 universities, 2 companies, 1 JAXA volunteer)|
|August ’09|Visit space startup in L.A USA|
|February ’10|Start design of next experimental machine (X-02)|
|November ’10|Centrair Space Port initiative started(CV2020)|
|December ’10|Start development of new engine|
|February ’13|Inaugurated an extraordinary member of Cabinet Office Space Strategy Office Space Policy Committee (Space Transportation System Subcommittee)|
|December ’13|Joint research agreement signed with JAXA, Kyushu Institute of Technology|
|February ’14|Invited as speaker at TEDxNayabashi|
|March ’14|Visited & had technical meeting with the University of Texas, NASA Johnson Space Center, visited Spaceport America|


## The End

end of file
