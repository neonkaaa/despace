# Терминатор
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [БНО](nnb.md), **[Space](index.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Терминатор** — русскоязычный термин. **Terminator / Twilight zone** — англоязычный эквивалент.</small>

**Термина́тор** *(от лат. terminare — прекращать)* — линия светораздела, отделяющая освещённую (светлую) часть тела (например, космического тела) от неосвещённой — тёмной — части. Терминатор шарообразного тела всегда наблюдается в виде полуэллипса, принимая в конце первой и начале последней четвертей вид прямой линии.

**Термина́тор** *(англ. terminator 「ограничитель; тут ликвидатор」)* — общее название для серий различных роботов и автоматических боевых механизмов, созданных искусственным интеллектом 「Скайнет」 для истребления человечества.



## Терминаторы небесных тел
Терминатор [Луны](moon.md) после новолуния называется утренним, а после полнолуния — вечерним. При наблюдении [Земли](earth.md) из космоса говорят о терминаторе Земли. Когда терминатор пересекает географические полюса Земли (пролегает по меридиану), наступает равноденствие. Аналогичное явление наблюдается и на других планетах в том числе в других звездных системах. Если на таких планетах как, например, Земля или [Юпитер](jupiter.md) и спутниках планет (например, на Луне) терминатор постоянно движется по видимой поверхности тела, то на планетах, находящихся у своих звезд в приливном захвате и, следовательно, обращенных к своей звезде только одной своей стороной, терминатор неподвижен.

Внешний вид терминатора может дать информацию о поверхности космического тела, по которому он проходит. Так, например, размытая линия терминатора говорит о наличии у планеты или её спутника атмосферы.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**`Баллистико‑навигационное обеспечение (БНО):`**<br> [SPICE](spice.md) ~~ [Апоцентр и перицентр](apopericentre.md) ~~ [Гравманёвр](gravass.md) ~~ [Кеплеровы элементы](keplerian.md) ~~ [Космическая скорость](esc_vel.md) ~~ [Сфера Хилла](hill_sphere.md) ~~ [Терминатор](terminator.md) ~~ [Точки Лагранжа](l_points.md) ~~ [Эффект Оберта](oberth_eff.md)|
|**【[Space](index.md)】**<br> [Apparent magnitude](app_mag.md) ~~ [Astro.object](aob.md) ~~ [Blue Marble](earth.md) ~~ [Cosmic rays](ion_rad.md) ~~ [Ecliptic](ecliptic.md) ~~ [Escape velocity](esc_vel.md) ~~ [Health](health.md) ~~ [Hill sphere](hill_sphere.md) ~~ [Information](info.md) ~~ [Lagrangian points](l_points.md) ~~ [Near space](near_space.md) ~~ [Pale Blue Dot](earth.md) ~~ [Parallax](parallax.md) ~~ [Point Nemo](earth.md) ~~ [Silver Snoopy award](silver_snoopy_award.md) ~~ [Solar constant](solar_const.md) ~~ [Terminator](terminator.md) ~~ [Time](time.md) ~~ [Wormhole](wormhole.md) ┊ ··•·· **Solar system:** [Ariel](ariel.md) ~~ [Callisto](callisto.md) ~~ [Ceres](ceres.md) ~~ [Deimos](deimos.md) ~~ [Earth](earth.md) ~~ [Enceladus](enceladus.md) ~~ [Eris](eris.md) ~~ [Europa](europa.md) ~~ [Ganymede](ganymede.md) ~~ [Haumea](haumea.md) ~~ [Iapetus](iapetus.md) ~~ [Io](io.md) ~~ [Jupiter](jupiter.md) ~~ [Makemake](makemake.md) ~~ [Mars](mars.md) ~~ [Mercury](mercury.md) ~~ [Moon](moon.md) ~~ [Neptune](neptune.md) ~~ [Nereid](nereid.md) ~~ [Nibiru](nibiru.md) ~~ [Oberon](oberon.md) ~~ [Phobos](phobos.md) ~~ [Pluto](pluto.md) ~~ [Proteus](proteus.md) ~~ [Rhea](rhea.md) ~~ [Saturn](saturn.md) ~~ [Sedna](sedna.md) ~~ [Solar day](solar_day.md) ~~ [Sun](sun.md) ~~ [Titan](titan.md) ~~ [Titania](titania.md) ~~ [Triton](triton.md) ~~ [Umbriel](umbriel.md) ~~ [Uranus](uranus.md) ~~ [Venus](venus.md)|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Terminator_(solar)>


## The End

end of file
