# Nautilus-X
> 2019.03.20 [🚀](../../index/index.md) [despace](index.md) → [косм.города‑бублики.md](rwss.md), **[Project](project.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Nautilus-X** — англоязычный термин, не имеющий аналога в русском языке. **Наутилус‑Икс** — дословный перевод с английского на русский.</small>

**Nautilus-X** *(Non‑Atmospheric Universal Transport Intended for Lengthy United States Exploration)* rotating wheel space station concept developed by engineers Mark Holderman and Edward Henderson of the Technology Applications Assessment Team of NASA.

The concept was 1st proposed in 2011.01 for long‑duration (1 to 24 months) exo‑atmospheric space journeys for a six‑person crew. In order to limit the effects of microgravity on human health, the spacecraft would be equipped with a centrifuge. The Nautilus-X design concept did not advance beyond the initial drawings and proposal.

|<small>*Nautilus-X spacecraft*</small>|
|:--|
|[![](f/project/a/ais/nautilus_x_pic1t.webp)](f/project/a/ais/nautilus_x_pic1.webp)|


## Objectives
The original goal of Nautilus-X was to be a stopover to long‑term missions for the Moon or Mars. To ease route planning of the whole mission, the station would be placed at the Lagrange point L1 or L2 of the Moon or Mars, depending on which location is to be visited.

It would also have served as an emergency station and hospital for current mission crews.

Other objectives included:

1. Self‑sustained for long‑duration missions with crews as large as 6.
1. Support manned landing missions.
1. Satisfy requirements of NASA Authorization Act of 2010.



## Description
**Design**

The proposal notionally included a 6.5 by 14 metre main corridor, a rotating habitable centrifuge, inflatable modules for logistical stores and crew use, solar power arrays, and a reconfigurable thrust structure.

The design was modular, enabling it to accommodate any of a number of mission specific propulsion modules, manipulator arms, docking port for an Orion or commercial crew capsule, and landing craft for destination worlds. In theory the engines and fuel could be swapped out depending on the mission. The proposal also had an industrial slide‑out airlock unit and a command, control and observation deck.

On the other end of the docking port would have been the spacecraft’s centrifuge equipped with an external dynamic ring‑flywheel. Behind the centrifuge would be water and slush hydrogen tanks, which could mitigate the dangers of cosmic radiation for the crew, to a certain degree. In the aft of the craft are the communication and propulsion systems.

The standard version of Nautilus-X had only three inflatable modules. The Extended Duration Explorer variant on the Nautilus-X design concept would have several more, plus docking bays for science payloads and away‑mission vehicles.

**Technologies**

In order to deploy this massive spacecraft as easily as possible, it would consist of a variety of rigid and inflatable modules and solar dynamic arrays. The expandable modules are based on the technology used by the inflatable living quarters proposed by Bigelow Aerospace, which has continued the development of inflatable modules initially designed and developed by NASA.

**Attributes:**

1. Robust Environmental Control and Life Support and communication suite
1. Large storage volumes (for food, mechanical parts or medical supplies)
1. Real‑time visual command & observe capability for crew
1. Low crew irradiation
1. Semi‑autonomous integration of multiple mission specific propulsion units



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[](.md)】**<br> <mark>NOCAT</mark>|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Nautilus-X>


## The End

end of file
