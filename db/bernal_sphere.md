# Сфера Бернала
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [Project](project.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Сфера Бернала** — русскоязычный термин. **Bernal sphere** — англоязычный эквивалент.</small>

**Сфера Бернала** — это тип орбитальной станции и космического поселения, разработанный в 1929 году Джоном Десмондом Берналом; 「пространственная среда」, предназначенная для постоянного проживания людей. Оригинальный проект Бернала представлял собой сферу диаметром около 10 миль (16 км), способную вместить 20—30 тыс. человек и наполненную воздухом.

|<small>*Внешний вид<br> сферы Бернала*</small>|<small>*Внутренняя часть<br> сферы — вид через<br> световое окно сферы*</small>|
|:--|:--|
|[![](f/project/a/ais/bernal_sphere_pic1t.webp)](f/project/a/ais/bernal_sphere_pic1.webp)|[![](f/project/a/ais/bernal_sphere_pic2t.webp)](f/project/a/ais/bernal_sphere_pic2.webp)|



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[](.md)】**<br> <mark>NOCAT</mark>|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Bernal_sphere>


## The End

end of file
