# Electro-automatic system (a list)
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → **[БАС](eas.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

A list of [Electro-automatic system](eas.md).

## Current



### БАК-01
> <small>**БАК‑01** — русскоязычный термин, не имеющий аналога в английском языке. **BAK‑01** — дословный перевод с русского на английский.</small>

**БАК‑01** — комплекс автоматики и стабилизации производства [ПАО Сатурн](пао_сатурн.md).  
Разработан в рамках проекта [Luna‑27](луна_27.md).

|**Характеристика**|**Значение**|
|:--|:--|
|Composition| |
|Consumption, W|18.8|
|Dimensions, ㎜|295×320×215|
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… (3) / …|
|Mass, ㎏|8.3|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)|0.998|
|[Thermal](tcs.md), ℃|–20 ‑ +45 (циклирование); –50 ‑ +50 (транспортирование)|
|[TRL](trl.md)| |
|[Voltage](sps.md), V|27.5 ± 0.35 (СН); 28 ‑ 32.8 (НСт)|
|**【Specific】**|~ ~ ~ ~ ~ |
|Длительность переходных процессов, не более, ㎳|50|
|Мощность, не менее, W|СН — 150,<br> НСт — 900|

**Notes:**

1. Состав:
   1. силовой регулятор тока батареи фотоэлектрической (БФ);
   1. измерители тока БФ, аккумуляторной батареи (АБ) и нагрузки;
   1. устройства коммутации БФ, АБ;
   1. стабилизированный преобразователь напряжения (СН);
   1. устройство контроля напряжений аккумуляторов;
   1. устройство контроля температуры АБ;
   1. устройство выравнивания напряжений аккумуляторов;
   1. пороговый датчик минимального напряжения АБ.
1. **Applicability:** [Luna‑27](луна_27.md)



### КАС-ЛОА

**КАС‑ЛОА** — комплекс автоматики и стабилизации <mark>TBD</mark>.
1. **Разработан** в рамках проекта [Luna‑26](луна_26.md).

|**Характеристика**|**Значение**|
|:--|:--|
|Composition| |
|Consumption, W|55|
|Dimensions, ㎜|БСПК — 580×580×200; БЭ АБ — 310×196×152|
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / 37 670|
|Mass, ㎏|37.2|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)|0.999|
|[Thermal](tcs.md), ℃|–20 ‑ +40 (циклирование); … ‑ … (транспортирование)|
|[TRL](trl.md)| |
|[Voltage](sps.md), V|27.5 ± 0.35 (СН); 28 ‑ 32.8 (НСт)|
|**【Specific】**|~ ~ ~ ~ ~ |
|Длительность переходных процессов, не более, ㎳|50|
|Мощность, W|40 ‑ 4 000|

**Notes:**

1. Состав:
   1. блок силового электропитания, коммутации и контроля (БСПК‑ЛОА);
   1. блок электроники аккумуляторной батареи 1 (БЭ АБ1);
   1. блок электроники аккумуляторной батареи 2 (БЭ АБ2);
   1. комплект межблочных кабелей.
1. **Applicability:** [Luna‑26](луна_26.md)



## Archive



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Spacecraft power system (SPS)](sps.md)】**<br> [Charge eff.](charge_eff.md) ~~ [EAS](eas.md) ~~ [EB](eb.md) ~~ [EMI, RFI](emi.md) ~~ [NR](nr.md) ~~ [Rotor](iu.md) ~~ [RTG](rtg.md) ~~ [Solar cell](sp.md) ~~ [SP](sp.md) ~~ [SPB/USPB](suspb.md) ~~ [Voltage](sps.md) ~~ [WT](wt.md)<br>~ ~ ~ ~ ~<br> **RF/CIF:** [BAK‑01](eas_lst.md) ~~ [KAS‑LOA](eas_lst.md)|

1. Docs: …
1. <…>


## The End

end of file
