# Electron
> 2019.05.20 [🚀](../../index/index.md) [despace](index.md) → [LV](lv.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Electron** — англоязычный термин, не имеющий аналога в русском языке. **Электрон** — дословный перевод с английского на русский.</small>

**「Электрон」** (англ. **Electron**) — новозеландская одноразовая жидкостная [ракета‑носитель](lv.md) сверхлёгкого класса, разработанная новозеландским подразделением частной аэрокосмической компании [Rocket Lab](rocket_lab.md) (США).

|**Version**|**Description**|**Activity**|
|:--|:--|:--|
|Electron|Базовый вариант.|**Активен** (2017 ‑ …)|



## Electron
**「Электрон」** (англ. **Electron**) — новозеландская одноразовая жидкостная [ракета‑носитель](lv.md) сверхлёгкого класса, разработанная новозеландским подразделением частной аэрокосмической компании [Rocket Lab](rocket_lab.md) (США).  
Electron позволяет вывести полезную нагрузку массой до 150 ㎏ на ССО 500 ㎞ (до 250 ㎏ на НОО). Основные элементы РН, несущий корпус и баки ступеней выполнены из [углепластика](cfrp.md) и производятся Rocket Lab. Применение композиционных материалов позволило существенно снизить вес конструкции. РН оборудована композитным ГО длиной 2.5 м, диаметром 1.2 м и массой около 50 ㎏.

|**Characteristic**|**[Value](si.md)**|
|:--|:--|
|Активность|**Активен** (2017‑…)|
|[Аналоги](analogue.md)|**Kuaizhou** (Китай) ┊ **LauncherOne** (США) ┊ **Pegasus** (США) ┊ **Super Strypi** (США) ┊ **Vector‑H** (США)|
|Длина/диаметр|17 / 1.2 м|
|[Космодромы](spaceport.md)|[Rocket Lab LC1](spaceport.md)|
|Масса старт./сух.|12 550 / 1 250 ㎏|
|Разраб./изготов.|[Rocket Lab](rocket_lab.md) (США) / [Rocket Lab](rocket_lab.md) (США)|
|Ступени|2|
|[Fuel](ps.md)|[O + Kerosene](o_plus.md)|
| |[![](f/lv/electron/electron_1st_launcht.webp)](f/lv/electron/electron_1st_launch.webp)|

**Выводимые массы.**

|**Космодром**|**РН**|<small>*Масса,<br> [НОО](nnb.md), т*</small>|<small>*Масса,<br> [ГСО](nnb.md), т*</small>|<small>*Масса к<br> [Луне](moon.md), т*</small>|<small>*Масса к<br> [Венере](venus.md), т*</small>|<small>*Масса к<br> [Марсу](mars.md), т*</small>|**Примечания**|
|:--|:--|:--|:--|:--|:--|:--|:--|
|[Rocket Lab LC1](spaceport.md)|Electron|0.25|—|—|—|—|Пуск — $ 6 млн (2019 г);<br> ПН 1.99 % от ст.массы|

<small>Примечания:<br> **1)** Указана масса для наихудших условий старта.<br> **2)** В скобках указана масса для наилучших условий старта.</small>

|**РН в сравнении<br> с человеком**|**В сборочном<br> помещении**|**Двигатель<br> 「Резерфорд」**|
|:--|:--|:--|
|[![](f/lv/electron/electron_and_human_2017t.webp)](f/lv/electron/electron_and_human_2017.webp)|[![](f/lv/electron/2017_04t.webp)](f/lv/electron/2017_04.webp)|[![](f/lv/electron/electron_rocket_engine_5t.webp)](f/lv/electron/electron_rocket_engine_5.webp)|



## Архивные

…



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Launch vehicle (LV)](lv.md)】**<br> [ICBM](icbm.md) ~~ [Integrated payload unit](lv.md) ~~ [Non‑rocket spacelaunch](nrs.md) ~~ [Throw weight](throw_weight.md)<br>~ ~ ~ ~ ~<br> **China:** [Long March](long_march.md) ┊ **EU:** [Arian](arian.md), [Vega](vega.md) ┊ **India:** [GSLV](gslv.md), [PSLV](pslv.md) ┊ **Israel:** [Shavit](shavit.md) ┊ **Japan:** [Epsilon](epsilon.md), [H2](h2.md), [H3](h3.md) ┊ **Korea N.:** [Unha](unha.md) ┊ **Korea S.:** *([Naro‑1](kslv.md))* ┊ **RF,CIF:** [Angara](angara.md), [Proton](proton.md), [Soyuz](soyuz.md), [Yenisei](yenisei.md), [Zenit](zenit.md) *([Energia](energia.md), [Korona](korona.md), [N‑1](n_1.md), [R‑1](r_7.md))* ┊ **USA:** [Antares](antares.md), [Atlas](atlas.md), [BFR](bfr.md), [Delta](delta.md), [Electron](electron.md), [Falcon](falcon.md), [Firefly Alpha](firefly_alpha.md), [LauncherOne](launcherone.md), [New Armstrong](new_armstrong.md), [New Glenn](new_glenn.md), [Minotaur](minotaur.md), [Pegasus](pegasus.md), [Shuttle](shuttle.md), [SLS](sls.md), [Vulcan](vulcan.md) *([Saturn](saturn_lv.md), [Sea Dragon](sea_dragon.md))**|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Electron_(rocket)>
1. <https://en.wikipedia.org/wiki/LauncherOne>
1. <https://en.wikipedia.org/wiki/Kuaizhou>
1. <https://en.wikipedia.org/wiki/Northrop_Grumman_Pegasus>
1. <https://en.wikipedia.org/wiki/Rocket_Lab_Electron>
1. <https://en.wikipedia.org/wiki/Comparison_of_orbital_launch_systems>
1. 2016.09.26 Rocket Lab Opens Private Orbital Launch Site in New Zealand (<https://www.space.com/34195-rocket-lab-opens-private-launch-site-new-zealand.html>)


## The End

end of file
