# Systems Tool Kit
> 2019.01.01 [🚀](../../index/index.md) [despace](index.md) → **[Soft](soft.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Systems Tool Kit (STL)** — англоязычный термин, не имеющий аналога в русском языке. **Набор системных инструментов (СТЛ)** — дословный перевод с английского на русский.</small>

**Systems Tool Kit** (formerly Satellite Tool Kit), often referred to by its initials **STK**, is a proprietary physics‑based software package for MS Windows from Analytical Graphics, Inc. that allows engineers & scientists to perform complex analyses of ground, sea, air, & space platforms, & share results in one integrated environment.

At the core of STK is a geometry engine for determining the time‑dynamic position & attitude of objects (「assets」), & the spatial relationships among the objects under consideration including their relationships or accesses given a number of complex, simultaneous constraining conditions. STK has been developed since 1989 as a commercial off the shelf software tool. Originally created to solve problems involving Earth‑orbiting satellites, it is now used in the aerospace & defense communities & for many other applications.

STK is used in government, commercial, & defense applications around the world. Clients of AGI are organizations such as NASA, ESA, CNES, DLR, Boeing, JAXA, ISRO, Lockheed Martin, Northrop Grumman, Airbus, DOD, & Civil Air Patrol.

In 1989, the 3 founders of Analytical Graphics, Inc. — Paul Graziani, Scott Reynolds & Jim Poland, left GE Aerospace to create Satellite Tool Kit (STK) as an alternative to bespoke, project‑specific aerospace software.

The original version of STK ran only on Sun Microsystems computers, but as PCs became more powerful, the code was converted to run on Windows.

STK was 1st adopted by the aerospace community for orbit analysis & access calculations (when a satellite can see a ground‑station or image target), but as the software was expanded, more modules were added that included the ability to perform calculations for communications systems, radar, interplanetary missions & orbit collision avoidance.

The addition of 3D viewing capabilities led to the adoption of the tool by military users for real‑time visualization of air, land & sea forces as well as the space component. STK has also been used by various news organizations to graphically depict current events to a wider audience, including the deorbit of Russia’s Mir Space Station, the Space Shuttle Columbia disaster, the Iridium/Cosmos collision, the asteroid 2012 DA14 close approach & various North Korea missile tests.

As of version 10, the software underwent a name change from Satellite Tool Kit to Systems Tool Kit to reflect its applicability in land, sea, air, & space systems.

In 2019, Dutch amateur skywatcher Marco Langbroek used STK to analyze a high‑resolution photograph of an Iranian launch site accident tweeted by US President Trump. It was 「the 1st time in three & a half decades that an image had become public that revealed the sophistication of US spy satellites in orbit」. Langbroek & astronomer Cees Bassa, identified the specific classified spysat (USA‑224, a KH‑11 satellite with an objective mirror as large as the Hubble Space Telescope) that had taken the photograph, & the time when it was taken on a particular satellite pass.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Software](soft.md)】**<br> [ASP](asp.md) ~~ [Blender](blender.md) ~~ [C](plang.md) ~~ [Cosmographia](cosmographia.md) ~~ [DOORS](doors.md) ~~ [DWG](cad_f.md) ~~ [GIMP](gimp.md) ~~ [Git](git.md) ~~ [IGES](cad_f.md) ~~ [ISIS](isis.md) ~~ [JT](cad_f.md) ~~ [NGT](neogeography_toolkit.md) ~~ [NX](nx.md) ~~ [Octave](gnu_octave.md) ~~ [OS](os.md) ~~ [PDF](pdf.md) ~~ [Python](plang.md) ~~ [R](plang.md) ~~ [SPICE](spice.md) ~~ [STEP](cad_f.md) ~~ [STL](stk.md) ~~ [SVG](cad_f.md) ~~ [Syncthing](syncthing.md) ~~ [SysML](sysml.md) ~~ [Teamcenter](teamcenter.md) ~~ [Valispace](valispace.md) ~~ [Система управления версиями](vcs.md) ~~ [ХРИП](adra.md)|

1. Docs: …
1. <http://www.agi.com/products>
1. <https://en.wikipedia.org/wiki/Systems_Tool_Kit>


## The End

end of file
