# SEESE
> 2021.12.08 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/s/seese_logo1t.webp)](f/c/s/seese_logo1.webp)|<info@seese.jp>, <mark>nophone</mark>, Fax …;<br> *2-1-6 Sengen, Tsukuba-shi, Ibaraki, Japan*<br> <https://seese.jp> ~~ [FB ⎆](https://www.facebook.com/SEESE-Inc-102612468412931) ~~ [LI ⎆](https://www.linkedin.com/company/seese-inc) ~~ [X ⎆](https://twitter.com/seese2020)|
|:--|:--|
|**Business**|Environmental testing (one stop service), space development services|
|**Mission**|Make space exploration simple|
|**Vision**|Achieving a sustainable space economy|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO — Waku Tanada<br> ・COO — Nguyen Tat Trung<br> ・CFO — Nao Lee|

**SEESE Co., Ltd.** is a Japanese company aimed for one stop services for testing, R&D consulting. Founded 2020.12. Services:

- **Environmental test one-stop service:**
   - **Consulting** — we will introduce satellite development experts according to the user’s request. In addition to environmental tests, we also select materials & provide consulting from the design stage.
   - **Search reservation system** — easily search for test equipment on the Web. We also support detailed conditions such as equipment specifications & prices, & whether or not there is a clean room for work. It greatly reduces the trouble of searching for a test site.
   - **Equipment preparation** — you can order the equipment & consumables required for test preparation on the Web.
   - **Delivery** — arrangements for delivery of specimens to the test site are also completed on the Web.
   - **Test operator arrangement** — we will arrange a professional operator to carry out the test.
   - **Analysis evaluation** — you can also analyze & evaluate the test results on the Web.

<p style="page-break-after:always"> </p>

## The End

end of file
