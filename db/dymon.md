# Dymon
> 2021.12.02 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/d/dymon_logo1t.webp)](f/c/d/dymon_logo1.webp)|<info@dymon.co.jp>, +81(4)8482-8339, Fax …;<br> *4-10-20 Omori-minami, Ota-ku, Tokyo, 143-0013, Japan*<br> <http://dymon.co.jp> ~~ [FB ⎆](https://www.facebook.com/dymon.co.jp) ~~ [IG ⎆](https://www.instagram.com/yaoki_space) ~~ [X ⎆](https://twitter.com/yaoki_space)|
|:--|:--|
|**Business**|Robots, rovers, antennas|
|**Mission**|…|
|**Vision**|Ultimate mobility development that runs the Moon|
|**Values**|**Principal & Will** — leading the world with a strong will. **Sustainable & Symbiosis** — aiming for a sustainable symbiotic society. **Challenge & Respect** — big challenge & respect for the individual.|
|**[MGMT](mgmt.md)**|・CEO, Founder — Shinichiro Nakajima<br> ・COO — Sota Miyake|

**Dymon Co., Ltd.** is a Japanese company aimed to design & manufacture space robots, rovers, antennas, etc. Founded 2012.02.22. Products:

- **YAOKI** — the small (0.5 ㎏) lunar rover. What YAOKI will achieve:
   - In 2021 Winter — be the world’s 1st commercial rover for lunar exploration. YAOKI will be the 1st Japanese rover to participate in NASA’s lunar transportation mission, 「CLPS」 and be aboard Astrobotic’s lunar lander Peregrine.
   - In 2024 — Cooperate with the Artemis Project. As a forerunner in NASA’s lunar development project, the Artemis Project, YAOKI, with its light footwork, aims to contribute to the field of mobility systems.
   - In 2030 — Construct a lunar base.

<p style="page-break-after:always"> </p>

## The End

end of file
