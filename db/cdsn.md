# Chinese Deep Space Network
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [CNSA](cnsa.md), **[НС](sc.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Chinese Deep Space Network (CDSN)** — англоязычный термин, не имеющий аналога в русском языке. **Китайская сеть дальней космической связи** — дословный перевод с английского на русский.</small>

The **Chinese Deep Space Network (CDSN)** is a network of large antennas and communication facilities that supports the lunar and interplanetary spacecraft missions of China. It is managed by the China Satellite Launch and Tracking Control General (CLTC).

| |
|:--|
|[![](f/gs/cdsn_pic1t.webp)](f/gs/cdsn_pic1.webp)|

As of 2007, the network consisted of:

1. Ground control stations in Kashgar and Qingdao (in the Shandong province).
1. 18 meter antennas in Qingdao and Kashgar. [Bands](comms.md): <mark>TBD</mark>
1. A 50-meter antenna at Miyun (~116°E), near Beijing. [Bands](comms.md): <mark>TBD</mark>
1. A 40-meter antenna in Yunnan (~101°E). [Bands](comms.md): <mark>TBD</mark>

Planned improvements by 2012, to support Chang'e 3 and Chang'e 4, include:

1. Upgrades to the ground facilities at Kashgar and Qingdao, and a deep-space ground control station at Jiamusi.
1. A new 35-meter antenna at the Kashgar station. [Bands](comms.md): <mark>TBD</mark>
1. A 64-meter antenna in Jiamusi. (~130°E) [Bands](comms.md): <mark>TBD</mark>

As of 2017, China was constructing an additional ground station in South America, in the Neuquen province of Argentina (~70°W), with a 50 million-dollar investment. The facility, a part of China’s Lunar Exploration Program. It was inaugurated in October 2017. The station is seen by some as a symbol of China’s increased role in South America’s politics and economy.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**`Наземная станция (НС):`**<br> … <br><br> [CDSN](cdsn.md) ~~ [DSN](dsn.md) ~~ [ESTRACK](estrack.md) ~~ [IDSN](idsn.md) ~~ [SSC_GGSN](ssc_ggsn.md) ~~ [UDSC](udsc.md)|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Chinese_Deep_Space_Network>



## The End

end of file
