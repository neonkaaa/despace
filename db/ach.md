# Anechoic chamber
> 2020.05.14 [🚀](../../index/index.md) [despace](index.md) → [Test](test.md), [EMC](emc.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small> ・**Anechoic chamber (ACH)** — EN term. **Безэховая камера (БЭК)** — RU analogue.<br> ・**Anechoic gas chamber (ACHG)** — EN term. **Безэховая газовая камера (БЭКГ)** — RU analogue.</small>

**Anechoic chamber (ACH)** (an‑echoic meaning 「non‑reflective, non‑echoing, echo‑free」) is a room designed to completely absorb reflections of either sound or electromagnetic waves.

They are also often isolated from waves entering from their surroundings. This combination means that a person or detector exclusively hears direct sounds (no reverberant sounds), in effect simulating being inside an infinitely large room.

They’re two types of chambers:

1. Acoustic anechoic chambers;
1. Radio‑frequency anechoic chambers.

They are also often isolated from waves entering from their surroundings. The size of the chamber depends on the size of the objects and frequency ranges being tested.



## Acoustic anechoic chambers
Anechoic chambers are commonly used in acoustics to conduct experiments in nominally 「free field」 conditions, free‑field meaning that there are no reflected signals. All sound energy will be traveling away from the source with almost none reflected back. Common anechoic chamber experiments include measuring the transfer function of a loudspeaker or the directivity of noise radiation from industrial machinery. In general, the interior of an anechoic chamber is very quiet, with typical noise levels in the 10 ‑ 20 dBA range.



## Radio‑frequency anechoic chambers
The interior surfaces of the radio frequency (RF) are covered with radiation absorbent material  instead of acoustically absorbent material. Uses for RF anechoic chambers include testing antennas, radars, and is typically used to house the antennas for performing measurements of antenna radiation patterns, electromagnetic interference.

Performance expectations (gain, efficiency, pattern characteristics, etc.) constitute primary challenges in designing stand alone or embedded antennas. Designs are becoming ever more complex with a single device incorporating multiple technologies such as cellular, WiFi, Bluetooth, LTE, MIMO, RFID and GPS.

|Acoustic anechoic chambers|Acoustic anechoic chambers, volume — 2 000 m³, useful volume — 1 000 m³. University of Applied Sciences Dresden, German|
|:--|:--|
|[![](f/tests/anechoic_chambert.webp)](f/tests/anechoic_chamber.webp)|[![](f/tests/anechoic_chamber_schalltoter_raum_tu_dresdent.webp)](f/tests/anechoic_chamber_schalltoter_raum_tu_dresden.webp)|



## Docs/Links
|**Sections & pages**|
|:--|
|**【[Test](test.md)】**<br> [JTAG](jtag.md) ~~ [Proto fligt model](pfm.md) ~~ [Безэховая камера](ach.md) ~~ [Валидация](vnv.md) ~~ [Класс чистоты](clean_lvl.md) ~~ [КПЭО](ctpr.md) ~~ [Перечень методик испытаний](list_tp.md) ~~ [Программа и методика испытаний](pmot.md) ~~ [Опытный образец](pilot_sample.md) ~~ [Циклограмма](obc.md) ~~ [Штатный образец](flight_unit.md) ~~ [ЭО](test.md) ~~ [Экспериментально‑теоретический метод](etetm.md)|
|**`Электромагнитная совместимость (ЭМС):`**<br> [Безэховая камера](ach.md) ~~ [Помехи](emi.md) (EMI, RFI) ~~ [СКЭ](elmsys.md)|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Anechoic_chamber>


## The End

end of file
