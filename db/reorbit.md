# ReOrbit
> 2022.03.04 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/r/reorbit_logo1t.webp)](f/c/r/reorbit_logo1.webp)|<info@reorbit.space>, +46766325518, Fax …;<br> *Itälahdenkatu 22 A, 00210 Helsinki, Finland*<br> <https://www.reorbit.space> ~~ [LI ⎆](https://www.linkedin.com/company/reorbit) ~~ [X ⎆](https://twitter.com/ReOrbitOy)|
|:--|:--|
|**Business**|R&D for LEO / GEO sats|
|**Mission**|Redefine. Reuse. Reorbit.|
|**Vision**|To enable the next generation of space applications by adopting an approach that transforms the traditional thought of making rigid & single‑use satellites into reusable, flexible, & low‑cost space systems|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO, Founder — Sethu Saveda Suvanam<br> ・CTO — Ignacio Chechile|

**ReOrbit** is bringing technology together to shape the future of space exploration. We develop a set of technologies to make spacecraft platforms modular & configurable to accommodate different types of payloads, & equipped with built‑in autonomous orbital capabilities & a software‑defined architecture.

ReOrbit provides an advanced multi‑purpose, highly software‑defined satellite platform scalable from micro to large mission by being able to manage various applications & orbits (LEO, MEO, GEO & deep space), with a dedicated payload volume on the platform.

Optimised for autonomy, availability, connectivity & fault‑tolerance, ReOrbit delivers a smart satellite, enabling functions such as autonomous flight, close proximity operations, docking, interconnectivity & distributed systems.

Providing satellite as a service, ReOrbit acts as an integrator of hardware systems, enabled by advanced software & simulation tools. ReOrbit manages payload development, qualification, insurance & risk management, launch service as well as ground service.

<p style="page-break-after:always"> </p>

## The End

end of file
