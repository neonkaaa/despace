# Space Entertainment
> 2022.02.01 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/s/space_e11t_logo1t.webp)](f/c/s/space_e11t_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *2-17-3 Kita-Aoyama, Minato-ku, Tokyo, Japan*<br> <https://space-entertainment.co.jp> ~~ [LI ⎆](https://www.linkedin.com/company/space-entertainment-inc)|
|:--|:--|
|**Business**|Space entertainment activities|
|**Mission**|Turn the universe into a playground for tomorrow|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO — Kaho Sakakibara<br> ・CBO — Yasunori Yamazaki|

**Space Entertainment Co., Ltd.** is a Japanese company aimed for space entertainment & promotion activities. Founded 2021.02.

> The Earth is full of various issues such as population problems & environmental problems.<br> Humanity's advance into space is essential for us to continue to prosper & prosper, but while the rocket & satellite data businesses are expanding, the growth rate of the entire space industry is still insufficient.<br> Space Entertainment provides an entertainment experience that anyone can enjoy using various space technologies, boosts people's enthusiasm for space, & promotes humankind's advance into space by making space closer.<br> And we will create a world where human beings can live happily in various places in the universe.

<p style="page-break-after:always"> </p>

## The End

end of file
