# Нитрон
> 2019.12.16 [🚀](../../index/index.md) [despace](index.md) → [SGM](sc.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Нитрон** — русскоязычный термин. **Acrylic fiber** — англоязычный эквивалент.</small>

**Нитрон** *(Акриловое волокно, Акрил)* — синтетическое волокно, получаемое путём формования из растворов полиакрилонитрила или его производных. Само волокно прочное, жёсткое, устойчивое к окрашиванию. Содержание добавок варьируется в зависимости от типа волокна. Применяют для изготовления трикотажных изделий, костюмных тканей, в том числе используется в виде напыления, для придания материалам водоотталкивающих свойств. Также применяется в различных технических изделиях.

**Стеклонитрон** — нитрон со стекловолокном.

**Acrylic fibers** are synthetic fibers made from a polymer (polyacrylonitrile) with an average molecular weight of -100,000, about 1900 monomer units. For a fiber to be called "acrylic" in the US, the polymer must contain at least 85 % acrylonitrile monomer. Typical comonomers are vinyl acetate or methyl acrylate. DuPont created the 1st acrylic fibers in 1941 and trademarked them under the name Orlon. It was 1st developed in the mid-1940s but was not produced in large quantities until the 1950s. Strong and warm, acrylic fiber is often used for sweaters and tracksuits and as linings for boots and gloves, as well as in furnishing fabrics and carpets. It is manufactured as a filament, then cut into short staple lengths similar to wool hairs, and spun into yarn.

Modacrylic is a modified acrylic fiber that contains at least 35 % and at most 85 % acrylonitrile monomer. The comonomers vinyl chloride, vinylidene chloride or vinyl bromide used in modacrylic give the fiber flame retardant properties. End-uses of modacrylic include faux fur, wigs, hair extensions and protective clothing.

1. **Достоинства:**
   1. прочность
   1. износоустойчивость
   1. дешевизна
1. **Недостатки:**
   1. низкая эластичность
   1. негигиеничность



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Structures, gears, materials (SGM)](sc.md)】**<br> [Гермоконтейнер](гермоконтейнер.md) ~~ [Датчик](sensor.md) ~~ [Задел](margin.md) ~~ [Изделие](unit.md) ~~ [Испарение материалов](matc.md) ~~ [Кавитация](cavitation.md) ~~ [КЗУ](cinu.md) (ВБУ КТ) ~~ [КХГ](cgs.md) ~~ [Контейнеры для транспортировки](ship_contain.md) ~~ [Крейцкопф](crosshead.md) ~~ [Номинал](nominal.md) ~~ [ПУС](lag.md) ~~ [ПНА, ПОНА, ПСНА](devd.md) ~~ [Резерв](reserve.md) ~~ [Слайс](слайс.md) ~~ [ТСП](tsp.md) ~~ [Типичные формы КА](sc.md) ~~ [Толкатель](толкатель.md) ~~ [Унификация](commonality.md)|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Acrylic_fiber>


## The End

end of file
