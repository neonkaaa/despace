# Space Cubics
> 2020.07.20 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/s/spacecubics_logo1t.webp)](f/c/s/spacecubics_logo1.webp)|<contact@spacecubics.com>, +81-50-7112-6213, Fax …;<br> *2 Chome−１−番地, Minami 3 Johigashi, Chuo Ward, Sapporo, 〒060-0053 Hokkaido, Japan*<br> <https://spacecubics.com> ~~ [FB ⎆](https://www.facebook.com/101922994607924) ~~ [GitHub ⎆](https://github.com/spacecubics) ・ [JAXA ⎆](https://aerospacebiz.jaxa.jp/en/spacecompany/spacecubics) ~~ [LI ⎆](https://www.linkedin.com/company/spacecubics) ~~ [X ⎆](https://www.linkedin.com/company/spacecubics)|
|:--|:--|
|**Business**|Consult on space project management. Design & develop space product (OBC, [application software](soft.md), FPGA…). Sale of space‑graded COTS computer with auto recovery function.|
|**Mission**|…|
|**Vision**|Make it cheaper. Make it easier. Everyone in space.|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO — Yasushi Shoji<br> ・CTO — Daisuke Sasaki<br> ・CMO — Masayuki Goto|

**Space Cubics, LLC.** produces a high‑reliability [computer](obc.md) for satellites or other space products, incorporating techniques accumulated through operations at International Space Station (ISS) by [JAXA](jaxa.md). Our products are easy to use, ready to use for startups & serious space industries. Founded in 2018.



## The End

end of file
