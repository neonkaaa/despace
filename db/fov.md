# Поле зрения
> 2019.05.10 [🚀](../../index/index.md) [despace](index.md) → **[OE](sc.md)**, [ЗД](sensor.md), [СД](sensor.md)
  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Поле зрения (ПЗр, ПЗ)** — русскоязычный термин. **Field of view (FOV)** — англоязычный эквивалент.</small>

**Поле зрения (ПЗр, ПЗ)** — угловое пространство, видимое глазом или оптическим элементом при фиксированном взгляде и неподвижной голове.

Поле зрения оптического прибора зависит от его увеличения — чем выше увеличение, тем меньше поле зрения.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**`Бортовая аппаратура (БА):`**<br> [PDD](pdd.md) ~~ [Антенна](antenna.md) ~~ [АПС](hns.md) ~~ [БУ](eas.md) ~~ [ЗУ](ds.md) ~~ [Изделие](unit.md) ~~ [КЛЧ](clean_lvl.md) ~~ [ПЗР](fov.md) ~~ [ПО](soft.md) ~~ [Прототип](prototype.md) ~~ [Радиосвязь](comms.md) ~~ [СКЭ](elmsys.md) ~~ [ССИТД](tsdcs.md) ~~ [СИТ](etedp.md) ~~ [УГТ](trl.md) ~~ [ЭКБ](elc.md) ~~ [EMC](emc.md)|
|**`Звёздный датчик (ЗД):`**<br> [Видимая звёздная величина](app_mag.md) ~~ [ПЗр](fov.md)<br>~ ~ ~ ~ ~<br> **Европа:** [ASTRO 15](st_lst.md) (6.15) ~~ [Hydra](st_lst.md) (4.6) ~~ [ASTRO 10](st_lst.md) (3.8) ~~ [A-STR](st_lst.md) (3.55) ~~ [AA-STR](st_lst.md) (2.6) ~~ [HE-5AS](st_lst.md) (2.2) ~~ [ASTRO APS](st_lst.md) (2) ~~ [Horus](st_lst.md) (1.6) ~~ [T2](st_lst.md) (0.8) ~~ [T1](st_lst.md) (0.6 ‑ 1) ~~ [Auriga](st_lst.md) (0.21)  ▮  **РФ:** [348К](st_lst.md) (3.45) ~~ [360К](st_lst.md) () ~~ [АД-1](st_lst.md) (3.8) ~~ [БОКЗ-МФ](st_lst.md) (2.8) ~~ [мБОКЗ-2](мбокз_2.md) (1.5) ~~ [SX-SR-MicroBOKZ](st_lst.md) (0.5)  ▮  **США:** [HAST](st_lst.md) (7.7) ~~ [CT-2020](st_lst.md) (3) ~~ [µSTAR](st_lst.md) (2.1) ~~ [MIST](st_lst.md) (0.55) |
|**`Солнечный датчик (СД):`**<br> [ПЗр](fov.md) <br>~ ~ ~ ~ ~<br> (КА) **Европа:** [FSS](ss_lst.md) (650) ~~ [FSS](ss_lst.md) (50 ‑ 375) ~~ [CSS](ss_lst.md) (275) ~~ [CoSS](ss_lst.md) (15 ‑ 24)  ▮  **РФ:** [ТДС](ss_lst.md) (2 300) ~~ [347К](ss_lst.md) (700) ~~ [ОСД](ss_lst.md) (650) ~~ [СДП-1](ss_lst.md) (400) ~~ [SX-SUNR-01](ss_lst.md) (40)<br> *(Кубсаты) **США:** [MSS](ss_lst.md) (0.036) ~~ [CSS](ss_lst.md) (0.02)**|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Field_of_view>


## The End

end of file
