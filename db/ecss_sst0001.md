# ECSS‑S‑ST‑00‑01C Rev.1 (11‑Oct‑2023)
> 2023.10.11 [🚀](../../index/index.md) [despace](index.md) → [Doc](doc.md), [ECSS](ecss.md), [SC](sc.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

## 1. Introduction & Scope

**「Glossary of terms」**

The ECSS Glossary is part of the series of ECSS Standards intended to be applied together for the management, engineering, product assurance & sustainability in space projects & applications. ECSS is a cooperative effort of the European Space Agency, national space agencies & European industry associations for the purpose of developing & maintaining common standards.

Requirements in ECSS Standards are defined in terms of what shall be accomplished, rather than in terms of how to organize & perform the necessary work. This allows existing organizational structures & methods to be applied where they are effective, & for the structures & methods to evolve as necessary without rewriting the standards.

This document has been prepared by the ECSS Glossary Task Force, reviewed & approved by the ECSS Technical Authority.

Traceability to the previous ECSS Glossary 「ECSS‑S‑ST‑00‑01C」 is ensured through tracked changes & the matrix given in Annex A.

ECSS does not provide any warranty whatsoever, whether expressed, implied, or statutory, including, but not limited to, any warranty of merchantability or fitness for a particular purpose or any warranty that the contents of the item are error‑free. In no respect shall ECSS incur any liability for any damages, including, but not limited to, direct, indirect, special, or consequential damages arising out of, resulting from, or in any way connected to the use of this Standard, whether or not based upon warranty, business agreement, tort, or otherwise; whether or not injury was sustained by persons or property or otherwise; & whether or not loss was sustained from, or arose out of, the results of, the item, or any services that may be provided by ECSS.

This document controls the definition of all common terms used in the European Cooperation for Space Standardization (ECSS) Standards System. Terms specific to a particular ECSS Standard are defined in that standard.

This document does not include the definition of terms used with their common meaning. In this case, the definition from the Oxford English Dictionary applies.

**Bibliography:**

1. ECSS‑S‑ST‑00 — ECSS system — Description, implementation & general requirement.
3. [ECSS‑E‑HB‑10‑02](ecss_ehb1002.md) — Space engineering — Verification guidelines
5. ECSS‑Q‑ST‑70 — Space product assurance — Materials, mechanical parts & processes
7. EN 45020:1998 — Standardization & related activities — General vocabulary
9. EN 45020:2006 — Standardization & related activities — General vocabulary corrected (ISO/IEC Guide 2:2004)
10. IEC 60050 — International Electrotecnical Vocabulary (online: https://www.electropedia.org/)
11. IEC 60194 (1999‑04) — Printed board design, manufacture & assembly — Terms & definitions
12. ISO 3534‑2:2006 — Statistics — Vocabulary & symbols — Part 2: Applied statistics
13. ISO 9000:2005 — Quality management systems — Fundamentals & vocabulary
14. ISO 9000:2015 — Quality management systems — Fundamentals & vocabulary
15. ISO 10007:2003 — Quality management systems — Guidelines for configuration management
16. ISO 10795:2019 — Space systems — Programme management & quality — Vocabulary
17. ISO 14623:2003 — Space systems — Pressure vessels & pressurized structures — Design & operation
18. ISO 14625:2007 — Space systems — Ground support equipment for use at launch, landing or retrieval sites — General requirements
19. ISO 15388:2012 — Space systems — Contamination & cleanliness control
20. ISO 17666:2016 — Space systems — Risk management
21. ISO 24113:2019 — Space systems — Space debris mitigation requirements
22. IEC Multilingual Dictionary — 2001 edition


## 2. Terms, definitions & abbreviated terms

### 2.1. Terms & definitions
When using the ECSS standards, the following is the order of precedence of documents as the source of definition of terms:

1. the standard in question
2. the present Glossary of terms
3. the Oxford English dictionary.

A term used within a definition, which is defined elsewhere in this document is shown in boldface. A boldface term can be replaced within the definition by its own definition.

A document reference shown after a definition in square brackets, [ ], indicates that this definition is reproduced from the referenced document. For example:

> 2.3.17 auditee organization being audited [ISO 9000:2005]

All terms & their definitions appear in alphabetic order in clause 2.3 of this Glossary. However, wherever it is considered important to present together a set of terms that are interrelated (i.e. constitute a particular 「view」), these terms & their definitions are repeated in standalone sections of this Glossary or in Annexes. For example, clause 2.2 collects together all terms that relate to the breakdown of the overall Space System.


### 2.2. Space system breakdown

#### 2.2.1. Introduction

ECSS‑S‑ST‑00C defines the highest‑level system within a space project — i.e. the one at the mission‑level — as the 「Space System」. The purpose of the present clause is to identify the breakdown of a typical space system & to define a set of standard terms for the constituent levels within the breakdown (see Figure 2‑1).

In so doing, it is acknowledged that each distinct domain (i.e. space, ground & launcher) already has its own domain‑specific terminology for its internal entities e.g. elements & systems. In the case of the launcher domain, this terminology has been formally defined & agreed at programme‑level. It is not the intention to define new terms in this Glossary to supersede those already in universal use. Rather, the intention is to define a standard set of terms for the levels of the space system breakdown & then to show where the domain‑specific entities fit into these levels. To this end, Annex B contains examples of entities from the three principal space system segments, mapped to the space system breakdown levels defined below.

The terms are defined in clause 2.2.2 to 2.2.7 & are listed not in alphabetic order but according to the hierarchy defined in Figure 2‑1: Space system breakdown below.

1. 2.2.2 defines generic terms
2. 2.2.3 defines the space system
3. 2.2.4 defines terms relating to the space segment
4. 2.2.5 defines terms relating to the ground segment
5. 2.2.6 defines terms relating to the launch segment
6. 2.2.7 defines terms relating to the support segment

【**Fig.2‑1:** Space system breakdown】  
![](f/doc/ecss/ecss_sst0001/21.png)


#### 2.2.2. Definitions for generic terms

1. **System** — Set of interrelated or interacting **functions** constituted to achieve a specified objective.
2. **Segment** — Set of **elements** or combination of **systems** that fulfils a major, self‑contained, subset of the **space mission** objectives
3. **Element** — Combination of integrated **equipment**, **components** & **parts**. An element fulfils a major, self‑contained, subset of a segment's objectives.
4. **Subsystem** — Part of a **system** fulfilling one or more of its **functions**.
5. **Equipment** — Integrated set of parts & components. An equipment accomplishes a specific function. An equipment is self‑contained & classified as such for the purposes of separate manufacture, procurement, drawings, specification, storage, issue, maintenance or use. The term 「unit」 is synonymous with the term 「equipment」.
6. **Component** — Set of materials, assembled according to defined & controlled processes, which cannot be disassembled without destroying its capability & which performs a simple function that can be evaluated against expected performance requirements. The term 「part」 is synonymous. The term 「part」 is preferred when referring to purely mechanical devices. The term 「component」 is preferred for EEE devices.
7. **Part** — See 「component」.
8. **Material** — Raw, semi‑finished or finished substance (gaseous, liquid, solid) of given characteristics from which processing into a **component** or **part** is undertaken.


#### 2.2.3. Definitions for space system

1. **Space system** — System that contains at least a space,  a ground or a launch segment. Generally a space system is composed of all three segments & is supported by a support segment.


#### 2.2.4. Definitions for space segment

1. **Space segment** — Part of a space system, placed in space, to fulfil the space mission objectives.
2. **Space segment system** — System within a space segment. Examples are given in Annex B.1.
3. **Space segment element** — Element within a space segment. A space segment element can be composed of several space segment elements, e.g. a spacecraft is composed of instruments, a payload module & a service module. Examples are given in Annex B.1.
4. **Stand‑alone space segment element** — Space segment element that performs its mission independently of the rest of the space system. For example: satellite, rover, lander.
5. **Embedded space segment element** — Space segment element that performs its mission as part of another space segment element. For example: platform, module, instrument, payload.
6. **Space segment subsystem** — Subsystem within a space segment. Examples are given in Annex B.1.
7. **Space segment equipment** — Equipment within a space segment. Examples are given in Annex B.1.


#### 2.2.5. Definitions for ground segment

1. **Ground segment** — Part of a space system, located on ground, which monitors & controls space segment element(s). A ground segment is composed of one or more ground segment elements.
2. **Ground segment system** — System within a ground segment. Examples are given in Annex B.2.
3. **Ground segment element** — Element within a ground segment. A ground segment element can be composed of several ground segment elements, e.g. a ground station network is a ground segment element that can be composed of a set of ground stations & a communication network. Examples are given in Annex B.2.
4. **Ground segment subsystem** — Subsystem within a ground segment. Examples are given in Annex B.2.
5. **Ground segment equipment** — Equipment within a ground segment. Examples are given in Annex B.2.


#### 2.2.6. Definitions for launch segment

1. **Launch segment** — Part of a space system which is used to transport space segment element(s) into space. A launch segment is composed of a launcher system with it specific launch complex, supported by a launch range. A launch segment is composed of the integrated launcher & the facilities needed for manufacturing, testing & delivering launcher elements.
2. **Launch segment system** — System within a launch segment. Examples are given in Annex B.3
3. **Launch segment element** — Element within a launch segment. A launch segment element can be composed of several launch segment elements, e.g. a launcher is a launch segment element that is composed of several launch segment elements, such as stage, engine & upper part. Examples are given in Annex B.3.
4. **Launch segment subsystem** — Subsystem within a launch segment. Examples are given in Annex B.3.
5. **Launch segment equipment** — Equipment within a launch segment. Examples are given in Annex B.3.


#### 2.2.7. Definitions for support segment

1. **Support segment** — Generic infrastructure & services used to support the development & operation of space system elements. Examples are ground stations & associated networks, orbit computing facilities, test centres, astronaut centre, launch ranges (e.g. Plestek, Baikonour, Guiana Space Centre). Items can be part of other segments during their development & later become part of the support segment when used (e.g. a tracking network).


### 2.3. Terms & definitions

1. **Absorbed dose** — Energy absorbed locally per unit mass as a result of radiation exposure which is transferred through ionisation, displacement damage & excitation. It is the sum of the ionising dose & non‑ionising dose. It is normally represented by D, & in accordance with the definition, it can be calculated as the quotient of the energy imparted due to radiation in the matter in a volume element & the mass of the matter in that volume element. It is measured in units of gray, ㏉ (1 ㏉ = 1 J/㎏ (= 100 rad)). The absorbed dose is the basic physical quantity that measures radiation exposure. A portion of the energy absorption can result in damage to the lattice structure of solids through displacement of atoms, & this is commonly referred to as Non‑Ionizing Energy Loss (NIEL).
2. **Acceptance** — Act by which the customer agrees that the product is designed & produced according to its specifications & the agreed deviations & waivers, & it is free of defects when delivered by the supplier
3. **Acceptance process** — Part of the verification process which demonstrates that the product meets specified acceptance margins' The term 「acceptance」 is used as the short form of 「customer acceptance」.
4. **Acceptance margin** — Increase of the environmental, mechanical, thermal, electrical, EMC, or operational extremes above the worst case levels predicted over the specified product lifetime for the purpose of workmanship verification. Margins can include an increase in level or range, an increase in duration or cycles of exposure, as well as any other appropriate increase in severity. For thermal acceptance margin refer to [ECSS‑E‑ST‑31](ecss_est31.md).
5. **Acceptance test level** — test level reflecting the maximum level expected to be encountered during the flight product lifetime increased by acceptance margins
6. **Accident** — undesired event arising from operation of any project‑specific item that results in: 1) human death or injury, 2) loss of, or damage to, project hardware, software or facilities that can then affect the accomplishment of the mission, 3) loss of, or damage to, public or private property, or 4) detrimental effects on the environment. Accident & mishap are synonymous.
7. **Active redundancy** — redundancy where all entities are operating & the system can continue to operate without downtime or defects despite the loss of one or more entities.
8. **Actuator** — device that transforms an input signal into motion.
9. **Alert** — formal notification to users, informing them of failures or problems that can affect more than one user, or can recur in other projects or circumstances, if no preventive actions are taken. An alert describes the observed failure or problem, its cause, the actions to be taken to correct it & to prevent its recurrence, as well as comments from the manufacturer of the affected product. An alert can also be raised when a deficiency in the specified requirements, which can affect the fitness for purpose in the defined application, has been identified.
10. **Allowable load** — maximum load that can be accommodated in a structural part for a given operating environment without rupture, collapse, detrimental deformation or unacceptable crack growth. (Adapted from ISO 14623:2003)
11. **Analysis** — verification method utilizing techniques & tools to confirm that verification requirements have been satisfied. Examples of techniques & tools are mathematical models, compilation similarity assessments & validation of records. (Adapted from ISO 10795:2019)
12. **Applicable document** — document that contains provisions which, through reference in the source document, constitute additional provisions of the source document. (Adapted from ISO 10795:2019)
13. **Approval** — formal agreement by a designated management official to use or apply an item or proceed with a proposed course of action. Approvals must be documented. Approval implies that the approving authority has verified that the item conforms to its requirements.
14. **Assembly** — physically combining components, equipment or elements to form a larger entity
15. **Assurance** — planned & systematic activities implemented, & demonstrated as needed, to provide adequate confidence that an entity fulfils its requirements
16. **Audit** — systematic, independent & documented process for obtaining objective evidence & evaluating it objectively to determine the extent to which audit criteria are fulfilled. (1) The fundamental elements of an audit include the determination of the conformance of an object according to a procedure carried out by personnel not being responsible for the object audited. (2) An audit can be an internal audit (first party), or an external audit (second party or third party), & it can be a combined audit or a joint audit. (3) Internal audits, sometimes called first‑party audits, are conducted by, or on behalf of, the organization itself for management review & other internal purposes, & can form the basis for an organization’s declaration of conformance. Independence can be demonstrated by the freedom from responsibility for the activity being audited. (4) External audits include those generally called second‑ & third‑party audits. Second party audits are conducted by parties having an interest in the organization, such as customers, or by other persons on their behalf. Third‑party audits are conducted by external, independent auditing organizations, such as those providing certification/registration of conformance or governmental agencies. (5) The terms 「conformance」 & 「conformity」 are synonymous. The term 「conformance」 is recommended in ECSS. [ISO 9000:2015]
17. **Audit criteria** — set of policies, procedures or requirements used as a reference against which objective evidence is compared. [ISO 9000:2015]
18. **Audit evidence** — records, statements of fact or other information which are relevant to the audit criteria & verifiable. [ISO 9000:2015]
19. **Auditee** — organization being audited. [ISO 9000:2015]
20. **Auditor** — person with the demonstrated personal attributes & competence to conduct an audit. (Adapted from ISO 9000:2005)
21. **Availability** — ability of an item to be in a state to perform a required function under given conditions. Adapted from IEC 60050. (1) This ability depends on the combined aspects of the reliability performance, the maintainability performance & the maintenance support performance. (2) Required external resources, other than maintenance resources do not affect the availability performance of the item. (3) When referring to the measure for availability, the preferred term is 「instantaneous availability」.
22. **Backward contamination** — contamination of the terrestrial biosphere by extra‑terrestrial life forms in the course of spaceflight missions.
23. **Bakeout** — activity of increasing the temperature of hardware to accelerate its outgassing rates with the intent of reducing the content of molecular contaminants within the hardware. Bakeout is usually performed in a vacuum environment, but can be done in a controlled atmosphere.
24. **Batch** — quantity produced at one operation.
25. **Biodiversity** — types of microorganisms, identified with specified assays.
26. **Blister** — delamination in the form of a localized swelling & separation between any of the layers of a lamination base material, or between base material & conductive foil or protective coating. [IEC 60194 (1999‑04)][IPC‑T‑50‑M]
27. **Bremsstrahlung** — high‑energy electromagnetic radiation in the X‑γ energy range emitted by charged particles slowing down by scattering of atomic nuclei. (1) The primary particle is ultimately absorbed while the bremsstrahlung can be highly penetrating. In space, the most common source of bremsstrahlung is electron scattering. (2) Its energy is continuously distributed down from the energy of the incident particle.
28. **Business agreement** — legally binding agreement, for the supply of goods or services, between two or more actors in the customer‑supplier chain. Business agreements are recorded in a variety of forms, such as: Contracts, Memoranda of understanding, Inter‑governmental agreements, Inter‑agency agreements, Partnerships, Bartering agreements, & Purchase orders.
29. **Calibration** — determination of the error values of measuring instruments and, if necessary, other metrological properties. The metrological use of the term 「calibration」 is often extended to include operations such as adjustments, scale graduation, etc. This use is deprecated.
30. **Capability** — ability of an organization, system or process to realize a product that will fulfil the requirements for that product. Process capability terms in the field of statistics are defined in ISO 3534‑2. [ISO 9000:2005]
31. **catastrophic** — resulting in loss of life, life‑threatening, permanently disabling injury or occupational illness, loss of system, loss of an interfacing manned flight system, loss of launch site facilities or severe detrimental environmental effects
32. **certification** — procedure by which a party gives formal assurance that a person or an organization acts, or a product is, in compliance with specified requirements. Certification can be carried out by a first, second or third party.
33. **clean area** — area under contamination control. Examples of clean areas are cleanrooms, integration tent, gloves box.
34. **cleanliness** — level of particulate & molecular contamination
35. **cleanliness controlled area** — environmentally controlled area, operated as a cleanroom, with two pre‑filter stages but without the final stage of high‑efficiency particulate air filters (or better) used in cleanrooms
36. **cleanroom** — clean area controlled according to specified levels. Specified levels are humidity, temperature, particulates number versus size & volume & chemical contamination.
37. **cold redundancy** — redundancy where one entity is operating & the others are powered off
38. **collected volatile condensable material** — mass that outgasses from a material & subsequently condenses on a collector, expressed as a percentage of the initial specimen mass. [ISO 15388:2012]
39. **commandability** — provision of adequate control functions to configure the on‑board systems for the execution of nominal mission operations, failure detection, identification, isolation, diagnosis & recovery, & maintenance operations
40. **commissioning** — verification & validation activities conducted after the launch & before the entry into operational service either on the space segment elements only or on the overall system (including the ground segment elements)
41. **common cause failure** — failure of multiple items occurring from a single common cause
42. **common mode failure** — common cause failure of multiple identical items that fail in the same mode. (1) Common mode failures are a particular case of common cause failures. (2) An example of common mode is the same observed end effect or state in identical items once they failed due to the same cause. For instance, all command receivers in a satellite failed in lock status due to the radiation sensitivity.
43. **component** — set of materials, assembled according to defined & controlled processes, which cannot be disassembled without destroying its capability & which performs a simple function that can be evaluated against expected performance requirements. (1) The term 「part」 is synonymous. (2) The term 「part」 is preferred when referring to purely mechanical devices. (3) The term 「component」 is preferred for EEE devices.
44. **configurable code** — code (source code or executable code) that can be tailored by setting values of parameters. This definition covers in particular classes of configurable code obtained by the following configuration means: (1) configuration based on the use of a compilation directive; (2) configuration based on the use of a link directive; (3) configuration performed through a parameter defined in a configuration file; (4) configuration performed through data defined in a database with impact on the actually executable parts of the software (e.g. parameters defining branch structures that result in the non‑execution of existing parts of the code).
45. **configuration** — interrelated functional and/or physical characteristics of a product defined in configuration documents subject to configuration management. Adapted from ISO 10007:2003.
46. **configuration baseline** — approved product configuration information that establishes the characteristics of a product or service at a point in time that serves as reference for activities throughout the life cycle of the product or service. [ISO 9000:2015]
47. **configuration control** — coordinated activities for controlling modifications to a configuration baseline. Requests for deviation are also considered modifications to a baseline.
48. **configuration document** — document that defines the requirements for function, design, build, production, & verification for a configuration item. For space standards, configuration documents can include documents relating to operation & disposal of the configuration item.
49. **configuration identification** — coordinated activities to establish rules for configuration item selection, configuration baseline content definition, & product & document identifiers definition
50. **configuration item** — aggregation of hardware, software, processed materials, services or any of its discrete portions, that is designated for configuration management & treated as a single entity in the configuration management process. A configuration item can contain other configuration item(s).
51. **configuration management** — activity for establishing & maintaining consistent records of the performance parameters of a product & its functional & physical attributes compared to product design & operational requirements. Adapted from ISO 10007:2003. Configuration management is applied throughout the entire life cycle of the product (i.e. development, production, deployment, operation & disposal).
52. **configuration status accounting** — formalized recording & reporting of product characteristics & configuration information, the status of applicable changes & the status of their implementation. Adapted from ISO 10007:2003.
53. **configuration verification** — coordinated activities to determine the conformance of the configuration item to its configuration document(s)
54. **configured item** — any level of product whose functional or physical characteristics are recorded in a retrievable, consistent manner
55. **conformal coating** — thin protective coating which conforms to the shape of the assembly to be coated
56. **conformance** — fulfilment of a requirement. The term 「conformity」 is synonymous, but deprecated in ECSS.
57. **contaminant** — undesirable molecular or particulate matter. This includes microbiological matter.
58. **contamination** — introduction of contaminant to an item or to the environment of interest
59. **contract** — legally enforceable business agreement in which payment is part of the conditions
60. **corrective action** — action to eliminate the cause of a detected nonconformance & to prevent recurrence. Adapted from ISO 9000:2015. (1) There can be more than one cause for a non‑conformance. (2) Corrective action is taken to prevent recurrence whereas preventive action is taken to prevent occurrence.
61. **commercial off‑the‑shelf** — equipment, including hardware & associated software or procedures, that is commercially available from current industry inventory. [ISO 14625:2007]. In common usage, COTS equipment is understood to not be manufactured, inspected or tested in accordance with military or space standards. COTS equipment is generally cheaper & easier to procure, yet has associated risk in terms of quality & performance in the space environment.
62. **critical** — characteristic of a process, process condition, parameter, requirement or item that deserves control & special attention in order to meet the objectives (e.g. of a mission) within given constraints
63. **critical item** — potential threat to the schedule, cost, performance & quality of a project or programme that is controlled by a specific action plan in order to mitigate emanating risks & to prevent undesirable consequences. Examples of critical items are: (1) item not qualified or validated for the application in question (or has caused problems previously which remained unresolved). (2) item for which it is difficult to demonstrate design performance. (3) item highly sensitive to the conditions under which it is produced or used (e.g. contamination, radiation). (4) item having the potential to degrade the quality of the product significantly, & hence the ability of the end‑product to accomplish defined mission objectives. (5) item for which major difficulties or uncertainties are expected in the procurement, manufacturing, assembly, inspection, test, handling, storage & transportation, that have the potential to lead to a major degradation in the quality of the product.
64. **critical path** — chain of activities that determines the earliest completion of the project. As a consequence, any delay of one task belonging to the critical path extends the project duration.
65. **customer** — organization or person that receives a product as part of a business agreement. A customer can be internal or external to the supplier organization.
66. **defect** — non‑fulfilment of a requirement related to an intended or specified use. (1) The distinction between the concepts defect & nonconformance is important as it has legal connotations, particularly those associated with product liability issues. Consequently the term 「defect」 should be used with extreme caution. (2) The intended use as intended by the customer can be affected by the nature of the information, such as operating or maintenance instructions, provided by the supplier.
67. **delamination** — physical separation between two material layers, which are joined in design. See also 「blister」.
68. **delta qualification** — qualification performed on a product which has undergone minor design modifications or has been qualified to operate in environments different than those specified
69. **dependability** — the extent to which the fulfilment of a required function can be justifiably trusted. (1) Its main components are reliability, availability & maintainability. (2) Dependability shall be considered in conjunction with safety.
70. **derating** — action when designing a product to limit the component stresses to specified levels that are below their  ratings in order to increase its reliability
71. **design** — set of information, or the process used to generate the set of information, that defines the characteristics of a product
72. **development** — complete process of elaborating a product from concept to manufacturing including its qualification & final acceptance. Technology development & design production are part of the process (i.e. from phase 0 to phase D).
73. **deviation** — departure from the originally specified requirements for a product, prior to production. Waiver is an a posteriori decision whereas deviation is an a priori decision with respect to the production phase.
74. **discipline** — specific area of expertise within a general subject. The name of the discipline normally indicates the type of expertise (e.g. in the ECSS system, system engineering, mechanical engineering, software & communications are disciplines within the engineering domain).
75. **discrepancy** — departure from expected performance. (1) A discrepancy can be the result of nonconforming hardware or software, or conditions occurring in test set‑up. A discrepancy can be momentary, non‑repeatable, or permanent. (2) Adapted from ISO 10795:2019.
76. **disposal** — actions performed by a spacecraft or launch vehicle orbital stage to permanently reduce its chance of accidental break‑up & to achieve its required long‑term clearance of the protected regions. Actions can include removing stored energy & performing post‑mission orbital manoeuvres. [ISO 24113:2019]
77. **effectiveness** — extent to which planned activities are realized & planned results are achieved. [ISO 9000:2015]
78. **efficiency** — relationship between the result achieved & the resources used. [ISO 9000:2015]
79. **element** — combination of integrated equipment, components & parts. An element fulfils a major, self‑contained, subset of a segment's objectives.
80. **emergency** — situation where hazardous events have occurred with potentially catastrophic or critical consequences requiring an immediate action
81. **embedded space segment element** — space segment element that performs its mission as part of another space segment element. For example: platform, module, instrument, payload.
82. **end item** — product that is deliverable under a business agreement
83. **engineering model** — flight representative model in terms of form, fit & function used for functional & failure effect verification. (1) The engineering model is usually not equipped with high reliability parts or full redundancy. (2) The engineering model is also used for final validation of test facilities, GSE & associated procedures. (3) More detailed information on the build standard & the use of this model is given in [ECSS‑E‑HB‑10‑02](ecss_ehb1002.md).
84. **engineering qualification model** — model, which fully reflects the design of the flight model except for the parts standard, used for functional performance & EMC verification & possibly for qualification. (1) Military grade or lower‑level parts can be used instead of high reliability parts, provided they are procured from the same manufacturer with the same packaging. (2) Functional performance qualification includes verification of procedures for failure detection, isolation & recovery & for redundancy management. (3) The engineering qualification model can also be used for environmental testing if the customer accepts the risk, in which case the qualification model rules apply. (4) More detailed information on the build standard & the use of this model is given in [ECSS‑E‑HB‑10‑02](ecss_ehb1002.md).
85. **environment** — natural conditions & induced conditions that constrain the design definitions or operations of a product. (1) Examples of natural conditions are weather, climate, ocean conditions, terrain, vegetation, dust, light & radiation. (2) Examples of induced conditions are electromagnetic interference, heat, vibration, pollution & contamination.
86. **equipment** — integrated set of parts & components. (1) An equipment accomplishes a specific function.  (2) An equipment is self‑contained & classified as such for the purposes of separate manufacture, procurement, drawings, specification, storage, issue, maintenance or use. (3) The term 「unit」 is synonymous with the term 「equipment」.
87. **fail safe** — design property of a system, subsystem, or component which prevents its failuresfrom resulting in catastrophic or critical consequences (i.e. remain safe after one failure). Adapted from ISO 26871:2020.
88. **failure** — the event resulting in an item being no longer able to perform its required  function. 「Failure」 is an event, as distinguished from 「fault」 which is a state.
89. **failure mode** — mechanism through which a failure occurs. (1) For example,. short‑circuit, open‑circuit, fracture, or excessive wear. (2) This term is equivalent to the term 「fault mode」 in IEC Multilingual Dictionary: 2001 edition.
90. **failure tolerance** — attribute of an item that makes it able to perform a required function in the presence of certain given sub‑item failures
91. **fastener** — device used to physically secure hardware components & parts in place. These include, but are not limited to, bolts, nuts, screws, pins & rivets. [ISO 7176‑19:2022]
92. **fault** — state of an item characterized by inability to perform as required. (1) A fault can be the result of a failure of the item itself or can exist without prior failure. (2) A fault can generate a failure.
93. **fault tolerance** — attribute of an item that makes it able to perform a required function in the presence of certain given sub‑item faults
94. **firmware** — hardware that contains a computer program or data that cannot be changed in its user environment. The computer program & data contained in firmware are classified as software; the circuitry containing the computer program & data is classified as hardware.
95. **flight model** — end product that is intended for flight. (1) The flight model is subjected to formal functional & environmental acceptance testing. (2) More detailed information on the build standard & the use of this model is given in [ECSS‑E‑HB‑10‑02](ecss_ehb1002.md).
96. **flight operations** — all activities related to the planning, execution & evaluation of the control of the space segment when in orbit
97. **flight spare** — spare flight model that could be used in place of the flight model. (1) Exceptionally, a refurbished qualification model can be used as a flight spare. (2) More detailed information on the build standard & the use of this model is given in [ECSS‑E‑HB‑10‑02](ecss_ehb1002.md).
98. **fluence** — time‑integrated flux
99. **flux** — number of particles passing through a given area in a specified time. [ISO 23038:2018]
100. **forward contamination** — contamination of celestial bodies other than the Earth by terrestrial life forms in the course of spaceflight missions
101. **function** — intended effect of a product
102. **function tree** — hierarchical breakdown of a function into successive levels of function
103. **functional analysis** — process that describes completely the functions & their relationships, which are systematically characterised, classified & evaluated
104. **ground segment** — part of a space system, located on ground, which monitors & controls space segment element(s). A ground segment is composed of one or more ground segment elements.
105. **ground segment element** — element within a ground segment. (1) A ground segment element can be composed of several ground segment elements, e.g. a ground station network is a ground segment element that can be composed of a set of ground stations & a communication network. (2) Examples are given in Annex B.2.
106. **ground segment equipment** — equipment within a ground segment. Examples are given in Annex B.2.
107. **ground segment subsystem** — subsystem within a ground segment. Examples are given in Annex B.2.
108. **ground segment system** — system within a ground segment. Examples are given in Annex B.2.
109. **ground support equipment** — non flight product (hardware/software) used on ground to assemble, integrate, test, transport, access, handle, maintain, measure, calibrate, verify, protect or service a flight product (hardware/software)
110. **handbook** — non‑normative document providing background information, orientation, advice or recommendations related to one specific discipline or to a specific technique, technology, process or activity
111. **hardware‑software interaction analysis** — analysis to verify that the software is specified to react to hardware failures as required
112. **hazard** — existing or potential condition that can result in an accident. (1) This condition can be associated with the design, manufacturing, operation or environment. (2) Hazards are not events but potential threats to safety.
113. **hazard analysis** — systematic & iterative process of the identification, classification & reduction of hazards
114. **hazard control** — preventive or mitigation measure, associated to a hazard scenario, which is introduced into the system design, production & operation to avoid the events or the consequences of the events
115. **hazardous event** — accident resulting from a hazard
116. **hot redundancy** — redundancy where all entities are powered on with only one operating
117. **human factors** — model of observed human physical & psycho‑physiological behaviour in relation to environment & product
118. **hydrogen embrittlement** — mechanical & environmental process that results from the initial presence or absorption of excessive amounts of hydrogen in metals. Usually it occurs in combination with residual or applied tensile stresses.
119. **implementation document** — formal response from the supplier to the customer’s Project Requirements Document describing how all requirements will be met
120. **incident** — unexpected event that might be, or could lead to, an operational interruption, disruption, loss, emergency, crisis or accident. Incidents are recorded for further assessment.
121. **informative** — providing non‑normative information intended to assist the understanding or use of requirements
122. **inhibit** — design feature that prevents a function from undesirable execution. Can be software or hardware.
123. **inspection** — determination of conformance to specified requirements. Adapted from ISO 9000:2015.
124. **integration** — functionally combining lower‑level functional entities (hardware or software) so they operate together to constitute a higher‑level functional entity. Assembly is a pre‑requisite for integration.
125. **interchangeability** — ability of a product to be used in place of another to fulfil the same requirements
126. **interface control document** — specification that describes the characteristics that must be controlled at the boundaries between systems, subsystems & other elements. [ISO 10795:2019]. The ICD DRD is Annex B of [ECSS‑E‑ST‑10‑24C](ecss_est1024.md).
127. **interface definition document** — document defining the design of one interface end. The IDD DRD is Annex C of [ECSS‑E‑ST‑10‑24C](ecss_est1024.md).
128. **launch base** — composed of launch range & launch complexes
129. **launch campaign** — launch activities which include: (1) Launcher preparation & final integration, (2) Payload processing & integration on the launcher, (3) Launch Operations until lift‑off, including Flight Data Gathering
130. **launch complex** — facilities necessary to carry out the final integration of the launcher elements, of the payload into the launcher, as well as the launch operations. A Launch System is associated with its specific Launch Complex, which can include facilities shared with other Launch Systems (e.g.: Lox plant at CSG).
131. **launch operations** — all launch related activities taking place after completion of the activities necessary to deliver a fully integrated launcher up to reception of post flight data
132. **launch range** — systems, facilities & means, required to provide the necessary service & support for carrying out a launch campaign, including operations after lift‑off & Flight Data Gathering, & to ensure safety & security of persons, assets & protection of the environment. The Launch Range includes in particular the CNES/CSG technical centre, the payload Preparation Facilities as well as the downrange stations for launcher tracking & flight data acquisition.
133. **launch segment** — part of a space system which is used to transport space segment element(s) into space. (1) A launch segment is composed of a launcher system with it specific launch complex, supported by a launch range. (2) A launch segment is composed of the integrated launcher & the facilities needed for manufacturing, testing & delivering launcher elements.
134. **launch segment element** — element within a launch segment. (1) A launch segment element can be composed of several launch segment elements, e.g. a launcher is a launch segment element that is composed of several launch segment elements, such as stage, engine & upper part. (2) Examples are given in Annex B.3.
135. **launch segment equipment** — equipment within a launch segment. Examples are given in Annex B.3.
136. **launch segment subsystem** — subsystem within a launch segment. Examples are given in Annex B.3.
137. **launch segment system** — system within a launch segment. Examples are given in Annex B.3
138. **launch service** — activities required to conclude a launch service contract & to place a payload in the orbit, at the time, & under the payload environment conditions required by the customer. Launch Service activities cover in particular: Commercialisation, Mission analysis, Procurement of a fully integrated launcher, Procurement of flight programme(s), Procurement of launcher adaptations to meet specific mission requirements, Payload processing & integration on the launcher, Launch Operations including Flight Data Gathering, Launch Range Operations, Post Flight Analysis.
139. **launch system** — system comprising the launcher system & the launch complex
140. **launch vehicle** —  vehicle designed to transport payloads to space. The term 「launcher」 is synonymous.
141. **launcher element** — building block of a launcher
142. **launcher production facilities** — launcher element manufacturing facilities & testing facilities. (1) The term production facilities is used as shorthand. (2) The launcher element manufacturing facilities include the test facilities specific to the launcher elements’ manufacturing.
143. **launcher system** — fully integrated launcher & the needed facilities for manufacturing, testing & delivering the launcher elements. 「Fully integrated launcher」 means the integrated launcher, including payload, & ready to be launched i.e. all launch control lights on green.
144. **life cycle** — all phases in the life of a product from needs identification through disposal
145. **life profile** — conditions to which a product is chronologically submitted from its manufacturing to its disposal
146. **lifetime** — period, or number of cycles, over which a product is required to perform according to its specification
147. **linear energy transfer** — energy deposited by a charged particle passing through a substance & locally absorbed per unit length of path. Adapted from ISO 15856:2010. It is measured in joules per metre. Other dimensions are keV×μm⁻¹, J×m²×㎏⁻¹, MeV×㎝²×g⁻¹, MeV×㎝²×㎎⁻¹.
148. **lot** — batch or portion of a batch
149. **maintainability** — ease of performing maintenance on a product. Maintainability can be expressed as the probability that a  maintenance action on a product can be carried out within a defined time interval, using stated procedures & resources.
150. **maintenance** — actions needed to retain a product in, or restore it to, a state in which it can perform its required function. Actions include tuning, control, inspection, repair, replacement or redesign.
151. **major nonconformance** — nonconformance which can have an impact on the customer’s requirements in the following areas & cases: (1) safety of people or equipment, (2) operational, functional or any technical requirements imposed by the business agreement, (3) reliability, maintainability, availability, (4) lifetime, (5) functional or dimensional interchangeability, (6) interfaces with hardware or software regulated by different business agreements, (7) changes to or deviations from approved qualification or acceptance test procedures, (8) project specific items which are proposed to be scrapped.
152. **material** — raw, semi‑finished or finished substance (gaseous, liquid, solid) of given characteristics from which processing into a component or part is undertaken
153. **maximum expected operating pressure** — highest pressure that a product is expected to experience during its mission life & retain its functionality, in association with its applicable operating environments. Adapted from ISO 10785:2011 (1) The maximum expected operating pressure corresponds to limit loads. (2) The maximum expected operating pressure includes effects of temperature & acceleration on pressure, maximum relief pressure, maximum regulator pressure & effects of failures within the system or its components. The effect of pressure transient is assessed for each component of the system & used to define its MEOP. (3) The maximum expected operating pressure includes effects of failures of an external system (e.g. spacecraft), as specified by the customer ,on systems (e.g. propulsion ) or components. (4) The maximum expected operating pressure does not include testing factors, which are included in [ECSS‑E‑ST‑32‑02](ecss_est3202.md) & [ECSS‑E‑ST‑10‑03](ecss_est1003.md).
154. **minor nonconformance** — nonconformance which by definition cannot be classified as major. For example, the following EEE discrepancies after delivery from the manufacturer can be classified as minor: (1) random failures, where no risk for a lot‑related reliability or quality problem exists; (2) if the form, fit or function are not affected; (3) minor inconsistencies in the accompanying documentation.
155. **mission** — set of tasks, duties or functions to be accomplished by an element
156. **mission statement** — document expressing the set of collected needs. The mission statement is a document established by the customer, which reflects the users needs, & is used as input to Phase 0 of a space system project.
157. **model** — physical or abstract representation used for calculations, predictions or further assessment. Model can also be used to identify particular instances of the product e.g. flight model.
158. **multipaction** — resonant back & forth flow of secondary electrons in a vacuum between two surfaces separated by a distance such that the electron transit time is an odd integral multiple of one half the period of the alternating voltage impressed on the surface. The effects of multipaction can be loss of output power up to reaching the multipaction breakdown voltage leading to the generation of spark.
159. **non‑volatile residue** — soluble or suspended material & insoluble particulate matter remaining after temperature‑controlled evaporation of a volatile liquid
160. **nonconformance** — non‑fulfilment of a requirement. The term 「nonconformity」 is synonymous but deprecated.
161. **normative** — providing requirements for activities or their results. (1) A 「normative document」 covers documents such as standards, technical specifications, codes of practice & regulations. (2) A 「normative reference」 incorporates requirements from a cited publication into a normative document.
162. **offgassing** — gaseous release from a material under atmospheric or near‑atmospheric pressure. Examples are manned & biological missions.
163. **off‑the‑shelf** — procured from the market, even if not developed for space application
164. **outage** — state of a product being unable to perform its required function
165. **outgassing** — gaseous release from a material in vacuum conditions
166. **part** — see 「component」.
167. **particle** — unit of solid or liquid matter with observable size in a defined range. (1) Various methods for defining particle size are used & are dependant upon the measurement technique, for example between 0,1 μm & 1000 μm. (2) For the manual method the apparent maximum linear dimension of a particle in the plane of observation as observed with instruments such as optical, electron, or atomic force microscopes is the particle size. (3) For the automatic method, the equivalent diameter of a particle detected by automatic instrumentation is the particle size. (4) The equivalent diameter is the diameter of a reference sphere having known properties & producing the same response in the sensing instrument as the particle being measured.
168. **payload** — set of space segment elements. (1) A spacecraft payload is a set of instruments or equipment which performs the user mission. (2) A launcher payload is a set of space segment elements carried into space in accordance with agreed position, time & environmental conditions. 
169. **performance** — quantifiable characteristics of a function
170. **planetary protection** — policy & the technical implementations to prevent forward & backward contamination
171. **preventive action** — action to eliminate the cause of a potential nonconformance or other undesirable potential situation. (1) There can be more than one cause for a potential non‑conformance. (2) Preventive action is taken to prevent occurrence whereas corrective action is taken to prevent recurrence.
172. **procedure** — specified way to carry out an activity or process. Procedures can be documented or not. [ISO 9000:2015]
173. **process** — set of interrelated or interacting activities which transform inputs into outputs. Inputs to a process are generally outputs of other processes.
174. **product** — result of a process. Adapted from ISO 9000:2005. There are four generic product categories: (1) services, (2) software, (3) hardware, (4) processed materials.
175. **product assurance** — discipline devoted to the study, planning & implementation of activities intended to assure that the design, controls, methods & techniques in a project result in a satisfactory degree of quality in a product
176. **product tree** — hierarchical representation of the product into successive levels of detail
177. **project** — set of coordinated & controlled activities with start & finish dates, undertaken to achieve an objective conforming to specific requirements, including constraints of time, cost & resources
178. **project requirements document** — documented information including technical & programmatic requirements, as well as political, commercial, & industrial constraints
179. **protoflight** — approach based on a single model  to be flown after it has been subjected to a qualification & acceptance test campaign. The protoflight approach can be applied at each level of decomposition of space system.
180. **provision** — expression in the content of a normative document, that takes the form of a statement, an instruction, a recommendation or a requirement. These types of provision are distinguished by the form of wording they employ, e.g. instructions are expressed in the imperative mood, recommendations by the use of the auxiliary 「should」 & requirements by the use of the auxiliary 「shall」. [EN 45020:2006]
181. **qualification** — that part of verification which demonstrates that the product meets specified qualification margins. This can apply to personnel, products, manufacturing & assembly processes.
182. **qualification margin** — increase in severity of the environmental, mechanical, electrical, EMC, or operational extreme levels expected to be encountered during the specified product lifetime for the purpose of design margin demonstration. (1) This margin can include an increase in level, an extension of range, an increase in duration or cycles of exposure, as well as any other appropriate increase in severity. (2) This definition is not applicable for thermal aspects. Refer to [ECSS‑E‑ST‑31](ecss_est31.md) for 「qualification margin」. For temperatures, the qualification margin is the difference between the upper or lower qualification temperature & the upper or lower acceptance temperature (for operating & non‑operating mode).
183. **qualification model** — model, which fully reflects all aspects of the flight model design, used for complete functional & environmental qualification testing. (1) A qualification model is only necessary for newly‑designed hardware or when a delta qualification is performed for adaptation to the project. (2) The qualification model is not intended to be used for flight, since it is overtested. (3) More detailed information on the build standard & the use of this model is given in [ECSS‑E‑HB‑10‑02](ecss_ehb1002.md).
184. **qualification test level** — test level reflecting the maximum level expected to be encountered during the flight product lifetime increased by qualification margins, or qualification margins on top of acceptance margins for thermal testing
185. **quality** — degree to which a set of characteristics of a product or process fulfils requirements. Adapted from ISO 9000:2015.
186. **quality assurance** — part of quality management focused on providing confidence that quality requirements will be fulfilled. [ISO 9000:2015]
187. **quality control** — part of quality management focused on fulfilling quality requirements. [ISO 9000:2015]
188. **redundancy** — existence of more than one means for performing a given function with the intention of increasing reliability. See also definitions for 「active redundancy」, 「hot redundancy」 & 「cold redundancy」.
189. **reliability** — ability of an item to perform a required function under given conditions for a given time interval. (1) It is generally assumed that the item is in a state to perform this required function at the beginning of the time interval. (2) Generally, reliability performance is quantified using appropriate measures. In some applications these measures include an expression of reliability performance as a probability, which is also called reliability.
190. **relifing** — product assurance activity for the extension of the expiry datecode of a EEE component which is intended to be used for space application
191. **repair** — action on a nonconforming product or service to make it acceptable for the intended use. Adapted from ISO 9000:2015. (1) A successful repair of a nonconforming product or service does not necessarily make the product or service conform to the requirements. It can be that in conjunction with a repair a waiver is required. (2) Repair includes remedial action taken on a previously conforming product or service to restore it for use, for example as part of maintenance. (3) Repair can affect or change parts of the nonconforming product or service. (4) Repair leads to a configuration item change.
192. **requirement** — documented demand to be complied with
193. **residual risk** — risk remaining after implementation of risk reduction measures. [ISO 17666:2016]
194. **review** — determination of the suitability, adequacy or effectiveness of a product or process to achieve established objectives. Adapted from ISO 9000:2015. (1) Review can also include the determination of efficiency. (2) This is often performed at specific milestones as a dedicated activity. Examples are: critical design review, preliminary design review, & system requirements review.
195. **rework** — action on a nonconforming product or service to make it conform to the requirements. Adapted from ISO 9000:2015.(1) Rework can affect or change parts of the nonconforming product or service. (2) Rework does not lead to a configuration item change.
196. **risk** — undesirable situation or circumstance that has both a likelihood of occurring & a potential negative consequence on a project. Adapted from ISO 17666:2003. (1) Risks are inherent to any project, & can arise at any time during the project life cycle. (2) Predictability & control of events facilitate risk reduction. (3) The terms 「risk assessment」, 「risk mitigation」 & 「risk control」 are in common use in ECSS.
197. **safe life** — required period during which a structural item, even containing the largest undetected flaw, is shown by analysis or testing not to fail catastrophically under the expected service load & environment. Safe life is also referred as damage tolerance life or fatigue life. [ISO 10786:2011]
198. **safe mode** — configuration of a spacecraft in which it can remain safely without ground segment intervention for a specified period. (1) Safe mode is also commonly known as 「survival mode」. (2) In safe mode, typically all non‑essential on‑board units or subsystems are powered off, either to conserve power or to avoid interference with other subsystems, & the spacecraft can be (automatically) oriented to a particular attitude with respect to the sun.
199. **safety** — state where an acceptable level of risk is not exceeded. Risk relates to: (1) fatality, (2) injury or occupational illness, (3) damage to launcher hardware or launch site facilities, (4) damage to an element of an interfacing manned flight system, (5) the main functions of a flight system itself, (6) pollution of the environment, atmosphere or outer space, (7) damage to public or private property.
200. **safety critical** — resulting in temporarily disabling but not life threatening injury, temporary occupational illness, major detrimental environmental effects, major damage to public or private properties, major damage to interfacing flight systems or major damage to ground facilities. The term 「critical」 is used as shorthand for 「safety critical」 in the safety context.
201. **safety critical function** — function that, if lost or degraded, or as a result of incorrect or inadvertent operation, can result in catastrophic or critical consequences
202. **safing** — action of containment or control of emergency & warning situations, or placing a system (or part thereof), in a predetermined safe condition
203. **scrap** — action on a nonconforming product to preclude its originally intended use. (1) The scrapped product is not recoverable by rework or repair for technical or economic reasons. As a consequence, it will be recycled or destroyed. (2) A service is scrapped by being discontinued.
204. **security** — state where an acceptable level of risk arising from malevolent action is not exceeded
205. **segment** — set of elements or combination of systems that fulfils a major, self‑contained, subset of the space mission objectives. Examples are space segment, ground segment, launch segment & support segment.
206. **service life** — interval beginning with the last item inspection or flaw screening proof test after manufacturing, & ending with completion of its specified life
207. **severity** — classification of a failure or undesired event according to the magnitude of its possible consequences
208. **shop traveller** — document recording the complete production process, including repairs, malfunction of equipment, inspections, & reference to produced samples
209. **single point failure** — part of a product that, if it fails, will result in the unrecoverable failure of that product
210. **software integration testing** — testing in which software components, hardware components, or both are combined & tested to evaluate the interaction between them. [IEEE 610.12:1990]
211. **solar array** — assembly of  solar panels on a supporting  structure with associated hardware. The associated hardware includes mounting features, cables and, in the case of a deployable solar array, a deployment mechanism.
212. **solar cell assembly** — solar cell together with interconnector, coverglass and, if used, by‑pass diode
213. **solar panel** — interconnected solar cell assemblies mounted on a substrate
214. **space debris** — objects of human origin in Earth orbit or re‑entering the Earth’s atmosphere, including fragments & elements thereof, that no longer serve a useful purpose. Adapted from ISO 24113:2019. (1) Spacecraft in reserve or standby modes awaiting possible reactivation are considered to serve a useful purpose. (2) The term 「orbital debris」 is synonymous, but deprecated.
215. **space mission** — user‑defined needs to be achieved by a space system
216. **space programme** — set of related space projects managed in a coordinated way to contribute to an overall objective
217. **space segment** — part of a space system, placed in space, to fulfil the space mission objectives
218. **space segment element** — element within a space segment. (1) A space segment element can be composed of several space segment elements, e.g. a spacecraft is composed of instruments, a payload module & a service module. (2) Examples are given in Annex B.1.
219. **space segment equipment** — equipment within a space segment. Examples are given in Annex B.1.
220. **space segment subsystem** — subsystem within a space segment. Examples are given in Annex B.1.
221. **space segment system** — system within a space segment. Examples are given in Annex B.1.
222. **space system** — system that contains at least a space,  a ground or a launch segment. Generally a space system is composed of all three segments & is supported by a support segment.
223. **spacecraft** — manned or unmanned vehicle designed to orbit or travel in space. A spacecraft is a space segment element.
224. **special process** — process where the quality cannot be completely ensured by inspection of the resulting product only
225. **specification** — document stating requirements. A specification can be related to activities (e.g. procedure document, process specification & test specification), or products (e.g. product specification, performance specification & drawing). [ISO 9000:2005]
226. **stand‑alone space segment element** — space segment element that performs its mission independently of the rest of the space system. For example: satellite, rover, lander.
227. **standard** — normative document for use in invitations to tender & business agreements for implementing space related activities. (1) A standard states verifiable requirements, supported by the minimum descriptive text necessary to understand their context. Each requirement has a unique identification, allowing full traceability & easy verification of compliance. (2) A standard is established by consensus amongst all ECSS stakeholders. (3) Other Standards Development Organisations (SDOs) use a different definition.
228. **statement of work** — contractual document that describes & plans deliverables & activities required to complete a project. The statement of work is issued by the customer at the start of a project for implementation by the supplier.
229. **stress‑corrosion** — combined action of sustained tensile stress & corrosion that can lead to premature failure of materials
230. **structural model** — structurally representative model of the flight model used for qualification of the structural design & for correlation with structural mathematical models. (1) The system structural model usually consists of a representative structure, with structural dummies of the flight equipment, & also includes representative mechanical parts of other subsystems (e.g. mechanisms & solar panels). (2) The system structural model is also used for final validation of test facilities, GSE, & associated procedures. (3) More detailed information on the build standard & the use of this model is given in [ECSS‑E‑HB‑10‑02](ecss_ehb1002.md).
231. **subsystem** — part of a system fulfilling one or more of its functions
232. **supplier** — organization or person that provides a product as part of a business agreement. A supplier can be internal or external to the customer organization.
233. **support segment** — generic infrastructure & services used to support the development & operation of space system elements. (1) Examples are ground stations & associated networks, orbit computing facilities, test centres, astronaut centre, launch ranges (e.g. Plestek, Baikonour, Guiana Space Centre). (2) Items can be part of other segments during their development & later become part of the support segment when used (e.g. a tracking network).
234. **support system** — see 「support segment」. The term 「support system」 is deprecated.
235. **system** — set of interrelated or interacting functions constituted to achieve a specified objective
236. **tailoring** — process by which standards are made applicable to a specific project by selection of existing requirements, with or without modification, or addition of new ones
237. **technical memorandum** — non‑normative document providing useful information to the space community on a specific subject. Technical Memoranda are prepared to record & present data which are not the subject for a standard or for a handbook or not yet mature enough to be published as standard or handbook.
238. **test** — measurement of product characteristics, performance or functions under representative environments
239. **thermal balance test** — test conducted under steady state conditions to correlate & adjust the thermal mathematical model & verify the thermal design
240. **thermal model** — thermally representative model of the flight model used for verification of the thermal design & for correlation with thermal mathematical models. (1) The system thermal model usually consists of a representative structure, with thermal dummies of the flight equipment, & also includes representative thermal parts of other subsystems. (2) More detailed information on the build standard & the use of this model is given in [ECSS‑E‑HB‑10‑02](ecss_ehb1002.md).
241. **thermal vacuum test** — test conducted in vacuum under predefined temperature conditions to demonstrate the capability of the test item to operate according to requirements. Temperature conditions can be expressed as temperature level, gradient, difference, & variation.
242. **third party** — person or body that is recognized as being independent of the parties involved, as concerns the issue in question. Parties involved are usually supplier (「first party」) & purchaser (「second party」). [EN 45020:1998]
243. **traceability** — ability to track the history, location or application by means of documented records. When considering a product, traceability can relate to: (1) the origin of materials & parts, (2) the processing history, (3) the distribution & location of the product after delivery.
244. **unit** — see 「equipment」. The term 「equipment」 is strongly recommended for use in the ECSS system.
245. **validation** — process which demonstrates that the product is able to accomplish its intended use in the intended operational environment. (1) The status of the product following validation is 「validated」. (2) Verification is a pre‑requisite for validation.
246. **verification** — process which demonstrates through the provision of objective evidence that the product is designed & produced according to its specifications & the agreed deviations & waivers, & is free of defects. (1) A waiver can arise as an output of the verification process. (2) Verification can be accomplished by one or more of the following methods: analysis (including similarity), test, inspection, review of design. (3) The status of the product following verification is 「verified」.
247. **visibly clean** — absence of surface contamination when examined with a specific light source, angle of incidence & viewing distance using normal or magnified vision
248. **waiver** — authorised departure from the originally specified requirements for a product, during or after production. (1) Deviation is an a priori decision whereas waiver is an a posteriori decision with respect to the production phase. (2) The term 「concession」 is synonymous & can be used for materials as per [ECSS‑Q‑ST‑70](ecss_qst70.md).
249. **work breakdown structure** — hierarchical representation of the activities necessary to complete a project
250. **work package** — group of related tasks that are defined down to the lowest level within a work breakdown structure


### 2.4. Abbreviated terms

1. **A/D** — analogue‑to‑digital
2. **ABM** — apogee boost motor
3. **AC** — alternating current
4. **ADC** — analogue‑to‑digital converter
5. **AIT** — assembly, integration & test
6. **AIV** — assembly, integration & verification
7. **AOCS** — attitude & orbit control subsystem
8. **APS** — active pixel sensor
9. **AQL** — acceptance quality level
10. **AR** — acceptance review
11. **ASIC** — application specific integrated circuit
12. **ASTM** — American Society for Testing & Materials
13. **ATOX** — atomic oxygen
14. **AWG** — American wire gauge
15. **BOL** — beginning‑of‑life
16. **CAD** — computer aided design
17. **CCB** — configuration control board
18. **CCD** — charge coupled device
19. **CCSDS** — Consultative Committee for Space Data Systems
20. **CDR** — critical design review
21. **CIDL** — configuration item data list
22. **CIL** — critical items list
23. **CoG** — centre of gravity
24. **CoM** — centre of mass
25. **COTS** — commercial off‑the‑shelf
26. **CRR** — commissioning result review
27. **CVCM** — collected volatile condensable material
28. **DC** — direct current
29. **DDF** — design definition file
30. **DDR** — detailed design review
31. **DJF** — design justification file
32. **DML** — declared materials list
33. **DMPL** — declared mechanical parts list
34. **DPL** — declared processes list
35. **DRB** — delivery review board
36. **DRD** — document requirements definition
37. **DRL** — document requirements list
38. **ECLS** — environmental control & life support
39. **ECSS** — European Cooperation for Space Standardization
40. **EED** — electro‑explosive device
41. **EEE** — electrical, electronic & electromechanical
42. **EGSE** — electrical ground support equipment
43. **EIDP** — end item data package
44. **ELR** — end‑of‑life review
45. **EM** — engineering model
46. **EMC** — electromagnetic compatibility
47. **EMI** — electromagnetic interference
48. **EN** — European Standard
49. **EOL** — end‑of‑life
50. **ESA** — European Space Agency
51. **ESCC** — European Space Components Coordination
52. **ESD** — electrostatic discharge
53. **FDIR** — failure detection isolation & recovery
54. **FM** — flight model
55. **FMEA** — failure modes & effects analysis
56. **FMECA** — failure modes, effects & criticality analysis
57. **FOS** — factor of safety
58. **FRR** — flight readiness review
59. **FTA** — fault tree analysis
60. **GEO** — geostationary orbit
61. **GS** — ground segment
62. **GSE** — ground support equipment
63. **HMI** — human‑machine interface
64. **HSIA** — hardware‑software interaction analysis
65. **HW** — hardware
66. **ICD** — interface control document
67. **ILS** — integrated logistic support
68. **IRD** — interface requirements document
69. **ISO** — International Organization for Standardization
70. **ISS** — International Space Station
71. **I/F** — interface
72. **I/O** — input/output
73. **LEO** — low Earth orbit
74. **LEOP** — launch & early orbit phase
75. **LRR** — launch readiness review
76. **MCR** — mission close‑out review
77. **MDD** — mission description document
78. **MDP** — maximum design pressure
79. **MDR** — mission definition review
80. **MEOP** — maximum expected operating pressure
81. **MGSE** — mechanical ground support equipment
82. **MIP** — mandatory inspection point
83. **MLI** — multi‑layer insulation
84. **MMIC** — monolithic microwave integrated circuit
85. **MOI** — moment of inertia
86. **NASA** — National Aeronautics & Space Administration
87. **NCR** — nonconformance report
88. **NDI** — non‑destructive inspection
89. **NDT** — non‑destructive test
90. **NRB** — nonconformance review board
91. **N/A** — not applicable
92. **OBDH** — on‑board data handling
93. **ORR** — operational readiness review
94. **OTS** — off‑the‑shelf
95. **PA** — product assurance
96. **PCB** — printed circuit board
97. **PDR** — preliminary design review
98. **PFM** — protoflight model
99. **PID** — process identification document
100. **PMP** — parts, materials & processes
101. **PRR** — preliminary requirements review
102. **PTR** — post test review
103. **QA** — quality assurance
104. **QM** — qualification model
105. **QR** — qualification review
106. **RAMS** — reliability, availability, maintainability & safety
107. **RB** — requirements baseline
108. **RF** — radio frequency
109. **RFA** — request for approval
110. **RFD** — request for deviation
111. **RFW** — request for waiver
112. **RH** — relative humidity
113. **RID** — review item discrepancy
114. **ROD** — review of design
115. **r.m.s.** — root‑mean‑square
116. **SCC** — stress‑corrosion cracking
117. **SEE** — single event effect
118. **SEP** — system engineering plan
119. **SRR** — system requirements review
120. **STM** — structural‑thermal model
121. **SVT** — system validation test
122. **S/C** — spacecraft
123. **SW** — software
124. **TC** — telecommand
125. **TCS** — thermal control subsystem
126. **TM** — telemetry
127. **TM/TC** — telemetry/telecommand
128. **TML** — total mass loss
129. **TRB** — test review board
130. **TRL** — technology readiness level
131. **TRR** — test readiness review
132. **TS** — technical specification
133. **TT&C** — telemetry, tracking & command
134. **UTC** — coordinated universal time
135. **UV** — ultraviolet
136. **VCD** — verification control document
137. **VP** — verification plan
138. **WBS** — work breakdown structure


## Annex A Traceability with respect to ECSS‑S‑ST‑00‑01C
Deleted terms (terms that appeared in ECSS‑S‑ST‑00‑01C but do not appear in the current document) are listed below. The terms marked with an asterisk (*) are still accepted as synonyms for ECSS terms, but their use in ECSS is discouraged.

Terms deleted with respect to the previous issue of the glossary (ECSS‑S‑ST‑00‑01CP‑001B).



## Annex B Segment trees
This annex includes example for the terms defined in paragraph 2.1 & Figure   2 ‑1: Space system breakdown:

1. Space segment
2. Ground segment
3. Launch segment
4. Support segment

### B.1 Space segment

![](f/doc/ecss/ecss_sst0001/b1.png)


### B.2 Ground segment

![](f/doc/ecss/ecss_sst0001/b2.png)

### B.3 Launch segment

![](f/doc/ecss/ecss_sst0001/b31.png)

![](f/doc/ecss/ecss_sst0001/b32.png)


### B.4 Support segment

|**Systems**| |**Elements**|
|:--|:--|:--|
| |**Examples**| |
|Data Relay Satellite System| |Data Relay Satellite|
|Navigation Satellite System| |ISS|
|Concurrent Design Facility (CDF)| |Astronaut training centre|
|Generic Flight Dynamics system (e.g. ORATOS)| |Ground Statin Network Control Centre|
| | |Main Control room|
|LEOP Ground Station Network| |Briefing Room|
|Deep Space Ground Station Network| |Test Centre|
| | |MGSE|
| | |TGSE|
| | |FGSE|
| | |…|
| | |Launch range|



## Annex C Launch segment‑specific terms
The following terms are specific to the launcher domain & are cross‑referenced from clause 2.3.
The following terms are specific to the launcher domain & are cross‑referenced from clause 2.3.

1. **launch base** — composed of launch range & launch complexes.
2. **launch campaign** — launch activities which include: (1) Launcher preparation & final integration, (2) Payload processing & integration on the launcher, (3) Launch Operations until lift‑off, including Flight Data Gathering.
3. **launch complex** — facilities necessary to carry out the final integration of the launcher elements, of the payload into the launcher, as well as the launch operations. A Launch System is associated with its specific Launch Complex, which can include facilities shared with other Launch Systems (e.g.: Lox plant at CSG).
4. **launch operations** — all launch related activities taking place after completion of the activities necessary to deliver a fully integrated launcher up to reception of post flight data.
5. **launch range** — systems, facilities & means, required to provide the necessary service & support for carrying out a launch campaign, including operations after lift‑off & Flight Data Gathering, & to ensure safety & security of persons, assets & protection of the environment. The Launch Range includes in particular the CNES/CSG technical centre, the payload Preparation Facilities as well as the downrange stations for launcher tracking & flight data acquisition.
6. **launch service** — activities required to conclude a launch service contract & to place a payload in the orbit, at the time, & under the payload environment conditions required by the customer. Launch Service activities cover in particular: Commercialisation, Mission analysis, Procurement of a fully integrated launcher, Procurement of flight programme(s), Procurement of launcher adaptations to meet specific mission requirements, Payload processing & integration on the launcher, Launch Operations including Flight Data Gathering, Launch Range Operations, Post Flight Analysis.
7. **launch system** — system comprising the launcher system & the launch complex.
8. **launch vehicle** — vehicle designed to transport payloads to space. The term 「launcher」 is synonymous.
9. **launcher element** — building block of a launcher.
10. **launcher production facilities** — launcher element manufacturing facilities & testing facilities. (1) The term production facilities is used as shorthand. (2) The launcher element manufacturing facilities include the test facilities specific to the launcher elements’ manufacturing.
11. **launcher system** — fully integrated launcher & the needed facilities for manufacturing, testing & delivering the launcher elements. 「Fully integrated launcher」 means the integrated launcher, including payload, & ready to be launched i.e. all launch control lights on green.



## The End

end of file
