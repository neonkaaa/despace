# Бережливое производство
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [Качество](qm.md), [R&D](rnd.md), [Control](control.md), [SE](se.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Бережли́вое произво́дство** — русскоязычный термин. **Lean manufacturing / Lean production** — англоязычный эквивалент.</small>

**Бережли́вое произво́дство** *(от англ. lean production, lean manufacturing — 「стройное производство」)* — концепция управления производственным предприятием, основанная на постоянном устранении всех видов потерь. Бережливое производство предполагает вовлечение в процесс оптимизации бизнеса каждого сотрудника и максимальную ориентацию на потребителя. Возникла как интерпретация идей производственной системы компании Toyota американскими исследователями её феномена.



## Основные аспекты
Отправная точка концепции — оценка ценности продукта для конечного потребителя, на каждом этапе его создания. В качестве основной задачи предполагается создание процесса непрерывного устранения потерь, то есть устранение любых действий, которые потребляют ресурсы, но не создают ценности (не являются важными) для конечного потребителя. В качестве синонима для понятия потерь иногда используется термин из производственной системы Toyota — muda (яп. 無駄 муда), означающий всевозможные затраты, потери, отходы, мусор. Например, потребителю совершенно не нужно, чтобы готовый продукт или его детали лежали на складе. Тем не менее, при традиционной системе управления складские издержки, а также все расходы, связанные с переделкой, браком, и другие косвенные издержки перекладываются на потребителя.

В соответствии с концепцией бережливого производства, вся деятельность предприятия делится на операции и процессы, добавляющие ценность для потребителя, и операции и процессы, не добавляющие ценности для потребителя. Задачей 「бережливого производства」 является планомерное сокращение процессов и операций, не добавляющих ценности.


### Виды потерь
Тайити Оно (1912‑1990), один из главных создателей производственной системы компании Toyota, выделил 7 видов потерь:

1. потери из‑за перепроизводства;
1. потери времени из‑за ожидания;
1. потери при ненужной транспортировке;
1. потери из‑за лишних этапов обработки;
1. потери из‑за лишних запасов;
1. потери из‑за ненужных перемещений;
1. потери из‑за выпуска дефектной продукции;
1. нереализованный творческий потенциал сотрудников. *(добавлен Джеффри Лайкером, исследователем производственной системы Toyota)*

Также принято выделять ещё два источника потерь — muri (яп. 無理 му́ри), — перегрузка рабочих, сотрудников или мощностей при работе с повышенной интенсивностью и mura (яп. 斑 му́ра) — неравномерность выполнения операции, например, прерывистый график работ из‑за колебаний спроса.


### Основные принципы
Джеймс Вумек и Дэниел Джонс в книге 「Бережливое производство: Как избавиться от потерь и добиться процветания вашей компании」 излагают суть бережливого производства как процесс, который включает пять этапов:

1. Определить ценность конкретного продукта.
1. Определить поток создания ценности для этого продукта.
1. Обеспечить непрерывное течение потока создания ценности продукта.
1. Позволить потребителю вытягивать продукт.
1. Стремиться к совершенству.

Среди других принципов выделяются: достижение превосходного [качества](qm.md) (сдача с первого предъявления, система 「ноль дефектов」, обнаружение и решение проблем у истоков их возникновения), гибкость, установление долговременных отношений с потребителями (путём деления [рисков](qm.md), затрат и информации).

Производственная система Toyota основывается на двух базовых принципах: 「точно вовремя」 и принципе автономизации (autonomation). Первый принцип требует, чтобы необходимые для сборки детали поступали на производственную линию строго в тот момент, когда это нужно, и строго в необходимом количестве с целью сокращения складских запасов.

Впоследствии в рамках концепции бережливого производства было выделено множество элементов, каждый из которых представляет собой определённый метод, а некоторые (например, кайдзен) сами претендуют на статус самостоятельной производственной концепции:

1. поток единичных изделий
1. канбан
1. всеобщий уход за оборудованием (англ. total productive maintenance, TPM)
1. Система 5S
1. быстрая переналадка (SMED)
1. кайдзен
1. пока‑ёкэ (「защита от ошибок」 и бака‑ёкэ — 「защита от дурака」) — метод предотвращения ошибок.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**`Качество:`**<br> [Bus factor](bus_factor.md) ~~ [Way](faq.md) ~~ [АВПКО](fmeca.md) ~~ [Авторский надзор](des_spv.md) ~~ [Бережливое производство](lean_man.md) ~~ [Валидация, верификация](vnv.md) ~~ [Класс чистоты](clean_lvl.md) ~~ [Конструктивное совершенство](con_vel.md) ~~ [Крит. технологии](kt.md) ~~ [Крит. элементы](sens_elem.md) ~~ [Метрология](metrology.md) ~~ [Надёжность](qm.md) ~~ [Нештатная ситуация](emergency.md) ~~ [Номинал](nominal.md) ~~ [Ошибки](faq.md) ~~ [Система менеджмента качества](qms.md) ~~ [УГТ](trl.md)/[TRL](trl.md)|
|**【[Control](Control.md)】**<br> [Ad hoc](ad_hoc.md) ~~ [Business travel](business_travel.md) ~~ [Chief designers council](cocd.md) ~~ [CML](trl.md) ~~ [Competence](competence.md) ~~ [Confident](confident.md) ~~ [Consp.theory](consp_theory.md) ~~ [Control sys. (CS)](cs.md) ~~ [Coordinate system](coord_sys.md) ~~ [Curator](curator.md) ~~ [Designer’s supervision](des_spv.md) ~~ [E‑sig](esig.md) ~~ [Engineer](se.md) ~~ [Errand](errand.md) ~~ [Federal law](fed_law.md) ~~ [Federal TP](fed_tp.md) ~~ [Federal SP](fed_sp.md) ~~ [GNC](gnc.md) ~~ [Gravity assist](gravass.md) ~~ [Industrial archaeology](ind_arch.md) ~~ [Instruction](instruction.md) ~~ [Lean manuf.](lean_man.md) ~~ [Lifetime](lifetime.md) ~~ [Manager](manager.md) ~~ [MBSE](se.md) ~~ [Meeting](meeting.md) ~~ [MCC](sc.md) ~~ [MIC](mic.md) ~~ [MML](mml.md) ~~ [MoU](contract.md) ~~ [Nav. & ballistics (NB)](nnb.md) ~~ [Nonprofit org.](nonprof_org.md) ~~ [NX](nx.md) ~~ [Oberth effect](oberth_eff.md) ~~ [Org.structure](orgstruct.md) ~~ [Outcomes commission](outccom.md) ~~ [Patent](patent.md) ~~ [Peter prin.](peter_principle.md) ~~ [Plan](plan.md) ~~ [PMBok](pmbok.md) ~~ [Quorum](quorum.md) ~~ [R&D management](mgmt.md) ~~ [R&D support](rnd_support.md) ~~ [Recursion](recurs.md) ~~ [Schulze_method](schulze_method.md) ~~ [Sci'N'Tech activities](st_act.md) ~~ [Sci'N'Tech council](satc.md) ~~ [Single-window system](sw_sys.md) ~~ [Situ.leadership](situ_leadership.md) ~~ [Skunk works](se.md) ~~ [State arm. plan](plan_sa.md) ~~ [Swamp](swamp.md) ~~ [Teamcenter](teamcenter.md) ~~ [Tennis racket theorem](tr_theorem.md) ~~ [TRIZ](triz.md) ~~ [TRL](trl.md) ~~ [V‑model](v_model.md) ~~ [Veto](veto.md) ~~ [Workflow](workflow.md) ~~ [Workgroup](wg.md)|
|**【[Systems engineering](se.md)】**<br> [Competence](competence.md) ~~ [Coordinate system](coord_sys.md) ~~ [Designer’s supervision](des_spv.md) ~~ [Industrial archaeology](ind_arch.md) ~~ [Instruction](instruction.md) ~~ [Lean manuf.](lean_man.md) ~~ [Lifetime](lifetime.md) ~~ [MBSE](se.md) ~~ [MML](mml.md) ~~ [Nav. & ballistics (NB)](nnb.md) ~~ [NASA SEH](book_nasa_seh.md) ~~ [Oberth effect](oberth_eff.md) ~~ [PMBok](pmbok.md) ~~ [Quorum](quorum.md) ~~ [R&D management](mgmt.md) ~~ [R&D support](rnd_support.md) ~~ [Recursion](recurs.md) ~~ [Schulze_method](schulze_method.md) ~~ [Sci'N'Tech activities](st_act.md) ~~ [Sci'N'Tech council](satc.md) ~~ [Skunk works](se.md) ~~ [SysML](sysml.md) ~~ [Tennis racket theorem](tr_theorem.md) ~~ [TRIZ](triz.md) ~~ [TRL](trl.md) ~~ [V‑model](v_model.md) ~~ [Workflow](workflow.md) ~~ [Workgroup](wg.md)|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Lean_manufacturing>


## The End

end of file
