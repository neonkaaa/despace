# Amulapo
> 2021.12.06 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/a/amulapo_logo1t.webp)](f/c/a/amulapo_logo1.webp)|<amulapo.contact@gmail.com>, <mark>nophone</mark>, Fax …;<br> *37-22 Wakamatsu-cho, Shinjuku-ku, Tokyo 162-0056, Japan*<br> <https://amulapo-inc.com> ~~ [FB ⎆](https://www.facebook.com/amulapo.info) ~~ [LI ⎆](https://www.linkedin.com/company/amulapo-inc) ~~ [X ⎆](https://twitter.com/amulapo)|
|:--|:--|
|**Business**|Creation of space experience content using ICT technology such as xR, robots, & AI<br> ・**xR content.** Contract development & sales.<br> ・**Education.** Space, ICT technology, etc. STEAM education & human resource development.<br> ・**Community.** Researcher's, Providing a place for cross‑disciplinary exchange.<br> ・**Development** of technology related to space Development & support.|
|**Mission**|…|
|**Vision**|Use the universe to change the structure of society|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO, Representative Director — Katsuaki Tanaka<br> ・COO, Director — Ko Matsuhiro<br> ・CTO, Director — Moeko Hidaka|

**Amulapo Co., Ltd.** is a Japanese company aimed to create space experience content such as xR, robots, AI, etc. Founded 2020.02.28. A group of young researchers who produce content using ICT technology. Responsible for the transmission of science & technology & social implementation for the future of 2050, while aiming to promote science & technology. We will deliver a fun future experience.

**Members:**

- **Katsuaki Tanaka** — space robot expert, doctor (engineering). While working as an engineer in the development of a lunar rover at a space development venture company, he established amulapo Co., Ltd., which creates space experiences with ICT technologies such as xR, robots, AI, & promotes science & technology centered on space. He also serves as an invited researcher at Waseda Univ. & a director of ELPISNEX (one company), & creates a system for the future of 2050.
- **Ko Matsuhiro** — responsible for commercialization of space experience content. Engaged in research on lunar vertical hole exploration robots & communication & robots. Waseda University Graduate School of Advanced Science & Engineering Doctoral course, VLEAP Founder COO, Temporary employee of a major telecommunications‑related company. We are working to make the world like science fiction, which was thought to be a long way off, a reality.
- **Moeko Hidaka** — joined a space‑related company from 2020. Engaged in research specializing in navigation GNC technology for SC. While studying Moon landing technology as a student, she experienced a wide range of manufacturing through part‑time work at a space development venture company. I want to bring excitement to society with technology.

<p style="page-break-after:always"> </p>

## The End

end of file
