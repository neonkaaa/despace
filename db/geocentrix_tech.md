# Geocentrix Technologies
> 2021.04.01 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/g/geocentrix_logo1t.webp)](f/c/g/geocentrix_logo1.webp)|<info@geocentrix.ca>, +1(778)988-63-43, Fax …;<br> *…, Vancouver, B.C. Canada*<br> <https://www.geocentrix.ca>|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Geocentrix Technologies Ltd.** is a consulting, management, & development company for space missions & related high‑tech industries. We bring over 20 years’ experience in design, analysis, management, operations, visualisation, education, & applications, to the benefit of our customers, incl.:

- satellite orbit & constellation design & analysis
- spacecraft design & analysis (engineering budgets)
- launch contract & campaign management
- STK™ Grand-Master certification
- Canadian Controlled Goods & Export Controls management
- Canadian Contract Security Programme management
- Secondary school & university STEM competition management



## The End

end of file
