# ХРИП
> 2019.09.20 [🚀](../../index/index.md) [despace](index.md) → **[Soft](soft.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Хранение, разработка, исследования, планирование (ХРИП)** — русскоязычный термин, не имеющий аналога в английском языке. **Archives, development, research, administration (ADRA)** — дословный перевод с русского на английский.</small>

**Хранение, разработка, исследования, планирование (ХРИП)** — PLM/PDM-система; управление/планирование процессами исследований и разработок на НПОЛ.



## Описание
Цель: Создать систему, автоматизирующую процессы НПОЛ, снижающую затрачиваемое время, при этом избавившись от недочётов систем ЭДО, АСЭС, NX, Teamcenter.

В PDM-системах обобщены такие технологии, как:

1. управление инженерными данными (engineering data management — EDM)
1. управление документами
1. управление информацией об изделии (product information management — PIM)
1. управление техническими данными (technical data management — TDM)
1. управление технической информацией (technical information management — TIM)
1. управление изображениями и манипулирование информацией, всесторонне определяющей конкретное изделие.

Базовые функциональные возможности PDM-систем охватывают следующие основные направления:

1. управление хранением данных и документами
1. управление потоками работ и процессами
1. управление структурой продукта
1. автоматизация генерации выборок и отчетов
1. механизм авторизации



## Заметки
1. Хранение в более‑менее удобочитаемом и парсящемся формате (JSON, XML, MD и пр.). Чтобы потом экспортировать в нужный (MS Word, Excel, PDF и пр.).
1. Документы, исполнители, места, процессы, ресурсы, сроки и пр. должны быть:
   1. взаимоувязаны между собой;
   1. классифицированы таким образом, чтобы можно было составлять всевозможные перечни в пару кликов.
1. Планирующая система должна предупреждать о возможных рисках.
1. [Система управления версиями](vcs.md) ([Git](git.md)) для хранения изменений и согласования в наглядном виде без просмотра всего документа заново.
1. Электронная подпись.
1. Запасы по времени.
1. Запасы по человеческим ресурсам.
1. Доставка обновлённых версий автоматически до адресатов.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Software](soft.md)】**<br> [ASP](asp.md) ~~ [Blender](blender.md) ~~ [C](plang.md) ~~ [Cosmographia](cosmographia.md) ~~ [DOORS](doors.md) ~~ [DWG](cad_f.md) ~~ [GIMP](gimp.md) ~~ [Git](git.md) ~~ [IGES](cad_f.md) ~~ [ISIS](isis.md) ~~ [JT](cad_f.md) ~~ [NGT](neogeography_toolkit.md) ~~ [NX](nx.md) ~~ [Octave](gnu_octave.md) ~~ [OS](os.md) ~~ [PDF](pdf.md) ~~ [Python](plang.md) ~~ [R](plang.md) ~~ [SPICE](spice.md) ~~ [STEP](cad_f.md) ~~ [STL](stk.md) ~~ [SVG](cad_f.md) ~~ [Syncthing](syncthing.md) ~~ [SysML](sysml.md) ~~ [Teamcenter](teamcenter.md) ~~ [Valispace](valispace.md) ~~ [Система управления версиями](vcs.md) ~~ [ХРИП](adra.md)|

1. Docs: …
1. <…>


## The End

end of file
