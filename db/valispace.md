# Valispace
> 2022.09.18 [🚀](../../index/index.md) [despace](index.md) → [Soft](soft.md)  
> Nav.:  
> **[FAQ](faq.md)**【**[SCS](scs.md)**·КК, **[SC (OE+SGM)](sc.md)**·КА】**[CON](contact.md)·[Pers](person.md)**·Кнкт, **[Ctrl](control.md)**·Упр., **[Doc](doc.md)**·Док., **[Draw.](draw.md)**·Черт., **[EF](ef.md)**·ВВФ, **[Event](event.md)**·Соб., **[FS](fs.md)**·ТЭО, **[HF&E](hfe.md)**·Эрго., **[KT](kt.md)**·КТ, **[N&B](nnb.md)**·БНО, **[Project](project.md)**·Проект, **[QM](qm.md)**·БКНР, **[R&D](rnd.md)**·НИОКР, **[SI](si.md)**·СИ, **[Test](test.md)**·ЭО, **[TRL](trl.md)**·УГТ

[TOC]

---

> <small>**Valispace** — EN term. **Валиспэйс** — literal RU translation.</small>

<mark>TBD</mark>



<p style="page-break-after:always"> </p>

## Description
<mark>TBD</mark>



<p style="page-break-after:always"> </p>

## Docs/Links
|**Sections & pages**|
|:--|
|**【[Software](soft.md)】**<br> [ASP](asp.md) ~~ [Blender](blender.md) ~~ [C](plang.md) ~~ [Cosmographia](cosmographia.md) ~~ [DOORS](doors.md) ~~ [DWG](cad_f.md) ~~ [GIMP](gimp.md) ~~ [Git](git.md) ~~ [IGES](cad_f.md) ~~ [ISIS](isis.md) ~~ [JT](cad_f.md) ~~ [NGT](neogeography_toolkit.md) ~~ [NX](nx.md) ~~ [Octave](gnu_octave.md) ~~ [OS](os.md) ~~ [PDF](pdf.md) ~~ [Python](plang.md) ~~ [R](plang.md) ~~ [SPICE](spice.md) ~~ [STEP](cad_f.md) ~~ [STL](stk.md) ~~ [SVG](cad_f.md) ~~ [Syncthing](syncthing.md) ~~ [SysML](sysml.md) ~~ [Teamcenter](teamcenter.md) ~~ [Valispace](valispace.md) ~~ [Система управления версиями](vcs.md) ~~ [ХРИП](adra.md)|

1. Docs: …
1. <…>


## The End

end of file
