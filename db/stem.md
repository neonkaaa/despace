# STEM
> 2019.05.05 [🚀](../../index/index.md) [despace](index.md) → [Doc](doc.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Science, technology, engineering, and mathematics (STEM)** — EN term. **Наука, технология, инженерия и математика** — RU analogue.</small>

<mark>TBD</mark>

**Science, Technology, Engineering and Mathematics (STEM)**, previously **Science, Math, Engineering and Technology (SMET)**, is a term used to group together these academic disciplines. This term is typically used when addressing education policy and curriculum choices in schools to improve competitiveness in science & tech development. It has implications for workforce development, national security concerns and immigration policy.



## Description
**Other variations:**

1. **AMSEE** (Applied Math, Science, Engineering, & Entrepreneurship)
1. **A‑STEM** (Arts, Science, Technology, Engineering, & Mathematics); more focus & based on humanism & arts
1. **eSTEM** (environmental STEM)
1. **GEMS** (Girls in Engineering, Math, & Science); used for programs to encourage women to enter these fields
1. **iSTEM** (invigorating Science, Technology, Engineering, & Mathematics); identifies new ways to teach STEM-related fields
1. **METALS** (STEAM + Logic), introduced by Su Su at Teachers College, Columbia University
1. **MINT** (Mathematics, Informatics, Natural sciences & Technology)
1. **STEAM** (Science, Technology, Engineering, Arts, & Mathematics)
1. **STEAM** (Science, Technology, Engineering & Applied Mathematics); more focus on applied mathematics
1. **STEMIE** (Science, Technology, Engineering, Mathematics, Invention & Entrepreneurship); adds Inventing & Entrepreneurship as means to apply STEM to real world problem solving & markets
1. **STEMLE** (Science, Technology, Engineering, Mathematics, Law & Economics); identifies subjects focused on fields such as applied social sciences & anthropology, regulation, cybernetics, machine learning, social systems, computational economics & computational social sciences
1. **STEMM** (Science, Technology, Engineering, Mathematics, & Medicine)
1. **STEMS^2** (Science, Technology, Engineering, Mathematics, Social Sciences & Sense of Place); integrates STEM with social sciences & sense of place
1. **STM** (Scientific, Technical, Mathematics / Science, Technology, Medicine / Scientific, Technical, Medical)
1. **STREAM** (Science, Technology, Robotics, Engineering, Arts, & Mathematics); adds robotics & arts as fields
1. **STREM** (Science, Technology, Robotics, Engineering, & Mathematics); adds robotics as a field
1. **STREM** (Science, Technology, Robotics, Engineering, & Multimedia); adds robotics as a field & replaces mathematics with media
1. **THAMES** (Technology, Hands‑On, Art, Mathematics, Engineering, Science)



## Criticism
The focus on increasing participation in STEM fields has attracted criticism. In the 2014 article 「The Myth of the Science and Engineering Shortage」 in The Atlantic, demographer Michael S. Teitelbaum criticized the efforts of the U.S. government to increase the number of STEM graduates, saying that, among studies on the subject, 「No one has been able to find any evidence indicating current widespread labor market shortages or hiring difficulties in science and engineering occupations that require bachelor’s degrees or higher」, and that 「Most studies report that real wages in many—but not all—science and engineering occupations have been flat or slow‑growing, and unemployment as high or higher than in many comparably‑skilled occupations.」 Teitelbaum also wrote that the then‑current national fixation on increasing STEM participation paralleled previous U.S. government efforts since World War II to increase the number of scientists and engineers, all of which he stated ultimately ended up in 「mass layoffs, hiring freezes, and funding cuts」; including one driven by the Space Race of the late 1950s and 1960s, which he wrote led to 「a bust of serious magnitude in the 1970s.」

IEEE Spectrum contributing editor Robert N. Charette echoed these sentiments in the 2013 article 「The STEM Crisis Is a Myth」, also noting that there was a 「mismatch between earning a STEM degree and having a STEM job」 in the United States, with only around ¼ of STEM graduates working in STEM fields, while less than half of workers in STEM fields have a STEM degree.

Economics writer Ben Casselman, in a 2014 study of post‑graduation earnings for FiveThirtyEight, wrote that, based on the data, science should not be grouped with the other three STEM categories, because, while the other three generally result in high‑paying jobs, 「many sciences, particularly the life sciences, pay below the overall median for recent college graduates.」

Efforts to remedy the perceived domination of STEM subjects by men of Asian and non‑Hispanic European backgrounds has led to intense efforts to diversify the STEM workforce. However, some critics feel that this practice in higher education, as opposed to a strict meritocracy, causes lower academic standards.



## Docs/Links
|**Sections & pages**|
|:--|
|**`Documents:`**<br> …|

**Documents:**

1. …

**Links:**

1. <https://en.wikipedia.org/wiki/Science,_technology,_engineering,_and_mathematics>
1. <https://missionstem.nasa.gov/diversity-inclusion-leadrshp.html> — STEM from NASA
1. 2018.10.23 <https://www.liga.net/society/opinion/chto-takoe-stem-i-pochemu-on-vajen-v-sovremennom-obrazovanii>


## The End

end of file
