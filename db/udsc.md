# Usuda Deep Space Center
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [JAXA](jaxa.md), **[НС](sc.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Usuda Deep Space Center (UDSC)** — англоязычный термин, не имеющий аналога в русском языке. **Центр дальней космической связи Усуда (UDSC)** — дословный перевод с английского на русский.</small>

**Usuda Deep Space Center (UDSC)** is a facility of the Japan Aerospace Exploration Agency. It is a spacecraft tracking station opened in October, 1984 in Usuda Town, Minami‑saku, Nagano Prefecture (Saku City from 2005.04). The main feature of the station is a 64‑meter beam waveguide antenna. 36°07'59.0"N 138°21'44.0"E

| | | | |
|:--|:--|:--|:--|
|[![](f/gs/udsc_pic1t.webp)](f/gs/udsc_pic1.webp)|[![](f/gs/udsc_pic2t.webp)](f/gs/udsc_pic2.webp)|[![](f/gs/udsc_pic3t.webp)](f/gs/udsc_pic3.webp)|[![](f/gs/udsc_pic4t.webp)](f/gs/udsc_pic4.webp)|

**Telescopes:**

1. 64‑meter DSN Antenna — Deep Space Tracking Antenna. [Bands](comms.md): <mark>TBD</mark>
1. 54‑meter DSN Antenna — Deep Space Tracking Antenna. [Bands](comms.md): <mark>TBD</mark>



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**`Наземная станция (НС):`**<br> … <br><br> [CDSN](cdsn.md) ~~ [DSN](dsn.md) ~~ [ESTRACK](estrack.md) ~~ [IDSN](idsn.md) ~~ [SSC_GGSN](ssc_ggsn.md) ~~ [UDSC](udsc.md)|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Usuda_Deep_Space_Center>
1. <http://global.jaxa.jp/about/centers/udsc/index.html>


## The End

end of file
