# Reflex Aerospace GmbH
> 2022.04.01 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/r/reflex_as_logo1t.webp)](f/c/r/reflex_as_logo1.webp)|<info@reflexaerospace.com>, +49 152 285 399 36, Fax …;<br> *Grünberger Str. 48B, 10245 Berlin, Germany*<br> <https://www.reflexaerospace.com> ~~ [IG ⎆](https://www.instagram.com/reflexaerospace) ~~ [LI ⎆](https://www.linkedin.com/company/reflex-aerospace) ~~ [X ⎆](https://twitter.com/reflexaerospace)|
|:--|:--|
|**Business**|Small sats|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO — Walter Ballheimer|

**Reflex Aerospace GmbH** is the 1st venture backed satellite manufacturer in Germany. We aim to deliver tailored, high performance small satellite platforms to our customers an order of magnitude faster than anyone else on the market. To do so, we are carrying digital engineering & cutting edge manufacturing & assembly techniques to the extreme.

<p style="page-break-after:always"> </p>

## The End

end of file
