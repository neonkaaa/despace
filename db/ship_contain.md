# Контейнеры для транспортировки
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [КК](sc.md), [SGM](sc.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Контейнеры для транспортировки** — русскоязычный термин. **Shipping containeer** — англоязычный эквивалент.</small>



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Structures, gears, materials (SGM)](sc.md)】**<br> [Гермоконтейнер](гермоконтейнер.md) ~~ [Датчик](sensor.md) ~~ [Задел](margin.md) ~~ [Изделие](unit.md) ~~ [Испарение материалов](matc.md) ~~ [Кавитация](cavitation.md) ~~ [КЗУ](cinu.md) (ВБУ КТ) ~~ [КХГ](cgs.md) ~~ [Контейнеры для транспортировки](ship_contain.md) ~~ [Крейцкопф](crosshead.md) ~~ [Номинал](nominal.md) ~~ [ПУС](lag.md) ~~ [ПНА, ПОНА, ПСНА](devd.md) ~~ [Резерв](reserve.md) ~~ [Слайс](слайс.md) ~~ [ТСП](tsp.md) ~~ [Типичные формы КА](sc.md) ~~ [Толкатель](толкатель.md) ~~ [Унификация](commonality.md)|
|**`Космический комплекс (КК):`**<br> [Выводимая масса](throw_weight.md) ~~ [ГО и ПхО](lv.md) ~~ [Класс чистоты](clean_lvl.md) ~~ [Контейнеры для транспортировки](ship_contain.md) ~~ [СЧ](sui.md)|

1. Docs: …
1. <…>


## The End

end of file
