# SPACETIDE
> 2020.07.20 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/s/spacetide_logo1t.webp)](f/c/s/spacetide_logo1.webp)|<info@spacetide.org>, <mark>nophone</mark>, Fax …;<br> *Fukushima Building 7F X-NIHONBASHI, 1-5-3, Muromachi, Nihonbashi, Chuo-ku, Tokyo, Japan*<br> <https://spacetide.jp> ~~ [FB ⎆](https://www.facebook.com/SPACETIDE) ~~ [LI ⎆](https://www.linkedin.com/company/spacetide-foundation) ~~ [X ⎆](https://twitter.com/spacetide_conf)|
|:--|:--|
|**Business**|Promotes newspace businesses worldwide, annual conference|
|**Mission**|Create New Waves of the Space Industry|
|**Vision**|Drive the future of human society through the space industry|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO, President, Founder — Masayasu Ishida<br> ・COO, Director, Founder — Masashi Sato|

**SPACETIDE Foundation** is a Tokyo‑based nonprofit organization to promotes newspace businesses worldwide. Our main mission is to hold annual business conference. Founded 2015, estateed 2016.08.

Purpose: Design a sustainable ecosystem through connecting industries/communities/individuals & materializing the potential of the space industry.

**4 main activities.** We aim to achieve our vison & mission through four initiatives:

- **Conferences.** Provide opportunities where industry leaders around the world discuss hot topics of space business & increase awareness of the space business.
- **Events.** Hold small‑group events for intensive idea exchange & tight‑kit networking. Through the events, participants generate innovative, impactful ideas as well as share their visions of business that are not yet well recognized.
- **Compass.** Publish industry reports named SPACETIDE COMPASS to provdide latest news & increase your interest in the space business.
- **Spacetide Q.** Broardcast contents with leaders & experts from various industries via YouTube, about hot topics & key persons of space business.



## The End

end of file
