# TNL
> 2022.04.10 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/t/tnlab_logo1t.webp)](f/c/t/tnlab_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *2 Chome-28-4 Tomigaya, Shibuya City, Tokyo 151-8677, Japan*<br> <https://tnlabsa.wixsite.com> ~~ [FB ⎆](https://www.facebook.com/tnlab.sa) ~~ [X ⎆](https://twitter.com/tnlab1)|
|:--|:--|
|**Business**|Student association for space achitectures|
|**Mission**|Realize living in space|
|**Vision**|A place where friends who aspire to space architecture gather. A place where you can challenge.|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Tokai New-space Lab** (**TNL**, akso **TNLab**) is a Japanese student association (based in Tokai University) from different universities aimed to perform activities (brainstorms, meetings, awards, etc.) for space achitectures explorations. Founded 2015.06.

**Purpose of establishment.** Students who are interested in space architecture all over the country study together at TNL. It was established as a place to deepen exchanges. Students of various majors & grades learn one theme of space architecture from their individual interests. We would like to make space architecture more public.

**Activity content.** Activities as TNL depend on social conditions. We mainly do it online. Every month, we hold study sessions based on individual interests & irregularly design for competitions.



…



## The End

end of file
