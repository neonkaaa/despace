# Измеритель скорости и расстояния
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → **[ИСР](doppler.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Измеритель скорости и расстояния (ИСР)** — русскоязычный термин. **Doppler** — англоязычный эквивалент.</small>

**Измеритель скорости и расстояния (ИСР)**, также **допплеровский измеритель скорости и дальности (ДИСД)** — общее название технических средств для измерения линейной скорости и расстояния.

| |**[Фирма](contact.md)**|**Модели (масса, ㎏)**|
|:--|:--|:--|
|**RU**|[Концерн Вега](vega_k_1_2.md)|[ДИСД-Р](дисд‑р.md) () ~~ [ДИСД-ЛР](дисд‑лр.md) ()|
|•|~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |
|**US**|[Ball A&T](ball_at.md)| |

【**Table.** Manufacturers】

| | |
|:--|:--|
|**AE**|…|
|**AU**|…|
|**CA**|・[MDA](mda.md)|
|**CN**|…|
|**EU**|…|
|**IL**|…|
|**IN**|…|
|**JP**|…|
|**KR**|…|
|**RU**|・[Концерн Вега](vega_k_1_2.md)|
|**SA**|…|
|**SG**|…|
|**US**|・[Ball A&T](ball_at.md)|
|**VN**|…|



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Guidance, Navigation & Control (GNC)](gnc.md)】**<br> [CAN](can.md) ~~ [LVDS](lvds.md) ~~ [MIL‑STD‑1553](mil_std_1553.md) (МКО) ~~ [OS](os.md) ~~ [RS‑232, 422, 485](rs_xxx.md) ~~ [SpaceWire](spacewire.md) ~~ [АСН, САН](ans.md) ~~ [БНО](nnb.md)[MIL‑STD‑1553](mil_std_1553.md) (МКО)[БАППТ](eas.md) ~~ [БКС](cable.md) ~~ [БУ](eas.md) ~~ [БШВ](time.md) ~~ [Гироскоп](iu.md) ~~ [Дальномер](doppler.md) (ИСР) ~~ [ДМ](iu.md) ~~ [ЗД](sensor.md) ~~ [Компьютер](obc.md) (ЦВМ, БЦВМ) ~~ [Магнитометр](sensor.md) ~~ [МИХ](mic.md) ~~ [МКО](mil_std_1553.md) ~~ [ПО](soft.md) ~~ [ПНА, ПОНА, ПСНА](devd.md) ~~ [СД](sensor.md) ~~ [Система координат](coord_sys.md) ~~ [СОСБ](devd.md)|
|**`Измеритель скорости и расстояния (ИСР):`**<br> … <br>~ ~ ~ ~ ~<br> **РФ:** [ДИСД-Р](дисд‑р.md) () ~~ [ДИСД-ЛР](дисд‑лр.md) ()|

1. Docs: …
1. <https://ru.wikipedia.org/wiki/Доплеровский_измеритель>
1. <https://ru.wikipedia.org/wiki/Доплеровский_измеритель_скорости_и_сноса>
1. <http://www.ball.com/aerospace/markets-capabilities/capabilities/instruments-sensors/laser-lidar-imaging>


## The End

end of file
