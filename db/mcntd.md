# Средства связи и передачи данных
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → **[НКУ](sc.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Средства (система) связи и передачи данных (ССПД)** — русскоязычный термин. **Means of communication and transmission of data** — англоязычный эквивалент.</small>

**Средства (система) связи и передачи данных (ССПД)** — совокупность технических и программных средств, предназначенных для организации каналов и линий связи, интегрированной системы обмена информацией между центрами сбора и обработки информации с [космических аппаратов](sc.md), [РБ](lv.md), [РКН](lv.md), [ЦУПами](sc.md), командно‑измерительными и измерительными и командными пунктами.



## Описание ССПД
…

Согласно типовой [схеме деления](draw.md) ССПД входит в состав [НКУ](sc.md) или в [НАКУ](sc.md).



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**`Наземный комплекс управления (НКУ):`**<br> [БЦ](sc.md) ~~ [КИС](sc.md) ~~ [КСИСО](sc.md) ~~ [НИК](lm_sys.md) ~~ [НИП](sc.md) ~~ [НС](sc.md) ~~ [ПОЗ](fp.md) ~~ [СГК](cd_segm.md) ~~ [ССПД](mcntd.md) ~~ [ЦУП](sc.md)|

1. Docs:
   1. [ГОСТ 53802](гост_53802.md), п. 259
1. <…>


## The End

end of file
