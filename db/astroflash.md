# ASTROFLASH
> 2021.07.14 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/a/astroflash_logo1t.webp)](f/c/a/astroflash_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *4-1-3 Hongo, Bunkyo-ku, Tokyo, Japan*<br> <https://www.astroflash.co.jp> ~~ [FB ⎆](https://www.facebook.com/astroflash.info) ~~ [X ⎆](https://twitter.com/ASTROFLASH2019)|
|:--|:--|
|**Business**|[Small satellites](sc.md)|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO — Morito Katsuyama |

**ASTROFLASH, Inc.** is a space-sector startup from the University of Tokyo. Through our small satellite technology, we endeavor to bring space closer to our everyday life.

**Service.** ​Bridging the night sky & entertainment. ASTROFLASH is bringing to you the world’s 1st entertainment spacecraft, visible to the naked eye of observers here on Earth. Our spacecraft is capable of adjusting both color & brightness, & its 1st launch is planned to take place in 2021. Through our enterprise, we hope to give an opportunity for people to look up at the sky, & feel the world that lies beyond the clouds, among the moons, planets, & stars. Following the demonstration of our technology with our inaugural mission, we aim to expand our operation to multiple spacecraft flown simultaneously, enabling a larger variety of use of our platform.

<p style="page-break-after:always"> </p>

## The End

end of file
