# Циклограмма
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [Test](test.md), [ЛИ](rnd_e.md), [ЦГМ](obc.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Циклограмма** — русскоязычный термин. **Timeline** — англоязычный эквивалент.</small>

**Циклограмма** — временная диаграмма событий или функционирования [изделия](unit.md).



## Циклограмма пуска

**Циклограмма пуска** — точное расписание команд, исполняемых человеком либо подаваемых человеком или автоматикой на исполнительные органы технических комплексов. Обычно употребляется в контексте запусков космических ракет‑носителей и относится к командам для стартового ракетного комплекса, ракеты‑носителя, разгонного блока и пр.

Изначально, в частности, на карточке стреляющего для космического корабля 「Восток‑1」 при записи действия или команды указывалось точное время их начала и завершения. Впоследствии время стали отсчитывать с момента контакта подъёма ракеты, причём моменты времени до него брались со знаком 「минус」, а после — со знаком 「плюс」.

**Команды и действия**

1. **Сброс ШО** — штепсельный разъём отрывной отрывается от обтекателя корабля, после чего от него отводится заправочно‑дренажная мачта.
1. **Минутная готовность** — минута до команды 「Ключ на старт」.
1. **Ключ на старт** — при помощи поворота специального ключа подготовка запуска переводится на автоматический режим.
1. **Протяжка‑1** — протягивается полоса бумаги, на которой в наземном пункте подготовки к запуску начинается запись информации о ракете.
1. **Продувка** — топливные коммуникации и другие элементы ракетного двигателя продуваются азотом для противопожарного освобождения их от паров горючего и окислителя.
1. **Протяжка‑2** — протягивается полоса бумаги, на которой в наземном пункте подготовки к запуску начинается запись информации о стартовом комплексе.
1. **Ключ на дренаж** — закрытие дренажных клапанов, через которые шёл отвод испаряющегося жидкого кислорода от ракеты в атмосферу, что визуально проявлялось в белых облачках, окутывающих ракету. Дренаж шёл одновременно с возмещением испарившегося кислорода в баках с окислителем, которое по этой команде также прекращается.
1. **Земля‑борт** — от ракеты отходит кабель‑мачта, ракета готова перейти на собственное питание.
1. **Пуск** — начинается подача компонентов топлива в двигательную установку.
1. **Зажигание** — воспламенение топлива в камерах сгорания.
1. **Предварительная, промежуточная, главная, подъём** — этапы набора тяги ДУ.
1. **Есть контакт подъёма** — сработал датчик, фиксирующий отрыв ракеты от стартового стола.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Test](test.md)】**<br> [JTAG](jtag.md) ~~ [Proto fligt model](pfm.md) ~~ [Безэховая камера](ach.md) ~~ [Валидация](vnv.md) ~~ [Класс чистоты](clean_lvl.md) ~~ [КПЭО](ctpr.md) ~~ [Перечень методик испытаний](list_tp.md) ~~ [Программа и методика испытаний](pmot.md) ~~ [Опытный образец](pilot_sample.md) ~~ [Циклограмма](obc.md) ~~ [Штатный образец](flight_unit.md) ~~ [ЭО](test.md) ~~ [Экспериментально‑теоретический метод](etetm.md)|
|**`Циклограмма (ЦГМ):`**<br> [Точки Лагранжа](l_points.md)|

1. Docs: …
1. <https://ru.wikipedia.org/wiki/Циклограмма>
1. <https://ru.wiktionary.org/wiki/циклограмма>


## The End

end of file
