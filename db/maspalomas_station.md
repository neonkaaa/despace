# Maspalomas Station
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [НК](sc.md), **[НС](sc.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Maspalomas Station** — англоязычный термин, не имеющий аналога в русском языке. **Станция Маспаломас** — дословный перевод с английского на русский.</small>

**Maspalomas Station** is an INTA‑operated, [ESTRACK](estrack.md) radio antenna station for communication with spacecraft located at the southern area of Gran Canaria island, on the INTA campus. The site hosts a 15‑metre antenna with reception in S‑ and X‑[band](comms.md) and transmission in S‑band.

|**Фото**|**Карта**|
|:--|:--|
|[![](f/gs/maspalomas_station_pic1t.webp)](f/gs/maspalomas_station_pic1.webp)|[![](f/gs/maspalomas_station_map1t.webp)](f/gs/maspalomas_station_map1.webp)|



## Характеристики
|**Характеристика**|**Описание**|
|:--|:--|
|Антенны|15 м — 1 шт.|
|Дальность связи, ㎞| |
|[Диапазоны частот](comms.md)|S, X — 🚀↘; S — ♁↗|
|Расстояние до…|3 000 — до экватора;<br> 5 500 — до NASA;<br> 5 800 — до НПОЛ|



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**`Наземная станция (НС):`**<br> … <br><br> [CDSN](cdsn.md) ~~ [DSN](dsn.md) ~~ [ESTRACK](estrack.md) ~~ [IDSN](idsn.md) ~~ [SSC_GGSN](ssc_ggsn.md) ~~ [UDSC](udsc.md)|

1. Docs: …
1. <http://en.wikipedia.org/wiki/Maspalomas_Station>
1. <https://www.esa.int/Our_Activities/Operations/Estrack/Maspalomas_station>



## The End

end of file
