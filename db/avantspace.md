# AvantSpace
> 2020.01.18 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/a/avantspace_logo1t.webp)](f/c/a/avantspace_logo1.webp)|<info@avantspace.com>, +7(499)994-95-05, Fax …;<br> *Луговая 4/7, Инновационный центр Сколково, г. Москва, 143026, RU.*<br> <http://www.avantspace.com>・ <http://projects.avantspace.com> ~~ [LI ⎆](https://www.linkedin.com/company/avant-space-systems)|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**AvantSpace** — Сколковский стартап, производящий малые [спутники](sc.md), ионные [двигатели](ps.md), [радиопередатчики](comms.md).

We are working since 2010, providing cost-effective solutions, based on microsatellite technology, and a high level of technical support at all stages of the product life cycle, to our clients. Our approach is to being attentive to the customer’s wishes, working on time and flexible pricing. The core of small but efficient and professional team are young professionals with the real experience in the design and operation of spacecrafts, incl. microsatellites.

Domestic technology, our own experience and promising developments of our cooperation are the basis of all our key developments.

**Продукция:**

- Спутники <http://projects.avantspace.com/satellites.html>.
   - The 1st Polar Broadband Communication System, 76 кг, попутный запуск с Арктика‑М;
   - The 1st Polar Broadband System for both Arctic and Antarctica Regions, 200 кг, попутный запуск с Арктика‑М;
   - 8 satellites of the Kronus-O series.
- Подсистемы.
   - Radio system UHF‑band на ExoMars-2016;
   - Command and Data Handling for Science and Power Module (ISS).

**Component Testing.** Avant-Space supports our customers with all device type. We work with our customers to develop test plans in accordance with your specific program requirements. Testing can consist of:

- Electrical, environmental, and other test services
- Testing for space, MIL, and industrial level qualification
- Manufacture and test of obsolete / heritage devices
- Upscreening, special testing, Quality Conformance Inspection (QCI) performed regularly on a wide variety of parts
- Complete die lot qualification
- From basic characterization to full qualification
- Screening and lot qualification
- Total ionizing dose and single event effects



## The End

end of file
