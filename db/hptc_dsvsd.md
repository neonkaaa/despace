# ДСВСД (гипотетический)
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [Venus](venus.md), **[Project](project.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Длительное Существование на Венере Самостоятельного Долгостроя (ДСВСД)** — русскоязычный термин, не имеющий аналога в английском языке. **Venusian Long Lived Self-dependent Neverending Project (DSVSD)** — дословный перевод с русского на английский.</small>

**Длительное Существование на Венере Самостоятельного Долгостроя (ДСВСД)**.  
Начат 2019.05.12.


1. АБ: серно‑натриевый (оптимум при 400 ℃)
1. БКС: никель
1. Конструкция: никель, титан


**Д**лительное **С**уществование на **В**енере **С**амостоятельного **Д**олгостроя **(ДСВСД)**.
1. [БКС](cable.md): никель
1. [SGM](sc.md): никель, титан
1. [ХИТ](eb.md): серно‑натриевый (оптимум при 400 ℃)



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[](.md)】**<br> <mark>NOCAT</mark>|

1. Docs: …
1. <…>


## The End

end of file
