# Cooperation
> 2022.01.08 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md), [Persons](person.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> And these people forbid us to pick our noses!

|**Агентство**|**Бюджет, $ млн (%)**|**Год**|**Примечание**|
|:--|:--|:--|:--|
|США, [NASA](nasa.md)|17 700 (100 %)|2013, прогноз| |
|Европа, [ESA](esa.md)|5 500 (31 %)|2012|€ 4 020 100 млн|
|Россия, [Роскосмос](roskosmos.md)|4 700 (27 %)|2014|₽ 165 800 млн|
|Япония, [JAXA](jaxa.md)|2 460 (14 %)|2010|¥ 229 000 млн|
|Китай, [CNSA](cnsa.md)|1 300 (7.3 %)|2009, оценка| |
|Индия, [ISRO](isro.md)|1 200 (6.8 %)|2010 ‑ 2011| |
|Канада, [CSA](csa.md)|425 (2.4 %)|2010 ‑ 2011| |
|Корея Южная, [KARI](kari.md)|366 (2 %)|2007| |
|Алжир, [ASAL](asal.md)|360 (2 %)|2002| |
|Украина, [ГКА](nkau.md)|300 (1.7 %)| |
|Аргентина, [CONAE](conae.md)|180 (1 %)|2014| |

1. [ALPO](alpo.md) — Association of Lunar & Planetary Observers
2. [CCSDS](ccsds.md) — международный Консультативный Комитет по косм. системам передачи данных
3. [IAF](iaf.md) — International astronautical federation — Международная астронавтическая федерация
4. [INCOSE](incose.md)
5. [Project Management Institute](project_management_institute.md)
6. [SEDS](seds.md) — Students for the Exploration & Development of Space
7. [SGAC](sgac.md) represent students & young space profs to the United Nations, States, & space agencies
8. [Synergy Moon](synergy_moon.md)
9. [Болото](swamp.md)
10. [Worldview in different countries](wwidc.md)
11. For the person’s info page see **[Persons](person.md)**



## (TBD±) Australia

(**E**)du / (**N**)on‑profit・(**L**)aunch serv.・(**P**)romote・(**R**)&D&MAIT / (**C**)onsult.・(**D**)ata provider

1. (–––––) **【[ASRI](asri.md)】** — Australian Space Agency
2. (–––––) **【[CSIRO](csiro.md)】** — Australian national science agency
3. (E––––) **[ANU](anu.md)** — Australian National University
4. (––––D) **[Arlula](arlula.md)** — A single point of access for a global network of sats imagery
5. (–––––) **[ASL](asl.md)** — Australia Space Launch
6. (N–P––) **[AYAA](ayaa.md)** — Education, awareness, involvement in the A/S industry to youth
7. (–––R–) **[Fleet Space Tech.](fleet_space_tech.md)** — IoT with nanosat constellation
8. (–L–R–) **[Gilmour Space Tech.](gilmour_st.md)** — Develop / launch low cost hybrid LV & engines for small sats to LEO
9. (–––RD) **[HEO Robotics](heo_robotics.md)** — Acquire imagery of sats, space‑debris, resource‑rich asteroids
10. (–––––) **[Myriota](myriota.md)** — IoT with nanosat constellation
11. (–––C–) **[Nominal Systems](nominal_sys.md)** — Software for simulation, system analysis
12. (–––CD) **[Optus](optus.md)** — Mobile, tel., internet, sat, entertainment & biz network services
13. (–––––) **[Saber Astronautics](saber_an.md)** — Model the interaction between SC telemetry & the space environment
14. (–––––) **[SERC](serc.md)** — Space debris removal
15. (–––P–) **[SIAA](siaa.md)** — Promote the growth of the Australian space industry
16. (–––C–) **[Shoal](shoal.md)** — Systems engineering consulting
17. (–––––) **[Southern Launch](southern_launch.md)** — Infrastructure / logistics support for (sub)orbital launches
18. (–––R–) **[Spiral Blue](spiral_blue.md)** — CPU



## Canada
(**E**)du / (**N**)on‑profit・(**L**)aunch serv.・(**P**)romote・(**R**)&D&MAIT / (**C**)onsult.・(**D**)ata provider

1. (–––––) **【[CSA](csa.md)】** (QC) — Canadian Space Agency
2. (–––C–) **[ADGA](adga.md)** (ON) — Advanced technology solutions, professional engineering & consultancy
3. (––P––) **[AIAC](aiac.md)** (ON) — Aerospace Industries Association of Canada
4. (N––R–) **[AlbertaSat](albertasat.md)** (AB) — (Student community) CubeSats R&D
5. (E––R–) **[August IST](august_ist.md)** (ON) — Space infrastructure, R&D, edu., commercialization of innov.solutions
6. (–––R–) **[Baryon Dynam.](baryon_dyn.md)** (ON) — Power, space propulsion, engineering designs & expertise
7. (E–PC–) **[BRASS](brass.md)** (ON) — Mission design, analysis, planning, operations, project mgmt
8. (–––C–) **[C-CORE](c_core.md)** (ON) — Risks mitigation, remote sensing systems, ice/geotech. engineering
9. (–L–R–) **[C6 Launch](c6_launch.md)** (ON) — Launch services, Launcher design for CubeSats
10. (–––––) **[Calian AT](calian_at.md)** (SK,QC) — R&D & AIT of products & services in comms
11. (–––R–) **[Canadensys](canadensys.md)** (ON) — Cubesats, rovers, SC systems, avionics, sensors
12. (E–P––) **[CASI](casi.md)** (ON) — Canadian Aeronautics & Space Institute
13. (E––––) **[CCERA](ccera.md)** (ON) — Canadian Center for Experimental Radio Astronomy
14. (–L–––) **[Columbiad](columbiad.md)** (ON) — Commercial low‑cost space launch
15. (E––––) **[Concordia Un.](concordia_univ.md)** (QC) — Higher education
16. (E–P––) **[CPSX](cpsx.md)** (ON) — Planetary science, exploration research, training
17. (–––R–) **[CSMC](csmc.md)** (ON) — Space mining, resource extraction & exploration
18. (N–P––) **[CSS](css.md)** (ON) — Sponsorship & promotion for Canadians in the space sector
19. (–––R–) **[Deltion Ltd](deltion.md)** (ON) — Tech. dev./improvement, automation & robotics, Moon mining
20. (N––R–) **[DSS](dss.md)** (NS) — CubeSats development. Student community
21. (–––RD) **[EarthDaily](earthdaily.md)** (BC) — Remote sensing, Geospatial info
22. (–––C–) **[Euroconsult](euroconsult.md)** (QC) — Strategic, research, summits, training programs
23. (–––C–) **[Geocentrix](geocentrix_tech.md)** (BC) — Sat/Launch mission design, analysis, operations, mgmt, consulting
24. (–––R–) **[GHGSat](ghgsat.md)** (QC) — Gases monitoring, gas sensors & imaging interferometers for Cubesats
25. (–––R–) **[Globvision](globvision.md)** (QC) — Software for spacecraft, support & automate the design
26. (–––R–) **[Honeywell A/S](honeywell_as.md)** () — Software, signal & data processing, flight & ground operations
27. (–––R–) **[Kepler Comms](kepler_comms.md)** (ON) — CubeSats for global comms & IoT, antennas, data retranslation
28. (E––––) **[Lassonde SoE](lassonde_soe.md)** (ON) — Higher education
29. (N––R–) **[Launch Canada](launch_ca.md)** () — (Student community) LV & small sats building
30. (–L–––) **[Luna D&I](luna_dni.md)** (ON) — Launch & support for experiments in space
31. (–––R–) **[Macfab](macfab.md)** (ON) — ADCS, structures, comms, electronics, sensors, propulsion, separation
32. (–––R–) **[Magellan A/S](magellan_as.md)** (ON) — Sats R&D, small sat bus platform, ground facilities
33. (–––R–) **[MCSS Inc](mcss_inc.md)** (ON) — Software, simulation, mission operations, onboard autonomy, & AI
34. (–––R–) **[MDA](mda.md)** (ON,QC) — Robotics, Space operations, Ground stations, Sat systems, Software
35. (–L–––) **[MLS](mls.md)** (NS) — Launch services using Cyclone-4M LV in Nova Skotia
36. (–––R–) **[MSCI](msci.md)** (ON) — RW, rate measurement units, microsats, attitude control systems
37. (–––R–) **[NGC A/S](ngc_as.md)** (QC) — Software for GNC, simulation, data processing
38. (–––R–) **[Northstar ESI](northstar_esi.md)** (QC) — Monitor space (object tracking), from space, via a sat constellation
39. (–––R–) **[Obruta SS](obruta_ss.md)** (ON) — Mechanisms for docking & in‑orbit servicing
40. (N––R–) **[PolyOrbite](polyorbite.md)** (QC) — CubeSat’s R&D. Student society
41. (–––R–) **[Pratt&Whitney](pratt_n_whitney.md)** (QC) — Engines, gas turbines
42. (–––R–) **[QSTC](qstc.md)** (QC) — GEO sats, el. propulsion, materials, remote sensing, robotics
43. (N––C–) **[Satcan](satcan.md)** (ON) — Engineering & business consultancy for space sat telecom & tech
44. (N–P––) **[SEDS Canada](seds.md)** (ON) — (Student community) Promoting Exploration & Development of Space
45. (–––R–) **[Sinclair I/P](sinclair_ip.md)** (ON) — Star trackers, RW, optical comms, software, training & expertise
46. (–––R–) **[SpaceBridge](spacebridge.md)** (QC) — Sat network equip. & services
47. (–L–R–) **[SpaceRyde](spaceryde.md)** (ON) — On‑schedule, dedicated launch for CubeSats using balloons
48. (N––R–) **[Space Concord.](space_concordia.md)** (QC) — (Student community) CubeSats, small rockets, robots
49. (–––R–) **[Space Engine](space_engs.md)** (AL) — SSTO propulsion, pumps, compressors, gear boxes, PM motors
50. (–––CD) **[SkyWatch](skywatch.md)** (ON) — Providing commercial sat imagery
51. (–––C–) **[SSCL](sscl.md)** (ON) — Consulting for space policy, planning, strategy, & technology
52. (–––C–) **[Telecan Space](telecan_space.md)** (QC) — Consulting, Training, Thermal R&D & testing
53. (–––RD) **[Telesat](telesat.md)** (ON) — Sat comms & integration services, sat operator
54. (–––R–) **[Thales Canada](thales.md)** (All) — Sat & LV hardware
55. (–––R–) **[Thoth Tech.](thoth_tech.md)** (ON) — Cameras, In‑orbit tracking, thermo‑vac/vibr. tests, mission planning
56. (E––R–) **[UTIAS](utias.md)** (ON) — Research for space mechatronics, robotics, microsats, fluid dynamics
57. (–––CD) **[Wyvern Space](wyvern.md)** (AB) — Providing commercial sat imagery from small sats
58. (–––R–) **[Xiphos](xiphos.md)** (QC) — CPU, Firmware & Software

**Not space:** Avant Aerospace・ CAE

**Org. lists:** <http://www.spaceq.ca/canadian-space-companies> ・<https://aiac.ca/product-categories/space-systems/#sat-systems-components> ・<https://www.thecanadianencyclopedia.ca/en/article/aerospace-industry>

[![](f/c/map/canada_political_rut.webp)](f/c/map/canada_political_ru.webp)



## (TBD) China

(**E**)du / (**N**)on‑profit・(**L**)aunch serv.・(**P**)romote・(**R**)&D&MAIT / (**C**)onsult.・(**D**)ata provider

1. (–––––) **【[CNSA](cnsa.md)】** — Chinese Space Agency
2. (–L–R–) **[LandSpace ](landspace.md)** — R&D & operations of small & medium LV, launch services



## (TBD) Europe

(**E**)du / (**N**)on‑profit・(**L**)aunch serv.・(**P**)romote・(**R**)&D&MAIT / (**C**)onsult.・(**D**)ata provider

1. (–––––) **【[ESA](esa.md)】** (EU) — Исследование космоса. [Роскосмос](roskosmos.md)** и  ГНИО в одном лице
2. (–––––) **【[ESTEC](estec.md)】** (NL) — Разработка и создание космических технологий и КА
3. (–––––) **[PTScientists](ptscientists.md)** — …
4. (–––––) **[Space Mining Tech.](space_mintech.md)** — Moon & asteroids mining

### Austria (AT)

1. (N––––) **[Moon Village As.](moon_village_a.md)** — Forum for govt/industry/edu. / etc. for the Moon Village dev.
2. (–––––) **[SGAC](sgac.md)** — Represent students & young space pros to the United Nations

### Belgium (BE)

1. (–––––) **[EVONA](evona.md)** — …

### Bulgaria (BG)

1. (–––R–) **[Endurosat](endurosat.md)** — CubeSat R&D

### Czechia (CZ)

1. (–––––) **[CUNI](cuni.md)** — Карлов университет. Один из старейших университетов мира
2. (–––––) **[OHB CZ](ohb.md)** — …
3. (–––––) **[SAB A/S](sab_as.md)** — TBD
4. (–––––) **[Serenum Space](serenum_space.md)** — TBD
5. (–––––) **[UFA](ufa.md)** — …
6. <https://kosmok.space/>
7. <https://www.vzlu.cz/>

### Denmark (DK)

1. (–––R–) **[SAGA SA](saga_sa.md)** — Space architectures
2. (–––––) **[Terma A/S](terma.md)** — …

### Finland (FL)

1. (–––R–) **[ICEYE](iceye.md)** — Разработка и создание микроспутников и кубсатов
2. (–––R–) **[ReOrbit](reorbit.md)** — R&D for LEO / GEO sats

### France (FR)

1. (–––––) **[Airbus](airbus.md)** — …
2. (–––R–) **[Astroscale](astroscale.md)** — Space debris removal, sat life extension services, EoL services
3. (–––––) **[CNES](cnes.md)** — Французское космическое агентство
4. (–––––) **COMAT** — …
5. (–––R–) **[E-Space](e_space.md)** — Telecom sats
6. (–––––) **EREMS** — …
7. (–––C–) **[Euroconsult](euroconsult.md)** — Strategic, research, summits, training programs
8. (–––R–) **[Exotrail](exotrail.md)** — Electric propulsion, simulation / operation software, space tugs
9. (–––––) **GMV** — …
10. (–––R–) **[Infinite Orbits](infinite_orbits.md)** — In‑orbit servicing, servicing platforms
11. (E––––) **[Int. Space Univ.](int_space_univ.md)** — Space education for a changing world
12. (–––––) **[IRAP](irap.md)** — Research Institute in Astrophysics & Planetology
13. (E––––) **[ISU](isu.md)** — …
14. (–––––) **Latitude** — …
15. (–––––) **[LATMOS](latmos.md)** — Фундам. исследования наземных и планетных атмосфер
16. (–––––) **[LMD](lmd.md)** — …
17. (–––––) **[Paris Obs.](paris_obs.md)** — …
18. (–––R–) **[Safran](safran.md)** — Propulsion, optics
19. (–––––) **[Saft](saft.md)** — Элементы электропитания
20. (–––––) **[Sodern](sodern.md)** — …
21. (–––––) **[Sorbonne Univ.](sorbonne_univ.md)** — …
22. (–––R–) **[Spartan Space](spartan_space.md)** — Space habitats
23. (–L–R–) **[Thales AS](thales_as.md)** — R&D of SC, launch services, comms, sat control, Earth observation
24. (–––––) **[UVSQ](uvsq.md)** — …

### Germany (DE)

1. (–––R–) **[ASP Equipment](asp_equip.md)** — Power: DC/DC converters, IPU, PCDU, EPC, propulsion power
2. (N–P––) **[Disrupt Space](disrupt_space.md)** — Startup accelerator, entrepreneurship, & community
3. (–––––) **[DLR](dlr.md)** — Германское космическое агентство
4. (–––R–) **[Morpheus Space](morpheus_space.md)** — Electric propulsion & AI
5. (–––R–) **[OHB SE](ohb.md)** — Sats, exploration, human spaceflight
6. (–––R–) **[Reflex Aerospace](reflex_as.md)** — Small sats
7. (–L–––) **[Exolaunch](exolaunch.md)** — Launch booking
8. **[NEUROSPACE](neurospace.md)** — 

### Greece (GR)

1. (–––––) **[ADS](ads.md)** — Military Aircraft; SCS, Electronics, Comms, Intelligence, Security
2. (N–P––) **[Libre space](libre_space_f.md)** — Promote, advance, dev. libre technologies & knowledge for space
3. (–––––) **[Neutron Star S.](neutron_ss.md)** — Electric propulsion (EP) systems & subsystems
4. (–––––) **[OPC LAM](opc_lam.md)** — Разработка и создание двигателей, ДУ

### Italy (IT)

1. (–––––) **[ARCA Dynamics](arca_dymanics.md)** — Cubesats, GNC, space robotics, AI
2. (–––R–) **[D-Orbit](d_orbit.md)** — Moving, precise deployment and removing satellites
3. (E––R–) **[GAUSS Srl](gauss_srl.md)** — Cubesats, GS operations, space debris observation
4. (–––––) **[IRSPS](irsps.md)** — …
5. (–––––) **[Leonardo](leonardo.md)** — …
6. (–L–R–) **[Zero 2 Infinity](zero_2_infinity.md)** — Space transportation, launch small sats

### Luxembourg (LU)

1. (E–PR–) **【[LU Space Agency](lu_space_agency.md)】** — Luxembourg Space Agency
2. (–––––) **[ESRIC](esric.md)** — …
3. (–––R–) **[LuxSpace Sarl](luxspace.md)** — Components & technolofy, microsats, apps & services
4. (–––R–) **[Maana Electric](maana_electric.md)** — Electric systems, in‑situ el.systems production
5. (–––C–) **[Odysseus](odysseus.md)** — Support for R&D, operations, launches, purchase, logistics
6. (–––R–) **[OQ Technology ](oq_technology.md)** — Sats constellation for IoT
7. (–––––) **[SES Satellites](ses_sats.md)** — Deliver video & data solutions, LEO / MEO sats

### Netherlands (NL)

1. (–––––) **[Bradford Eng.](bradford_eng.md)** — СУ КА: ДМ, СД, смесители топлива
2. (–––R–) **[Maana Electric](maana_electric.md)** — Electric systems, in‑situ el.systems production
3. (–––––) **[TU Delft](tu_delft.md)** — …
4. Aurora — …

### Norway (NO)

1. (–––R–) **[Nammo](nammo.md)** — Engines

### Poland (PL)

1. (––P––) **[EU Space Found.](eu_space_foundation.md)** — Promote knowledge in science, engineering, technology & math
2. (–––R–) **[Waven](waven.md)** — Energy harvesting — alt. to photovoltaics & RTG. Consump. reducing
3. (E––––) **[Zielona Univ.](zielona_univ.md)** — …

### Portugal (PT)

1. (–––––) **[IA](ia.md)** — Instituto de Astrofísica e Ciências do Espaço

### Slovakia (SK)

1. (–––P–) **[Slovak Space Office](slovak_so.md)** — Support space activities in Slovakia
2. (–––––) **[UEF SAV](uef_sav.md)** — Ядерная, субатомная физика, физика космоса, биофизика

### Spain (ES)

1. (–––––) **[Hisdesat](hisdesat.md)** — …
2. (–L–R–) **[PLD Space](pld_space.md)** — Reusable (sub)orbital LV for small sats
3. (–––R–) **[Plus Ultra](plus_ultra.md)** — Off‑Earth infrastructure, Lunar satellites
4. (–––––) **[Solar MEMS](solarmems.md)** — СД

### Sweden (SE)

1. (–––––) **[AAC CS](aac_cs.md)** — Кубсаты, малые КА, СЧ
2. (–––––) **[IRF](icomms.md)** — Фундам. исследования косм. физики и физики атмосферы
3. (–––––) **[Swedish SC](swedish_sc.md)** — КА, кубсаты, аэростаты, НС

### Switzerland (CH)

1. (–––––) **[Astrocast](astrocast.md)** — Sat operator, constellation of nano‑sats on LEO, IoT comms
2. (–––R–) **[ClearSpace SA](clearspace_sa.md)** — Space debris removal: technologies & services
3. (–––R–) **[Beyond Gravity](beyond_gravity.md)** — SC / LV elecronics & components
4. (N––––) **[Space Expl. Inst.](space_explor_inst.md)** — Developing space exploration activities, instruments
5. (–––––) **[SSO](sso.md)** — Swiss Space Office, national space program of Switzerland
6. (–––––) **[UNIBE](unibe.md)** — …

### United Kingdom (UK)

1. (ELPR–) **【[UKSA](uksa.md)】** — UK Space Agency
2. (–––R–) **[Asteroid Mining](asteroid_mc.md)** — Robotic & satellite platforms for asteroid mining
3. (–L–––) **[AvalonSpace](avalonspace.md)** — Green & cost‑effective UK launches
4. (–––––) **[B2Space](b2space.md)** — Launch services (balloon & LV) for small‑micro sats
5. (–––R–) **[Clear Space](clear_space.md)** — In‑orbit servicing and space debris removal
6. (–––––) **[Cobham](cobham.md)** — SC units, CPUs
7. (–––––) **[Edinburgh Univ.](edinburgh_univ.md)** — University of Edinburgh
9. (E–P––) **[InnovaSpace](innovaspace.md)** — Promote / conduct R&D, education, innovations for humanity in space
10. (–––––) **[Open Cosmos](open_cosmos.md)** — Sats R&D
11. (–––R–) **[Open Source Sat.](open_source_sat.md)** — R&D for small open source satellites
12. (–––––) **[Oxford Univ.](oxford_univ.md)** — …
13. (–––––) **[QitetiQ](qitetiq.md)** — Comms & security, spacecraft & subsystems, spaceflight training
14. (–––R–) **[Space Forge](space_forge.md)** — Reusable sats for material manufacturing in space
15. (–––R–) **[Space Talos](space_talos.md)** — Radiation shielding technologies, testing
16. (–––––) **[Telespazio](telespazio.md)** — …
17. (–––––) **[UCL](ucl.md)** — …

**Org. lists:** <https://space-agency.public.lu/en/expertise/space-directory.html>・<https://tem.fi/en/space>



## (TBD) India

(**E**)du / (**N**)on‑profit・(**L**)aunch serv.・(**P**)romote・(**R**)&D&MAIT / (**C**)onsult.・(**D**)ata provider

1. (–––––) **【[ISRO](isro.md)】** — Indian Space Agency
2. (E––––) **[BSIP](bsip.md)** — Higher education
3. (–––––) **[Skyroot A/S](skyroot_as.md)** — (TBD) LVs
4. (–––––) **[Space Dev. Network](space_dev_netw.md)** — (TBD) Space Development Network
5. (N–P––) **[Space Dev. Nexus](sd_nexus.md)** — Various space developments, integration, colonization
6. (–––R–) **[Synergy Moon](synergy_moon.md)** — Lunar rover
7. (–––––) **[Team Indus](team_indus.md)** — (TBD)
8. (–L–R–) **[Timewarp Space](timewarp_space.md)** — LV for small sats
9. (E––––) **[UPES](upes.md)** — Higher education



## (TBD±) Israel

(**E**)du / (**N**)on‑profit・(**L**)aunch serv.・(**P**)romote・(**R**)&D&MAIT / (**C**)onsult.・(**D**)ata provider

1. (ELPR–) **【[ISA](isa.md)】** — National aerospace & space agency
2. (–––––) **[AMOS-Spacecom](amos_spacecom.md)** — Fixed‑sat operator, comms solutions
3. (E––––) **[Asher SRI](asher_sri.md)** — Asher Space Research Institute
4. (–––R–) **[Astroscale](astroscale.md)** — Space debris removal, sat life extension services, EoL services
5. (E–P––) **[D-MARS](d_mars.md)** — Promote the space sector. Simulations, training, tech testing, edu.
6. (–––––) **[DSO Tech. IL](dso_tech.md)** — …
7. (N––––) **[Herzliya Space Lab](herzliya_space_lab.md)** — Student sat building lab
8. (–––––) **[IAI](iai.md)** — …
9. (–––––) **[ImageSat Int.](imagesat_int.md)** — Geospatial solutions & services: imagery, data, analytics
10. (–––––) **[Israeli Air Force](israeli_af.md)** — …
11. (–––––) **[NovelSat](novelsat.md)** — Content connectivity solutions for sat comms
12. (–––R–) **[NSLComm](nslcomm.md)** — Antenna technologies
13. (–––R–) **[Rafael](rafael.md)** — LV
14. (–––R–) **[Sim.Space](sim_space.md)** — Simulation software, Hardware-In-The-Loop (HIL) tests
15. (–––––) **[Space-Nest](space_nest.md)** — Space tech / applications incubator
16. (––P––) **[Spacecialist](spacecialist.md)** — Nano‑Cubesats, promote a prosperous civilian space industry
17. (––PR–) **[SpaceIL](spaceil.md)** — Promote science & science education. Moon landers
18. (E––––) **[Technion](technion.md)** — Israel Institute of Technology
19. (–––RD) **[WeSpace Tech.](wespace_tech.md)** — R&D robots, operation, data

**Not space:** Meteor Aerospace



## Japan

(**E**)du / (**N**)on‑profit・(**L**)aunch serv.・(**P**)romote・(**R**)&D&MAIT / (**C**)onsult.・(**D**)ata provider

1. (ELPR–) **【[JAXA](jaxa.md)】** 13 — National aerospace & space agency
2. (–––R–) **【[JAXA ESCC](isas.md)】** 14 — ET materials documentation, preservation, preparation, distribution
3. (–––R–) **【[JAXA ISAS](isas.md)】** 14 — Astrophysical explorations with rockets, SC
4. (–––R–) **【[JAXA Kakuda](kakuda_sc.md)】** 4 — R&D, testing engines & propulsion systems
5. (–––R–) **【[JAXA Noshiro](noshiro_rtc.md)】** 5 — R&D, testing of rockets & engines
6. (E––R–) **【[JAXA Tsukuba](tsukuba_sc.md)】** 8 — Astronauts training, manufactures sats & ISS modules
7. N–PC– **【[SCJ](scj.md)】** 13 — Major Japanese science organization
8. (–––R–) **[3D Print. Corp.](3d_printing_corp.md)** 14 — Composite 3D printing
9. (E––C–) **[ABLab](ablab.md)** 13 — Community for the space development, space consulting
10. (–––R–) **[ALE](ale.md)** 13 — Space entertainment (sky canvas), data service, small sats
11. (E––R–) **[Amanogi](amanogi.md)** 13 — Sensors & advanced sat data analysis solutions
12. (E––C–) **[Amulapo](amulapo.md)** 13 — Space experience content using ICT technology (xR, robots, AI)
13. (–––R–) **[ArkEdge Space](arkedge_space.md)** 13 — Small sats
14. (E––––) **[ASE-Lab](ase_lab.md)** 13 — Students community
15. (–L–––) **[Astrocean](astrocean.md)** 13 — Hybrid rocket sea launch, sub‑orbital & orbital rocket sea launch
16. (–––R–) **[Astroflash](astroflash.md)** 13 — Small sats
17. (–––R–) **[AstroTerrace](astroterrace.md)** 13 — Space development, marketing, research, consulting
18. (–––R–) **[Astroscale](astroscale.md)** 13 — Space debris removal, sat life extension services, EoL services
19. (–L–R–) **[Axelspace](axelspace.md)** 13 — Ready‑to‑launch space solutions, micro‑sat technology
20. (E–P––) **[Bascule](bascule.md)** 13 — Advert., media, products, sports, edu., mobility, urban dev., space
21. (–––––) **[Boeing](boeing.md)** — Разработка, эксплуатация космической техники, ПКА
22. (–––R–) **[Bull](bull.md)** 9 — Sats deorbiting
23. (–––R–) **[Canon El. Space](ce_space.md)** 13 — Small Earth sats, their components
24. (–––R–) **[Cosmobloom](cosmobloom.md)** 13 — Foldable structures
25. (E––C–) **[CTST](ctst.md)** 13 — Space tourism, consulting, education
26. (––––D) **[DATAFLUCT](datafluct.md)** 13 — Software for sat data analysis
27. (–––C–) **[Digital Blast](digital_blast.md)** 13 — Business & technology consulting
28. (–––R–) **[Dymon](dymon.md)** 13 — Robots, rovers, antennas
29. (–––R–) **[ElevationSpace](elevationspace.md)** 4 — Space constructions, bases, transportations, experiments
30. (–––C–) **[Eunoia Space](eunoia_space.md)** — 14 JP & overseas space industry bridging
31. (–––C–) **[Euroconsult](euroconsult.md)** 13 — Strategic, research, summits, training programs
32. (–––R–) **[eVanTEC Co Ltd](evantec_co.md)** 13 — Software, sat support
33. (–––R–) **[GS Yuasa](gs_yuasa.md)** 26 — Batteries
34. (–––R–) **[Hamamatsu Phot.](hamamatsu_phot.md)** 22 — Optical sensors, trackers, cameras
35. (–––R–) **[Harada Seiki](harada_seiki.md)** 22 — Parts for sats, rovers, small telescopes for observing Earth
36. (–––R–) **[HIREC](hirec.md)** 14 — Electronic components, Reliability / quality assurance
37. (–––R–) **[Hokkaido Sat.](hokkaido_sat.md)** 1 — Cameras
38. (–L–R–) **[IHI](ihi.md)** 13 — LV, engines & their components R&D
39. (–––R–) **[IMV Corp.](imv_corp.md)** 27 — Ground vibration test & measurement systems
40. (–––C–) **[Inclusive SC](inclusive_sc.md)** 13 — Artificial sat data utilization consulting
41. (–––R–) **[Infostellar](infostellar.md)** 13 — Sat Ground Segment as a Service provider, cubesat components
42. (–L–R–) **[Interstellar](interstellar_tech.md)** 1 — LV for small sats (≤ 100 ㎏) R&D, MAIT, launch service
43. E––RD **[iQPS Inc.](iqps.md)** 40 — Earth sats for near real‑time SAR data
44. (–––R–) **[ISC](isc.md)** 13 — Space transportation
45. (–––R–) **[ispace / Hakuto](ispace.md)** 13 — Moon lander & base
46. (E––––) **[Iwaya Giken](iwaya_giken.md)** 7 — Develops & sells space video material, children edu
47. (–––C–) **[J-spacesystems](jspacesys.md)** 13 — R&D, HR, promotion activities for space business opportunities
48. (–L–R–) **[JAMSS](jamss.md)** 13 — Space experiments operations, austronaut training, sat utilization
49. (–––R–) **[JEPICO Corp.](jepico_corp.md)** 13 — Electronic components
50. (N–P––) **[JpGU](jpgu.md)** 13 — Japan geoscience academic union
51. (N–P––) **[JSASS](jsass.md)** 13 — Journals, conferences, awards, recommendations
52. (N–P––) **[JSGA](jsga.md)** 13 — Observations, protects Earth from space objects
53. (–––RD) **[JSI Corp](jsi_corp.md)** 13 — Sat remote sensing commercial services, imagery data
54. (–––R–) **[Kawasaki HVI](kawasaki_hvi.md)** 13 — Sats, fairings, rocket launch complex, space transport
55. (E––R–) **[Kobe Univ.](kobe_univ.md)** 28 — A leading Japanese national university
56. (–––R–) **[Kratos IS Japan](kratos.md)** 13 — Ground antennas, optimizing / managing satellites, signals
57. (–––R–) **[Kyocera](kyocera.md)** 26 — Ceramics
58. (E––R–) **[Kyutech](kyutech.md)** 40 — Scientific & technological institute
59. (–––R–) **[Lab Space Syst.](lab_of_space_systems.md)** 1 — Engine researches
60. (–––R–) **[Letara](letara.md)** 1 — Engines & propulsion systems for small sats
61. (–––R–) **[LSAS Tec](lsas_tec.md)** 13 — R&D analysis/simulation software
62. (–––R–) **[Magellan SJ](magellan_sj.md)** 28 — High precision sat positioning system
63. (–––R–) **[Maxar Japan](maxar.md)** 13 — SC platforms, robotics, servicing, geospatial info
64. (–––R–) **[Meisei](meisei.md)** 10 — Radiosondes, cameras, transceivers, MGMs, power, detectors, microsats
65. (–––R–) **[Meltin](meltin.md)** 13 — Robots, medical equip.
66. (––PC–) **[Minsora](minsora.md)** 44 — Space promotion
67. (–––R–) **[Mitsubishi El.](mitsubishi.md)** 13 — Near‑Earth sats, their components, ground segment
68. (–L–R–) **[Mitsubishi HVI](mitsubishi.md)** 13 — LV, rocket engines manuf. & testing, RCS, temp. sensors
69. (–L–R–) **[Mitsui Bussan AS](mitsui_bas.md)** 1 — Sat R&D, launch, deployment, operation
70. (E––R–) **[NAOJ](naoj.md)** 13 — Observations, astronomy researches, promotes joint research programs
71. (–––R–) **[NDK](ndk.md)** 13 — Outgas analysis sys., crystal devs (oscillator, filter, transducer)
72. (–––R–) **[NEC Space Tech.](nec.md)** 13 — Payload electrical & bus equip., antennas / spaceborne radar
73. (–––R–) **[NEC Space Sys.](nec.md)** 13 — Sats, ground systems
74. (–––RD) **[New Space Int.](nsi.md)** — 35 Data utilization
75. (N––R–) **[NGSL](ngsl.md)** 13 — Solving global issues utilizing Japanese space technology
76. (E––––) **[Nihon Univ.](nihon_univ.md)** 13 — Aerospace engineering
77. (–––R–) **[Orbspace](orbspace.md)** 8 — Space tourism using small reusable rocket
78. (–––R–) **[Outsense Inc.](outsense.md)** 13 — Space facilities & bases, space residence
79. (–––C–) **[PADECO](padeco.md)** 13 — Space consulting
80. (–––R–) **[Pale Blue Inc.](pale_blue_inc.md)** 12 — Propulsion for small sats, consulting of SC design & integration
81. (–––RD) **[PASCO CORP.](pasco_corp.md)** 13 — Geospatial info
82. (–L–R–) **[PD AeroSpace](pd_aerospace.md)** 23 — Suborbital spaceplane, space transportation, space tourism
83. (–L–R–) **[Polar Star Space](polar_star_space.md)** 13 — Launch service that uses small rockets
84. (–––RD) **[RESTEC Japan](restec.md)** 13 — Earth sat’s data receiving, processing, providing; R&D remote sensing
85. (E––R–) **[RIKEN](riken.md)** — 11 Biology, chemistry, physics, & research
86. (–––R–) **[Robotic Mining](rom.md)** 13 — R&D devices for space mineral exploration & mining
87. (–––R–) **[RyuTeC](ryutec.md)** 12 — Stratospheric solutions
88. (––––D) **[Sagri](sagri.md)** 28 — Space data utilization, machine learning, software
89. (E––C–) **[Sat. Biz Network](sb_net.md)** 13 — Technology consulting, space policy, strategy; space startups support
90. (–––R–) **[SE4](se4.md)** 13 — Remote robotics using VR
91. (–––R–) **[SEESE](seese.md)** 8 — Environmental testing (one stop service), space development services
92. (–––R–) **[SKY Perf. JSAT](sky_perfect_jsat.md)** 13 — Telecomms, multi‑channel pay TV company
93. (–––R–) **[Skygate tech.](skygate_tech.md)** 13 — Ground Station as a Service for sats
94. (–––R–) **[SNET](snet.md)** 13 — Earth sats comms, Earth observation, ground segment, drones
95. (–––RD) **[Solafune](solafune.md)** 47 — Sat data analysis, software development, related services
96. (–––R–) **[Sony](sony.md)** 13 — CubeSats
97. (––PC–) **[sorano me](soranome.md)** 13 — Space media, business consulting, HR
98. (N––R–) **[SPAC](spac.md)** 13 — Addition to GPS
99. (–––R–) **[Space Balloon](space_balloon.md)** 8 — Earth observation using high altitude balloons
100. (––PC–) **[Space Basil](space_basil.md)** 13 — Outer space advertising & entertainment
101. (EL–C–) **[Space BD](space_bd.md)** 13 — Space for business, sats launch, ISS experiments, education
102. (–L–––) **[SPACE COTAN](space_cotan.md)** 1 — Sales & PR for Hokkaido Spaceport
103. (–––R–) **[Space Cubics](spacecubics.md)** 1 — CPU, application software, FPGA, consult on space project management
104. (E–P––) **[Space Entertain.](space_e11t.md)** 13 — Space entertainment activities
105. (–––R–) **[Space Ent. Lab](space_entlab.md)** 13 — UAV, drones, high altitude (stratosphere) balloons, monitoring
106. (–––R–) **[Space NTK](space_ntk.md)** 13 — Space burial
107. (–L–R–) **[Space One](space_one_co.md)** 13 — Launch service using small LV. Small LV & dedicated launch site
108. (E–P––) **[Space Port Japan](spaceportjapan.md)** 13 — Promote the country’s aerospace & related industries
109. (–––R–) **[Space Quarters](space_quarters.md)** 13 — In-orbit assembly
110. (–––R–) **[Space Shift Inc.](space_shift.md)** 13 — Software for sat data processing, consulting (marketing, software)
111. (–L–R–) **[Space Walker](space_walker.md)** 13 — Suborbital spaceplane, space transport / tourism, small sats launch
112. (N–P––) **[Spacetide](spacetide.md)** 13 — Promotes newspace businesses worldwide, annual conference
113. (E––R–) **[SRL (Tohoku)](srl_tohoku.md)** 4 — R&D robotic space systems
114. (–––R–) **[STARS SS](stars_ss.md)** 22 — Debris removal & recycling
115. (–––R–) **[Sumitomo PP](sumitomo_pp.md)** 28 — Heat exchangers & control, semiconductor / MEMS equip., sensors
116. (–––R–) **[Synspective Inc.](synspective.md)** 13 — Utilize/integr. data from SAR sat const., big data, machine learning
117. (–––R–) **[Tamagawa Seiki](tamagawa_seiki.md)** 20 — Servo units, motor controllers, robots, IMU, autom. control units
118. (–––R–) **[Tenchijin](tenchijin.md)** — Business solution using Earth observation data. Startup for big data
119. (–––R–) **[Terra Space](terra_space.md)** 26 — CubeSats
120. (N––––) **[TNLab](tnlab.md)** 13 — Student association for space achitectures
121. (E––––) **[Tohoku SC](tohoku_sc.md)** 4 — Students community
122. (E––R–) **[Tokyo Univ.](tokyo_univ.md)** 13 — Tokyo University
123. (–––R–) **[TOWING](towing.md)** 23 — Space food, soil & cultivation
124. (–––R–) **[Umitron](umitron.md)** 13 — Aquafarmers support: IoT, sat remote sensing, machine learning
125. (E––––) **[TUPLS](tupls.md)** 13 — Tokyo University of Pharmacy & Life Sciences
126. (–––R–) **[Warpspace Inc.](warpspace.md)** 8 — Comms infrastructure for LEO sats; cubesats modules
127. (E––C–) **[Yspace LLC](yspace.md)** 8 — VR

**Not space:** Ashirase・ Avatarin・ Dynamic Map Platform・ [Euglena](euglena.md)・ Global Positioning Augmentation Service・ Integriculture・ ONETABLE・ Panasonic Avionics Corp.・ Ridge-i・ Sakura Internet・ SIGNATE・ Space Bio-Laboratories・ sustainacraft・ [Telexistence Inc.](telexistence.md)

**Org. lists:** <https://aerospacebiz.jaxa.jp/en/spacecompany>・ <https://aerospacebiz.jaxa.jp/en/partner>・ <https://www.j-startup.go.jp/en>・ <https://www.spacebizguide.com/country-japan>・ <https://en.wikipedia.org/wiki/Japanese_space_program>

TBD

1. Houkiboshi
2. New Space Intelligence
3. [Our stars](our_stars.md)
4. Satellite Data Service
5. Space Art and design (2018)
6. https://spacedata.jp/
7. https://orbitallasers.com

[![](f/c/map/japan_politicalt.webp)](f/c/map/japan_political.webp)



## Middle East



### Saudi Arabia

(**E**)du / (**N**)on‑profit・(**L**)aunch serv.・(**P**)romote・(**R**)&D&MAIT / (**C**)onsult.・(**D**)ata provider

1. (–––P–) **【[SSC](saudi_sc.md)】** — Saudi Space Agency
2. (–––RD) **[Taqnia Space](taqnia_space.md)** — Geospatial solutions



### (TBD) UAE

(**E**)du / (**N**)on‑profit・(**L**)aunch serv.・(**P**)romote・(**R**)&D&MAIT / (**C**)onsult.・(**D**)ata provider

1. (–––––) **【[MBRSC](mbrsc.md)】** — Разработка и создание КА
2. (–––––) **【[UAESA](uaesa.md)】** — Космическое агентство Объединённых Арабских Эмиратов
3. EDGE
4. FADA <https://fadaspace.com/>
5. (–––––) **StarLab Oasis LTD**
6. Technology Innovation Institute
7. Yahsat

[![](f/c/map/uae_politicalt.webp)](f/c/map/uae_political.webp)



## (TBD) Russia
**Космическая отрасль России** — это около 100 предприятий, в которых занято 250 000 человек. Большинство предприятий российской космической отрасли являются потомками советской государственной космической индустрии, занимавшейся разработкой и производством КА.

Крупнейшим предприятием является РКК 「Энергия」, главный подрядчик пилотируемых космических полётов. Ведущими производителями ракет‑носителей являются ГКНПЦ им. М.В. Хруничева и ЦСКБ‑Прогресс. Крупнейшим разработчиком спутников являются 「Информационные спутниковые системы」 им. академика М.Ф. Решетнёва, а лидером в области межпланетных КА является Научно‑производственное объединение им. С.А. Лавочкина.

См. также: [Научно‑производственное объединение](spo.md)

(**E**)du / (**N**)on‑profit・(**L**)aunch serv.・(**P**)romote・(**R**)&D&MAIT / (**C**)onsult.・(**D**)ata provider

1. (–––––) **【[4116 ВП МО РФ](milro.md)】** —  Контролирующие работы на LAV
2. (–––––) **【[ДОГОЗ](dogoz.md)】** — Размещение гособоронзаказа, контроль, учёт выполнения
3. (–––––) **【[ИКИ РАН](iki_ras.md)】** — Исследование космоса, НА, ННК, научные задачи (НРДК)
4. (–––––) **【[Главкосмос](glavkosmos.md)】** — Внешне‑экономическая деятельность РКТ, НИОКР
5. (–––––) **【[Роскосмос](roskosmos.md)】** — Федеральный орган исполн. власти по косм. деятельности
6. (–––––) **【[ЦНИИмаш](tsniimash.md)】** — [ГНИО](hrorsi.md)** —. Проектирование, экспер. и исследования КТ
7. (–––––) **[AvantSpace](avantspace.md)** — Малые спутники, ионные двигатели, радиопередатчики
8. (–––––) **[АВЭКС](aveks.md)** — Проектирование и изготовление систем электроснабжения (МКТН)
9. (–––––) **[Агат](agat.md)** — Технико‑экономическая экспертиза
10. (–––––) **[Аксион‑холдинг](axion_h.md)** — Приборостроение
11. (–––––) **[АКЦ ФИАН](asc_fian.md)** — Астроном. системы изучения космоса в различных спектрах
12. (–––––) **[Андроидная техника](андр_техника.md)** — Робототехника
13. (–––––) **[Астрон Электроника](astron_e.md)** — Разработка и производство аппаратуры для КА
14. (E––––) **[ВКА Можайского](vka_m.md)** — …
15. (–––––) **[ВНИИЭМ](vniiem.md)** — Метеоспутники, ДЗЗ, автоматика для АЭС, электротехника (ТАИК)
16. (–––––) **[ГАИШ МГУ](sai_msu.md)** — …
17. (–––––) **[Геофизика‑Космос](geofizika_s.md)** — Автоматизированные системы управления
18. (–––––) **[ГЕОХИ РАН](geokhi_ras.md)** — …
19. (–––––) **[ГКНПЦ Хруничева](khrunichev.md)** — Разработка и создание LV
20. (–––––) **[ИНАСАН](inasan.md)** — …
21. (–––––) **[ИПМ Келдыша](keldysh_ipm.md)** — Исследования математики, механики, кибернетики, информатики
22. (–––––) **[ИРЗ](irz.md)** — Группа приборостроительных компаний (ЦВИЯ)
23. (–––––) **[ИРЭ РАН](ire_ras.md)** — …
24. (–––––) **[ИСС](iss_r.md)** — КА связи, телевещания, ретрансляции, навигации, геодезии
25. (–––––) **[ИЦ Келдыша](keldysh_its.md)** — R&D, производство техники в области РКТ
26. (–––––) **[КБХМ](kbhm.md)** — Разработка и создание двигателей
27. (–––––) **[Композит](kompozit_mv.md)** — ГНИО РФ в области материаловедения
28. (–––––) **[Концерн Вега](vega_k_1_2.md)** — Разработка и создание приборов радиолокации
29. (–––––) **[КосмоКурс](космокурс.md)** — Суборбитальный космический полёт
30. (–––––) **[Лин Индастриал](spacelin.md)** — …
31. (–––––) **[МАИ](mai.md)** — …
32. (–––––) **[МГТУ Баумана](мгтубаумана.md)** — …
33. (–––––) **[МГУ](msu.md)** — …
34. (–––––) **[МНИИРИП](mniirip.md)** — Исследования в области ЭКБ
35. (–––––) **[МОКБ Марс](mars_mokb.md)** — Бортовые системы автоматического управления и навигации КА (СИЯМ)
36. (–––––) **[МФТИ](mipt.md)** — …
37. (–––––) **[НГУ](nsu.md)** — … (ЮМП)
38. (–––––) **[НИИ КП](niikp.md)** — Космическое приборостроение
39. (–––––) **[НИИ Гуськова](niimp.md)** — Микроэлектронная аппаратура (ЭКПВЕ)
40. (–––––) **[НИИ МТ](mt_nc.md)** — … (КДИЖ)
41. (–––––) **[НИИ ПМЭ](niipme.md)** — …
42. (–––––) **[НИИ ТП](niitp.md)** — …
43. (–––––) **[НИИКП](niicom.md)** — Разработка и создание ДМ, управляющих приводов
44. (–––––) **[НИИМАШ](niimash.md)** — Создание и производство ДУ
45. (–––––) **[НИИФИ](niifi.md)** — Датчики, преобразователи, с‑мы контроля, управления (СДАИ)
46. (–––––) **[НИИЯФ МГУ](ниияф_мгу.md)** — …
47. (–––––) **[НИЦ Планета](planeta_src.md)** — Гидрометеорология
48. (–––––) **[НИЦ РКП](nic_rkp.md)** — Отработка ДУ и КА
49. (–––––) **[НПК СПП](npk_spp.md)** — Системы прецизионного приборостроения
50. (–––––) **[НПО ИТ](npoit.md)** — …
51. (–––––) **[НПО Лавочкина](lav.md)** — …
52. (–––––) **[НПП Антарес](npp_antares.md)** — Автоматизированные системы управления
53. (–––––) **[НПП Саит](sait_ltd.md)** — Электросвязь (СНГА)
54. (–––––) **[НПЦ Полюс](polus_tomsk.md)** — Бортовая / наземная электротехника, точная механика (ЕИЖА)
55. (–––––) **[НПЦАП](npcap.md)** — Разработка и создание гироскопов и ДМ
56. (–––––) **[НТЦ Модуль](ntc_module.md)** — Обработка изображений, вычислительные комплексы
57. (–––––) **[НСТР](нстр.md)** — …
58. (–––––) **[ОКБ 5](okb5.md)** — Блоки управления и схемы (МКТУ)
59. (–––––) **[ОКБ МЭИ](okbmei.md)** — Радиотехнические системы / комплексы РКТ, испытания ракет / КА (ЯГАИ)
60. (–––––) **[ОКБ Факел](edb_fakel.md)** — Создание и производство эл. двигателей, ДУ
61. (–––––) **[Орион‑ХИТ](орион‑хит.md)** — ХИТ
62. (–––––) **[РАН](ран.md)** — …
63. (–––––) **[РАДИС](radis.md)** — Оборудование для беспроводной связи
64. (–––––) **[РИРВ](рирв.md)** — …
65. (–––––) **[РКК Энергия](ркк_энергия.md)** — …
66. (–––––) **[РКС](rss.md)** — Космические информационные системы (ИВЯФ)
67. (–––––) **[РКЦ Процесс](progress_rsc.md)** — Разработка, производство и эксплуатация РН среднего класса
68. (–––––) **[РНЦ ПХ](rsc_ac.md)** — Химические вещества, композиционные / полимерные материалы
69. (–––––) **[Росгидромет](roshydromet.md)** — Гидрометеорология, мониторинг природной среды, гос. надзор
70. (–––––) **[РФЯЦ-ВНИИЭФ](vniief.md)** — …
71. (–––––) **[ПАО Сатурн](пао_сатурн.md)** — Солнечные и аккумуляторные батареи (ЖЦПИ)
72. (–––––) **[Сафит](safit.md)** — Трубы, баллоны
73. (–––––) **[СКБ](sdb_ire_ras.md)** — … (ТИДН)
74. (–––––) **[СКБ КП](skbkp.md)** — OE для фундаментальных космических исследований
75. (–––––) **[Совзонд](sovzond.md)** — Изображения Земли, аналитика
76. (–––––) **[Спутникс](sputnix.md)** — …
77. (–––––) **[Техномаш](tehnomash.md)** — Метрологическая аттестация, сертификация, аудит
78. (–––––) **[ФАНО](фано.md)** — Регулирование, услуги науки, образ., здравоохр., агропрома
79. (–––––) **[ФИАН](fian.md)** — Физический институт
80. (–––––) **[ЦКБ ИУС](ckbius.md)** — Информационно‑управляющие системы, радиоэл. БА (АИТЛ)
81. (–––––) **[ЦНИИХМ](tsniihm.md)** — …
82. (–––––) **[ЦЭНКИ](tsenki.md)** — Пусковые услуги
83. (–––––) **[Энергомаш](энергомаш.md)** — Разработка и создание ракетных двигателей



## Singapore

(**E**)du / (**N**)on‑profit・(**L**)aunch serv.・(**P**)romote・(**R**)&D&MAIT / (**C**)onsult.・(**D**)ata provider

1. (–––R–) **[Astroscale](astroscale.md)** — Space debris removal, sat life extension services, EoL services
2. (–––R–) **[Infinite Orbits](infinite_orbits.md)** — In‑orbit servicing, servicing platforms
3. (E––R–) **[NTU](ntu.md)** — Higher education, small sats dev
4. (E––R–) **[NUS](nus.md)** — Higher education, small sats dev
5. (––P––) **[OSTIn](ostin.md)** — Singapore space program
6. (––P––) **[SSTL](sstl.md)** — Space promotion, business connections
7. (–––R–) **[STAR@NUS](nus_star.md)** — Small sats dev
8. (–––R–) **[Transcelestial](transcelestial.md)** — Telecoms

**Org. lists:** <https://www.spacebizguide.com/country-singapore>・ <https://www.edb.gov.sg/en/our-industries/aerospace.html>



## South Korea

(**E**)du / (**N**)on‑profit・(**L**)aunch serv.・(**P**)romote・(**R**)&D&MAIT / (**C**)onsult.・(**D**)ata provider

1. (ELPR–) **【[KARI](kari.md)】** 6 — Korea Aerospace Research Institute
2. (–––R–) **[APSI](apsi.md)** 1 — CubeSats R&D, EPS, space electronics, testing
3. (–––R–) **[CONTEC](contec.md)** 6 — GS Services, Sat image processing & application services
4. (–––R–) **[Hanwha](hanwha.md)** 15 — Engines
5. (–L–R–) **[INNOSPACE](innospace.md)** 17 — LV development, PS test & evaluation, engineering services
6. (–L–R–) **[KAI](kai.md)** 15 — R&D sats & LV
7. (E––––) **[KAIST](kaist.md)** 6 — Higher education
8. (E––R–) **[KASI](kasi.md)** 6 — Research institute in astronomy & space science
9. (–––R–) **[KT SAT](ktsat.md)** 1 — Telecomms
10. (–––R–) **[NARA Space Tech.](nara_st.md)** 2 — CubeSat & components R&D, Testing, Consulting, Training
11. (–L–R–) **[Perigee Aerospace](perigee_as.md)** 6 — Small / medium orbital & sub‑orbital LV
12. (–––R–) **[Satrec Initiative](satreci.md)** 6 — Earth observ., sats & their components, ground systems, analytics
13. (–––RD) **[SIIS](siis.md)** 6 — Sat imagery provider
14. (–––RD) **[SOLETOP](soletop.md)** 6 — CubeSat R&D, geospatial info, unmanned system, instruments, software
15. (–––RD) **[TelePIX](telepix.md)** 1 — Sat images, data analytics, ground station operations
16. (–––R–) **[UNASTELLA](unastella.md)** 1 — Small LV for space tourism

[![](f/c/map/korea_south_politicalt.webp)](f/c/map/korea_south_political.webp)



## (TBD) USA

(**E**)du / (**N**)on‑profit・(**L**)aunch serv.・(**P**)romote・(**R**)&D&MAIT / (**C**)onsult.・(**D**)ata provider

1. (ELPR–) **【[NASA](nasa.md)】** — Нац. управление США по аэронавтике и исследованию КП
2. (–––––) **[Advanced Space](advanced_space.md)** — Mission sheduling, research, design, analysis, support
3. (–––––) **[Aerojet Rocketdyne](aerojet_rocketdyne.md)** — Разработка и эксплуатация двигателей и ДУ
4. (–––––) **[AI SpaceFactory](ai_spacefactory.md)** — Extraplanetary habitats & systems
5. (–––––) **[Ames R/C](arc.md)** — …
6. (–––––) **[AGI](agi.md)** — Analytical Graphics Inc.
7. (–––––) **[Apache Obs.](apache_obs.md)** — …
8. (–––––) **[Arizona Univ.](arizona_univ.md)** — …
9. (–––––) **[Astrobotic](astrobotic.md)** — Разработка и эксплуатация лунных грузовых КА
10. (–––––) **[Ball A&T](ball_at.md)** — Разработка, создание КА и их СЧ
11. (–––––) **[Berkeley Univ.](berkeley_univ.md)** — …
12. (–––––) **[Blue Origin](blue_origin.md)** — Разработка и эксплутация LV, ПКА, двигателей; пусковые услуги
13. (–––––) **[Boeing](boeing.md)** — Разработка, эксплуатация космической техники, ПКА
14. (–––––) **[Brown Univ.](brown_univ.md)** — …
15. (–––––) **[Bryce S&T](bryce_st.md)** — Systems engineering, data analytics, planning & management
16. (–––––) **[Buffalo Univ.](buffalo_univ.md)** — …
17. (–––––) **[Capella Space](capella_space.md)** — Sub 0.5 m SAR Earth observation via a small sats constellation
18. (–––––) **[CPP](cpp.md)** — California State Polytechnic University, Pomona
19. (–––––) **[Chicago Univ.](chicago_univ.md)** — …
20. (–––––) **[Collins A/S](collins_aerospace.md)** — …
21. (–––––) **[Cornell Univ.](cornell_univ.md)** — …
22. (–––R–) **[DDC](ddc.md)** — CPU, memory modules, microelectronics, radiation shielding
23. (–––R–) **[Deep Space Ecology](deep_space_ecology.md)** — Closed ecological systems, space farming
24. (–––––) **[Draper lab](draper_lab.md)** — GNC, space systems, technology solutions
25. (–––––) **[Dynetics](dynetics.md)** — …
26. (–––R–) **[E-Space](e_space.md)** — Telecom sats
27. (–––––) **[Elysium Space, Inc.](elysium_space.md)** — Celestial memorials
28. (–––C–) **[Euroconsult](euroconsult.md)** — Strategic, research, summits, training programs
29. (–L–––) **[Exolaunch](exolaunch.md)** — Launch booking
29. (–––––) **[EXOS AS&T](exos_ast.md)** — Developer of reusable LV
30. (–––––) **[FAA](faa.md)** — …
31. (–––––) **[Firefly A/S](firefly_as.md)** — Разработка и эксплутация LV; пусковые услуги
32. (N––––) **[For All Moonkind](for_all_moonkind.md)** — Saving lunar & aouter space memorials
33. (–––––) **[Frontier A/S](frontier_as.md)** — (TBD) Rocket propulsion
34. (–––––) **[GFSC](gfsc.md)** — Goddard Space Flight Center
35. (–––––) **[GISS](giss.md)** — …
36. (–––R–) **[GITAI](gitai.md)** — Remotely controlled robots
37. (–––––) **[Global Aerosp. Corp.](gac.md)** — R&D. Engineering work, assembly, integration, & testing
38. (–––––) **[GRC](grc.md)** — Научные исследования, разработка аэрокосмических технологий
39. (–––––) **[Hampton Univ.](hampton_univ.md)** — …
40. (–––––) **[Honeywell A/S](honeywell_as.md)** — …
41. (N––R–) **[Icarus Interstellar](icarus_interstellar.md)** — R&D tech., assist the development of interstellar flight
42. (–––––) **[ImageSat Int.](imagesat_int.md)** — Geospatial solutions & services: imagery, data, analytics
43. (–––––) **[Int. Moonbase All.](i18n_moonbase_alc.md)** — Advancing the dev. / implementation of base on the Moon
44. (–––––) **[Intuitive Machines](intt_machines.md)** — Automatics, drones, SC & suit model/sim services
45. (–––C–) **[Jacobs](jacobs.md)** — Civil engineering & solutions
46. (–––––) **[J.Hopkins Univ.](jhu.md)** — …
47. (–––––) **[Jena‑Optronik](jenaoptronik.md)** — …
48. (–––––) **[JHUAPL](jhuapl.md)** — Applied Physics Laboratory
49. (–––––) **[JPL](jpl.md)** — Создание и обслуживание беспилотных КА для NASA
50. (N–P––) **[Karman Project](karman_project.md)** — Connecting people
51. (–––––) **[Keck Inst. SS](keck_inst_ss.md)** — Policy, research & advocacy for space mission concepts & technology
52. (–––––) **[Kepler Space Institute](kepler_spin.md)** — Education, research, publications in space exploration, commerc., colonization
53. (–––––) **[KinetX A/S](kinetx_as.md)** — Deep space missions’ nav.services, R&D of SC & telecom systems
54. (–––R–) **[Kuiper Systems](kuiper_systems.md)** — LEO constellation
55. (–––––) **[LASP](lasp.md)** — Laboratory for Atmospheric & Space Physics
56. (–––––) **[Lockheed Martin](lockheed_martin.md)** — Разработка и эксплутация LV, КА, ПКА; пусковые услуги
57. (–––––) **[Loft Orbital](loft_orb.md)** — End‑to‑end service provider, sats
58. (–––––) **[LPI](lpi.md)** — Lunar & Planetary Institute
59. (–––––) **[Lunar Station Corp](lunar_station_corp.md)** — Consult. prod./services for navigating, prospecting, surveying Moon
60. (–––R–) **[Made In Space](made_in_space.md)** — Space settlement, technology investigations, off‑Earth manufacturing
61. (E–P––) **[Mars Generation](mars_generation.md)** — Space education for youth, STEM
62. (E–P––) **[Mars Society](mars_society.md)** — Establishing a permanent human presence on Mars
63. (–––R–) **[Maverick SS](maverick_ss.md)** — Sat deployment hardware, integration & launch brokering services
64. (–––––) **[Maxar Tech.](maxar.md)** — Спутники, механизмы
65. (E––––) **[Michigan Univ.](michigan_univ.md)** — …
66. (N––––) **[MILO SSI](milo_ssi.md)** — Education, technology development, cost optimization
67. (–––––) **[Moog inc](moog_inc.md)** — Авиа‑ и космические системы управления, ракетные двигатели
68. (–––––) **[Moon Express](moon_express.md)** — Разработка и эксплуатация лунных грузовых КА
69. (–––––) **[Mount Holyoke Coll.](mtholyoke_college.md)** — …
70. (–L–R–) **[Nanoracks](nanoracks.md)** — …
71. (N––––) **[National Space Soc.](nspace_society.md)** — Creation of a spacefaring civilization
72. (–––––) **[NCSU](ncsu.md)** — NC State University
73. (–––––) **[Northrop Grumman](northrop_grumman.md)** — Разраб. / эксплуат. КА, лазерных систем, микроэлектроники
74. (–––––) **[Open Univ.](open_univ.md)** — The Open University (OU)
75. (–––––) **[Orbital ATK](orbital_atk.md)** — Разработка и эксплуатация РН, КА
76. (–––––) **[Planet](planet.md)** — To image all of Earth’s landmass every day
77. (N––––) **[Planetary Society](planetary_society.md)** — Астрономия, планетология, иссл. космоса, популяризации науки
78. (–––R–) **[Pratt & Whitney](pratt_n_whitney.md)** — Rocket engines
79. (E––––) **[Princeton Univ.](princeton_univ.md)** — …
80. (–––––) **[PSI](psi.md)** — Planetary Science Institute
81. (–L–R–) **[Rocket Lab](rocket_lab.md)** — Разработка и эксплутация LV; пусковые услуги
82. (––PR–) **[Ryman Sat](ryman_sat.md)** — Space exploration, cubesats by ordinary people
83. (–––––) **[Apache Obs.](apache_obs.md)** — …
84. (–––R–) **[Spaceship Company](spaceship_co.md)** — Aerospace‑system manufacturing
85. (–––––) **[SpaceQuest](spacequest.md)** — Кубсаты и их СЧ
86. (–––––) **[SpaceX](spacex.md)** — Разработка и эксплутация LV, ПКА, двигателей; пусковые услуги
87. (N–P––) **[Space Foundation](space_foundation.md)** — Inspire, educate, connect & advocate on behalf of the space community
88. (–––––) **[Spire](spire.md)** — Space‑to‑cloud analyt., sats const., maritime, avia, weather tracking
89. (–––––) **[SWRI](swri.md)** — Southwest Research Institute
90. (N––––) **[Tau Zero](tau_zero.md)** — Pioneering advancements toward interstellar flight
91. (–––––) **[TransDigm](transdigm.md)** — Космическая электроника и механизмы
92. (–––––) **[ULA](ula.md)** — Эксплутация LV; пусковые услуги
93. (–––R–) **[Ursa Major Tech.](ursa_major_tech.md)** — Rocket engines
94. (–––R–) **[Utilis Corp.](utilis_corp.md)** — Earth observation, water leaks detection
95. (–––––) **[Venturi Astrolab](venturi_astrolab.md)** — …
96. (–––––) **[Wisconsin Univ.](wisconsin_univ.md)** — …
97. (–––––) **[WUSTL](wustl.md)** — Washington University in St. Louis
98. (–––––) **[XISP-Inc](xisp_inc.md)** — Mission / product R&D, sales, support
99. (–––––) **[Xplore](xplore.md)** — SC

[![](f/c/map/usa_politicalt.webp)](f/c/map/usa_political.webp)



## (TBD±) Vietnam

(**E**)du / (**N**)on‑profit・(**L**)aunch serv.・(**P**)romote・(**R**)&D&MAIT / (**C**)onsult.・(**D**)ata provider

1. (–––––) **【[VNSC](vnsc.md)】** — Vietnam Space Agency



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[](.md)】**<br> <mark>NOCAT</mark>|

1. Docs: …
2. <http://mentallandscape.com/V_Biographies.htm>
3. <https://en.wikipedia.org/wiki/List_of_government_space_agencies>
4. <https://platform.space-marketplace.com/>
5. <https://spaceindividuals.com/space-companies>
6. <https://www.nanosats.eu/companies>


## The End

end of file

