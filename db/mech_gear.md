# Mechanical gears
> 2022.09.30 [🚀](../../index/index.md) [despace](index.md) → [SC](sc.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Mechanical gears** — EN term. **** — RU analogue.</small>

## Named gears

| | |
|:--|:--|
|**Manual transmission**<br> ![](f/gear/gear_manual_transmission.gif)|**Round gear ruler**<br> ![](f/gear/gear_round_gear_ruller.webp)|
|**Train gear**<br> ![](f/gear/gear_train.webp)|**Ackerman steering system**<br> ![](f/gear/gear_ackerman_steering_system.gif)|
|**Bevel gear**<br> ![](f/gear/gear_bevel_gear.webp)|**Dounble wishbone suspension**<br> ![](f/gear/gear_double_wishbone_suspension.webp)|
|**Worm gear**<br> ![](f/gear/gear_worm_gear.webp)|****<br> ![](f/gear/.webp)|
|****<br> ![](f/gear/.webp)|****<br> ![](f/gear/.webp)|
|****<br> ![](f/gear/.webp)|****<br> ![](f/gear/.webp)|
|****<br> ![](f/gear/.webp)|****<br> ![](f/gear/.webp)|



## Unnamed gears

| | |
|:--|:--|
|![](f/gear/gear_01.webp)|![](f/gear/gear_02.webp)|
|![](f/gear/gear_03.webp)|![](f/gear/gear_04.webp)|
|![](f/gear/gear_05.webp)|![](f/gear/gear_06.webp)|
|![](f/gear/gear_07.webp)|![](f/gear/gear_08.webp)|
|![](f/gear/gear_09.webp)|![](f/gear/gear_10.webp)|
|![](f/gear/gear_11.webp)|![](f/gear/gear_12.webp)|
|![](f/gear/gear_13.webp)|![](f/gear/gear_14.webp)|
|![](f/gear/gear_15.gif)|![](f/gear/gear_16.webp)|
|![](f/gear/gear_17.webp)|![](f/gear/gear_18.webp)|
|![](f/gear/gear_19.webp)|![](f/gear/gear_20.webp)|
|![](f/gear/gear_21.webp)|![](f/gear/gear_22.webp)|
|![](f/gear/gear_23.webp)|![](f/gear/gear_24.webp)|
|![](f/gear/gear_25.webp)|![](f/gear/gear_26.webp)|
|![](f/gear/gear_27.webp)|![](f/gear/gear_28.webp)|
|![](f/gear/gear_29.webp)|![](f/gear/gear_30.webp)|
|![](f/gear/gear_31.webp)|![](f/gear/gear_32.webp)|

## The End

end of file
