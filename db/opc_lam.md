# OPC LAM
> 2019.08.05 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/o/opc_lam_logo1t.webp)](f/c/o/opc_lam_logo1.webp)|<susana.cortes-borgmeyer@ariane.group>, <mark>nophone</mark>, Fax …;<br> *ArianeGroup GmbH, Robert-Koch-Str. 1, 82024 Taufkirchen, Germany*<br> <http://www.space-propulsion.com> ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/Orbital_Propulsion_Centre)|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Orbital Propulsion Centre Lampoldshausen (LAM)** — европейский производитель двигателей и двигательных установок космических аппаратов. Является подразделением ArianeGroup GmbH. Founded in 1963 by Ludwig Bölkow as a branch of the Bölkow‑Entwicklungs‑KG, the centre is situated within the German Aerospace Center site at Lampoldshausen, Germany. Нынешняя продукция:

1. Двигатель [RIT 10](engine_lst.md)
1. Двигатель [RIT 2X](engine_lst.md)
1. Двигатель [RIT µX](engine_lst.md)
1. Двигатель [S400](engine_lst.md)

[Products brochures ❐](f/c/o/opc_lam_brochures.7z)



## The End

end of file
