# Unit
> 2019.05.10 [🚀](../../index/index.md) [despace](index.md) → **[OE](sc.md)**, [SGM](sc.md), [Проекты](project.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---
> <small>**Unit** — EN term. **Изделие** — RU analogue.</small>

**Unit** is a general name for any part of spacecraft & ground segment (e.g. propulsion system, sensor, wire, CPU).

In common any unit (SC, system, unit, etc.) can be briefly described using the table below.

【**Table.** A brief description of a unit】

|**Characteristics**|**Value**|
|:--|:--|
|Composition| 【single unit, wires, a number of units, pressurization level, etc.】 |
|Consumption, W| 【typical, basic timeline, etc.】 |
|Dimensions, ㎜| 【general dimensions, not volume】 |
|Interfaces| 【connectors, mounting points, orientation】 |
|Lifetime/Resource, h(y)| 【lifetime for total lifetime, resource for active state】 |
|Mass, ㎏| 【mass of the unit(s) in space】 |
|Overload, Grms| 【acceptable loads & their direction】 |
|Performance| 【timelined performance】 |
|Rad.resist, ㏉(㎭)| 【total ionising dose, heavy particles】 |
|Reliability| 【a calculated/proven reliability】 |
|Thermal range, ℃| 【for an active condition & for transportation】 |
|Timeline|【basic operations timeline】|
|TRL| 【current TRL, plans】 |
|Voltage, V| 【nominal, accept. ranges for a constant work & transition periods】 |
|**【Specific】**|~ ~ ~ ~ ~ |
|Specific req. #1| |
|Specific req. #2| |
|…| |
| | 【photo, render, scheme, sketch, etc.】 |



## RU notes

**Изделие** в широком понимании ─ общее наименование [КК](sc.md), [КА](sc.md), их [СЧ](sui.md), да и вообще всего, что касается ракетной и военной техники.

В Российском космическом секторе термин Изделие применяется для унификации плодходов в разработке гражданских и секретных НИОКР. Общеупотребительные термины шифруются цифро‑буквенными кодами, к которым составляются перечни и классификаторы, без наличия которых понять принадлежность того или иного документа к НИОКР и её СЧ проблематично (а при их наличии — просто долго).

Варианты изделий:

1. **Изделие комплекса** ─ составная часть [комплекса](sc.md), система, аппаратура, агрегат, прибор, блок, узел, [ЭРИ](elc.md), комплектующее изделие (включая КИМП), программное изделие (продукт), бортинструмент, АСУ всех видов, входящее в состав комплекса или любой его СЧ.
1. **Комплектующее изделие** ─ изделие организации‑поставщика (система, аппаратура, прибор, блок, узел, деталь, ЭРИ, комплектующий элемент), применяемое как СЧ изделия, выпускаемого организацией‑изготовителем (ГОСТ 3.1109). В тексте [РК‑11‑КТ](const_rk.md) ─ изделие, поставляемое одной организацией другой организации.
1. **Покупное изделие** ─ к покупным изделиям относят изделия, не изготовляемые на данном предприятии, а получаемые им в готовом виде, кроме получаемых в порядке кооперирования. К изделиям, получаемым в порядке кооперирования, относят СЧ разрабатываемого изделия, изготовляемые на другом предприятии по [конструкторской документации](doc.md), входящей в комплект документов разрабатываемого изделия.
1. **Штатное изделие** — см. [штатный образец](flight_unit.md).



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**`Бортовая аппаратура (БА):`**<br> [PDD](pdd.md) ~~ [Антенна](antenna.md) ~~ [АПС](hns.md) ~~ [БУ](eas.md) ~~ [ЗУ](ds.md) ~~ [Изделие](unit.md) ~~ [КЛЧ](clean_lvl.md) ~~ [ПЗР](fov.md) ~~ [ПО](soft.md) ~~ [Прототип](prototype.md) ~~ [Радиосвязь](comms.md) ~~ [СКЭ](elmsys.md) ~~ [ССИТД](tsdcs.md) ~~ [СИТ](etedp.md) ~~ [УГТ](trl.md) ~~ [ЭКБ](elc.md) ~~ [EMC](emc.md)|
|**【[Structures, gears, materials (SGM)](sc.md)】**<br> [Гермоконтейнер](гермоконтейнер.md) ~~ [Датчик](sensor.md) ~~ [Задел](margin.md) ~~ [Изделие](unit.md) ~~ [Испарение материалов](matc.md) ~~ [Кавитация](cavitation.md) ~~ [КЗУ](cinu.md) (ВБУ КТ) ~~ [КХГ](cgs.md) ~~ [Контейнеры для транспортировки](ship_contain.md) ~~ [Крейцкопф](crosshead.md) ~~ [Номинал](nominal.md) ~~ [ПУС](lag.md) ~~ [ПНА, ПОНА, ПСНА](devd.md) ~~ [Резерв](reserve.md) ~~ [Слайс](слайс.md) ~~ [ТСП](tsp.md) ~~ [Типичные формы КА](sc.md) ~~ [Толкатель](толкатель.md) ~~ [Унификация](commonality.md)|

1. Docs:
   1. [РК‑11](const_rk.md), стр. 15, 16.
   1. [ГОСТ 2.124](гост_2_124.md).
1. <…>


## The End

end of file
