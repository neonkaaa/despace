# Polar Star Space
> 2020.11.19 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/p/polar_star_space_logo1t.webp)](f/c/p/polar_star_space_logo1.webp)|<pss_info@polarstarspace.com>, +81(3)5542-1272, Fax +81(3)5542-1276;<br> *104-0031, Kyobashi 1-5-12 Maruhiro_Kyobashi_building 4th-floor, Chuo-ku, Tokyo*<br> <http://polarstarspace.com>|
|:--|:--|
|**Business**|Launch service that uses small rockets|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO — Takahiro Nakamura|

**Polar Star Space CO., Ltd** is a venture company started by Hokkaido University that utilizes the technologies of Hokkaido University, Tohoku University and Shiga University of Medical Science as a base, in order to provide data-driven problem-solving services in the fields of agriculture, fishery, resources and energy, disaster prevention, etc. Founded 2017.09.

- Business: Using microsatellite, drone, ground measuring equipment, etc. Solution proposal. Acquired by microsatellite, drone, ground measuring equipment, etc.
- Data business. Design and manufacture of microsatellite and related components. Launch arrangement and operation support / contract for microsatellite.
- With a seamless observation data solution from space to the ground we will solve various problems on the ground and contribute to affluent living.

**Tokyo branch**  
2-5-8 Nishikanda, Chiyoda-ku, Tokyo 101-0065  
Kyowa 15th Building 8th Floor  
tel: 03-6261-6045  
fax: 03-5210-8011

In the $200 billion global space industry market, commercial use of remote sensing is a promising area that is expected to grow in the future. The realization of global measurements using hundreds of ultra-small satellites by the US satellite company Planet, means that a door for new satellite competition has been opened as opposed to the heavy and large, traditional satellite competition. We are aiming for multiple operations of ultra-small satellites equipped with super multi-color cameras as a unique strategy to realize an "on-demand" and "flexible" satellite observation, as well as a 「problem-solving type" required for commercial use of satellites. .

We make use of our proprietary measurement method with our proprietary smartphone spectroscope and drone-mounted multi-spectral camera, and construct a highly accurate bidirectional reflectance distribution function
(BRDF) spectral library for various crops at every azimuth, angle, and solar altitude in every season. From the spectrum of the target measured based on this knowledge, we will provide value in the field of agriculture incl. but not limited to the discovery of lesions, and optimization of fertilization and pesticide use. In addition to this, we aim to achieve the realization of high-precision remote sensing in a wide area in which micro satellites play an active role, such as with harvest prediction, natural disaster prediction and resource mapping in the large-scale agriculture industry that is closely related to population explosion and global environmental problems.

Implementing a high-precision BRDF spectrum library can be a source of powerful competitive advantage for understanding the state of any target.
The mission of Polar Star Space is to realize high-precision remote sensing ahead of the rest of the world, and contribute to solving global issues.



## The End

end of file
