# VEXAG, 17th
> 2019.05.21 [🚀](../../index/index.md) [despace](index.md) → [VEXAG](vexag.md), [Venus](venus.md), **[Events](event.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Venus Exploration Analysis Group, 17th (VEXAG)** — англоязычный термин, не имеющий аналога в русском языке. **17‑я аналитическая группа исследования Венеры (ВЕКСАГ)** — дословный перевод с английского на русский.</small>

**[VEXAG](vexag.md)** was established by NASA in 2005 to identify scientific priorities and opportunities for the exploration of [Venus](venus.md), Earth’s sister planet. The group has an open membership and an 7‑person Executive Committee, 3 Focus Groups, and 2 Topical Analysis Groups. Input from the scientific community is actively sought. The VEXAG provides findings to NASA Headquarters, but does not make recommendations.

The Venus Exploration Analysis Group is NASA’s community‑based forum designed to provide scientific input and technology development plans for planning and prioritizing the exploration of Venus over the next several decades, including a Venus surface sample return. VEXAG is chartered by NASA’s Solar System Exploration Division and reports its findings to NASA. Open to all interested scientists, VEXAG regularly evaluates Venus exploration goals, scientific objectives, investigations and critical measurement requirements, including especially recommendations in the NRC Decadal Survey and the Solar System Exploration Strategic Roadmap.

In addition to interested members of the scientific community, each focus group will include technology experts, NASA representatives, international partner representatives, EPO experts, and the chair. The current focus groups are listed below. Other focus groups may be added, as needed.

|**VEXAG 2019**|<https://www.lpi.usra.edu/vexag/meetings/vexag-17>;<br> <https://www.lpi.usra.edu/vexag>;<br> <https://twitter.com/unveilvenus>;<br> <https://www.facebook.com/groups/432478660134856>|
|:--|:--|
|Date|November 6 (We) ‑ 8 (Fr), 2019|
|Venue|[LASP](lasp.md) Space Sciences Building, Room W-120, 3665 Discovery Dr., Boulder, CO 80303|
|Host Org.| |
|Co‑host Org.| |
|Supported by| |
|Accepted by| |
|Content|Oral and Poster Presentations|
|Focus| |
|Reg. Fee| |
|Attendees| |
|Related Mtg.| |
|Contact|[Darby Dyar](person.md) (Mt.Holyoke College) — Chair;<br> [Adriana Ocampo](person.md) (NASA) — Executive Officer for VEXAG;<br> [Allan Treiman](person.md) (LPI)|

**Program Committee:**

1. [Darby Dyar](person.md) (Mt.Holyoke College)
1. [Emilie Royer](person.md) (LASP)
1. [Kevin McGouldrick](person.md) (LASP)
1. [Noam Izenberg](person.md) (JHUAPL)

**Local Organizing committee:**

1. …

**Secretariat:**

1. …

|**Steering Committe Member**|**Position/Roles**|**Tenure**|
|:--|:--|:--|
|[Darby Dyar](person.md) (Mt.Holyoke College)|Chair|12.2018 ‑ 12.2021|
|[Noam Izenberg](person.md) (JHUAPL)|Deputy Chair|12.2018 ‑ 12.2021|
|[Gary Hunter](person.md) (GRC)|Lead, Technology Development<br> & Laboratory Measurements Focus Group|02.2017 ‑ 02.2020|
|[Allan Treiman](person.md) (LPI)|Lead, Venus Goals & Exploration Sites Focus Group|06.2018 ‑ 02.2020|
|[Candace Gray](person.md) (N.Mexico Univ.);<br> [Joseph O'Rourke](person.md) (Arizona Univ.)|Co‑Leads Lead,<br> Early‑Career Scholars Outreach Focus Group|09.2018 ‑ 09.2021|
|[James Cutts](person.md) (JPL)|Venus Exploration Roadmap Focus Group|11.2016 ‑ 11.2019|
|[Kevin McGouldrick](person.md) (LASP)|At‑Large Member|02.2017 ‑ 02.2020|
|[Lynn Carter](person.md) (Arizona Univ.)|At‑Large Member|02.2017 ‑ 02.2020|
|[Robert E. Grimm](person.md) (SWRI)|Past Chair|07.2016 ‑ 12.2018|
|[Colin Wilson](person.md) (Oxford Univ.)|At‑Large Member|07.2018 ‑ 07.2021|
|[Patrick McGovern](person.md) (LPI)|At‑Large Member|07.2018 ‑ 07.2021|
|[Emilie Royer](person.md) (LASP)|At‑Large Member|07.2018 ‑ 07.2021|
|[Giada Arney](person.md) (GFSC)|At‑Large Member|09.2018 ‑ 09.2021|



## Описание

**Tentative Agenda** (2019.10.18)

1. **November 6, 2019**
   1. NASA updates
   1. Invited technique tutorial talks – Venus exploration state of the art in multiple fields (radar, atmospheric analysis, surface analysis, longevity, laboratory simulation, etc.)
   1. Lightning talks for poster session 1 (1 slide, 3-minute talks)
   1. Poster session 1
1. **November 7, 2019**
   1. White paper presentation
   1. White paper breakout groups and discussions
   1. Lunch/early afternoon field trip
   1. Lightning talks for poster session 2 (1 slide, 3-minute talks)
   1. Poster session 2
1. **November 8, 2019**
   1. Findings development, write-up
   1. VEXAG business
   1. Steering committee meeting



<p style="page-break-after: always"> </p>

## Итоги

**Таблица.** Общие участники с ближайшими конференциями.

|**Конференция**|**Участники**|
|:--|:--|
|**[IVC 2019](ivc_2019.md)**| |
|**[2019 VD Workshop](vdws2019.md)**| |
|**[MSSS, 10th](msss_10.md)**| |



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Events](event.md)】**<br> **Meetings:** [AGU](agu.md) ~~ [CGMS](cgms.md) ~~ [COSPAR](cospar.md) ~~ [DPS](dps.md) ~~ [EGU](egu.md) ~~ [EPSC](epsc.md) ~~ [FHS](fhs.md) ~~ [IPDW](ipdw.md) ~~ [IVC](ivc.md) ~~ [JpGU](jpgu.md) ~~ [LPSC](lpsc.md) ~~ [MAKS](maks.md) ~~ [MSSS](msss.md) ~~ [NIAC](niac_program.md) ~~ [VEXAG](vexag.md) ~~ [WSI](wsi.md) ┊ ··•·· **Contests:** [Google Lunar X Prize](google_lunar_x_prize.md)|

1. Docs: …
1. <https://www.lpi.usra.edu/vexag/meetings/vexag-17>
1. <https://www.lpi.usra.edu/vexag>


## The End

end of file
