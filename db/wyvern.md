# Wyvern Space
> 2019.08.15 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/w/wyvern_logo1t.webp)](f/c/w/wyvern_logo1.webp)|<info@wyvern.space>, <mark>nophone</mark>, Fax …;<br> *Edmonton, Alberta, Canada*<br> <https://www.wyvern.space> ~~ [LI ⎆](https://www.linkedin.com/company/wyvern-space) ~~ [X ⎆](https://twitter.com/wyvernspace)|
|:--|:--|
|**Business**|Providing commercial satellite imagery from small sats|
|**Mission**|…|
|**Vision**|Provide actionable intelligence from space anywhere in the solar system to enable a sustainable future for humanity|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Wyvern** is a space company delivering premium Earth observation imagery from satellite platforms (made by other companies).

Our images fuel the expanding Earth observation data market, which is rapidly progressing due to advances in analytics & machine learning. We’re creating proprietary technologies to increase the resolution of Earth observation imagery from a small satellite platform. Earth observation data improves quality of life for people around the globe & its applications are continually growing. Analytics firms can utilize our data in industrial processes, agriculture,  environmental monitoring, & countless more vital applications.



## The End

end of file
