# RW (a list)
> 2019.07.31 [🚀](../../index/index.md) [despace](index.md) → [IU](iu.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

A list of [Reaction wheels](sensor.md).

## Current

### AAC RW222   ［EU］
**RW222** — an [RW](iu.md) for orientation & stabilization of a [SC](sc.md) (pref. for 1U ‑ 3U CubeSats).  
*Manufacturer:* [AAC ClydeSpace](aac_cs.md). Designed in 2017.

|**Characteristics**|**(RW222-3)**|**(RW222-6)**|
|:--|:--|:--|
|Composition|1 unit w/ speed/torque control|Same|
|Consumption, W| | |
|Dimensions, ㎜|25 × 25 × 15|25 × 25 × 15|
|[Interfaces](interface.md)|I²C interface|I²C interface|
|[Lifetime](lifetime.md), h(y)|… / …|… / …|
|Mass, ㎏| | |
|[Overload](vibration.md), Grms| | |
|[Radiation](ion_rad.md), ㏉(㎭)| | |
|[Reliability](qm.md)| | |
|[Thermal](tcs.md), ℃|−20 ‑ +60|−20 ‑ +60|
|[TRL](trl.md)|9|9|
|[Voltage](sps.md), V|3.25 ‑ 3.3 ‑ 3.5|3.25 ‑ 3.3 ‑ 3.5|
|**【Specific】**|~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |
|Axes|1|1|
|Max momentum, N·m·s|0.003|0.006|
|Max torque, N·m|0.002|0.002|
|Ready mode| | |
|RPM|10 000 ‑ 15 000 ±1%|10 000 ‑ 15 000 ±1%|
| |[![](f/iu/r/rw222_pic1t.webp)](f/iu/r/rw222_pic1.webp)| |

**Notes:**

1. [3D model (STEP) ❐](f/iu/r/rw222_3d.zip)
1. [Datasheet (PDF) ❐](f/iu/r/rw222_datasheet.pdf)
1. **Applicability:** …



### AAC RM400   ［EU］
**RW222** — an [RW](iu.md) for orientation & stabilization of a [SC](sc.md) (pref. for 6U ‑ 12U CubeSats).  
*Manufacturer:* [AAC ClydeSpace](aac_cs.md). Designed in ….

|**Characteristics**|**(RM400-15)**|**(RM400-30)**|**(RM400-50)**|
|:--|:--|:--|:--|:--|
|Composition|1 unit w/ speed/torque control|Same|Same|
|Consumption, W|0 ‑ 1 ‑ 1.9 (15 peak)|0 ‑ 1 ‑ 1.9 (15 peak)|0 ‑ 1 ‑ 1.9 (15 peak)|
|Dimensions, ㎜|50 × 50 × 27|50 × 50 × 27|50 × 50 × 27|
|[Interfaces](interface.md)| | | |
|[Lifetime](lifetime.md), h(y)|… / …|… / …|… / …|
|Mass, ㎏|0.2|0.21|0.38|
|[Overload](vibration.md), Grms| | | |
|[Radiation](ion_rad.md), ㏉(㎭)|360 (36 000)|360 (36 000)|360 (36 000)|
|[Reliability](qm.md)| | | |
|[Thermal](tcs.md), ℃|−40 ‑ +60|−40 ‑ +60|−40 ‑ +60|
|[TRL](trl.md)|9|9|9|
|[Voltage](sps.md), V|4.5 ‑ 5 ‑ 5.25|4.5 ‑ 5 ‑ 5.25|4.5 ‑ 5 ‑ 5.25|
|**【Specific】**|~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |
|Axes|1|1|1|
|Max momentum, N·m·s|0.015|0.03|0.05|
|Max torque, N·m|0.008|0.008|0.008|
|Ready mode| | | |
|RPM|5 000 ±1%|5 000 ±1%|5 000 ±1%|
| |[![](f/iu/r/rw400_pic1t.webp)](f/iu/r/rw400_pic1.webp)| | |

**Notes:**

1. [3D model (STEP) ❐](f/iu/r/rw400_3d.zip)
1. [Datasheet (PDF) ❐](f/iu/r/rw400_datasheet.pdf)
1. **Applicability:** …



### AAC Trillian-1   ［EU］
**Trillian-1** — an [RW](iu.md) for orientation & stabilization of a [SC](sc.md). Designed by [AAC ClydeSpace](aac_cs.md) in 2019.

|**Characteristics**|**(Trillian-1)**|
|:--|:--|
|Composition|1 unit w/ speed/torque control|
|Consumption, W|1.5 ‑ 12 ‑ 24|
|Dimensions, ㎜|135 × 135 × 82|
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|1.5|
|[Overload](vibration.md), Grms|10|
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃|−10 ‑ +60|
|[TRL](trl.md)|9|
|[Voltage](sps.md), V|20 ‑ 34|
|**【Specific】**|~ ~ ~ ~ ~ |
|Axes|1|
|Max momentum, N·m·s|1.2|
|Max torque, N·m|0.047|
|Ready mode| |
|RPM|6 500 ±1%|
| |[![](f/iu/t/trillian_1_pic1t.webp)](f/iu/t/trillian_1_pic1.webp)|

**Notes:**

1. [Datasheet (PDF) ❐](f/iu/t/trillian_1_datasheet.pdf)
1. **Applicability:** …



### NSS NRWA-T   ［ZA］
**NRWA-T** — a [RW](iu.md) for orientation & stabilization of a [SC](sc.md). Designed by [NewSpace Systems](newspace_sys.md) in ….

|**Characteristics**|**(NRWA-T065)**|**(NRWA-T2)**|**(NRWA-T6)**|
|:--|:--|:--|:--|
|Composition|1 unit (incl. electr.)|1 unit (incl. electr.)|1 unit (incl. electr.)|
|Consumption, W|2.6 (Quiescent)<br> 4 (steady 3 000 RPM)<br> 6 (steady 6 500 RPM)<br> 1.7 / mNm|1.7 (Quiescent)<br> 4 (steady 1 500 RPM)<br> 4.8 (steady 2 600 RPM)<br> 0.4 / mNm| 6 (Quiescent)<br> 20 (steady 2 000 RPM)<br> 30 (steady 3 000 RPM)<br> 83 (0.2 Nm at 2 000 RPM)<br> 136 (0.2 Nm at 3 800 RPM)|
|Dimensions, ㎜|102 × 102 × 105|150 × 150 × 75|210 × 210 × 100|
|[Interfaces](interface.md)|RS-422<br> 9‑pin D‑type Male<br> 4off M5|RS-422<br> 9‑pin D‑type Male<br> 4off M5|RS-485 (full duplex)<br> 9‑pin D‑type Male (power)<br> 25‑pin D‑type Male (data)<br> 8off M5|
|[Lifetime](lifetime.md), h(y)| | | |
|Mass, ㎏|1.55|2.8|5|
|[Overload](vibration.md), Grms|16.95 (random)|16.95 (random)|14 (random)|
|[Radiation](ion_rad.md), ㏉(㎭)|100 (10 000)|100 (10 000)|200 (20 000)|
|[Reliability](qm.md)|0.998 (2 y)|0.998 (2 y)|0.998 (2 y)|
|[Thermal](tcs.md), ℃|–10 ‑ +45|–20 ‑ +58|–20 ‑ +60|
|[TRL](trl.md)|9|9|9|
|[Voltage](sps.md), V|28|28|28|
|**【Specific】**|~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |
|Axes|1|1|1|
|Max momentum, N·m·s|0.65 (6 500 RPM, 0.02 Nm)<br> 0.94 (9 000 RPM)|1 (1 850 RPM, 0.84 Nm)<br> 1.47 (2 700 RPM)|6 (3 800 RPM, 0.2 Nms)<br> 7.83 (5 000 RPM)|
|Max torque, N·m|0.02|0.09|0.2 (nominal), 0.3 (peak)|
|Ready mode| | | |
|RPM|9 000|2 700|5 000|
| |[![](f/iu/n/nss_nrwa_t10_pic1t.webp)](f/iu/n/nss_nrwa_t10_pic1.webp)|[![](f/iu/n/nss_nrwa_t10_pic1t.webp)](f/iu/n/nss_nrwa_t10_pic1.webp)|[![](f/iu/n/nss_nrwa_t10_pic1t.webp)](f/iu/n/nss_nrwa_t10_pic1.webp)|

**Notes:**

1. **Applicability:** …



### Polus Agat‑5   ［RU］
> <small>**Агат‑5** — русскоязычный термин, не имеющий аналога в английском языке. **Agat-5** — дословный перевод с русского на английский.</small>

**Агат‑5** — инерциальное устройство ([двигатель‑маховик](iu.md)), предназначенное для использования в качестве исполнительного органа системы ориентации и стабилизации [космического аппарата](sc.md).  
*Разработчик:* [НПЦ Полюс](polus_tomsk.md). Разработано в 1995 году активное применение

|**Characteristics**|**(Агат‑5)**|
|:--|:--|
|Composition|1 unit (1 БЭ, 1 УДМ)|
|Consumption, W|80 постоянно, 175 макс.|
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / 100 000 (11.4)|
|Mass, ㎏|87|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|Axes| |
|Max momentum, N·m·s|100|
|Max torque, N·m|0.125|
|Ready mode| |
|RPM| |
|[![](f/iu/a/agat_5_pic1t.webp)](f/iu/a/agat_5_pic1.webp)|

**Notes:**

1. <http://www.polus.tomsknet.ru/?:id=211>
1. **Applicability:** …



### Polus Agat‑10   ［RU］
> <small>**Агат‑10** — русскоязычный термин, не имеющий аналога в английском языке. **Agat-10** — дословный перевод с русского на английский.</small>

**Агат‑10** — инерциальное устройство ([двигатель‑маховик](iu.md)), предназначенное для использования в качестве исполнительного органа системы ориентации и стабилизации [космического аппарата](sc.md).  
*Разработчик:* [НПЦ Полюс](polus_tomsk.md). Разработано в 1995 году активное применение

|**Characteristics**|**(Агат‑10)**|
|:--|:--|
|Composition|1 unit (1 БЭ, 1 УДМ)|
|Consumption, W|45 постоянно, 95 макс.|
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / 70 000 (8)|
|Mass, ㎏|39|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|Axes| |
|Max momentum, N·m·s|250|
|Max torque, N·m|0.1|
|Ready mode| |
|RPM| |
| |[![](f/iu/a/agat_10_pic1t.webp)](f/iu/a/agat_10_pic1.webp)|

**Notes:**

1. <http://www.polus.tomsknet.ru/?:id=211>
1. **Applicability:** …



### Polus Agat‑15   ［RU］
> <small>**Агат‑15** — русскоязычный термин, не имеющий аналога в английском языке. **Agat-15** — дословный перевод с русского на английский.</small>

**Агат‑15** — инерциальное устройство ([двигатель‑маховик](iu.md)), предназначенное для использования в качестве исполнительного органа системы ориентации и стабилизации [космического аппарата](sc.md).  
Разработчик [НПЦ Полюс](polus_tomsk.md). Разработано в 2005 году Активное использование. Покупное изделие. <small>(по состоянию на 21.06.2017)</small>

|**Characteristics**|**(Агат‑15)**|
|:--|:--|
|Composition|4 УДМ [УДМ‑15‑0,15](rw_lst.md); 1 БА.<br> БА содержит 4 канала управления моментом. Каждый из каналов управления моментом управляет соответствующим УДМ. Агат‑15 обеспечивает формирование динамических моментов по осям вращения каждого УДМ.|
|Consumption, W|–75 ‑ +145 (для 2 работающих УДМ)|
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… (); 1 000 при выходе кинетического момента за допустимые нормы / 100 000 (11.4); 2 500 циклов вкл/выкл|
|Mass, ㎏|21 (3.7 УДМ; 6.2 БА)|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)|0.98 за 11.4 года; 0.99799 за 3 года|
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)| |
|[Voltage](sps.md), V|27 (24  ‑ 28)|
|**【Specific】**|~ ~ ~ ~ ~ |
|Axes| |
|Max momentum, N·m·s|15 для каждого УДМ|
|Max torque, N·m| |
|RPM| |
|Ready mode|1 s с момента подачи напряжения|
| |[![ ❐](f/iu/a/agat_15_ba_pic001t.webp)](f/iu/a/agat_15_ba_pic001.webp) [![](f/iu/a/agat_15_ba_pic002t.webp)](f/iu/a/agat_15_ba_pic002.webp)|

**Notes:**

1. [3D-модель ❐](f/iu/a/agat_15_ba_3d_2017.7z) ~~ [Чертёж блока автоматики ❐](f/iu/a/agat_15_ba_sketch_2015.djvu)
1. <http://polus.tomsknet.ru/?:id=211> *([Архивировано ❐](f/iu/a/agat_15_polus_tomsknet_ru.djvu) 2018.03.07)*
1. **Applicability:** [Венера‑Д](венера‑д.md) ~~ [Luna‑26](луна_26.md)



### Polus Agat‑40   ［RU］
> <small>**Агат‑40** — русскоязычный термин, не имеющий аналога в английском языке. **Agat-40** — дословный перевод с русского на английский.</small>

**Агат‑40** — инерциальное устройство ([двигатель‑маховик](iu.md)), предназначенное для использования в качестве исполнительного органа системы ориентации и стабилизации [космического аппарата](sc.md).  
Разработчик [НПЦ Полюс](polus_tomsk.md). Разработано. Активное использование. Покупное изделие (на 2018.03.07).

|**Characteristics**|**(Агат‑40)**|
|:--|:--|
|Composition|1 БЭ, 4 УДМ|
|Consumption, W|–200 ‑ +200|
|Dimensions, ㎜|⌀ 368 × 110 — УДМ;<br> 200 × 200 × 340|
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|133 650 (15.25);<br> 1 000 при выходе кинетического момента за допустимые нормы / 148 920 (17);<br> 5 000 команд|
|Mass, ㎏|39.5 (8.25 УДМ, 6.5 БЭ)|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)|0.99|
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)| |
|[Voltage](sps.md), V|25 ‑ 28|
|**【Specific】**|~ ~ ~ ~ ~ |
|Axes| |
|Max momentum, N·m·s|40|
|Max torque, N·m| |
|Ready mode| |
|RPM| |
| |[![](f/iu/a/agat_40_pic1t.webp)](f/iu/a/agat_40_pic1.webp)|[![](f/iu/a/agat_40_pic2t.webp)](f/iu/a/agat_40_pic2.webp)|

**Notes:**

1. [3D‑модель ❐](f/iu/a/agat_40_3d.7z) ~~ [Чертежи ❐](f/iu/a/agat_40_sketches.pdf) (БА и УДМ)
1. **Applicability:** Экспресс‑АМ5・ Экспресс‑АМ6・ Ямал‑401・ Благосвет・ [Электро‑Л](электро_л.md) №3



### Polus GD-02-150   ［RU］
> <small>**ГД-02-150** — русскоязычный термин, не имеющий аналога в английском языке. **GD-02-150** — дословный перевод с русского на английский.</small>

**ГД-02-150** — инерциальное устройство ([двигатель‑маховик](iu.md)), предназначенное для использования в качестве исполнительного органа системы ориентации и стабилизации [космического аппарата](sc.md).  
*Разработчик:* [НПЦ Полюс](polus_tomsk.md). Разработано в 2007 году активное применение

|**Characteristics**|**(ГД-02-150)**|
|:--|:--|
|Composition|1 unit|
|Consumption, W|75 постоянно, 145 макс.|
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / 100 000 (11.4)|
|Mass, ㎏| |
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|Axes| |
|Max momentum, N·m·s|150|
|Max torque, N·m|2|
|Ready mode| |
|RPM| |
| |[![](f/iu/g/gd_02_150_pic1t.webp)](f/iu/g/gd_02_150_pic1.webp)|

**Notes:**

1. <http://www.polus.tomsknet.ru/?:id=211>
1. **Applicability:** …



### Polus GD-200-125   ［RU］
> <small>**ГД-200-125** — русскоязычный термин, не имеющий аналога в английском языке. **GD-200-125** — дословный перевод с русского на английский.</small>

**ГД-200-125** — инерциальное устройство ([двигатель‑маховик](iu.md)), предназначенное для использования в качестве исполнительного органа системы ориентации и стабилизации [космического аппарата](sc.md).  
*Разработчик:* [НПЦ Полюс](polus_tomsk.md). Разработано в 2007 году активное применение

|**Characteristics**|**(ГД-200-125)**|
|:--|:--|
|Composition|1 unit|
|Consumption, W|75 постоянно, 160 макс.|
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / 150 000 (17)|
|Mass, ㎏| |
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|Axes| |
|Max momentum, N·m·s|200|
|Max torque, N·m|125|
|Ready mode| |
|RPM| |
| |[![](f/iu/g/gd_200_125_pic1t.webp)](f/iu/g/gd_200_125_pic1.webp)|

**Notes:**

1. <http://www.polus.tomsknet.ru/?:id=211>
1. **Applicability:** …



### Polus MDM-0,5   ［RU］
> <small>**МДМ-0,5** — русскоязычный термин, не имеющий аналога в английском языке. **MDM-0,5** — дословный перевод с русского на английский.</small>

**МДМ-0,5** — инерциальное устройство ([двигатель‑маховик](iu.md)), предназначенное для использования в качестве исполнительного органа системы ориентации и стабилизации [космического аппарата](sc.md).  
*Разработчик:* [НПЦ Полюс](polus_tomsk.md). Разработано в 2007 году активное применение

|**Characteristics**|**(МДМ-0,5)**|
|:--|:--|
|Composition|1 БЭ, 2 УДМ|
|Consumption, W|2 постоянно, 15 макс.|
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / 150 000 (17)|
|Mass, ㎏|2.8 (1.2 УДМ, 0.4 БЭ)|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|Axes| |
|Max momentum, N·m·s|0.5|
|Max torque, N·m|0.015|
|Ready mode| |
|RPM| |
| |[![](f/iu/m/mdm_0_5_pic1t.webp)](f/iu/m/mdm_0_5_pic1.webp)|

**Notes:**

1. <http://www.polus.tomsknet.ru/?:id=211>
1. **Applicability:** …



### Polus MDM-1,0   ［RU］
> <small>**МДМ-1,0** — русскоязычный термин, не имеющий аналога в английском языке. **MDM-1,0** — дословный перевод с русского на английский.</small>

**МДМ-1,0** — инерциальное устройство ([двигатель‑маховик](iu.md)), предназначенное для использования в качестве исполнительного органа системы ориентации и стабилизации [космического аппарата](sc.md).  
*Разработчик:* [НПЦ Полюс](polus_tomsk.md). Разработано в 2007 году активное применение

|**Characteristics**|**(МДМ-1,0)**|
|:--|:--|
|Composition|1 БЭ, 2 УДМ|
|Consumption, W|2 постоянно, 20 макс.|
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / 150 000 (17)|
|Mass, ㎏|3.4 (1.5 УДМ, 0.4 БЭ)|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|Axes| |
|Max momentum, N·m·s|1|
|Max torque, N·m|0.02|
|Ready mode| |
|RPM| |
| |[![](f/iu/m/mdm_1_0_pic1t.webp)](f/iu/m/mdm_1_0_pic1.webp)|

**Notes:**

1. <http://www.polus.tomsknet.ru/?:id=211>
1. **Applicability:** …



### Polus MDM-2-50   ［RU］
> <small>**МДМ-2-50** — русскоязычный термин, не имеющий аналога в английском языке. **MDM-2-50** — дословный перевод с русского на английский.</small>

**МДМ-2-50** — инерциальное устройство ([двигатель‑маховик](iu.md)), предназначенное для использования в качестве исполнительного органа системы ориентации и стабилизации [космического аппарата](sc.md).  
*Разработчик:* [НПЦ Полюс](polus_tomsk.md). Разработано в 2007 году активное применение

|**Characteristics**|**(МДМ-2-50)**|
|:--|:--|
|Composition|1 unit (1 БЭ, 2 УДМ)|
|Consumption, W|3 постоянно, 39 макс.|
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / 175 000 (20)|
|Mass, ㎏|4.5 (2 УДМ, 0.5 БЭ)|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|Axes| |
|Max momentum, N·m·s|2|
|Max torque, N·m|0.05|
|Ready mode| |
|RPM| |
| |[![](f/iu/m/mdm_2_50_pic1t.webp)](f/iu/m/mdm_2_50_pic1.webp)|

**Notes:**

1. <http://www.polus.tomsknet.ru/?:id=211>
1. **Applicability:** …



### Polus SGK-20-20   ［RU］
> <small>**СГК-20-20** — русскоязычный термин, не имеющий аналога в английском языке. **SGK-20-20** — дословный перевод с русского на английский.</small>

**СГК-20-20** — инерциальное устройство ([двигатель‑маховик](iu.md)), предназначенное для использования в качестве исполнительного органа системы ориентации и стабилизации [космического аппарата](sc.md).  
*Разработчик:* [НПЦ Полюс](polus_tomsk.md). Разработано в 2007 году активное применение

|**Characteristics**|**(СГК-20-20)**|
|:--|:--|
|Composition|1 БЭ, 2 УДМ|
|Consumption, W|30 постоянно, 60 макс.|
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / 150 000 (17)|
|Mass, ㎏|37 (8 БЭ, 14.5 УДМ)|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|Axes| |
|Max momentum, N·m·s|20|
|Max torque, N·m|20|
|Ready mode| |
|RPM| |
| |[![](f/iu/s/sgk_20_20_pic1t.webp)](f/iu/s/sgk_20_20_pic1.webp)|

**Notes:**

1. <http://www.polus.tomsknet.ru/?:id=211>
1. **Applicability:** …



### Polus UDM-15   ［RU］
> <small>**УДМ‑15** — русскоязычный термин, не имеющий аналога в английском языке. **UDM‑15** — дословный перевод с русского на английский.</small>

**Управляющий двигатель‑маховик 「УДМ‑15」** — инерциальное устройство, предназначенное для использования в качестве исполнительного органа системы ориентации и стабилизации [космического аппарата](sc.md).  
Разработчик и изготовитель [НПЦ Полюс](polus_tomsk.md); разработан в 2005 году Активное использование. Покупное изделие. (по состоянию на 21.06.2017)

|**Characteristics**|**(UDM‑15)**|
|:--|:--|
|Composition|1 unit|
|Consumption, W|–37.5 ‑ +72.5 (для 2 работающих УДМ)|
|Dimensions, ㎜|80 × 315|
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / 1 000 при выходе кин.момента за нормы; 2 500 циклов вкл/выкл|
|Mass, ㎏|3.7|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)|0.98|
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V|27 — штатное;<br> от 24 до 28 — допустимое|
|**【Specific】**|~ ~ ~ ~ ~ |
|Axes| |
|Max momentum, N·m·s|15|
|Max torque, N·m| |
|Ready mode|1 s с момента подачи напряжения|
|RPM| |
| |[![ ❐](f/iu/a/agat_25_udm-15_pic002.webp)](f/iu/a/agat_25_udm-15_pic002.webp)|[![](f/iu/a/agat_25_udm-15_pic003t.webp)](f/iu/a/agat_25_udm-15_pic003.webp)
| |[![](f/iu/a/agat_25_udm-15_pic001t.webp)](f/iu/a/agat_25_udm-15_pic001.webp) Система координат УДМ‑15|

**Notes:**

1. [3D-модель ❐](f/iu/a/agat_25_udm-15-0.15_3d_2017.7z) ~~ [Чертёж ❐](f/iu/a/agat_25_udm-15-0.15_sketch_2016.djvu)
1. **Applicability:** [Венера‑Д](венера‑д.md) ~~ [Luna‑26](луна_26.md) (в составе [Агат‑15](rw_lst.md))



### Serenum RW25   ［EU］
**RW25** — an [RW](iu.md) for orientation & stabilization of a [SC](sc.md) (pref. for 1U ‑ 3U CubeSats).  
*Manufacturer:* [Serenum Space](serenum_space.md). Designed in ….

|**Characteristics**|**(RW25)**|
|:--|:--|
|Composition|1 unit:<br> BLDC motor w/ 3 Hall sensors, ARM Cortex-M0+ (24 ㎒),<br> 6‑axis IMU (3‑axis accelerometer, 3‑axis gyroscope)|
|Consumption, W|0.075 ‑ 0.3 ‑ 0.4|
|Dimensions, ㎜|25×25×25|
|[Interfaces](interface.md)|I²C or UART, CubeSat Space Protocol|
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|0.04|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)|360 (36 000)|
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃|−40 ‑ +85|
|[TRL](trl.md)|9|
|[Voltage](sps.md), V|5 or 8|
|**【Specific】**|~ ~ ~ ~ ~ |
|Axes|1|
|Max momentum, N·m·s|0.0006|
|Max torque, N·m|0.0002|
|Ready mode| |
|RPM|5 600|
| |[![](f/iu/r/rw25_pic1t.webp)](f/iu/r/rw25_pic1.webp)|

**Notes:**

1. <https://www.serenumspace.com/products/reaction-wheels>
1. [Datasheet (PDF) ❐](f/iu/r/rw25_datasheet.pdf)
1. **Applicability:** …



### VNIIEM DM1-20   ［RU］
> <small>**ДМ1-20** — русскоязычный термин, не имеющий аналога в английском языке. **DM1-20** — дословный перевод с русского на английский.</small>

**ДМ1-20** — инерциальное устройство ([двигатель‑маховик](iu.md)), предназначенное для использования в качестве исполнительного органа системы ориентации и стабилизации [космического аппарата](sc.md).  
*Разработчик:* [ВНИИЭМ](vniiem.md). Разработано ранее 2015 года активное применение

|**Characteristics**|**(dm1-20)**|
|:--|:--|
|Composition|1 unit (1 БЭ, 2 УДМ)|
|Consumption, W|3 постоянно, 15 макс.|
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / 45 000 (5.1)|
|Mass, ㎏|1.4|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V|24 ‑ 34|
|**【Specific】**|~ ~ ~ ~ ~ |
|Axes| |
|Max momentum, N·m·s|1|
|Max torque, N·m|20|
|Ready mode| |
|RPM| |
| |[![](f/iu/d/dm1_20_pic1t.webp)](f/iu/d/dm1_20_pic1.webp)|

**Notes:**

1. <http://www.vniiem.ru/ru/index.php?:view=article&id=288:2010-02-17-21-42-21>
1. **Applicability:** Метеор‑М・ Татьяна‑2



### VNIIEM DM5-50   ［RU］
> <small>**ДМ5-50** — русскоязычный термин, не имеющий аналога в английском языке. **DM5-50** — дословный перевод с русского на английский.</small>

**ДМ5-50** — инерциальное устройство ([двигатель‑маховик](iu.md)), предназначенное для использования в качестве исполнительного органа системы ориентации и стабилизации [космического аппарата](sc.md).  
*Разработчик:* [ВНИИЭМ](vniiem.md). Разработано ранее 2015 года активное применение

|**Characteristics**|**(dm5-50)**|
|:--|:--|
|Composition|1 unit (2 УДМ, 1 БЭ)|
|Consumption, W|4 постоянно, 31 макс.|
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / 50 000 (5.7)|
|Mass, ㎏|3.8|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V|24 ‑ 34|
|**【Specific】**|~ ~ ~ ~ ~ |
|Axes| |
|Max momentum, N·m·s|5|
|Max torque, N·m|50|
|Ready mode| |
|RPM| |
| |[![](f/iu/d/dm5_50_pic1t.webp)](f/iu/d/dm5_50_pic1.webp)|

**Notes:**

1. <http://www.vniiem.ru/ru/index.php?:view=article&id=288:2010-02-17-21-42-21>
1. **Applicability:** …



### VNIIEM DM10-25   ［RU］
> <small>**ДМ-10-25** — русскоязычный термин, не имеющий аналога в английском языке. **DM-10-25** — дословный перевод с русского на английский.</small>

**ДМ-10-25** — инерциальное устройство ([двигатель‑маховик](iu.md)), предназначенное для использования в качестве исполнительного органа системы ориентации и стабилизации [космического аппарата](sc.md).  
*Разработчик:* [ВНИИЭМ](vniiem.md). Разработано ранее 2015 года активное применение

|**Characteristics**|**(ДМ-10-25)**|
|:--|:--|
|Composition|1 unit (1 БЭ, 2 УДМ)|
|Consumption, W|5 постоянно, 31 макс.|
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / 50 000 (5.7)|
|Mass, ㎏|4|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V|24 ‑ 34|
|**【Specific】**|~ ~ ~ ~ ~ |
|Axes| |
|Max momentum, N·m·s|10|
|Max torque, N·m|25|
|Ready mode| |
|RPM| |
| |[![](f/iu/d/dm_10_25_pic1t.webp)](f/iu/d/dm_10_25_pic1.webp)|

**Notes:**

1. <http://www.vniiem.ru/ru/index.php?:view=article&id=288:2010-02-17-21-42-21>
1. **Applicability:** …



### VNIIEM DM14-120   ［RU］
> <small>**ДМ14-120** — русскоязычный термин, не имеющий аналога в английском языке. **DM14-120** — дословный перевод с русского на английский.</small>

**ДМ14-120** — инерциальное устройство ([двигатель‑маховик](iu.md)), предназначенное для использования в качестве исполнительного органа системы ориентации и стабилизации [космического аппарата](sc.md).  
*Разработчик:* [ВНИИЭМ](vniiem.md). Разработано в … году разработка

|**Characteristics**|**(dm14-120)**|
|:--|:--|
|Composition|1 БЭ, 2 УДМ|
|Consumption, W|5.5 постоянно, 65 макс.|
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / 50 000 (5.7)|
|Mass, ㎏|6.3|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|4|
|[Voltage](sps.md), V|24 ‑ 34|
|**【Specific】**|~ ~ ~ ~ ~ |
|Axes| |
|Max momentum, N·m·s|14|
|Max torque, N·m|120|
|Ready mode| |
|RPM| |
| |[![](f/iu/d/dm14_120_pic1t.webp)](f/iu/d/dm14_120_pic1.webp)|

**Notes:**

1. <http://www.vniiem.ru/ru/index.php?:view=article&id=288:2010-02-17-21-42-21>
1. **Applicability:** …



### VNIIEM DM20-250   ［RU］
> <small>**ДМ20-250** — русскоязычный термин, не имеющий аналога в английском языке. **DM20-250** — дословный перевод с русского на английский.</small>

**ДМ20-250** — инерциальное устройство ([двигатель‑маховик](iu.md)), предназначенное для использования в качестве исполнительного органа системы ориентации и стабилизации [космического аппарата](sc.md).  
*Разработчик:* [ВНИИЭМ](vniiem.md). Разработано ранее 2015 года активное применение

|**Characteristics**|**(dm20-250)**|
|:--|:--|
|Composition|1 unit (1 БЭ, 2 УДМ)|
|Consumption, W|6 постоянно, 70 макс.|
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / 45 000 (5.1)|
|Mass, ㎏|11.5|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V|24 ‑ 34|
|**【Specific】**|~ ~ ~ ~ ~ |
|Axes| |
|Max momentum, N·m·s|20|
|Max torque, N·m|250|
|Ready mode| |
|RPM| |
| |[![](f/iu/d/dm20_250_pic1t.webp)](f/iu/d/dm20_250_pic1.webp)|

**Notes:**

1. <http://www.vniiem.ru/ru/index.php?:view=article&id=288:2010-02-17-21-42-21>
1. **Applicability:** …



### VNIIEM DMB   ［RU］
> <small>**ДМБ** — русскоязычный термин, не имеющий аналога в английском языке. **DMB** — дословный перевод с русского на английский.</small>

**ДМБ** — инерциальное устройство ([двигатель‑маховик](iu.md)), предназначенное для использования в качестве исполнительного органа системы ориентации и стабилизации [космического аппарата](sc.md).  
*Разработчик:* [ВНИИЭМ](vniiem.md). Разработано ранее 2015 года активное применение

|**Characteristics**|**(ДМБ)**|
|:--|:--|
|Composition|1 БЭ, 1 УДМ|
|Consumption, W|7 постоянно, 100 макс.|
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / 29 930 (3.4)|
|Mass, ㎏|17.5 (13.8 УДМ, 3.7 БЭ)|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V|24 ‑ 34|
|**【Specific】**|~ ~ ~ ~ ~ |
|Axes| |
|Max momentum, N·m·s|29.4|
|Max torque, N·m|350|
|Ready mode| |
|RPM| |
| |[![](f/iu/d/dmb_pic1t.webp)](f/iu/d/dmb_pic1.webp)|

**Notes:**

1. <http://www.vniiem.ru/ru/index.php?:view=article&id=288:2010-02-17-21-42-21>
1. **Applicability:** …



## Archive

### ・Template・   ［…］
**...** — a [RW](iu.md) for orientation & stabilization of a [SC](sc.md). Designed by [...](....md) in ….

|**Characteristics**|**(...)**|
|:--|:--|
|Composition| |
|Consumption, W| |
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)| |
|Mass, ㎏| |
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)| |
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|Axes| |
|Max momentum, N·m·s| |
|Max torque, N·m| |
|Ready mode| |
|RPM| |
| |[![](f/iu/….webp)](f/iu/….webp)|

**Notes:**

1. …
1. **Applicability:** …



### NIIKP Koler-E   ［RU］
> <small>**Колер‑Э** — русскоязычный термин, не имеющий аналога в английском языке. **Koler-E** — дословный перевод с русского на английский.</small>

**Колер‑Э** — инерциальное устройство ([двигатель‑маховик](iu.md)), предназначенное для использования в качестве исполнительного органа системы ориентации и стабилизации [космического аппарата](sc.md).  
*Разработчик:* [НИИКП](niicom.md). Разработано в … году архивное изделие

|**Characteristics**|**(Колер‑Э)**|
|:--|:--|
|Composition|1 БЭ, 2 УДМ|
|Consumption, W|66 (65 БЭ, 0.5 УДМ)|
|Dimensions, ㎜|УДМ: ⌀315 × 72.5, БЭ: 338 × 280 × 230|
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|87 600 (10) / …|
|Mass, ㎏|22.8 (4.6 УДМ, 13.6 БЭ)|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)|0.967|
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|Axes| |
|Max momentum, N·m·s| |
|Max torque, N·m| |
|Ready mode| |
|RPM| |
| |[![](f/iu/k/koler_e_pic1t.webp)](f/iu/k/koler_e_pic1.webp)|

**Notes:**

1. …
1. **Applicability:** …



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**`Двигатель‑маховик (ДМ):`**<br> …<br>~ ~ ~ ~ ~<br> **РФ:** [ДМБ](rw_lst.md) (29.4/350) ~~ [Агат‑10](rw_lst.md) (250/0.1) ~~ [ДМ20-250](rw_lst.md) (20/250) ~~ [ГД-200-125](rw_lst.md) (200/125) ~~ [ГД-02-150](rw_lst.md) (150/2) ~~ [ДМ14-120](rw_lst.md) (14/120) ~~ [Агат‑5](rw_lst.md) (100/0.125) ~~ [ДМ5-50](rw_lst.md) (5/50) ~~ [Агат‑40](rw_lst.md) (40/…) ~~ [ДМ10-25](rw_lst.md) (10/25) ~~ [ДМ1-20](rw_lst.md) (1/20) ~~ [СГК-20-20](rw_lst.md) (20/20) ~~ [Агат‑15](rw_lst.md) (15/0.15) ~~ [МДМ-2-50](rw_lst.md) (2/0.05) ~~ [МДМ-1,0](rw_lst.md) (1/0.02) ~~ [МДМ-0,5](rw_lst.md) (0.5/0.015) ~~ [SX-WH](sx_wh.md) (0.5 ‑ 0.03/…) ~~ [SX-GY](sx_gy.md) (0.04/…)|

1. Docs: …
1. …



## The End

end of file
