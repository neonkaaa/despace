# Engines (a list)
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [PS](ps.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

A list of [Engines](ps.md).

## Current


### Aerojet AMBR   ［US・2comp.］
**AMBR** (Advanced material bipropellant rocket) — 2‑component [engine](ps.md) by [Aerojet Rocketdyne](aerojet_rocketdyne.md). Designed in 2012. Successor of [HiPAT](hipat.md).

|**Characteristics**|**AMBR**|
|:--|:--|
|Composition| |
|Consumption, W| |
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|3 ‑ 10 / …|
|Mass, ㎏|5.4|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[NTO+Hydrazine](nto_plus.md) (H₂,H₂O,NH₃,N₂)|
|Fuel: consump., ㎏/s, ≤|0.202.960|
|Fuel: mass ratio|1.2|
|Fuel: resource, ㎏, ≥| |
|Pres.: c.chamber, ㎫(㍴)|1.89 (18.54)|
|Pres.: inlet, ㎫(㍴)|2.75 (27)|
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)|2 473 (2 200)|
|T°: nozzle cut, К(℃)| |
|Thrust, N|667|
|Thrust: 1 thrust, s|… ‑ 3 600|
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|335|
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥| |
|Thrust: Σ thrust, N, ≥| |
|Thrust: Σ time, s(h), ≥| |
|[Turbopump](turbopump.md), rpm|—|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. [Presentation of NASA, 2009 ❐](f/ps/a/ambr_presentation1.pdf)
1. <http://www.rocket.com/propulsion-systems/bipropellant-rockets>
1. [https://www.researchgate.net/publication/277533096](https://www.researchgate.net/publication/277533096_Performance_Results_for_the_Advanced_Materials_Bipropellant_Rocket_AMBR_Engine)
1. **Applicability** — …



### Aerojet HiPAT   ［US・2comp.］
**HiPAT** (High performance liquid apogee thruster) — 2‑component [engine](ps.md) by [Aerojet Rocketdyne](aerojet_rocketdyne.md).

|**Characteristics**|**HiPAT or HiPAT DM**|
|:--|:--|
|Composition| |
|Consumption, W|46 (for 28 W)|
|Dimensions, ㎜|720 × 360|
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|5.2 (300:1 nozzle) or 5.44 (375:1 nozzle)|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[NTO+MMH](nto_plus.md) (HiPAT); [NTO+Hydrazine](nto_plus.md) (H₂,H₂O,NH₃,N₂) (HiPAT DM)|
|Fuel: consump., ㎏/s, ≤|0.141.755 or 0.140.439|
|Fuel: mass ratio|1.0|
|Fuel: resource, ㎏, ≥|6 530|
|Pres.: c.chamber, ㎫(㍴)|0.96 (9.4)|
|Pres.: inlet, ㎫(㍴)|0.91 ‑ 2.81 (8.9 ‑ 27.6)|
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|445|
|Thrust: 1 thrust, s|… ‑ 3 600|
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|320 (300:1 nozzle) or 323 (375:1 nozzle)|
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥|500|
|Thrust: Σ thrust, N, ≥|20 616 500|
|Thrust: Σ time, s(h), ≥|46 330|
|[Turbopump](turbopump.md), rpm|—|
| |![](f/ps/h/hipat_pic1.webp)|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. [Promo specs ❐](f/ps/h/hipat_spec1.webp)
1. <http://www.astronautix.com/h/hipat.html>
1. <http://www.rocket.com/propulsion-systems/bipropellant-rockets>
1. **Applicability** — …



### Aerojet R-1E   ［US・2comp.］
**R-1E** — 2‑component [engine](ps.md) by [Aerojet Rocketdyne](aerojet_rocketdyne.md). Designed in 1981.

|**Characteristics**|**R-1E**|
|:--|:--|
|Composition| |
|Consumption, W|36 (for 28 V)|
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|2|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[NTO+MMH](nto_plus.md)|
|Fuel: consump., ㎏/s, ≤|0.040.410|
|Fuel: mass ratio| |
|Fuel: resource, ㎏, ≥|4 047|
|Pres.: c.chamber, ㎫(㍴)|0.74 (7.3)|
|Pres.: inlet, ㎫(㍴)|0.7 ‑ 2.81 (6.9 ‑ 27.6)|
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|111|
|Thrust: 1 thrust, s|… ‑ 100 180|
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|280|
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥|330 000|
|Thrust: Σ thrust, N, ≥|11 120 000|
|Thrust: Σ time, s(h), ≥|100 180|
|[Turbopump](turbopump.md), rpm|—|
| |![](f/ps/r/r-1e_pic1.webp)|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. [Promo specs ❐](f/ps/r/r-1e_spec1.webp)
1. <http://www.astronautix.com/r/r-1eengine.html>
1. <http://www.rocket.com/propulsion-systems/bipropellant-rockets>
1. **Applicability** — Space Shuttle, Orbiter’s vernier attitude control & orbit adjust thruster



### Aerojet R-4D   ［US・2comp.］
**R-4D** — 2‑component [engine](ps.md) by [Aerojet Rocketdyne](aerojet_rocketdyne.md). Designed in 1966.

|**Characteristics**|**R-4D**|
|:--|:--|
|Composition| |
|Consumption, W|46 (using 28 V)|
|Dimensions, ㎜|550 × 280|
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|3.4 (44:1 nozzle) or 3.76 (164:1 nozzle) or 4.31 (300:1 nozzle)|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[NTO+MMH](nto_plus.md)|
|Fuel: consump., ㎏/s, ≤|0.160.093|
|Fuel: mass ratio|1.65 (1.0 ‑ 2.4)|
|Fuel: resource, ㎏, ≥|6 450|
|Pres.: c.chamber, ㎫(㍴)|0.76 (7.45)|
|Pres.: inlet, ㎫(㍴)|0.41 ‑ 2.93 (4 ‑ 28.74)|
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|490|
|Thrust: 1 thrust, s|… ‑ 3 600|
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|312|
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥|20 700|
|Thrust: Σ thrust, N, ≥|20 016 000|
|Thrust: Σ time, s(h), ≥|40 850|
|[Turbopump](turbopump.md), rpm|—|
| |[![](f/ps/r/r-4d_pic1t.webp)](f/ps/r/r-4d_pic1.webp)|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. [Promo specs ❐](f/ps/r/r-4d_spec1.webp)
1. <https://en.wikipedia.org/wiki/R-4D>
1. <http://www.astronautix.com/r/r-4d.html>
1. **Applicability** — Apollo・ Cassini・ H-II Transfer Vehicle・ Orion・ Space Shuttle



### Aerojet R-6   ［US・2comp.］
**R-6** — 2‑component [engine](ps.md) by [Aerojet Rocketdyne](aerojet_rocketdyne.md).

|**Characteristics**|**R-6**|
|:--|:--|
|Composition| |
|Consumption, W| |
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|0.45|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[NTO+MMH](nto_plus.md)|
|Fuel: consump., ㎏/s, ≤| |
|Fuel: mass ratio| |
|Fuel: resource, ㎏, ≥| |
|Pres.: c.chamber, ㎫(㍴)| |
|Pres.: inlet, ㎫(㍴)| |
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|22|
|Thrust: 1 thrust, s|… ‑ … |
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥| |
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥| |
|Thrust: Σ thrust, N, ≥| |
|Thrust: Σ time, s(h), ≥| |
|[Turbopump](turbopump.md), rpm|—|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. <http://www.rocket.com/propulsion-systems/bipropellant-rockets>
1. **Applicability** — …



### Aerojet R-40   ［US・2comp.］
**R-40** — 2‑component [engine](ps.md) by [Aerojet Rocketdyne](aerojet_rocketdyne.md). Designed in 1981.

|**Characteristics**|**R-40A**|**R-40B**|
|:--|:--|:--|
|Composition| | |
|Consumption, W| |70 (for 28 V)|
|Dimensions, ㎜|1 040 × 520|710 × 400|
|[Interfaces](interface.md)| | |
|[Lifetime](lifetime.md), h(y)|… / …|… / …|
|Mass, ㎏|10|6.8|
|[Overload](vibration.md), Grms| | |
|[Radiation](ion_rad.md), ㏉(㎭)| | |
|[Reliability](qm.md)| | |
|[Thermal](tcs.md), ℃| | |
|[TRL](trl.md)|9|9|
|[Voltage](sps.md), V| | |
|**【Specific】**|~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[NTO+MMH](nto_plus.md)|[NTO+MMH](nto_plus.md)|
|Fuel: consump., ㎏/s, ≤|1.037.356 ‑ 1.289.200 ‑ 1.778.230|1.391.628|
|Fuel: mass ratio|1.6| |
|Fuel: resource, ㎏, ≥|20 800|3 220|
|Pres.: c.chamber, ㎫(㍴)|1.07 (10.5)|1.05 (10.34)|
|Pres.: inlet, ㎫(㍴)| |1.05 ‑ 2.81 (10.3 ‑ 27.6)|
|Pres.: nozzle cut, ㎫(㍴)| | |
|T°: comb.chamber, К(℃)| | |
|T°: nozzle cut, К(℃)| | |
|Thrust, N|3 114 ‑ 3 870 ‑ 5 338|4 000|
|Thrust: 1 thrust, s|… ‑ 500|… ‑ 23 000|
|Thrust: [Ing](ing.md), N·s, ≤| | |
|Thrust: [Isp](ps.md), s, ≥|306|293|
|Thrust: switch.rate, ㎐, ≤| | |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥| |50 000|
|Thrust: Σ thrust, N, ≥|59 211 000|92 073 600|
|Thrust: Σ time, s(h), ≥|15 300|23 000|
|[Turbopump](turbopump.md), rpm|—|—|
| | |[![](f/ps/r/r-40b_pic1t.webp)](f/ps/r/r-40b_pic1.webp)|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. [Промо‑спецификации ❐](f/ps/r/r-40_spec1.webp)
1. <http://www.astronautix.com/r/r-40a.html>
1. <http://www.astronautix.com/r/r-40b.html>
1. <http://www.rocket.com/propulsion-systems/bipropellant-rockets>
1. **Applicability** — Shuttle Orbiter orbit control (R-40A)



### Aerojet R-42   ［US・2comp.］
**R-42** — 2‑component [engine](ps.md) by [Aerojet Rocketdyne](aerojet_rocketdyne.md).

|**Characteristics**|**R-42**|**R-42DM**|
|:--|:--|:--|
|Composition| | |
|Consumption, W|46 (for 28 V)|46 (for 28 V)|
|Dimensions, ㎜|790 × 390|790 × 390|
|[Interfaces](interface.md)| | |
|[Lifetime](lifetime.md), h(y)|… / …|… / …|
|Mass, ㎏|4.53|4.53|
|[Overload](vibration.md), Grms| | |
|[Radiation](ion_rad.md), ㏉(㎭)| | |
|[Reliability](qm.md)| | |
|[Thermal](tcs.md), ℃| | |
|[TRL](trl.md)|9|9|
|[Voltage](sps.md), V| | |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[NTO+MMH](nto_plus.md)|[NTO+Hydrazine](nto_plus.md) (H₂,H₂O,NH₃,N₂)|
|Fuel: consump., ㎏/s, ≤|0.299.418|0.299.418|
|Fuel: mass ratio| | |
|Fuel: resource, ㎏, ≥|8 100|8 100|
|Pres.: c.chamber, ㎫(㍴)|0.72 (7.1)|0.72 (7.1)|
|Pres.: inlet, ㎫(㍴)|0.7 ‑ 2.98 (6.9 ‑ 29.3)|0.7 ‑ 2.98 (6.9 ‑ 29.3)|
|Pres.: nozzle cut, ㎫(㍴)| | |
|T°: comb.chamber, К(℃)| | |
|T°: nozzle cut, К(℃)| | |
|Thrust, N|890|890|
|Thrust: 1 thrust, s|… ‑ 3 940|… ‑ 3 940|
|Thrust: [Ing](ing.md), N·s, ≤| | |
|Thrust: [Isp](ps.md), s, ≥|303|303|
|Thrust: switch.rate, ㎐, ≤| | |
|Thrust: torch angle, °| | |
|Thrust: Σ pulses, ≥|134|134|
|Thrust: Σ thrust, N, ≥|24 271 000|24 271 000|
|Thrust: Σ time, s(h), ≥|27 000|27 000|
|[Turbopump](turbopump.md), rpm|—|—|
| |[![](f/ps/r/r-42_pic1t.webp)](f/ps/r/r-42_pic1.webp)|[![](f/ps/r/r-42_pic1t.webp)](f/ps/r/r-42_pic1.webp)|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. [Промо‑спецификации ❐](f/ps/r/r-42_spec1.webp)
1. <http://www.astronautix.com/r/r-42.html>
1. <http://www.rocket.com/propulsion-systems/bipropellant-rockets>
1. **Applicability** — …



### Aerojet RS-25   ［US・2comp.］
**RS-25** or **SSME** (Space Shuttle Main Engine) — 2‑component [engine](ps.md) by [Aerojet Rocketdyne](aerojet_rocketdyne.md). Designed in 1972.

|**Characteristics**|**RS-25**|
|:--|:--|
|Composition| |
|Consumption, W| |
|Dimensions, ㎜|4 240 × 2 400|
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|3 390|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[O+H](o_plus.md) (H₂,H₂O)|
|Fuel: consump., ㎏/s, ≤|493.5|
|Fuel: mass ratio| |
|Fuel: resource, ㎏, ≥| |
|Pres.: c.chamber, ㎫(㍴)|18.9 (192.7)|
|Pres.: inlet, ㎫(㍴)| |
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)|3 573 (3 300)|
|T°: nozzle cut, К(℃)| |
|Thrust, N|1 192 250 ‑ 1 779 500 ‑ 1 939 650 sea lvl;<br> 1 462 600 ‑ 2 183 000 ‑ 2 379 500 vacuum|
|Thrust: 1 thrust, s|… ‑ … |
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|363 sea lvl; 453 vacuum|
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥| |
|Thrust: Σ thrust, N, ≥| |
|Thrust: Σ time, s(h), ≥|520|
|[Turbopump](turbopump.md), rpm| |
| |[![](f/ps/r/rs-25t.webp)](f/ps/r/rs-25.webp)|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. <http://www.astronautix.com/s/ssme.html>
1. <https://en.wikipedia.org/wiki/Space_Shuttle_main_engine>
1. **Applicability** — …



### Ariane 1NMHT   ［EU・1comp.］
**1NMHT** — 1‑component [engine](ps.md) by [OPC LAM](opc_lam.md). Designed in 1996.

|**Characteristics**|**1NMHT**|
|:--|:--|
|Composition| |
|Consumption, W| |
|Dimensions, ㎜|172 × ⌀30|
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|0.29 w/ valve|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[Hydrazine](hydrazine.md) (H₂,N₂)|
|Fuel: consump., ㎏/s, ≤|0.000.163.098 ‑ … ‑ 0.000.502.827|
|Fuel: mass ratio|—|
|Fuel: resource, ㎏, ≥|67|
|Pres.: c.chamber, ㎫(㍴)| |
|Pres.: inlet, ㎫(㍴)|0.56 ‑ 2.24 (5.5 ‑ 22)|
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|0.32 ‑ 1 ‑ 1.1|
|Thrust: 1 thrust, s|… ‑ 43 200|
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|200 ‑ … ‑ 223|
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥|59 000|
|Thrust: Σ thrust, N, ≥|135 000|
|Thrust: Σ time, s(h), ≥|180 000|
|[Turbopump](turbopump.md), rpm|—|
| |[![](f/ps/0/1nmht_pic1t.webp)](f/ps/0/1nmht_pic1.webp)|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. [Products brochures ❐](f/c/o/opc_lam_brochures.7z)
1. <http://www.space-propulsion.com/spacecraft-propulsion/hydrazine-thrusters/1n-hydrazine-thruster.html> — [archived ❐](f/ps/0/1nmht_site.pdf) 2019.02.21
1. **Applicability** — CSO-3 (…)・ CSO-2 (…)・ CSO-1 (2016)・ Sentinel 2B (2016)・ Sentinel 5P (2016)・ SEOSAT (Ingenio) (2015)・ Sentinel 2A (2015)・ SEOSAR (Paz) (2015)・ Taranis (2015)・ AstroTerra 2 (2014)・ KRS (2014)・ Jason-3 (2013)・ Vietnam-1 (2013)・ AstroTerra 1 (2012)・ Pleiades HR2 (2012)・ Elisa-1 (2011)・ Elisa-2 (2011)・ Elisa-3 (2011)・ Elisa-4 (2011)・ Pleiades HR1 (2011)・ SSOT (2011)・ Alsat 2A (2010)・ Alsat 2B (2010)・ CosmoSkymed-4 (2010)・ TANDEM-X (2010)



### Ariane 20NMHT   ［EU・1comp.］
**20NMHT** — 1‑component [engine](ps.md) by [OPC LAM](opc_lam.md). Designed in 1982.

|**Characteristics**|**20NMHT**|
|:--|:--|
|Composition| |
|Consumption, W| |
|Dimensions, ㎜|195 × ⌀33|
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|0.65 w/ 1.5m flying leads |
|[Overload](vibration.md), Grms|16.2|
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[Hydrazine](hydrazine.md) (H₂,N₂)|
|Fuel: consump., ㎏/s, ≤|0.003.627.480 ‑ 0.010.902.805|
|Fuel: mass ratio|—|
|Fuel: resource, ㎏, ≥|290|
|Pres.: c.chamber, ㎫(㍴)| |
|Pres.: inlet, ㎫(㍴)|0.56 ‑ 2.44 (5.5 ‑ 24)|
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|7.9 ‑ 24.6|
|Thrust: 1 thrust, s|… ‑ 5 400|
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|222 ‑ 230|
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥|93 130|
|Thrust: Σ thrust, N, ≥|517 000|
|Thrust: Σ time, s(h), ≥|37 800|
|[Turbopump](turbopump.md), rpm|—|
| |[![](f/ps/0/20nmht_pic1t.webp)](f/ps/0/20nmht_pic1.webp)|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. [Products brochures ❐](f/c/o/opc_lam_brochures.7z)
1. <http://www.space-propulsion.com/spacecraft-propulsion/hydrazine-thrusters/20n-hydrazine-thruster.html> — [archived ❐](f/ps/0/20nmht_site.pdf) 2019.02.21
1. **Applicability** — NGSAR (2018 ‑ 2019)・ Herschel (2009)・ Planck (2009)・ METOP 1-3 (2006‑2018)・ Integral (2002)



### Ariane 400NMHT   ［EU・1comp.］
**400NMHT** — 1‑component [engine](ps.md) by [OPC LAM](opc_lam.md). Designed in 1996.

|**Characteristics**|**400NMHT**|
|:--|:--|
|Composition| |
|Consumption, W| |
|Dimensions, ㎜|325 × ⌀67|
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|2.7 or 3.8|
|[Overload](vibration.md), Grms|16.2|
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[Hydrazine](hydrazine.md) (H₂,N₂)|
|Fuel: consump., ㎏/s, ≤|0.057.700 ‑ 0.194.606|
|Fuel: mass ratio|—|
|Fuel: resource, ㎏, ≥|300|
|Pres.: c.chamber, ㎫(㍴)| |
|Pres.: inlet, ㎫(㍴)|0.56 ‑ 2.65 (5.5 ‑ 26)|
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|120 ‑ 420|
|Thrust: 1 thrust, s|… ‑ 450|
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|212 ‑ 220|
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥|3 900|
|Thrust: Σ thrust, N, ≥|188 000|
|Thrust: Σ time, s(h), ≥|850|
|[Turbopump](turbopump.md), rpm|—|
| |[![](f/ps/0/400nmht_pic1t.webp)](f/ps/0/400nmht_pic1.webp)|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. [Products brochures ❐](f/c/o/opc_lam_brochures.7z)
1. <http://www.space-propulsion.com/spacecraft-propulsion/hydrazine-thrusters/400n-hydrazine-thruster.html> — [archived ❐](f/ps/0/400nmht_site.pdf) 2019.02.21
1. **Applicability** — Ariane 5 G, GS & ES versions for the roll & attitude control・ ExoMars-2016 DM



### Ariane RIT 2X   ［EU・el.］
**RIT 2X** — electric [engine](ps.md) by [OPC LAM](opc_lam.md).

|**Characteristics**|**RIT 2X**|**RIT 2X**|**RIT 2X**|
|:--|:--|:--|:--|
|Composition| | | |
|Consumption, W|2 000 ‑ 2 500|4 000 ‑ 4 500|4 800 ‑ 5 300|
|Dimensions, ㎜|220 × ⌀330|220 × ⌀330|220 × ⌀330|
|[Interfaces](interface.md)| | | |
|[Lifetime](lifetime.md), h(y)|… / …|… / …|… / …|
|Mass, ㎏|10|10|10|
|[Overload](vibration.md), Grms|8.1|8.1|8.1|
|[Radiation](ion_rad.md), ㏉(㎭)| | | |
|[Reliability](qm.md)| | | |
|[Thermal](tcs.md), ℃|−50 ‑ +190 for operation,<br> −60 ‑ +190 for storage|−50 ‑ +190 for operation,<br> −60 ‑ +190 for storage|−50 ‑ +190 for operation,<br> −60 ‑ +190 for storage|
|[TRL](trl.md)|9|9|9|
|[Voltage](sps.md), V| | | |
|**【Specific】**|~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[Xenon](xenon.md)|[Xenon](xenon.md)|[Xenon](xenon.md)|
|Fuel: consump., ㎏/s, ≤|0.000.002.098 ‑ <br>0.000.002.562|0.000.004.664 ‑ <br>0.000.004.980|0.000.008.238 ‑ <br>0.000.007.969|
|Fuel: mass ratio|—|—|—|
|Fuel: resource, ㎏, ≥|151.1 ‑ 184.53|335.83 ‑ 358.58|593.14 ‑ 573.81|
|Pres.: c.chamber, ㎫(㍴)| | | |
|Pres.: inlet, ㎫(㍴)| | | |
|Pres.: nozzle cut, ㎫(㍴)| | | |
|T°: comb.chamber, К(℃)| | | |
|T°: nozzle cut, К(℃)| | | |
|Thrust, N|0.070 ‑ 0.088|0.151 ‑ 0.171|0.198 ‑ 0.215|
|Thrust: 1 thrust, s|… ‑ … | | |
|Thrust: [Ing](ing.md), N·s, ≤| | | |
|Thrust: [Isp](ps.md), s, ≥|3 400 ‑ 3 500|3 300 ‑ 3 500|2 450 ‑ 2 750|
|Thrust: switch.rate, ㎐, ≤| | | |
|Thrust: torch angle, °|25|25|25|
|Thrust: Σ pulses, ≥|10 000|10 000|10 000|
|Thrust: Σ thrust, N, ≥|5 040 000 ‑ 6 335 000|10 870 000 ‑ 12 310 000|14 256 000 ‑ 15 480 000|
|Thrust: Σ time, s(h), ≥|72 000 000 (20 000 h / 2.3 y)|72 000 000 (20 000 h / 2.3 y)|72 000 000 (20 000 h / 2.3 y)|
|[Turbopump](turbopump.md), rpm|—|—|—|
| |[![](f/ps/r/rit_2x_ion_thrustert.webp)](f/ps/r/rit_2x_ion_thruster.webp)|[![](f/ps/r/rit_2x_ion_thrustert.webp)](f/ps/r/rit_2x_ion_thruster.webp)|[![](f/ps/r/rit_2x_ion_thrustert.webp)](f/ps/r/rit_2x_ion_thruster.webp)|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. <http://www.space-propulsion.com/spacecraft-propulsion/propulsion-systems/electric-propulsion/index.html>
1. **Applicability** — …



### Ariane RIT‑10   ［EU・el.］
**RIT 10 EVO** — electric [engine](ps.md) by [OPC LAM](opc_lam.md).

|**Characteristics**|**RIT‑10**|**RIT‑10**|**RIT‑10**|
|:--|:--|:--|:--|
|Composition| | | |
|Consumption, W|145|435|760|
|Dimensions, ㎜|134 × ⌀186|134 × ⌀186|134 × ⌀186|
|[Interfaces](interface.md)| | | |
|[Lifetime](lifetime.md), h(y)|… / …|… / …|… / …|
|Mass, ㎏|1.8|1.8|1.8|
|[Overload](vibration.md), Grms|22.9|22.9|22.9|
|[Radiation](ion_rad.md), ㏉(㎭)| | | |
|[Reliability](qm.md)| | | |
|[Thermal](tcs.md), ℃|−75 ‑ +140 for operation,<br> −85 ‑ +140 for storage|−75 ‑ +140 for operation,<br> −85 ‑ +140 for storage|−75 ‑ +140 for operation,<br> −85 ‑ +140 for storage|
|[TRL](trl.md)|9|9|9|
|[Voltage](sps.md), V| | | |
|**【Specific】**|~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[Xenon](xenon.md)|[Xenon](xenon.md)|[Xenon](xenon.md)|
|Fuel: consump., ㎏/s, ≤|0.000.000.268.254|0.000.000.509.683|0.000.000.796.381|
|Fuel: mass ratio|—|—|—|
|Fuel: resource, ㎏, ≥|19.31|36.69|57.34|
|Pres.: c.chamber, ㎫(㍴)| | | |
|Pres.: inlet, ㎫(㍴)| | | |
|Pres.: nozzle cut, ㎫(㍴)| | | |
|T°: comb.chamber, К(℃)| | | |
|T°: nozzle cut, К(℃)| | | |
|Thrust, N|0.005|0.015|0.025|
|Thrust: 1 thrust, s|… ‑ … | | |
|Thrust: [Ing](ing.md), N·s, ≤| | | |
|Thrust: [Isp](ps.md), s, ≥|1 900|3 000|3 200|
|Thrust: switch.rate, ㎐, ≤| | | |
|Thrust: torch angle, °|15|15|15|
|Thrust: Σ pulses, ≥|10 000|10 000|10 000|
|Thrust: Σ thrust, N, ≥|360 000|1 080 000|1 800 000|
|Thrust: Σ time, s(h), ≥|72 000 000 (20 000 h / 2.3 y)|72 000 000 (20 000 h / 2.3 y)|72 000 000 (20 000 h / 2.3 y)|
|[Turbopump](turbopump.md), rpm|—|—|—|
| |[![](f/ps/r/rit_10_ion_thrustert.webp)](f/ps/r/rit_10_ion_thruster.webp)|[![](f/ps/r/rit_10_ion_thrustert.webp)](f/ps/r/rit_10_ion_thruster.webp)|[![](f/ps/r/rit_10_ion_thrustert.webp)](f/ps/r/rit_10_ion_thruster.webp)|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. <http://www.space-propulsion.com/spacecraft-propulsion/propulsion-systems/electric-propulsion/index.html>
1. **Applicability** — …



### Ariane RIT µX   ［EU・el.］
**RIT µX** — electric [engine](ps.md) by [OPC LAM](opc_lam.md).

|**Characteristics**|**RIT µX**|
|:--|:--|
|Composition| |
|Consumption, W|50|
|Dimensions, ㎜|76 × ⌀78|
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|0.44|
|[Overload](vibration.md), Grms|18.4|
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃|−40 ‑ +160 for operation, −60 ‑ +160 for storage|
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[Xenon](xenon.md)|
|Fuel: consump., ㎏/s, ≤|0.000.000.016.989 ‑ 0.000.000.016.989|
|Fuel: mass ratio|—|
|Fuel: resource, ㎏, ≥|1.223|
|Pres.: c.chamber, ㎫(㍴)| |
|Pres.: inlet, ㎫(㍴)| |
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|0.00005 ‑ 0.0005|
|Thrust: 1 thrust, s|… ‑ … |
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|300 ‑ 3 000|
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °|17|
|Thrust: Σ pulses, ≥|10 000|
|Thrust: Σ thrust, N, ≥|3 600 ‑ 36 000|
|Thrust: Σ time, s(h), ≥|72 000 000 (20 000 h / 2.3 y)|
|[Turbopump](turbopump.md), rpm|—|
| |[![](f/ps/r/rit_micro_ion_thrustert.webp)](f/ps/r/rit_micro_ion_thruster.webp)|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. <http://www.space-propulsion.com/spacecraft-propulsion/propulsion-systems/electric-propulsion/index.html>
1. **Applicability** — …



### Ariane S10   ［EU・2comp.］
**S10** — 2‑component [engine](ps.md) by [OPC LAM](opc_lam.md). Designed in 1975.

|**Characteristics**|**S10‑13**|**S10‑18**|**S10‑26**|
|:--|:--|:--|:--|
|Composition| | | |
|Consumption, W|24|24|24|
|Dimensions, ㎜| | | |
|[Interfaces](interface.md)| | | |
|[Lifetime](lifetime.md), h(y)|… / …|… / …|… / …|
|Mass, ㎏|0.35 (SST), 0.65 (DST)|0.35 (SST), 0.65 (DST)|0.35 (SST), 0.65 (DST)|
|[Overload](vibration.md), Grms| | | |
|[Radiation](ion_rad.md), ㏉(㎭)| | | |
|[Reliability](qm.md)| | | |
|[Thermal](tcs.md), ℃| | | |
|[TRL](trl.md)|9|9|9|
|[Voltage](sps.md), V| | | |
|**【Specific】**|~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[NTO (MON‑1, ‑3) + MMH](nto_plus.md)|[NTO (MON‑1, ‑3) + MMH](nto_plus.md)|[NTO (MON‑1, ‑3) + MMH](nto_plus.md)|
|Fuel: consump., ㎏/s, ≤|0.003.490 (0.0023 ‑ 0.0042)|0.003.490 (0.0023 ‑ 0.0042)|0.003.490 (0.0023 ‑ 0.0042)|
|Fuel: mass ratio|1.65 (1.2 ‑ 2.1)|1.65 (1.2 ‑ 2.1)|1.65 (1.2 ‑ 2.1)|
|Fuel: resource, ㎏, ≥|870|870|870|
|Pres.: c.chamber, ㎫(㍴)|0.91 (9)|0.91 (9)|0.91 (9)|
|Pres.: inlet, ㎫(㍴)|1.01 ‑ 2.34 (10 ‑ 23)|1.01 ‑ 2.34 (10 ‑ 23)|1.01 ‑ 2.34 (10 ‑ 23)|
|Pres.: nozzle cut, ㎫(㍴)| | | |
|T°: comb.chamber, К(℃)| | | |
|T°: nozzle cut, К(℃)| | | |
|Thrust, N|6 ‑ 10 ‑ 12.5|6 ‑ 10 ‑ 12.5|6 ‑ 10 ‑ 12.5|
|Thrust: 1 thrust, s|… ‑ 28 800|… ‑ 28 800|… ‑ 28 800|
|Thrust: [Ing](ing.md), N·s, ≤| | | |
|Thrust: [Isp](ps.md), s, ≥|292|292|292|
|Thrust: switch.rate, ㎐, ≤| | | |
|Thrust: torch angle, °| | | |
|Thrust: Σ pulses, ≥|1 000 000|1 000 000|1 000 000|
|Thrust: Σ thrust, N, ≥|2 484 000|2 484 000|2 484 000|
|Thrust: Σ time, s(h), ≥|248 400|248 400|248 400|
|[Turbopump](turbopump.md), rpm|—|—|—|
| |[![](f/ps/s/s10-13_pic1t.webp)](f/ps/s/s10-13_pic1.webp)|[![](f/ps/s/s10-18_pic1t.webp)](f/ps/s/s10-18_pic1.webp)|[![](f/ps/s/s10-26_pic1t.webp)](f/ps/s/s10-26_pic1.webp)|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. [Products brochures ❐](f/c/o/opc_lam_brochures.7z)
1. <http://www.space-propulsion.com/spacecraft-propulsion/bipropellant-thrusters/10-bipropellant-thrusters.html> — [archived ❐](f/ps/s/s10_site.pdf) 2019.02.21
1. Modifications:
   1. **S10-13** w/ single seat, monostable, torque motor valve from a US supplier.
   1. **S10-18** w/ dual seat valve, comprising an upstream latch valve & a downstream monostable valve from a US supplier.
   1. **S10-26** w/ dual seat valve, comprising an upstream latch valve & a downstream monostable valve supplied by ArianeGroup, DE.
1. **[1]** — see <mark>TBD</mark>
1. **Applicability** — …



### Ariane S200   ［EU・2comp.］
**S200** — 2‑component [engine](ps.md) by [OPC LAM](opc_lam.md).

|**Characteristics**|**S200**|
|:--|:--|
|Composition| |
|Consumption, W|32|
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|1.9|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|MON-3 + [MMH](mmh.md) or [NTO + UDMH](nto_plus.md) (H₂,H₂O,CO,CO₂,N₂)|
|Fuel: consump., ㎏/s, ≤|0.067.957 ‑ 0.081.549 ‑ 0.101.936|
|Fuel: mass ratio|1.65 (1.2 ‑ 1.9)|
|Fuel: resource, ㎏, ≥|3 600|
|Pres.: c.chamber, ㎫(㍴)|0.815 (8)|
|Pres.: inlet, ㎫(㍴)|1.32 ‑ 2.45 (13 ‑ 24)|
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)|1 420 (+1 150)|
|T°: nozzle cut, К(℃)|1 070 (800)|
|Thrust, N|180±15 ‑ 216±10 ‑ 270±15|
|Thrust: 1 thrust, s|… ‑ 11 400|
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|270|
|Thrust: switch.rate, ㎐, ≤|5|
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥|270 000|
|Thrust: Σ thrust, N, ≥|10 040 000|
|Thrust: Σ time, s(h), ≥|46 500|
|[Turbopump](turbopump.md), rpm|—|
| |[![](f/ps/s/s200_pic1t.webp)](f/ps/s/s200_pic1.webp)|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. [Products brochures ❐](f/c/o/opc_lam_brochures.7z)
1. <http://www.space-propulsion.com/spacecraft-propulsion/bipropellant-thrusters/200n-bipropellant-thrusters.html> — [archived ❐](f/ps/s/s200_site.pdf) 2019.02.21
1. **Applicability** — …



### Ariane S400   ［EU・2comp.］
**S400** — 2‑component [engine](ps.md) by [OPC LAM](opc_lam.md).

|**Characteristics**|**S400‑12**|**S400‑15**|
|:--|:--|:--|
|Composition| | |
|Consumption, W|35|35|
|Dimensions, ㎜|503 × 248|669 × 316|
|[Interfaces](interface.md)| | |
|[Lifetime](lifetime.md), h(y)|… / …|… / …|
|Mass, ㎏|3.6|4.3|
|[Overload](vibration.md), Grms| | |
|[Radiation](ion_rad.md), ㏉(㎭)| | |
|[Reliability](qm.md)| | |
|[Thermal](tcs.md), ℃| | |
|[TRL](trl.md)|9|9|
|[Voltage](sps.md), V| | |
|**【Specific】**|~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[NTO (MON‑1, MON‑3) + MMH](nto_plus.md)|[NTO (MON‑1, MON‑3) + MMH](nto_plus.md)|
|Fuel: consump., ㎏/s, ≤|0.108.989 ‑ 0.134.633 ‑ 0.141.044|0.107.970 ‑ 0.134.963 ‑ 0.139.726|
|Fuel: mass ratio|1.65 (1.5 ‑ 1.8)|1.65 (1.5 ‑ 1.8)|
|Fuel: resource, ㎏, ≥|4 000|4 100|
|Pres.: c.chamber, ㎫(㍴)|1.02 (10)|1.02 (10)|
|Pres.: inlet, ㎫(㍴)|1.27 ‑ 1.88 (12.5 ‑ 18.5)|1.27 ‑ 1.88 (12.5 ‑ 18.5)|
|Pres.: nozzle cut, ㎫(㍴)| | |
|T°: comb.chamber, К(℃)| | |
|T°: nozzle cut, К(℃)| | |
|Thrust, N|340 ‑ 420 ‑ 440|340 ‑ 425 ‑ 440|
|Thrust: 1 thrust, s|… ‑ 4 000|… ‑ 6 700|
|Thrust: [Ing](ing.md), N·s, ≤| | |
|Thrust: [Isp](ps.md), s, ≥|318|321|
|Thrust: switch.rate, ㎐, ≤| | |
|Thrust: torch angle, °| | |
|Thrust: Σ pulses, ≥|100|135|
|Thrust: Σ thrust, N, ≥|12 550 000|13 000 000|
|Thrust: Σ time, s(h), ≥|29 900|30 600|
|[Turbopump](turbopump.md), rpm|—|—|
| |[![](f/ps/s/s400-12_pic1t.webp)](f/ps/s/s400-12_pic1.webp)|[![](f/ps/s/s400-15_pic1t.webp)](f/ps/s/s400-15_pic1.webp)|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. [Products brochures ❐](f/c/o/opc_lam_brochures.7z)
1. <https://en.wikipedia.org/wiki/S400_(rocket_engine)>
1. <http://www.space-propulsion.com/spacecraft-propulsion/apogee-motors/index.html>
1. **[1]** — see <mark>TBD</mark>
1. **Applicability** — Symphonie 1 (1974)・ Symphonie 2 (1975)・ TV-Sat 1 (1987)・ TDF-1 (1988)・ Galileo (1989)・ Tele X (1989)・ DFS Kopernikus 1 (1989)・ TV-Sat 2 (1989)・ DFS Kopernikus 2 (1989)・ TDF-2 (1990)・ Eutelsat 2-F1 (1990)・ Eutelsat 2-F2 (1991)・ Eutelsat 2-F3 (1991)・ Arabsat 1C (1992)・ Eutelsat 2-F4 (1992)・ DFS Kopernikus 3 (1992)・ Eutelsat 2-F5 (1994)・ Turksat 1A (1994)・ Turksat 1AR (1994)・ Turksat 1B (1994)・ HotBird 1 (1995)・ Amos 1 (1995)・ Arabsat 2A (1996)・ Turksat 1C (1996)・ Arabsat 2B (1996)・ Nahuel 1A (1997)・ Thaicom 3 (1997)・ Sirius 2 FM1 (1997)・ Sinosat (1998)・ Eutelsat W2 (1998)・ GE 5 (1998)・ Arabsat 3A (1999)・ Eutelsat W3 (1999)・ Hispasat 1C (2000)・ Eutelsat W4 (2000)・ Cluster FM 6・ FM 7 (2000)・ Cluster FM 5・ FM 8 (2000)・ Amsat P3D (2000)・ Eurasiasat (2001)・ Sicral (2001)・ Eurobird (2001)・ Artemis (2001)・ Atlantic Bird 2 (2001)・ Atlantic Bird 3 (2002)・ Atlantic Bird 1 (2002)・ HotBird 6 (2002)・ Meteosat SG 1 (2002)・ Hispasat 1D (2002)・ Eutelsat W5 (2002)・ Astra 1K (2002)・ Mars Express (2003)・ AMC 9 (2003)・ Amos 2 (2003)・ Apstar 6 (2005)・ AMC 12 (2005)・ AMC 13 (2005)・ MSG FM2 (2005)・ Syracruse 3A (2005)・ Venus Express (2005)・ HB7A・ APA2 (2006)・ Koreasat 5 (2006)・ Syracuse 3B FM2 (2006)・ THAICOM 5 (2006)・ Chinasat 6B (2007)・ Galaxy 17 (2007)・ Rascom RC1 (2007)・ Star One C1 (2007)・ Chinasat 9 (2008)・ CIEL 2 (2008)・ Star One C2 (2008)・ Turksat 3A (2008)・ Eutelsat W3A (2009)・ Eutelsat W7 (2009)・ Comsat-Bw 1 (2009)・ Palapa D (2009)・ SICRAL2 (1B) (2009)・ Thor-6 (2009)・ Eutelsat W3B (2010)・ Comsat-Bw 2 (2010)・ Nilesat 201 (2010)・ RASCOM QAF 1R (2010)・ Eutelsat W3C (2011)・ MSG FM3 (2011)・ Apstar 7A (2012)・ Apstar 7B (2012)・ Eutelsat W6A (2012)・ Alphasat (2013)・ MSG FM4 (2013)・ AMOS 4 (2013)・ ARSAT (2014)・ Athena Fidus (2014)・ MSG FM4 (2015)・ ARSAT 2 (2015)・ E8WB (2015)・ Türkmensat (2015)・ Sicral 2 (2015)・ Exomars Orbiter (2016)・ Amos 6 (LV failure)・ EDRS (2016)・ SGDC (2017)・ Koreasat 7 (2017)・ Koreasat 5A (2017)・ Telkom 3S (2017)・ Inmarsat HS3 (2018)・ KARI GK2A (2018)・ KARI GK2B ( )・ BS1 (2018)・ Quantum (2019)・ MTG I1 (2020)・ Egyptsat (2019)・ Yamal 601 (2019)・ MTG-S1 ()



### AvantSpace GT-50   ［RU・el.］
**GT-50** — electric ion [engine](ps.md) by [Avantspace](avantspace.md).

|**Characteristics**|**GT-50**|**GT-50**|
|:--|:--|:--|
|Composition|engine, tanks, structures<br> (in fact, entire PS)|engine, tanks, structures<br> (in fact, entire PS)|
|Consumption, W|180|240|
|Dimensions, ㎜|100 × 200 × 300|100 × 200 × 300|
|[Interfaces](interface.md)| | |
|[Lifetime](lifetime.md), h(y)|20 000 (2.28) / …|20 000 (2.28) / …|
|Mass, ㎏|8|8|
|[Overload](vibration.md), Grms| | |
|[Radiation](ion_rad.md), ㏉(㎭)| | |
|[Reliability](qm.md)| | |
|[Thermal](tcs.md), ℃| | |
|[TRL](trl.md)|4|4|
|[Voltage](sps.md), V| | |
|**【Specific】**|~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[Xenon](xenon.md)|[Xenon](xenon.md)|
|Fuel: consump., ㎏/s, ≤|0.000.000.254.841|0.000.000.356.778|
|Fuel: mass ratio| | |
|Fuel: resource, ㎏, ≥| | |
|Pres.: c.chamber, ㎫(㍴)| | |
|Pres.: inlet, ㎫(㍴)| | |
|Pres.: nozzle cut, ㎫(㍴)| | |
|T°: comb.chamber, К(℃)| | |
|T°: nozzle cut, К(℃)| | |
|Thrust, N|0.005|0.007|
|Thrust: 1 thrust, s|… ‑ … | |
|Thrust: [Ing](ing.md), N·s, ≤| | |
|Thrust: [Isp](ps.md), s, ≥|2 000|2 000|
|Thrust: switch.rate, ㎐, ≤| | |
|Thrust: torch angle, °| | |
|Thrust: Σ pulses, ≥| | |
|Thrust: Σ thrust, N, ≥| | |
|Thrust: Σ time, s(h), ≥| | |
|[Turbopump](turbopump.md), rpm|—|—|
|[![](f/ps/g/gt_50_pic02t.webp)](f/ps/g/gt_50_pic02.webp) [![](f/ps/g/gt_50_pic03t.webp)](f/ps/g/gt_50_pic03.webp)|[![](f/ps/g/gt_50_pic01t.webp)](f/ps/g/gt_50_pic01.webp)|[![](f/ps/g/gt_50_pic01t.webp)](f/ps/g/gt_50_pic01.webp)|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. [A bunch of presentations](f/ps/g/gt_50_doc01.pdf)
1. <http://www.avantspace.com>
1. <https://habr.com/ru/post/484364>
1. **[1]** — see site [Avantspace](avantspace.md), 2020 y.
1. **Applicability** — …



### Blue Origin BE-3   ［US・2comp.］
**BE-3** — 2‑component [engine](ps.md) by [Blue Origin](blue_origin.md).

|**Characteristics**|**BE-3**|**BE-3U**|
|:--|:--|:--|
|Composition| | |
|Consumption, W| | |
|Dimensions, ㎜| | |
|[Interfaces](interface.md)| | |
|[Lifetime](lifetime.md), h(y)|… / …|… / …|
|Mass, ㎏| | |
|[Overload](vibration.md), Grms| | |
|[Radiation](ion_rad.md), ㏉(㎭)| | |
|[Reliability](qm.md)| | |
|[Thermal](tcs.md), ℃| | |
|[TRL](trl.md)|9|6|
|[Voltage](sps.md), V| | |
|**【Specific】**|~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[O+H](o_plus.md) (H₂,H₂O)|[O+H](o_plus.md) (H₂,H₂O)|
|Fuel: consump., ㎏/s, ≤| | |
|Fuel: mass ratio| | |
|Fuel: resource, ㎏, ≥| | |
|Pres.: c.chamber, ㎫(㍴)| | |
|Pres.: inlet, ㎫(㍴)| | |
|Pres.: nozzle cut, ㎫(㍴)| | |
|T°: comb.chamber, К(℃)| | |
|T°: nozzle cut, К(℃)| | |
|Thrust, N|93 100 ‑ 490 000|670 000|
|Thrust: 1 thrust, s|… ‑ … | |
|Thrust: [Ing](ing.md), N·s, ≤| | |
|Thrust: [Isp](ps.md), s, ≥| | |
|Thrust: switch.rate, ㎐, ≤| | |
|Thrust: torch angle, °| | |
|Thrust: Σ pulses, ≥| | |
|Thrust: Σ thrust, N, ≥| | |
|Thrust: Σ time, s(h), ≥| | |
|[Turbopump](turbopump.md), rpm| | |

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. <https://en.wikipedia.org/wiki/BE-3>
1. There are 2 variations: **BE-3** — basic version; **BE-3U** — for upper stages operating in vacuum.
1. **Applicability:**
   1. **BE-3** — [New Shepard](new_shepard.md) (from 2015)・ LV [New Glenn](new_glenn.md)・ LV [Vulcan](vulcan.md) (from 2020)
   1. **BE-3U** — …



### Blue Origin BE-4   ［US・2comp.］
**BE‑4** — 2‑component [engine](ps.md) by [Blue Origin](blue_origin.md).

|**Characteristics**|**BE-4**|
|:--|:--|
|Composition| |
|Consumption, W| |
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏| |
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|6|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[O+Methane](o_plus.md) (H₂,H₂O,CO,CO₂)|
|Fuel: consump., ㎏/s, ≤| |
|Fuel: mass ratio| |
|Fuel: resource, ㎏, ≥| |
|Pres.: c.chamber, ㎫(㍴)|13.4 (1.36)|
|Pres.: inlet, ㎫(㍴)| |
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|1 200 000 ‑ 2 400 000|
|Thrust: 1 thrust, s|… ‑ … |
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥| |
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥|25|
|Thrust: Σ thrust, N, ≥| |
|Thrust: Σ time, s(h), ≥| |
|[Turbopump](turbopump.md), rpm| |
|[![](f/ps/be4_m1d_raptor_comparison1t.webp)](f/ps/be4_m1d_raptor_comparison1.webp)<br> **Comparison<br> [BE‑4](engine_lst.md), [Raptor](engine_lst.md), Merlin 1D**| |

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. <https://en.wikipedia.org/wiki/BE-4>
1. **[1]** — на уровне моря.
1. Has a drive, deviates by ±5°.
1. **Applicability** — РН [New Glenn](new_glenn.md)・ РН [Vulcan](vulcan.md) (с 2020 года)



### Blue Origin BE-7   ［US・2comp.］
**BE-7** — 2‑component [engine](ps.md) by [Blue Origin](blue_origin.md).

|**Characteristics**|**BE-7**|
|:--|:--|
|Composition| |
|Consumption, W| |
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏| |
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|3|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[O+H](o_plus.md) (H₂,H₂O)|
|Fuel: consump., ㎏/s, ≤| |
|Fuel: mass ratio| |
|Fuel: resource, ㎏, ≥| |
|Pres.: c.chamber, ㎫(㍴)| |
|Pres.: inlet, ㎫(㍴)| |
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|40 000|
|Thrust: 1 thrust, s|… ‑ … |
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥| |
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥| |
|Thrust: Σ thrust, N, ≥| |
|Thrust: Σ time, s(h), ≥| |
|[Turbopump](turbopump.md), rpm| |
| |[![](f/ps/b/be7_pic1t.webp)](f/ps/b/be7_pic1.webp)  [![](f/ps/b/be7_pic2t.webp)](f/ps/b/be7_pic2.webp)|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. <https://www.blueorigin.com/engines/be-7>
1. **Applicability** — [Blue Moon](blue_moon.md) (from 2024)



### Fakel K10   ［RU・1comp.］
**K10** (ru. **К10**) — 1‑component [engine](ps.md) by [OKB Fakel](edb_fakel.md).

|**Characteristics**|**K10**|
|:--|:--|
|Composition| |
|Consumption, W| |
|Dimensions, ㎜|137×130×44|
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|0.2|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[Hydrazine](hydrazine.md) (H₂,N₂)|
|Fuel: consump., ㎏/s, ≤|0.000.046.334|
|Fuel: mass ratio|—|
|Fuel: resource, ㎏, ≥|20|
|Pres.: c.chamber, ㎫(㍴)| |
|Pres.: inlet, ㎫(㍴)|0.18 (1.8)|
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|0.1|
|Thrust: 1 thrust, s|… ‑ … |
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|220|
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥|70 000|
|Thrust: Σ thrust, N, ≥|44 000|
|Thrust: Σ time, s(h), ≥|440 000|
|[Turbopump](turbopump.md), rpm|—|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 4th second after energizing the solenoid valves.
1. **[1]** — see [OKB Fakel](edb_fakel.md) site, 2016 y.
1. **Applicability** — …



### Fakel K50   ［RU・1comp.］
**K50** (ru. **К50**) — 1‑component [engine](ps.md) by [OKB Fakel](edb_fakel.md).

|**Characteristics**|**К50-10.1**|
|:--|:--|
|Composition| |
|Consumption, W| |
|Dimensions, ㎜|112×70×38|
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|0.46|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[Hydrazine](hydrazine.md) (H₂,N₂)|
|Fuel: consump., ㎏/s, ≤|0.000.250.208|
|Fuel: mass ratio|—|
|Fuel: resource, ㎏, ≥|20|
|Pres.: c.chamber, ㎫(㍴)| |
|Pres.: inlet, ㎫(㍴)|0.8 (8)|
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|0.54|
|Thrust: 1 thrust, s|… ‑ 10 800|
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|220|
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥|130 000|
|Thrust: Σ thrust, N, ≥|47 500|
|Thrust: Σ time, s(h), ≥|88 000|
|[Turbopump](turbopump.md), rpm|—|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 4th second after energizing the solenoid valves.
1. **[1]** — see [OKB Fakel](edb_fakel.md) site, 2016 y.
1. **Applicability** — [Arktika-M](арктика_м.md) ~~ [Elektro-M](электро_м.md)



### Fakel SPD-25   ［RU・el.］
**SPD-25** (ru. **СПД-25**, Стационарный плазменный двигатель) — electric [engine](ps.md) by [OKB Fakel](edb_fakel.md).

|**Characteristics**|**SPD-25**|
|:--|:--|
|Composition| |
|Consumption, W|100|
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|0.3|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|4|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[Xenon](xenon.md)|
|Fuel: consump., ㎏/s, ≤|0.000.000.891.946|
|Fuel: mass ratio|—|
|Fuel: resource, ㎏, ≥|4.81|
|Pres.: c.chamber, ㎫(㍴)| |
|Pres.: inlet, ㎫(㍴)| |
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|0.007|
|Thrust: 1 thrust, s|… ‑ … |
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|800|
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥| |
|Thrust: Σ thrust, N, ≥|37 800|
|Thrust: Σ time, s(h), ≥|5 400 000 (1 500 h, 62 d)|
|[Turbopump](turbopump.md), rpm|—|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. **[1]** — see [OKB Fakel](edb_fakel.md) site, 2016 y.
1. **Applicability** — …



### Fakel SPD-50   ［RU・el.］
**SPD-50** (ru. **СПД-50**, Стационарный плазменный двигатель) — electric [engine](ps.md) by [OKB Fakel](edb_fakel.md).

|**Characteristics**|**SPD-50**|
|:--|:--|
|Composition| |
|Consumption, W|220|
|Dimensions, ㎜|160×120×91|
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|1.23|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[Xenon](xenon.md)|
|Fuel: consump., ㎏/s, ≤|0.000.001.659.436|
|Fuel: mass ratio|—|
|Fuel: resource, ㎏, ≥|14.93|
|Pres.: c.chamber, ㎫(㍴)| |
|Pres.: inlet, ㎫(㍴)| |
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|0.014|
|Thrust: 1 thrust, s|… ‑ … |
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|860|
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥| |
|Thrust: Σ thrust, N, ≥|126 000|
|Thrust: Σ time, s(h), ≥|9 000 000 (2 500 h, 104 d)|
|[Turbopump](turbopump.md), rpm|—|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. **[1]** — see [OKB Fakel](edb_fakel.md) site, 2016 y.
1. **Applicability** — …



### Fakel SPD-70   ［RU・el.］
**SPD-70** (ru. **СПД-70**, Стационарный плазменный двигатель) — electric [engine](ps.md) by [OKB Fakel](edb_fakel.md).

|**Characteristics**|**SPD-70**|
|:--|:--|
|Composition| |
|Consumption, W|660|
|Dimensions, ㎜|198×146×98|
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|2|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[Xenon](xenon.md)|
|Fuel: consump., ㎏/s, ≤|0.000.002.773.790|
|Fuel: mass ratio|—|
|Fuel: resource, ㎏, ≥|30.95|
|Pres.: c.chamber, ㎫(㍴)| |
|Pres.: inlet, ㎫(㍴)| |
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|0.04|
|Thrust: 1 thrust, s|… ‑ … |
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|1 470|
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥| |
|Thrust: Σ thrust, N, ≥|446 400|
|Thrust: Σ time, s(h), ≥|11 160 000 (3 100 h, 129 d)|
|[Turbopump](turbopump.md), rpm|—|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. **[1]** — see [OKB Fakel](edb_fakel.md) site, 2016 y.
1. **Applicability** — …



### Fakel SPD-100   ［RU・el.］
**SPD-100** (ru. **СПД-100**, Стационарный плазменный двигатель) — electric [engine](ps.md) by [OKB Fakel](edb_fakel.md).

|**Characteristics**|**SPD-100**|
|:--|:--|
|Composition| |
|Consumption, W|1 350|
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|3 650 (10) / …|
|Mass, ㎏|3.5|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[Xenon](xenon.md)|
|Fuel: consump., ㎏/s, ≤|0.000.005.640.502|
|Fuel: mass ratio|—|
|Fuel: resource, ㎏, ≥|203.05|
|Pres.: c.chamber, ㎫(㍴)| |
|Pres.: inlet, ㎫(㍴)| |
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|0.083|
|Thrust: 1 thrust, s|… ‑ … |
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|1 500|
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥| |
|Thrust: Σ thrust, N, ≥|2 988 000|
|Thrust: Σ time, s(h), ≥|36 000 000 (10 000 h, 1.139 y)|
|[Turbopump](turbopump.md), rpm|—|


**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. **[1]** — see [OKB Fakel](edb_fakel.md) site, 2016 y.
1. **Applicability** — …



### Fakel SPD-140   ［RU・el.］
**SPD-140** (ru. **СПД-140**, Стационарный плазменный двигатель) — electric [engine](ps.md) by [OKB Fakel](edb_fakel.md).

|**Characteristics**|**SPD-140**|**SPD-140D**|
|:--|:--|:--|
|Composition| | |
|Consumption, W|3 000|4 500|
|Dimensions, ㎜| | |
|[Interfaces](interface.md)| | |
|[Lifetime](lifetime.md), h(y)|… / …|3 650 (10) / …|
|Mass, ㎏|8.4|8.4|
|[Overload](vibration.md), Grms| | |
|[Radiation](ion_rad.md), ㏉(㎭)| | |
|[Reliability](qm.md)| | |
|[Thermal](tcs.md), ℃| | |
|[TRL](trl.md)|9|9|
|[Voltage](sps.md), V| | |
|**【Specific】**|~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[Xenon](xenon.md)|[Xenon](xenon.md)|
|Fuel: consump., ㎏/s, ≤|0.000.011.710.596|0.000.017.701.599|
|Fuel: mass ratio|—|—|
|Fuel: resource, ㎏, ≥|330.94|495.64|
|Pres.: c.chamber, ㎫(㍴)| | |
|Pres.: inlet, ㎫(㍴)| |
|Pres.: nozzle cut, ㎫(㍴)| | |
|T°: comb.chamber, К(℃)| | |
|T°: nozzle cut, К(℃)| | |
|Thrust, N|0.193|0.29|
|Thrust: 1 thrust, s|… ‑ … |60 ‑ 360 000|
|Thrust: [Ing](ing.md), N·s, ≤| | |
|Thrust: [Isp](ps.md), s, ≥|1 680|1 670|
|Thrust: switch.rate, ㎐, ≤| |1 per 15 m|
|Thrust: torch angle, °| | |
|Thrust: Σ pulses, ≥| |625|
|Thrust: Σ thrust, N, ≥|5 450 000|8 120 000|
|Thrust: Σ time, s(h), ≥|28 260 000 (7 850 h, 0.895 y)|28 000 000 (7 800 h, 0.89 y)|
|[Turbopump](turbopump.md), rpm|—|—|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. **[1]** — see [OKB Fakel](edb_fakel.md) site, 2016 y.
1. **Applicability** — [Intergeliozond]([интергелиозонд.md) (SPD-140D)



### Fakel SPD-230   ［RU・el.］
**SPD-230** (ru. **СПД-230**, Стационарный плазменный двигатель) — electric [engine](ps.md) by [OKB Fakel](edb_fakel.md).

|**Characteristics**|**SPD-230**|
|:--|:--|
|Composition| |
|Consumption, W|15 000|
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|25|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|3|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[Xenon](xenon.md)|
|Fuel: consump., ㎏/s, ≤|0.000.029.637.180|
|Fuel: mass ratio|—|
|Fuel: resource, ㎏, ≥| |
|Pres.: c.chamber, ㎫(㍴)| |
|Pres.: inlet, ㎫(㍴)| |
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|0.785|
|Thrust: 1 thrust, s|… ‑ … |
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|2 700|
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥| |
|Thrust: Σ thrust, N, ≥| |
|Thrust: Σ time, s(h), ≥| |
|[Turbopump](turbopump.md), rpm|—|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. **[1]** — see [OKB Fakel](edb_fakel.md) site, 2016 y.
1. **Applicability** — …



### Fakel TK500M   ［RU・1comp.］
**TK500M** (ru. **ТК500М**) — 1‑component [engine](ps.md) by [OKB Fakel](edb_fakel.md).

|**Characteristics**|**TK500M**|
|:--|:--|
|Composition| |
|Consumption, W| |
|Dimensions, ㎜|110×129×67|
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|3 650 (10) / …|
|Mass, ㎏|0.44|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[Hydrazine](hydrazine.md) (H₂,N₂)|
|Fuel: consump., ㎏/s, ≤|0.000.463.349 - 0.002.594.754|
|Fuel: mass ratio|—|
|Fuel: resource, ㎏, ≥|200|
|Pres.: c.chamber, ㎫(㍴)|0.8 (8)|
|Pres.: inlet, ㎫(㍴)| |
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|1.0 - 5.6|
|Thrust: 1 thrust, s|… ‑ 1 800|
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|220|
|Thrust: switch.rate, ㎐, ≤|1 per 3.5 h|
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥|60 000|
|Thrust: Σ thrust, N, ≥|432 000|
|Thrust: Σ time, s(h), ≥| |
|[Turbopump](turbopump.md), rpm|—|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 4th second after energizing the solenoid valves.
1. **[1]** — see [OKB Fakel](edb_fakel.md) site, 2016 y.
1. **Applicability** — [Arktika-M](арктика_м.md) ~~ [Elektro-M](электро_м.md)



### IHI 22N   ［JP・2comp.］
**IHI 22N** — 2‑component [engine](ps.md) by [IHI](ihi.md). Designed in ….

|**Characteristics**|**IHI 22N**|
|:--|:--|
|Composition| |
|Consumption, W| |
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏| |
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[NTO+Hydrazine](nto_plus.md) (H₂,H₂O,NH₃,N₂)|
|Fuel: consump., ㎏/s, ≤|0.007.429.292|
|Fuel: mass ratio| |
|Fuel: resource, ㎏, ≥|583|
|Pres.: c.chamber, ㎫(㍴)| |
|Pres.: inlet, ㎫(㍴)|1.69 (16.579)|
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|21.5|
|Thrust: 1 thrust, s|… ‑ … |
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|295|
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥| |
|Thrust: Σ thrust, N, ≥|1 687 190|
|Thrust: Σ time, s(h), ≥|78 500|
|[Turbopump](turbopump.md), rpm|—|
| |[![](f/ps/i/ihi_22nt.webp)](f/ps/i/ihi_22n.webp)|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the …nd second after energizing the solenoid valves.
1. **Applicability** — …



### IHI 120N   ［JP・2comp.］
**IHI 120N** — 2‑component [engine](ps.md) by [IHI](ihi.md). Designed in ….

|**Characteristics**|**IHI 120N**|
|:--|:--|
|Composition| |
|Consumption, W| |
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏| |
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|MON3+MMH ()|
|Fuel: consump., ㎏/s, ≤|0.046.129|
|Fuel: mass ratio| |
|Fuel: resource, ㎏, ≥|446|
|Pres.: c.chamber, ㎫(㍴)| |
|Pres.: inlet, ㎫(㍴)|2.05 (20.11)|
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|124.4|
|Thrust: 1 thrust, s|… ‑ … |
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|274.9|
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥| |
|Thrust: Σ thrust, N, ≥|1 202 700|
|Thrust: Σ time, s(h), ≥|9 668|
|[Turbopump](turbopump.md), rpm|—|
| |[![](f/ps/i/ihi_120nt.webp)](f/ps/i/ihi_120n.webp)|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the …nd second after energizing the solenoid valves.
1. **Applicability** — …



### IHI 450N   ［JP・2comp.］
**IHI 450N** — 2‑component [engine](ps.md) by [IHI](ihi.md). Designed in ….

|**Characteristics**|**IHI 450N**|
|:--|:--|
|Composition| |
|Consumption, W| |
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏| |
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[NTO+Hydrazine](nto_plus.md) (H₂,H₂O,NH₃,N₂)|
|Fuel: consump., ㎏/s, ≤|0.139.427|
|Fuel: mass ratio| |
|Fuel: resource, ㎏, ≥|4 580|
|Pres.: c.chamber, ㎫(㍴)| |
|Pres.: inlet, ㎫(㍴)|O — 1.62 (15.9), F — 1.69 (16.6)|
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|450|
|Thrust: 1 thrust, s|… ‑ … |
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|329|
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥| |
|Thrust: Σ thrust, N, ≥|14 782 100|
|Thrust: Σ time, s(h), ≥|32 850|
|[Turbopump](turbopump.md), rpm|—|
| |[![](f/ps/i/ihi_450nt.webp)](f/ps/i/ihi_450n.webp)|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the …nd second after energizing the solenoid valves.
1. **Applicability** — …



### IHI 490N   ［JP・2comp.］
**IHI 490N** — 2‑component [engine](ps.md) by [IHI](ihi.md). Designed in ….

|**Characteristics**|**IHI 490N**|
|:--|:--|
|Composition| |
|Consumption, W| |
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏| |
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|MON3+MMH ()|
|Fuel: consump., ㎏/s, ≤|0.158.066|
|Fuel: mass ratio| |
|Fuel: resource, ㎏, ≥|2 371|
|Pres.: c.chamber, ㎫(㍴)| |
|Pres.: inlet, ㎫(㍴)|1.72 (16.87)|
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|478 ± …|
|Thrust: 1 thrust, s|… ‑ … |
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|316|
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥| |
|Thrust: Σ thrust, N, ≥|7 350 000|
|Thrust: Σ time, s(h), ≥|15 000|
|[Turbopump](turbopump.md), rpm|—|
| |[![](f/ps/i/ihi_490nt.webp)](f/ps/i/ihi_490n.webp)|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the …nd second after energizing the solenoid valves.
1. **Applicability** — …



### IHI BT-4   ［JP・2comp.］
**BT-4** — 2‑component [engine](ps.md) by [IHI](ihi.md). Designed in 2007.

|**Characteristics**|**BT-4**|
|:--|:--|
|Composition| |
|Consumption, W| |
|Dimensions, ㎜|80 × …|
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|4|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[NTO+Hydrazine](nto_plus.md) (H₂,H₂O,NH₃,N₂)|
|Fuel: consump., ㎏/s, ≤|0.079.638 ‑ 0.159.276|
|Fuel: mass ratio|1.69|
|Fuel: resource, ㎏, ≥| |
|Pres.: c.chamber, ㎫(㍴)|13.4 (1.36)|
|Pres.: inlet, ㎫(㍴)| |
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|250 ‑ 500|
|Thrust: 1 thrust, s|… ‑ … |
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|320 ± 5|
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥|25|
|Thrust: Σ thrust, N, ≥| |
|Thrust: Σ time, s(h), ≥| |
|[Turbopump](turbopump.md), rpm| |

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. <https://en.wikipedia.org/wiki/BT-4_(rocket_engine)>
1. **[1]** — in vacuum.
1. BT-4 is a family that has been used as Liquid Apogee Engine, orbital maneuvering engine & as a thruster. Variations:
    - **BT-4 (Cygnus)** — Used mainly as thruster, it burns MMH/N₂O₄ with a thrust of 450 N. It weighs 4 ㎏ & is 65 ㎝ tall.
    - **BT-4 (450N)** — Used mainly as LAE, it burns Hydrazine/N₂O₄ in a 1.69 O/F ratio. It has a thrust of 450 N, a specific impulse of 329 s (3.23 ㎞/s) & an input pressure of 1.62 ㎫. As of 2014, it had a demonstrated life of 32 850 s.
    - **BT-4 (500N)** — Used mainly as LAE, it burns Hydrazine/N₂O₄ with a thrust of 500 N, a specific impulse of 329 s (3.23 ㎞/s). It weights 4 ㎏ & is 80 ㎝ tall.
    - **490N MON Thruster** — Burns MMH/MON-3 with a 478 N nominal thrust, a specific impulse of 316 s (3.10 ㎞/s) & an inlet pressure of 1.72 ㎫ (249 psi). As of 2014, it had a demonstrated life of 15 000 s.
    - **HBT-5** — Developed for the HTV to crew-rated standards, it burns MMH/MON-3, & has a thrust of 500 N. Used in HTV-3 & since HTV-5 onward.
    - **SELENE OME** — Based on the DRTS Liquid Apogee Engine, the SELENE Orbital Maneuvering Engine burned a Hydrazine/MON-3 mixture. It had a thrust of 547 ± 54 N & a specific impulse of 319.8 ± 5.1 s (3.136 ± 0.050 ㎞/s) with an input pressure of 1.77 ㎫ (257 psi).
1. **Applicability** — Lunar-A (cancelled)・ Some geostationary communications satellite based on the Lockheed Martin A2100 & GEOStar-2 satellite buses.



### IHI MT-2   ［JP・1comp.］
**MT-2** — 1‑component [engine](ps.md) by [IHI](ihi.md). Designed in ….

|**Characteristics**|**IHI MT-2**|
|:--|:--|
|Composition| |
|Consumption, W| |
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏| |
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[Hydrazine](hydrazine.md) (H₂,N₂)|
|Fuel: consump., ㎏/s, ≤|0.003.349.351 ‑ 0.008.840.536|
|Fuel: mass ratio|—|
|Fuel: resource, ㎏, ≥|115|
|Pres.: c.chamber, ㎫(㍴)| |
|Pres.: inlet, ㎫(㍴)|0.48 ‑ 2.76 (4.7 ‑ 27.1)|
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|6.9 ‑ 19.6|
|Thrust: 1 thrust, s|… ‑ … |
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|210 ‑ 226|
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥|261 914|
|Thrust: Σ thrust, N, ≥|237 000 ‑ 255 000|
|Thrust: Σ time, s(h), ≥|34 400 ‑ 13 000|
|[Turbopump](turbopump.md), rpm|—|
| |[![](f/ps/i/ihi_mt2t.webp)](f/ps/i/ihi_mt2.webp)|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the …nd second after energizing the solenoid valves.
1. **Applicability** — …



### IHI MT-6   ［JP・1comp.］
**MT-6** — 1‑component [engine](ps.md) by [IHI](ihi.md). Designed in ….

|**Characteristics**|**IHI MT-6**|
|:--|:--|
|Composition| (ITAR‑free)|
|Consumption, W| |
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏| |
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[Hydrazine](hydrazine.md) (H₂,N₂)|
|Fuel: consump., ㎏/s, ≤|0.010.904.866 ‑ 0.022.652.622|
|Fuel: mass ratio|—|
|Fuel: resource, ㎏, ≥|120|
|Pres.: c.chamber, ㎫(㍴)| |
|Pres.: inlet, ㎫(㍴)|0.9 ‑ 2.76 (8.82 ‑ 27.1)|
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|23 ‑ 50|
|Thrust: 1 thrust, s|… ‑ … |
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|215 ‑ 225|
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥|14 800|
|Thrust: Σ thrust, N, ≥|253 100 ‑ 265 000|
|Thrust: Σ time, s(h), ≥|11 000 ‑ 5 300|
|[Turbopump](turbopump.md), rpm|—|
| |[![](f/ps/i/ihi_mt6t.webp)](f/ps/i/ihi_mt6.webp)|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the …nd second after energizing the solenoid valves.
1. **Applicability** — …



### IHI MT-8A   ［JP・1comp.］
**MT-8A** — 1‑component [engine](ps.md) by [IHI](ihi.md). Designed in ….

|**Characteristics**|**IHI MT-8A**|
|:--|:--|
|Composition| (ITAR‑free)|
|Consumption, W| |
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏| |
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[Hydrazine](hydrazine.md) (H₂,N₂)|
|Fuel: consump., ㎏/s, ≤|0.000.727.413 ‑ 0.002.560.005|
|Fuel: mass ratio|—|
|Fuel: resource, ㎏, ≥|180|
|Pres.: c.chamber, ㎫(㍴)| |
|Pres.: inlet, ㎫(㍴)|0.55 ‑ 2.76 (5.4 ‑ 27.1)|
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|1.47 ‑ 5.5|
|Thrust: 1 thrust, s|… ‑ … |
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|206 ‑ 219|
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥|850 000|
|Thrust: Σ thrust, N, ≥|364 000 ‑ 387 000|
|Thrust: Σ time, s(h), ≥|247 500 ‑ 70 500|
|[Turbopump](turbopump.md), rpm|—|
| |[![](f/ps/i/ihi_mt8at.webp)](f/ps/i/ihi_mt8a.webp)|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the …nd second after energizing the solenoid valves.
1. **Applicability** — …



### IHI MT-9   ［JP・1comp.］
**MT-9** — 1‑component [engine](ps.md) by [IHI](ihi.md). Designed in ….

|**Characteristics**|**IHI MT-9**|
|:--|:--|
|Composition| (ITAR‑free)|
|Consumption, W| |
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏| |
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[Hydrazine](hydrazine.md) (H₂,N₂)|
|Fuel: consump., ㎏/s, ≤|0.000.142.123 ‑ 0.000.535.760|
|Fuel: mass ratio|—|
|Fuel: resource, ㎏, ≥|100|
|Pres.: c.chamber, ㎫(㍴)| |
|Pres.: inlet, ㎫(㍴)|0.55 ‑ 2.76 (5.4 ‑ 27.1)|
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|0.29 ‑ 1.13|
|Thrust: 1 thrust, s|… ‑ … |
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|208 ‑ 215|
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥|850 000|
|Thrust: Σ thrust, N, ≥|204 000 ‑ 211 000|
|Thrust: Σ time, s(h), ≥|704 000 ‑ 187 000|
|[Turbopump](turbopump.md), rpm|—|
| |[![](f/ps/i/ihi_mt9t.webp)](f/ps/i/ihi_mt9.webp)|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the …nd second after energizing the solenoid valves.
1. **Applicability** — …



### ILOT 1N   ［EU・1comp.］
**ILOT 1N** — 1‑component [engine](ps.md) by ILOT (PL).

|**Characteristics**|**ILOT 1N**|
|:--|:--|
|Composition|1 unit, 1 off Solenoid, double‑seat|
|Consumption, W|13 — operation,, 5 up to 30 min pre‑heating before firing|
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|166 500 (19)|
|Mass, ㎏|0.3|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)|0.99999|
|[Thermal](tcs.md), ℃|−50 ‑ +100 (storage), +5 ‑ +90 (operation)|
|[TRL](trl.md)|5|
|[Voltage](sps.md), V|27.5 ‑ 35.5|
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|98% HTP (…)|
|Fuel: consump., ㎏/s, ≤|0.0004 ‑ 0.0006067 ‑ 0.00067|
|Fuel: mass ratio|1|
|Fuel: resource, ㎏, ≥|20|
|Pres.: c.chamber, ㎫(㍴)| |
|Pres.: inlet, ㎫(㍴)|0.56 ‑ 2.45 (5.5 ‑ 24)|
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|0.65 ‑ 1 ‑ 1.1|
|Thrust: 1 thrust, s|0.05 ‑ 3 600|
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|168|
|Thrust: switch.rate, ㎐, ≤|14|
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥|20 000|
|Thrust: Σ thrust, N, ≥|18 000|
|Thrust: Σ time, s(h), ≥|18 000 (5)|
|[Turbopump](turbopump.md), rpm|—|
| |[![](f/ps/i/ilot_1n_pic1t.webp)](f/ps/i/ilot_1n_pic1.webp)|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 0.02nd second after energizing the solenoid valves.
1. **Applicability** — …



### ILOT 420N   ［EU・2comp.］
**ILOT 420N** — 2‑component [engine](ps.md) by ILOT (PL).

|**Characteristics**|**ILOT 420N**|
|:--|:--|
|Composition|1 unit, 2 off Solenoid, Single Seat, Redundant Coil|
|Consumption, W|100 — operation, 50 up to 30 min — cat bed pre‑heating before firing|
|Dimensions, ㎜|600 × ⌀230|
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|166 500 (19)|
|Mass, ㎏|4.3|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)|0.99999|
|[Thermal](tcs.md), ℃|−50 ‑ +100 (storage), +5 ‑ +90 (operation)|
|[TRL](trl.md)|5|
|[Voltage](sps.md), V|27.5 ‑ 35.5|
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|98% HTP+TMPDA|
|Fuel: consump., ㎏/s, ≤|0.143: 0.101 (HTP), 0.28 (TMPDA) ‑ 0.119 (HTP), 0.33 (TMPDA)|
|Fuel: mass ratio|1:3.62|
|Fuel: resource, ㎏, ≥|500 (HTP), 140 (TMPDA)|
|Pres.: c.chamber, ㎫(㍴)|1 (10)|
|Pres.: inlet, ㎫(㍴)|1.32 ‑ 1.73 ‑ 1.93 (13 ‑ 17 ‑ 19)|
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|420|
|Thrust: 1 thrust, s|… ‑ 3 600|
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|299|
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥|20|
|Thrust: Σ thrust, N, ≥| |
|Thrust: Σ time, s(h), ≥|4 500 (1.25)|
|[Turbopump](turbopump.md), rpm|—|
| |[![](f/ps/i/ilot_420n_pic1t.webp)](f/ps/i/ilot_420n_pic1.webp)|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. **Applicability** — …



### KBHM 14D30   ［RU・2comp.］
**11D30** (ru. **14Д30**) — 2‑component [engine](ps.md) by [KBHM](kbhm.md)

|**Characteristics**|**14D30**|
|:--|:--|
|Composition| |
|Consumption, W| |
|Dimensions, ㎜|948 × 1 150|
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|95|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[NTO+UDMH](nto_plus.md) (H₂,H₂O,CO,CO₂,N₂)|
|Fuel: consump., ㎏/s, ≤|6.085|
|Fuel: mass ratio|2.0|
|Fuel: resource, ㎏, ≥|19 470|
|Pres.: c.chamber, ㎫(㍴)|10 (100)|
|Pres.: inlet, ㎫(㍴)| |
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|19 620|
|Thrust: 1 thrust, s|… ‑ 2 500|
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|328.6|
|Thrust: switch.rate, ㎐, ≤|1 per 2.6 s|
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥|8|
|Thrust: Σ thrust, N, ≥|62 700 000|
|Thrust: Σ time, s(h), ≥|3 200|
|[Turbopump](turbopump.md), rpm|—|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. **[1]** — see [KBHM](kbhm.md) site, 2016 y.
1. **Applicability** — [Briz](бриз.md)



### KBHM 255U.487   ［RU・2comp.］
**255U.487** (ru. **255У.487**) — 2‑component [engine](ps.md) by [KBHM](kbhm.md)

|**Characteristics**|**255U.487**|**DMT-600**|
|:--|:--|:--|
|Composition| | |
|Consumption, W|24.3|24.3|
|Dimensions, ㎜|416 × 148| |
|[Interfaces](interface.md)| | |
|[Lifetime](lifetime.md), h(y)|30 / …|… / …|
|Mass, ㎏|3.6|4.2|
|[Overload](vibration.md), Grms| | |
|[Radiation](ion_rad.md), ㏉(㎭)| | |
|[Reliability](qm.md)| | |
|[Thermal](tcs.md), ℃| | |
|[TRL](trl.md)|6|3|
|[Voltage](sps.md), V| | |
|**【Specific】**|~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[NTO+UDMH](nto_plus.md) (H₂,H₂O,CO,CO₂,N₂)|[NTO+UDMH](nto_plus.md) (H₂,H₂O,CO,CO₂,N₂)|
|Fuel: consump., ㎏/s, ≤|0.202.825|0.200.534|
|Fuel: mass ratio|1.8169 ± 0.066|1.85|
|Fuel: resource, ㎏, ≥|20|1 000|
|Pres.: c.chamber, ㎫(㍴)|1.177 (12.0)|0.9 (9)|
|Pres.: inlet, ㎫(㍴)|1.96 (20)|1.7 (17)|
|Pres.: nozzle cut, ㎫(㍴)|0.00167 (0.0119)| |
|T°: comb.chamber, К(℃)|3 000 (2 700)| |
|T°: nozzle cut, К(℃)|840 (560)| |
|Thrust, N|603±16|600|
|Thrust: 1 thrust, s|0.2 ‑ 50|0.05 ‑ 1 500|
|Thrust: [Ing](ing.md), N·s, ≤|17±4.6| |
|Thrust: [Isp](ps.md), s, ≥|303±2.2|305|
|Thrust: switch.rate, ㎐, ≤|3| |
|Thrust: torch angle, °| | |
|Thrust: Σ pulses, ≥|100|6 000|
|Thrust: Σ thrust, N, ≥|60 300|300 000|
|Thrust: Σ time, s(h), ≥|100|5 000|
|[Turbopump](turbopump.md), rpm|—|—|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. **[1]** — see 255У.487.00‑0 ТУ‑5, 2015 y.
1. **[2]** — see [KBHM](kbhm.md) site, 2016 y.
1. The content of dissolved helium in the fuel is ≤ 0.03 g/l. Work poorly with [CINU](cinu.md).
1. The angular deviation of the nozzle axis relative to the base longitudinal axis is ≤ 5 " (angular), & ≤ 0.5 ㎜ (linear).
1. The purity of the fuel supplied to the engine is not lower than [class 5](clean_lvl.md).
1. **Applicability** — [Luna‑25](луна_25.md)



### KBHM 255U.530   ［RU・2comp.］
**255U.530** (ru. **255У.530**) — 2‑component [engine](ps.md) by [KBHM](kbhm.md).

|**Characteristics**|**255У.530.00-0**|
|:--|:--|
|Composition| |
|Consumption, W|24.3|
|Dimensions, ㎜|416 × 148|
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|26 280 (3) / …|
|Mass, ㎏|3.6|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|4|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[NTO+UDMH](nto_plus.md) (H₂,H₂O,CO,CO₂,N₂)|
|Fuel: consump., ㎏/s, ≤|0.202.825|
|Fuel: mass ratio|1.8169 ± 0.066|
|Fuel: resource, ㎏, ≥|20|
|Pres.: c.chamber, ㎫(㍴)|1.177 (12.0)|
|Pres.: inlet, ㎫(㍴)|1.96 (20)|
|Pres.: nozzle cut, ㎫(㍴)|0.00167 (0.0119)|
|T°: comb.chamber, К(℃)|3 000 (2 700)|
|T°: nozzle cut, К(℃)|840 (560)|
|Thrust, N|588±49|
|Thrust: 1 thrust, s|0.2 ‑ 70|
|Thrust: [Ing](ing.md), N·s, ≤|17±4.6|
|Thrust: [Isp](ps.md), s, ≥|303±2.2|
|Thrust: switch.rate, ㎐, ≤|3|
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥|200|
|Thrust: Σ thrust, N, ≥|120 600|
|Thrust: Σ time, s(h), ≥|200|
|[Turbopump](turbopump.md), rpm|—|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 0.13st second after energizing the solenoid valves.
1. Designed for the needs of the [Luna-27](luna_27.md) based on the engine [255U.487](engine_lst.md).
1. The content of dissolved helium in the fuel is ≤ 0.03 g/l. Work poorly with [CINU](cinu.md).
1. The angular deviation of the nozzle axis relative to the base longitudinal axis is ≤ 5 " (angular), & ≤ 0.5 ㎜ (linear).
1. The purity of the fuel supplied to the engine is not lower than [class 5](clean_lvl.md).
1. **Applicability** — [Luna‑27](луна_27.md)



### KBHM DMT-500   ［RU・2comp.］
**DMT-500** (ru. **ДМТ-500**) — 2‑component [engine](ps.md) by [KBHM](kbhm.md).

|**Characteristics**|**DMT-500**|
|:--|:--|
|Composition| |
|Consumption, W|24.3|
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|3.6|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[NTO+UDMH](nto_plus.md) (H₂,H₂O,CO,CO₂,N₂)|
|Fuel: consump., ㎏/s, ≤|0.164.419|
|Fuel: mass ratio|1.85|
|Fuel: resource, ㎏, ≥|1 645|
|Pres.: c.chamber, ㎫(㍴)|0.9 (9)|
|Pres.: inlet, ㎫(㍴)|1.6 (16)|
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|500|
|Thrust: 1 thrust, s|0.05 ‑ 3 500|
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|310|
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥|20|
|Thrust: Σ thrust, N, ≥|5 000 000|
|Thrust: Σ time, s(h), ≥|10 000|
|[Turbopump](turbopump.md), rpm|—|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. **[1]** — see [KBHM](kbhm.md) site, 2016 y.
1. They put forward requirements for fuel degassing. They work poorly with [CINU](cinu.md).
1. **Applicability** — …



### KBHM DMT-1000   ［RU・2comp.］
**DMT-1000** (ru. **ДМТ-1000**) — 2‑component [engine](ps.md) by [KBHM](kbhm.md)

|**Characteristics**|**DMT-1000**|
|:--|:--|
|Composition| |
|Consumption, W|24.3|
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|2|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[NTO+UDMH](nto_plus.md) (H₂,H₂O,CO,CO₂,N₂)|
|Fuel: consump., ㎏/s, ≤|0.362.844|
|Fuel: mass ratio|1.85|
|Fuel: resource, ㎏, ≥|10|
|Pres.: c.chamber, ㎫(㍴)|4 (40)|
|Pres.: inlet, ㎫(㍴)|7 (70)|
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|1 000|
|Thrust: 1 thrust, s|0.035 ‑ 3|
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|281|
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥|1 000|
|Thrust: Σ thrust, N, ≥|30 000|
|Thrust: Σ time, s(h), ≥|30|
|[Turbopump](turbopump.md), rpm|—|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. **[1]** — see [KBHM](kbhm.md) site, 2016 y.
1. They put forward requirements for fuel degassing. They work poorly with [CINU](cinu.md).
1. **Applicability** — …



### KBHM DMT-2200   ［RU・2comp.］
**DMT-2200** (ru. **ДМТ-2200**) — 2‑component [engine](ps.md) by [KBHM](kbhm.md).

|**Characteristics**|**DMT-2200**|
|:--|:--|
|Composition| |
|Consumption, W|10.8|
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|6.5|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[NTO+UDMH](nto_plus.md) (H₂,H₂O,CO,CO₂,N₂)|
|Fuel: consump., ㎏/s, ≤|0.752.005|
|Fuel: mass ratio|1.95|
|Fuel: resource, ㎏, ≥|157|
|Pres.: c.chamber, ㎫(㍴)|1.8 (18)|
|Pres.: inlet, ㎫(㍴)|2.45 (24.5)|
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|2 250|
|Thrust: 1 thrust, s|0.05 ‑ 210|
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|305|
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥|500|
|Thrust: Σ thrust, N, ≥|472 500|
|Thrust: Σ time, s(h), ≥|210|
|[Turbopump](turbopump.md), rpm|—|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. **[1]** — see [KBHM](kbhm.md) site, 2016 y.
1. They put forward requirements for fuel degassing. They work poorly with [CINU](cinu.md).
1. **Applicability** — …



### KBHM DOC-10   ［RU・1comp.］
**DOC-10** (ru. **ДОК-10**) — 1‑component [engine](ps.md) by [KBHM](kbhm.md)

|**Characteristics**|**DOC-10**|
|:--|:--|
|Composition| |
|Consumption, W|7.9|
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|0.6|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)| |
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[Hydrazine](hydrazine.md) (H₂,N₂)|
|Fuel: consump., ㎏/s, ≤|0.004.451.388|
|Fuel: mass ratio|—|
|Fuel: resource, ㎏, ≥|6.5|
|Pres.: c.chamber, ㎫(㍴)|1.0 (10)|
|Pres.: inlet, ㎫(㍴)|1.5 (15)|
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|10|
|Thrust: 1 thrust, s|0.05 ‑ 600|
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|229|
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥|4 000|
|Thrust: Σ thrust, N, ≥|15 000|
|Thrust: Σ time, s(h), ≥|1 500|
|[Turbopump](turbopump.md), rpm|—|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. **[1]** — see [KBHM](kbhm.md) site, 2016 y.
1. **Applicability** — …



### KBHM DOC-50   ［RU・1comp.］
**DOC-50** (ru. **ДОК-50**) — 1‑component [engine](ps.md) by [KBHM](kbhm.md)

|**Characteristics**|**DOC-50**|
|:--|:--|
|Composition| |
|Consumption, W|16.2|
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|1.1|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)| |
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[Hydrazine](hydrazine.md) (H₂,N₂)|
|Fuel: consump., ㎏/s, ≤|0.022.256|
|Fuel: mass ratio|—|
|Fuel: resource, ㎏, ≥|33|
|Pres.: c.chamber, ㎫(㍴)|0.8 (8)|
|Pres.: inlet, ㎫(㍴)|1.5 (15)|
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|50|
|Thrust: 1 thrust, s|0.05 ‑ 600|
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|229|
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥|4 000|
|Thrust: Σ thrust, N, ≥|75 000|
|Thrust: Σ time, s(h), ≥|1 500|
|[Turbopump](turbopump.md), rpm|—|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. **[1]** — see [KBHM](kbhm.md) site, 2016 y.
1. **Applicability** — …



### KBHM DOT-5   ［RU・1comp.］
**DOT-5** (ru. **ДОТ-5**) — 1‑component [engine](ps.md) by [KBHM](kbhm.md)

|**Characteristics**|**DOT-5**|
|:--|:--|
|Composition| |
|Consumption, W|15.8|
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|0.9|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)| |
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[Hydrazine](hydrazine.md) (H₂,N₂)|
|Fuel: consump., ㎏/s, ≤|0.002.216.017|
|Fuel: mass ratio|—|
|Fuel: resource, ㎏, ≥|260|
|Pres.: c.chamber, ㎫(㍴)|0.38 (3.8)|
|Pres.: inlet, ㎫(㍴)|1.2 (12)|
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|5|
|Thrust: 1 thrust, s|0.05 ‑ 8 000|
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|230|
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥|55 000|
|Thrust: Σ thrust, N, ≥|600 000|
|Thrust: Σ time, s(h), ≥|120 000|
|[Turbopump](turbopump.md), rpm|—|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. **[1]** — see [KBHM](kbhm.md) site, 2016 y.
1. **Applicability** — …



### KBHM DOT-25   ［RU・1comp.］
**DOT-25** (ru. **ДОТ-25**) — 1‑component [engine](ps.md) by [KBHM](kbhm.md)

|**Characteristics**|**DOT-25**|
|:--|:--|
|Composition| |
|Consumption, W|7.9|
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|1.3|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)| |
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[Hydrazine](hydrazine.md) (H₂,N₂)|
|Fuel: consump., ㎏/s, ≤|0.010.890|
|Fuel: mass ratio|—|
|Fuel: resource, ㎏, ≥|267|
|Pres.: c.chamber, ㎫(㍴)|0.45 (4.5)|
|Pres.: inlet, ㎫(㍴)|1.5 (15)|
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|25|
|Thrust: 1 thrust, s|0.05 ‑ 6 000|
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|234|
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥|6 000|
|Thrust: Σ thrust, N, ≥|625 000|
|Thrust: Σ time, s(h), ≥|25 000|
|[Turbopump](turbopump.md), rpm|—|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. **[1]** — see [KBHM](kbhm.md) site, 2016 y.
1. **Applicability** — …



### KBHM KVD1   ［RU・2comp.］
**KVD1** (ru. **КВД1**) — 2‑component [engine](ps.md) by [KBHM](kbhm.md)

|**Characteristics**|**KVD1**|
|:--|:--|
|Composition| |
|Consumption, W| |
|Dimensions, ㎜|2 140 × 1 580|
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|282|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[O+H](o_plus.md) (H₂,H₂O)|
|Fuel: consump., ㎏/s, ≤|15.365|
|Fuel: mass ratio|6|
|Fuel: resource, ㎏, ≥|12 300|
|Pres.: c.chamber, ㎫(㍴)|5.7 (57)|
|Pres.: inlet, ㎫(㍴)| |
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|69 650|
|Thrust: 1 thrust, s|… ‑ 600|
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|462|
|Thrust: switch.rate, ㎐, ≤|1 per 7.5 s|
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥|3|
|Thrust: Σ thrust, N, ≥|55 720 000|
|Thrust: Σ time, s(h), ≥|800|
|[Turbopump](turbopump.md), rpm|—|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. <https://en.wikipedia.org/wiki/KVD-1>
1. **[1]** — see [KBHM](kbhm.md) site, 2016 y.
1. **Applicability** — …



### KBHM S5.86   ［RU・2comp.］
**S5.86** (ru. **С5.86**, also **КМД** (кислородно‑метановый двигатель) — 2‑component [engine](ps.md) by [KBHM](kbhm.md)

|**Characteristics**|**S5.86**|
|:--|:--|
|Composition| |
|Consumption, W| |
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏| |
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|5|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[O+Methane](o_plus.md) (H₂,H₂O,CO,CO₂)|
|Fuel: consump., ㎏/s, ≤|20.268|
|Fuel: mass ratio| |
|Fuel: resource, ㎏, ≥| |
|Pres.: c.chamber, ㎫(㍴)| |
|Pres.: inlet, ㎫(㍴)| |
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|73 575|
|Thrust: 1 thrust, s|… ‑ … |
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|370|
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥| |
|Thrust: Σ thrust, N, ≥| |
|Thrust: Σ time, s(h), ≥| |
|[Turbopump](turbopump.md), rpm|—|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. **[1]** — see [KBHM](kbhm.md) site, 2016 y.
1. **Applicability** — …



### KBHM S5.92   ［RU・2comp.］
**S5.92** (ru. **С5.92**) — 2‑component [engine](ps.md) by [KBHM](kbhm.md)

|**Characteristics**|**S5.92, high thrust**|**S5.92, low thrust**|
|:--|:--|:--|
|Composition| | |
|Consumption, W| | |
|Dimensions, ㎜|677 × 838 × 1028|677 × 838 × 1028|
|[Interfaces](interface.md)| | |
|[Lifetime](lifetime.md), h(y)|300 / …|300 / …|
|Mass, ㎏|75|75|
|[Overload](vibration.md), Grms| | |
|[Radiation](ion_rad.md), ㏉(㎭)| | |
|[Reliability](qm.md)| | |
|[Thermal](tcs.md), ℃| | |
|[TRL](trl.md)|9|9|
|[Voltage](sps.md), V| | |
|**【Specific】**|~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[NTO+UDMH](nto_plus.md) (H₂,H₂O,CO,CO₂,N₂)|[NTO+UDMH](nto_plus.md) (H₂,H₂O,CO,CO₂,N₂)|
|Fuel: consump., ㎏/s, ≤|6.115.960|4.430.322|
|Fuel: mass ratio|1.95 ‑ 2.05|2 ‑ 2.1|
|Fuel: resource, ㎏, ≥|12 230|8 860|
|Pres.: c.chamber, ㎫(㍴)|9.8 (98)|6.85 (58.5)|
|Pres.: inlet, ㎫(㍴)|0.6 ‑ 0.8 (6 ‑ 8)|0.6 ‑ 0.8 (6 ‑ 8)|
|Pres.: nozzle cut, ㎫(㍴)| | |
|T°: comb.chamber, К(℃)| | |
|T°: nozzle cut, К(℃)| | |
|Thrust, N|19 620|13 734|
|Thrust: 1 thrust, s|… ‑ … | |
|Thrust: [Ing](ing.md), N·s, ≤| | |
|Thrust: [Isp](ps.md), s, ≥|327|316|
|Thrust: switch.rate, ㎐, ≤|1 per 6 min (max pause 300 d)|1 per 6 min (max pause 300 d)|
|Thrust: torch angle, °| | |
|Thrust: Σ pulses, ≥|50|50|
|Thrust: Σ thrust, N, ≥|39 200 000|27 400 000|
|Thrust: Σ time, s(h), ≥|2 000|2 000|
|[Turbopump](turbopump.md), rpm|58 000|43 000|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. **[1]** — see [KBHM](kbhm.md) site, 2016 y.
1. Turbopump consumes 4 ㎏ of fuel for the start, 5.5 — for the stop.
1. **Applicability** — [Fregat](fregat.md) & MDU (regular main engine) ~~ [Fobos-Grunt](фобос_грунт.md) (as a part of MDU)



### KBHM S5.120   ［RU・2comp.］
**S5.120** (ru. **С5.120**) — 2‑component [engine](ps.md) by [KBHM](kbhm.md)

|**Characteristics**|**S5.120**|
|:--|:--|
|Composition| |
|Consumption, W| |
|Dimensions, ㎜|820 × 440|
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|30|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[NTO+UDMH](nto_plus.md) (H₂,H₂O,CO,CO₂,N₂)|
|Fuel: consump., ㎏/s, ≤|1.967.213|
|Fuel: mass ratio| |
|Fuel: resource, ㎏, ≥| |
|Pres.: c.chamber, ㎫(㍴)| |
|Pres.: inlet, ㎫(㍴)| |
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|5 886|
|Thrust: 1 thrust, s|… ‑ … |
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|305|
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥|10|
|Thrust: Σ thrust, N, ≥| |
|Thrust: Σ time, s(h), ≥| |
|[Turbopump](turbopump.md), rpm|—|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. <http://lpre.de/kbhm/index.htm>
1. **[1]** — expert review.
1. **Applicability** — 「Аргон」 (「Zenith‑6U」)・ Upper-stage 「Курьер」 (1998 y)



### KBHM S5.140   ［RU・2comp.］
**S5.140** (ru. **С5.140**, also **ДМТ-6**) — 2‑component [engine](ps.md) by [KBHM](kbhm.md)

|**Characteristics**|**S5.140**|**DMT-6**|
|:--|:--|:--|
|Composition| | |
|Consumption, W|15.8|15.8|
|Dimensions, ㎜|200 × 26.6| |
|[Interfaces](interface.md)| | |
|[Lifetime](lifetime.md), h(y)|60 / 87 600 (10)|… / 87 600 (10)|
|Mass, ㎏|0.95|0.9|
|[Overload](vibration.md), Grms| | |
|[Radiation](ion_rad.md), ㏉(㎭)| | |
|[Reliability](qm.md)| | |
|[Thermal](tcs.md), ℃| | |
|[TRL](trl.md)|6|3|
|[Voltage](sps.md), V|27±3|27±3|
|**【Specific】**|~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[NTO+UDMH](nto_plus.md) (H₂,H₂O,CO,CO₂,N₂)|[NTO+UDMH](nto_plus.md) (H₂,H₂O,CO,CO₂,N₂)|
|Fuel: consump., ㎏/s, ≤|0.002.354.435|0.002.446.483|
|Fuel: mass ratio|1.838 ± 0.097|1.85|
|Fuel: resource, ㎏, ≥|11|48|
|Pres.: c.chamber, ㎫(㍴)|0.647 (6.6)|0.62 (6.3)|
|Pres.: inlet, ㎫(㍴)|1.47 (15)|1.2 (12)|
|Pres.: nozzle cut, ㎫(㍴)|0.000296 (0.00302)| |
|T°: comb.chamber, К(℃)|1 900 (1 700)| |
|T°: nozzle cut, К(℃)|600 (330)| |
|Thrust, N|6.19±0.143|6|
|Thrust: 1 thrust, s|0.045 ‑ 100|0.045 ‑ 150|
|Thrust: [Ing](ing.md), N·s, ≤|0.294| |
|Thrust: [Isp](ps.md), s, ≥|268.4±15.4|250|
|Thrust: switch.rate, ㎐, ≤|10.5| |
|Thrust: torch angle, °| | |
|Thrust: Σ pulses, ≥|30 000|300 000|
|Thrust: Σ thrust, N, ≥|29 000|118 000|
|Thrust: Σ time, s(h), ≥|4 700|20 000|
|[Turbopump](turbopump.md), rpm|—|—|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. **[1,2]** — see Specification & [KBHM](kbhm.md) site, 2016 y.
1. They put forward requirements for fuel degassing. They work poorly with [CINU](cinu.md).
1. The angular deviation of the axis of the nozzle relative to the base longitudinal axis is ≤ 30 ".
1. **Applicability** — [Luna‑25](луна_25.md) ~~ [Luna‑27](луна_27.md)



### KBHM S5.142   ［RU・2comp.］
**S5.142** (ru. **С5.142**) — 2‑component [engine](ps.md) by [KBHM](kbhm.md)

|**Characteristics**|**S5.142**|**DST-25**|
|:--|:--|:--|
|Composition| | |
|Consumption, W|15.8|15.8|
|Dimensions, ㎜| | |
|[Interfaces](interface.md)| | |
|[Lifetime](lifetime.md), h(y)|… / …|… / …|
|Mass, ㎏|0.9|0.9|
|[Overload](vibration.md), Grms| | |
|[Radiation](ion_rad.md), ㏉(㎭)| | |
|[Reliability](qm.md)| | |
|[Thermal](tcs.md), ℃| | |
|[TRL](trl.md)|9|9|
|[Voltage](sps.md), V| | |
|**【Specific】**|~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[NTO+UDMH](nto_plus.md) (H₂,H₂O,CO,CO₂,N₂)|[NTO+UDMH](nto_plus.md) (H₂,H₂O,CO,CO₂,N₂)|
|Fuel: consump., ㎏/s, ≤|0.008.762.988・ 0.008.324.838|0.008.973.309|
|Fuel: mass ratio| |1.85|
|Fuel: resource, ㎏, ≥|208|220|
|Pres.: c.chamber, ㎫(㍴)|0.78 (8)|0.8 (8)|
|Pres.: inlet, ㎫(㍴)|1.47 (15)|1.5 (10.5)|
|Pres.: nozzle cut, ㎫(㍴)| | |
|T°: comb.chamber, К(℃)|3 300 ‑ 3 800 (3 000 ‑ 3 500)|3 300 ‑ 3 800 (3 000 ‑ 3 500)|
|T°: nozzle cut, К(℃)| | |
|Thrust, N|24.5|25|
|Thrust: 1 thrust, s|0.03 ‑ 4 000|0.03 ‑ 4 000|
|Thrust: [Ing](ing.md), N·s, ≤| | |
|Thrust: [Isp](ps.md), s, ≥|285・ 300|284|
|Thrust: switch.rate, ㎐, ≤| | |
|Thrust: torch angle, °| | |
|Thrust: Σ pulses, ≥|100 000|300 000|
|Thrust: Σ thrust, N, ≥|582 000・ 613 000|613 000|
|Thrust: Σ time, s(h), ≥|25 000|25 000|
|[Turbopump](turbopump.md), rpm|—|—|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. **[1]** — see [KBHM](kbhm.md) site, 2016 y.
1. **[2]** — can be achieved by modernization by increasing the pressure by 10 atm.
1. They put forward requirements for fuel degassing. They work poorly with [CINU](cinu.md).
1. **Applicability** — Soyuz‑TMA



### KBHM S5.144   ［RU・2comp.］
**S5.144** (ru. **С5.144**) — 2‑component [engine](ps.md) by [KBHM](kbhm.md)

|**Characteristics**|**S5.144**|**DST-100**|**DST-100A**|
|:--|:--|:--|:--|
|Composition| | | |
|Consumption, W|34.2|34.2|34.2|
|Dimensions, ㎜| | | |
|[Interfaces](interface.md)| | | |
|[Lifetime](lifetime.md), h(y)|… / …|… / …|… / …|
|Mass, ㎏|1.5|1.1|1.5|
|[Overload](vibration.md), Grms| | | |
|[Radiation](ion_rad.md), ㏉(㎭)| | | |
|[Reliability](qm.md)| | | |
|[Thermal](tcs.md), ℃| | | |
|[TRL](trl.md)|9|9|9|
|[Voltage](sps.md), V| | | |
|**【Specific】**|~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[NTO+UDMH](nto_plus.md)<br> (H₂,H₂O,CO,CO₂,N₂)|[NTO+UDMH](nto_plus.md)<br> (H₂,H₂O,CO,CO₂,N₂)|[NTO+UDMH](nto_plus.md)<br> (H₂,H₂O,CO,CO₂,N₂)|
|Fuel: consump., ㎏/s, ≤|0.033.141・ 0.032.259|0.036.968|0.033.557|
|Fuel: mass ratio|1.85|1.85|1.85|
|Fuel: resource, ㎏, ≥|1 610|371|1 680|
|Pres.: c.chamber, ㎫(㍴)|0.74 (7.5)|1.5 (15)|0.77 (7.7)|
|Pres.: inlet, ㎫(㍴)|1.81 (18.5)|2.5 (25)|1.6 (16)|
|Pres.: nozzle cut, ㎫(㍴)| | | |
|T°: comb.chamber, К(℃)|3 300 ‑ 3 800 (3 000 ‑ 3 500)|3 300 ‑ 3 800 (3 000 ‑ 3 500)|3 300 ‑ 3 800 (3 000 ‑ 3 500)|
|T°: nozzle cut, К(℃)| | | |
|Thrust, N|98.1|100|100|
|Thrust: 1 thrust, s|0.03 ‑ 2 500|0.05 ‑ 300|0.05 ‑ 4 000|
|Thrust: [Ing](ing.md), N·s, ≤| | | |
|Thrust: [Isp](ps.md), s, ≥|302・ 310|275|303|
|Thrust: switch.rate, ㎐, ≤| | | |
|Thrust: torch angle, °| | | |
|Thrust: Σ pulses, ≥|450 000|10 000|450 000|
|Thrust: Σ thrust, N, ≥|4 770 000・ 4 900 000|1 001 000|5 000 000|
|Thrust: Σ time, s(h), ≥|50 000|10 000|50 000|
|[Turbopump](turbopump.md), rpm|—|—|—|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. **[1]** — see [KBHM](kbhm.md) site, 2016 y.
1. **[2]** — can be achieved by modernization by increasing the pressure by 10 atm.
1. They put forward requirements for fuel degassing. They work poorly with [CINU](cinu.md).
1. **Applicability** — S5.144 — ISS in 2016 ‑ 2018.



### KBHM S5.145   ［RU・2comp.］
**S5.145** (ru. **С5.145**) — 2‑component [engine](ps.md) by [KBHM](kbhm.md)

|**Characteristics**|**S5.145**|**DST-50**|
|:--|:--|:--|
|Composition| | |
|Consumption, W|15.8|15.8|
|Dimensions, ㎜|280 × 102.25| |
|[Interfaces](interface.md)| | |
|[Lifetime](lifetime.md), h(y)|60 / …|… / …|
|Mass, ㎏|1.3|1.1|
|[Overload](vibration.md), Grms| | |
|[Radiation](ion_rad.md), ㏉(㎭)| | |
|[Reliability](qm.md)| | |
|[Thermal](tcs.md), ℃| | |
|[TRL](trl.md)|6|3|
|[Voltage](sps.md), V| | |
|**【Specific】**|~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[NTO+UDMH](nto_plus.md) (H₂,H₂O,CO,CO₂,N₂)|[NTO+UDMH](nto_plus.md) (H₂,H₂O,CO,CO₂,N₂)|
|Fuel: consump., ㎏/s, ≤|0.017.686|0.017.574|
|Fuel: mass ratio|1.85 ± 0.05|1.85 ± 0.05|
|Fuel: resource, ㎏, ≥|177|439|
|Pres.: c.chamber, ㎫(㍴)|0.775 (7.6)|0.75 (7.5)|
|Pres.: inlet, ㎫(㍴)|1.47 (15)|1.5 (15)|
|Pres.: nozzle cut, ㎫(㍴)|0.000179 (0.00176)| |
|T°: comb.chamber, К(℃)|2 500 (2 300)| |
|T°: nozzle cut, К(℃)|600 (330)| |
|Thrust, N|52.9|50|
|Thrust: 1 thrust, s|0.045 ‑ 3 000|0.03 ‑ 4 000|
|Thrust: [Ing](ing.md), N·s, ≤|0.75 ‑ 2.34| |
|Thrust: [Isp](ps.md), s, ≥|305±2|290|
|Thrust: switch.rate, ㎐, ≤|10.5| |
|Thrust: torch angle, °| | |
|Thrust: Σ pulses, ≥|20 000|300 000|
|Thrust: Σ thrust, N, ≥|530 000|1 250 000|
|Thrust: Σ time, s(h), ≥|10 000|25 000|
|[Turbopump](turbopump.md), rpm|—|—|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. **[1,2]** — see Specification & [KBHM](kbhm.md) site, 2016 y.
1. They put forward requirements for fuel degassing. They work poorly with [CINU](cinu.md).
1. The angular deviation of the axis of the nozzle relative to the base longitudinal axis is ≤ 30 ".
1. **Applicability** — [Luna‑25](луна_25.md) ~~ [Luna‑27](луна_27.md)



### KBHM S5.146   ［RU・2comp.］
**S5.146** (ru. **С5.146**) — 2‑component [engine](ps.md) by [KBHM](kbhm.md).

|**Characteristics**|**S5.146**|**DST-200**|**DST-200A**|
|:--|:--|:--|:--|
|Composition| | | |
|Consumption, W|34.2|21.6|34.2|
|Dimensions, ㎜| | | |
|[Interfaces](interface.md)| | | |
|[Lifetime](lifetime.md), h(y)|… / …|… / …|… / …|
|Mass, ㎏|1.7|1.3|1.7|
|[Overload](vibration.md), Grms| | | |
|[Radiation](ion_rad.md), ㏉(㎭)| | | |
|[Reliability](qm.md)| | | |
|[Thermal](tcs.md), ℃| | | |
|[TRL](trl.md)|9|9|9|
|[Voltage](sps.md), V| | | |
|**【Specific】**|~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[NTO+UDMH](nto_plus.md)<br> (H₂,H₂O,CO,CO₂,N₂)|[NTO+UDMH](nto_plus.md)<br> (H₂,H₂O,CO,CO₂,N₂)|[NTO+UDMH](nto_plus.md)<br> (H₂,H₂O,CO,CO₂,N₂)|
|Fuel: consump., ㎏/s, ≤|0.065.139・ 0.063.495|0.072.727|0.068.027|
|Fuel: mass ratio|1.85|1.85|1.85|
|Fuel: resource, ㎏, ≥|1 270|364|682|
|Pres.: c.chamber, ㎫(㍴)|0.74 (7.5)|1.5 (15)|0.7 (7)|
|Pres.: inlet, ㎫(㍴)|1.81 (18.5)|2.5 (25)|1.6 (16)|
|Pres.: nozzle cut, ㎫(㍴)| | | |
|T°: comb.chamber, К(℃)|3 300 ‑ 3 800 (3 000 ‑ 3 500)|3 300 ‑ 3 800 (3 000 ‑ 3 500)|3 300 ‑ 3 800 (3 000 ‑ 3 500)|
|T°: nozzle cut, К(℃)| | | |
|Thrust, N|196.2|200|200|
|Thrust: 1 thrust, s|0.03 ‑ 2 500|0.05 ‑ 300|0.05 ‑ 4 000|
|Thrust: [Ing](ing.md), N·s, ≤| | | |
|Thrust: [Isp](ps.md), s, ≥|307・ 315|280|299|
|Thrust: switch.rate, ㎐, ≤| | | |
|Thrust: torch angle, °| | | |
|Thrust: Σ pulses, ≥|45 000|10 000|100 000|
|Thrust: Σ thrust, N, ≥|3 830 000・ 3 930 000|1 000 000|2 000 000|
|Thrust: Σ time, s(h), ≥|20 000|5 000|10 000|
|[Turbopump](turbopump.md), rpm|—|—|—|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. **[1]** — see [KBHM](kbhm.md) site, 2016 y.
1. **[2]** — can be achieved by upgrading.
1. They put forward requirements for fuel degassing. They work poorly with [CINU](cinu.md).
1. **Applicability** — С5.146 — ISS in 2016 ‑ 2018 y.



### KBHM S5.154   ［RU・2comp.］
**S5.154** (ru. **С5.154, КТД** (корректирующе‑тормозной двигатель)) — 2‑component [engine](ps.md) by [KBHM](kbhm.md)

|**Characteristics**|**S5.154**|
|:--|:--|
|Composition| |
|Consumption, W|130|
|Dimensions, ㎜|280×340×860|
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|720 (30 days) **⁽² ³⁾** / …|
|Mass, ㎏|54|
|[Overload](vibration.md), Grms|…, ↕10g, ↔5g|
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃|−50 ‑ +70 (structures), −5 ‑ +35 (structures w/ fuel), +5 ‑ +35 (fuel)|
|[TRL](trl.md)|9|
|[Voltage](sps.md), V|27±3|
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[NTO+UDMH](nto_plus.md) (H₂,H₂O,CO,CO₂,N₂)|
|Fuel: consump., ㎏/s, ≤|1.254.638 - 1.505.438|
|Fuel: mass ratio|1.886 ± 0.1|
|Fuel: resource, ㎏, ≥|1 580|
|Pres.: c.chamber, ㎫(㍴)|4.02 ‑ 4.78 (39.7 ‑ 47.2) |
|Pres.: inlet, ㎫(㍴)|1.96 (20)|
|Pres.: nozzle cut, ㎫(㍴)|0.000742 (0.00728)|
|T°: comb.chamber, К(℃)|2 500 (2 300)|
|T°: nozzle cut, К(℃)|690 (420)|
|Thrust, N|3 922 - 4 706|
|Thrust: 1 thrust, s|5 - 460|
|Thrust: [Ing](ing.md), N·s, ≤|1 961±392|
|Thrust: [Isp](ps.md), s, ≥|319±3|
|Thrust: switch.rate, ㎐, ≤|1 pulse/15 s for landing; 1 pulse/9 000 s for flight|
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥|10 **⁽¹ ³⁾**|
|Thrust: Σ thrust, N, ≥|4 510 300 - 5 411 900|
|Thrust: Σ time, s(h), ≥|1 150|
|[Turbopump](turbopump.md), rpm|<mark>TBD</mark>|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 3.5rd second after energizing the solenoid valves.
1. **[1,2,4]** — see ТУ.
1. **[3]** — requires clarification. Potentially, the values can be much higher.
1. They put forward requirements for fuel degassing. They work poorly with [CINU](cinu.md).
1. Turbopump consumes 1.1 ㎏ of fuel for the start, 1.7 — for the stop.
1. **Applicability** — [Luna‑25](луна_25.md)



### KBHM S5.175   ［RU・2comp.］
**S5.175** (**С5.175, КТД** (корректирующе‑тормозной двигатель)) — 2‑component [engine](ps.md) by [KBHM](kbhm.md)

|**Characteristics**|**S5.175**|
|:--|:--|
|Composition| |
|Consumption, W|130|
|Dimensions, ㎜|280×340×860 [1, 2]|
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|720 (30 d) / …|
|Mass, ㎏|54|
|[Overload](vibration.md), Grms|…, ↕10g, ↔5g|
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃|−50 ‑ +70 (structures), −5 ‑ +35 (structures w/ fuel), +5 ‑ +35 (fuel)|
|[TRL](trl.md)|9|
|[Voltage](sps.md), V|27±3|
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[NTO+UDMH](nto_plus.md) (H₂,H₂O,CO,CO₂,N₂)|
|Fuel: consump., ㎏/s, ≤|1.254.638 - 1.726.167|
|Fuel: mass ratio|1.886 ± 0.1|
|Fuel: resource, ㎏, ≥|1 580|
|Pres.: c.chamber, ㎫(㍴)| |
|Pres.: inlet, ㎫(㍴)|1.96 (20)|
|Pres.: nozzle cut, ㎫(㍴)|0.000742 (0.00728)|
|T°: comb.chamber, К(℃)|2 500 (2 300)|
|T°: nozzle cut, К(℃)|690 (420)|
|Thrust, N|3 922-5 396|
|Thrust: 1 thrust, s|… ‑ 460|
|Thrust: [Ing](ing.md), N·s, ≤|1 961±392|
|Thrust: [Isp](ps.md), s, ≥|319±3|
|Thrust: switch.rate, ㎐, ≤|1 pulse/15 s for landing; 1 pulse/9 000 s for flight|
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥|10 **⁽¹ ²⁾**|
|Thrust: Σ thrust, N, ≥|4 950 000|
|Thrust: Σ time, s(h), ≥|1 150|
|[Turbopump](turbopump.md), rpm|<mark>TBD</mark>|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 3.5rd second after energizing the solenoid valves.
1. **[1]** — see ТУ С5.154.0000‑0 ТУ‑6, 2015 y.
1. **[2]** — requires clarification. Potentially, the values can be much higher.
1. They put forward requirements for fuel degassing. They work poorly with [CINU](cinu.md).
1. Designed for the needs of the [Luna-27](луна_27.md) based on the engine [С5.154](engine_lst.md).
1. **Applicability** — [Luna‑27](луна_27.md)



### Nammo LEROS   ［US・2comp.］
**LEROS** — 2‑component [engine](ps.md) by [Moog inc](moog_inc.md). Manufacturing in UK. Classified as [HTAE](htae.md).

|**Characteristics**|**LEROS 1b**|**LEROS 1c**|**LEROS 2b**|**LEROS 4**|
|:--|:--|:--|:--|:--|
|Composition|1 unit|1 unit|1 unit|1 unit|
|Consumption, W|100|100| | |
|Dimensions, ㎜|540 × 289|527 × 288|671 × 334|1 106 × 500|
|[Interfaces](interface.md)| | | | |
|[Lifetime](lifetime.md), h(y)|… (19)|… (19)|… (19)|… (19)|
|Mass, ㎏|4.5|4.3|5|8.41|
|[Overload](vibration.md), Grms| | | | |
|[Radiation](ion_rad.md), ㏉(㎭)| | | | |
|[Reliability](qm.md)|0.995|0.995|0.995|0.995|
|[Thermal](tcs.md), ℃| |−53 ‑ +65 (+10 ‑ +35 fuel)|−53 ‑ +65 (+4 ‑ +40 fuel)|−50 ‑ +65 (0 ‑ +40 fuel)|
|[TRL](trl.md)|9|9|9|6|
|[Voltage](sps.md), V| |27.5 ‑ 35.5|19 ‑ 27|18 ‑ 27|
|**【Specific】**|~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[NTO+Hydrazine](nto_plus.md)<br> (H₂,H₂O,NH₃,N₂)|[NTO+Hydrazine](nto_plus.md)<br> (H₂,H₂O,NH₃,N₂)|[NTO+MMH](nto_plus.md)<br> ()|MON3+MMH<br> ()|
|Fuel: consump., ㎏/s, ≤|0.204.180|0.144.115|0.130.532|0.352.790|
|Fuel: mass ratio|0.8 ‑ 0.9|0.78 ‑ 0.85 ‑ 0.89|1.43 ‑ 1.65 ‑ 1.81|1.5 ‑ 1.65 ‑ 1.8|
|Fuel: resource, ㎏, ≥|4 180|4 760|4 000|4 220|
|Pres.: c.chamber, ㎫(㍴)| | | | |
|Pres.: inlet, ㎫(㍴)|1.5 ‑ 2 (15 ‑ 20)|1.3 ‑ 2 (13 ‑ 21)|1.3 ‑ 1.5 ‑ 1.7 (13 ‑ 15.4 ‑ 18)|1.3 ‑ 1.5 ‑ 1.7 (13 ‑ 15.4 ‑ 18)|
|Pres.: nozzle cut, ㎫(㍴)| | | | |
|T°: comb.chamber, К(℃)| | | | |
|T°: nozzle cut, К(℃)| |1 623 (1 350)|1 623 (1 350)|1 623 (1 350)|
|Thrust, N|587 ‑ 635 ‑ 707|386 ‑ 458 ‑ 470|367 ‑ 420 ‑ 456|900 ‑ 1 000 ‑ 1 100|
|Thrust: 1 thrust, s|… ‑ 2 520|… ‑ 5 800|… ‑ 6 600|3 600|
|Thrust: [Ing](ing.md), N·s, ≤| | | | |
|Thrust: [Isp](ps.md), s, ≥|317|324|319|321|
|Thrust: switch.rate, ㎐, ≤| | | |17 ‑ 34|
|Thrust: torch angle, °| | | | |
|Thrust: Σ pulses, ≥|10 500|10 500|10 500|70|
|Thrust: Σ thrust, N, ≥|13 000 000|13 200 000|13 600 000| |
|Thrust: Σ time, s(h), ≥|20 500|40 000|30 600|12 300|
|[Turbopump](turbopump.md), rpm|—|—|—|—|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. Docs: [Upper Stage Engines Rev 0913 ❐](f/ps/l/leros_upper_stage_engines_rev_0913.djvu) ~~ [An overview of LEROS 4, Space Propulsion 2014 Conference ❐](f/ps/l/leros_sp2014_2969298.djvu) ~~ [An overview of LEROS 4, Space Propulsion 2012 Conference ❐](f/ps/l/leros_sp2012_2394092_witherrata.djvu)
1. <https://en.wikipedia.org/wiki/LEROS>
1. <https://www.nammo.com/product/leros-1c/>
1. **[1]** — see Upper Stage Engines Rev 0913, 2018 y.
1. **[2]** — see An overview of LEROS 4, Space Propulsion 2014 Conference, 2014 y.
1. **[3]** — see An overview of LEROS 4, Space Propulsion 2012 Conference, 2012 y.
1. **Applicability** — A2100 platform (including GPS Ⅲ, GOES-R/S, & SBIRS)・ 702MP Platform (including Intelsat)・ Mercury MESSENGER・ Mars Global Surveyor・ Mars Climate Orbiter・ Mars Odyssey・ [Juno](juno.md)



### NIIMASH 11D428A-16   ［RU・2comp.］
**11D428A-16** (ru. **11Д428А-16**) — 2‑component [engine](ps.md) by [NIIMASH](niimash.md).

|**Characteristics**|**11D428A-16**|**11D428AF-16**|
|:--|:--|:--|
|Composition| | |
|Consumption, W| | |
|Dimensions, ㎜|290 × …|372 × 158|
|[Interfaces](interface.md)| | |
|[Lifetime](lifetime.md), h(y)|… / …|… / …|
|Mass, ㎏|1.5|1.9|
|[Overload](vibration.md), Grms| | |
|[Radiation](ion_rad.md), ㏉(㎭)| | |
|[Reliability](qm.md)| | |
|[Thermal](tcs.md), ℃| | |
|[TRL](trl.md)|9|9|
|[Voltage](sps.md), V| | |
|**【Specific】**|~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[NTO+UDMH](nto_plus.md) (H₂,H₂O,CO,CO₂,N₂)|[NTO+UDMH](nto_plus.md) (H₂,H₂O,CO,CO₂,N₂)|
|Fuel: consump., ㎏/s, ≤|0.045.287|0.041.139|
|Fuel: mass ratio|1.85 ± 0.15|1.85 ± 0.05|
|Fuel: resource, ㎏, ≥|2 250|2 042|
|Pres.: c.chamber, ㎫(㍴)| | |
|Pres.: inlet, ㎫(㍴)|0.98 ‑ 1.86 (9.8 ‑ 18.6)|1.37 ‑ 1.57 (13.7 ‑ 15.7)|
|Pres.: nozzle cut, ㎫(㍴)| | |
|T°: comb.chamber, К(℃)| | |
|T°: nozzle cut, К(℃)| | |
|Thrust, N|129.16|123.5|
|Thrust: 1 thrust, s|0.03 ‑ 2 000|0.03 ‑ 2 000|
|Thrust: [Ing](ing.md), N·s, ≤| | |
|Thrust: [Isp](ps.md), s, ≥|290|306|
|Thrust: switch.rate, ㎐, ≤| | |
|Thrust: torch angle, °| | |
|Thrust: Σ pulses, ≥|500 000|500 000|
|Thrust: Σ thrust, N, ≥|6 458 000|6 175 000|
|Thrust: Σ time, s(h), ≥|50 000|50 000|
|[Turbopump](turbopump.md), rpm|—|—|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 0.5nd second after energizing the solenoid valves.
1. **[1]** — see [NIIMASH](niimash.md) site, 2016 y.
1. **Applicability:**
   1. **17D428A-16** — Orbital manned station Salyut・ Orbital manned station Mir・ Soyuz-TM・ Progress-M・ Service module Zvezda at the International Space Station Astrophysical Observatory Gamma
   1. **17D428AF-16** — Fobos-Grunt, PS of the returning module



### NIIMASH 11D457   ［RU・2comp.］
**11D457** (ru. **11Д457**) — 2‑component [engine](ps.md) by [NIIMASH](niimash.md).

|**Characteristics**|**11D457**|**11D457F**|
|:--|:--|:--|
|Composition| | |
|Consumption, W| | |
|Dimensions, ㎜|236 × …|257 × 68|
|[Interfaces](interface.md)| | |
|[Lifetime](lifetime.md), h(y)|… / …|1 825 (5) / …|
|Mass, ㎏|1.2|1.2|
|[Overload](vibration.md), Grms| | |
|[Radiation](ion_rad.md), ㏉(㎭)| | |
|[Reliability](qm.md)| | |
|[Thermal](tcs.md), ℃| | |
|[TRL](trl.md)|9|9|
|[Voltage](sps.md), V| | |
|**【Specific】**|~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[NTO+UDMH](nto_plus.md) (H₂,H₂O,CO,CO₂,N₂)|[NTO+UDMH](nto_plus.md) (H₂,H₂O,CO,CO₂,N₂)|
|Fuel: consump., ㎏/s, ≤|0.021.655|0.019.001|
|Fuel: mass ratio|1.85 ± 0.15| |
|Fuel: resource, ㎏, ≥| |109|
|Pres.: c.chamber, ㎫(㍴)| | |
|Pres.: inlet, ㎫(㍴)|1.05 ‑ 1.35 (10 ‑ 13)|1.18 (12)|
|Pres.: nozzle cut, ㎫(㍴)| | |
|T°: comb.chamber, К(℃)| | |
|T°: nozzle cut, К(℃)| | |
|Thrust, N|53.9|54|
|Thrust: 1 thrust, s|0.03 ‑ 300|0.03 ‑ 2 000|
|Thrust: [Ing](ing.md), N·s, ≤| |2.45|
|Thrust: [Isp](ps.md), s, ≥|253|290|
|Thrust: switch.rate, ㎐, ≤| |8|
|Thrust: torch angle, °| | |
|Thrust: Σ pulses, ≥|100 000|100 000|
|Thrust: Σ thrust, N, ≥| |310 000|
|Thrust: Σ time, s(h), ≥| |5 750|
|[Turbopump](turbopump.md), rpm|—|—|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. **[1]** — see Specification & [NIIMASH](niimash.md) site, 2016 y.
1. **Applicability:**
   1. **11D457** — manned station 「Yantar」・ SC 「Neman」・ SC 「Sapphire」・ SC 「Kobalt-M」・ SC 「Orlets」・ SC 「Kometa」・ SC 「Don」・ SC 「Lotos」・ SC 「Resurs-Dk」
   1. **11D457F** — [Luna‑26](луна_26.md) ~~ [Spektr-M](спектр_м.md)



### NIIMASH 11D458   ［RU・2comp.］
**11D458** (ru. **11Д458**) — 2‑component [engine](ps.md) by [NIIMASH](niimash.md).

|**Characteristics**|**11D458**|**11D458M**|**11D458F**|
|:--|:--|:--|:--|
|Composition| | | |
|Consumption, W| | | |
|Dimensions, ㎜|370 × …|470 × 192|465 × 202|
|[Interfaces](interface.md)| | | |
|[Lifetime](lifetime.md), h(y)|… / …|… / …|1 825 (5) / …|
|Mass, ㎏|2.5|3|3|
|[Overload](vibration.md), Grms| | | |
|[Radiation](ion_rad.md), ㏉(㎭)| | | |
|[Reliability](qm.md)| | | |
|[Thermal](tcs.md), ℃| | | |
|[TRL](trl.md)|9|9|9|
|[Voltage](sps.md), V| | | |
|**【Specific】**|~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[NTO+UDMH](nto_plus.md)<br> (H₂,H₂O,CO,CO₂,N₂)|[NTO+UDMH](nto_plus.md)<br> (H₂,H₂O,CO,CO₂,N₂)|[NTO+UDMH](nto_plus.md)<br> (H₂,H₂O,CO,CO₂,N₂)|
|Fuel: consump., ㎏/s, ≤|0.158.704|0.132.433|0.128.091|
|Fuel: mass ratio|1.85 ± 0.15|1.85 ± 0.15|1.85 ± 0.15|
|Fuel: resource, ㎏, ≥| |132; 509|350|
|Pres.: c.chamber, ㎫(㍴)| | | |
|Pres.: inlet, ㎫(㍴)|0.98 ‑ 1.96 (9.8 ‑ 19.6)|1.27 ‑ 1.97 (12.7 ‑ 19.7)|1.13 ‑ 1.23 (11.3 ‑ 12.3)|
|Pres.: nozzle cut, ㎫(㍴)| | | |
|T°: comb.chamber, К(℃)| | | |
|T°: nozzle cut, К(℃)| | | |
|Thrust, N|392|392.4|382|
|Thrust: 1 thrust, s|0.1 ‑ 3 000|0.05 ‑ 1 000; 0.05 ‑ 1 500|0.05 ‑ 2 000|
|Thrust: [Ing](ing.md), N·s, ≤| | |9.41|
|Thrust: [Isp](ps.md), s, ≥|252|302|304|
|Thrust: switch.rate, ㎐, ≤| | |8|
|Thrust: torch angle, °| | | |
|Thrust: Σ pulses, ≥|33 000|10 000|10 000|
|Thrust: Σ thrust, N, ≥| |392 000<br> 1 569 600|1 044 000|
|Thrust: Σ time, s(h), ≥| |1 000; 4 000|2 750|
|[Turbopump](turbopump.md), rpm|—|—|—|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 0.5nd second after energizing the solenoid valves.
1. **[1,2,3]** — see Specification & [NIIMASH](niimash.md) site, 2016 y.
1. **Applicability:**
   1. **11Д458** — [Briz](бриз.md)・ Orbital manned station 「Almaz」・ Orbital station 「Mir」・ FGB 「Zarya」 (ISS)
   1. **11Д458М** — [Briz](бриз.md) ~~ [Luna‑26](луна_26.md)
   1. **11Д458Ф** — [Spektr-M](спектр_м.md)



### NIIMASH 17D16   ［RU・2comp.］
**17D16** (ru. **17Д16**) — 2‑component [engine](ps.md) by [NIIMASH](niimash.md).

|**Characteristics**|**17D16**|
|:--|:--|
|Composition| |
|Consumption, W| |
|Dimensions, ㎜|360 × 88|
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|7|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[O+Kerosene](o_plus.md) ()|
|Fuel: consump., ㎏/s, ≤|0.077.826|
|Fuel: mass ratio| |
|Fuel: resource, ㎏, ≥|355|
|Pres.: c.chamber, ㎫(㍴)| |
|Pres.: inlet, ㎫(㍴)|2.45 ‑ 5.88 (24.5 ‑ 58.8)|
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|196.2|
|Thrust: 1 thrust, s|0.06 ‑ 180|
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|257|
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥|40 000|
|Thrust: Σ thrust, N, ≥|902 500|
|Thrust: Σ time, s(h), ≥|4 600|
|[Turbopump](turbopump.md), rpm|—|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 0.5nd second after energizing the solenoid valves.
1. [Описание двигателя ❐](f/ps/0/17d16_doc_20180130.pdf) (2018 год)
1. **[1]** — see [NIIMASH](niimash.md) site, 2016 y.
1. **[2]** — see engine description from 2018 y.
1. **Applicability** — 「Buran」, the engine designed to control the orientation in space at the command of the control system.



### NIIMASH 17D58E   ［RU・2comp.］
**17D58E** (ru. **17Д58Э**) — 2‑component [engine](ps.md) by [NIIMASH](niimash.md).

|**Characteristics**|**17D58E**|**17D58EF**|
|:--|:--|:--|
|Composition| | |
|Consumption, W| | |
|Dimensions, ㎜|137 × 43|137 × 43|
|[Interfaces](interface.md)| | |
|[Lifetime](lifetime.md), h(y)|5 475 (15) / …|… / …|
|Mass, ㎏|0.5|0.55|
|[Overload](vibration.md), Grms| | |
|[Radiation](ion_rad.md), ㏉(㎭)| | |
|[Reliability](qm.md)| | |
|[Thermal](tcs.md), ℃| | |
|[TRL](trl.md)|9|9|
|[Voltage](sps.md), V| | |
|**【Specific】**|~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[NTO+UDMH](nto_plus.md) (H₂,H₂O,CO,CO₂,N₂)|[NTO+UDMH](nto_plus.md) (H₂,H₂O,CO,CO₂,N₂)|
|Fuel: consump., ㎏/s, ≤|0.004.966.628|0.004.881.204|
|Fuel: mass ratio|1.85 ± 0.2|1.85 ± 0.2|
|Fuel: resource, ㎏, ≥|893| |
|Pres.: c.chamber, ㎫(㍴)| | |
|Pres.: inlet, ㎫(㍴)|1.47 (15)|1.13 ‑ 3 (11.3 ‑ 30)|
|Pres.: nozzle cut, ㎫(㍴)| | |
|T°: comb.chamber, К(℃)| | |
|T°: nozzle cut, К(℃)| | |
|Thrust, N|13.35 ± 10|12.45|
|Thrust: 1 thrust, s|0.03 ‑ 10 000|0.03 ‑ 10 000|
|Thrust: [Ing](ing.md), N·s, ≤|0.32| |
|Thrust: [Isp](ps.md), s, ≥|274|260|
|Thrust: switch.rate, ㎐, ≤|10| |
|Thrust: torch angle, °| | |
|Thrust: Σ pulses, ≥|450 000|450 000|
|Thrust: Σ thrust, N, ≥|2 400 000| |
|Thrust: Σ time, s(h), ≥|180 000| |
|[Turbopump](turbopump.md), rpm|—|—|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 0.5nd second after energizing the solenoid valves.
1. **[1]** — see [NIIMASH](niimash.md) site, 2016 y.
1. **[2]** — see 17Д58Э.000.00 ТУ11, 1984 y.
1. **Applicability:**
   1. **17D458E** — Almaz・ Mir (modules 「Kvant」, 「Kristall」, 「Spectrum」, 「Nature」)・ FGB 「Zarya」 (ISS)
   1. **17D458EF** — [Luna‑26](луна_26.md) ~~ [Spektr-M](спектр_м.md) ~~ [Fobos-Grunt](фобос_грунт.md)



### NIIMASH MD08   ［RU・gas］
**MD08** (ru. **МД08**) — gas [engine](ps.md) by [NIIMASH](niimash.md).

|**Characteristics**|**MD08**|**MD08-02**|
|:--|:--|:--|
|Composition| | |
|Consumption, W|10.2|10.2|
|Dimensions, ㎜|93 × ⌀8|93 × ⌀8|
|[Interfaces](interface.md)| | |
|[Lifetime](lifetime.md), h(y)|1 095 (3) / …|1 095 (3)  / …|
|Mass, ㎏|0.25|0.25|
|[Overload](vibration.md), Grms| | |
|[Radiation](ion_rad.md), ㏉(㎭)| | |
|[Reliability](qm.md)|0.999|0.999|
|[Thermal](tcs.md), ℃|−10 ‑ +50 for structures|−10 ‑ +50 for structures|
|[TRL](trl.md)|9|9|
|[Voltage](sps.md), V|27 (20 ‑ 34)|27 (20 ‑ 34)|
|**【Specific】**|~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[N](азот.md) ・ [He](гелий.md)|[N](азот.md) ・ [He](гелий.md)|
|Fuel: consump., ㎏/s, ≤|0.001.143.647 ・ 0.000.442.128|0.001.143.647 ・ 0.000.442.128|
|Fuel: mass ratio|—|—|
|Fuel: resource, ㎏, ≥|11.4 ・ 4.4|11.4 ・ 4.4|
|Pres.: c.chamber, ㎫(㍴)| | |
|Pres.: inlet, ㎫(㍴)|1.47 ‑ 1.96 (15 ‑ 20)|1.47 ‑ 1.96 (15 ‑ 20)|
|Pres.: nozzle cut, ㎫(㍴)| | |
|T°: comb.chamber, К(℃)| | |
|T°: nozzle cut, К(℃)| | |
|Thrust, N|0.819±0.0082・ 0.733±0.0074|0.819±0.0082・ 0.733±0.0074|
|Thrust: 1 thrust, s|0.05 ‑ 100|0.05 ‑ 100|
|Thrust: [Ing](ing.md), N·s, ≤| | |
|Thrust: [Isp](ps.md), s, ≥|73 ・ 169|73 ・ 169|
|Thrust: switch.rate, ㎐, ≤|10|10|
|Thrust: torch angle, °| | |
|Thrust: Σ pulses, ≥|80 000|80 000|
|Thrust: Σ thrust, N, ≥|8 190 ・ 7 330|8 190 ・ 7 330|
|Thrust: Σ time, s(h), ≥|10 000|10 000|
|[Turbopump](turbopump.md), rpm|—|—|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 0.02nd second after energizing the solenoid valves.
1. **[1,2]** — see Specification & [NIIMASH](niimash.md) site, 2016 y.
1. **Applicability** — [Fobos‑Grunt](фобос_грунт.md) (MD08-02)



### NIIMASH MD5   ［RU・gas］
**MD5** (ru. **МД5**) — gas [engine](ps.md) by [NIIMASH](niimash.md). Archived.

|**Characteristics**|**MD5**|
|:--|:--|
|Composition| |
|Consumption, W|1.35|
|Dimensions, ㎜|91 × ⌀20|
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|0.35|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V|27 (20 ‑ 34)|
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[N](азот.md) ・ [Air](воздух.md) (N ・ Air)|
|Fuel: consump., ㎏/s, ≤|0.007.281.199|
|Fuel: mass ratio| |
|Fuel: resource, ㎏, ≥|214.79|
|Pres.: c.chamber, ㎫(㍴)| |
|Pres.: inlet, ㎫(㍴)|1.08 ‑ 1.57 (10 ‑ 15)|
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|5|
|Thrust: 1 thrust, s|0.012 ‑ 3 000|
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|70|
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥|250 000|
|Thrust: Σ thrust, N, ≥|147 500|
|Thrust: Σ time, s(h), ≥|29 500 (8.2)|
|[Turbopump](turbopump.md), rpm|—|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 0.02nd second after energizing the solenoid valves.
1. **[1]** — see [NIIMASH](niimash.md) site, 2016 y.
1. **Applicability** — …



### Pale Blue WIT   ［JP・el］
**Water Ion Thruster (WIT)** — electric [engine](ps.md) by [Pale Blue Inc](pale_blue_inc.md).

|**Characteristics**|**Water Ion Thruster**|
|:--|:--|
|Composition|engine, tanks, structures (in fact, the entire PS)|
|Consumption, W|30 ‑ 60|
|Dimensions, ㎜|… (0.7U)|
|[Interfaces](interface.md)|UART, RS422|
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|1.6 w/o tank, 0.2 for propellant|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃|+4 ‑ +49 for operations, 0 ‑ +68 for storage|
|[TRL](trl.md)|8|
|[Voltage](sps.md), V|5 or 12|
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|Water (O,H)|
|Fuel: consump., ㎏/s, ≤|0.000.000.027.726 ‑ 0.000.000.032.223|
|Fuel: mass ratio|—|
|Fuel: resource, ㎏, ≥|0.2 ‑ 0.35|
|Pres.: c.chamber, ㎫(㍴)| |
|Pres.: inlet, ㎫(㍴)| |
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|0.000136 ‑ 0.000306|
|Thrust: 1 thrust, s|… ‑ … |
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|500 ‑ 968|
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥| |
|Thrust: Σ thrust, N, ≥|981 ‑ 3 323|
|Thrust: Σ time, s(h), ≥|7 213 300 ‑ 10 859 500 (2 003 ‑ 3 016)|
|[Turbopump](turbopump.md), rpm|—|
| |[![](f/ps/w/water_it_pic1t.webp)](f/ps/w/water_it_pic1.webp)|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the …nd second after energizing the solenoid valves.
1. [Datasheet ❐](f/ps/w/water_it_doc1.pdf)
1. **Applicability** — …



### Pale Blue WRT   ［JP・el］
**Water Resistojet Thruster (WRT)** — electric [engine](ps.md) by [Pale Blue Inc](pale_blue_inc.md).

|**Characteristics**|**Water Resistojet Thruster**|
|:--|:--|
|Composition|engine, tanks, structures (in fact, the entire PS)|
|Consumption, W|5 ‑ 20|
|Dimensions, ㎜|… (0.5U)|
|[Interfaces](interface.md)|UART, RS422|
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|0.8 w/o tank, 0.4 for propellant |
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃|+4 ‑ +49 for operations, 0 ‑ +68 for storage|
|[TRL](trl.md)|9|
|[Voltage](sps.md), V|5 or 8|
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|Water (O,H)|
|Fuel: consump., ㎏/s, ≤|0.000.001.456.239 ‑ … ‑ 0.000.004.077.471|
|Fuel: mass ratio|—|
|Fuel: resource, ㎏, ≥|0.1 ‑ … ‑ 1.1|
|Pres.: c.chamber, ㎫(㍴)| |
|Pres.: inlet, ㎫(㍴)| |
|Pres.: nozzle cut, ㎫(㍴)|0.06|
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|0.001 ‑ 0.002 ‑ 0.004|
|Thrust: 1 thrust, s|… ‑ … |
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|70 ‑ … ‑ 100|
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥| |
|Thrust: Σ thrust, N, ≥|68 ‑ … ‑ 1 080|
|Thrust: Σ time, s(h), ≥|67 500 ‑ … ‑ 270 000 (18.75 ‑ 75)|
|[Turbopump](turbopump.md), rpm|—|
| |[![](f/ps/w/water_rjt_pic1t.webp)](f/ps/w/water_rjt_pic1.webp)|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the …nd second after energizing the solenoid valves.
1. [Datasheet ❐](f/ps/w/water_rjt_doc1.pdf)
1. **Applicability** — …



### SpaceX Draco   ［US・2comp.］
**Draco** — a family of 2-component [engines](ps.md) by [SpaceX](spacex.md).

|**Characteristics**|**Draco**|**SuperDraco**|
|:--|:--|:--|
|Composition| | |
|Consumption, W| | |
|Dimensions, ㎜|14 000 × …| |
|[Interfaces](interface.md)| | |
|[Lifetime](lifetime.md), h(y)|… / …|… / …|
|Mass, ㎏| | |
|[Overload](vibration.md), Grms| | |
|[Radiation](ion_rad.md), ㏉(㎭)| | |
|[Reliability](qm.md)| | |
|[Thermal](tcs.md), ℃| | |
|[TRL](trl.md)|9|6|
|[Voltage](sps.md), V| | |
|**【Specific】**|~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[NTO+MMH](nto_plus.md) ()|[NTO+MMH](nto_plus.md) ()|
|Fuel: consump., ㎏/s, ≤|0.135.915|30.148|
|Fuel: mass ratio| | |
|Fuel: resource, ㎏, ≥| |1 388|
|Pres.: c.chamber, ㎫(㍴)| |6.9 (67.7)|
|Pres.: inlet, ㎫(㍴)| | |
|Pres.: nozzle cut, ㎫(㍴)| | |
|T°: comb.chamber, К(℃)| | |
|T°: nozzle cut, К(℃)| | |
|Thrust, N|400|71 000|
|Thrust: 1 thrust, s|… ‑ … | |
|Thrust: [Ing](ing.md), N·s, ≤| | |
|Thrust: [Isp](ps.md), s, ≥|300|240|
|Thrust: switch.rate, ㎐, ≤| | |
|Thrust: torch angle, °| | |
|Thrust: Σ pulses, ≥| | |
|Thrust: Σ thrust, N, ≥| |1 770 000|
|Thrust: Σ time, s(h), ≥| |25|
|[Turbopump](turbopump.md), rpm| | |

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. <https://en.wikipedia.org/wiki/Draco_(rocket_engine_family)>
1. <https://en.wikipedia.org/wiki/SpaceX_rocket_engines>
1. <https://en.wikipedia.org/wiki/SuperDraco>
1. There are 2 variations: **Draco**; **SuperDraco** uses 3D printing, the combustion chamber is created from [inconel](nickel.md).
1. **Applicability:**
   1. **Draco** — [Dragon](dragon.md) ~~ [Dragon V2](dragon.md) ~~ [Falcon v.1.0](falcon.md), 2nd stage (2010‑2013)
   1. **SuperDraco** — [Dragon V2](dragon.md)



### SpaceX Merlin   ［US・2comp.］
**Merlin** — a family of 2‑components [engines](ps.md) by [SpaceX](spacex.md).

|**Characteristics**|**Merlin 1A**|**Merlin 1B**|**Merlin 1C**|**Merlin 1C Vac**|
|:--|:--|:--|:--|:--|
|Composition| | | | |
|Consumption, W| | | | |
|Dimensions, ㎜| | |2 920 × …| |
|[Interfaces](interface.md)| | | | |
|[Lifetime](lifetime.md), h(y)|… / …|… / …|… / …|… / …|
|Mass, ㎏| | |630| |
|[Overload](vibration.md), Grms| | | | |
|[Radiation](ion_rad.md), ㏉(㎭)| | | | |
|[Reliability](qm.md)| | | | |
|[Thermal](tcs.md), ℃| | | | |
|[TRL](trl.md)| | |9|9|
|[Voltage](sps.md), V| | | | |
|**【Specific】**|~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[O+Kerosene](o_plus.md)|[O+Kerosene](o_plus.md)|[O+Kerosene](o_plus.md)|[O+Kerosene](o_plus.md)|
|Fuel: consump., ㎏/s, ≤| |141.318|160.425|122.205|
|Fuel: mass ratio|2.17|2.17|2.17|2.17|
|Fuel: resource, ㎏, ≥| | |260 000|40 500|
|Pres.: c.chamber, ㎫(㍴)| | |6.77 (66.4)| |
|Pres.: inlet, ㎫(㍴)| | | | |
|Pres.: nozzle cut, ㎫(㍴)| | | | |
|T°: comb.chamber, К(℃)| | | | |
|T°: nozzle cut, К(℃)| | | | |
|Thrust, N|340 000|420 000|480 000|410 000|
|Thrust: 1 thrust, s|… ‑ … | | | |
|Thrust: [Ing](ing.md), N·s, ≤| | | | |
|Thrust: [Isp](ps.md), s, ≥| |303|305|342|
|Thrust: switch.rate, ㎐, ≤| | | | |
|Thrust: torch angle, °| | | | |
|Thrust: Σ pulses, ≥| | | | |
|Thrust: Σ thrust, N, ≥| | |778 080 000|135 700 000|
|Thrust: Σ time, s(h), ≥| | |1 620|330|
|[Turbopump](turbopump.md), rpm|—|—|—|—|
|[![](f/ps/be4_m1d_raptor_comparison1t.webp)](f/ps/be4_m1d_raptor_comparison1.webp)<br> **Comparison<br> [BE‑4](engine_lst.md), [Raptor](engine_lst.md), Merlin 1D**|[![](f/ps/m/merlin_1a_pic1t.webp)](f/ps/m/merlin_1a_pic1.webp)| |[![](f/ps/m/merlin_1c_pic1t.webp)](f/ps/m/merlin_1c_pic1.webp)|[![](f/ps/m/merlin_1c_vac_pic1t.webp)](f/ps/m/merlin_1c_vac_pic1.webp)|



**Continue:**

|**Characteristics**|**Merlin 1D**|**Merlin 1D Vac**|
|:--|:--|:--|
|Composition| | |
|Consumption, W| | |
|Dimensions, ㎜|… × 1 000| |
|[Interfaces](interface.md)| | |
|[Lifetime](lifetime.md), h(y)|… / …|… / …|
|Mass, ㎏|470|470|
|[Overload](vibration.md), Grms| | |
|[Radiation](ion_rad.md), ㏉(㎭)| | |
|[Reliability](qm.md)| | |
|[Thermal](tcs.md), ℃| | |
|[TRL](trl.md)|9|7|
|[Voltage](sps.md), V| | |
|**【Specific】**|~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[O+Kerosene](o_plus.md)|[O+Kerosene](o_plus.md)|
|Fuel: mass ratio|2.34|2.36|
|Fuel: consump., ㎏/s, ≤|119.833 ‑ 299.582|106.696 ‑ 273.588|
|Fuel: resource, ㎏, ≥| | |
|Pres.: c.chamber, ㎫(㍴)|9.7 (95.2)| |
|Pres.: inlet, ㎫(㍴)| | |
|Pres.: nozzle cut, ㎫(㍴)| | |
|T°: comb.chamber, К(℃)| | |
|T°: nozzle cut, К(℃)| | |
|Thrust, N|365 600 ‑ 914 000 (37 270 ‑ 93 170)|364 250 ‑ 934 000 (37 130 ‑ 95 208)|
|Thrust: 1 thrust, s|… ‑ … | |
|Thrust: [Ing](ing.md), N·s, ≤| | |
|Thrust: [Isp](ps.md), s, ≥|311 (3 051)|348 (3 413)|
|Thrust: switch.rate, ㎐, ≤| | |
|Thrust: torch angle, °| | |
|Thrust: Σ pulses, ≥| | |
|Thrust: Σ thrust, N, ≥| | |
|Thrust: Σ time, s(h), ≥| | |
|[Turbopump](turbopump.md), rpm| | |
| |[![](f/ps/m/merlin_1d_pic1t.webp)](f/ps/m/merlin_1d_pic1.webp)| |

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. <https://en.wikipedia.org/wiki/Merlin_(rocket_engine_family)>
1. 2014.10.05 Хабр: Превосходство Маска. О магии 「Мерлина」 замолвим слово (<https://habr.com/ru/post/236761>) — [archived ❐](f/archive/20141005_1.pdf) 2014.10.05
1. 2014.11.21 Why is the Merlin 1D specific impulse so low (<https://www.reddit.com/r/spacex/comments/2my35n/why_is_the_merlin_1d_specific_impulse_so_low>) — [archived ❐](f/archive/20141121_1.pdf) 2014.11.21
1. There are 6 variations: **Merlin 1A** — 2 pieces were made. **Merlin 1B** — it was a successor of 1A, 1 piece was made, but it never flew. **Merlin 1C**. **Merlin 1C Vac**. **Merlin 1D**. **Merlin 1D Vac**.
1. **[1]** — …
1. **Applicability:**
   1. **Merlin 1A** — [Falcon 1](falcon.md) (2006 ‑ 2007)
   1. **Merlin 1B** — [Falcon 1](falcon.md) (never)
   1. **Merlin 1C** — [Falcon 1](falcon.md) (2008) ~~ [Falcon 9](falcon.md) (2010 ‑ 2013)
   1. **Merlin 1C Vac** — [Falcon 9](falcon.md) (2010 ‑ 2013)
   1. **Merlin 1D** — [Falcon 9 v.1.1](falcon.md) (2013 ‑ 2018) ~~ [Falcon Heavy](falcon.md) (2018 ‑ 2018)
   1. **Merlin 1D Vac** — [Falcon 9 v.1.1](falcon.md) (2015 ‑ 2018) ~~ [Falcon Heavy](falcon.md) (2018 ‑ 2018)



### SpaceX Raptor   ［US・2comp.］
**Raptor** — a family of 2-component [engines](ps.md) by [SpaceX](spacex.md).

|**Characteristics**|**ER40**|**ER200**|**Raptor 2017‑SL**|**Raptor 2017‑V**|
|:--|:--|:--|:--|:--|
|Composition| | | | |
|Consumption, W| | | | |
|Dimensions, ㎜|3 300 × 1 730|6 400 × 3 870|… × 1 300|… × 2 400|
|[Interfaces](interface.md)| | | | |
|[Lifetime](lifetime.md), h(y)|… / …|… / …|… / …|… / …|
|Mass, ㎏|1 000|2 300| | |
|[Overload](vibration.md), Grms| | | | |
|[Radiation](ion_rad.md), ㏉(㎭)| | | | |
|[Reliability](qm.md)| | | | |
|[Thermal](tcs.md), ℃| | | | |
|[TRL](trl.md)|3|3|6|6|
|[Voltage](sps.md), V| | | | |
|**【Specific】**|~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[O+Methane](o_plus.md)<br> (H₂,H₂O,CO,CO₂)|[O+Methane](o_plus.md)<br> (H₂,H₂O,CO,CO₂)|[O+Methane](o_plus.md)<br> (H₂,H₂O,CO,CO₂)|[O+Methane](o_plus.md)<br> (H₂,H₂O,CO,CO₂)|
|Fuel: consump., ㎏/s, ≤|922.5|934.08|486.82|516.58|
|Fuel: mass ratio|3.5 ‑ 3.8|3.5 ‑ 3.8|3.5 ‑ 3.8|3.5 ‑ 3.8|
|Fuel: resource, ㎏, ≥| | | | |
|Pres.: c.chamber, ㎫(㍴)|5.9 (57) - 30.6 (300)|5.9 (57) - 30.6 (300)| | |
|Pres.: inlet, ㎫(㍴)| | | | |
|Pres.: nozzle cut, ㎫(㍴)|0.0735 (0.721)|0.09 (0.883)| | |
|T°: comb.chamber, К(℃)|3 250|3 250|3 250|3 250|
|T°: nozzle cut, К(℃)| | | | |
|Thrust, N|657 000 ‑ 3 285 000|700 000 ‑ 3 500 000|1 700 000|1 900 000|
|Thrust: 1 thrust, s|… ‑ … | | | |
|Thrust: [Ing](ing.md), N·s, ≤| | | | |
|Thrust: [Isp](ps.md), s, ≥|363|382|356|375|
|Thrust: switch.rate, ㎐, ≤| | | | |
|Thrust: torch angle, °| | | | |
|Thrust: Σ pulses, ≥| | | | |
|Thrust: Σ thrust, N, ≥| | | | |
|Thrust: Σ time, s(h), ≥| | | | |
|[Turbopump](turbopump.md), rpm|—|—|—|—|
|[![](f/ps/be4_m1d_raptor_comparison1t.webp)](f/ps/be4_m1d_raptor_comparison1.webp)<br> **Comparison<br> [BE‑4](engine_lst.md), [Raptor](engine_lst.md), Merlin 1D**| | | | |

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. <https://en.wikipedia.org/wiki/Raptor_(rocket_engine_family)>
1. <https://en.wikipedia.org/wiki/SpaceX_rocket_engines>
1. 2017.06.28 Хабр: Подробный разбор ЖРД 「Raptor」 (<https://geektimes.ru/post/290549>) — [archived ❐](f/archive/20170628_1.pdf) 2019.02.13
1. 2017.10.18 Всё, что известно о двигателях Раптор и даже больше (<https://elonmusk.su/vse-chto-izvestno-o-dvigatelyakh-raptor-i-dazhe-bolshe>) — [archived ❐](f/archive/20171018_1.pdf) 2019.02.13
1. It’s the successor to [Merlin](engine_lst.md). In contrast, it runs on the fuel [O + Methane](o_plus.md) & has a full‑flow closed cycle. For 2017 — the only full‑flow closed cycle engine in the world.
1. There are 4 variations: **ER40** — basic version, works in both atmosphere & vacuum. **ER200** — ER40 version for vacuum operation. **Raptor2017‑SL** — development of 2017, works in both atmosphere & vacuum. **Raptor2017-V** — 2017 version for vacuum operation.
1. **[1]** — …
1. **Applicability** — [BFR](bfr.md) ~~ [ITC](itc.md)

|Raptor Engine Test Site, 2017.01.30<br> Source 1 (<https://www.reddit.com/r/engineteststands/comments/43lmbn/spacexs_raptor_test_stand_under_construction_at>), Source 2 (<http://pictures.jtbuice.com/SpaceX-2/McGregor-Flyover-1-30-2016>)|
|:--|
|[![](f/ps/r/raptor_test_stand_20170130t.webp)](f/ps/r/raptor_test_stand_20170130.webp)|



## Archive

### Blue Origin BE-1   ［US・1comp.］
**BE-1** — 1‑component [engine](ps.md) by [Blue Origin](blue_origin.md).

|**Characteristics**|**BE-1**|
|:--|:--|
|Composition| |
|Consumption, W| |
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏| |
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)| |
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[H peroxide](hydrogen.md) (H₂,H₂O,O)|
|Fuel: consump., ㎏/s, ≤| |
|Fuel: mass ratio|—|
|Fuel: resource, ㎏, ≥| |
|Pres.: c.chamber, ㎫(㍴)| |
|Pres.: inlet, ㎫(㍴)| |
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|9 000|
|Thrust: 1 thrust, s|… ‑ … |
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥| |
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥| |
|Thrust: Σ thrust, N, ≥| |
|Thrust: Σ time, s(h), ≥| |
|[Turbopump](turbopump.md), rpm| |

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. <https://www.blueorigin.com/engines>
1. **Applicability** — …



### Blue Origin BE-2   ［US・2comp.］
**BE-7** — 2‑component [engine](ps.md) by [Blue Origin](blue_origin.md).

|**Characteristics**|**BE-2**|
|:--|:--|
|Composition| |
|Consumption, W| |
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏| |
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)| |
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[H peroxide](hydrogen.md) + Kerosene|
|Fuel: consump., ㎏/s, ≤| |
|Fuel: mass ratio| |
|Fuel: resource, ㎏, ≥| |
|Pres.: c.chamber, ㎫(㍴)| |
|Pres.: inlet, ㎫(㍴)| |
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|140 000|
|Thrust: 1 thrust, s|… ‑ … |
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥| |
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥| |
|Thrust: Σ thrust, N, ≥| |
|Thrust: Σ time, s(h), ≥| |
|[Turbopump](turbopump.md), rpm| |

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. <https://www.blueorigin.com/engines>
1. **Applicability** — …



### KBHM S5.78   ［RU・2comp.］
**S5.78** (ru. **С5.78**) — 2‑component [engine](ps.md) by [KBHM](kbhm.md). Archived.

|**Characteristics**|**S5.78**|
|:--|:--|
|Composition| | |
|Consumption, W| |
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|34.5|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[NTO+Hydrazine](nto_plus.md) (H₂,H₂O,NH₃,N₂)|
|Fuel: consump., ㎏/s, ≤|0.001.512.605|
|Fuel: mass ratio|1|
|Fuel: resource, ㎏, ≥|5 630|
|Pres.: c.chamber, ㎫(㍴)|1.77 (17.4)|
|Pres.: inlet, ㎫(㍴)|3.16 ‑ 4.48 (31 ‑ 44)|
|Pres.: nozzle cut, ㎫(㍴)|0.0087 (0.0853)|
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|6 000±10|
|Thrust: 1 thrust, s|… ‑ … |
|Thrust: [Ing](ing.md), N·s, ≤|1 982±392|
|Thrust: [Isp](ps.md), s, ≥|323|
|Thrust: switch.rate, ㎐, ≤|0.66|
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥|9|
|Thrust: Σ thrust, N, ≥|17 840 000|
|Thrust: Σ time, s(h), ≥|3 000|
|[Turbopump](turbopump.md), rpm|39 500|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. **[1]** — see document ФГ‑0000‑0ПЗ.5.
1. Has a turbopump without afterburning the working fluid of the turbine.
1. **Applicability** — [Fobos-Grunt](фобос_грунт.md)



### NIIMASH RDMT3   ［RU・2comp.］
**RDMT3** (ru. **РДМТ3**) — 2‑component [engine](ps.md) by [NIIMASH](niimash.md). Archived.

|**Characteristics**|**RDMT3**|
|:--|:--|
|Composition| |
|Consumption, W| |
|Dimensions, ㎜|137 × …|
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|0.31|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|4|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[NTO+UDMH](nto_plus.md) (H₂,H₂O,CO,CO₂,N₂)|
|Fuel: consump., ㎏/s, ≤|0.001.080.602|
|Fuel: mass ratio|1.85±0.2|
|Fuel: resource, ㎏, ≥|0.63|
|Pres.: c.chamber, ㎫(㍴)| |
|Pres.: inlet, ㎫(㍴)|1.47 (14.4)|
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|3|
|Thrust: 1 thrust, s|0.02 ‑ 600|
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|283|
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥|450 000|
|Thrust: Σ thrust, N, ≥|1 800|
|Thrust: Σ time, s(h), ≥|600|
|[Turbopump](turbopump.md), rpm|—|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 0.5nd second after energizing the solenoid valves.
1. **Applicability** — …



### NIIMASH RDMT10   ［RU・2comp.］
**RDMT10** (ru. **РДМТ10**) — 2‑component [engine](ps.md) by [NIIMASH](niimash.md). Archived.

|**Characteristics**|**RDMT10**|
|:--|:--|
|Composition| |
|Consumption, W| |
|Dimensions, ㎜|164 × …|
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|0.35|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|4|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[NTO+UDMH](nto_plus.md) (H₂,H₂O,CO,CO₂,N₂)|
|Fuel: consump., ㎏/s, ≤|0.004.146.581|
|Fuel: mass ratio|1.85±0.2|
|Fuel: resource, ㎏, ≥|41.3|
|Pres.: c.chamber, ㎫(㍴)| |
|Pres.: inlet, ㎫(㍴)|1.47 (14.4)|
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|12|
|Thrust: 1 thrust, s|0.02 ‑ 10 000|
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|295|
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥|450 000|
|Thrust: Σ thrust, N, ≥|120 000|
|Thrust: Σ time, s(h), ≥|10 000|
|[Turbopump](turbopump.md), rpm|—|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 1st second after energizing the solenoid valves.
1. **Applicability** — …



### NIIMASH RDMT50   ［RU・2comp.］
**RDMT50** (ru. **РДМТ50**) — 2‑component [engine](ps.md) by [NIIMASH](niimash.md). Archived.

|**Characteristics**|**RDMT50M**|
|:--|:--|
|Composition| |
|Consumption, W| |
|Dimensions, ㎜|253 × …|
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|1.3|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|4|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[NTO+UDMH](nto_plus.md) (H₂,H₂O,CO,CO₂,N₂)|
|Fuel: consump., ㎏/s, ≤|0.018.980|
|Fuel: mass ratio|1.85±0.2|
|Fuel: resource, ㎏, ≥| |
|Pres.: c.chamber, ㎫(㍴)| |
|Pres.: inlet, ㎫(㍴)|1.18 (11.5)|
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|54|
|Thrust: 1 thrust, s|0.03 ‑ 300|
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|290|
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥|100 000|
|Thrust: Σ thrust, N, ≥| |
|Thrust: Σ time, s(h), ≥| |
|[Turbopump](turbopump.md), rpm|—|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 0.5nd second after energizing the solenoid valves.
1. **Applicability** — …



### NIIMASH RDMT100   ［RU・2comp.］
**RDMT100 (ru. **РДМТ100**) — 2‑component [engine](ps.md) by [NIIMASH](niimash.md). Archived.

|**Characteristics**|**RDMT100-OH**|
|:--|:--|
|Composition| |
|Consumption, W| |
|Dimensions, ㎜|270 × …|
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|1.2|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|4|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[O+H](o_plus.md) (H₂,H₂O)|
|Fuel: consump., ㎏/s, ≤|0.026.968|
|Fuel: mass ratio|1.6 ‑ 1.7|
|Fuel: resource, ㎏, ≥| |
|Pres.: c.chamber, ㎫(㍴)| |
|Pres.: inlet, ㎫(㍴)|1.1 (10.8)|
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|100|
|Thrust: 1 thrust, s|0.01 ‑ 20|
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|378|
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥|100 000|
|Thrust: Σ thrust, N, ≥| |
|Thrust: Σ time, s(h), ≥| |
|[Turbopump](turbopump.md), rpm|—|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. **Applicability** — …



### NIIMASH RDMT2600   ［RU・2comp.］
**RDMT2600** (ru. **РДМТ2600**) — 2‑component [engine](ps.md) by [NIIMASH](niimash.md). Archived.

|**Characteristics**|**RDMT2600**|
|:--|:--|
|Composition| |
|Consumption, W| |
|Dimensions, ㎜|464 × …|
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|5|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|4|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|O₂ (gas) + ethyl alcohol (H₂,H₂O,CO,CO₂,N₂)|
|Fuel: consump., ㎏/s, ≤|1|
|Fuel: mass ratio|1.2|
|Fuel: resource, ㎏, ≥| |
|Pres.: c.chamber, ㎫(㍴)| |
|Pres.: inlet, ㎫(㍴)|4.5 ‑ 5.7 (44.1 ‑ 55.9)|
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|2 600|
|Thrust: 1 thrust, s|0.015 ‑ 15|
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|265|
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥|1 000|
|Thrust: Σ thrust, N, ≥| |
|Thrust: Σ time, s(h), ≥| |
|[Turbopump](turbopump.md), rpm|—|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. **Applicability** — …



### SpaceX Kestrel   ［US・2comp.］
**Kestrel** — 2‑component [engine](ps.md) by [SpaceX](spacex.md).

|**Characteristics**|**Kestrel**|
|:--|:--|
|Composition| |
|Consumption, W| |
|Dimensions, ㎜|0.7 × …|
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|52|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)|[O+Kerosene](o_plus.md)|
|Fuel: consump., ㎏/s, ≤|9.871.382|
|Fuel: mass ratio| |
|Fuel: resource, ㎏, ≥|987|
|Pres.: c.chamber, ㎫(㍴)|0.93 (9.12)|
|Pres.: inlet, ㎫(㍴)| |
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N|30 700|
|Thrust: 1 thrust, s|… ‑ … |
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥|317|
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥| |
|Thrust: Σ thrust, N, ≥|3 070 000|
|Thrust: Σ time, s(h), ≥|100|
|[Turbopump](turbopump.md), rpm|—|
| |![](f/ps/kestrel_engine_pic1.webp)|

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the 2nd second after energizing the solenoid valves.
1. <https://en.wikipedia.org/wiki/Kestrel_(rocket_engine)>
1. <https://en.wikipedia.org/wiki/SpaceX_rocket_engines>
1. Built on the basis of the engine [Merlin](engine_lst.md), but does not have [turbopump](turbopump.md). Has a drive.
1. **Applicability** — [Falcon 1](falcon.md), 2nd stage (2006‑2009)



## Template



### …   ［…・…comp.］
**…** — …‑component [engine](ps.md) by ….

|**Characteristics**|**…**|
|:--|:--|
|Composition| |
|Consumption, W| |
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏| |
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)| |
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|[Fuel](ps.md) (comb.products)| |
|Fuel: consump., ㎏/s, ≤| |
|Fuel: mass ratio| |
|Fuel: resource, ㎏, ≥| |
|Pres.: c.chamber, ㎫(㍴)| |
|Pres.: inlet, ㎫(㍴)| |
|Pres.: nozzle cut, ㎫(㍴)| |
|T°: comb.chamber, К(℃)| |
|T°: nozzle cut, К(℃)| |
|Thrust, N| |
|Thrust: 1 thrust, s|… ‑ … |
|Thrust: [Ing](ing.md), N·s, ≤| |
|Thrust: [Isp](ps.md), s, ≥| |
|Thrust: switch.rate, ㎐, ≤| |
|Thrust: torch angle, °| |
|Thrust: Σ pulses, ≥| |
|Thrust: Σ thrust, N, ≥| |
|Thrust: Σ time, s(h), ≥| |
|[Turbopump](turbopump.md), rpm| |

**Notes:**

1. Specs’re for nominal vacuum continuous thrust starting from the …nd second after energizing the solenoid valves.
1. **Applicability** — …



## Docs/Links
|Navigation|
|:--|
|**[FAQ](faq.md)**［**[SCS](sc.md)**·КК, **[SC (OE+SGM)](sc.md)**·КА］**[CON](contact.md)·[Pers](person.md)**·Контакт, **[Ctrl](control.md)**·Упр., **[Doc](doc.md)**·Док., **[EF](ef.md)**·ВВФ, **[Error](faq.md)**·Ошибки, **[Event](event.md)**·События, **[FS](fs.md)**·ТЭО, **[HF&E](hfe.md)**·Эрго., **[KT](kt.md)**·КТ, **[Model](draw.md)**·Модель, **[N&B](nnb.md)**·БНО, **[Project](project.md)**·Проект, **[QM](qm.md)**·БКНР, **[R&D](rnd.md)**·НИОКР, **[SI](si.md)**·СИ, **[Test](test.md)**·ЭО, **[TRL](trl.md)**·УГТ, **[Way](faq.md)**·Пути|
|**Sections & pages**|
|**`Двигательная установка (ДУ):`**<br> [HTAE](htae.md) ~~ [TALOS](talos.md) ~~ [Баки топливные](fuel_tank.md) ~~ [Варп‑двигатель](ps.md) ~~ [Газовый двигатель](ps.md) ~~ [Гибридный двигатель](гбрд.md) ~~ [Двигатель Бассарда](ps.md) ~~ [ЖРД](ps.md) ~~ [ИПТ](ing.md) ~~ [Ионный двигатель](иод.md) ~~ [Как считать топливо?](si.md) ~~ [КЗУ](cinu.md) ~~ [КХГ](cgs.md) ~~ [Номинал](nominal.md) ~~ [Мятый газ](exhsteam.md) ~~ [РДТТ](ps.md) ~~ [Сильфон](сильфон.md) ~~ [СОЗ](соз.md) ~~ [СОИС](соис.md) ~~ [Солнечный парус](солнечный_парус.md) ~~ [ТНА](turbopump.md) ~~ [Топливные мембраны](топливные_мембраны.md) ~~ [Топливные мешки](топливные_мешки.md) ~~ [Топливо](ps.md) ~~ [Тяговооружённость](ttwr.md) ~~ [ТЯРД](тярд.md) ~~ [УИ](ps.md) ~~ [Фотонный двигатель](фотонный_двигатель.md) ~~ [ЭРД](ps.md) ~~ [Эффект Оберта](oberth_eff.md) ~~ [ЯРД](ps.md)|

1. Docs: [Basic calculator ❐](f/ps/calc_ps_general.ods)
1. …


## The End

end of file
