# МАИ
> 2019.08.11 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/m/mai_logo1t.webp)](f/c/m/mai_logo1.webp)|<mai@mai.ru>, +7(499)158-43-33, Fax +7(499)158-29-77;<br> *Волоколамское шоссе, д. 4, г. Москва, 125993, Россия*<br> <https://mai.ru> ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/Moscow_Aviation_Institute)|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Моско́вский авиацио́нный институ́т (национальный исследовательский университет) (МАИ)**, англ. **Moscow Aviation Institute (National Research University) (MAI)** — российское высшее учебное заведение, расположенное в Москве. Основан 20 марта 1930 года. Член [IAF](iaf.md) с 1990 г.

Alumni of the institute form the backbone of many companies like Sukhoi, Mikoyan, Ilyushin, Tupolev, Yakovlev, Beriev, Myasishchev, Mil Moscow Helicopter Plant, OAO S.P. Korolev Rocket & Space Corporation Energia, [Lavochkin](lav.md), Makeyev Rocket Design Bureau, Khrunichev State Research & Production Space Center, NPO Energomash, Almaz-Antey & others.

Московский авиационный институт представляет собой аналог технопарка и включает в себя научные лаборатории, конструкторские бюро, ресурсные центры, научно‑образовательные центры и лаборатории, экспериментально‑опытный завод, аэродром, центр управления полётами и т.д. Эти подразделения оснащены уникальным оборудованием, что позволяет повышать эффективность проведения научно‑исследовательских, опытно‑конструкторских и технологических услуг.

В МАИ создана соверменная производственная база на основе станков с числовым программным управлением, позволяющая изготавливать непосредственно в университете элементы конструкций космических аппаратов и производить их сборку. Научно‑технический парк также принимает заявки на оказание услуг по следующим направлениям:

- Аддитивные технологии
- Инженерный консалтинг
- Исследование материалов
- Металлообработка
- Нанесение покрытий
- Радиотехнические технологии



## The End

end of file
