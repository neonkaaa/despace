# Spectroscopy
> 2020.01.24 [🚀](../../index/index.md) [despace](index.md) → [Project](project.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

**Spectroscopy** is the field of study that measures & interprets the electromagnetic spectra that result from the interaction between electromagnetic radiation & matter as a function of the wavelength or frequency of the radiation. In simpler terms, spectroscopy is the precise study of color as generalized from visible light to all bands of the electromagnetic spectrum.

The central theory of spectroscopy is that light is made of different wavelengths & that each wavelength corresponds to a different frequency. The importance of spectroscopy is centered around the fact that every different element in the periodic table has a unique light spectrum described by the frequencies of light it emits or absorbs consistently appearing in the same part of the electromagnetic spectrum when that light is diffracted.

Available approaches:

1. XRF (X‑ray fluorescence)



## Methods

1. **Type of radiative energy**
   1. **Electromagnetic radiation** — microwave, terahertz, IR, near-IR, UV-visible, X-ray, gamma
   1. **Particles** — electron, neutron
   1. **Acoustic**
   1. **Dynamic mechanical** analysis, similar to acoustic waves, to solid materials
1. **Nature of the interaction:**
   1. **Absorption spectroscopy** — energy from the radiative source is absorbed by the material.
   1. **Emission spectroscopy** — radiative energy is released by the material. A material's blackbody spectrum is a spontaneous emission spectrum determined by its temperature. This can be measured in the IR by instruments such as the atmospheric emitted radiance interferometer. Emission can also be induced by other sources of energy such as flames, sparks, electric arcs or electromagnetic radiation in the case of fluorescence.
   1. **Elastic scattering** & **reflection spectroscopy** determine how incident radiation is reflected or scattered by a material. Crystallography employs the scattering of high energy radiation, such as x-rays & electrons, to examine the arrangement of atoms in proteins & solid crystals.
   1. **Impedance spectroscopy** is the ability of a medium to impede or slow the transmittance of energy. For optical applications, this is characterized by the index of refraction.
   1. **Inelastic scattering** — an exchange of energy between the radiation & the matter that shifts the wavelength of the scattered radiation. Incl. Raman & Compton scattering.
   1. **Coherent** or **resonance spectroscopy** — radiative energy couples 2 quantum states of the material in a coherent interaction that is sustained by the radiating field. The coherence can be disrupted by other interactions (particle collisions, energy transfer), & so often require high intensity radiation to be sustained. Nuclear magnetic resonance (NMR) spectroscopy is a widely used resonance method, & ultrafast laser spectroscopy is also possible in the IR & visible spectral regions.
   1. **Nuclear spectroscopy** are methods that use the properties of specific nuclei to probe the local structure in matter, mainly condensed matter, molecules in liquids or frozen liquids & bio-molecules.
1. **Type of material:**
   1. **Atoms.** Atomic spectroscopy was the first application of spectroscopy developed. Atomic absorption spectroscopy & atomic emission spectroscopy involve visible & UV light. These absorptions & emissions, often referred to as atomic spectral lines, are due to electronic transitions of outer shell electrons as they rise & fall from one electron orbit to another. Atoms also have distinct x-ray spectra that are attributable to the excitation of inner shell electrons to excited states. Atoms of different elements have distinct spectra & therefore atomic spectroscopy allows for the identification & quantitation of a sample's elemental composition. After inventing the spectroscope, Robert Bunsen & Gustav Kirchhoff discovered new elements by observing their emission spectra. Atomic absorption lines are observed in the solar spectrum & referred to as Fraunhofer lines after their discoverer. A comprehensive explanation of the hydrogen spectrum was an early success of quantum mechanics & explained the Lamb shift observed in the hydrogen spectrum, which further led to the development of quantum electrodynamics. Modern implementations of atomic spectroscopy for studying visible & UV transitions include flame emission spectroscopy, inductively coupled plasma atomic emission spectroscopy, glow discharge spectroscopy, microwave induced plasma spectroscopy, & spark or arc emission spectroscopy. Techniques for studying x-ray spectra include X-ray spectroscopy & X-ray fluorescence.
   1. **Molecules.** The combination of atoms into molecules leads to the creation of unique types of energetic states & therefore unique spectra of the transitions between these states. Molecular spectra can be obtained due to electron spin states (electron paramagnetic resonance), molecular rotations, molecular vibration, & electronic states. Rotations are collective motions of the atomic nuclei & typically lead to spectra in the microwave & millimeter-wave spectral regions. Rotational spectroscopy & microwave spectroscopy are synonymous. Vibrations are relative motions of the atomic nuclei & are studied by both IR & Raman spectroscopy. Electronic excitations are studied using visible & UV spectroscopy as well as fluorescence spectroscopy. Studies in molecular spectroscopy led to the development of the first maser & contributed to the subsequent development of the laser.
   1. **Crystals & extended materials.** The combination of atoms or molecules into crystals or other extended forms leads to the creation of additional energetic states. These states are numerous & therefore have a high density of states. This high density often makes the spectra weaker & less distinct, i.e., broader. For instance, blackbody radiation is due to the thermal motions of atoms & molecules within a material. Acoustic & mechanical responses are due to collective motions as well. Pure crystals, though, can have distinct spectral transitions, & the crystal arrangement also has an effect on the observed molecular spectra. The regular lattice structure of crystals also scatters x-rays, electrons or neutrons allowing for crystallographic studies.
   1. **Nuclei.** Nuclei also have distinct energy states that are widely separated & lead to gamma ray spectra. Distinct nuclear spin states can have their energy separated by a magnetic field, & this allows for nuclear magnetic resonance spectroscopy.
1. **Other types:**
   1. **Acoustic resonance** spectroscopy — sound waves (mostly audible & ultrasonic)
   1. **Auger electron** spectroscopy is a method used to study surfaces of materials on a micro-scale. It is often used in connection with electron microscopy.
   1. **Cavity ring-down** spectroscopy
   1. **Circular dichroism** spectroscopy
   1. **Coherent anti-Stokes Raman** spectroscopy is a recent technique that has high sensitivity & powerful applications for in vivo spectroscopy & imaging.
   1. **Cold vapour atomic fluorescence** spectroscopy
   1. **Correlation** spectroscopy encompasses several types of two-dimensional NMR spectroscopy.
   1. **Deep-level transient** spectroscopy measures concentration & analyzes parameters of electrically active defects in semiconducting materials.
   1. **Dielectric** spectroscopy
   1. **Dual-polarization** interferometry measures the real & imaginary components of the complex refractive index.
   1. **Electron energy loss** spectroscopy in transmission electron microscopy.
   1. **Electron phenomenological** spectroscopy measures the physicochemical properties & characteristics of the electronic structure of multicomponent & complex molecular systems.
   1. **Electron paramagnetic resonance** spectroscopy
   1. **Force** spectroscopy
   1. **Fourier-transform** spectroscopy is an efficient method for processing spectra data obtained using interferometers. Fourier-transform IR spectroscopy is a common implementation of IR spectroscopy. NMR also employs Fourier transforms.
   1. **Gamma** spectroscopy
   1. **Hadron** spectroscopy studies the energy/mass spectrum of hadrons according to spin, parity, & other particle properties. Baryon spectroscopy & meson spectroscopy are types of hadron spectroscopy.
   1. **Hyperspectral imaging** is a method to create a complete picture of the environment or various objects, each pixel containing a full visible, visible near IR, near IR, or IR spectrum.
   1. **Inelastic electron tunneling** spectroscopy uses the changes in current due to inelastic electron-vibration interaction at specific energies that can also measure optically forbidden transitions.
   1. **Inelastic neutron** scattering is similar to Raman spectroscopy, but uses neutrons instead of photons.
   1. **Laser-induced breakdown** spectroscopy, also called laser-induced plasma spectrometry
   1. **Laser** spectroscopy uses tunable lasers & other types of coherent emission sources, such as optical parametric oscillators, for selective excitation of atomic or molecular species.
   1. **Mass** spectroscopy is a historical term used to refer to mass spectrometry. The current recommendation is to use the latter term. The term "mass spectroscopy" originated in the use of phosphor screens to detect ions.
   1. **Mössbauer** spectroscopy probes the properties of specific isotopic nuclei in different atomic environments by analyzing the resonant absorption of gamma rays. See also Mössbauer effect.
   1. **Multivariate optical computing** is an all optical compressed sensing technique, generally used in harsh environments, that directly calculates chemical information from a spectrum as analogue output
   1. **Neutron spin echo** spectroscopy measures internal dynamics in proteins & other soft matter systems
   1. **Perturbed angular correlation (PAC)** uses radioactive nuclei as probe to study electric & magnetic fields (hyperfine interactions) in crystals (condensed matter) & bio-molecules
   1. **Photoacoustic** spectroscopy measures the sound waves produced upon the absorption of radiation
   1. **Photoemission** spectroscopy
   1. **Photothermal** spectroscopy measures heat evolved upon absorption of radiation
   1. **Pump-probe** spectroscopy can use ultrafast laser pulses to measure reaction intermediates in the femtosecond timescale
   1. **Raman optical activity** spectroscopy exploits Raman scattering & optical activity effects to reveal detailed information on chiral centers in molecules
   1. **Raman** spectroscopy
   1. **Saturated** spectroscopy
   1. **Scanning tunneling** spectroscopy
   1. **Spectrophotometry**
   1. **Spin noise** spectroscopy traces spontaneous fluctuations of electronic & nuclear spins
   1. **Time-resolved** spectroscopy measures the decay rates of excited states using various spectroscopic methods
   1. **Time-stretch** spectroscopy
   1. **Thermal IR** spectroscopy measures thermal radiation emitted from materials & surfaces & is used to determine the type of bonds present in a sample as well as their lattice environment. The techniques are widely used by organic chemists, mineralogists, & planetary scientists.
   1. **Transient grating** spectroscopy measures quasiparticle propagation. It can track changes in metallic materials as they are irradiated
   1. **UV photoelectron** spectroscopy
   1. **UV–visible** spectroscopy
   1. **Vibrational circular dichroism** spectroscopy
   1. **Video** spectroscopy
   1. **X-ray photoelectron** spectroscopy



## Space-related methods

The most general method may be an X-ray diffraction method (in particular, APXS).


### APXS
**Alpha particle X-ray spectrometer (APXS)**

TBD

**Heritage:**

1. Mars Exploration Rover (MER) — Spirit, Opportunity (2003 US)



### Echelle

TBD

**Heritage:**

1. Fobos-Grunt (2011 RU, failed)



### Gamma

TBD

**Heritage:**

1. Fobos-Grunt (2011 RU, failed)



### IR Fourier

TBD

**Heritage:**

1. ExoMars 2016 (2016 EU-RU, for atmospheric investigations)
1. Fobos-Grunt (2011 RU, failed)



### Mössbauer

TBD

**Heritage:**


1. ExoMars 2016 (2016 EU-RU, latety excluded)
1. Fobos-Grunt (2011 RU, failed)
1. Mars Exploration Rover (MER) — Spirit, Opportunity (2003 US)
1. Mars-96 (1996 RU)


### Neutron

TBD

**Heritage:**

1. Fobos-Grunt (2011 RU, failed)



### Raman
> <small>**Рамановская спектроскопия** — русскоязычный термин. **Raman spectroscopy** — англоязычный эквивалент.</small>

**Рамановская спектроскопия** *(спектроскопия комбинационного рассеяния)* — вид спектроскопии, в основе которой лежит способность исследуемых систем (молекул) к неупругому (рамановскому, или комбинационному) рассеянию монохроматического света.

Суть метода заключается в том, что через образец исследуемого вещества пропускают луч с определённой длиной волны, который при контакте с образцом рассеивается. Полученные лучи с помощью линзы собираются в один пучок и пропускаются через светофильтр, отделяющий слабые (0.001 % интенсивности) рамановские лучи от более интенсивных (99.999 %) рэлеевских. 「Чистые」 рамановские лучи усиливаются и направляются на детектор, который фиксирует частоту их колебания.

1. <https://en.wikipedia.org/wiki/Raman_spectroscopy>

TBD

**Heritage:**

1. ...



### XRF

> <small>**Рентгенофлуоресцентный анализ (РФА)** — русскоязычный термин. **Английский** — англоязычный эквивалент.</small>

**X‑ray fluorescence (XRF)** is the emission of characteristic 「secondary」 (or fluorescent) X‑rays from a material that has been excited by being bombarded with high‑energy X‑rays or gamma rays. The phenomenon is widely used for elemental analysis & chemical analysis, particularly in the investigation of metals, glass, ceramics & building materials, & for research in geochemistry, forensic science, archaeology & art objects such as paintings & murals.

**Рентгенофлуоресцентный анализ (РФА)** — один из современных спектроскопических методов исследования вещества с целью получения его элементного состава, то есть его элементного анализа. С помощью него могут быть найдены различные элементы от бериллия (Be) до урана (U).

Метод РФА основан на сборе и последующем анализе спектра, возникающего при облучении исследуемого материала рентгеновским излучением. При взаимодействии с высокоэнергетичными фотонами атомы вещества переходят в возбуждённое состояние, что проявляется в виде перехода электронов с нижних орбиталей на более высокие энергетические уровни вплоть до ионизации атома. В возбуждённом состоянии атом пребывает крайне малое время, порядка одной микросекунды, после чего возвращается в спокойное положение (основное состояние). При этом электроны с внешних оболочек заполняют образовавшиеся вакантные места, а излишек энергии либо испускается в виде фотона, либо энергия передается другому электрону из внешних оболочек (оже‑электрон). При этом каждый атом испускает фотон с энергией строго определённого значения, например железо при облучении рентгеновскими лучами испускает фотоны Кα = 6.4 кэВ. Далее соответственно по энергии и количеству квантов судят о строении вещества.

В качестве источника излучения могут использоваться как рентгеновские трубки, так и изотопы каких‑либо элементов. Поскольку каждая страна имеет свои требования к ввозу и вывозу излучающих изотопов, в производстве рентгенофлуоресцентной техники в последнее время стараются использовать, как правило, рентгеновскую трубку. Трубки могут быть как с родиевым, так и с медным, молибденовым, серебряным или другим анодом. Анод трубки, в некоторых случаях, выбирается в зависимости от типа задачи (элементов, требующих анализа), для решения которой будет использоваться данный прибор. Для разных групп элементов используются различные значения силы тока и напряжения на трубке. Для исследования лёгких элементов вполне достаточно установить напряжение 10 ㎸, для средних 20 ‑ 30 ㎸, для тяжёлых — 40 ‑ 50 ㎸. Кроме того, при исследовании лёгких элементов большое влияние на спектр оказывает атмосфера, поэтому камеру с образцом либо вакуумируют либо заполняют гелием. После возбуждения спектр регистрируется на специальном детекторе. Чем лучше спектральное разрешение детектора, тем точнее он сможет отделять друг от друга фотоны от разных элементов, что в свою очередь скажется и на точности самого прибора. В настоящее время наилучшей возможной разрешающей способностью детектора является 123 эВ.

После попадания на детектор фотон преобразовывается в импульс напряжения, который в свою очередь подсчитывается счётной электроникой и наконец передается на компьютер. Ниже приведён пример спектра, полученный при анализе корундовой ступки (содержание Al₂O₃ более 98 %, концентрации Ca, Ti порядка 0.05 %). По пикам полученного спектра можно качественно определить, какие элементы присутствуют в образце. Для получения точного количественного содержания необходимо обработать полученный спектр с помощью специальной программы калибровки (количественной градуировки прибора). Калибровочная программа должна быть предварительно создана с использованием стандартных образцов, чей элементный состав точно известен. Упрощённо, при количественном анализе спектр неизвестного вещества сравнивается со спектрами полученными при облучении стандартных образцов, таким образом получается информация о количественном составе вещества.

Рентгенофлуоресцентный метод широко используется в промышленности, научных лабораториях. Благодаря простоте, возможности экспресс‑анализа, точности, отсутствию сложной пробоподготовки, сферы его применения продолжают расширяться.

**Heritage:**

1. Multiple times for lunar & martian missions
1. [Vega-1/2](vega_1_2.md) (1984)
1. [Venera-13/14](venera_13_14.md) (1981)



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Project](project.md)】**<br> [Interferometer](interferometer.md) ~~ [NASA open](nasa_open.md) ~~ [NASA STI](nasa_sti.md) ~~ [NIH](nih.md) ~~ [Past, future & everything](pfaeverything.md) ~~ [PSDS](us_psds.md) [MGSC](mgsc.md) ~~ [Raman spectroscopy](spectroscopy.md) ~~ [SC price](sc_price.md) ~~ [SC typical forms](sc.md) ~~ [Spectrometry](spectroscopy.md) ~~ [Tech derivative laws](td_laws.md) ~~ [View](view.md)|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Spectroscopy>
1. <https://en.wikipedia.org/wiki/X‑ray_fluorescence>


## The End

end of file
