# Cosmographia
> 2019.09.13 [🚀](../../index/index.md) [despace](index.md) → **[Soft](soft.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Cosmographia** — англоязычный термин, не имеющий аналога в русском языке. **Космография** — дословный перевод с английского на русский.</small>

**Cosmographia** — ПО для визуализации движения планет.

This is an interactive tool used to produce 3D visualizations of planet ephemerides, sizes & shapes; spacecraft trajectories & orientations; & instrument fields‑of‑view & footprints. Cosmographia has many user controls, allowing one to manage what is displayed, what vantage point is used, & how fast the animation progresses. One can add dynamic event annotations to the screen, such as indicators for when an instrument is acquiring data.

Часть пакета [SPICE](spice.md). [Open-source](soft.md). [ITAR](itar.md)‑free.



## Описание
Cosmographia originated as primarily a general interest solar system simulator, & it still works wonderfully in that role. However, with consent & assistance from the tool’s author, NAIF is extending the Cosmographia tool to make use of SPICE kernels to accurately model the observation geometry of planetary missions for which substantial or complete sets of SPICE data are available. With these extensions SPICE‑enhanced Cosmographia should be of interest to scientists & engineers.

Cosmographia is a stand‑alone application for which executable binaries have been prepared on relatively recent versions of OSX, Windows & Linux. They likely work on more modern versions of the named operating systems & perhaps on some older versions as well.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Software](soft.md)】**<br> [ASP](asp.md) ~~ [Blender](blender.md) ~~ [C](plang.md) ~~ [Cosmographia](cosmographia.md) ~~ [DOORS](doors.md) ~~ [DWG](cad_f.md) ~~ [GIMP](gimp.md) ~~ [Git](git.md) ~~ [IGES](cad_f.md) ~~ [ISIS](isis.md) ~~ [JT](cad_f.md) ~~ [NGT](neogeography_toolkit.md) ~~ [NX](nx.md) ~~ [Octave](gnu_octave.md) ~~ [OS](os.md) ~~ [PDF](pdf.md) ~~ [Python](plang.md) ~~ [R](plang.md) ~~ [SPICE](spice.md) ~~ [STEP](cad_f.md) ~~ [STL](stk.md) ~~ [SVG](cad_f.md) ~~ [Syncthing](syncthing.md) ~~ [SysML](sysml.md) ~~ [Teamcenter](teamcenter.md) ~~ [Valispace](valispace.md) ~~ [Система управления версиями](vcs.md) ~~ [ХРИП](adra.md)|

1. Docs: …
1. <https://naif.jpl.nasa.gov/naif/cosmographia.html>



## The End

end of file
