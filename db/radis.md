# РАДИС
> 2019.08.05 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/r/radis_logo1t.webp)](f/c/r/radis_logo1.webp)|<info@radis.ru>, +7(499)735-35-13, +7(499)762-45-90, +7(499)762-46-10, Fax …;<br> *Россия, 124460, Зеленоград, Зеленоградский АО, 4‑й Западный пр‑д, 8, стр. 1*<br> <http://www.radis.ru>|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**РАДИС Лтд** специализируется на разработке и производстве оборудования для систем [спутниковой и радиорелейной связи](comms.md): полупроводниковых (твердотельных) усилителей мощности (SSPA) [СВЧ‑диапазона (C, Ku, …)](comms.md), Up‑Down и BUC конверторов, спутниковые модемы, синтезатороы, МШУ и другие СВЧ‑устройства.

**Проезд:**

1. От ст.метро 「Речной вокзал」 автобусом №400 до конечной остановки. Далее по карте.
1. От Ленинградского вокзала электричкой до станции 「Крюково」, далее авт. №23, 11 до конечной остановки. Далее по карте.
1. Автомобилем по Ленинградскому шоссе до 41 ㎞. (2‑й въезд в Зеленоград), далее по карте.



## The End

end of file
