# Our Stars
> 2022.06.17 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c//_logo1t.webp)](f/c//_logo1.webp)|<info@ourstars.jp>, <mark>nophone</mark>, Fax …;<br> *…*<br> <https://ourstars.jp>|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO, President — Takafumi Horie<br> ・CTO — Atsushi Noda|


Founded 2021.01.

<p style="page-break-after:always"> </p>

## The End

end of file
