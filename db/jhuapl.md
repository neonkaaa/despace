# JHUAPL
> 2019.08.08 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/j/jhuapl_logo1t.webp)](f/c/j/jhuapl_logo1.webp)|<mark>noemail</mark>, +1(240)228-5000, Fax …;<br> *11100 Johns Hopkins Road, Laurel, Maryland 20723-6099, USA*<br> <https://www.jhuapl.edu> ~~ [IG ⎆](https://www.instagram.com/johnshopkinsapl) ~~ [LI ⎆](https://www.linkedin.com/company/johns-hopkins-university-applied-physics-laboratory) ~~ [X ⎆](https://twitter.com/JHUAPL) ~~ [FB ⎆](https://www.facebook.com/JHUAPL) ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/Applied_Physics_Laboratory)|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Лаборато́рия прикладно́й фи́зики** *(сокр. ЛПФ, англ. Johns Hopkins Applied Physics Laboratory, APL)* — лаборатория в округе Хауард близ города Лорел в штате Мэриленд, на территории университета Джонса Хопкинса размером 360 акров. Лаборатория специализируется на исследованиях в интересах Министерства обороны США, НАСА и других структур федерального правительства США, а также иностранные правительственные заказы. Она была основана де‑факто в 1940 году (де‑юре в 1942 году) для разработки методов повышения эффективности ПВО союзников во время Второй мировой войны.

We solve complex research, engineering, & analytical problems that present critical challenges to our nation. APL — the nation’s largest university affiliated research center — provides U.S. government agencies with deep expertise in specialized fields to support national priorities & technology development programs. We also serve as independent trusted technical agents to the government, providing continuity for highly complex, multigenerational technology development systems.

Лаборатория была основана вскоре после начала Второй мировой войны, сугубо с одной целью — разработки дистанционного взрывателя для осколочно‑фугасных снарядов зенитной артиллерии. За два года существования, с 1940 по конец 1942 года лаборанты преуспели в создании ДВ и тематика создания взрывателей и детонационных механизмов для различных категорий боеприпасов стала основным направлением научно‑исследовательской работы лаборатории на долгое время (к артиллерийским снарядам вскоре добавились авиабомбы, торпедное и ракетное вооружение). К концу 1950‑х годов штат лаборатории составлял 1 300 сотрудников, из которых 525 инженерно‑технические и научные работники.

Ныне лаборатория является полу‑самостоятельным субъектом хозяйственной деятельности (полная самостоятельность от университета в административных и исследовательских вопросах, частичная самостоятельность в финансовых вопросах) и одним из крупнейших подрядчиков ВПК США по части НИОКР военной тематики, а также одним из крупнейших работодателей в окру́ге.

На данный момент в лаборатории трудятся более 3 800 человек, и выполняется более 100 научных программ. Годовой бюджет APL более $ 600 млн.

Оборудование:

- камера высокого давления [AVEC](avec.md).



## The End

end of file
