# Концерн Вега
> 2019.04.01 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/k/koncern_vega_logo1t.webp)](f/c/k/koncern_vega_logo1.webp)|<mail@vega.su>, +7(499)753-40-04, Fax +7(495)933-15-63;<br> *Россия, 121170, Москва, Кутузовский проспект, 34*<br> <http://www.vega.su>|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Концерн радиостроения 「Вега」** — разработка и создание приборов радиолокации наземного, авиационного и космического назначения.



## The End

end of file
