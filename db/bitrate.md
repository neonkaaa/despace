# Битрейт
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → **[Радиосвязь](comms.md)**, [ТМС](tms.md), [ЦВМ](obc.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

**Битре́йт** *(от англ. bitrate)* — количество бит, используемых для передачи/обработки данных в единицу времени. Битрейт принято использовать при измерении эффективной скорости передачи потока данных по каналу, то есть минимального размера канала, который сможет пропустить этот поток без задержек.  
In telecommunications and computing, **bit rate** (**bitrate** or as a variable R) is the number of bits that are conveyed or processed per unit of time.

*Термины:*

1. **Битрейт** — русскоязычный термин. **Bit rate** — англоязычный эквивалент.</small>
1. **Информативность** — <small>несуществующий в НД термин, но периодически проскакивающий. Вместо него рекомендуется использовать 「Битрейт」.</small>

Битрейт выражается битами в секунду (бит/c, bps), а также производными величинами с приставками кило‑ (кбит/с, kbps), мега‑ (Мбит/с, Mbps) и т. д.

1. **Patch antennas**
   - **L‑band**: Up to 100 kbps (12 kBps)
   - **S‑band**: Up to 1 Mbps (128 kBps)
   - **X‑band**: 100 kbps to 2 Mbps (12 to 256 kBps)
2. **Low‑gain antennas**
   - **L‑band**: 50 kbps to 500 kbps (6 to 64 kBps)
   - **S‑band**: 100 kbps to 1 Mbps (12 to 128 kBps)
   - **X‑band**: 500 kbps to 5 Mbps (62 to 640 kBps)
3. **Medium‑gain antennas**
   - **S‑band**: 1 Mbps to 10 Mbps (128 kBps to 1.25 MBps)
   - **X‑band**: 10 Mbps to 100 Mbps (1.25 to 12.5 MBps)
   - **Ku‑band**: 50 Mbps to 200 Mbps (6.2 to 23 MBps)
4. **High‑gain antennas**
   - **X‑band**: 50 Mbps to 100 Mbps (6.2 to 12.5 MBps)
   - **Ku‑band**: 100 Mbps to 1 Gbps (12.5 to 125 MBps)
   - **Ka‑band**: 500 Mbps to 10 Gbps (62 to 1250 MBps)



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Communications](comms.md)】**<br> [CCSDS](ccsds.md) ~~ [Антенна](antenna.md) ~~ [АФУ](afdev.md) ~~ [Битрейт](bitrate.md) ~~ [ВОЛП](ofts.md) ~~ [ДНА](дна.md) ~~ [Диапазоны частот](comms.md) ~~ [Зрение](view.md) ~~ [Интерферометр](interferometer.md) ~~ [Информация](info.md) ~~ [КНД](directivity.md) ~~ [Код Рида‑Соломона](rsco.md) ~~ [КПДА](antenna.md) ~~ [КСВ](swr.md) ~~ [КУ](ку.md) ~~ [ЛКС, АОЛС, FSO](fso.md) ~~ [Несущий сигнал](carrwave.md) ~~ [ПНА, ПОНА, ПСНА](devd.md) ~~ [Помехи](emi.md) (EMI, RFI) ~~ [Последняя миля](last_mile.md) ~~ [Регламент радиосвязи](comms.md) ~~ [СИТ](etedp.md) ~~ [Фидер](feeder.md) <br>~ ~ ~ ~ ~<br> **РФ:** [БА КИС](ба_кис.md) (21) ~~ [БРК](brk_lav.md) (12) ~~ [РУ ПНИ](ру_пни.md) () ~~ [HSXBDT](comms_lst.md) (1.8) ~~ [CSXBT](comms_lst.md) (0.38) ~~ [ПРИЗЫВ-3](comms_lst.md) (0.17) *([ПРИЗЫВ-1](comms_lst.md) (0.075))**|

1. Docs: …
   1. <https://en.wikipedia.org/wiki/Bit_rate>


## The End

end of file
