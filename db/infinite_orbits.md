# Infinite Orbits
> 2022.03.30 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/i/infinite_orbits_logo1t.webp)](f/c/i/infinite_orbits_logo1.webp)|<info@infiniteorbits.io>, <mark>nophone</mark>, Fax …;<br> *Infinite Orbits SAS, 13 Rue Sainte-Ursule, 31000 Toulouse, Occitanie, France*<br> <https://www.infiniteorbits.io> ~~ [LI ⎆](https://www.linkedin.com/company/infinite-orbits)|
|:--|:--|
|**Business**|In‑orbit servicing, servicing platforms|
|**Mission**|…|
|**Vision**|To provide reliable, turnkey in‑orbit services to satellite operators all over the world|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO, Founder — Adel Haddoud<br> ・CTO, Founder — Akshay Gulati<br> ・Chief Architect, Founder — Manos Koumantakis<br> ・CFO — Yasmin Hassan<br> ・CCO — Yasmin Hassan|

**Infinite Orbits** is a Newspace company aimed to provide GEO life extension & servicing. We develop & operate life extension platforms with a new space approach to extend the operational lifetime of in‑orbit GEO telecommunication satellites & deliver optimized satellite fleet management solutions Founded in 2017.

- **GEO Life extension.** Life Extension Services for GEO satellites. Support in fleet management decisions, in the extension of  lifetime of assets & replacement of heavy capital investment by an operational service.
- **Space Surveillance.** Infinite Orbits provides In‑Site Intelligence for Space Surveillance, covering SST, SSA & SDA. Thus, responding to a significant need of operating satellites.

**Singapore office:** Infinite Orbits PTE Ltd. 1 North Bridge Rd, 30 00 High Street Centre, Singapore 179094

The company is supporting its customers in their fleet management from frequency application & inspection to life‑extension. Infinite Orbits has roots in Silicon Valley & Columbia University. The company was founded in 2017, following a NASA challenge at Columbia University. It was initially set up in Singapore & partnered with experts from the Stanford Rendezvous lab & MIT. Since 2021, the company has been based in Toulouse, Occitanie, the European capital of Aeronautics & Space. With offices in France, Singapore & Silicon Valley, Infinite Orbits brings together experts from the GNC, computer vision, satellite development & telecom service fields with previous or current experience at SSL, IIT, Inmarsat & Kacific. With two generations of in‑orbit services, Autonomous Space Navigation & Docking, Infinite Orbits is ready to provide high‑end NewSpace solutions to cover all satellite servicing needs.

<p style="page-break-after:always"> </p>

## The End

end of file
