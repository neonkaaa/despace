# Ядерный реактор
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [СЭС](sps.md), **[ЯР](nr.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Ядерный реактор (ЯР)** — русскоязычный термин. **Nuclear reactor (NR)** — англоязычный эквивалент.</small>

**Ядерный реактор (ЯР)** — устройство, предназначенное для организации управляемой самоподдерживающейся цепной реакции деления, которая всегда сопровождается выделением энергии.

**Область применения**

|**[Mercury](mercury.md)**|**[Venus](venus.md)**|**[Earth](earth.md)**|**[Moon](moon.md)**|**[Mars]( mars.md)**|**[And further](index.md)**|
|:--|:--|:--|:--|:--|:--|
|+|<small>на орбите</small>|+|+|+|+|

Любой ядерный реактор состоит из следующих частей:

1. Активная зона с ядерным топливом и замедлителем;
1. Отражатель нейтронов, окружающий активную зону;
1. Теплоноситель;
1. Система регулирования цепной реакции, в том числе аварийная защита;
1. Радиационная защита;
1. Система дистанционного управления.



## Применение
**В космосе:**

1. Космические реакторы, предназначенные для получения электрической энергии на космических аппаратах требующих значительных энергетических мощностей, которые не в силах обеспечить [солнечные батареи](sp.md) и [РИТЭГи](rtg.md). [Ромашка, Бук, Тополь, Енисей].
1. [ЯРДУ](ps.md).



## Топливо
См. [Топливо](ps.md).



## Designers, manufacturers
1. **РФ:**
   1. 「Курчатовский институт」
   1. НПО 「Красная звезда」
1. **США:**
   1. [Boeing](boeing.md)
   1. [GRC](grc.md)



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Spacecraft power system (SPS)](sps.md)】**<br> [Charge eff.](charge_eff.md) ~~ [EAS](eas.md) ~~ [EB](eb.md) ~~ [EMI, RFI](emi.md) ~~ [NR](nr.md) ~~ [Rotor](iu.md) ~~ [RTG](rtg.md) ~~ [Solar cell](sp.md) ~~ [SP](sp.md) ~~ [SPB/USPB](suspb.md) ~~ [Voltage](sps.md) ~~ [WT](wt.md)<br>~ ~ ~ ~ ~<br> **RF/CIF:** [BAK‑01](eas_lst.md) ~~ [KAS‑LOA](eas_lst.md)|
|**`Ядерный реактор (ЯР):`**<br> …|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Nuclear_reactor>
1. <https://en.wikipedia.org/wiki/Nuclear_power_in_space>
1. 2017.07.13 Хабр: Ядерные реакторы в космосе: ТЭМ (<https://habr.com/ru/post/381701>) — [archived ❐](f/archive/20150713_1.pdf) 2019.02.05



## The End

end of file
