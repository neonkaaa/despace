# CUNI
> 2019.08.05 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/c/cuni_logo1t.webp)](f/c/c/cuni_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *…*<br> <http://www.cuni.cz> ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/Charles_University)|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Ка́рлов университе́т** в Праге (чеш. **Univerzita Karlova v Praze**, междунар. **Charles University in Prague (CUNI)** — главный университет Чехии, старейший университет Центральной Европы и один из старейших университетов мира. Был основан императором Карлом IV в 1348 году.



## The End

end of file
