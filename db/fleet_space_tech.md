# Fleet Space Technologies
> 2021.10.22 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/f/fleet_space_tech_logo1t.webp)](f/c/f/fleet_space_tech_logo1.webp)|<mark>noemail</mark>, +61(8)7200-26-33, Fax …;<br> *ABN 27 607 948 729, 8a Myer Court, Beverley, SA 5009 Australia*<br> <https://fleetspace.com> ~~ [FB ⎆](https://www.facebook.com/fleetspace) ~~ [IG ⎆](https://www.instagram.com/fleet.space) ~~ [LI ⎆](https://au.linkedin.com/company/fleet-space-technologies) ~~ [X ⎆](https://twitter.com/fleetspace)|
|:--|:--|
|**Business**|IoT with nanosat constellation|
|**Mission**|To Connect Everything using cutting‑edge communications & space technologies to enable the next giant leap in human civilisation|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO, Founder — Flavia Tata Nardini<br> ・COO — Matthew Pearson|

**Fleet Space Technologies Pty Ltd** is focused on building advanced nanosatellite communications technologies for ultra‑efficient satellite networks.

Fleet Space made history by launching Australia’s 1st four commercial nanosatellites in November 2018. Over the course of three weeks, Proxima 1 & 2 & Centauri 1 & 2 were launched into Low Earth Orbit. Fleet is continuing its work on the Centauri Program, having launched its 5th & 6th satellites in 2021, & the next batch of satellites in 2022.

**Remote connectivity experts**  
We are experts in remote connectivity systems. Australia’s particular connectivity challenges have been a major driver in creating new connectivity technologies.

**We take the rocket science out of IoT**  
We specialise in creating low‑cost satellite based systems for Industrial Internet of Things applications. We’re using space technologies to simplify IoT because improving efficiency in industry shouldn’t be rocket science!

<p style="page-break-after:always"> </p>

## The End

end of file
