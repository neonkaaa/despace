# Simorgh
> 2019.07.02 [🚀](../../index/index.md) [despace](index.md) → [LV](lv.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Simorgh** — англоязычный термин, не имеющий аналога в русском языке. **Симург** — дословный перевод с английского на русский.</small>

**Simorgh** (перс. سیمرغ‎ — по имени фантастической птицы Симург) — иранская одноразовая твердотопливная ракета‑носитель лёгкого класса.

|**Version**|**Description**|**Activity**|
|:--|:--|:--|
|Simorgh|Базовый вариант.|**Активен** (2016 ‑ …)|

[![](f/lv/simorgh/simorgh_design_725t.webp)](f/lv/simorgh/simorgh_design_725.webp)


---



## Simorgh
**Simorgh** (перс. سیمرغ‎ — по имени фантастической птицы Симург) — иранская одноразовая твердотопливная ракета‑носитель лёгкого класса. В качестве первой ступени используется переделенная первая ступень баллистической ракеты 「Шахаб‑5」 со связкой из четырёх жидкостных ракетных двигателей по типу 「Нодон」, а в роли второй ступени выступает жидкостная баллистическая ракета 「Гадр‑1」. Похожа на северо‑корейскую РН [Unha](unha.md).

|**Characteristic**|**[Value](si.md)**|
|:--|:--|
|Активность|**Активен** (2016.04.19 ‑ …)|
|[Аналоги](analogue.md)|[Electron](electron.md) (США) ~~ [Unha](unha.md) (Корея сев.)|
|Длина/диаметр|27 м / 2.5 м|
|[Космодромы](spaceport.md)|[Semnan](spaceport.md)|
|Масса старт./сух.|87 000 ㎏ / … ㎏|
|Разраб./изготов.|[ISA](isa.md) (Иран) / [ISA](isa.md) (Иран)|
|Ступени|3|
|[Fuel](ps.md)|[АТ + Красная дымящая азотная кислота](nto_plus.md)|
| |[![](f/lv/simorgh/simorgh_01t.webp)](f/lv/simorgh/simorgh_01.webp) [![](f/lv/simorgh/simorgh_02t.webp)](f/lv/simorgh/simorgh_02.webp)|

**Выводимые массы.**

|**Космодром**|**РН**|<small>*Масса,<br> [НОО](nnb.md), т*</small>|<small>*Масса,<br> [ГСО](nnb.md), т*</small>|<small>*Масса к<br> [Луне](moon.md), т*</small>|<small>*Масса к<br> [Венере](venus.md), т*</small>|<small>*Масса к<br> [Марсу](mars.md), т*</small>|**Примечания**|
|:--|:--|:--|:--|:--|:--|:--|:--|
|[Semnan](spaceport.md)|Simorgh|0.15|—|—|—|—|Пуск — $ … млн (… г);<br> ПН 0.17 % от ст.массы|

<small>Примечания:<br> **1)** Указана масса для наихудших условий старта.<br> **2)** В скобках указана масса для наилучших условий старта.</small>



## Архивные

…



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Launch vehicle (LV)](lv.md)】**<br> [ICBM](icbm.md) ~~ [Integrated payload unit](lv.md) ~~ [Non‑rocket spacelaunch](nrs.md) ~~ [Throw weight](throw_weight.md)<br>~ ~ ~ ~ ~<br> **China:** [Long March](long_march.md) ┊ **EU:** [Arian](arian.md), [Vega](vega.md) ┊ **India:** [GSLV](gslv.md), [PSLV](pslv.md) ┊ **Israel:** [Shavit](shavit.md) ┊ **Japan:** [Epsilon](epsilon.md), [H2](h2.md), [H3](h3.md) ┊ **Korea N.:** [Unha](unha.md) ┊ **Korea S.:** *([Naro‑1](kslv.md))* ┊ **RF,CIF:** [Angara](angara.md), [Proton](proton.md), [Soyuz](soyuz.md), [Yenisei](yenisei.md), [Zenit](zenit.md) *([Energia](energia.md), [Korona](korona.md), [N‑1](n_1.md), [R‑1](r_7.md))* ┊ **USA:** [Antares](antares.md), [Atlas](atlas.md), [BFR](bfr.md), [Delta](delta.md), [Electron](electron.md), [Falcon](falcon.md), [Firefly Alpha](firefly_alpha.md), [LauncherOne](launcherone.md), [New Armstrong](new_armstrong.md), [New Glenn](new_glenn.md), [Minotaur](minotaur.md), [Pegasus](pegasus.md), [Shuttle](shuttle.md), [SLS](sls.md), [Vulcan](vulcan.md) *([Saturn](saturn_lv.md), [Sea Dragon](sea_dragon.md))**|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Simorgh_(rocket)>
1. <https://space.skyrocket.de/doc_lau/simorgh.htm>


## The End

end of file
