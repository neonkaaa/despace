# ArkEdge Space
> 2022.04.06 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/a/arkedge_space_logo1t.webp)](f/c/a/arkedge_space_logo1.webp)|<ae-publicity@arkedgespace.com>, <mark>nophone</mark>, Fax …;<br> *Ginjinsha Tokyo Akihabara Building #501 3-5-2 Iwamotocho, Chiyoda-ku, Tokyo, 101-0032, Japan*<br> <https://arkedgespace.com/en> ~~ [LI ⎆](https://www.linkedin.com/company/arkedge-space-inc) ~~ [X ⎆](https://twitter.com/arkedgespace)|
|:--|:--|
|**Business**|Small sats|
|**Mission**|Provide satellite business opportunities for all|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO, Founder — Takayoshi Fukuyo<br> ・CTO, Founder — Shuhei Matsushita|

**ArkEdge Space Inc.** is a Japanese company aimed for creation of small sats. Founded in 2018.

Services: design, manufacturing, & operation service of spacecraft (microsatellites), ground stations & related components, software development, education & consulting services, related to the above.

<p style="page-break-after:always"> </p>

## The End

end of file
