# ЕСТПП
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → **[НД](doc.md#НД)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Единая система технологической подготовки производства (ЕСТПП)** — русскоязычный термин, не имеющий аналога в английском языке. **Unified system of technological preparation of production (USOTPP)** — дословный перевод с русского на английский.</small>

**Единая система технологической подготовки производства (ЕСТПП)** — это система, которая предусматривает широкое применение прогрессивных типовых технологических процессов, стандартной оснастки и оборудования, средств механизации и автоматизации производственных процессов, инженерно‑технических управленческих работ.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[](.md)】**<br> <mark>NOCAT</mark>|

1. Docs: …
1. <…>


## The End

end of file
