# Tokyo University
> 2019.08.09 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/t/tokyo_univ_logo1t.webp)](f/c/t/tokyo_univ_logo1.webp)|<mark>noemail</mark>, +81(3)3812-2111, Fax …;<br> *7-chōme-3-1 Hongō, Bunkyo City, Tōkyō-to 113-8654, Japan*<br> <https://www.u-tokyo.ac.jp> ~~ [FB ⎆](https://www.facebook.com/UTokyo.News.en) ~~ [LI ⎆](https://www.linkedin.com/school/university-of-tokyo) ~~ [X ⎆](https://twitter.com/UTokyo_News_en) ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/University_of_Tokyo)|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**The University of Tokyo** (東京大学, Tōkyō daigaku), abbreviated as Todai (東大, Tōdai) or UTokyo, is a public research university located in Bunkyō, Tokyo, Japan. Established in 1877, it was the 1st of the imperial universities.

The university has ten faculties, 15 graduate schools & enrolls about 30 000 students, 2 100 of whom are international students. Its five campuses are in Hongō, Komaba, Kashiwa, Shirokane & Nakano. It is among the top echelon of the select Japanese universities assigned additional funding under the MEXT’s Top Global University Project to enhance Japan’s global educational competitiveness.

Notable space departments:

- **ISSL (Intelligent Space System Laboratory, Nakasuka Laboratory)** <https://www.space.t.u-tokyo.ac.jp/nlab/index_e.html> — We are studying innovative space systems, incl. very small 1 ㎏ pico‑satellite to large "Furoshki‑satellite" of 1 ㎞ size, conceptuarizing new ideas on mi ssions, systems & how to operate them. We have already developed, launched & successfully operated three pico & nano‑satellites, "XI‑IV", "XI‑V" (1 ㎏ CubeSat) & "PRISM" (8 ㎏ remote sensing satellite), with which we lead the world research scene on nano‑satellites. Current target is how to develop not only "educational" but also "really practical & useful" nano‑satellites, & we are pursuing technology innovations & mission creations for less than 50 ㎏ satellites. "Nano‑JASMINE," a satellite with astrometry mission, is one example of such practical satellites. We’re also conducting basic research on control engineering & artificial intelligence, in order to realize more dependable & intelligent space systems.
- **Department of Aeronautics & Astronautics** <http://www.aerospace.t.u-tokyo.ac.jp/english>



## The End

end of file
