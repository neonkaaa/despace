# LSAS Tec
> 2022.07.18 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/l/lsas_tec_logo1t.webp)](f/c/l/lsas_tec_logo1.webp)|<mark>noemail</mark>, 03-4334-8102, Fax …;<br> *Kasumigaseki Building 5th floor, Kasumigaseki 3-2-5, Chiyoda-ku, 〒100-6005, Tokyo*<br> <https://lsas-tec.co.jp>|
|:--|:--|
|**Business**|R&D analysis/simulation software|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO, President — Eiji Takahashi|

**LSAS Tec CO., Ltd** sells, customizes, & introduces analysis/simulation software for outer space, land, sea, & air based on the package software Systems Tool Kit (STK) of Analytical Graphics Inc. (AGI) in the United States. We provide operational consultation & support. Founded 2009.04.02.

LSAS is a professional group that handles various calculations, analyzes, & simulations related to space & aviation. Over the last 10 years, he has been a part of many large‑scale projects.

Nowadays, not only national projects but also private venture companies are launching artificial satellites, & the market for analysis & simulation related to the design & operation of artificial satellites that require advanced know‑how is expanding rapidly.

LSAS Tec, one of the few private professional groups in Japan, pursues further expertise & develops experienced human resources, & is advancing the path of a solution provider in the space & aviation fields.

<p style="page-break-after:always"> </p>

## The End

end of file
