# Terra Space
> 2021.12.02 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/t/terra_space_logo1t.webp)](f/c/t/terra_space_logo1.webp)|<info@terraspace.jp>, +81(7)7474-8202, Fax …;<br> *36-1 Yoshidahonmachi, Sakyo Ward, Kyoto, 606-8317, JP*<br> <https://www.terraspace.jp>|
|:--|:--|
|**Business**|CubeSats|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO, Representative Director — Sadahiro Kitagawa|

**Terra Space Co., Ltd.** (also **Terra Space Inc.**) is a Japanese company aimed for Development / launch / operation of artificial satellites. Founded 2020.02.04. Services:

- **Development / launch / operation of artificial satellites** — at Terra Space, we are developing a mass‑produced microsatellite (6U) that can be provided at a low price & with a short delivery time. In addition, based on this satellite, we will also undertake the development / launch / operation of the original satellite according to the customer’s mission.
- **Hosted Payload Service** — this is a service for carpooling by mounting equipment entrusted to us by our customers on an artificial satellite launched in Terra Space. Of course, it is also possible to send & receive data after launch & to act as an operation agent. It is easier to use than developing & occupying one satellite in‑house. It can be used for missions that do not require large equipment, & for checking the operation of various devices in outer space.

**Message from the Representative Director Sadahiro Kitagawa**

> The company name "terra space" is a coined word that combines "terra", which means the earth in Latin, & "space", which means the universe in English, & has the desire to be a bridge connecting the earth & the universe. The universe, which was so far away in our childhood, is now within reach with a little more effort. Based on the technology that our predecessors have built, we would like to create a world where everyone can play an active role on the vast stage of the universe.<br> At the same time, "tera space" also means "temple of the universe." The temple used to be a place where various people gathered to study the latest scholarship, protect the country, & foster art. It is exactly the hero who has created Japanese culture. "Terra Space" also includes the determination to be a space palace carpenter who creates such a modern temple in space. And it is also the 1st mission of Terra Space.<br> We would like to do our best to be an entity that is evaluated by people all over the world as "waiting for such a company."

<p style="page-break-after:always"> </p>

## The End

end of file
