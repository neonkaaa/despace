# Obruta Space Solutions
> 2022.04.04 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/o/obruta_ss_logo1t.webp)](f/c/o/obruta_ss_logo1.webp)|<contact@obruta.com>, <mark>nophone</mark>, Fax …;<br> *…*<br> <https://www.obruta.com> ~~ [FB ⎆](https://www.facebook.com/obrutaspace) ~~ [IG ⎆](https://www.instagram.com/obrutaspace) ~~ [LI ⎆](https://www.linkedin.com/company/obruta) ~~ [X ⎆](https://twitter.com/ObrutaSpace)|
|:--|:--|
|**Business**|Mechanisms for docking & in‑orbit servicing|
|**Mission**|To ensure the sustainability of all spacecraft so that we may create a robust off‑world economy which will enhance all lives back on Earth|
|**Vision**|To make servicing a spacecraft as easy & as commonplace as servicing your car|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO, Founder — Kevin Stadnyk<br> ・CTO, Founder — Kirk Hovell|

**Obruta** is powering the interactions of the emerging space economy  through reimagined spacecraft Rendezvous, Proximity Operations, & Docking (RPOD).

Obruta Space Solutions was founded from a simple idea: what if servicing a spacecraft was as easy & commonplace as servicing your car? To achieve this, we are developing the foundational technologies to meet the growing needs of the on‑orbit servicing & logistics markets, & the growing concerns around the safety, removal, & future mitigation of all space debris.

<p style="page-break-after:always"> </p>

## The End

end of file
