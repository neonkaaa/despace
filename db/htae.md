# HTAE
> 2019.05.07 [🚀](../../index/index.md) [despace](index.md) → [PS](ps.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**High thrust apogee engine (HTAE)** — англоязычный термин, не имеющий аналога в русском языке. **Апогейный двигатель с высокой тягой (HTAE)** — дословный перевод с английского на русский.</small>


**HTAE** — high thrust apogee engine.

Маршевый двигатель. Общее название ДУ с тягой >1 000 Н и высоким УИ, например, [LEROS 4](engine_lst.md).



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**`Двигательная установка (ДУ):`**<br> [HTAE](htae.md) ~~ [TALOS](talos.md) ~~ [Баки топливные](fuel_tank.md) ~~ [Варп‑двигатель](ps.md) ~~ [Газовый двигатель](ps.md) ~~ [Гибридный двигатель](гбрд.md) ~~ [Двигатель Бассарда](ps.md) ~~ [ЖРД](ps.md) ~~ [ИПТ](ing.md) ~~ [Ионный двигатель](иод.md) ~~ [Как считать топливо?](si.md) ~~ [КЗУ](cinu.md) ~~ [КХГ](cgs.md) ~~ [Номинал](nominal.md) ~~ [Мятый газ](exhsteam.md) ~~ [РДТТ](ps.md) ~~ [Сильфон](сильфон.md) ~~ [СОЗ](соз.md) ~~ [СОИС](соис.md) ~~ [Солнечный парус](солнечный_парус.md) ~~ [ТНА](turbopump.md) ~~ [Топливные мембраны](топливные_мембраны.md) ~~ [Топливные мешки](топливные_мешки.md) ~~ [Топливо](ps.md) ~~ [Тяговооружённость](ttwr.md) ~~ [ТЯРД](тярд.md) ~~ [УИ](ps.md) ~~ [Фотонный двигатель](фотонный_двигатель.md) ~~ [ЭРД](ps.md) ~~ [Эффект Оберта](oberth_eff.md) ~~ [ЯРД](ps.md)|

1. Docs: …
1. <…>


## The End

end of file
