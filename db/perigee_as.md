# Perigee Aerospace
> 2020.07.24 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/p/perigee_as_logo1t.webp)](f/c/p/perigee_as_logo1.webp)|<info@perigee.kr>, +82(0)42 710 3513, Fax …;<br> *398 Daedeok-daero, 4th floor, Seo-gu, Daejeon, Republic of Korea 35203*<br> <https://perigee.kr> ~~ [FB ⎆](https://www.facebook.com/perigeespace) ~~ [IG ⎆](https://www.instagram.com/perigee.space) ~~ [LI ⎆](https://www.linkedin.com/company/perigee-aerospace) ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/Perigee_Aerospace)|
|:--|:--|
|**Business**|Small / medium orbital & sub‑orbital LV|
|**Mission**|Sustainable access to Earth orbit & beyond|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO, Founder — Yoon Shin<br> ・CTO, Founder — Youngjin Song|

**Perigee Aerospace** is a Korean private developer & manufacturer of orbital & sub‑orbital [LV](lv.md). Formally founded in 2018, but work began in 2012, initially with the launch of sounding rockets.

Vehicles:

1. **Blue Whale 1** — the 2‑stage Blue Whale 1 is planned to become the smallest orbital rocket in the world with a mass of 1 790 ㎏. Launching from a pad owned by Southern Launch at [Whalers Way](spaceport.md) in South Australia it can deliver up to 50 kg to a sun‑synchronous orbit or 63 kg to LEO with an altitude of 500 ㎞. The maiden flight is planned for July 2020. Perigee Aerospace plans to launch up to 40 Blue Whale 1 rockets per year at a price of $ 2 million.
1. **Sounding Rockets.** According to an interview posted on the website of Explore University, an educational program by Dong-A Science, Perigee Aerospace also plans to launch a sounding rocket developed in collaboration with [KAIST](kaist.md). The exact specifications & launch date are yet to be confirmed.



## The End

end of file
