# Device drive
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [Антенна](antenna.md), [GNC](gnc.md), [SGM](sc.md), [Радиосвязь](comms.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

## Antenna drive (AND)
> <small>**Привод направленной антенны (ПНА)** — русскоязычный термин, не имеющий аналога в английском языке. **Antenna drive (AND)** — дословный перевод с русского на английский.</small>

**Привод направленной антенны (ПНА)** предназначен для программного разворота [направленной антенны](antenna.md) на требуемые углы.

|**Страна**|**Актуальные**|**Исторические**|
|:--|:--|:--|
|**Россия**|[ПНА ЛР1](пна_лр1.md) ~~ [ПОНА ЛРОА](пона_лроа.md)| |

【**Table.** Manufacturers】

1. **РФ:**
   1. [ИСС](iss_r.md) — ПОНА
   1. [LAV](lav.md) — ПНА



## Solar panels orientation system (SPOS)
> <small>**Система ориентации солнечных батарей (СОСБ)** — русскоязычный термин. **Solar panels orientation system (SPOS)** — англоязычный эквивалент.</small>

**Система ориентации солнечных батарей (СОСБ)** предназначена для программного разворота крыльев солнечной батареи активной стороной на [Солнце](sun.md), для передачи электроэнергии от батарей фотоэлектрических в [систему электроснабжения](devd.md) КА, для передачи электрических сигналов.

| |**[Фирма](contact.md)**|**Актуальные**|**Исторические**|
|:--|:--|:--|:--|
|**RU**|[ВНИИЭМ](vniiem.md)|[Система ЛР-10](система_лр_10.md)| |
| |[ИСС](iss_r.md)| |[СОСБ ЛРОА](сосб_лроа.md)|
| |[LAV](lav.md)|[Э10](э10.md)| |

【**Table.** Manufacturers】

1. **Россия**
   1. [Аксион‑холдинг](axion_h.md) — СЧ СОСБ.
   1. [ВНИИЭМ](vniiem.md)
   1. [ИСС](iss_r.md)
   1. [НИИКП](niicom.md) — поворотные устройства, приводы;
   1. [LAV](lav.md)



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Guidance, Navigation & Control (GNC)](gnc.md)】**<br> [CAN](can.md) ~~ [LVDS](lvds.md) ~~ [MIL‑STD‑1553](mil_std_1553.md) (МКО) ~~ [OS](os.md) ~~ [RS‑232, 422, 485](rs_xxx.md) ~~ [SpaceWire](spacewire.md) ~~ [АСН, САН](ans.md) ~~ [БНО](nnb.md)[MIL‑STD‑1553](mil_std_1553.md) (МКО)[БАППТ](eas.md) ~~ [БКС](cable.md) ~~ [БУ](eas.md) ~~ [БШВ](time.md) ~~ [Гироскоп](iu.md) ~~ [Дальномер](doppler.md) (ИСР) ~~ [ДМ](iu.md) ~~ [ЗД](sensor.md) ~~ [Компьютер](obc.md) (ЦВМ, БЦВМ) ~~ [Магнитометр](sensor.md) ~~ [МИХ](mic.md) ~~ [МКО](mil_std_1553.md) ~~ [ПО](soft.md) ~~ [ПНА, ПОНА, ПСНА](devd.md) ~~ [СД](sensor.md) ~~ [Система координат](coord_sys.md) ~~ [СОСБ](devd.md)|
|**【[Structures, gears, materials (SGM)](sc.md)】**<br> [Гермоконтейнер](гермоконтейнер.md) ~~ [Датчик](sensor.md) ~~ [Задел](margin.md) ~~ [Изделие](unit.md) ~~ [Испарение материалов](matc.md) ~~ [Кавитация](cavitation.md) ~~ [КЗУ](cinu.md) (ВБУ КТ) ~~ [КХГ](cgs.md) ~~ [Контейнеры для транспортировки](ship_contain.md) ~~ [Крейцкопф](crosshead.md) ~~ [Номинал](nominal.md) ~~ [ПУС](lag.md) ~~ [ПНА, ПОНА, ПСНА](devd.md) ~~ [Резерв](reserve.md) ~~ [Слайс](слайс.md) ~~ [ТСП](tsp.md) ~~ [Типичные формы КА](sc.md) ~~ [Толкатель](толкатель.md) ~~ [Унификация](commonality.md)|
|**【[Communications](comms.md)】**<br> [CCSDS](ccsds.md) ~~ [Антенна](antenna.md) ~~ [АФУ](afdev.md) ~~ [Битрейт](bitrate.md) ~~ [ВОЛП](ofts.md) ~~ [ДНА](дна.md) ~~ [Диапазоны частот](comms.md) ~~ [Зрение](view.md) ~~ [Интерферометр](interferometer.md) ~~ [Информация](info.md) ~~ [КНД](directivity.md) ~~ [Код Рида‑Соломона](rsco.md) ~~ [КПДА](antenna.md) ~~ [КСВ](swr.md) ~~ [КУ](ку.md) ~~ [ЛКС, АОЛС, FSO](fso.md) ~~ [Несущий сигнал](carrwave.md) ~~ [ПНА, ПОНА, ПСНА](devd.md) ~~ [Помехи](emi.md) (EMI, RFI) ~~ [Последняя миля](last_mile.md) ~~ [Регламент радиосвязи](comms.md) ~~ [СИТ](etedp.md) ~~ [Фидер](feeder.md) <br>~ ~ ~ ~ ~<br> **РФ:** [БА КИС](ба_кис.md) (21) ~~ [БРК](brk_lav.md) (12) ~~ [РУ ПНИ](ру_пни.md) () ~~ [HSXBDT](comms_lst.md) (1.8) ~~ [CSXBT](comms_lst.md) (0.38) ~~ [ПРИЗЫВ-3](comms_lst.md) (0.17) *([ПРИЗЫВ-1](comms_lst.md) (0.075))**|

1. Docs: …
1. <…>


## The End

end of file
