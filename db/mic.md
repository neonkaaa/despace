# Массово‑инерционные характеристики
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [GNC](gnc.md), [МИХ](.md), **[Модель](draw.md)**, [Control](control.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

<small>*Термины:*<br> ・**Массово‑инерционные характеристики (МИХ)** — русскоязычный термин. **Mass-inertia characteristics (MIC)** — англоязычный эквивалент.<br> ・**Центр масс (ЦМ)** — русскоязычный термин. **Center of mass (CoGM)** — англоязычный эквивалент.<br> ・**Центр тяжести (ЦТ)** — русскоязычный термин. **Center of gravity (CoG)** — англоязычный эквивалент.</small>

**Массово‑инерционные характеристики (МИХ)** — совокупность показателей, характеризующих распределение масс в [изделии](unit.md).

МИХ предназначены для определения нагрузок на узлы изделия, его СЧ, а также внешних объектов на различных этапах деятельности.  
К МИХ относятся:

1. массы и координаты центров масс изделия и его СЧ;
1. моменты инерции изделия и его СЧ при различных положениях;


## Осциллятор
> <small>**Осциллятор** — русскоязычный термин. **Oscillator** — англоязычный эквивалент.</small>

**Осцилля́тор** *(от лат. oscillo — качаюсь)* — система, совершающая колебания, то есть показатели которой периодически повторяются во времени.

1. <https://ru.wikipedia.org/wiki/Осциллятор>

Понятие осциллятора играет важную роль в физике и повсеместно используется, например, в квантовой механике и квантовой теории поля, теории твёрдого тела, электромагнитных излучений, колебательных спектров молекул. В принципе это понятие используется по крайней мере при описании почти любой линейной или близкой к линейности физической системы, и уже поэтому пронизывает практически всю физику.

Используется при создании [Схемы осцилляторов](draw.md).


## Центр масс
**Центр масс, центр ине́рции, барице́нтр (ЦМ)** (от др.‑греч. βαρύς — тяжёлый + κέντρον — центр) — (в механике) геометрическая точка, характеризующая движение тела или системы частиц как целого. Не является тождественным понятию центра тяжести (хотя чаще всего совпадает).



## Центр тяжести
Центр масс тела не следует путать с центром тяжести.

**Центром тяжести (ЦТ)** механической системы называется точка, относительно которой суммарный момент сил тяжести, действующих на систему, равен нулю.

Например, в системе, состоящей из двух одинаковых масс, соединённых несгибаемым стержнем, и помещённой в неоднородное гравитационное поле (например, планеты), центр масс будет находиться в середине стержня, в то время как центр тяжести системы будет смещён к тому концу стержня, который находится ближе к планете (ибо вес массы `P = m·g` зависит от параметра гравитационного поля g), и, вообще говоря, даже расположен вне стержня.

В однородном гравитационном поле центр тяжести всегда совпадает с центром масс. В некосмических задачах гравитационное поле обычно может считаться постоянным в пределах объёма тела, поэтому на практике эти два центра почти совпадают.

По этой же причине понятия центр масс и центр тяжести совпадают при использовании этих терминов в геометрии, статике и тому подобных областях, где применение его по сравнению с физикой можно назвать метафорическим и где неявно предполагается ситуация их эквивалентности (поскольку реального гравитационного поля нет, то и учёт его неоднородности не имеет смысла). В этих применениях традиционно оба термина синонимичны, и нередко второй предпочитается просто в силу того, что он более старый.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Guidance, Navigation & Control (GNC)](gnc.md)】**<br> [CAN](can.md) ~~ [LVDS](lvds.md) ~~ [MIL‑STD‑1553](mil_std_1553.md) (МКО) ~~ [OS](os.md) ~~ [RS‑232, 422, 485](rs_xxx.md) ~~ [SpaceWire](spacewire.md) ~~ [АСН, САН](ans.md) ~~ [БНО](nnb.md)[MIL‑STD‑1553](mil_std_1553.md) (МКО)[БАППТ](eas.md) ~~ [БКС](cable.md) ~~ [БУ](eas.md) ~~ [БШВ](time.md) ~~ [Гироскоп](iu.md) ~~ [Дальномер](doppler.md) (ИСР) ~~ [ДМ](iu.md) ~~ [ЗД](sensor.md) ~~ [Компьютер](obc.md) (ЦВМ, БЦВМ) ~~ [Магнитометр](sensor.md) ~~ [МИХ](mic.md) ~~ [МКО](mil_std_1553.md) ~~ [ПО](soft.md) ~~ [ПНА, ПОНА, ПСНА](devd.md) ~~ [СД](sensor.md) ~~ [Система координат](coord_sys.md) ~~ [СОСБ](devd.md)|
|**【[Control](Control.md)】**<br> [Ad hoc](ad_hoc.md) ~~ [Business travel](business_travel.md) ~~ [Chief designers council](cocd.md) ~~ [CML](trl.md) ~~ [Competence](competence.md) ~~ [Confident](confident.md) ~~ [Consp.theory](consp_theory.md) ~~ [Control sys. (CS)](cs.md) ~~ [Coordinate system](coord_sys.md) ~~ [Curator](curator.md) ~~ [Designer’s supervision](des_spv.md) ~~ [E‑sig](esig.md) ~~ [Engineer](se.md) ~~ [Errand](errand.md) ~~ [Federal law](fed_law.md) ~~ [Federal TP](fed_tp.md) ~~ [Federal SP](fed_sp.md) ~~ [GNC](gnc.md) ~~ [Gravity assist](gravass.md) ~~ [Industrial archaeology](ind_arch.md) ~~ [Instruction](instruction.md) ~~ [Lean manuf.](lean_man.md) ~~ [Lifetime](lifetime.md) ~~ [Manager](manager.md) ~~ [MBSE](se.md) ~~ [Meeting](meeting.md) ~~ [MCC](sc.md) ~~ [MIC](mic.md) ~~ [MML](mml.md) ~~ [MoU](contract.md) ~~ [Nav. & ballistics (NB)](nnb.md) ~~ [Nonprofit org.](nonprof_org.md) ~~ [NX](nx.md) ~~ [Oberth effect](oberth_eff.md) ~~ [Org.structure](orgstruct.md) ~~ [Outcomes commission](outccom.md) ~~ [Patent](patent.md) ~~ [Peter prin.](peter_principle.md) ~~ [Plan](plan.md) ~~ [PMBok](pmbok.md) ~~ [Quorum](quorum.md) ~~ [R&D management](mgmt.md) ~~ [R&D support](rnd_support.md) ~~ [Recursion](recurs.md) ~~ [Schulze_method](schulze_method.md) ~~ [Sci'N'Tech activities](st_act.md) ~~ [Sci'N'Tech council](satc.md) ~~ [Single-window system](sw_sys.md) ~~ [Situ.leadership](situ_leadership.md) ~~ [Skunk works](se.md) ~~ [State arm. plan](plan_sa.md) ~~ [Swamp](swamp.md) ~~ [Teamcenter](teamcenter.md) ~~ [Tennis racket theorem](tr_theorem.md) ~~ [TRIZ](triz.md) ~~ [TRL](trl.md) ~~ [V‑model](v_model.md) ~~ [Veto](veto.md) ~~ [Workflow](workflow.md) ~~ [Workgroup](wg.md)|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Center_of_mass>


## The End

end of file
