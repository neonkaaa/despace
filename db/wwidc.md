# Worldview in different countries
> 2021.04.12 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---



Here is a bit of notes concerning culture differencies. Of course, nothing personal and results may vary.

1. **General:**
   1. — How the process is going on?<br> — Not finished yet.<br> — Okay. Then on what phase is it?<br> — We will begin working on it soon.
1. **Finland:**
   1. I believe also the Finnish mentality plays a role here. We are very shy, as you may have heard! I think the professors may even think that if they ask directly from you, it may be impolite towards you as you don’t know well each others (and have not physically met yet). All this will change with time. Cultural issues are quite challenging, I guess.
1. **Russia:**
   1. Sometimes they think there will be no future for a project (phase, process, etc.), or nobody need it, or that we will come back to this matter not earlier than in X years, and that the point of current activities is to gain some money and forget about it. Thus they can create documentation which will show that everything is fine, while the real picture can be different.



## Docs/Links
|**Sections & pages**|
|:--|
|**【[](.md)】**<br> <mark>NOCAT</mark>|

1. Docs: …
1. …


## The End

end of file
