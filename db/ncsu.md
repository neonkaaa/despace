# NCSU
> 2019.08.08 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/n/ncsu_logo1t.webp)](f/c/n/ncsu_logo1.webp)|<newstips@ncsu.edu>, +1(919)515-20-11, Fax …;<br> *Campus Box 7001, A Holladay Hall, Raleigh, NC, 27695, USA*<br> <https://www.ncsu.edu> ~~ [FB ⎆](https://www.facebook.com/ncstate) ~~ [IG ⎆](https://www.instagram.com/ncstate) ~~ [LI ⎆](https://www.linkedin.com/edu/north-carolina‑state-university-18786) ~~ [X ⎆](https://twitter.com/ncstate) ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/North_Carolina_State_University)|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Университет штата Северная Каролина** (официально Университет штата Северная Каролина в Роли (англ. North Carolina State University at Raleigh), сокращение англ. NCSU) — государственный исследовательский университет в г. Роли, Северная Каролина, США. Один из ведущих технических вузов США. Входит в систему Университета Северной Каролины. Наряду с Университетом Дьюка и Университетом Северной Каролины в Чапел‑Хилл является одним из углов 「исследовательского треугольника」. Университет был основан 7 марта 1887 года как Северо‑Каролинский колледж сельского хозяйства и механических искусств.



## The End

end of file
