# Space Entertainment Laboratory
> 2021.12.13 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/s/space_entlab_logo2t.webp)](f/c/s/space_entlab_logo1.webp)|<info@selab.jp>, <mark>nophone</mark>, Fax …;<br> *22-17 Nishimine-machi, Ota-ku, Tokyo, Japan*<br> <http://www.selab.jp>|
|:--|:--|
|**Business**|UAV, drones, high altitude (stratosphere) balloons, monitoring|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Space Entertainment Laboratory** is a Japanese company aimed to low-to-high altitude observations & monitoring using UAV, drones, balloons. Founded 2021.

- R&D laboratories — KBIC 7-7 Shinkawasaki, Saiwai-ku, Kawasaki, Kanagawa, Japan
- Fukushima branch — B#1, 45-245 Sukakeba Kaibama, Haramachi-ku, Minamisoma city, Fukushima, Japan

<p style="page-break-after:always"> </p>

## The End

end of file
