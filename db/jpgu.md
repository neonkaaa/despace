# JpGU
> 2020.07.02 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md), **[Events](event.md)**, [JpGU](jpgu.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Japan Geoscience Union (JpGU)** — EN term. **Японское объединение по геонаукам (ДжпГУ)** — literal RU translation.</small>

|[![](f/c/j/jpgu_logo1t.webp)](f/c/j/jpgu_logo1.webp)|**4F Gakkai Center Bldg., 2-4-16 Yayoi, Bunkyo-ku, Tokyo 113-0032, Japan**|
|:--|:--|
|E‑mail|<office@jpgu.org>|
|Link|<http://www.jpgu.org>|
|Tel|<mark>nophone</mark>, Fax …|

The **Japan Geoscience Union (JpGU, 公益社団法人日本地球惑星科学連合, 以下)**, is a public interest incorporated association as defined under the Japanese law. We are an academic union that covers all the disciplines of Earth & planetary sciences & related fields. Our main activities are:

1. the annual scientific [meeting](event.md) held in spring,
1. the publication of our peer‑reviewed journal (Progress in Earth & Planetary Sciences),
1. other activities are targeted towards promotion of the science & public outreach steered by various committees & by publication of quarterly Newsletters.

The scientific functions of JpGU are currently conducted by five independent Science Sections:

1. atmospheric & hydrographic sciences,
1. biogeosciences,
1. human geosciences,
1. solid Earth sciences,
1. space & planetary sciences.

Our members are individuals who are researchers, engineers, educators, science communicators, students & the general public with interests in the science & academic societies concerned with the full range of subjects covered by Earth & planetary sciences. As of 2016.09.30, the membership of JpGU consists of over 9 000 individual members & 50 society members.

We represent the Japanese community of Earth & planetary scientists, promote the science, provide a platform for communications among the members, coordinate internationally, & reach out to the public; thus contributing to the advancement & dissemination of Earth & planetary sciences. JpGU supports international discussion & exchange that contributes to driving the boundaries of our science & understanding further forward & stimulate moving forwards the leading of science & will help invigorate the global Earth & planetary science community. We firmly believe that the advancement of our science will contribute to the good well‑being of the human race.

**Events & prices.**

JpGU have an annual paid membership, which gives an access to JpGU archives, publications, & discount for the annual meetings fees:

1. JpGU Member Regular, K-12 Teacher (elem., j.high, high, career coll.), Senior — 2 000 JPY
1. JpGU Member Graduate Student — 1 000 JPY
1. Associate Member (Undergraduate Student or younger), [AGU](agu.md) Member — 0 JPY

Averaged fees for meetings:

|**Category**|**JpGU Member Rate<br> (AGU, AOGS & EGU Member)**|**Nonmember Rate**|
|:--|:--|:--|
|Regular|14 300 JPY|23 100 JPY|
|School Teacher|7 700 JPY|14 300 JPY|
|Graduate Student|7 700 JPY|14 300 JPY|
|Senior (70 or over)|7 700 JPY|—|
|Undergraduate Student or younger|Free of charge|Free of charge|

**Previous meetings.**

|**Event**|**Dates**|**Comments**|
|:--|:--|:--|
|JpGU 2021|2021.05.30-03| |
|Joint JpGU-AGU 2020 (Virtual)|2020.07.12-14|<http://www.jpgu.org/meeting_e2020v>|
|JpGU 2019|2019.05.26-30|<http://www.jpgu.org/meeting_e2019>|
|JpGU 2018|2018.05.20-24|<http://www.jpgu.org/meeting_e2018>|
|Joint JpGU-AGU 2017|2017.05.20-25|<http://www.jpgu.org/meeting_e2017>|
|JpGU 2016|2016.05.20-25|<http://www.jpgu.org/meeting_e2016>|
|JpGU 2015|2015.05.20-28|<http://www.jpgu.org/meeting_e2015>|
|JpGU 2014|2014.04.28-02|<http://www.jpgu.org/meeting_e2014>|
|JpGU 2013|2013.05.19-24|<http://www.jpgu.org/meeting_e2013>|
|JpGU 2012|2012.05.20-25|<http://www.jpgu.org/meeting_e2012>|
|JpGU 2011|2011.05.20-25|<http://www.jpgu.org/meeting_e2011>|
|JpGU 2010|2010.05.23-28|<http://www.jpgu.org/meeting_e2010>|
|JpGU 2009|2009.05.16-21|<http://www.jpgu.org/meeting_e2009>|



## Docs/Links
|**Sections & pages**|
|:--|
|**【[Events](event.md)】**<br> **Meetings:** [AGU](agu.md) ~~ [CGMS](cgms.md) ~~ [COSPAR](cospar.md) ~~ [DPS](dps.md) ~~ [EGU](egu.md) ~~ [EPSC](epsc.md) ~~ [FHS](fhs.md) ~~ [IPDW](ipdw.md) ~~ [IVC](ivc.md) ~~ [JpGU](jpgu.md) ~~ [LPSC](lpsc.md) ~~ [MAKS](maks.md) ~~ [MSSS](msss.md) ~~ [NIAC](niac_program.md) ~~ [VEXAG](vexag.md) ~~ [WSI](wsi.md) ┊ ··•·· **Contests:** [Google Lunar X Prize](google_lunar_x_prize.md)|

1. Docs: …
1. <http://www.jpgu.org>


## The End

end of file
