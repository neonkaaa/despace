# Shoal
> 2022.01.21 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/s/shoal_logo1t.webp)](f/c/s/shoal_logo1.webp)|<support@shoalgroup.com>, +61(2)6239-4288, Fax …;<br> *Ground Floor, 309 Angas St, Adelaide, South Australia, 5000*<br> <https://www.shoalgroup.com> ~~ [LI ⎆](https://www.linkedin.com/company/shoal-group-pty-ltd)|
|:--|:--|
|**Business**|Systems engineering consulting|
|**Mission**|Making sense of a complex & changing world|
|**Vision**|To become an iconic company in systems thinking|
|**Values**|**Boldness** — we speak up; we look at what’s possible & responibly lead the way forward, even if we don’t have all the answers. **Self‑awareness** — we know what we’re good at — & what we could do better; we take action to develop ourselves & each other. **Professionalism** — we’re positive & resilient; we act ethically; we’re ambassadors for Shoal & influencers of systems thinking. **Creativity** — we look at things differently, ask questions & try new things (even though we might not succeed). **Sociability** — we support each other, seek connections, start conversations, build relationships & engage with our community.|
|**[MGMT](mgmt.md)**|…|

**Shoal** is a Australian (with office in NZ) company aimed for systems engineering consulting for areas of defence, space, transport & infrastructure.

We help our clients clearly define the complex problems they are facing & design solutions they can trust:

- **Asset management.** Systems & mission assurance, governance, risk management & operational worthiness.
- **Capability design.** Early‑phase activities linking strategic goals & objectives with capability needs, concept development & execution.
- **Capture support.** Engineering control of bid responses, tender alignment, documentation & reporting.
- **Decision analytics.** Quantitative (incl. physics‑based) evaluation of architecture, design, vulnerability, performance & integration risk for operational analysis & technical, business & strategic decision support.
- **Research & development.** Innovative Systems Engineering approaches, tools & methodologies to digitise the future & solve complex challenges.
- **Systems engineering.** Systems integration, interface development & requirements derivation & traceability through digital engineering.

<p style="page-break-after:always"> </p>

## The End

end of file
