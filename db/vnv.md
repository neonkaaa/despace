# Валидация и верификация
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [Test](test.md), [Качество](qm.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

## Валидация

> <small>**Валидация** — русскоязычный термин. **Validation** — англоязычный эквивалент.</small>

**Валида́ция** *(от лат. validus 「здоровый, крепкий; сильный」)* в технике или в системе менеджмента качества — доказательство того, что требования конкретного пользователя, продукта, услуги или системы удовлетворены.

Отличия от верификации:

1. верификация — проводится практически всегда, выполняется методом проверки (сличения) характеристик продукции с заданными требованиями, результатом является вывод о соответствии (или несоответствии) продукции,
1. валидация — проводится при необходимости, выполняется методом анализа заданных условий применения и оценки соответствия характеристик продукции этим требованиям, результатом является вывод о возможности применения продукции для конкретных условий.

Исходя из вышеописанного, валидация должна быть определена как подтверждение на основе объективных свидетельств того, что требования предопределены, а цель достигнута.

A simplified validation asks the questions: Does the system work as expected? How does the system respond to failures, faults, & anomalies? Is the system affordable? If the answer to any of these questions is no, then changes to the design or stakeholder expectations will be required, & the process is started over again.



## Верификация
> <small>**Верификация** — русскоязычный термин. **Verification** — англоязычный эквивалент.</small>

**Верификация** — *по [ГОСТ 56469](гост_56469.md)* — подтверждение с помощью объективных данных того факта, что заданные требования выполнены.

At least one method of verification have to be used. Verification methods:

1. **Testing.** Testing is a verification method using technical aids, such as special equipment, instruments, simulation methods & application of common principles & methonds to assess components, subsystems & systems for cimpliance to the requirements. Testing is a primary method of verification & is applied when analytical methods do not produce necessary results, when there are failures which may endanger the safety of personnel, have negative effect on in‑flight systems or payload functioning, or may endanger mission objectives.
1. **Analysis.** Verification by analysis applied together with or instead of other verification methods to confirm compliance to the specification requirements. The methods selected may include without limitation technical analysis, statistical & qualitative analysis, software & hardware simulation & analogue modeling. Analysis may be used when ➀ thorough & precise analys is possible, ➁ testing is unpractical due to costs, ➂ verification by inspection is insufficient.
1. **Design Review.** Design Review is a verification method where the verification is carried out by means of checking documented data or by demonstration of approved Basic Design documents or approved design reports, technical descriptions or drawings definitely supporting compliance to a requirement.
1. **Inspection.** During the Inspection principal attention is paid to examination of physical characteristics of the product to support compliance to the requirements to design elements, compliance to documentation & drawings, quality standards & physical conditions without application of special laboratory equipment, methods, testing instrumentation or services.



## Docs/Links (TRANSLATEME ALREADY)
|Navigation|
|:--|
|**[FAQ](faq.md)**【**[SCS](sc.md)**·КК, **[SC (OE+SGM)](sc.md)**·КА】**[CON](contact.md)·[Pers](person.md)**·Контакт, **[Ctrl](control.md)**·Упр., **[Doc](doc.md)**·Док., **[Drawing](draw.md)**·Чертёж, **[EF](ef.md)**·ВВФ, **[Error](faq.md)**·Ошибки, **[Event](event.md)**·Событ., **[FS](fs.md)**·ТЭО, **[HF&E](hfe.md)**·Эрго., **[KT](kt.md)**·КТ, **[N&B](nnb.md)**·БНО, **[Project](project.md)**·Проект, **[QM](qm.md)**·БКНР, **[R&D](rnd.md)**·НИОКР, **[SI](si.md)**·СИ, **[Test](test.md)**·ЭО, **[TRL](trl.md)**·УГТ|
|**[FAQ](faq.md)**【**[SCS](sc.md)**·КК, **[SC (OE+SGM)](sc.md)**·КА】**[CON](contact.md)·[Pers](person.md)**·Контакт, **[Ctrl](control.md)**·Упр., **[Doc](doc.md)**·Док., **[Drawing](draw.md)**·Чертёж, **[EF](ef.md)**·ВВФ, **[Error](faq.md)**·Ошибки, **[Event](event.md)**·События, **[FS](fs.md)**·ТЭО, **[HF&E](hfe.md)**·Эрго., **[KT](kt.md)**·КТ, **[N&B](nnb.md)**·БНО, **[Project](project.md)**·Проект, **[QM](qm.md)**·БКНР, **[R&D](rnd.md)**·НИОКР, **[SI](si.md)**·СИ, **[Test](test.md)**·ЭО, **[TRL](trl.md)**·УГТ|
|**Sections & pages**|
|**【[Test](test.md)】**<br> [JTAG](jtag.md) ~~ [Proto fligt model](pfm.md) ~~ [Безэховая камера](ach.md) ~~ [Валидация](vnv.md) ~~ [Класс чистоты](clean_lvl.md) ~~ [КПЭО](ctpr.md) ~~ [Перечень методик испытаний](list_tp.md) ~~ [Программа и методика испытаний](pmot.md) ~~ [Опытный образец](pilot_sample.md) ~~ [Циклограмма](obc.md) ~~ [Штатный образец](flight_unit.md) ~~ [ЭО](test.md) ~~ [Экспериментально‑теоретический метод](etetm.md)|
|**`Качество:`**<br> [Bus factor](bus_factor.md) ~~ [Way](faq.md) ~~ [АВПКО](fmeca.md) ~~ [Авторский надзор](des_spv.md) ~~ [Бережливое производство](lean_man.md) ~~ [Валидация, верификация](vnv.md) ~~ [Класс чистоты](clean_lvl.md) ~~ [Конструктивное совершенство](con_vel.md) ~~ [Крит. технологии](kt.md) ~~ [Крит. элементы](sens_elem.md) ~~ [Метрология](metrology.md) ~~ [Надёжность](qm.md) ~~ [Нештатная ситуация](emergency.md) ~~ [Номинал](nominal.md) ~~ [Ошибки](faq.md) ~~ [Система менеджмента качества](qms.md) ~~ [УГТ](trl.md)/[TRL](trl.md)|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Validation>


## The End

end of file
