# Лётные испытания
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [R&D](rnd.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

|**Phase**| | |**Design**| | | | |**Mass prod.:**| |
|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|
|**[R&D phases](rnd.md)**|0 (pre‑A)|A|≈ B|≈ B|≈ C|≈ C/D|≈ E|…|F|
|**[НИОКР](rnd.md)**|[НИР](rnd_0.md)|[АП](rnd_ap.md)|[ЭП](rnd_ep.md)|[ТП](rnd_tp.md)|[РКД (РРД)](rnd_rkd.md)|[Макеты, НЭО](test.md)|[ЛИ](rnd_e.md)|ПСП → СП → ПЭ|Вывод|
| |**[NIR](rnd_0.md)**|**[AP](rnd_ap.md)**|**[EP](rnd_ep.md)**|**[TP](rnd_tp.md)**|**[RKD (RRD)](rnd_rkd.md)**|**[Models, Tests](test.md)**|**[LI](rnd_e.md)**|**PSP → SP → PE**|**Closeout**|

> <small>**Лётные испытания (ЛИ)** — русскоязычный термин. **Phase E** — англоязычный эквивалент.</small>

**Лётные испытания (ЛИ)** — испытания [комплекса](sc.md) (его [изделий](unit.md)) в реальных натурных условиях функционирования и выполнения целевых задач.

**Эксплуатация изделия** — стадия жизненного цикла изделия с момента принятия его в эксплуатацию и до момента снятия с эксплуатации.

**Phase E.** Operations & sustainment.

1. **[УЗК](trl.md)** отсутствует.

ЛИ проводятся в целях:

1. всесторонней проверки и подтверждения характеристик (в т.ч. предельно допустимых их значений) комплекса (его СЧ и систем), заданных в [ТТЗ (ТЗ)](tor.md), в условиях, максимально приближённых к условиям применения и эксплуатации, и в реальных условиях функционирования;
1. отработки [ЭД](doc.md) и проверки достаточности и эффективности [экспериментальной отработки](test.md) изделий комплекса и комплекса в целом, проведения той отработки комплекса и его изделий, которую невозможно осуществить в наземных условиях;
1. определения возможности принятия комплекса в эксплуатацию и (или) решения (выполнения) им целевых задач.

ЛИ являются приёмочными государственными испытаниями комплексов. Общее руководство ЛИ осуществляет госкомиссия, назначаемая в установленном Правительством РФ порядке. В состав госкомиссии назначают представителей гос. заказчика (заказчика), полигона (космодрома), [ЦУП](sc.md), головных НИИ гос. заказчика (заказчика) и ГНИО РКП, заинтересованных федеральных органов исполнительной власти, организаций, ответственных за эксплуатацию комплекса, РАН (для КК гражданского назначения), головных организаций‑разработчиков комплекса и его основных изделий, ФГБУ 「НИИ ЦПК им. Ю.А. Гагарина」 (для ПКК), потребителей космической продукции (услуг).



## Документация ЛИ
<mark>TBD</mark>



## Рабочий процесс
1. Создание госкомиссии.
1. Создание [ГОГУ](hotg.md).
1. Написание ПЛИ и [ПОЗ](fp.md).
1. …
1. PROFIT!



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[R&D](rnd.md)】**<br> [Design review](design_review.md) ~~ [Management](mgmt.md) ~~ [MBSE](se.md) ~~ [Proposal](proposal.md) ~~ [Test](test.md) ~~ [V‑model](v_model.md) ~~ [Validation, Verification](vnv.md)<br> [АП](rnd_ap.md) ~~ [ЛИ](rnd_e.md) ~~ [Макеты, НЭО](test.md) ~~ [НИР](rnd_0.md) ~~ [РКД (РРД)](rnd_rkd.md) ~~ [ТП](rnd_tp.md) ~~ [ЭП](rnd_ep.md)|

1. Docs: …
1. <…>


## The End

end of file
