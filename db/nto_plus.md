# АТ+…
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [Fuel](ps.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Тетраоксид диазота / Азотный тетраоксид (АТ)** — русскоязычный термин. **Dinitrogen tetroxide / Nitrogen tetroxide (NTO)** — англоязычный эквивалент.</small>

Ниже приведены некоторые примечательные виды ракетного топлива на основе соединений с [АТ](nto.md).



## АТ + Гидразин
**Азотный тетраоксид + гидразин** — жидкостное ракетное топливо.

|**Параметр**|**Описание**|
|:--|:--|
|Компоненты топлива|окислитель — [АТ](nto.md)<br> горючее — [Гидразин](hydrazine.md)|
|Соотношение масс|67 % окислитель (1 443 ㎏/m³), 33 % горючее (1 010 ㎏/m³)|
|Плотность, ㎏/m³|1 217|
|Fuel — combustion products|H₂, H₂O, NH₃, N₂|
|[УИ](ps.md), с|291|
|Т в камере сгорания, K|3 287|
|[Токсичность](nfpa_704.md)|токсично, требуется [нейтрализация](нейтрализация_крт.md)|



## АТ + ММГ
**Азотный тетраоксид + монометилгидразин** — жидкостное ракетное топливо.

|**Параметр**|**Описание**|
|:--|:--|
|Компоненты топлива|окислитель — [АТ](nto.md)<br> горючее — [ММГ](mmh.md)|
|Соотношение масс|1 443 ㎏/m³ к 880 ㎏/m³ = 2.16|
|Плотность, ㎏/m³|1 200|
|Fuel — combustion products|<mark>TBD</mark>|
|[УИ](ps.md), с|325|
|Т в камере сгорания, K|3 385|
|[Токсичность](nfpa_704.md)|токсично, требуется [нейтрализация](нейтрализация_крт.md)|

N₂O₄/MMH propellant. Monomethylhydrazine (MMH) is a storable liquid fuel that found favor in the United States for use in orbital spacecraft engines. Its advantages in comparison to UDMH are higher density and slightly higher performance.



## АТ + НДМГ
**Азотный тетраоксид + несимметричный диметил гидразин** — популярное жидкостное ракетное топливо.

|**Параметр**|**Описание**|
|:--|:--|
|Компоненты топлива|окислитель — [АТ](nto.md)<br> горючее — [НДМГ](udmh.md) (гептил)|
|Соотношение масс|1 443 ㎏/m³ к 790 ㎏/m³ = 1.8159|
|Плотность, ㎏/m³|1 185 (привязано к равенсту объёмов компонентов топлива)|
|Fuel — combustion products|H₂, H₂O, CO, CO₂, N₂|
|[УИ](ps.md), с|344|
|Т в камере сгорания, K|3 469|
|[Токсичность](nfpa_704.md)|токсично, требуется [нейтрализация](нейтрализация_крт.md)|

1. Преимущества:
   1. большая взрывобезопасность по сравнению с парой водород+кислород
   1. самовоспламеняемость при контакте топливных компонентов
   1. возможность длительного хранения в заправленном виде при нормальных температурах
1. Недостатки:
   1. токсичность
   1. канцерогенность
   1. вероятность взрыва НДМГ в присутствии окислителя
   1. меньший удельный импульс, чем у кислородно‑керосиновой пары
   1. цена НДМГ заметно выше цены керосина, что существенно для больших ракет



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**`Топливо:`**<br> [Кавитация](cavitation.md) ~~ [Мятый газ](exhsteam.md) ~~ [Нейтрализация КРТ](нейтрализация_крт.md)|

1. Docs: …
1. <http://www.astronautix.com/m/monhydrazine.html>


## The End

end of file
