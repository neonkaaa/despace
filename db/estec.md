# ESTEC
> 2019.08.05 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/e/esa_logo1t.webp)](f/c/e/esa_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *Keplerlaan 1, 2201 AZ Noordwijk, Нидерланды*<br> <http://www.esa.int/About_Us/ESTEC> ~~ [Wiki 1 ⎆](https://en.wikipedia.org/wiki/European_Space_Research_and_Technology_Centre) ~~ [Wiki 2 ⎆](https://en.wikipedia.org/wiki/Concurrent_Design_Facility)|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**European Space Research & Technology Centre (ESTEC)** — подразделение [ESA](esa.md), ответственное за разработку и создание космических технологий и аппаратов, проведение испытаний. ESTEC основан в 1968 году. Включает в себя:

- **Life of a space mission**
- **Concurrent Design Facility (CDF)** is the European Space Agency main assessment center for future space missions & industrial review. Located at ESTEC, ESA’s technical center in Noordwijk in The Netherlands, it has been operational since early 2000. As suggested by its name, the CDF uses concurrent engineering methodology to perform effective, fast & cheap space mission studies. Equipped with a state‑of‑the‑art network of computers, multimedia devices & software tools, the CDF allow team of experts to perform design studies during working sessions.
- **Developing a mission**
- **Test Centre**
- **Engineering laboratories**



## The End

end of file
