# Albedo
> 2020.05.14 [🚀](../../index/index.md) [despace](index.md) → [SGM](sc.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Albedo** — EN term. **Альбедо** — RU analogue.</small>

**Альбе́до** *(лат. albus — белый)* — характеристика диффузной отражательной способности поверхности.



## Description

Albedo (/ælˈbiːdoʊ) (Latin: albedo, meaning 'whiteness') is the measure of the diffuse reflection of solar radiation out of the total solar radiation and measured on a scale from 0, corresponding to a black body that absorbs all incident radiation, to 1, corresponding to a body that reflects all incident radiation. 

## Astronomical albedo
The albedos of planets, satellites and minor planets such as asteroids can be used to infer much about their properties. The study of albedos, their dependence on wavelength, lighting angle ("phase angle"), and variation in time composes a major part of the astronomical field of photometry. For small and far objects that cannot be resolved by telescopes, much of what we know comes from the study of their albedos. For example, the absolute albedo can indicate the surface ice content of outer Solar System objects, the variation of albedo with phase angle gives information about regolith properties, whereas unusually high radar albedo is indicative of high metal content in asteroids. 

Two common albedos that are used in astronomy are the (V‑band) geometric albedo (measuring brightness when illumination comes from directly behind the observer) and the Bond albedo (measuring total proportion of electromagnetic energy reflected). Their values can differ significantly, which is a common source of confusion

*Planet’s and planetoid’s albedo in [Solar System](солнечная_система.md).*  

|**Planet**|<small>*Geometric<br> albedo *</small>|<small>*Bond<br> albedo*</small>|
|:--|:--|:--|
|[Merccury](mercury.md)|0.106|0.119|
|[Venus](venus.md)|0.65|0.76|
|[Earth](earth.md)|0.367|0.306|
|[Mars](mars.md)|0.15|0.16|
|[Jupiter](jupiter.md)|0.52|0.343|
|[Saturn](saturn.md)|0.47|0.342|
|[Uranus](uranus.md)|0.51|0.3|
|[Neptune](neptune.md)|0.41|0.29|
|[Pluto](pluto.md)|0.6|0.5|

![](f/aob/universe/albedo_e_hg.webp)



## Ламбертово (истинное, плоское) альбедо
Истинное или плоское альбедо — коэффициент диффузного отражения, то есть отношение светового потока, рассеянного плоским элементом поверхности во всех направлениях, к потоку, падающему на этот элемент. Обычно определяется с помощью специального фотометрического прибора — альбедометра.

В случае освещения и наблюдения, нормальных к поверхности, истинное альбедо называют нормальным.

Нормальное альбедо чистого снега составляет ~0,9, древесного угля ~0,04.



## Geometric albedo
В планетной фотометрии геометрическое (плоское) альбедо `Ar = E0/Eπ` определяется отношением освещённости у Земли, создаваемой планетой в полной фазе **E0**, к освещённости **Eπ**, которую создал бы плоский абсолютно белый экран того же размера, что и планета, расположенный на её месте перпендикулярно лучу зрения и солнечным лучам.

Геометрическое оптическое альбедо [Луны](moon.md) — 0.12, [Земли](earth.md) — 0.367.



## The Bond albedo
Альбедо Бонда **Ac** определяется как отношение светового потока, рассеянного сферическим телом во всех направлениях, к потоку, падающему на тело. Альбедо Бонда связано с геометрическим альбедо следующим отношением: `Ac = Ar·Q`, где:  
**Ar** — геометрическое альбедо;  
**Q** — фазовый интеграл, учитывающий только ту часть освещённой поверхности, которая видна наблюдателю.

Бондовское альбедо Земли — около 0.29, Луны — 0.067.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[](.md)】**<br> <mark>NOCAT</mark>|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Albedo>
1. <https://en.wikipedia.org/wiki/Bond_albedo>
1. <https://en.wikipedia.org/wiki/Geometric_albedo>


## The End

end of file
