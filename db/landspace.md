# LandSpace
> 2022.04.07 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/l/landspace_logo1t.webp)](f/c/l/landspace_logo1.webp)|<bp@landspace.com>, +86(10)678-651-35, Fax +86(10)678-652-55;<br> *Floor H1, AVIC International Plaza, No. 13 Ronghua South Road, Yizhuang Economic & Technological Development Zone, Beijing, China*<br> <http://www.landspace.com> ~~ [LI ⎆](https://www.linkedin.com/company/landspace-technology) ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/LandSpace)|
|:--|:--|
|**Business**|R&D & operations of small & medium LV, launch services|
|**Mission**|Land Your Dream In Space|
|**Vision**|Forging a 1st‑class commercial aerospace enterprise in the world|
|**Values**|Integrity, openness, innovation, passion|
|**[MGMT](mgmt.md)**|・CEO, Founder — Changwu Zhang<br> ・CTO — Shufan Wu|

**LandSpace** (Chinese: 蓝箭; pinyin: Lán Jiàn; lit. 'Blue Arrow') or **Landspace Technology Corporation** (Chinese: 蓝箭空间科技; pinyin: Lán Jiàn Kōngjiān Kējì; lit. 'Blue Arrow Space Technology') is a Chinese private space launch company based in Beijing. It was founded in 2015 by Tsinghua University alumni Zhang Changwu.

The firm aims to develop, build & operate a solid‑fueled orbital rocket Zhuque-1, which is technologically based on the Long March 11 rocket of the Chinese government. LandSpace also aims to develop an original rocket design, the liquid‑fueled orbital rocket Zhuque-2.

<p style="page-break-after:always"> </p>

## The End

end of file
