# Harada Seiki
> 2020.07.22 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/h/harada_seiki_logo1t.webp)](f/c/h/harada_seiki_logo1.webp)|<mark>noemail</mark>, +81-53-436-7341, Fax +81-53-438-0595;<br> *4-7-9 Saiwai Naka-ku Hamamatsu City Shizuoka-pref, 433-8123 Japan*<br> <http://www.haradaseiki.co.jp> & <http://www.haradaseiki.co.jp/english/index.html>・ <https://aerospacebiz.jaxa.jp/en/partner/company/15> ~~ [LI ⎆](https://www.linkedin.com/company/原田精機株式会社)|
|:--|:--|
|**Business**|Parts for sats, rovers, small telescopes for observing Earth|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Harada Seiki Co., Ltd.** is a Japanese manufacturer of special‑purpose vehicles in the automotive industry, parts for satellites, [rovers](robot.md), ultra‑small satellites, small telescopes for observing Earth, prototyping. Founded in 2007.



## The End

end of file
