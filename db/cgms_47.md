# CGMS 47
> 2019.05.21 [🚀](../../index/index.md) [despace](index.md) → [CGMS](cgms.md), **[Events](event.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Coordination Group for Meteorological Satellites, 47th (CGMS)** — англоязычный термин, не имеющий аналога в русском языке. **47-я координационная группа по метеорологическим спутникам (CGMS)** — дословный перевод с английского на русский.</small>

|**CGMS 47**|<https://www.cgms-info.org/index_.php/cgms/meeting-detail/cgms-47>|
|:--|:--|
|Date| |
|Venue| |
|Host Org.| |
|Co‑host Org.| |
|Supported by| |
|Accepted by| |
|Content| |
|Focus| |
|Reg. Fee| |
|Attendees| |
|Related Mtg.| |
|Contact| |

**Science Organizing committee:**

1. …

**Local Organizing committee:**

1. …

**Secretariat:**

1. …



## Описание
…



## Итоги
…



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Events](event.md)】**<br> **Meetings:** [AGU](agu.md) ~~ [CGMS](cgms.md) ~~ [COSPAR](cospar.md) ~~ [DPS](dps.md) ~~ [EGU](egu.md) ~~ [EPSC](epsc.md) ~~ [FHS](fhs.md) ~~ [IPDW](ipdw.md) ~~ [IVC](ivc.md) ~~ [JpGU](jpgu.md) ~~ [LPSC](lpsc.md) ~~ [MAKS](maks.md) ~~ [MSSS](msss.md) ~~ [NIAC](niac_program.md) ~~ [VEXAG](vexag.md) ~~ [WSI](wsi.md) ┊ ··•·· **Contests:** [Google Lunar X Prize](google_lunar_x_prize.md)|

1. Docs: …
1. <…>


## The End

end of file
