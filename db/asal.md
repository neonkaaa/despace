# ASAL
> 2019.08.05 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/a/asal_logo1t.webp)](f/c/a/asal_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *…*<br> <http://www.asal.dz> ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/Algerian_Space_Agency)|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Алжирское космическое агентство (ASAL)** отвечает за алжирскую космическую программу. Основано 16 января 2002 года.



## The End

end of file
