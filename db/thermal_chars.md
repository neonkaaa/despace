# Thermal characteristics
> 2020.09.11 **[🚀](../../index/index.md) [despace](index.md) → **[TCS](tcs.md)** <mark>NOCAT</mark>  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Thermal characteristics** — EN term. **Тепловые характеристики** — RU analogue.</small>

<mark>TBD</mark>



## Thermal conductivity
> <small>**Thermal conductivity** — EN term. **Теплопроводность** — RU analogue.</small>

The **thermal conductivity** of a material is a measure of its ability to conduct heat.

Heat transfer occurs at a lower rate in materials of low thermal conductivity than in materials of high thermal conductivity. For instance, metals typically have high thermal conductivity and are very efficient at conducting heat, while the opposite is true for insulating materials like Styrofoam. Correspondingly, materials of high thermal conductivity are widely used in heat sink applications, and materials of low thermal conductivity are used as thermal insulation. The reciprocal of thermal conductivity is called thermal resistivity.

**List of thermal conductivities.** This concerns materials at atmospheric pressure & ~ 293 K (20 ℃).

|Material|Thermal conductivity [W·m⁻¹·K⁻¹]|
|:--|:--|
|Acrylic glass (Plexiglas V045i)|0.170 ‑ 0.200|
|Alcohols, oils|0.100|
|Aluminium|237|
|Alumina|30|
|Atmosphere|0.0209|
|Atmosphere (10⁻⁷ atm)|0.000119|
|Beryllia|209 ‑ 330|
|Boron arsenide|1 300|
|Copper (pure)|401|
|Diamond|1 000|
|Fiberglass or foam‑glass|0.045|
|Helium|0.142|
|Helium (100 atm)|0.147|
|Hydrogen|0.1819|
|Polystyrene expanded|0.033 ‑ 0.046|
|Polyurethane foam|0.03|
|Manganese|7.810|
|Marble|2.070 ‑ 2.940|
|Silica aerogel|0.02|
|Snow (dry)|0.050 ‑ 0.250|
|Teflon|0.250|
|Water|0.5918|



## Thermal resistivity
> <small>**Thermal resistivity** — EN term. **Термическое сопротивление** — RU analogue.</small>

**Thermal resistance** is a heat property and a measurement of a temperature difference by which an object or material resists a heat flow. Thermal resistance is the reciprocal of thermal conductance.



## Docs/Links
|**Sections & pages**|
|:--|
|**【Thermal control system (TCS)】**<br> [Thermal characteristics](thermal_chars.md) ~~ [Гермоконтейнер](гермоконтейнер.md) ~~ [Насосы для СОТР](сотр_насос.md) ~~ [Покрытия для СОТР](сотр_покрытия.md) ~~ [Радиатор](радиатор.md) ~~ [РИТ (РИТЭГ)](rtg.md) ~~ [Стандартные условия](sctp.md) ~~ [Тепловая труба](hp.md) ~~ [ТЗП](hs.md) ~~ [Тепловой аккумулятор](heat_bank.md) ~~ [ТСП](tsp.md) ~~ [Шторка](thermal_curtain.md) ~~ [ЭВТИ](mli.md)|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Thermal_conductivity>
1. <https://en.wikipedia.org/wiki/Thermal_resistance>
1. <http://www.teplotim.ru/table_kteplo_sort.html>


## The End

end of file
