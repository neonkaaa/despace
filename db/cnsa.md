# CNSA
> 2019.08.05 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/c/cnsa_logo1t.webp)](f/c/c/cnsa_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *…*<br> <http://www.cnsa.gov.cn> ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/China_National_Space_Administration)|
|:--|:--|
|**Business**|Chinese Space Agency|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Китайское национальное космическое управление** *(CNSA, кит. трад. 國家航天局, упр. 国家航天局, пиньинь: Guó Jiā Háng Tiān Jú, палл.: Го Цзя Хан Тянь Цзюй, буквально национальная аэрокосмическая администрация)* — национальное космическое агентство Китайской народной республики, ответственное за национальную космическую программу. Основано в 1993 году.



## The End

end of file
