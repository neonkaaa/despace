# ECSS
> 2023.07.07 [🚀](../../index/index.md) [despace](index.md) → [Doc](doc.md), [SC](sc.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---



## Document tree & status

**ECSS Disciplines** (as of 2021.09.15)

1. **ECSS-S-ST-00C** — System description
2. **[ECSS-S-ST-00-01C](ecss_sst0001.md)** — Glossary of terms

**Space engineering branch**

1. E-10 discipline — System engineering
2. E-20 discipline — Electrical & optical engineering
3. E-30 discipline — Mechanical engineering
4. E-40 discipline — Software engineering
5. E-50 discipline — Communications
6. E-60 discipline — Contol engineering
7. E-70 discipline — Ground systems & operations

**Space product assurance branch**

1. Q-10 discipline — Product assurance management
2. Q-20 discipline — Quality assurance
3. Q-30 discipline — Dependability
4. Q-40 discipline — Safety
5. Q-60 discipline — EEE components
6. Q-70 discipline — Materials, mechanical parts & processes
7. Q-80 discipline — Software product assurance

**Space project management branch**

1. M-10 discipline — Project planning & implementation
2. M-40 discipline — Configuration & information management
3. M-60 discipline — Cost & schedule
4. M-70 discipline — Integrated logistic support
5. M-80 discipline — Risk management

**Space sustainability branch**

1. U-10 discipline — Space debris
2. U-20 discipline — Planetary protection
3. U-30 discipline — Space situational awareness



### Space engineering

**E‑10 discipline — System engineering**

1. **[ECSS-E-ST-10C](ecss_est10.md)** — System engineering general requirements
2. **[ECSS-E-ST-10-02C](ecss_est1002.md)** (✘) — Verification
3. **[ECSS-E-ST-10-03C](ecss_est1003.md)** (✘) — Testing
4. **ECSS-E-ST-10-04C** — Space environment
5. **[ECSS-E-ST-10-06C](ecss_est1006.md)** — Technical requirements specification
6. **[ECSS-E-ST-10-09C](ecss_est1009.md)** (✘) — Reference coordinate system
7. **ECSS-E-ST-10-11C** — Human factors engineering
8. **ECSS-E-ST-10-12C** — Calculation of radiation received & its effects, & a policy for design margins
9. **[ECSS-E-ST-10-24C](ecss_est1024.md)** (✘) — Interface management
10. **ECSS-E-AS-11C** — Definition of TRLs & their criteria of assessment (ISO 16290)

**E‑20 discipline — Electrical & optical engineering**

1. **ECSS-E-ST-20C** — Electrical & electronic
2. **ECSS-E-ST-20-01C** — Multifactor design & test
3. **ECSS-E-ST-20-06C** — Spacecraft charging
4. **ECSS-E-ST-20-07C** — Electromagnetic compatibility
5. **ECSS-E-ST-20-08C** — Photovoltaic assemblies & components
6. **ECSS-E-ST-20-20C** — Interface requirements for electrical power
7. **ECSS-E-ST-20-21C** — Interface requirements for electrical actuators
8. **ECSS-E-ST-20-40C** — ASIC & FPGA engineering

**E‑30 discipline — Mechanical engineering**

1. **[ECSS-E-ST-31C](ecss_est31.md)** (✘) — Thermal control general requirements
2. **ECSS-E-ST-31-02C** — Two‑phase heat transport equipment
3. **ECSS-E-ST-31-04C** — Exchange of thermal analysis data
4. **[ECSS-E-ST-32C](ecss_est32.md)** (✘) — Structural general requirements
5. **ECSS-E-ST-32-01C** — Fracture control
6. **ECSS-E-ST-32-02C** — Structural design & verification of pressurized hardware
7. **ECSS-E-ST-32-03C** — Structural finite element models
8. **ECSS-E-ST-32-08C** — Materials
9. **ECSS-E-ST-32-10C** — Structural factors of safety for spaceflight hardware
10. **ECSS-E-ST-32-11C** — Modal survey assessment
11. **ECSS-E-ST-33-01C** — Mechanisms
12. **ECSS-E-ST-33-11C** — Explosive subsystems & devices
13. **ECSS-E-ST-34C** — Environmental control & life support
14. **ECSS-E-ST-35C** — Propulsion general requirements
15. **ECSS-E-ST-35-01C** — Liquid & electric propulsion for spacecraft
16. **ECSS-E-ST-35-02C** — Solid propulsion for spacecraft & launchers
17. **ECSS-E-ST-35-03C** — Liquid propulsion for launchers
18. **ECSS-E-ST-35-06C** — Cleanliness requirements for spacecraft propulsion hardware
19. **ECSS-E-ST-35-10C** — Compatibility testing for liquid propulsion systems

**E‑40 discipline — Software engineering**

1. **ECSS-E-ST-40C** — Software general requirements
2. **ECSS-E-ST-40-07C** — Simulation modelling platform
3. **ECSS-E-ST-40-08C** — Simulation modelling platform, level 2

**E‑50 discipline — Communications**

1. **ECSS-E-ST-50C** — Communications
2. **ECSS-E-ST-50-02C** — Ranging & Doppler tracking
3. **ECSS-E-ST-50-05C** — Radio frequency & modulation
4. **ECSS-E-ST-50-21C** — TM syncronization & channel coding (CCSDS 131.0‑B‑3)
5. **ECSS-E-ST-50-22C** — TM space data link protocol (CCSDS 132.0‑B2)
6. **ECSS-E-ST-50-23C** — AOS space data link protocol (CCSDS 732.0‑B‑3)
7. **ECSS-E-ST-50-24C** — TC syncronization & channel coding (CCSDS 231.0‑B‑3)
8. **ECSS-E-ST-50-25C** — TC space data link protocol (CCSDS 232.0‑B‑3)
9. **ECSS-E-ST-50-26C** — Communications operation procedure‑1 (CCSDS 232.1‑B‑2)
10. **ECSS-E-ST-50-11C** — SpaceFibre — Very high‑speed serial link
11. **ECSS-E-ST-50-12C** — SpaceWire — Links, nodes, routers & networks
12. **ECSS-E-ST-50-13C** — Interface & communication protocol for Mil‑Std‑1553B
13. **ECSS-E-ST-50-14C** — Spacecraft discrete interfaces
14. **ECSS-E-ST-50-15C** — CAN bus extension protocol
15. **ECSS-E-ST-50-16** — Time‑triggered ethernet (TTE)
16. **ECSS-E-ST-50-51C** — SpaceWire — Protocol identification
17. **ECSS-E-ST-50-52C** — SpaceWire — Remote memory access protocol
18. **ECSS-E-ST-50-53C** — SpaceWire — CCSDS packet transfer protocol

**E‑60 discipline — Contol engineering**

1. **ECSS-E-ST-60-10C** — Control performances
2. **ECSS-E-ST-60-20C** — Star sensor terminology & performance specification
3. **ECSS-E-ST-60-21C** — Gyros terminology & performance
4. **ECSS-E-ST-60-30C** — Attitude & orbit control system (AOCS) requirements

**E‑70 discipline — Ground systems & operations**

1. **ECSS-E-ST-70C** — Ground systems & operations
2. **ECSS-E-ST-70-01C** — On‑board control procedures
3. **ECSS-E-ST-70-11C** — Space segment operability
4. **ECSS-E-ST-70-31C** — Ground systems & operations — Monitoring & control data definition
5. **ECSS-E-ST-70-32C** — Test & operations procedure language
6. **ECSS-E-ST-70-41C** — Telemetry & telecommanf packet utilization



### Space product assurance

**Q-10 discipline — Product assurance management**

1. **[ECSS-Q-ST-10C](ecss_qst10.md)** (✘) — Product assurance management
2. **ECSS-Q-ST-10-04C** — Critical‑item control
3. **ECSS-Q-ST-10-09C** — Non‑conformance control system

**Q‑20 discipline — Quality assurance**

1. **ECSS-Q-ST-20C** — Quality assurance
2. **ECSS-Q-ST-20-07C** — Quality & safety assurance for space test centres
3. **ECSS-Q-ST-20-08C** — Storage, handling & transportation of spacecraft hardware
4. **ECSS-Q-ST-20-10C** — Off‑the‑shelf items utilization in space systems
5. **ECSS-Q-ST-20-30C** — Manufacturing & control of electronic harness

**Q‑30 discipline — Dependability**

1. **ECSS-Q-ST-30C** — Dependability
2. **ECSS-Q-ST-30-02C** — Failure modes, effects (& criticality) analysis
3. **ECSS-Q-ST-30-09C** — Availability analysis
4. **ECSS-Q-ST-30-11C** — Derating — EEE components

**Q‑40 discipline — Safety**

1. **ECSS-Q-ST-40C** — Safety
2. **ECSS-Q-ST-40-02C** — Hazard analysis
3. **ECSS-Q-ST-40-12C** — Fault tree analysis (IEC 61025)

**Q‑60 discipline — EEE components**

1. **ECSS-Q-ST-60C** — Electrical, electronic & electromechanical (EEE) components
2. **ECSS-Q-ST-60-02C** — ASIC & FPGA development
3. **ECSS-Q-ST-60-05C** — Generic procurement requirements for hybrids
4. **ECSS-Q-ST-60-12C** — Design, selection, procurement & use of die form monolithic microwave integrated circuits
5. **ECSS-Q-ST-60-13C** — Commercial EEE components
6. **ECSS-Q-ST-60-14C** — Relifing procedure — EEE components
7. **ECSS-Q-ST-60-15C** — Radiation hardness assurance — EEE components

**Q‑70 discipline — Materials, mechanical parts & processes**

1. **ECSS-Q-ST-70C** — Materials, mechanical parts & processes
2. **ECSS-Q-ST-70-71C** — Materials, processes & their data selection
3. **ECSS-Q-ST-70-80C** — Processing & quality assurance requirements for metallic powder bed fusion technologies for space applications
4. **【Cleanliness】**
5. **ECSS-Q-ST-70-01C** — Cleanliness & contamination control
6. **ECSS-Q-ST-70-05C** — Detection of organic contamination surfaces by IR spectroscopy
7. **ECSS-Q-ST-70-50C** — Particle contamination monitoring for spacecraft systems & cleanrooms
8. **【Material testing】**
9. **ECSS-Q-ST-70-02C** — Thermal vacuum outgassing test for the screening if space materials
10. **ECSS-Q-ST-70-04C** — Thermal testing for the evaluation of space materials, processes, mechanical parts & assemblies
11. **ECSS-Q-ST-70-06C** — Particles & UV rafiation testing for space materials
12. **ECSS-Q-ST-70-15C** — Non‑destructive inspection
13. **ECSS-Q-ST-70-20C** — Determination of the susceptibility of silver‑plated cooper wire & cable to "red‑plague" corrosion
14. **ECSS-Q-ST-70-21C** — Flammability testing for the screening of space materials
15. **ECSS-Q-ST-70-29C** — Determination of off‑gassing products from materials & assembled articles to be used in a manned space vehicle crew compartment
16. **ECSS-Q-ST-70-36C** — Material selection for controlling stress‑corrosion cracking
17. **ECSS-Q-ST-70-37C** — Determination of the susceptibility of metals to stress‑corrosion cracking
18. **ECSS-Q-ST-70-45C** — Mechanical testing of metallic materials
19. **【Material processes】**
20. **ECSS-Q-ST-70-03C** — Black‑anodizing of metals with inorganic dyes
21. **ECSS-Q-ST-70-09C** — Measurements of thermo‑optical properties of thermal control materials
22. **ECSS-Q-ST-70-13C** — Measurements of the peel & pull‑off strengyh of coatings & finishes using pressure‑sensitive tapes
23. **ECSS-Q-ST-70-17C** — Durability testing of coatings
24. **ECSS-Q-ST-70-22C** — Control of limited shelf‑life materials
25. **ECSS-Q-ST-70-31C** — Application of paints & coatings on space hardware
26. **【Assembling processes】**
27. **ECSS-Q-ST-70-14C** — Corrosion
28. **ECSS-Q-ST-70-26C** — Crimping of high‑reliability electrical connections
29. **ECSS-Q-ST-70-28C** — Repair & modification of printed circuit board assemblies for space use
30. **ECSS-Q-ST-70-30C** — Wire wrapping of high‑reliability electrical connections
31. **ECSS-Q-ST-70-39C** — Welding of metallic materials for flight hardware
32. **ECSS-Q-ST-70-40C** — Processing & quality assurance requirements for brazing of flight hardware
33. **ECSS-Q-ST-70-61C** — High reliability assembly for surface mount & through hole connections
34. **ECSS-Q-ST-70-80C** — Processing & quality assurance requirements for metallic powder bed fusion technologies for space applications
35. **【Parts】**
36. **ECSS-Q-ST-70-12C** — Design tules for printed circuit boards
37. **ECSS-Q-ST-70-18C** — Preparation, assembly & mounting of RF coaxial cables
38. **ECSS-Q-ST-70-46C** — Requirements for manufacturing & procurement of threaded fasteners
39. **ECSS-Q-ST-70-60C** — Qualification & procurement of printed circuit boards
40. **【Planetary protection】**
41. **ECSS-Q-ST-70-53C** — Materials & hardware compatibility tests for sterilization processes
42. **ECSS-Q-ST-70-54C** — Ultra cleaning of flight hardware
43. **ECSS-Q-ST-70-55C** — Microbiological examination of flight hardware & cleanrooms
44. **ECSS-Q-ST-70-56C** — Vapour phase bioburden reduction for flight hardaware
45. **ECSS-Q-ST-70-57C** — Dry hear bioburden reduction for flight hardware
46. **ECSS-Q-ST-70-58C** — Bioburden control in cleanrooms

**Q‑80 discipline — Software product assurance**

1. **ECSS-Q-ST-80C** — Software product assurance
2. **ECSS-Q-ST-80-10C** — Software security in space systems lifecycles



### Space project management

**M‑10 discipline — Project planning & implementation**

1. **[ECSS-M-ST-10C](ecss_mst10.md)** — Project planning & implementation
2. **ECSS-M-ST-10-01C** — Organization & conduct of reviews
3. **ECSS-M-ST-10-02C** — Verification

**M‑40 discipline — Configuration & information management**

1. **[ECSS-M-ST-40C](ecss_mst40.md)** (✘) — Configuration & information management

**M‑60 discipline — Cost & schedule**

1. **[ECSS-M-ST-60C](ecss_mst60.md)** (✘) — Cost & schedule management

**M‑70 discipline — Integrated logistic support**

1. **ECSS-M-ST-70A** — Integrated logistic support

**M‑80 discipline — Risk management**

1. **[ECSS-M-ST-80C](ecss_mst80.md)** — Risk management



### Space sustainability

**U‑10 discipline — Space debris**

1. **[ECSS-U-AS-10C](ecss_uas10.md)** (✘) — Space systems — Space debris mitigation requirements

**U‑20 discipline — Planetary protection**

1. **ECSS-U-ST-20C** — Planetary protection

**U‑30 discipline — Space situational awareness**

1. …


### Policy (P) & Configuration & information management (D) documents

**D‑00 discipline — Configuration & information management**

1. **ECSS-D-00B** — ECSS processes
2. **ECSS-D-00-01C** — Drafting rules, template for ECSS Standards
3. **ECSS-D-00-02A** — Drafting rules, template for ECSS Handbooks
4. **ECSS-D-00-11A** — Criteria for acceptance for Public Review & Criteria for Handbook release for publication

**P‑00 discipline — ECSS Policy**

1. **ECSS-P-00C** — Standardization objectives, policies & organization



## Handbooks & Technical Memoranda

1. **[Despace ECSS guide #1](ecss_guide_ds1.md)**
2. **[Margin philosophy for science assessment studies](ecss_mpsas.md)**


### Space engineering

**E‑10 discipline — System engineering**

1. **ECSS-E-HB-10-02A** — Verification guidelines
2. **ECSS-E-HB-10-03A** — Testing guidelines
3. **ECSS-E-HB-10-12A** — Calculation of radiation & its effects, & margin policy handbook
4. **ECSS-E-HB-11A** — TRL guidelines
5. **ECSS-E-TM-10-10A** — Logistics engineering
6. **ECSS-E-TM-10-20A** — Product data exchange
7. **ECSS-E-TM-10-21A** — System modelling & simulation
8. **ECSS-E-TM-10-23A** — Space system data repository
9. **ECSS-E-TM-10-25A** — Engineering design model data exchange

**E‑20 discipline — Electrical & optical engineering**

1. **ECSS-E-HB-20-01A** — Multipactor handbook
2. **ECSS-E-HB-20-02A** — Li‑Ion battery handbook
3. **ECSS-E-HB-20-03A** — Magnetic cleanliness handbook
4. **ECSS-E-HB-20-05A** — High voltage engineering & design handbook
5. **ECSS-E-HB-20-06A** — Assessment of spacecraft worst case charging
6. **ECSS-E-HB-20-07A** — Spacecraft electromagnetic compatibility handbook
7. **ECSS-E-HB-20-08A** — Guidelines for delta‑qualification of photoboltaic assemblies
8. **ECSS-E-HB-20-20A** — Guidelines for electrical interface requirements for power supply
9. **ECSS-E-HB-20-21A** — Guidelines for electrical design & interface requirements for actuators

**E‑30 discipline — Mechanical engineering**

1. **ECSS-E-HB-31-01A** — Thermal design data handbook (16 parts)
2. **ECSS-E-HB-31-03A** — Thermal analysis handbook
3. **ECSS-E-HB-32-20A** — Structural design data handbook (8 parts)
4. **ECSS-E-HB-32-21A** — Adhesive bonding handbook
5. **ECSS-E-HB-32-22A** — Insert design handbook
6. **ECSS-E-HB-32-23A** — Threaded fasteners handbook
7. **ECSS-E-HB-32-24A** — Buckling handbook
8. **ECSS-E-HB-32-25A** — Mechanical shock design & verification handbook
9. **ECSS-E-HB-32-26A** — Spacecraft mechanicalloads analysis handbook

**E‑40 discipline — Software engineering**

1. **ECSS-E-HB-40A** — Software engineering handbook
2. **ECSS-E-HB-40-01A** — Agile software development handbook
3. **ECSS-E-HB-40-02A** — Machine learning qualification for space applocations handbook

**E‑50 discipline — Communications**

1. **ECSS-E-HB-50A** — Communications guidelines

**E‑60 discipline — Control engineering**

1. **ECSS-E-HB-60A** — Control engineering handbook
2. **ECSS-E-HB-60-10A** — Control performance guidelines

**E‑70 discipline — Ground systems & operations**

1. …



### Space product assurance

**Q‑10 discipline — Product assurance management**

1. …

**Q‑20 discipline — Quality assurance**

1. …

**Q‑30 discipline — Dependability**

1. **ECSS-Q-HB-30-01A** — Worst case analysis
2. **ECSS-Q-HB-30-03A** — Human dependability handbook
3. **ECSS-Q-HB-30-08A** — Component reliability data sources & their use
4. **ECSS-Q-TM-30-12A** — End‑of‑life parameters drifts — EEE components

**Q‑40 discipline — Safety**

1. **ECSS-Q-TM-40-04** — Sneak analysis — Part 1: Principles & requirements
2. **ECSS-Q-TM-40-04** — Sneak analysis — Part 2: Clue list

**Q‑60 discipline — EEE components**

1. **ECSS-Q-HB-60-02A** — Technique for radiation effects mitigation in ASICs & FPGAs handbook

**Q‑70 discipline — Materials, mechanical parts & processes**

1. **ECSS-Q-TM-70-51A** — Termination of optical fibres
2. **ECSS-Q-TM-70-52A** — Kinetic outgassing of materials for space

**Q‑80 discipline — Software product assurance**

1. **ECSS-Q-HB-80-01A** — Reuse of existing software
2. **ECSS-Q-HB-80-02** — Software process assessment & improvement — Part 1: Framework
3. **ECSS-Q-HB-80-02** — Software process assessment & improvement — Part 2: Assessor instrument
4. **ECSS-Q-HB-80-03A** — Methods & techniques to support the assessment of software dependability & safety
5. **ECSS-Q-HB-80-04A** — Software metrication programme definition & implementation



## Docs/Links
|**Sections & pages**|
|:--|
|**【[Documents](doc.md)】**<br> **Схема:** [КСС](ксс.md) ~~ [ПГС](пгс.md) ~~ [ПЛИС](плис.md) ~~ [СхД](draw.md) ~~ [СхО](draw.md) ~~ [СхПЗ](draw.md) ~~ [СхЧ](draw.md) ~~ [СхЭ](draw.md)<br> [Interface](interface.md) ~~ [Mission proposal](proposal.md)|

1. Docs: …
1. <https://ecss.nl/>


## The End

end of file
