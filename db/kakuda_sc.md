# Kakuda Space Center
> 2020.07.22 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/j/jaxa_logo1t.webp)](f/c/j/jaxa_logo1.webp)|<mark>noemail</mark>, +81-224-68-3111, Fax …;<br> *1 Koganesawa, Kimigaya, Kakuda-shi, Miyagi 981-1525, Japan*<br> <https://global.jaxa.jp/about/centers/kspc/index.html> ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/Kakuda_Space_Center)|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Kakuda Space Center** (角田宇宙センター, Kakuda Uchu Sentaa) is a facility of the [JAXA](jaxa.md), located in the city of Kakuda in Miyagi Prefecture in northern Japan, specializing in the development and testing of [rocket engines and space propulsion systems](ps.md). Founded in 1965.

The LE-5 (incl. the LE-5A/LE-5B version), and the LE-7 (incl. the LE-7A version) rocket enginese were developed at the Kakuda Space Center.

Main Facilities

- Cryogenic Advanced Turbopump Test Stand
- High altitude test stand for development
- High altitude test stand for research use
- High Enthalpy Shock Tunnel (HIEST)
- Integrated feed system test stand (FETS)
- Ramjet Engine Test Facility
- Reseacher Interchange Bldg



## The End

end of file
