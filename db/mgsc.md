# Морская группировка космических аппаратов
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [Project](project.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Морская группировка космических аппаратов (МГКА)** — русскоязычный термин, не имеющий аналога в английском языке. **Marine group of spacecrafts** — дословный перевод с русского на английский.</small>

**Морская группировка космических аппаратов (МГКА)** — совокупность [космических аппаратов](sc.md), расположенных в морях и океанах Земли в соответствии с баллистической структурой и объединённых общностью решаемых задач в составе космической системы или комплекса после окончания [САС](lifetime.md).



## Состав группировки
1. 2012.01.15 — [Фобос‑Грунт](фобос_грунт.md) (фрагментарно)
1. 2017.11.28 — РБ 「Фрегат」 с 19 спутниками.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Project](project.md)】**<br> [Interferometer](interferometer.md) ~~ [NASA open](nasa_open.md) ~~ [NASA STI](nasa_sti.md) ~~ [NIH](nih.md) ~~ [Past, future and everything](pfaeverything.md) ~~ [PSDS](us_psds.md) [MGSC](mgsc.md) ~~ [Raman spectroscopy](spectroscopy.md) ~~ [SC price](sc_price.md) ~~ [SC typical forms](sc.md) ~~ [Spectrometry](spectroscopy.md) ~~ [Tech derivative laws](td_laws.md) ~~ [View](view.md)|

1. Docs: …
1. <…>


## The End

end of file
