# On-board computer (a list)
> 2019.07.31 [🚀](../../index/index.md) [despace](index.md) → [ЦВМ](obc.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

A list of [On-board computers](obc.md).



## Current — AU

### AICraft Pulsar   ［AU］

**Pulsar** — [on-board computer](obc.md). Designed by [AICraft](aicraft.md) in ….

|**Characteristics**|**(Pulsar Edge)**|**(Pulsar Pro)**|
|:--|:--|:--|
|Composition|1 unit|1 unit|
|Consumption, W|5 (7 peak)|40|
|Dimensions, ㎜|95 × 90 × 21|95 × 90 × 50|
|[Interfaces](interface.md)|UART 5 Mbps, Ethernet 1 Gbps (option: 2.5 Gbps), USB 3.0 5 Gbps, SpaceWire 200 Mbps (optional), CAN, SPI|UART 5 Mbps, Ethernet 1 Gbps (option: 2.5 Gbps), USB 3.0 5 Gbps, SpaceWire 200 Mbps (optional), CAN, SPI|
|[Lifetime](lifetime.md), h(y)| | |
|Mass, ㎏|0.25|0.8|
|[Overload](vibration.md), Grms| | |
|[Radiation](ion_rad.md), ㏉(㎭)| | |
|[Reliability](qm.md)| | |
|[Thermal](tcs.md), ℃|−40 ‑ +105 (−40 ‑ +115 storage)|−40 ‑ +100 (−50 ‑ +125 storage)|
|[TRL](trl.md)| | |
|[Voltage](sps.md), V|5 VDC|12 VDC|
|**【Specific】**|~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |
|Bit depth|64|
|CPU type|Multi-core ARMv8, ML co-processor|Multi-core ARMv8, ML co-processor|
|Memory|4 ㎇ DDR4 SDRAM w/ ECC, 64 ㎇ SLC NAND with ECC (up to 2TB), 16 ㎆ rad-hard-equivalent memory with EDAC|4/8 ㎇ DDR4 SDRAM w/ ECC, 1 TB SSD with ECC (up to 2TB), 16 ㎆ rad-hard-equivalent memory with EDAC|
|Performance|90 TOPS peak|600 TOPS peak|
|Recover time, s| | |
|Sensors|Vibrations: 3-axis lateral and 3-axis longitudinal, Temperature: Device, processor, co-processor, Power monitors: Device (total power), processor, co-processor|Vibrations: 3-axis lateral and 3-axis longitudinal, Temperature: Device, processor, co-processor, Power monitors: Device (total power), processor, co-processor|
|Software|Linux, ML compiler: All common frameworks|Linux, ML compiler: All common frameworks|
| |[![](f/cpu/a/aicraft_pulsar_edge_pic1t.webp)](f/cpu/a/aicraft_pulsar_edge_pic1.webp)|[![](f/cpu/a/aicraft_pulsar_pro_pic1t.webp)](f/cpu/a/aicraft_pulsar_pro_pic1.webp)|



## Current — EU

### Bradford OBC   ［EU］

**OBC** — [on-board computer](obc.md). Designed by [Bradford](bradford_eng.md) in ….

|**Characteristics**|**OBC**|
|:--|:--|
|Composition|1 unit (2 cold-redun. microcontrollers, 2 hot-redun. real-time clocks)|
|Consumption, W|3|
|Dimensions, ㎜|20×120×135|
|[Interfaces](interface.md)|・1× CAN<br> ・8× RS-485 or 4× RS-422<br> ・3× SPI<br> ・2× UART with flow control<br> ・20× GPIO, 12 interrupt-enabled<br> ・12× 12-bit analog inputs<br> ・Redundant CAN bus<br> ・100 Mbps Ethernet bus<br> ・Pulse-command bus|
|[Lifetime](lifetime.md), h(y)|… (5) / …|
|Mass, ㎏|0.5|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)|300 (30 000)|
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃|−30 ‑ +60 (−40 ‑ +70 storage)|
|[TRL](trl.md)|6 in 2023|
|[Voltage](sps.md), V|28|
|**【Specific】**|~ ~ ~ ~ ~ |
|Bit depth| |
|CPU type|Dual ARM Cortex-R4F 32bit 200㎒|
|Memory|・4 ㎆ MRAM for sensitive program and payload data<br> ・256 ㎆ DRAM for program calculations<br> ・Shared 2×1 ㎇ FLASH for payload data<br> ・Shared 2×4 ㎇ MRAM for state recovery & diagnostics|
|Performance| |
|Recover time, s| |
|Software| |
| |[![](f/cpu/b/bradford_obc_pic1t.webp)](f/cpu/b/bradford_obc_pic1.webp)|

**Notes:**

1. …
1. **Applicability:** …



### KP Labs Antelope   ［EU］

**Antelope** — [on-board computer](obc.md). Designed by KP Labs in ….

|**Characteristics**|**(Antelope OBC)**|**(Antelope OBC+DPU)**|**(Antelope DPU)**|
|:--|:--|:--|:--|
|Composition|1 unit (w/o case)|1 unit (w/o case)|1 unit (w/o case)|
|Consumption, W|5|10|5|
|Dimensions, ㎜|PC-104 board<br> (95 × 90 × 21 avg.case)|(95 × 90 × 21 avg.case)|Daughterboard 70 × 40<br> (95 × 90 × 21 avg.case)|
|[Interfaces](interface.md)|CAN, I2C, GPIO, SPI, RS‑422/485, UART, GPS PPS| |LVDS, SPI, USB 2.0, USB 3.0, UART, CAN, Ethernet, GTH transceivers. LVDS for X/S‑Band radios & CCSDS comms upon request|
|[Lifetime](lifetime.md), h(y)| | | |
|Mass, ㎏| | | |
|[Overload](vibration.md), Grms| | | |
|[Radiation](ion_rad.md), ㏉(㎭)| | | |
|[Reliability](qm.md)| | | |
|[Thermal](tcs.md), ℃|−40 ‑ +85|−40 ‑ +85|−40 ‑ +85|
|[TRL](trl.md)|7|7|7|
|[Voltage](sps.md), V|6 ‑ 14 (VBAT), 5 regulated|6 ‑ 14 (VBAT)|6 ‑ 14 (VBAT)|
|**【Specific】**|~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |
|Bit depth| | | |
|CPU type|RM57 Herkules microcontroller: Dual 300 ㎒ ARM Cortex-R5F w/ FPU in lock‑step| |Zynq UltraScale+ MPSoC ZU2/ZU3/ZU4/ZU5: Quad ARM Cortex-A53 CPU up to 1.5 ㎓, Dual ARM Cortex-R5 in lock‑step, FPGA for custom function implementation|
|Memory|8/16 ㎆ of MRAM, 64 ㎆ of redundant NOR, 4/8 ㎇ SLC flash‑based filesystem NAND storage w/ ECC| |8 ㎇ DDR4 with ECC, 4 ㎇ SLC NAND Flash, Optional SATA SSD|
|Performance| | | |
|Recover time, s| | | |
|Software|Custom embedded software, KP Labs’s OBC Software — Oryx| |64‑bit Linux or bare‑metal applications|
| |[![](f/cpu/k/kplabs_antelope_obc_pic1t.webp)](f/cpu/k/kplabs_antelope_obc_pic1.webp)|[![](f/cpu/k/kplabs_antelope_obcdpu_pic1t.webp)](f/cpu/k/kplabs_antelope_obcdpu_pic1.webp)|[![](f/cpu/k/kplabs_antelope_dpu_pic1t.webp)](f/cpu/k/kplabs_antelope_dpu_pic1.webp)|



### CPU — GR712RC
**GR712RC** — процессор, предназначенный для использования в составе [КА](sc.md).  
Разработчик [Cobham](cobham.md). Разработано в 2019 году.

|**Characteristics**|**GR712RC**|
|:--|:--|
|Composition|1 unit|
|Consumption, W| |
|Dimensions, ㎜|75 × 75 × 3.5|
|[Interfaces](interface.md)|・4× [SpaceWire](spacewire.md) ports, maximum 200 Mbps full-duplex data rate;<br> ・Redundant [MIL-STD-1553B](mil_std_1553.md) BRM (BC/RT/BM) interface;<br> ・2× CAN 2.0B bus controllers;<br> ・6× UART ports, with 8-byte FIFO;<br> ・Ethernet MAC with RMII 10/100 Mbps port;<br> ・SPI master serial port;<br> ・I2C master serial port;<br> ・ASCS16 (STR) serial port;<br> ・SLINK 6 ㎒ serial port;<br> ・CCSDS/ECSS 5‑channel Telecommand decoder, 10 Mbps input rate;<br> ・CCSDS/ECSS Telemetry encoder, 50 Mbps output rate;<br> ・26 input and 38 input/output general purpose ports|
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|0.017|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)|3 000 (300 000)|
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃|−55 ‑ +125|
|[TRL](trl.md)|9|
|[Voltage](sps.md), V|1.8 или 3.3|
|**【Specific】**|~ ~ ~ ~ ~ |
|Bit depth|32 бит|

|Memory, ㎅|16 — multi-way instruction cache; 16 — multi-way data cache; 192 — memory block with EDAC|
|Commands,<br> sensors,<br> inputs|… — команд управления;<br>… — релейных матричных команд управления;<br>… — ТМ‑датчиков;<br>… — входов прерываний от контактных датчиков;<br>… — входов прерываний от импульсных датчиков|
|Performance|100 ㎒ (200 MIPS, 200 MFLOPS)|
|Recover time, s| |
|Software| |
| |[![](f/cpu/g/gr712rc_pic1t.webp)](f/cpu/g/gr712rc_pic1.webp)|

**Notes:**

1. <https://www.gaisler.com/index.php/products/components/gr712rc>
1. <https://www.gaisler.com/index.php/products/components/gr712rc>
1. **Applicability:** [Beresheet](beresheet.md)



## Current — RU

### BIVK‑MR   ［RU］
> <small>**БИВК‑МР** — русскоязычный термин, не имеющий аналога в английском языке. **BIVK‑MR** — дословный перевод с русского на английский.</small>

**Комплекс бортовой интегрированный вычислительный (БИВК‑МР)** — [цифровая вычислительная машина](obc.md)) для [КА](sc.md).  
Разработчик [НТЦ Модуль](ntc_module.md). Разработано в рамках ОКР 「[Аракс](araks.md)」 в 2017 году. разработка. (по состоянию на 27.06.2017)

Предшественник: [БИВК-Р](obc_lst.md). Отличается от предшественника [БИВК-Р](obc_lst.md) тем, что ряд иностранных компонентов заменён на отечественные [аналоги](analogue.md), также отсутствует горячее резервирование.

|**Characteristics**|**БИВК-МР**|
|:--|:--|
|Composition|1 unit, включающий в себя 2 независимых полукомплекта|
|Consumption, W|не более 20 Вт в режиме холодного резерва (экономичном);<br> не более 33 Вт в режиме горячего резерва (активном)|
|Dimensions, ㎜|280×135×147 (без учёта жгутов собственной кабельной сети устройства, со жгутами — 260 ㎜)|
|[Interfaces](interface.md)|1. два независимых резервированных интерфейса обмена по [МКО](mil_std_1553.md), ГОСТ Р 52070‑2003;<br>  2. интерфейс приёма внешних прерываний;<br>  3. интерфейс для выдачи релейных команд на БА КА;<br>  4. интерфейс приёма внешних релейных команд управления;<br>  5. интерфейс технологического канала ([RS232](rs_xxx.md));<br>  6. интерфейс приёма внешнего сигнала с частотой 1 ㎑;<br>  7. интерфейс сигнала сверки времени;<br>  8. интерфейс опроса ТМ‑датчиков;<br>  9. интерфейс регистра программной телеметрии (канал контроля состояния);<br>  10. интерфейс синхронизации 「1с」 ([RS422](rs_xxx.md)).|
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|7.1|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)|0.99966 за 3 года|
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|8|
|[Voltage](sps.md), V|24 ‑ 34|
|**【Specific】**|~ ~ ~ ~ ~ |
|Bit depth|32 бит, 64 бит с плавающей запятой|
|Commands,<br> sensors,<br> inputs|20 (по факту 10) — команд управления;<br> 304 (матрица 19×16), из них для потребности БИВК‑МР — 32 — релейных матричных команд управления;<br> 40 — ТМ‑датчиков;<br> 12 — входов прерываний от контактных датчиков;<br> 6 — входов прерываний от импульсных датчиков|
|CPU type|IDT79RC64V474<br> MIPS‑I Microprocessor IC — 1 Core, 64‑Bit 200㎒ 128‑PQFP (28×28)<br> Разработчик IDT, Integrated Device Technology Inc|
|Memory, ㎅|4 096 — ОЗУ (с кодом Хемминга)<br> 8 192 — ЭППЗУ<br> 32 — защищённой памяти|
|Performance|10 млн операций смеси DAIS;<br> время выполнения команд с плавающей запятой для операндов 64 разряда: сложение, вычитание — не более 3,5 мкс, умножение — не более 5 мкс.|
|Recover time, s|С момента фиксирования отказа аппаратурой автоматической реконфигурации:<br> ≤30 сек в режиме холодного резерва (экономичном);<br> ≤150 ㎳ в режиме горячего резерва (активном)|
|Software| |
| |![](f/cpu/b/bivkt.webp) [❐](f/cpu/b/bivk.webp)|

**Notes:**

1. <https://www.digikey.com/product-detail/en/idt-integrated-device-technology-inc/IDT79RC64V474-200DZ/IDT79RC64V474-200DZ-ND/2018290>
1. **Applicability:** 「[Аракс](araks.md)」



### BIVK‑R   ［RU］
> <small>**БИВК‑Р** — русскоязычный термин, не имеющий аналога в английском языке. **BIVK‑R** — дословный перевод с русского на английский.</small>

**Комплекс бортовой интегрированный вычислительный (БИВК‑Р)** — [цифровая вычислительная машина](obc.md)) для [КА](sc.md).  
Разработчик [НТЦ Модуль](ntc_module.md). Разработано в рамках ОКР 「[Luna‑25](луна_25.md)」 в 2015 году. активное использование. Заимств. с КА 「[Luna‑25](луна_25.md)」. (по состоянию на 17.06.2017)

Преемник: [БИВК-М](бивк_м.md), [БИВК-МР](obc_lst.md)

|**Characteristics**|**БИВК-Р**|
|:--|:--|
|Composition|1 unit, включающий в себя 2 независимых полукомплекта|
|Consumption, W|не более 20 Вт в режиме холодного резерва (экономичном);<br> не более 35 Вт в режиме горячего резерва (активном)|
|Dimensions, ㎜|280×135×147 (без учёта жгутов собственной кабельной сети устройства, со жгутами — 260 ㎜)|
|[Interfaces](interface.md)|1. два независимых резервированных интерфейса обмена по [МКО](mil_std_1553.md), ГОСТ Р 52070‑2003;<br>  2. интерфейс приёма внешних прерываний;<br>  3. интерфейс для выдачи релейных команд на БА КА;<br>  4. интерфейс приёма внешних релейных команд управления;<br>  5. интерфейс технологического канала ([RS232](rs_xxx.md));<br>  6. интерфейс приёма внешнего сигнала с частотой 1 ㎑;<br>  7. интерфейс сигнала сверки времени;<br>  8. интерфейс опроса ТМ‑датчиков;<br>  9. интерфейс регистра программной телеметрии (канал контроля состояния);<br>  10. интерфейс синхронизации 「1с」 ([RS422](rs_xxx.md)).|
|[Lifetime](lifetime.md), h(y)|61 320 (7) / …|
|Mass, ㎏|7.1|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)|0.99966 за 3 года|
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V|24 ‑ 34|
|**【Specific】**|~ ~ ~ ~ ~ |
|Bit depth|32 бит, 64 бит с плавающей запятой|
|Commands,<br> sensors,<br> inputs|20 (по факту 10) — команд управления;<br> 304 (матрица 19×16), из них для потребности БИВК‑Р — 32 — релейных матричных команд управления;<br> 40 — ТМ‑датчиков;<br> 12 — входов прерываний от контактных датчиков;<br> 6 — входов прерываний от импульсных датчиков|
|CPU type|IDT79RC64V474<br> MIPS‑I Microprocessor IC — 1 Core, 64‑Bit 200㎒ 128‑PQFP (28×28)<br> Разработчик IDT, Integrated Device Technology Inc|
|Memory, ㎅|4 096 — ОЗУ (с кодом Хемминга)<br> 8 192 — ЭППЗУ<br> 32 — защищённой памяти|
|Performance|10 млн операций смеси DAIS;<br> время выполнения команд с плавающей запятой для операндов 64 разряда: сложение, вычитание — не более 3,5 мкс, умножение — не более 5 мкс.|
|Recover time, s|С момента фиксирования отказа аппаратурой автоматической реконфигурации:<br> ≤30 сек в режиме холодного резерва (экономичном);<br> ≤150 ㎳ в режиме горячего резерва (активном)|
|Software| |
| |![](f/cpu/b/bivkt.webp) [❐](f/cpu/b/bivk.webp)|

**Notes:**

1. <https://www.digikey.com/product-detail/en/idt-integrated-device-technology-inc/IDT79RC64V474-200DZ/IDT79RC64V474-200DZ-ND/2018290>
1. Не может работать на [нестабилизированной шине](sps.md) в связи со входом в циклическую перезагрузку.
1. **Applicability:** [Венера‑Д](венера‑д.md) ~~ [Luna‑25](луна_25.md) ~~ [Luna‑26](луна_26.md) ~~ [Luna‑27](луна_27.md)



### BKU‑SxPA   ［RU］
> <small>**БКУ‑SxPA** — русскоязычный термин, не имеющий аналога в английском языке. **BKU‑SxPA** — дословный перевод с русского на английский.</small>

**БКУ‑SxPA** — цифровая вычислительная машина ([компьютер](obc.md)) для [КА](sc.md). Разработчик [Спутникс](sputnix.md). Разработано  

|**Characteristics**|**BKU‑SxPA**|
|:--|:--|
|Composition|2 блока (процессорный блок; маршрутизатор)|
|Consumption, W| |
|Dimensions, ㎜|179 × 167 × 30 — процессорный блок; 119 × 107 × 30 — маршрутизатор|
|[Interfaces](interface.md)|Space Plug-and-Play Architecture Standard; [SpaceWire](spacewire.md); [CAN2B](can.md)|
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|0.35 (0.2 — процессорный блок; 0.15 — маршрутизатор)|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)| |
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|Bit depth| |
|Commands,<br> sensors,<br> inputs|… — команд управления;<br> … — релейных матричных команд управления;<br> … — ТМ‑датчиков;<br> … — входов прерываний от контактных датчиков;<br> … — входов прерываний от импульсных датчиков|
|CPU type|LEON3|
|Memory, ㎅|… — ОЗУ;<br> … — ЭППЗУ;<br> … — защищённой памяти|
|Performance| |
|Recover time, s|… (from the moment the failure was detected by the automatic reconfiguration OE)|
|Software| |
| |[![](f/cpu/b/bku-sxpa_pic1t.webp)](f/cpu/b/bku-sxpa_pic1.webp)|

**Notes:**

1. [Чертёж процессорного блока ❐](f/cpu/b/bku-sxpa_sketch_bku1.pdf) ~~ [Чертёж маршрутизатора ❐](f/cpu/b/bku-sxpa_sketch_router1.pdf)
1. <http://www.sputnix.ru/ru/products/microsatellites-systems/obc-pnp/item/245-bku_sxpa_ru>
1. **Applicability:** …



### CVM-12   ［RU］
**ЦВМ‑12** — цифровая вычислительная машина ([компьютер](obc.md)) для [КА](sc.md).  
Разработчик [НТЦ Модуль](ntc_module.md). Разработано в 2004 году. Покупное изделие.

|**Characteristics**|**ЦВМ-12**|
|:--|:--|
|Composition|1 unit из 2 полукомплектов.|
|Consumption, W|20|
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|2.2|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V|27|
|**【Specific】**|~ ~ ~ ~ ~ |
|Bit depth|64|
|Commands,<br> sensors,<br> inputs|… — команд управления;<br> … — релейных матричных команд управления;<br> … — ТМ‑датчиков;<br> … — входов прерываний от контактных датчиков;<br> … — входов прерываний от импульсных датчиков|
|CPU type|R4000 (MIPS)|
|Memory, ㎅|4 096 — ОЗУ (с кодом Хэмминга);<br> 8 192 — ЭППЗУ (с кодом Хэмминга);<br> … — защищённой памяти|
|Performance|50 ㎒|
|Recover time, s|… (from the moment the failure was detected by the automatic reconfiguration OE)|
|Software| |
| |[![](f/cpu/t/cvm-12_pic1t.webp)](f/cpu/t/cvm-12_pic1.webp)|

**Notes:**

1. [Брошюра от НТЦ 「Модуль」 ❐](f/cpu/t/cvm-12_doc1.djvu)
1. <https://www.module.ru/catalog/space/central_naya_vichislitel_naya_mashina_cvm12>
1. **Applicability:** …



### MARS 4   ［RU］
**Марс‑4** — цифровая вычислительная машина ([компьютер](obc.md)) для [КА](sc.md). Разработчик [МОКБ Марс](mars_mokb.md). Разработано в рамках ОКР <mark>TBD</mark> в 2002 году. Активное использование (на 2017 год).

|**Characteristics**|**Марс-4**|
|:--|:--|
|Composition|1 unit. 4‑кратное резервирование.|
|Consumption, W|30|
|Dimensions, ㎜|156 × 205 × 214|
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|8|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|Bit depth| |
|Commands,<br> sensors,<br> inputs|… — команд управления;<br>… — релейных матричных команд управления;<br>… — ТМ‑датчиков;<br>… — входов прерываний от контактных датчиков;<br>… — входов прерываний от импульсных датчиков|
|CPU type|2 × 20 ㎒;<br> первый выполняет функции ЦП, второй — функции ввода‑вывода|
|Memory, ㎅|240 — ОЗУ;<br> 2 048 — ЭППЗУ;<br> … — защищённой памяти|
|Performance|2 × 2 500 000 оп/с|
|Recover time, s|… (с момента фиксирования отказа автоматикой)|
|Software| |
| |[![](f/cpu/m/mars-4_pic1t.webp)](f/cpu/m/mars-4_pic1.webp)|

**Notes:**

1. …
1. **Applicability:** (2005) Монитор‑Э・ (2006) Казсат・ (2009) Экспресс‑МД



### MARS 7   ［RU］
**Марс‑7** — цифровая вычислительная машина ([компьютер](obc.md)) для [КА](sc.md).  
Разработчик [МОКБ Марс](mars_mokb.md). Разработано в рамках ОКР <mark>TBD</mark> в <mark>TBD</mark> году.

|**Characteristics**|**Марс-7**|
|:--|:--|
|Composition|1 unit; 4‑кратное резервирование|
|Consumption, W|20|
|Dimensions, ㎜|147 × 204 × 158|
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / …|
|Mass, ㎏|6|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)| |
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|Bit depth| |
|Commands,<br> sensors,<br> inputs|… — команд управления;<br> … — релейных матричных команд управления;<br> … — ТМ‑датчиков;<br> … — входов прерываний от контактных датчиков;<br> … — входов прерываний от импульсных датчиков|
|CPU type|2 × 50 ㎒;<br> первый выполняет функции ЦП, второй — функции ввода‑вывода|
|Memory, ㎅|4 096 — ОЗУ;<br> 8 192 — ЭППЗУ;<br> … — защищённой памяти|
|Performance|2 × 50 000 000 оп/с|
|Recover time, s|… (с момента фиксирования отказа автоматикой)|
|Software| |
| |[![](f/cpu/m/mars-7_pic1t.webp)](f/cpu/m/mars-7_pic1.webp)|

**Notes:**

1. …
1. **Applicability:** …



### MPK-002   ［RU］
> <small>**МПК-002** — RU term w/o analogues in English. **MPK-002** — literal EN translation.</small>

**МПК‑002** — цифровая вычислительная машина ([компьютер](obc.md)) для [КА](sc.md).  
Разработчик [НПП Антарес](npp_antares.md). Разработано в 1983 году прошёл ЛИ в КА 「Фотон‑М」 №1,2,3.

|**Characteristics**|**MPK-002**|
|:--|:--|
|Composition|1 unit|
|Consumption, W|14|
|Dimensions, ㎜|174 × 185 × 191|
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… / 50 000 (5.7)|
|Mass, ㎏|3.85|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)|9|
|[Voltage](sps.md), V|27 (23 ‑ 32)|
|**【Specific】**|~ ~ ~ ~ ~ |
|Bit depth| |
|Commands,<br> sensors,<br> inputs|… — команд управления;<br> … — релейных матричных команд управления;<br> … — ТМ‑датчиков;<br> … — входов прерываний от контактных датчиков;<br> … — входов прерываний от импульсных датчиков|
|CPU type|AMD 5x86 с рабочей частотой 100 ㎒|
|Memory, ㎅|2 048 — ОЗУ;<br> 16 896 — ЭППЗУ;<br> … — защищённой памяти|
|Performance|10 млн операций в секунду|
|Recover time, s|20 (с момента фиксирования отказа автоматикой)|
|Software| |
| |[![](f/cpu/m/mpk-002_pic1t.webp)](f/cpu/m/mpk-002_pic1.webp)|

**Notes:**

1. <http://npp-antares.ru/index.php/mpk.html>
1. **Applicability:** Фотон‑М №1,2,3



### MPK-003   ［RU］
> <small>**МПК-003** — RU term w/o analogues in English. **MPK-003** — literal EN translation.</small>

**МПК‑003** — цифровая вычислительная машина ([компьютер](obc.md)) для [КА](sc.md).  
Разработчик [НПП Антарес](npp_antares.md). Разработано в 2005 году 

|**Characteristics**|**ДКШГ.468332.003**|**(ДКШГ.468332.003-01**|
|:--|:--|:--|
|Composition|1 unit|1 unit|
|Consumption, W|52|26|
|Dimensions, ㎜|454 × 268 × 172|454 × 268 × 104|
|[Interfaces](interface.md)|[МКО](mil_std_1553.md)|[МКО](mil_std_1553.md)|
|[Lifetime](lifetime.md), h(y)|43 800 (5) / 50 000 (5.7)|43 800 (5) / 50 000 (5.7)|
|Mass, ㎏|12|8.9|
|[Overload](vibration.md), Grms| | |
|[Radiation](ion_rad.md), ㏉(㎭)| | |
|[Reliability](qm.md)|0.995|0.995|
|[Thermal](tcs.md), ℃| | |
|[TRL](trl.md)|9|9|
|[Voltage](sps.md), V|27 (23 ‑ 32)|27 (23 ‑ 32)|
|**【Specific】**|~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |
|Bit depth| | |
|Commands,<br> sensors,<br> inputs|… — команд управления;<br> … — релейных матричных команд управления;<br> … — ТМ‑датчиков;<br> … — входов прерываний от контактных датчиков;<br> … — входов прерываний от импульсных датчиков| |
|CPU type| | |
|Memory, ㎅|… — ОЗУ;<br> … — ЭППЗУ;<br> … — защищённой памяти| |
|Performance| | |
|Recover time, s|20 (с момента фиксирования отказа автоматикой)|20 (с момента фиксирования отказа автоматикой)|
|Software| |
| |[![](f/cpu/m/mpk-003_pic1t.webp)](f/cpu/m/mpk-003_pic1.webp)<br> ДКШГ.468332.003|[![](f/cpu/m/mpk-003_pic2t.webp)](f/cpu/m/mpk-003_pic2.webp)<br> ДКШГ.468332.003-01|

**Notes:**

1. <http://npp-antares.ru/index.php/equipment.html>
1. **Applicability:** 「Персона」 (индекс ГУКОС — 14Ф137)



## Archive

### Template   ［EU］

**…** — [on-board computer](obc.md). Designed by … in ….

|**Characteristics**|**(…)**|
|:--|:--|
|Composition| |
|Consumption, W| |
|Dimensions, ㎜| |
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)| |
|Mass, ㎏| |
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃| |
|[TRL](trl.md)| |
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|Bit depth| |
|CPU type| |
|Memory| |
|Performance| |
|Recover time, s| |
|Software| |
| |[![](f/cpu/_pic1t.webp)](f/cpu/_pic1.webp)|



### CVM22   ［RU］
**ЦВМ22** — цифровая вычислительная машина ([компьютер](obc.md)) для [КА](sc.md).  
Разработчик АНО "НТИЦ "ТЕХКОМ". Разработано в рамках ОКР [Фобос‑Грунт](фобос_грунт.md) в 2009 году.

|**Characteristics**|**ЦВМ22**|
|:--|:--|
|Composition|1 unit|
|Consumption, W|7.1 ‑ 12|
|Dimensions, ㎜|122 × 121 × 121|
|[Interfaces](interface.md)|1 [МКО](mil_std_1553.md); 2 [RS-232](rs_xxx.md)|
|[Lifetime](lifetime.md), h(y)|23 880 (2.72) / 41 610 (4.75)|
|Mass, ㎏|2.1|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)|100 (10 000)|
|[Reliability](qm.md)|0.999|
|[Thermal](tcs.md), ℃|–20 ‑ +50|
|[TRL](trl.md)|9|
|[Voltage](sps.md), V|27 (23 ‑ 34)|
|**【Specific】**|~ ~ ~ ~ ~ |
|Bit depth|32 бит|
|Commands,<br> sensors,<br> inputs|… — команд управления;<br> … — релейных матричных команд управления;<br> … — ТМ‑датчиков;<br> … — входов прерываний от контактных датчиков;<br> … — входов прерываний от импульсных датчиков|
|CPU type|1890ВМ2Т (KOMDIV-32), аналог MIPS R3000|
|Memory, ㎅|2 048 — ОЗУ;<br> 4 096 — ЭППЗУ;<br> … — защищённой памяти|
|Performance|84 ㎒|
|Recover time, s| |
|Software| |

**Notes:**

1. <https://en.wikipedia.org/wiki/KOMDIV-32>
1. **Опыт использования:** Согласно заключению аварийной комиссии, расследовавшей обстоятельства выхода из строя КА [Фобос‑Грунт](фобос_грунт.md), основной версией являлся выход из строя ЦВМ22 из‑за ТЗЧ. Было отмечено, что принципы построения печатных плат, а также выбранная ЭКБ не обеспечивали необходимую рад.стойкость.
1. **Applicability:** [Фобос‑Грунт](фобос_грунт.md)



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[On-board computer (OBC)](obc.md)】**<br> … <br>~ ~ ~ ~ ~<br> **RU:** [OS](os.md) ~~ [МПК-003](obc_lst.md) (9) ~~ [БИВК-МР](obc_lst.md) (8) ~~ [МАРС 4](obc_lst.md) (8) ~~ [БИВК-Р](obc_lst.md) (7.1) ~~ [МАРС 7](obc_lst.md) (6) ~~ [МПК-002](obc_lst.md) (3.9) ~~ [ЦВМ-12](obc_lst.md) (2.2) ~~ [БКУ_SXPA](obc_lst.md) (0.35) ~~ [БИВК-МН](бивк‑мн.md) () *([ЦВМ22](obc_lst.md) (2.1)**|

1. Docs: …
1. …


## The End

end of file
