# Аналог
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [Doc](doc.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Ана́лог** — русскоязычный термин. **Analogue** — англоязычный эквивалент.</small>

**Ана́лог** *(от др.-греч. ἀνάλογος — соответственный, соразмерный)* — объект (техническое решение) того же назначения, близкий по совокупности существенных признаков. Часто используется совместно с понятием 「[прототип](prototype.md)」.



## Описание



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[](.md)】**<br> <mark>NOCAT</mark>|

1. Docs: …
1. <…>


## The End

end of file
