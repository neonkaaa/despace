# Протокол разрешения применения
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [Doc](doc.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Протокол разрешения применения (ПРП)** — русскоязычный термин, не имеющий аналога в английском языке. **Employment allowance protocol (EAPR)** — дословный перевод с русского на английский.</small>

**Протокол разрешения применения (ПРП)** — [документ](doc.md), разрешающий применение покупных [изделий](unit.md).

Форма и содержание регламентируются [ГОСТ 2.124](гост_2_124.md).



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[](.md)】**<br> <mark>NOCAT</mark>|

1. Docs: …
1. <…>


## The End

end of file
