# Несущий сигнал
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → **[Радиосвязь](comms.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Несущий сигнал** — русскоязычный термин. **Carrier wave /  Carrier signal / Carrier** — англоязычный эквивалент.</small>

**Несу́щий сигна́л** — сигнал, один или несколько параметров которого изменяются в процессе модуляции. Количественное изменение параметра (параметров) определяется мгновенным текущим значением информационного (модулирующего) сигнала.

The purpose of the carrier is usually either to transmit the information through space as an electromagnetic wave (as in radio communication), or to allow several carriers at different frequencies to share a common physical transmission medium by frequency division multiplexing (as, for example, a cable television system).

В качестве несущего может быть использован любой стационарный сигнал. Чаще всего в качестве несущего сигнала используется высокочастотное (относительно высшей частоты в спектре информационного сигнала) гармоническое колебание, что обусловлено простотой демодуляции и относительно узким спектром результирующего модулированного сигнала. Однако, в некоторых случаях используют другие формы несущего сигнала, например, последовательность прямоугольных импульсов.

**Другие названия**  
Несущий сигнал часто называют просто несу́щая (от несущая частота), либо несущее (колебание). Все эти термины означают практически одно и то же. В английской терминологии несущий сигнал называют словом carrier.

|<small>На примере частотной модуляции:<br> carrier — несущая,<br> signal — модулирующий сигнал,<br> output — собственно результат — частотная модуляция.|<small>The frequency spectrum of a typical radio signal from an AM or FM radio transmitter. It consists of a strong signal (C) at the carrier wave frequency fC, with the modulation contained in narrow frequency bands called sidebands (SB) just above and below the carrier.</small>|
|:--|:--|
|![](f/comms/frequency_modulation.webp)|![](f/comms/modulated_radio_signal_frequency_spectrum.webp)|



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Communications](comms.md)】**<br> [CCSDS](ccsds.md) ~~ [Антенна](antenna.md) ~~ [АФУ](afdev.md) ~~ [Битрейт](bitrate.md) ~~ [ВОЛП](ofts.md) ~~ [ДНА](дна.md) ~~ [Диапазоны частот](comms.md) ~~ [Зрение](view.md) ~~ [Интерферометр](interferometer.md) ~~ [Информация](info.md) ~~ [КНД](directivity.md) ~~ [Код Рида‑Соломона](rsco.md) ~~ [КПДА](antenna.md) ~~ [КСВ](swr.md) ~~ [КУ](ку.md) ~~ [ЛКС, АОЛС, FSO](fso.md) ~~ [Несущий сигнал](carrwave.md) ~~ [ПНА, ПОНА, ПСНА](devd.md) ~~ [Помехи](emi.md) (EMI, RFI) ~~ [Последняя миля](last_mile.md) ~~ [Регламент радиосвязи](comms.md) ~~ [СИТ](etedp.md) ~~ [Фидер](feeder.md) <br>~ ~ ~ ~ ~<br> **РФ:** [БА КИС](ба_кис.md) (21) ~~ [БРК](brk_lav.md) (12) ~~ [РУ ПНИ](ру_пни.md) () ~~ [HSXBDT](comms_lst.md) (1.8) ~~ [CSXBT](comms_lst.md) (0.38) ~~ [ПРИЗЫВ-3](comms_lst.md) (0.17) *([ПРИЗЫВ-1](comms_lst.md) (0.075))**|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Carrier_wave>


## The End

end of file
