# SpaceIL
> 2022.04.15 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/s/spaceil_logo1t.webp)](f/c/s/spaceil_logo1.webp)|<contacts@spaceil.com>, <mark>nophone</mark>, Fax …;<br> *…, Tel Aviv, Tel Aviv, Israel*<br> <https://en.wikipedia.org/wiki/SpaceIL> ~~ [FB ⎆](https://www.facebook.com/SpaceIL) ~~ [IG ⎆](https://www.instagram.com/spaceil) ~~ [LI ⎆](https://www.linkedin.com/company/spaceil) ~~ [X ⎆](https://twitter.com/TeamSpaceIL) ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/SpaceIL)|
|:--|:--|
|**Business**|Promote science & science education. Moon landers|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO — Shimon Sarid<br> Deputy CEO, Founder — Kfir Damari<br> ・CFO — Oren Alkaslasy|

**SpaceIL** is a non‑profit organization that strives to promote science & science education. Founded 2011.

SpaceIL is an Israeli organization, established in 2011, that competed in the Google Lunar X Prize (GLXP) contest to land a spacecraft on the Moon.

SpaceIL successfully launched its Beresheet lander on 2019.02.22 at 01:45 UTC; it entered lunar orbit on 2019.04.04 at 14:18 UTC. On 2019.04.11, during the landing procedure, a problem occurred in the final minutes of flight. Communications were lost with the spacecraft, long enough for the braking process to fail, & the vehicle crashed on the lunar surface. The Beresheet mission had included plans to measure the Moon’s magnetic field at the landing site, & was carrying a laser retroreflector, & a 「time capsule」 containing analog & digital information, created by the Arch Mission Foundation. Beresheet was the 1st Israeli spacecraft to travel beyond Earth's orbit & was the 1st privately funded landing on the Moon. Though the spacecraft crashed, Israel became the seventh country to make lunar orbit & the fourth country, after the Soviet Union, the United States, & China to attempt a soft landing on the Moon.

Two days after the failed attempt to soft land on the Moon, SpaceIL announced plans for a second attempt, Beresheet 2.

The SpaceIL team was founded as a nonprofit organization wishing to promote scientific & technological education in Israel. Its total budget for the mission is estimated at US$ 95 million, provided by Israeli billionaire Morris Kahn & other philanthropists, as well as the Israel Space Agency (ISA).

<p style="page-break-after:always"> </p>

## The End

end of file
