# ClearSpace
> 2022.03.24 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/c/clearspace_logo1t.webp)](f/c/c/clearspace_logo1.webp)|<info@clearspace.today>, <mark>nophone</mark>, Fax …;<br> *Rue de Lausanne 64, 1020 Renens, Switzerland*<br> <https://clearspace.today> ~~ [FB ⎆](https://www.facebook.com/clearspacetoday) ~~ [IG ⎆](https://www.instagram.com/clearspace.today) ~~ [LI ⎆](https://www.linkedin.com/company/clearspace-sa) ~~ [X ⎆](https://twitter.com/clearspacetoday)|
|:--|:--|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**Business**|In‑orbit servicing and space debris removal|
|**[MGMT](mgmt.md)**|・Management Lead — Luc Piguet<br> ・Engineering Lead — Muriel Richard <br> ・Technology Lead — Tim Maclay|

**ClearSpace** is a Switzerland‑UK company aimed for R&D of in‑orbit servicing and space debris removal are vital services for the future of space exploration and operations. Founded n 2018.

**ClearSpace UK:**

- Address (London): Tallis House, 2 Tallis Street, London EC4Y 0AB, United Kingdom
- Address (Harwell): R103, Fermi Avenue, Harwell, Oxford, OX11 0QX, United Kingdom

<p style="page-break-after:always"> </p>

## The End

end of file
