# DDC
> 2021.08.06 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/d/ddc_logo1t.webp)](f/c/d/ddc_logo1.webp)|<service@ddc-web.com>, +1(800)DDC-5757, +1(631)567-5600, Fax …;<br> *105 Wilbur Place, Bohemia, NY, 11716, USA*<br> <https://www.ddc-web.com> ~~ [LI ⎆](http://www.linkedin.com/company/data-device-corporation) ~~ [X ⎆](https://twitter.com/datadevicecorp)|
|:--|:--|
|**Business**|[OBC](obc.md), [memory modules](ds.md), microelectronics, [radiation shielding](ion_rad.md)|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Data Device Corporation (DDC)** is a world leader in the design & manufacture of high‑reliability Connectivity, Power & Control solutions (Data Networking Components to Processor Based Subsystems, Space Qualified SBCs & Radiation Hardened Components; Power Distribution, Control & Conversion; Motor Control & Motion Feedback) for aerospace, defense, space, & industrial applications.

<p style="page-break-after:always"> </p>

## The End

end of file
