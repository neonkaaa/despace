# Safran
> 2022.03.28 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/s/safran_logo1t.webp)](f/c/s/safran_logo1.webp)|<mark>noemail</mark>,  +33155642424, Fax …;<br> *2, bd du Général Martial-Valin 75724 Paris Cedex 15, France*<br> <https://www.safran-group.com> ~~ [FB ⎆](https://www.facebook.com/GroupeSafran) ~~ [IG ⎆](https://instagram.com/safran_group) ~~ [LI ⎆](https://fr.linkedin.com/company/safran) ~~ [X ⎆](https://twitter.com/SAFRAN) ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/Safran)|
|:--|:--|
|**Business**|Propulsion, optics|
|**Mission**|Contribute to safer & more sustainable aviation|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Safran S.A.** is a French multinational aircraft engine, rocket engine, aerospace‑component & defense corporation. It was formed by a merger between the aircraft & rocket engine manufacturer & aerospace component manufacturer group SNECMA & the security company SAGEM in 2005.

<p style="page-break-after:always"> </p>

## The End

end of file
