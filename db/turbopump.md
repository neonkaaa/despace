# Turbopump
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [PS](ps.md), [SGM](sc.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Turbopump** — EN term. **Турбонасосный агрегат (ТНА)** — RU analogue.</small>

**Turbopump** — a unit of the system for supplying liquid propellant in a [liquid propellant engine](ps.md) in order to create increased pressure in the combustion chamber, thereby increasing the engine thrust & Isp.

There are two types of turbopumps: a centrifugal pump, where the pumping is done by throwing fluid outward at high speed, or an axial‑flow pump, where alternating rotating and static blades progressively raise the pressure of a fluid.

Axial‑flow pumps have small diameters but give relatively modest pressure increases. Although multiple compression stages are needed, axial flow pumps work well with low‑density fluids. Centrifugal pumps are far more powerful for high‑density fluids but require large diameters for low‑density fluids.



## Docs/Links
|**Sections & pages**|
|:--|
|**`Двигательная установка (ДУ):`**<br> [HTAE](htae.md) ~~ [TALOS](talos.md) ~~ [Баки топливные](fuel_tank.md) ~~ [Варп‑двигатель](ps.md) ~~ [Газовый двигатель](ps.md) ~~ [Гибридный двигатель](гбрд.md) ~~ [Двигатель Бассарда](ps.md) ~~ [ЖРД](ps.md) ~~ [ИПТ](ing.md) ~~ [Ионный двигатель](иод.md) ~~ [Как считать топливо?](si.md) ~~ [КЗУ](cinu.md) ~~ [КХГ](cgs.md) ~~ [Номинал](nominal.md) ~~ [Мятый газ](exhsteam.md) ~~ [РДТТ](ps.md) ~~ [Сильфон](сильфон.md) ~~ [СОЗ](соз.md) ~~ [СОИС](соис.md) ~~ [Солнечный парус](солнечный_парус.md) ~~ [ТНА](turbopump.md) ~~ [Топливные мембраны](топливные_мембраны.md) ~~ [Топливные мешки](топливные_мешки.md) ~~ [Топливо](ps.md) ~~ [Тяговооружённость](ttwr.md) ~~ [ТЯРД](тярд.md) ~~ [УИ](ps.md) ~~ [Фотонный двигатель](фотонный_двигатель.md) ~~ [ЭРД](ps.md) ~~ [Эффект Оберта](oberth_eff.md) ~~ [ЯРД](ps.md)|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Turbopump>


## The End

end of file
