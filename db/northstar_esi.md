# Northstar ESI
> 2021.07.08 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/n/northstar_esi_logo1t.webp)](f/c/n/northstar_esi_logo1.webp)|<mark>noemail</mark>, +1(514)595-7474, Fax …;<br> *384 Rue Saint-Jacques #300, Montreal, Quebec H2Y 1S1, Canada*<br> <https://northstar-data.com> ・[LI ⎆](https://ca.linkedin.com/company/northstar-earth-and-space-inc)|
|:--|:--|
|**Business**|Monitor space (object tracking), from space, via a sat constellation|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**NorthStar Earth & Space Inc**

<p style="page-break-after:always"> </p>

## The End

end of file
