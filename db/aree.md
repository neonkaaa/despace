# AREE
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → **[Rover](robot.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Automaton Rover for Extreme Environments (AREE)** — EN term. **Вездеход‑автоматон для экстремальных условий (AREE)** — literal RU translation.</small>

**Automaton Rover for Extreme Environments (AREE)** — проект планетохода от NASA для условий Венеры.

|**1st variant of AREE**|**2nd variant of AREE**|
|:--|:--|
|[![](f/rover/a/aree_pic01t.webp)](f/rover/a/aree_pic01.webp)|[![](f/rover/a/aree_pic02t.webp)](f/rover/a/aree_pic02.webp)|



## Characteristic
|**Characteristic**|**[Value](si.md)**|
|:--|:--|
|Communication [rate](comms.md), bit/s| |
|Mass (payload), ㎏| |
|Readiness state| |
|Speed of movement, m/s| |
|[Storage](ds.md) volume, bit| |
|**【Etc.】**|~ ~ ~ ~ ~ |
|Consumption, W| |
|Dimensions, ㎜| |
|[Ext. factors](ef.md)| |
|[Lifetime](lifetime.md), h(y)| |
|Mass, ㎏| |
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)| |
|[Thermal](tcs.md), ℃|… ‑ 462|
|[TRL](trl.md)| |
|[Voltage](sps.md), V| |



## Notes

1. 2020.02.18 NASA started a competition concerning mechanical obstacles sensor.
   1. <https://www.jpl.nasa.gov/news/news.php?:feature=7604>
   1. <https://www.herox.com/VenusRover>
1. 2017 NASA’s Jet Propulsion Laboratory began seriously studying how AREE would operate.
1. 2015 The project was 1st proposed.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Rover](robot.md)】**<br> **Mars:** … ┆ **Moon:** … ┆ **Venus:** [AREE](aree.md), [Zephyr](zephyr.md)|

1. Docs: …
1. <https://www.nasa.gov/feature/automaton-rover-for-extreme-environments-aree>
1. <https://en.wikipedia.org/wiki/Automaton_Rover_for_Extreme_Environments>
1. 2017.04.10 В НАСА одобрили очередные 「безумные」 проекты по колонизации космоса (<https://ria.ru/20170410/1491926649.html>) — [archived ❐](f/archive/20170410_1.pdf) 2019.02.15
1. 2017.08.30 Хабр: JPL NASA разрабатывает заводной вездеход для изучения Венеры (<https://habr.com/ru/post/406309>) — [archived ❐](f/archive/20170830_1.7z) 2019.02.15


## The End

end of file
