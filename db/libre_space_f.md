# LSF
> 2020.07.07 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/l/lsf_logo1t.webp)](f/c/l/lsf_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *Libre Space Foundation, PO Box 24063, 11110 Athens, Greece*<br> <https://libre.space> ~~ [FB ⎆](https://www.facebook.com/librespacefoundation) ~~ [IG ⎆](http://instagram.com/libre.space) ~~ [LI ⎆](https://www.linkedin.com/company/libre-space-foundation) ~~ [X ⎆](https://twitter.com/LibreSpace_Fnd)|
|:--|:--|
|**Business**|To promote, advance, develop libre technologies & knowledge for space|
|**Mission**|To promote, advance & develop libre (free & open source) technologies & knowledge for space. To do that we design, develop & deliver space related projects ranging from Ground Station equipment to global monitoring Networks & satellite missions.|
|**Vision**|An Open & Accessible Outer Space for all. We believe that space should be claimed the libre (open source) way.|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Libre Space Foundation (LSF)** is a non-profit foundation registered since 2014 in Greece from the creators of the SatNOGS project.

The company through its actions aims to promote innovation in the following horizontal areas of society:

- Technology
- Culture
- Education
- Physical Sciences

More particularly the objectives have to be around the following thematic axes:

- Software. For the objectives of Libre Space Foundation such actions are related to Free Software, & mainly focused in:
   - Development of software & web applications
   - Publicity & promotional activities
   - Encouragement of participation & to contribution to international Free Software projects
- Hardware. For the objectives of Libre Space Foundation such actions are related to Open Hardware, & mainly focused in:
   - Development of Open Hardware projects & necessary software.
   - Construction of Open Hardware projects.
   - Public availability of Open Architecture equipment designs in accordance to international laboratory standards.
- Content. For the objectives of Libre Space Foundation such actions are related to Open Content & Data, & mainly focused in:
   - Development of Open Content tools, as Creative Commons.
   - Collection, creation & distribution of Open Content & Data related to space technologies.
- Network. For the objectives of Libre Space Foundation such actions are related to Free & Open Content & mainly focused in:
   - Development, creation & administration of Open Architecture networks.
   - Promotion of such networks.
   - Partnership of such networks with similar technological networks.



## The End

end of file
