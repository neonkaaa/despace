# Tennis racket theorem
> 2021.02.03 [🚀](../../index/index.md) [despace](index.md) → [Control](control.md), [SE](se.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Tennis racket theorem, Dzhanibekov effect** — EN term. **Теорема теннисной ракетки, Эффект Джанибекова** — RU analogue.</small>

The **tennis racket theorem** or **intermediate axis theorem** is a result in classical mechanics describing the movement of a rigid body with three distinct principal moments of inertia. It’s also dubbed the **Dzhanibekov effect**, after Russian cosmonaut Vladimir Dzhanibekov who noticed one of the theorem's logical consequences while in space in 1985 although the effect was already known for at least 150 years before that.

The theorem describes the following effect: rotation of an object around its 1st & third principal axes is stable, while rotation around its second principal axis (or intermediate axis) is not.

This can be demonstrated with the following experiment: hold a tennis racket at its handle, with its face being horizontal, & try to throw it in the air so that it will perform a full rotation around the horizontal axis perpendicular to the handle, & try to catch the handle. In almost all cases, during that rotation the face will also have completed a half rotation, so that the other face is now up. By contrast, it’s easy to throw the racket so that it will rotate around the handle axis (the third principal axis) without accompanying half‑rotation around another axis; it’s also possible to make it rotate around the vertical axis perpendicular to the handle (the 1st principal axis) without any accompanying half‑rotation.

The experiment can be performed with any object that has three different moments of inertia, for instance with a book, remote control or smartphone. The effect occurs whenever the axis of rotation differs only slightly from the object's second principal axis; air resistance or gravity are not necessary.

[Demonstration video ❐](f/control/dzhanibekov_effect_01.mkv)



## (RU) Эффект Джанибекова
**Теоре́ма промежу́точной оси́**, или **теоре́ма те́ннисной раке́тки**, в классической механике — утверждение о неустойчивости вращения твёрдого тела относительно второй главной оси инерции. Является следствием законов классической механики, описывающих движение твёрдого тела с тремя различными главными моментами инерции. Проявление теоремы при вращении такого тела в невесомости часто называют **эффектом Джанибекова** в честь советского космонавта Владимира Джанибекова, который заметил это явление 1985.06.25 во время миссии по спасению космической станции 「Салют‑7」. Статья, объясняющая эффект, была опубликована в 1991 году. В то же время сама теорема о неустойчивости вращения вокруг промежуточной оси инерции известна давно и доказывается в любом курсе классической механики. Неустойчивость такого вращения часто показывается в лекционных экспериментах.

Теорема описывает следующий эффект: вращение объекта относительно главных осей с наибольшим и наименьшим моментами инерции является устойчивым, в то время как вращение вокруг главной оси с промежуточным моментом инерции (откуда и название теорема промежуточной оси) — нет. Джанибеков увидел это с гайкой‑барашком: скрутив её в невесомости с длинной шпильки, он заметил, что она пролетает немного, разворачивается на 180°, потом, ещё немного пролетев, опять разворачивается.

На Земле этот эффект можно увидеть на таком эксперименте: возьмите за ручку теннисную ракетку и попытайтесь подбросить её в воздух так, чтобы она выполнила полный оборот вокруг оси, проходящей в плоскости ракетки перпендикулярно рукоятке, и поймайте за ручку. Почти во всех случаях ракетка выполнит пол‑оборота вдоль продольной оси и будет 「смотреть」 на вас другой стороной. Если подбрасывать ракетку и закручивать её по другим осям, то ракетка сохранит свою ориентацию после полного оборота.

Эксперимент может быть выполнен с любым предметом, который имеет три различных момента инерции, например с книгой или пультом дистанционного управления. Эффект возникает, когда ось вращения немного отличается от второй главной оси предмета; сопротивлением воздуха или гравитацией можно пренебречь.

Называть устойчивыми вращения вокруг осей с максимальным и минимальным моментом инерции всё же неправильно, учитывая реальные физические тела. Если существуют какие‑либо силы, способные рассеивать энергию вращения, например приливные, тело со временем будет вращаться только вокруг оси с максимальным моментом инерции. Так вращаются все астероиды и планеты, включая Землю. Поэтому спекуляции о возможном повороте оси вращения Земли необоснованны.



## Docs/Links
|**Sections & pages**|
|:--|
|**【[Control](Control.md)】**<br> [Ad hoc](ad_hoc.md) ~~ [Business travel](business_travel.md) ~~ [Chief designers council](cocd.md) ~~ [CML](trl.md) ~~ [Competence](competence.md) ~~ [Confident](confident.md) ~~ [Consp.theory](consp_theory.md) ~~ [Control sys. (CS)](cs.md) ~~ [Coordinate system](coord_sys.md) ~~ [Curator](curator.md) ~~ [Designer’s supervision](des_spv.md) ~~ [E‑sig](esig.md) ~~ [Engineer](se.md) ~~ [Errand](errand.md) ~~ [Federal law](fed_law.md) ~~ [Federal TP](fed_tp.md) ~~ [Federal SP](fed_sp.md) ~~ [GNC](gnc.md) ~~ [Gravity assist](gravass.md) ~~ [Industrial archaeology](ind_arch.md) ~~ [Instruction](instruction.md) ~~ [Lean manuf.](lean_man.md) ~~ [Lifetime](lifetime.md) ~~ [Manager](manager.md) ~~ [MBSE](se.md) ~~ [Meeting](meeting.md) ~~ [MCC](sc.md) ~~ [MIC](mic.md) ~~ [MML](mml.md) ~~ [MoU](contract.md) ~~ [Nav. & ballistics (NB)](nnb.md) ~~ [Nonprofit org.](nonprof_org.md) ~~ [NX](nx.md) ~~ [Oberth effect](oberth_eff.md) ~~ [Org.structure](orgstruct.md) ~~ [Outcomes commission](outccom.md) ~~ [Patent](patent.md) ~~ [Peter prin.](peter_principle.md) ~~ [Plan](plan.md) ~~ [PMBok](pmbok.md) ~~ [Quorum](quorum.md) ~~ [R&D management](mgmt.md) ~~ [R&D support](rnd_support.md) ~~ [Recursion](recurs.md) ~~ [Schulze_method](schulze_method.md) ~~ [Sci'N'Tech activities](st_act.md) ~~ [Sci'N'Tech council](satc.md) ~~ [Single-window system](sw_sys.md) ~~ [Situ.leadership](situ_leadership.md) ~~ [Skunk works](se.md) ~~ [State arm. plan](plan_sa.md) ~~ [Swamp](swamp.md) ~~ [Teamcenter](teamcenter.md) ~~ [Tennis racket theorem](tr_theorem.md) ~~ [TRIZ](triz.md) ~~ [TRL](trl.md) ~~ [V‑model](v_model.md) ~~ [Veto](veto.md) ~~ [Workflow](workflow.md) ~~ [Workgroup](wg.md)|
|**【[Systems engineering](se.md)】**<br> [Competence](competence.md) ~~ [Coordinate system](coord_sys.md) ~~ [Designer’s supervision](des_spv.md) ~~ [Industrial archaeology](ind_arch.md) ~~ [Instruction](instruction.md) ~~ [Lean manuf.](lean_man.md) ~~ [Lifetime](lifetime.md) ~~ [MBSE](se.md) ~~ [MML](mml.md) ~~ [Nav. & ballistics (NB)](nnb.md) ~~ [NASA SEH](book_nasa_seh.md) ~~ [Oberth effect](oberth_eff.md) ~~ [PMBok](pmbok.md) ~~ [Quorum](quorum.md) ~~ [R&D management](mgmt.md) ~~ [R&D support](rnd_support.md) ~~ [Recursion](recurs.md) ~~ [Schulze_method](schulze_method.md) ~~ [Sci'N'Tech activities](st_act.md) ~~ [Sci'N'Tech council](satc.md) ~~ [Skunk works](se.md) ~~ [SysML](sysml.md) ~~ [Tennis racket theorem](tr_theorem.md) ~~ [TRIZ](triz.md) ~~ [TRL](trl.md) ~~ [V‑model](v_model.md) ~~ [Workflow](workflow.md) ~~ [Workgroup](wg.md)|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Tennis_racket_theorem>


## The End

end of file
