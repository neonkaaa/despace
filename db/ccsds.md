# Consultative Committee for Space Data Systems
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md), **[НД](doc.md)**, [Радиосвязь](comms.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Consultative Committee for Space Data Systems (CCSDS)** — англоязычный термин, не имеющий аналога в русском языке. **Международный Консультативный Комитет по космическим системам передачи данных** — дословный перевод с английского на русский.</small>

**Consultative Committee for Space Data Systems** (CCSDS, Международный Консультативный Комитет по космическим системам передачи данных) — международный комитет, образованный в 1982 году. В настоящее время объединяет 11 космических агентств, 28 агентств‑наблюдателей и более 140 индустриальных групп. Комитет занимается разработкой стандартов и рекомендаций для космических информационных систем.

Целями комитета являются: развитие возможностей взаимодействия между различными космическими агентствами и уменьшение стоимости разработок космических проектов.



## Члены комитета
На настоящее время право голоса в CCSDS имеют следующие члены:

1. Бразилия — Instituto Nacional de Pesquisas Espaciais (INPE)
1. Канада — Канадское космическое агентство (CSA)
1. Китай — Китайское национальное космическое управление (CNSA)
1. Европейский союз — Европейское космическое агентство (ESA)
1. Франция — Национальный центр космических исследований (CNES)
1. Германия — Германский центр авиации и космонавтики (DLR)
1. Италия — Итальянское космическое агентство (ASI)
1. Япония — Японское агентство аэрокосмических исследований (JAXA)
1. Россия — Федеральное космическое агентство России (RFSA)
1. Великобритания — Космическое агентство Великобритании (UKSA)
1. США — Национальное управление по аэронавтике и исследованию космического пространства (NASA)



## Типы рекомендаций
Комитет CCSDS разрабатывает рекомендации по стандартизации, которые называются Blue Books, для:

1. уменьшения стоимости выполнения космических программ
1. возможности взаимодействия при поддержке программ
1. улучшение понимания данных, связанных с космосом
1. сохранение и архивация собранных данных

Типы отчетов комитета имеют цветовую кодировку:

1. Синий: Рекомендуемые стандарты
1. Пурпурный: Рекомендуемые практические применения
1. Зелёный: Информационные отчёты
1. Оранжевый: Экспериментальные спецификации
1. Жёлтый: Протоколы, административные документы
1. Серый (серебряный): Устаревшие документы

Для каналов связи Земля‑КА и КА‑Земля CCSDS рекомендует:

1. избегать использовать типы модуляции, не устойчивые к доплеровскому сдвигу, например, FM;
1. избегать коды коррекции ошибок с производительностью ниже кода‑А (7, 1/2) программы Вояджер;
1. использовать сжатие изображений на основе вейвлет‑преобразования (ICER, JPEG2000 и пр.).

Для каналов связи КА‑КА CCSDS рекомендует:

1. скорость передачи в [УКВ‑диапазоне](comms.md) не более 4 Мбит/с.



## Благородная миссия CCSDS

Возможно, у кого‑то возник вопрос: зачем всем придерживаться стандартов, если можно разработать свой проприентарный стек протоколов радиосвязи (или свой стандарт, с блэк‑джеком и новыми фичами)?: Как показывает практика, выгоднее придерживаться стандартам CCSDS по следующим причинам:

1. В комитет, отвечающий за публикацию стандартов, вошли представители всех крупных аэрокосмических агентств мира, привнеся свой бесценный опыт, полученный на протяжении многих лет проектирования и эксплуатации различных миссий. Было бы очень нелепо игнорировать данный опыт и снова наступать на их грабли.
1. Данные стандарты поддерживаются уже имеющимся на рынке оборудованием наземных станций.
1. В ходе устранения каких‑либо неполадок всегда можно обратиться за помощью к коллегам из других агентств, чтобы они провели сеанс связи с аппаратом со своей наземной станции. Как видите, стандарты – вещь крайне полезная, поэтому давайте разберёмся в их ключевых моментах.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Communications](comms.md)】**<br> [CCSDS](ccsds.md) ~~ [Антенна](antenna.md) ~~ [АФУ](afdev.md) ~~ [Битрейт](bitrate.md) ~~ [ВОЛП](ofts.md) ~~ [ДНА](дна.md) ~~ [Диапазоны частот](comms.md) ~~ [Зрение](view.md) ~~ [Интерферометр](interferometer.md) ~~ [Информация](info.md) ~~ [КНД](directivity.md) ~~ [Код Рида‑Соломона](rsco.md) ~~ [КПДА](antenna.md) ~~ [КСВ](swr.md) ~~ [КУ](ку.md) ~~ [ЛКС, АОЛС, FSO](fso.md) ~~ [Несущий сигнал](carrwave.md) ~~ [ПНА, ПОНА, ПСНА](devd.md) ~~ [Помехи](emi.md) (EMI, RFI) ~~ [Последняя миля](last_mile.md) ~~ [Регламент радиосвязи](comms.md) ~~ [СИТ](etedp.md) ~~ [Фидер](feeder.md) <br>~ ~ ~ ~ ~<br> **РФ:** [БА КИС](ба_кис.md) (21) ~~ [БРК](brk_lav.md) (12) ~~ [РУ ПНИ](ру_пни.md) () ~~ [HSXBDT](comms_lst.md) (1.8) ~~ [CSXBT](comms_lst.md) (0.38) ~~ [ПРИЗЫВ-3](comms_lst.md) (0.17) *([ПРИЗЫВ-1](comms_lst.md) (0.075))**|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Consultative_Committee_for_Space_Data_Systems>
1. <http://public.ccsds.org/default.aspx> — CCSDS Official Web Site
1. <http://public.ccsds.org/publications/default.aspx> — CCSDS Documents in PDF format
1. <http://public.ccsds.org/about/default.aspx> — an Introduction to CCSDS
1. <http://nssdc.gsfc.nasa.gov> — National Space Science Data Center (NSSDC) at the NASA Goddard Space Flight Center
1. 2019.07.06 [Хабр: Немного о стандартах космической связи](https://habr.com/ru/post/458884)



## The End

end of file
