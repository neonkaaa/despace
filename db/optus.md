# Optus Satellite
> 2021.11.18 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/o/optus_logo1t.webp)](f/c/o/optus_logo1.webp)|<satellite@optus.com.au>, +61(2)8087-31-40, Fax …;<br> *Ground Floor, 309 Angas St, Adelaide, 5000, South Australia*<br> <https://www.optus.com.au/enterprise/networking/satellite> ~~ [LI ⎆](https://www.linkedin.com/showcase/3804785) ~~ [X ⎆](https://twitter.com/optusenterprise)|
|:--|:--|
|**Business**|Sat & data solutions|
|**Mission**|…|
|**Vision**|…|
|**Values**|Mobile, telephony, internet, sat, entertainment & biz network services|
|**[MGMT](mgmt.md)**|…|

**Optus Satellite** has been providing premium satellite services for over 30 years. We are the only network provider in Australia to own & operate our own fleet of satellites, & have the largest fleet of satellites in Australia & New Zealand.

With a range of satellite solutions, we can provide you with voice & data connectivity, broadcast services & communication access for international businesses. Our solutions can meet all your domestic & regional satellite communication needs, ranging from handheld satellite phones through to business grade satellite broadband services.

By incorporating optical fibre, wireless & satellite technologies into a single network, Optus can provide 100% coverage across Australia & New Zealand. Optus is the only carrier in Australia to own & operate network infrastructure across these three platforms, giving us a unique network to help keep you in touch.

Since 1985, Optus has successfully launched ten satellites & operated thirteen spacecraft. The Optus Satellite fleet currently consists of five geostationary satellites providing satellite services across Australia & New Zealand, & to McMurdo Sound in the Antarctic. The Optus 10 satellite is the most recent satellite to launch, built by Space Systems/Loral & launched by Arianespace in French Guiana in September 2014.

In addition to domestic Teleports Services, Optus Satellite owns & operates two International Teleports with antennas pointed at third party satellites in the Pacific & Indian Ocean regions. This means that we have extended the coverage of services to two thirds of the Globe! As well as this, Optus Satellite also provides launch support & transfer orbit operations & Telemetry, Tracking & Command (TT&C) services to other satellite operators.

Optus provides a number of major satellite services incl.:

- Voice & data services
- Free to air television
- Pay television
- Radio broadcast
- Consumer Broadband IP
- Video conferencing
- Mobile satellite to all of Australia & New Zealand

Our satellite customers use their services in remote areas of Australia, NZ & surrounding islands. These services are supported from Optus’ major earth stations at Belrose (New South Wales), Hume (Canberra/ACT) & Lockridge (Western Australia). The Optus D1, D2 & Optus 10 satellites have even been configured to provide services to customers in Antarctica.

<p style="page-break-after:always"> </p>

## The End

end of file
