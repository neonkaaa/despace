# NeoGeography Toolkit
> 2019.09.13 [🚀](../../index/index.md) [despace](index.md) → **[Soft](soft.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**NeoGeography Toolkit (NGT)** — англоязычный термин, не имеющий аналога в русском языке. **НеоГеографический набор инструментов** — дословный перевод с английского на русский.</small>

The **Neo-Geography Toolkit (NGT)** is a collection of tools for automated processing of geospatial data, including images & maps. It is capable of processing raw raster data from remote sensing instruments & transforming it into useful cartographic products, such as visible image base maps & topographic models. Components of the NGT can perform data processing on extremely large geospatial data sets (up to several tens of terabytes) via parallel processing pipelines.

[Open-source](soft.md). [ITAR](itar.md)‑free.



## Описание
NGT is an evolving collection of loosely connected open-source modules designed by the NASA Ames Intelligent Robotics Group. The primary module is the NASA [Ames Stereo Pipeline](ames_stereo_pipeline.md).



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Software](soft.md)】**<br> [ASP](asp.md) ~~ [Blender](blender.md) ~~ [C](plang.md) ~~ [Cosmographia](cosmographia.md) ~~ [DOORS](doors.md) ~~ [DWG](cad_f.md) ~~ [GIMP](gimp.md) ~~ [Git](git.md) ~~ [IGES](cad_f.md) ~~ [ISIS](isis.md) ~~ [JT](cad_f.md) ~~ [NGT](neogeography_toolkit.md) ~~ [NX](nx.md) ~~ [Octave](gnu_octave.md) ~~ [OS](os.md) ~~ [PDF](pdf.md) ~~ [Python](plang.md) ~~ [R](plang.md) ~~ [SPICE](spice.md) ~~ [STEP](cad_f.md) ~~ [STL](stk.md) ~~ [SVG](cad_f.md) ~~ [Syncthing](syncthing.md) ~~ [SysML](sysml.md) ~~ [Teamcenter](teamcenter.md) ~~ [Valispace](valispace.md) ~~ [Система управления версиями](vcs.md) ~~ [ХРИП](adra.md)|

1. Docs: …
1. <https://ti.arc.nasa.gov/tech/asr/groups/intelligent-robotics/ngt>


## The End

end of file
