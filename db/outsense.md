# Outsense
> 2020.07.20 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/o/outsense_logo1t.webp)](f/c/o/outsense_logo1.webp)|<info@outsense.jp>, <mark>nophone</mark>, Fax …;<br> *Tokyo, Japan*<br> <https://www.outsense.jp> ~~ [JAXA ⎆](https://aerospacebiz.jaxa.jp/en/spacecompany/outsense)|
|:--|:--|
|**Business**|Space facilities & bases, space residence|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO — Yozan Takahashi<br> ・COO — Shuga Horii<br> ・CTO — Shinaro Ishimatu|

**OUTSENSE Inc.** was established in 2018 by Yozan Takahashi who are interested in space architecture, & studied 3‑Dimentional Expanded Structure (special ORIGAMI‑structure for space architecture) at Tokai Univ. in Japan. Now it’s in R&D phase. The future plan is providing facilities for space‑activity on the moon without human by 2025, & constructing moon base by 2030. Service Orverview. Using ORIGAMI technology for:

- Providing residence on the Moon
- Constructing facility on the Moon & leasing it
- Desinging (space) architecture
- Designing/developing Products

OUTSENSE has some patents related with ORIGAMI‑structure. At the current situation, there is no obvious demand for space facilities incl. residence, so he is going to make ecosystem between ordinary companies for the time to come. By co‑developing products based on the ORIGAMI technology, three benefits will be there. 1st OUTSENSE & ordinary companies will get profit, 2nd technology will be improved & invented, 3rd ground of network for realizing space residence will be created. When space facilities are required, we will invest space development with venture capitals.



## The End

end of file
