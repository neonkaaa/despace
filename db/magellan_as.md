# Magellan Aerospace
> 2019.08.14 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/m/magellan_as_logo1t.webp)](f/c/m/magellan_as_logo1.webp)|<magellan.corporate@magellan.aero>, +1(905)677-18-89, Fax +1(905)677-56-58;<br> *3160 Derry Road East Mississauga, ON L4T 1A9, Canada*<br> <http://magellan.aero>・ <http://magellan.aero/product/space> ~~ [LI ⎆](https://www.linkedin.com/company/magellan-aerospace) ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/Magellan_Aerospace)|
|:--|:--|
|**Business**|[Satellite's](sc.md) R&D, small sat bus platform, ground facilities|
|**Mission**|By setting the standard in quality, delivery, innovation and value.|
|**Vision**|To be the supplier of choice to the global aerospace industry.|
|**Values**|**Integrity** — deliver our commitments. **Respect** — everyone is important. **Innovation** — finding ways to do things better. **Ethics** — act ethically in all we do. **Collaboration** — work together to succeed.|
|**[MGMT](mgmt.md)**|…|

**Magellan Aerospace Corporation** is a Canadian manufacturer of aerospace systems & components. Magellan also repairs & overhauls, tests, & provides aftermarket support services for engines, & engine structural components. The company’s business units are divided into the product areas of aeroengines, aerostructures, rockets & space, & specialty products. Its corporate offices in Mississauga, Ontario, Magellan operates in facilities throughout CA, US, UK.

We develop & produce complex, integrated products that bring value to our customers. We serve the civil aerospace & defence markets as well as industrial power applications of aerospace engine technology.

Magellan is a component supplier for the Airbus A380, the Boeing 787 Dreamliner, the F‑35 Joint Strike Fighter, & Bombardier’s complete line of business & commuter aircraft. Magellan also supplies gas turbine components for airplanes, helicopters, & military vehicles such as the M‑1 Abrams tank. Magellan has more than 50 years of experience providing customers with solutions for space missions that spans sounding rocket & payload, space shuttle payload, International Space Station payload, & satellite missions.

- **[Satellites](sc.md).** Our engineering team performs a full range of mission activities:
   - Concept designs to detailed designs
   - Feasibility studies
   - Launch & operations support
   - Manufacturing, Assembly, Integration, & Test
- **Capabilities**
   - Aircraft refurbishment
   - Bonded engine & structure components
   - Composite structures & assemblies
   - Engine & component repair & overhaul
   - Engine core manufacturing
   - Engine development testing services
   - Engine test facility maintenance support
   - Engineering services (Detail Stress/Design of Aerostructures using CATIA/PATRAN/NASTRAN Classical Hand Calcs)
   - Exhaust systems & flow paths
   - Industrial power packages — OT3 engines
   - Machined components
   - Materials & engine testing
   - Meteological rocket systems
   - Rocket weapons systems
   - Rocketry fuel production
   - Sand casting
   - Space & satellite subsystems & components
   - Suborbital rocket systems
   - Wire strike protection for helicopters



## The End

end of file
