# Ситуационное лидерство
> 2019.07.26 [🚀](../../index/index.md) [despace](index.md) → [Control](control.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Ситуационное лидерство** — русскоязычный термин. **Situational leadership** — англоязычный эквивалент.</small>

**Ситуационное лидерство (ситуационное руководство)** — это стиль управления людьми, предполагающий использование одного из четырех стилей управления в зависимости от ситуации и уровня развития сотрудников по отношению к задаче.



## Описание
Существуют 4 стиля лидерства и 4 степени развития подчиненного. Модель ситуационного лидерства имеет ярко выраженную практическую направленность, а потому популярна среди [менеджеров](manager.md).

**Стили лидерства:**

1. **С1** — *Директивный стиль, или Лидерство путём приказа* — высокая ориентация на задачу и низкая — на людей. Лидер дает конкретные указания и следит за выполнением заданий. Основной способ лидерства — жесткая постановка целей и приказы.
1. **С2** — *Наставнический стиль, или Лидерство путём продажи идей* — высокая ориентация на задачу и людей. Лидер даёт указания и следит за выполнением, но при этом объясняет принятые решения подчинённому, предлагает высказывать идеи и предложения.
1. **С3** — *Поддерживающий стиль, или Лидерство путём участия в организации процесса работы* — высокая ориентация на людей и низкая — на задачу. Лидер поддерживает и помогает подчинённым в работе, участвует в принятии решений, но решения принимаются в основном подчинёнными.
1. **С4** — *Делегирующий стиль, или Лидерство путём делегирования* — низкая ориентация и на людей, и на задачу. Лидер передаёт полномочия, права и ответственность другим членам команды.

**Таблица.** Типы сотрудников и применяемый стиль управления.

|**Тип сотрудника**|**Стиль управления**|
|:--|:--|
|**Р1** — 「Не способен, но настроен」. Сотрудник высоко мотивирован, демонстрирует много энтузиазма, но владеет только базовыми знаниями и навыками. Например, выпускник ВУЗа. Т.е., 「новичок‑энтузиаст」.|Сотрудник нуждается в чёткой постановке задачи, обучении, разъяснении, инструкциях и контроле со стороны руководителя — используется директивный стиль (С1).|
|**Р2** — 「Не способен и не настроен」. У сотрудника есть определённые знания и навыки, однако он по какой‑то причине демотивирован. Частая ситуация, что это произошло из‑за того, что его ожидания от работы не нашли оправдания или его потребности не встретили отклика со стороны лидера.|Сотрудник нуждается и в директивах руководителя, и в его поддержке, управление с помощью приказов и указаний, жёсткий и пошаговый контроль — используется наставнический стиль (С2)|
|**Р3** — 「Способен, но не настроен」. Сотрудник  имеет знания и хорошо развитые навыки для выполнения задачи, однако его уверенность в себе и своих силах неустойчива, что может влиять на мотивацию.|Лидер мотивирует его, используя поддерживающий стиль (С3).|
|**Р4** — 「Способен и настроен」. Сотрудник демонстрирует мастерское владение необходимыми навыками, мотивирован и уверен в себе.|Сотрудник  не требует особого внимания со стороны руководителя — эффективен делегирующий стиль (С4).|



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Control](Control.md)】**<br> [Ad hoc](ad_hoc.md) ~~ [Business travel](business_travel.md) ~~ [Chief designers council](cocd.md) ~~ [CML](trl.md) ~~ [Competence](competence.md) ~~ [Confident](confident.md) ~~ [Consp.theory](consp_theory.md) ~~ [Control sys. (CS)](cs.md) ~~ [Coordinate system](coord_sys.md) ~~ [Curator](curator.md) ~~ [Designer’s supervision](des_spv.md) ~~ [E‑sig](esig.md) ~~ [Engineer](se.md) ~~ [Errand](errand.md) ~~ [Federal law](fed_law.md) ~~ [Federal TP](fed_tp.md) ~~ [Federal SP](fed_sp.md) ~~ [GNC](gnc.md) ~~ [Gravity assist](gravass.md) ~~ [Industrial archaeology](ind_arch.md) ~~ [Instruction](instruction.md) ~~ [Lean manuf.](lean_man.md) ~~ [Lifetime](lifetime.md) ~~ [Manager](manager.md) ~~ [MBSE](se.md) ~~ [Meeting](meeting.md) ~~ [MCC](sc.md) ~~ [MIC](mic.md) ~~ [MML](mml.md) ~~ [MoU](contract.md) ~~ [Nav. & ballistics (NB)](nnb.md) ~~ [Nonprofit org.](nonprof_org.md) ~~ [NX](nx.md) ~~ [Oberth effect](oberth_eff.md) ~~ [Org.structure](orgstruct.md) ~~ [Outcomes commission](outccom.md) ~~ [Patent](patent.md) ~~ [Peter prin.](peter_principle.md) ~~ [Plan](plan.md) ~~ [PMBok](pmbok.md) ~~ [Quorum](quorum.md) ~~ [R&D management](mgmt.md) ~~ [R&D support](rnd_support.md) ~~ [Recursion](recurs.md) ~~ [Schulze_method](schulze_method.md) ~~ [Sci'N'Tech activities](st_act.md) ~~ [Sci'N'Tech council](satc.md) ~~ [Single-window system](sw_sys.md) ~~ [Situ.leadership](situ_leadership.md) ~~ [Skunk works](se.md) ~~ [State arm. plan](plan_sa.md) ~~ [Swamp](swamp.md) ~~ [Teamcenter](teamcenter.md) ~~ [Tennis racket theorem](tr_theorem.md) ~~ [TRIZ](triz.md) ~~ [TRL](trl.md) ~~ [V‑model](v_model.md) ~~ [Veto](veto.md) ~~ [Workflow](workflow.md) ~~ [Workgroup](wg.md)|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Situational_leadership_theory>


## The End

end of file
