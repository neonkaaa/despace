# JPL’s Innovation Foundry
> 2019.10.16 [🚀](../../index/index.md) [despace](index.md) → **[JPL](jpl.md)**, [Contact](contact.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**JPL Innovation Foundry (JPL IF)** — англоязычный термин, не имеющий аналога в русском языке. **Инновационный цех JPL** — дословный перевод с английского на русский.</small>

**JPL’s Innovation Foundry (JPL IF)** — it’s an office within [JPL](jpl.md) that coordinates & provides leadership for all JPL activities associated with the development & capture of business opportunities. Working in partnership with JPL’s program directorates, the Foundry facilitates & guides advanced studies, concept development, & proposal support; ensures the excellence of our technical infrastructure, tools, & personnel for innovative project formulation. The JPL IF works with many [Principal Investigators](principal_investigator.md) & partners from other space centers, universities, & industry. NASA’s [Skunk Works](se.md).

1. JPL supports the science community to ideate, mature, & propose concepts for new NASA missions.
1. Continuously 「system engineer」 requirements & solutions to develop compelling new missions.
1. The JPL Innovation Foundry is JPL’s engine for formulation of exciting, new space mission concepts.

The Foundry Provides:

1. Method
   1. Stable, reliable, clear, understood, exercised
   1. Tailored for each stage of the formulation lifecycle
1. Smart access to Subject Matter Experts (SMEs)
   1. Standout SMEs (technical & programmatic)
   1. On‑demand when (but only when) needed
1. Facilities
   1. Optimized for pace & interactions of formulation
1. Smart access to prior work
   1. Thousands of engineered concepts, hundreds of vetted proposals, tens of PI‑led missions already 「in the can」
1. Hands‑on coaching of the formulation craft



## Описание

The Foundry is involved in just about every proposal that JPL produced since its inception. Here are a select few: NuSTAR, WFIRST, SMAP, Juno, Mars Helicopter, PIXL, SHERLOC, MOXIE, ARM, MarCO, GALEX, ECOSTRESS, OCO, Aquarius, NEOWISE.

Consists of two teams:

1. **A Team** — advanced studies (Pre‑Phase A);
1. **Team X** — concept development (Pre‑Phase A & Phase A).

The JPL Innovation Foundry provides each PI with institutional support to develop their own unique, high quality proposal. This support includes team formulation, tools, templates, facilities, information systems, on‑line resources, training, & access to lessons learned & best practices. The Foundry handles proposal types ranging from small research & analysis & technology development proposals, to large mission & instrument proposals.

**I have an idea, & I want to engage with the Foundry to mature it, what do I have to do?:**  
Please contact us. We can advise on the best approach given the current maturity of your concept. If your idea fits within the framework of JPL’s Earth Science, Planetary Science, Mars Exploration, Astronomy & Astrophysics or Interplanetary Network programs, we can put you in contact with the appropriate program manager. If it doesn’t quite fit within any of those programs, we can see if matches up with other funding sources or opportunities.

**What kind of ideas do you address in the Foundry?:**  
JPL is NASA’s center for robotic exploration, so the ideas we consider tend to center around space exploration, including Space Science & Earth Science, with a focus on space missions.



## A Team
> <small>**A‑Team** — англоязычный термин, не имеющий аналога в русском языке. **Команда А** — дословный перевод с английского на русский.</small>

**The A‑Team** is a virtual team & is staffed by a small group of experts in collaborative analysis & the use of advanced methods & tools; & subject matter experts from ’doing' organizations across JPL’s Engineering & Science Directorate. **The A‑Team** focuses on early Pre‑Phase A activities (e.g., idea generation, feasibility studies, & trade space exploration).

JPL’s unique approach for maturing advanced study mission concepts, performed by the architecture‑team (A‑Team), combines innovative collaborative methods with subject matter expertise & analysis tools. Each mission concept study is uniquely staffed to match the topic & scope including subject matter experts, scientists, technologists, flight & instrument systems engineers, & program managers as needed. Advanced analysis & collaborative engineering tools are also used to obtain greater insight into each concept’s strengths.



## Team X
> <small>**Team X** — англоязычный термин, не имеющий аналога в русском языке. **Команда Икс** — дословный перевод с английского на русский.</small>

**Team X** (Команда Икс) — междисциплинарная команда, состоящая из по большей части опытных сотрудников [JPL](jpl.md), занимающаяся быстрым проектированием и прототипированием КА и их СЧ.  
См. также [презентацию, 2013 год ❐](f/c/j/jpl_2013_case_presentation.pdf).

**Team X** is a virtual cross‑functional multidisciplinary team of engineers that utilizes concurrent engineering methodologies to complete rapid design, analysis & evaluation of mission concept designs & is staffed & backed by the "doing" organizations across JPL’s Engineering & Science directorate. There are over 200 Team X members at JPL. Founded in 1995. Team X is well‑suited for all aspects of Pre‑Phase A & Phase A design activities.

This advanced design team of experienced flight‑project engineers is co‑located in the Project Design Center to  complete architecture, mission, & instrument design studies in real time. Team X does point designs for missions & instruments. We also have the A‑Team which explores concepts at earlier stages of maturity, from the initial light bulb going off to wider exploration of the trade space.’doing' organizations across JPL’s Engineering & Science Directorate.

The Project Design Center is a state‑of‑the‑art facility consisting of networked workstations, a supporting data management infrastructure, large interactive graphic displays, computer modeling & simulation tools, historical data repositories & a shared project model that the design team updates. Products include exploration architectures, systems, payloads, instruments, & technologies for Earth, lunar, asteroid, comet, planetary, astronomy & physics missions.

Team X uses concurrent engineering for rapid design & analysis of space mission concepts. These advanced design teams are composed of a diverse group of specialists working in real time, in the same place, with shared data, to yield an integrated point design. Depending on the maturity of the customer’s concept, Team X offers different capabilities & products including instrument design, technology assessments, institutional cost estimates, Red Team Reviews, & much more.

Design Study Capabilities include:

1. Architecture Conceptualization & Trades
1. Block Diagrams & Interface Analyses
1. Configuration & Integration
1. Sensitivity Analysis & Trade Space Exploration
1. Systems Integration & Design Convergence
1. Technical Resource Scenario Analyses
1. Trajectory Analysis & Visualization

For more information please contact:  
[Kelley Case](person.md), Concept Design Methods Chief, +1(818)354‑58‑70, <Kelley.Case@jpl.nasa.gov>.



**JPL’s dream team**

Team X is the Jet Propulsion Laboratory’s advanced design team for rapidly generating innovative space mission concepts. The group focuses on advanced concept designs for projects still in the germination stage, in which prospective principal investigators, or PIs, are trying to formulate their ideas into proposals.

The team uses a process called concurrent engineering to do rapid design, evaluation & analysis of early concepts. Concurrent engineering is a type of parallel design process in which experts in various spacecraft systems, like propulsion & communications, work in the same room & communicate in real time.

Traditional mission design is a serial process, explains Rebecca Wheeler, the Advanced Design Methods Manager for Team X. 「You have people in separate offices, say a thermal engineer & a power engineer, & they don’t realize how they’re affecting each other’s work,」 she says. & even when communication is good, there are lots of delays built into doing design work when those involved are separated.

With Team X, JPL has created a facility in which all the designers, tools & capabilities to do design are gathered together with a conductor leading the study. So if the thermal engineer encounters a problem, they can simply call across the room & work it out, right then & there. Study teams are outfitted with tools like databases, spreadsheets & modeling software, to help capture their ideas, & do it faster. Cost estimates for the various systems are linked together, & the customer is in the room & can find out if their requirements are driving some aspect of the project’s price tag or other issues.

A typical Team X study consists of about 20 designers, representing many disciplines from across the laboratory, along with a study lead & supporting systems personnel. Most studies are completed in just three days, with three‑hour sessions each day. Studies can consider complete missions or focus on specific aspects, like mission operations & ground data systems or instrument concepts.

The fast pace at which Team X studies are completed carries with it an added benefit: objectivity. Lead Concurrent Engineer Keith Warfield thinks that’s one of the group’s greatest strengths. 「We don’t get any advantage out of one idea over another, personally. So you get a pretty frank discussion here, with people who don’t have a stake in the proposal’s success. They’re going to tell you what’s really going on.」

**From experiment to asset**

The team began operating in 1995 as something of an experiment, but the concept quickly proved its value in an era when streamlining the mission design process became a budgetary necessity.

「In the old days, we’d set up proposal teams who would work for a year on a proposal & submit it. It could be a costly process,」 recalls Wheeler. With the advent of the space agency’s 「faster, better, cheaper」 mantra, a lot of customers were looking for innovative solutions to help bring the costs of their missions down, & quick turnaround on early design was one way to do that. 「We simply didn’t have time or money to continue with the old paradigm,」 says Wheeler. 「So it was really an outgrowth of that era, having to do a lot more & do it cheaper, which started us on this approach.」

NASA & JPL have moved on from the 「faster, better, cheaper」 philosophy, & Team X has grown beyond its original impetus to become an institutional asset to JPL. Warfield has been with Team X from the beginning. 「NASA was looking for innovative ideas & better ways of doing things,」 he says. 「The Team X approach, the concurrent design methodology, is one of those that has really stuck over the years.」

Since its inception, Team X has completed more than 900 studies, with new studies booked almost every week. Their concurrent design process is now emulated by other NASA centers & in industry — even netting the group a profile in Time Magazine in 2005.

Team X allows their clients to customize how many days their study will last, plus how many & what areas of expertise & which types of technologists they wish to involve. The group also conducts student studies in the summer, in which promising students descend on JPL to work on their own concepts with Team X mentors.

**Addressing technologies**

The team maintains a core group of subsystem experts, which it augments as needed with specialists in additional technical domains. For new & underdeveloped technologies, study chairs can draw upon their own networks of contacts which reach into industry & academia.

Team X also tries to make prospective PIs aware of potential budgetary & scheduling issues associated with developing new technologies to an appropriate readiness level for spaceflight. The actual costs of a technology development program are difficult to pin down, but Team X does provide such estimates on request. 「It’s an ad hoc process,」 says Warfield. 「It has to be. You really have to get out there & connect with the people who are developing these technologies & find out what they think it’s going to take to get it done.」

**Early interaction is key**

A big part of the Team X philosophy is that good pre‑project design is just as important to the long‑term health of a mission as staffing it with the very best people. Early advanced study brings various technology factors, like production, packaging & deployment issues, to the attention of PIs. & getting this kind of input early in the process seems to make for stronger proposals & leaner missions.

「That’s where we’ve really helped a lot of the scientists we serve,」 says Wheeler. 「We’ve had PIs from outside JPL come to Team X who have never had significant interaction with engineers this early.」 These communications make an enormous difference later on, by addressing important issues while it is still easy to change things on paper, & before a project has become too attached to a particular technology design.

「When the scientists are able to have early conversations with engineers about ‘how are we really going to do this’ kinds of problems, we find that they’re open to changes in a way that makes things much easier from the engineering standpoint,」 Wheeler says. After all, engineering amazing feats of science is nothing new at JPL, but this really is rocket science.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[](.md)】**<br> <mark>NOCAT</mark>|

1. Docs:
   1. [Презентация, 2013 год ❐](f/c/j/jpl_2013_case_presentation.pdf)
   1. [Презентация, 2017 год ❐](f/c/j/jpl_2017_presentation.pdf)
1. [Skunk Works](se.md)
1. <https://jplfoundry.jpl.nasa.gov>
1. <https://jplteamx.jpl.nasa.gov>
1. <https://scienceandtechnology.jpl.nasa.gov/jpls-dream-team>


## The End

end of file
