# Taqnia Space
> 2022.03.31 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/t/taqnia_space_logo1t.webp)](f/c/t/taqnia_space_logo1.webp)|<info@taqniaspace.com.sa>, +966 11 4888286, Fax +966 11 4934483;<br> *P.O. Box 7833, Riyadh 12334, Saudi Arabia*<br> <http://www.taqniaspace.com.sa> ~~ [FB ⎆](https://www.facebook.com/Taqnia-Space-103108717091282) ~~ [IG ⎆](https://www.instagram.com/taqnia_space) ~~ [LI ⎆](https://www.linkedin.com/company/taqnia-space) ~~ [X ⎆](https://twitter.com/Taqnia_Space)|
|:--|:--|
|**Business**|Geospatial solutions|
|**Mission**|Provide Highly Secure Satellite Communications infrastructure. Provide voice, data, & video communications, TV Broadcasting. Provide High-Value Remote Sensing Data. Commercialize KACST’s Technological Developments. Human Capital Development.|
|**Vision**|To be a leader in providing mobile / fixed SATCOM, Geospatial services, & satellites manufacturing|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Taqnia Space** is a Subsidiary of The Saudi Technology Development & Investment Company (TAQNIA) that was established by royal decree in 2011. It is owned by PIF (Public Investment Fund). TAQNIA Co. has signed a Strategic Partnership with King Abdulaziz City for Science & Technology (KACST) under which KACST would provide TAQNIA with the studies & information related to all its products, services, & registered rights along with the technical support in all aspects for these products, services & registered rights.

<p style="page-break-after:always"> </p>

## The End

end of file
