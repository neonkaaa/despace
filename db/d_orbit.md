# D-Orbit
> 2022.03.24 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/d/d_orbit_logo1t.webp)](f/c/d/d_orbit_logo1.webp)|<info@dorbit.space>, +39-02-3792-0900, Fax …;<br> *Viale Risorgimento, 57 22073 Fino Mornasco, Como, Italy*<br> <https://www.dorbit.space> ~~ [FB ⎆](https://www.facebook.com/deorbitaldevices) ~~ [IG ⎆](https://instagram.com/wearedorbit) ~~ [LI ⎆](https://www.linkedin.com/company/d-orbit) ~~ [X ⎆](https://twitter.com/D_Orbit)|
|:--|:--|
|**Business**|Moving, precise deployment and removing satellites|
|**Mission**|Provide end‑to‑end solutions to improve new and traditional space businesses by streamlining in‑space and on‑ground operations with unique, innovative, and proprietary technologies.|
|**Vision**|In‑space servicing and transportation to enable profitable business and human expansion in a sustainable space.|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO, Founder — Luca Rossettini<br> ・Founder, CCO — Renato Panesi<br> ・CTO — Lorenzo Ferrario<br> ・COO — Jonathan Firth|

**D-Orbit SpA** is an Italian company aimed to provide solutions for moving, precise deployment and removing satellites. Founded 2011.03.01.

- **D-Orbit Portugal**. Rua Antonio. Champalimaud Lote 1 sala 2.07B, 1600-514, Lisboa, Portugal
- **D-Orbit UK Ltd**. HQ Building, Thomson Avenue, Harwell Campus, Didcot, OX11 0GD, United Kingdom
- **D-Orbit Inc**. 6864 Frase Dr, Falls Church, VA 22043, USA

<p style="page-break-after:always"> </p>

## The End

end of file
