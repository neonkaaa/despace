# Tohoku Space Community
> 2022.04.18 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/t/tohoku_sc_logo1t.webp)](f/c/t/tohoku_sc_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *…*<br> <https://tsctohoku.wixsite.com/tohokuspacecommunity> ~~ [FB ⎆](https://www.facebook.com/pg/tohokuspacecommunity) ~~ [IG ⎆](https://www.instagram.com/tohoku_space_community) ~~ [X ⎆](https://twitter.com/tsctohoku)|
|:--|:--|
|**Business**|Students community|
|**Mission**|Form a space-loving community & create new encounters & learning|
|**Vision**|Excite Tohoku in space|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|


Business content:

1. **Community formation business.** A business that creates connections between space lovers in Tohoku.
1. **Innovation promotion business.** Business that creates innovation by universities & companies in Tohoku.



<p style="page-break-after:always"> </p>

## The End

end of file
