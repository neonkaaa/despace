# SpaceX
> 2019.08.05 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/s/spacex_logo1t.webp)](f/c/s/spacex_logo1.webp)|<sales@spacex.com>, +7(310)363-60-00, Fax …;<br> *1030 15th Street N.W., Suite 220E, Washington, DC 20005-1503, USA*<br> <http://www.spacex.com> ~~ [FB ⎆](https://www.facebook.com/SpaceX) ~~ [IG ⎆](https://www.instagram.com/spacex) ~~ [X ⎆](https://twitter.com/spacex) ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/SpaceX)|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Space Exploration Technologies Corporation** (**SpaceX**, произносится **「Спэйс‑Экс」**) — американская компания, производитель космической техники со штаб‑квартирой в городе Хоторн, Калифорния, США. Компания основана в 2002 году.

**Нынешняя продукция:**

1. вакуумный поезд Hyperloop;
1. космический корабль [Dragon](dragon.md);
1. ракетные двигатели [Draco](engines_lst.md), [Kestrel](engines_lst.md), [Merlin](engines_lst.md), [Raptor](engines_lst.md);
1. РН [BFR](bfr.md), [Falcon](falcon.md);
1. владеют и эксплуатируют космодром [SpaceX STLS](spaceport.md).



## The End

end of file
