# Plus Ultra
> 2022.01.24 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/p/plus_ultra_logo1t.webp)](f/c/p/plus_ultra_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *Jorge Luis Borges 17, 28806 Alcana de Henares, Spain*<br> <https://www.plus-ultra.space> ~~ [LI ⎆](https://www.linkedin.com/company/plus-ultra-space-outposts) ~~ [X ⎆](https://twitter.com/PlusUltraSpace)|
|:--|:--|
|**Business**|Off‑Earth infrastructure, Lunar satellites|
|**Mission**|…|
|**Vision**|We aim to realize humanity’s future by enabling a sustainable space economy. We want to become the backbone of the lunar economy, building new services & capabilities that empower space businesses & facilitate space development to reduce our impact on Earth.<br> We’re convinced that commercialization should drive the lunar economy. We aim to promote interoperability & coordination in the lunar industry, to lower barriers of entry for new players, enable new business models, & foster the commercial transition of the space industry.<br> We believe space development is the key to unlock a bright future. We want to make it happen.|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO — Carlos M. Entrena Utrilla<br> ・CFO, COO — Renato Montoya Sanchez<br> ・CCO — Sebastian Strohl<br> ・CTO — Brian D’Souza|

**Plus Ultra Space Outposts SL** manages & deploys off‑Earth infrastructure for the growth & sustainability of the upcoming cislunar economy. Founded 2020.

Locations — Madrid, ES (HQ) ・ Granada, ES ・ Munich, DE ・ Berlin, DE ・ Luxembourg, LU

Our 1st solution **「Harmony」** is a lunar satellite constellation designed to solve all communications & navigation needs of commercial & institutional lunar missions: high‑speed connectivity (100 Mbit/s), high‑accuracy navigation, & high availability with global coverage. All as an end‑to‑end service.

<p style="page-break-after:always"> </p>

## The End

end of file
