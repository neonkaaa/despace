# Venera 1
> 2019.12.10 [🚀](../../index/index.md) [despace](index.md) → [Venus](venus.md), **[Project](project.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Венера-1 (Венера-1VA №2; Спутник 8)** — RU term w/o analogues in English. **Venera 1 (Venera 1VA No.2; Sputnik 8)** — literal EN translation.</small>


**Venera 1 (Венера‑1)**, also known as **Venera‑1VA No.2** and occasionally in the West as **Sputnik 8** was the 1st spacecraft to Venus, as part of the Soviet Union’s Venera programme. There were obtained some characteristics of the solar wind’s plasma near the Earth. Signal with SC was lost after 7 days of flight; theoretically SC fly by Venus at 100 000 ㎞ distance.

[![](f/project/v/venera_1/pic01t.webp)](f/project/v/venera_1/pic01.webp)



|**Type**|**[Param.](si.md)**|
|:--|:--|
|**【Mission】**|~ ~ ~ ~ ~ |
|Cost|… or … ㎏ of [gold](sc_price.md)|
|Development|…|
|Duration|7 days|
|Launch|1961.02.12 00:34:36 UTC, LV Molniya 8K78|
|Operator|O㎅-1|
|Programme|Venera programme|
|Similar to|・Proposed: Mariner 1, [Mariner 2](mariner_2.md)<br> ・Current: —<br> ・Past: Luna 2|
|Target|Exploring of the Venus|
|[Type](sc.md)|Venus impactor|
|**【Spacecraft】**|~ ~ ~ ~ ~ |
|Comms|2.4 m parabolic wire‑mesh antenna; 922.8 ㎒; wavelenth 32 ㎝|
|Composition|Orbiter, lander|
|Contractor|…|
|[ID](spaceid.md)|NSSDC ID (COSPAR ID): 1961-003A (<https://nssdc.gsfc.nasa.gov/nmc/spacecraft/display.action?:id=1961-003A>), SCN: 80|
|Manufacturer|O㎅-1|
|Mass|Dry 643.5 ㎏ ([medium satellite](sc.md), [EVN‑072](venus.md))|
|Orbit / Site|Heliocentric|
|Payload|a flux‑gate magnetometer, two ion traps, micrometeorite detectors, Geiger counter tubes, a sodium iodide scintillator|
|Power|…|

Achieved targets & investigations:

1. **T** — technical; **C** — contact research; **D** — distant research; **F** — fly‑by; **H** — manned; **S** — soil sample return; **X** — technology demonstration
1. **Sections of measurement and observation:**
   1. Atmospheric/climate — **Ac** composition, **Ai** imaging, **Am** mapping, **Ap** pressure, **As** samples, **At** temperature, **Aw** wind speed/direction.
   1. General — **Gi** planet’s interactions with outer space.
   1. Soil/surface — **Sc** composition, **Si** imaging, **Sm** mapping, **Ss** samples.

<small>

|**EVN‑XXX**|**T**|**EN**|**Section of m&o**|**D**|**C**|**F**|**H**|**S**|
|:--|:--|:--|:--|:--|:--|:--|:--|:--|
|EVN‑005|T|Exploration: from Venusian orbit| |D| |F| | |
|EVN‑072|T|Exploration with [satellites](sc.md): medium satellites| |D| |F| | |
|EVN‑097| |Atmosphere: effect of solar Rad & interplanetary space on the atmo| |D| |F| | |

</small>



## Mission
Venera 1 was the second of two attempts to launch a spacecraft to Venus in February 1961, immediately following the launch of its sister ship Venera‑1VA No.1, which failed to leave Earth orbit. Soviet experts launched Venera‑1 using a Molniya carrier rocket from the [Baikonur Cosmodrome](spaceport.md). The launch took place 1961.02.12 at 00:34:36 UTC.

The spacecraft, along with the rocket’s Blok‑L upper stage, were initially placed into a 229 × 282 ㎞ low Earth orbit, before the upper stage fired to place Venera 1 into a heliocentric orbit, directed towards Venus. The 11D33 engine was the world’s 1st staged‑combustion‑cycle rocket engine, and also the 1st use of an ullage engine to allow a liquid‑fuel rocket engine to start in space.

Three successful telemetry sessions were conducted, gathering solar‑wind and cosmic‑ray data near Earth ([EVN‑097](venus.md)), at the Earth’s Magnetopause, and on February 19 at a distance of 1 900 000 ㎞. After discovering the solar wind with [Luna 2](luna_2.md), Venera 1 provided the 1st verification that this plasma was uniformly present in deep space. Seven days later, the next scheduled telemetry session failed to occur. On 1961.05.19 Venera 1 passed within 100 000 ㎞ of Venus ([EVN‑005](venus.md)). With the help of the British radio telescope at Jodrell Bank, some weak signals from Venera 1 may have been detected in June. Soviet engineers believed that Venera 1 failed due to the overheating of a solar‑direction sensor.



## Science goals & payload
The main task of Venera 1 was to enter the atmosphere of Venus landing on the surface or water.

Venera 1 — spacecraft, designed to explore the planet Venus.

1. flux‑gate magnetometer;
1. Geiger counter tubes;
1. ion traps (2 instruments);
1. micrometeorite detectors;
1. sodium iodide scintillator.

The lander was equipped with scientific instruments including a flux‑gate magnetometer attached to the antenna boom, two ion traps to measure solar wind, micrometeorite detectors, and Geiger counter tubes and a sodium iodide scintillator for measurement of cosmic radiation.

1. flux‑gate magnetometer — are used for their electronic simplicity and low weight. The majority of early fluxgate magnetometers on spacecraft were made as vector sensors. Used to search for magnetic fields;
1. two ion traps — a combination of electric or magnetic fields used to capture charged particles, often in a system isolated from an external environment;
1. micrometeorite detectors — the device’s objective was to study the relationship of micrometeorite’s density as compared with their mass and velocity;
1. Geiger counter tubes — the sensing element of the Geiger counter instrument used for the detection of ionizing radiation;
1. sodium iodide scintillator.



## Spacecraft
Venera 1 was a 643.5 ㎏ spacecraft consisting of a cylindrical body 1.05 m in diameter topped by a dome, totalling 2.035 m in height. This was pressurized to 1.2 atm with dry nitrogen, with internal fans to maintain even distribution of heat. Two solar panels extended from the cylinder, charging a bank of silver‑zinc batteries. A 2 m parabolic wire‑mesh antenna was designed to send data from Venus to Earth on a frequency of 922.8 ㎒. A 2.4 m antenna boom was used to transmit short‑wave signals during the near‑Earth phase of the mission. Semidirectional quadrupole antennas mounted on the solar panels provided routine telemetry and telecommand contact with Earth during the mission, on a circularly‑polarized decimetre radio band.

An experiment attached to one solar panel measured temperatures of experimental coatings. Infrared and/or ultraviolet radiometers may have been included. The dome contained a KDU‑414 engine used for mid‑course corrections. Temperature control was achieved by motorized thermal shutters.

During most of its flight, Venera 1 was spin stabilized. It was the 1st spacecraft designed to perform mid‑course corrections, by entering a mode of 3‑axis stabilization, fixing on the Sun and the star Canopus. Had it reached Venus, it would have entered another mode of 3‑axis stabilization, fixing on the Sun and Earth, and using for the 1st time a parabolic antenna to relay data.



## Community, library, links

**PEOPLE:**

1. **Leaders:**
   1. [Mstislav Keldysh](person.md) (Мстислав Всеволодович Келдыш) a Soviet scientist in the field of mathematics and mechanics.
   1. [Sergei Korolev](person.md) (Сергей Павлович Королёв) a lead Soviet rocket engineer and spacecraft designer during the Space Race between the United States and the Soviet Union in the 1950s and 1960s.
1. **Members:**
   1. Alla Masevich (Алла Генриховна Масевич) a Soviet astronomer.
   1. Boris Chertok (Борис Евсеевич Черток) a Russian electrical engineer and the control systems designer in the SU space program, and later the Roscosmos in Russia.
   1. Alfred Charles Bernard Lovell an English physicist and radio astronomer. Theoretically couuld receive a signal from the spacecraft’s disoriented nondirectional antenna.
   1. Yuli Khodarev (Юлий Константинович Ходарев) — developer of satellite communication systems (inc. Venera 1).

**COMMUNITY:**

1. Academy of Science of the Soviet Union.



## Docs/Links
|**Sections & pages**|
|:--|
|**【[](.md)】**<br> <mark>NOCAT</mark>|

1. Docs:
   1. П. С. Шубин — Венера. Неукротимая планета. Издание второе, расширенное и дополненное. М.: Издательство 「Голос‑Пресс」; Кемерово: издатель П. С. Шубин, 2018. – 352 стр.
1. <https://en.wikipedia.org/wiki/Venera_1>


## The End

end of file
