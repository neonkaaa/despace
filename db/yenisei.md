# Yenisei
> 2019.06.27 [🚀](../../index/index.md) [despace](index.md) → [LV](lv.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Енисей** — RU term w/o analogues in English. **Yenisei** — literal EN translation.</small>

**Yenisei** — upcoming RU super heavy-lift launch vehicle (SHLV). Cost estimate in 2019 was ₽ 1 500 000 000 000.

|**Version**|**Description**|**Activity**|
|:--|:--|:--|
|Energia‑3|Baseline.|Development (2027 ‑ …)|
|Енисей (STK-1)|Baseline.|Development (2027 ‑ …)|
|Дон (STK-2)|Baseline.|Development (2027 ‑ …)|

[![](f/lv/stk/enisey_project_2028_0t.webp)](f/lv/stk/enisey_project_2028_0.webp)



---



## Yenisei
**Yenisei** — upcoming RU super heavy-lift launch vehicle.

1. Отработочный вариант РН СТК — для облёта Луны в автоматическом режиме кораблём 「Федерация」 или лунным вариантом корабля 「Союз」.
1. РН СТК первого этапа — для запусков пилотируемого транспортного корабля 「Федерация」 и других ПН на низкие окололунные орбиты, в т.ч. полярную.
1. РН СТК второго этапа — на первой ступени будут использоваться пакет из 6 первых ступеней 「[Союза‑5](soyuz.md)」 — один центральный и 5 боковых блоков. Вторая ступень будет, в отличие от СТК первого этапа и отработочной версии, оснащена водородным двигателем РД-0150. Также в РН будет использоваться кислородно‑водородный межорбитальный буксир (или [разгонный блок](lv.md)).

|**Characteristic**|**[Value](si.md)**|
|:--|:--|
|Активность|**Активен** (2027 ‑ …)|
|[Аналоги](analogue.md)|**Актуальные:* [Falcon Heavy](falcon.md) (США) ~~ [New Glenn](new_glenn.md) (США) ~~ [SLS Block 1](sls.md) (США);<br> *Исторические:* [Saturn‑5](saturn.md) (США) ~~ [Н-1](n_1.md) (СССР) ~~ [Энергия](energia.md) (СССР)|
|Длина/диаметр| |
|[Космодромы](spaceport.md)|[Восточный](spaceport.md)|
|Масса старт./сух.|**Энергия‑3:** 1 440 / …;<br> **Енисей:** 3 167 / …;<br> **Дон:** 3 281 / …|
|Разраб./изготов.|[РКК Энергия](ркк_энергия.md) (Россия) / [РКК Энергия](ркк_энергия.md) (Россия)|
|Ступени|3|
|[Fuel](ps.md)|[O + Kerosene](o_plus.md) (1, 2 ступ.), [Кислород + Водород](o_plus.md) (2 ступ.)|
| |[![](f/lv/stk/stk1_01t.webp)](f/lv/stk/stk1_01.webp) [![](f/lv/stk/stk1_02t.webp)](f/lv/stk/stk1_02.webp) [![](f/lv/stk/stk2_01t.webp)](f/lv/stk/stk2_01.webp)|

**Выводимые массы.**

|**Космодром**|**РН**|<small>*Масса,<br> [НОО](nnb.md), т*</small>|<small>*Масса,<br> [ГСО](nnb.md), т*</small>|<small>*Масса к<br> [Луне](moon.md), т*</small>|<small>*Масса к<br> [Венере](venus.md), т*</small>|<small>*Масса к<br> [Марсу](mars.md), т*</small>|**Примечания**|
|:--|:--|:--|:--|:--|:--|:--|:--|
|[Восточный](spaceport.md)|Энергия‑3|70|…|…|…|…|Пуск — $ … млн (… г);<br> ПН 4.86 % от ст.массы|
|[Восточный](spaceport.md)|Енисей|100|26|27|…|…|Пуск — $ … млн (… г);<br> ПН 3.15 % от ст.массы|
|[Восточный](spaceport.md)|Дон|140|29.3|33|…|…|Пуск — $ … млн (… г);<br> ПН 4.26 % от ст.массы|

<small>Примечания:<br> **1)** Указана масса для наихудших условий старта.<br> **2)** В скобках указана масса для наилучших условий старта.</small>



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Launch vehicle (LV)](lv.md)】**<br> [ICBM](icbm.md) ~~ [Integrated payload unit](lv.md) ~~ [Non‑rocket spacelaunch](nrs.md) ~~ [Throw weight](throw_weight.md)<br>~ ~ ~ ~ ~<br> **China:** [Long March](long_march.md) ┊ **EU:** [Arian](arian.md), [Vega](vega.md) ┊ **India:** [GSLV](gslv.md), [PSLV](pslv.md) ┊ **Israel:** [Shavit](shavit.md) ┊ **Japan:** [Epsilon](epsilon.md), [H2](h2.md), [H3](h3.md) ┊ **Korea N.:** [Unha](unha.md) ┊ **Korea S.:** *([Naro‑1](kslv.md))* ┊ **RF,CIF:** [Angara](angara.md), [Proton](proton.md), [Soyuz](soyuz.md), [Yenisei](yenisei.md), [Zenit](zenit.md) *([Energia](energia.md), [Korona](korona.md), [N‑1](n_1.md), [R‑1](r_7.md))* ┊ **USA:** [Antares](antares.md), [Atlas](atlas.md), [BFR](bfr.md), [Delta](delta.md), [Electron](electron.md), [Falcon](falcon.md), [Firefly Alpha](firefly_alpha.md), [LauncherOne](launcherone.md), [New Armstrong](new_armstrong.md), [New Glenn](new_glenn.md), [Minotaur](minotaur.md), [Pegasus](pegasus.md), [Shuttle](shuttle.md), [SLS](sls.md), [Vulcan](vulcan.md) *([Saturn](saturn_lv.md), [Sea Dragon](sea_dragon.md))**|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Super_heavy-lift_launch_vehicle>
1. <https://en.wikipedia.org/wiki/Yenisei_(rocket)>


## The End

end of file
