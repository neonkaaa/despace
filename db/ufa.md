# UFA
> 2019.08.05 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/u/ufa_logo1t.webp)](f/c/u/ufa_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *…*<br> <http://www.ufa.cas.cz> ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/Institute_of_Atmospheric_Physics_AS_CR)|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Институт атмосферной физики** (чеш. **Ústav fyziky atmosféry Akademie věd České republiky (UFA)**, междунар. **The Institute of Atmospheric Physics AS CR (IAP)** — университет Чехии, занимается изучением атмосферы. Основан в 1964 году.

1. Прибор 「ЛЕМРА」 на ОКР [Luna‑26](luna_26.md).



## The End

end of file
