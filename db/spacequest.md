# SpaceQuest
> 2019.08.16 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/s/spacequest_logo1t.webp)](f/c/s/spacequest_logo1.webp)|<mark>noemail</mark>, +1(703)424-78-01, Fax …;<br> *3554 Chain Bridge Rd, Suite 400, Fairfax VA, 22030, US*<br> <http://www.spacequest.com>|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**SpaceQuest, Ltd.** is a leading developer of advanced satellite technology for government, university & commercial use since 1994, specializing in the design, development, testing & manufacture of spacecraft as well as space & ground components for operation with low‑Earth orbiting satellites.  
SpaceQuest Canada Inc., a wholly-owned subsidiary of SpaceQuest, Ltd, was established as a Canadian Corporation in 2001 to better serve government & private organizations throughout Canada.  
Through innovative designs & the latest electronic technology, SpaceQuest is building microsatellites & satellite components faster & more cost effectively than ever before.



## The End

end of file
