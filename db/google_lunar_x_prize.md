# Google Lunar X Prize
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [Проекты](project.md), [Events](event.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Google Lunar X Prize** — англоязычный термин, не имеющий аналога в русском языке. **Лунный Приз Икс Гугль** — дословный перевод с английского на русский.</small>

**Google Lunar X PRIZE** — премия, которую учредили Фонд X Prize и компания Google. Будет вручена команде, которая сможет создать луноход, удовлетворяющий заданным условиям. Призовой фонд составляет 30 млн долл., который состоит из Первого (Главного) приза в 20 млн долл., Второго приза в 5 млн долл. и нескольких Дополнительных премий на общую сумму 5 млн долл. По условиям конкурса, в случае, если какое‑либо государство отправит на Луну луноход, созданный на правительственные деньги, размер главного приза уменьшится до 15 млн долл.

Конкурс на получение премии был объявлен 13 сентября 2007 в Санта‑Монике (Калифорния, США). Условия конкурса (полная миссия) включает 3 основных задания:

1. Мягкая посадка на лунную поверхность
1. Передвижение (мобильность) по лунной поверхности (не менее 500 м)
1. Передача на Землю изображений и видео в высоком разрешении.



## Участники
1. [Astrobotic](astrobotic.md), США
1. [Hakuto](hakuto.md), Япония
1. [Moon Express](moon_express.md), США
1. [PTScientists](ptscientists.md), Европа, Германия
1. [Team Indus](team_indus.md), Индия
1. [SpaceIL](spaceil.md), Израиль
1. [Synergy Moon](synergy_moon.md), Международная



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Events](event.md)】**<br> **Meetings:** [AGU](agu.md) ~~ [CGMS](cgms.md) ~~ [COSPAR](cospar.md) ~~ [DPS](dps.md) ~~ [EGU](egu.md) ~~ [EPSC](epsc.md) ~~ [FHS](fhs.md) ~~ [IPDW](ipdw.md) ~~ [IVC](ivc.md) ~~ [JpGU](jpgu.md) ~~ [LPSC](lpsc.md) ~~ [MAKS](maks.md) ~~ [MSSS](msss.md) ~~ [NIAC](niac_program.md) ~~ [VEXAG](vexag.md) ~~ [WSI](wsi.md) ┊ ··•·· **Contests:** [Google Lunar X Prize](google_lunar_x_prize.md)|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Google_Lunar_X_Prize>


## The End

end of file
