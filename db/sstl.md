# SSTL
> 2022.02.22 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/s/sstl_logo1t.webp)](f/c/s/sstl_logo1.webp)|<contact@space.org.sg>, +65(673)579-95, Fax …;<br> *318 Tanglin Road (Phoenix Park), #01-39, Singapore 247979*<br> <https://www.space.org.sg> ~~ [FB ⎆](https://www.facebook.com/SingaporeSpace) ~~ [IG ⎆](https://instagram.com/sg_space) ~~ [LI ⎆](https://www.linkedin.com/company/singaporespace) ~~ [X ⎆](https://twitter.com/SingaporeSpace) ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/Singapore_Space_and_Technology_Ltd)|
|:--|:--|
|**Business**|Space promotion, business connections|
|**Mission**|To harness & advance space technologies to benefit people, enterprises & the planet|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO — Lynette Tan<br> ・Chairman — Jonathan Hung|

**Singapore Space & Technology Ltd (SSTL)** — formerly Singapore Space & Technology Association (SSTA) — is Asia Pacific’s leading pioneer space organisation, harnessing & advancing space technologies to benefit people, enterprises & the planet. Founded 2007.

SSTL connects the different players in the region’s growing space sector to government agencies in the region, B2B & B2C technology companies & non‑government organisations. The focus is to accelerate the adoption & commercialisation of space‑related innovations, & to cultivate space talent ahead of the curve.

<p style="page-break-after:always"> </p>

## The End

end of file
