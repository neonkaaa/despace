# KAIST
> 2020.07.24 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/k/kaist_logo1t.webp)](f/c/k/kaist_logo1.webp)|<kaistpr@kaist.ac.kr>, +82-42-350-2114, Fax +82-42-350-2210(2220);<br> *291 Daehak-ro, Eoeun-dong, Yuseong-gu, Daejeon, Korea*<br> <https://www.kaist.ac.kr> ~~ [FB ⎆](https://www.facebook.com/KAIST.official) ~~ [IG ⎆](https://www.instagram.com/official_kaist) ~~ [LI 1 ⎆](https://www.linkedin.com/school/한국과학기술원-kaist-) & [LI 2 ⎆](https://www.linkedin.com/company/korea-advanced-institute-of-science-and-technology) ~~ [X ⎆](https://twitter.com/kaistpr) ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/KAIST)|
|:--|:--|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**Business**|Higher education|
|**[MGMT](mgmt.md)**|…|

**KAIST** (formally the **Korea Advanced Institute of Science & Technology**) is a national research university located in Daedeok Innopolis, Daejeon, South Korea.

KAIST was established by the Korean government in 1971 as the nation’s 1st research‑oriented science & engineering institution. KAIST also has been internationally accredited in business education, & hosting the Secretariat of AAPBS. KAIST has approximately 10 200 full‑time students & 1 140 faculty researchers & had a total budget of $ 765 million in 2013, of which $ 459 million was from research contracts.

Including College of Engineering:

- School of Mechanical & Aerospace Engineering
   - Department of Mechanical Engineering
   - [Department of Aerospace Engineering ⎆](https://ae.kaist.ac.kr)



## The End

end of file
