# Система разработки и постановки продукции на производство
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [Doc](doc.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Система разработки и постановки продукции на производство (СРПП)** — русскоязычный термин, не имеющий аналога в английском языке. **System of product development and launching into manufacture (SPDLIM)** — дословный перевод с русского на английский.</small>

**Система разработки и постановки продукции на производство (СРПП)** — это система правил, определяющих порядок проектирования, производства и эффективного применения потребителем продукции.



## Описание
В ней определены организационно‑технические принципы и порядок проведения работ по созданию высокоэффективной продукции. Система устанавливает порядок проведения [научно‑исследовательских](rnd.md), [опытно‑конструкторских](rnd.md), опытно‑технических работ, патентных исследований, прогнозирования технического уровня и тенденций развития техники, способов упаковки и транспортировки [изделий](unit.md), а также порядок постановки на производство продукции, изготавливаемой по лицензиям зарубежных фирм; регламентирует порядок снятия с производства продукции, не соответствующей современному уровню. И, что особенно важно, системой устанавливаются функции разработчика, заказчика (потребителя), изготовителя продукции.

Система разработки и постановки продукции на производство, как правило, предусматривает:

1. разработку [технического задания](tor.md);
1. разработку технической и нормативно‑технической документации;
1. изготовление и испытания образцов продукции;
1. приёмку результатов разработки;
1. подготовку и освоение производства.

Отдельные из указанных этапов может совмещать, изменять и дополнять в зависимости от специфики продукции и организации производства.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**`Баллистико‑навигационное обеспечение (БНО):`**<br> [SPICE](spice.md) ~~ [Апоцентр и перицентр](apopericentre.md) ~~ [Гравманёвр](gravass.md) ~~ [Кеплеровы элементы](keplerian.md) ~~ [Космическая скорость](esc_vel.md) ~~ [Сфера Хилла](hill_sphere.md) ~~ [Терминатор](terminator.md) ~~ [Точки Лагранжа](l_points.md) ~~ [Эффект Оберта](oberth_eff.md)|
|**`Модель:`**<br> [DEM](draw.md) ~~ [SPICE](spice.md) ~~ [ВДМ](draw.md) ~~ [Лимит](limit.md) ~~ [МИХ](mic.md) ~~ [Осциллятор](oscillator.md)|
|**【[Software](soft.md)】**<br> [ASP](asp.md) ~~ [Blender](blender.md) ~~ [C](plang.md) ~~ [Cosmographia](cosmographia.md) ~~ [DOORS](doors.md) ~~ [DWG](cad_f.md) ~~ [GIMP](gimp.md) ~~ [Git](git.md) ~~ [IGES](cad_f.md) ~~ [ISIS](isis.md) ~~ [JT](cad_f.md) ~~ [NGT](neogeography_toolkit.md) ~~ [NX](nx.md) ~~ [Octave](gnu_octave.md) ~~ [OS](os.md) ~~ [PDF](pdf.md) ~~ [Python](plang.md) ~~ [R](plang.md) ~~ [SPICE](spice.md) ~~ [STEP](cad_f.md) ~~ [STL](stk.md) ~~ [SVG](cad_f.md) ~~ [Syncthing](syncthing.md) ~~ [SysML](sysml.md) ~~ [Teamcenter](teamcenter.md) ~~ [Valispace](valispace.md) ~~ [Система управления версиями](vcs.md) ~~ [ХРИП](adra.md)|

1. Docs: …
1. <http://studopedia.ru/1_118256_sistema-razrabotki-i-postanovki-produktsii-na-proizvodstvo.html>
1. <https://ru.wikisource.org/wiki/Категория:ГОСТы>


## The End

end of file
