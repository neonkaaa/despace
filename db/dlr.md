# DLR
> 2019.08.13 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/d/dlr_logo1t.webp)](f/c/d/dlr_logo1.webp)|<contact-dlr@dlr.de>, +49(2203)60-10, Fax …;<br> *V449+2Q Koln, Germany*<br> <http://dlr.de> ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/German_Aerospace_Center)|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Германский центр авиации и космонавтики (нем. Deutsches Zentrum für Luft - und Raumfahrt e.V., англ. German Aerospace Center) (DLR)** — национальный центр аэрокосмических, энергетических и транспортных исследований Германии. Отделения и исследовательские центры организации располагаются в нескольких местах по всей территории Германии, штаб‑квартира находится в Кёльне. Организация несёт ответственность за планирование и осуществление германской космической программы от имени федерального правительства Германии. Центр занимается широким кругом исследовательских проектов, как национальных, так и международных. Организован в 1969 году.

Missions

- InSight
- Ariane
- Galileo
- ISS
- Mars Express
- HALO
- TerraSAR-X
- TanDEM-X



## The End

end of file
