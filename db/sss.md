# Soil sampling system
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [SSS](sss.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Soil sampling system (SSS)** — EN term. **Грунтозаборное устройство (ГЗУ)** — RU analogue.</small>

**Soil sampling system (SSS)** *(also Drilling rig, [Logging](sss.md)‑drilling rig)* — device for soil sampling, incl. from depths & wells, & for transferring them for research by analytical instruments on board the SC.

| |**[Company](contact.md)**|**SSS models**|
|:--|:--|:--|
|**RU**|[GEOKHI RAS](geokhi_ras.md)|[GZU-LR1](гзу_лр1.md) ~~ [LB-15](sss_lst.md)<br> **【Archived】** [LB-09](sss_lst.md) ~~ [LB-10](sss_lst.md) ~~ [VB02](sss_lst.md)|

【**Table.** Manufacturers】

| | |
|:--|:--|
|**AE**|…|
|**AU**|…|
|**CA**|・[Deltion](deltion.md)|
|**CN**|…|
|**EU**|…|
|**IL**|…|
|**IN**|…|
|**JP**|…|
|**KR**|…|
|**RU**|・[GEOKHI RAS](geokhi_ras.md) — design of SSS & zondes.<br> ・[IKI RAS](iki_ras.md)<br> ・KB OM|
|**SA**|…|
|**SG**|…|
|**US**|…|
|**VN**|…|



## Logging (carottage)
> <small>**Logging** — EN term. **Каротаж** — RU analogue.</small>

**Logging, Carottage** *(fr. Carottage, from carotte — carrot, with which the similarity of the logging probe)* is the general name of the methods of the most common variety of geophysical studies of wells. The logging is a detailed study of the structure of the hole of the well using the descent‑lift in it of the geophysical probe.

The method has a small radius of study around the well (from several ㎝ to several meters), but has a high detail that allows not only to determine the depth of the reservoir, but even the nature of the reservoir itself at all its low power.

The numerous of the logging methods is due to the variety of ground‑based geophysic methods, for each of which a similar 「underground」 option has been developed. Moreover, there are special types of logging that have no analogues in ground geophysics. Therefore, the logging methods differ in the nature of the physical fields studied by them: electrical, nuclear & others.



## Stratification
> <small>**Stratification** — EN term. **Стратификация** — RU analogue.</small>

**Stratification** (lat. stratum & facio — 「splitting」):

1. Earth sciences:
   1. Stable & unstable stratification
   1. Stratification, or stratum, the layering of rocks
   1. Stratification (archeology), the formation of layers (strata) in which objects are found
   1. Stratification (water), the formation of water layers based on temperature (and salinity, in oceans)
   1. Lake stratification, the formation of water layers based on temperature, with mixing in the spring & fall in seasonal climates.
   1. Atmospheric stratification, the dividing of the Earth's atmosphere into strata
   1. Inversion (meteorology)



## Docs/Links
|**Sections & pages**|
|:--|
|**【Soil sampling system (SSS)】**<br> [Logging](sss.md) ~~ [Stratification](sss.md)<br>~ ~ ~ ~ ~<br> **RU:** [GZU-LR1](гзу_лр1.md) ~~ [LB-15](sss_lst.md) *([VB02](sss_lst.md) ~~ [LB-09](sss_lst.md) ~~ [LB-10](sss_lst.md))**|

1. Docs: [Presentation for SC Luna‑Grunt ❐](f/sss/2018_ikiran_sluta.djvu) (Moscow, IKI RAS, 2018, Slyuta E.N.)
1. <http://geofpro.com/space-drilling.html>
1. <https://en.wikipedia.org/wiki/Stratification>
1. <https://ru.wikipedia.org/wiki/Каротаж>


## The End

end of file
