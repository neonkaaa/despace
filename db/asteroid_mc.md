# Asteroid Mining Corporation
> 2022.01.27 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/a/asteroid_mc_logo1t.webp)](f/c/a/asteroid_mc_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *…, Liverpool, United Kingdom*<br> <https://asteroidminingcorporation.co.uk> ~~ [LI ⎆](https://www.linkedin.com/company/asteroid-mining-corporation-limited)|
|:--|:--|
|**Business**|Robotic & satellite platforms for asteroid mining|
|**Mission**|To establish a market for space resource utilisation through innovative robotics & a Space Resources Database|
|**Vision**|Asteroids as the basis of the extraterrestrial economy|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO — Mitch Hunter-Scullion<br> ・CTO — Subham Gupta<br> ・COO — Jeremy Soper<br> ・Chief Robotics Engineer — Mickaël Laîné|

**Asteroid Mining Corporation Ltd** is a UK company aimed for space resources explorations. Founded 2016.03.25.

We are currently developing robotic & satellite platforms to enable the exploration & extraction of off‑world resources, incl. asteroids. These failed planetary remnants from the early formation of the solar system are an untapped source of Platinum Group Metals, key ingredients in a range of modern technologies from hydrogen cars to smartphones & prosthetic limbs.

AMC is developing Space Capable Asteroid Robotic Explorers (SCAR-E) in partnership with Tohoku University Space Robotics Lab (SRL), Japan. SCAR-E is being designed to open up the exploration of the Solar System, in line with current trends in the launch services market, with a low cost, highly functional, walking & climbing robot. Scalable up to 20x its initial size, SCAR-E will be capable of facilitating asteroid, planetary & lunar exploration & mining operations, as well as in-orbit asset maintenance i.e. spacewalking/EVAs. The low cost, highly ruggedised robot will be available for commercial use across a range of destinations both on Earth & throughout the Solar System. Terrestrial beachhead markets include critical infrastructure inspection, nuclear decommission, remote sensing, disaster relief & search & rescue: reducing human exposure to hostile environments.

<p style="page-break-after:always"> </p>

## The End

end of file
