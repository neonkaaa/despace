# ТЭО
> 2019.05.02 [🚀](../../index/index.md) [despace](index.md) → **[ТЭО](fs.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Технико‑экономическое обоснование (ТЭО)** — русскоязычный термин. **Feasibility study (FS)** — англоязычный эквивалент.</small>  
>> <small>*「Капитал … избегает шума и брани и отличается боязливой натурой. Это правда, но это ещё не вся правда. Капитал боится отсутствия прибыли или слишком маленькой прибыли, как природа боится пустоты. Но раз имеется в наличии достаточная прибыль, капитал становится смелым. Обеспечьте 10 %, и капитал согласен на всякое применение, при 20 % он становится оживлённым, при 50 % положительно готов сломать себе голову, при 100 % он попирает все человеческие законы, при 300 % нет такого преступления, на которое он не рискнул бы, хотя бы под страхом виселицы. Если шум и брань приносят прибыль, капитал станет способствовать тому и другому. Доказательство: контрабанда и торговля рабами.」<br> — Английский публицист XIX века Томас Джозеф Даннинг (1799 ‑ 1873)*</small>

**Технико‑экономическое обоснование (ТЭО)** — документ, в котором представлена информация, из которой выводится целесообразность (или нецелесообразность) создания продукта или услуги. ТЭО содержит анализ затрат и результатов какого‑либо проекта.

Раунды инвестиций:

1. **pre-seed:** на этом этапе стартаперы вкладывают собственные средства, просят у родителей и друзей; такие инвестиции ещё называют FFF (family, friends, fools — 「семья, друзья, дураки」; под дураками подразумеваются инвесторы без опыта);
1. **seed**, или посевные инвестиции: доработка главного продукта, поиска product‑market fit и первые продажи. В большинстве случаев компании на ранних стадиях убыточны;
1. **раунд А:** компания становится самостоятельной и вызывает интерес инвесторов. У неё уже есть готовый продукт, средства привлекаются на масштабирование продаж;
1. **раунд В:** компания привлекает средства также на масштабирование продаж, расширение географии;
1. **раунд С** и далее. Последующие раунды не так важны — они обозначают лишь количество проведённых раундов.



## Описание ТЭО
Не является составной частью КК (КС), в связи с чем [схема деления](draw.md) отсутствует.

ТЭО создаётся в результате одного из следующих воздействий:

1. требования рынка;
1. потребности организации;
1. требования заказчика;
1. технологический прогресс;
1. правовые требования;
1. экологические воздействия;
1. социальные потребности.

**Отличия от бизнес‑плана.**  
Несмотря на то, что ТЭО похоже на бизнес‑план, отличие заключается в том, что ТЭО представляет собой обоснование проекта, в то время как бизнес‑план содержит описание миссии и целей организации, то есть обоснование существования предприятия.

В ТЭО предполагается отображение следующих пунктов:

1. технологический процесс;
1. требования к производственной инфраструктуре;
1. основное оборудование, приспособления и оснастка;
1. персонал и трудозатраты;
1. сводная себестоимость продукции;
1. сроки осуществления проекта;
1. экономическая эффективность;
1. экологические воздействия.



## ТЭО в мире


### NASA

1. [NASA Instrument Cost Model (NICM)](nicm.md) — инструмент оценки стоимости разработки.
1. Классификация стоимости миссий/СЧ:
   1. Class A ( large strategic science missions or large strategic missions, formerly known as Flagship missions or Flagship-class missions) — дорогой/сложный;
   1. Class B;
   1. Class C;
   1. Class D — дешёвый/простой.

NASA’s **large strategic science missions** or **large strategic missions**, formerly known as **Flagship missions** or **Flagship-class missions**, are the costliest and most capable NASA science spacecraft. Flagship missions exist within all four divisions of NASA’s Science Mission Directorate: the astrophysics, Earth science, heliophysics and planetary science divisions.

"Large" refers to the budget of each mission, typically the most expensive mission in the scientific discipline. Within the Astrophysics Division and the Planetary Science Division, the large strategic missions are usually in excess of $1 billion. Within Earth Science Division and Heliophysics Division, the large strategic missions are usually in excess of $500 million. "Strategic" refers to their role advancing multiple strategic priorities set forth in plans such as the Decadal Surveys. "Science" marks these missions as primarily scientific in nature, under the Science Mission Directorate (SMD), as opposed to, e.g., human exploration missions under the Human Exploration and Operations Mission Directorate (HEOMD). The lines can be blurred, as when the Lunar Reconnaissance Orbiter began as a directed mission from the HEOMD, and was later transferred to the SMD.

Flagship missions are not under the purview of any larger "Flagship Program", unlike, e.g., Discovery-class missions that are under the purview of the Discovery Program. Unlike these competed classes that tender proposals through a competitive selection process, the development of Flagship missions is directed to a specific institution — usually a NASA center or the Jet Propulsion Laboratory — by the Science Mission Directorate. Flagship missions are developed ad-hoc, with no predetermined launch cadence or uniform budget size. Flagship missions are always Class A missions: high priority, very low risk.

**NASA Large Strategic Science Missions**

|**Mission name**|**Mission start**|
|:--|:--|
|**`Astrophysics Division`**| |
|Compton Gamma‑ray Observatory|1991|
|Hubble Space Telescope|1990|
|Chandra X‑ray Observatory|1999|
|James Webb Space Telescope|In development (2021 launch)|
|Wide-Field Infrared Survey Telescope (WFIRST)|In formulation (mid-2020s launch)|
|**`Earth Science Division`**| |
|Terra|1999|
|Aqua|2002|
|ICESat|2003|
|Aura|2004|
|Joint Polar Satellite System (JPSS) - a constellation|2011|
|Plankton, Aerosol, Cloud, ocean Ecosystem (PACE)|In development|
|**`Heliophysics Division`**| |
|Solar Dynamics Observatory|2010|
|Van Allen Probes|2012|
|Magnetospheric Multiscale Mission (MMS)|2015|
|Parker Solar Probe|2018|
|**`Planetary Science Division`**| |
|Viking 1, 2|1975|
|Voyager 1, 2|1977|
|Galileo|1989|
|Cassini|1999|
|Mars Science Laboratory/Curiosity rover|2011|
|Mars 2020 rover|In development (2020 launch)|
|Europa Clipper|In development (2022-25 launch)|
|Europa Lander|Proposed (2024 launch)|



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**`Технико‑экономическое обоснование (ТЭО):`**<br> [NICM](nicm.md) ~~ [Невозвратные затраты](sunk_cost.md) ~~ [Номинал](nominal.md) ~~ [Оценка стоимости работ на НПОЛ](lav.md) ~~ [Секвестр](budget_seq.md) ~~ [Стоимость аппарата в граммах](sc_price.md)|

1. Docs: …
1. <https://ru.wikipedia.org/wiki/Прибыль>
1. <https://en.wikipedia.org/wiki/Feasibility_study>
1. <https://en.wikipedia.org/wiki/Large_strategic_science_missions>


## The End

end of file
