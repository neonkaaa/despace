# R.o.M.
> 2021.11.18 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/r/rom_logo1t.webp)](f/c/r/rom_logo1.webp)|<peter@robotic-mining.com>, +81(3)3353-57-99, Fax …;<br> *18-35 Minami Motomachi, Shinjuku-ku. Tokyo-ku 160-0012, JP*<br> <http://robotic-mining.com> ~~ [LI ⎆](https://www.linkedin.com/company/robotic-mining)|
|:--|:--|
|**Business**|R&D devices for space mineral exploration & mining|
|**Mission**|Reach for the stars & transform the world to a Star Trek future, using robotics & AI to increase efficiencies & improve processes, not to replace humans|
|**Vision**|Taking you to the future, today|
|**Values**|Honesty, integrity, creativity, transparency, modesty, ethics, scince, technology, diversity, education, knowledge|
|**[MGMT](mgmt.md)**|・CEO, Managing Director — Peter Ness|

**Robotic Mining PTY LTD (R.o.M.)** is a start‑up aiming for constructing devices for space mineral exploration & mining; adapting them to market & sell on Earth, to fund Lunar & Mars mineral exploration. The aim is to use AI & robotics to help people do their jobs faster, easier & safer, not to replace them. Founded in 2020.

Registered office: Suite 2, Level 1, 9-11 Grosvenor Street, Neutral Bay, NSW, Australia 2089

[Description ❐](f/c/r/rom_descr1.pdf) (2021.11.18)

<p style="page-break-after:always"> </p>

## The End

end of file
