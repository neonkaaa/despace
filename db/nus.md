# NUS
> 2022.03.29 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/n/nus_logo1t.webp)](f/c/n/nus_logo1.webp)|<mark>noemail</mark>, +65 6516 6666, Fax …;<br> *21 Lower Kent Ridge Road, Singapore 119077*<br> <https://www.nus.edu.sg> ~~ [FB ⎆](https://www.facebook.com/nus.singapore) ~~ [IG ⎆](https://instagram.com/nus_singapore) ~~ [LI ⎆](https://www.linkedin.com/school/5524) ~~ [X ⎆](https://twitter.com/NUSingapore) ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/National_University_of_Singapore)|
|:--|:--|
|**Business**|Higher education, small sats development|
|**Mission**|To educate, inspire & transform|
|**Vision**|A leading global university shaping the future|
|**Values**|Innovation resilience, excellence, respect, integrity|
|**[MGMT](mgmt.md)**|・President — Tan Eng Chye<br> ・Senior Deputy President — Ho Teck Hua|

The **National University of Singapore (NUS)** is a national collegiate research university in Queenstown, Singapore. Founded in 1905 as the Straits Settlements & Federated Malay States Government Medical School, NUS has consistently been considered as being one of the top & prestigious academic institutions in the world as well as in the Asia‑Pacific itself. It plays a key role in the further development of modern technology & science, offering a global approach to education & research, with a focus on expertise & perspectives of Asia. In 2022, the QS World University Rankings ranked NUS 11th in the world & 1st in Asia.

**Notable space‑related departments:**

- **[Satellite Technology & Research Centre (STAR)](nus_star.md)** — small sats R&D

<p style="page-break-after:always"> </p>

## The End

end of file
