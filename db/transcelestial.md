# Transcelestial
> 2022.06.28 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/t/transcelestial_logo1t.webp)](f/c/t/transcelestial_logo1.webp)|<info@transcelestial.com>, <mark>nophone</mark>, Fax …;<br> *#06-04, 101 Eunos Ave 3, Singapore 409835*<br> <https://www.transcelestial.com> ~~ [FB ⎆](https://www.facebook.com/transcelestial) ~~ [IG ⎆](https://www.instagram.com/transcelestial) ~~ [LI ⎆](https://www.linkedin.com/company/transcelestial) ~~ [X ⎆](https://twitter.com/trans_celestial)|
|:--|:--|
|**Business**|Telecoms|
|**Mission**|Connecting every last mile with next generation wireless fiber optics|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO, Founder — Rohit Jha<br> ・CTO — Mohammad Danesh|

**Transcelestial** is a Singaporean company aimed to provide secure, last‑mile connectivity with fiber‑like speeds, at a fraction of fiber’s deployment time and cost. Founded 2016.12.08.

<p style="page-break-after:always"> </p>

## The End

end of file
