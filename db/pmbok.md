# Свод знаний по управлению проектами
> 2019.04.05 [🚀](../../index/index.md) [despace](index.md) → [Control](control.md), [SE](se.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Свод знаний по управлению проектами (СЗПУП)** — русскоязычный термин. **Project Management Body of Knowledge (P㎆oK)** — англоязычный эквивалент.</small>

**Свод знаний по управлению проектами** *(англ. Project Management Body of Knowledge, P㎆oK)* представляет собой сумму профессиональных знаний по управлению проектами. [Институт управления проектами](project_management_institute.md) использует этот документ в качестве основного справочного материала, руководства для своих программ по профессиональному развитию.

В настоящем руководстве описываются суть процессов управления проектами в терминах интеграции между процессами и взаимодействий между ними, а также цели, которым они служат. Эти процессы разделены на пять групп, называемых 「группы процессов управления проектом」.



## Группы процессов
Все процессы разделяются на следующие группы.
1. **Группа процессов инициирования.** Состоит из процессов, способствующих формальной авторизации начала нового проекта. Входят следующие процессы:
   1. Разработка устава проекта
   1. Определение заинтересованных сторон проекта
   2. **Группа процессов планирования.** Определяет/уточняет цели и планирует действия, необходимые для достижения целей/содержания, ради которых был предпринят проект. Входят следующие процессы:
   1. Разработка плана управления проектом
   1. Планирование содержания
   1. Определение содержания
   1. Создание иерархической структуры работ (ИСР)
   1. Определение состава операций
   1. Определение взаимосвязей операций
   1. Оценка ресурсов
   1. Оценка длительности операций
   1. Разработка расписания
   1. Стоимостная оценка
   1. Разработка бюджета расходов
   1. Планирование качества
   1. Планирование человеческих ресурсов
   1. Планирование коммуникаций
   1. Планирование управления рисками
   1. Идентификация рисков
   1. Качественный анализ рисков
   1. Количественный анализ рисков
   1. Планирование реагирования на риски
   1. Планирование покупок
   1. Планирование контрактов
   3. **Группа процессов исполнения.** Объединяет человеческие и другие ресурсы для выполнения плана управления проектом. Входят следующие процессы:
   1. Руководство и управление исполнением проекта
   1. Процесс обеспечения качества
   1. Набор команды проекта
   1. Развитие команды проекта
   1. Распространение информации
   1. Запрос информации у продавцов
   1. Выбор продавцов
   4. **Группа процессов мониторинга и управления.** Регулярно оценивает прогресс проекта и осуществляет мониторинг, чтобы обнаружить отклонения от плана, и, в случае необходимости, провести коррективы для достижения целей проекта. Входят следующие процессы:
   1. Мониторинг и управление работами проекта
   1. Общее управление изменениями
   1. Подтверждение содержания
   1. Управление содержанием
   1. Управление расписанием
   1. Управление стоимостью
   1. Процесс контроля качества
   1. Управление командой проекта
   1. Отчетность по исполнению
   1. Управление участниками проекта
   1. Наблюдение и управление рисками
   1. Администрирование контрактов
   5. **Группа завершающих процессов.** Формализует приёмку продукта/услуги/результата и подводит проект (фазу проекта) к правильному завершению. Содержит следующие процессы:
   1. Закрытие проекта
   1. Закрытие контрактов



## Области знаний
Рассматривает области знаний по управлению проектами:

1. Управление интеграцией проекта
1. Управление содержанием проекта
1. Управление сроками проекта
1. Управление стоимостью проекта
1. Управление качеством проекта
1. Управление человеческими ресурсами проекта
1. Управление коммуникациями проекта
1. Управление рисками проекта
1. Управление поставками проекта
1. Управление стэйкхолдерами



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Control](Control.md)】**<br> [Ad hoc](ad_hoc.md) ~~ [Business travel](business_travel.md) ~~ [Chief designers council](cocd.md) ~~ [CML](trl.md) ~~ [Competence](competence.md) ~~ [Confident](confident.md) ~~ [Consp.theory](consp_theory.md) ~~ [Control sys. (CS)](cs.md) ~~ [Coordinate system](coord_sys.md) ~~ [Curator](curator.md) ~~ [Designer’s supervision](des_spv.md) ~~ [E‑sig](esig.md) ~~ [Engineer](se.md) ~~ [Errand](errand.md) ~~ [Federal law](fed_law.md) ~~ [Federal TP](fed_tp.md) ~~ [Federal SP](fed_sp.md) ~~ [GNC](gnc.md) ~~ [Gravity assist](gravass.md) ~~ [Industrial archaeology](ind_arch.md) ~~ [Instruction](instruction.md) ~~ [Lean manuf.](lean_man.md) ~~ [Lifetime](lifetime.md) ~~ [Manager](manager.md) ~~ [MBSE](se.md) ~~ [Meeting](meeting.md) ~~ [MCC](sc.md) ~~ [MIC](mic.md) ~~ [MML](mml.md) ~~ [MoU](contract.md) ~~ [Nav. & ballistics (NB)](nnb.md) ~~ [Nonprofit org.](nonprof_org.md) ~~ [NX](nx.md) ~~ [Oberth effect](oberth_eff.md) ~~ [Org.structure](orgstruct.md) ~~ [Outcomes commission](outccom.md) ~~ [Patent](patent.md) ~~ [Peter prin.](peter_principle.md) ~~ [Plan](plan.md) ~~ [PMBok](pmbok.md) ~~ [Quorum](quorum.md) ~~ [R&D management](mgmt.md) ~~ [R&D support](rnd_support.md) ~~ [Recursion](recurs.md) ~~ [Schulze_method](schulze_method.md) ~~ [Sci'N'Tech activities](st_act.md) ~~ [Sci'N'Tech council](satc.md) ~~ [Single-window system](sw_sys.md) ~~ [Situ.leadership](situ_leadership.md) ~~ [Skunk works](se.md) ~~ [State arm. plan](plan_sa.md) ~~ [Swamp](swamp.md) ~~ [Teamcenter](teamcenter.md) ~~ [Tennis racket theorem](tr_theorem.md) ~~ [TRIZ](triz.md) ~~ [TRL](trl.md) ~~ [V‑model](v_model.md) ~~ [Veto](veto.md) ~~ [Workflow](workflow.md) ~~ [Workgroup](wg.md)|
|**【[Systems engineering](se.md)】**<br> [Competence](competence.md) ~~ [Coordinate system](coord_sys.md) ~~ [Designer’s supervision](des_spv.md) ~~ [Industrial archaeology](ind_arch.md) ~~ [Instruction](instruction.md) ~~ [Lean manuf.](lean_man.md) ~~ [Lifetime](lifetime.md) ~~ [MBSE](se.md) ~~ [MML](mml.md) ~~ [Nav. & ballistics (NB)](nnb.md) ~~ [NASA SEH](book_nasa_seh.md) ~~ [Oberth effect](oberth_eff.md) ~~ [PMBok](pmbok.md) ~~ [Quorum](quorum.md) ~~ [R&D management](mgmt.md) ~~ [R&D support](rnd_support.md) ~~ [Recursion](recurs.md) ~~ [Schulze_method](schulze_method.md) ~~ [Sci'N'Tech activities](st_act.md) ~~ [Sci'N'Tech council](satc.md) ~~ [Skunk works](se.md) ~~ [SysML](sysml.md) ~~ [Tennis racket theorem](tr_theorem.md) ~~ [TRIZ](triz.md) ~~ [TRL](trl.md) ~~ [V‑model](v_model.md) ~~ [Workflow](workflow.md) ~~ [Workgroup](wg.md)|

1. Docs:
   1. [Project Management Body of Knowledge (P㎆oK) ❐](f/doc/pmbok.pdf)
1. <https://en.wikipedia.org/wiki/Project_Management_Body_of_Knowledge>


## The End

end of file
