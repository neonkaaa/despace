# Technical report
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [Doc](doc.md), [Report](report.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

A **technical report** (also **scientific report**) is a document that describes the process, progress, or results of technical or scientific research or the state of a technical or scientific research problem. It might also include recommendations and conclusions of the research. Unlike other scientific literature, such as scientific journals and the proceedings of some academic conferences, technical reports rarely undergo comprehensive independent peer review before publication. They may be considered as grey literature. Where there is a review process, it is often limited to within the originating organization. Similarly, there are no formal publishing procedures for such reports, except where established locally.

Technical reports are today a major source of scientific and technical information. They are prepared for internal or wider distribution by many organizations, most of which lack the extensive editing and printing facilities of commercial publishers.

Technical reports are often prepared for sponsors of research projects. Another case where a technical report may be produced is when more information is produced for an academic paper than is acceptable or feasible to publish in a peer‑reviewed publication; examples of this include in‑depth experimental details, additional results, or the architecture of a computer model. Researchers may also publish work in early form as a technical report to establish novelty, without having to wait for the often long production schedules of academic journals. Technical reports are considered 「non‑archival」 publications, and so are free to be published elsewhere in peer‑reviewed venues with or without modification.


## (RU) Технический отчёт

> <small>**Technical report (TR)** — EN term. **Технический отчёт (ТО)** — RU analogue.</small>

**Технический отчёт (ТО)** — некий абстрактный технический документ, не являющийся согласно [ГОСТ 2.103](гост_2_103.md) ЕСКД [конструкторской документацией](doc.md).

Вместо термина *「технический отчёт」* рекомендуется использовать термин 「[научно‑технический отчёт](report.md)」, как соответствующий нормативной документации.

В ГОСТ 1.3‑2008 *「Межгосударственная система стандартизации. Правила и методы принятия международных и региональных стандартов в качестве межгосударственных стандартов」* написано:  
**Технический отчёт** *(Technical Report (TR)*: Международный документ, не являющийся Международным Стандартом, опубликованный ISO или IEC, содержащий различного рода данные, отличные от тех, которые обычно публикуются в качестве Международного Стандарта или технических требований/условий.



## Docs/Links
|**Sections & pages**|
|:--|
|**【[](.md)】**<br> <mark>NOCAT</mark>|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Technical_report>


## The End

end of file
