# Yspace
> 2020.07.15 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/y/yspace_logo1t.webp)](f/c/y/yspace_logo1.webp)|<team.ysapce.mars@gmail.com>, <mark>nophone</mark>, Fax …;<br> *2 Chome−1−6 A-19-I, Sengen, Tsukuba, 〒305-0047 Ibaraki, Japan*<br> <https://yspace-llc.com> ・ <https://www.facebook.com/TeamYspace>|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Yspace LLC** is a Japanese startup aimed for production & consulting of the VR-content for space applications. Founded 2018.06.



## The End

end of file
