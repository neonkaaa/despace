# Околоземное космическое пространство
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → **[Земля](earth.md)**, [Space](index.md), [Экология](ecology.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Околоземное космическое пространство (ОКП)** — русскоязычный термин. **Near space** — англоязычный эквивалент.</small>

**Околоземное космическое пространство (ОКП)** — область космоса вокруг [Земли](earth.md).

Расстояния:

1. По ГОСТ 25645.103-84 равно большой полуоси [Луны](moon.md) (**384 399 км**). В рамках этого расстояния от центра Земли предъявляются требования по незасорению.
1. По ОСТ 134-1022-99 — это область пространства на высотах **от 200 до 2 000 ㎞** от поверхности Земли.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Space](index.md)】**<br> [Apparent magnitude](app_mag.md) ~~ [Astro.object](aob.md) ~~ [Blue Marble](earth.md) ~~ [Cosmic rays](ion_rad.md) ~~ [Ecliptic](ecliptic.md) ~~ [Escape velocity](esc_vel.md) ~~ [Health](health.md) ~~ [Hill sphere](hill_sphere.md) ~~ [Information](info.md) ~~ [Lagrangian points](l_points.md) ~~ [Near space](near_space.md) ~~ [Pale Blue Dot](earth.md) ~~ [Parallax](parallax.md) ~~ [Point Nemo](earth.md) ~~ [Silver Snoopy award](silver_snoopy_award.md) ~~ [Solar constant](solar_const.md) ~~ [Terminator](terminator.md) ~~ [Time](time.md) ~~ [Wormhole](wormhole.md) ┊ ··•·· **Solar system:** [Ariel](ariel.md) ~~ [Callisto](callisto.md) ~~ [Ceres](ceres.md) ~~ [Deimos](deimos.md) ~~ [Earth](earth.md) ~~ [Enceladus](enceladus.md) ~~ [Eris](eris.md) ~~ [Europa](europa.md) ~~ [Ganymede](ganymede.md) ~~ [Haumea](haumea.md) ~~ [Iapetus](iapetus.md) ~~ [Io](io.md) ~~ [Jupiter](jupiter.md) ~~ [Makemake](makemake.md) ~~ [Mars](mars.md) ~~ [Mercury](mercury.md) ~~ [Moon](moon.md) ~~ [Neptune](neptune.md) ~~ [Nereid](nereid.md) ~~ [Nibiru](nibiru.md) ~~ [Oberon](oberon.md) ~~ [Phobos](phobos.md) ~~ [Pluto](pluto.md) ~~ [Proteus](proteus.md) ~~ [Rhea](rhea.md) ~~ [Saturn](saturn.md) ~~ [Sedna](sedna.md) ~~ [Solar day](solar_day.md) ~~ [Sun](sun.md) ~~ [Titan](titan.md) ~~ [Titania](titania.md) ~~ [Triton](triton.md) ~~ [Umbriel](umbriel.md) ~~ [Uranus](uranus.md) ~~ [Venus](venus.md)|
|**`Экология:`**<br> [Класс опасности](danger_goods.md) ~~ [Нейтрализация КРТ](нейтрализация_крт.md) ~~ [Околоземное космическое пространство](near_space.md)|

1. Docs: …
1. <…>


## The End

end of file
