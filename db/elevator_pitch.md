# Elevator pitch
> 2019.10.19 [🚀](../../index/index.md) [despace](index.md) → **[](.md)** <mark>NOCAT</mark>  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Elevator pitch** — EN term. **Презентация для лифта** — RU analogue.</small>

An **elevator pitch**, **elevator speech**, or **elevator statement** is a short description of an idea, product or company that explains the concept in a way such that any listener can understand it in a short period of time. This description typically explains who the thing is for, what it does, why it is needed, & how it will get done. Finally, when explaining an individual person, the description generally explains one’s skills & goals, & why they would be a productive & beneficial person to have on a team or within a company or project. An elevator pitch does not have to include all of these components, but it usually does at least explain what the idea, product, company, or person is & their value.

An elevator pitch can be used to entice an investor or executive in a company, or explain an idea to a founder’s parents. The goal is simply to convey the overall concept or topic in an exciting way. Unlike a sales pitch, there may not be a clear buyer‑seller relationship.

The name — elevator pitch — reflects the idea that it should be possible to deliver the summary in the time span of an elevator ride, or approximately thirty seconds to two minutes.



## (EN)

### Background information & history
There are many origin stories for the elevator pitch. One commonly‑known origin story is that of Ilene Rosenzweig & Michael Caruso, two former journalists active in the 1990s. According to Rosenzweig, Caruso was a senior editor at Vanity Fair & was continuously attempting to pitch story ideas to the Editor‑In‑Chief at the time, but could never pin her down long enough to do so simply because she was always on the move. So, in order to pitch her ideas, Caruso would join her during short free periods of time she had, such as on an elevator ride. Thus, the concept of an elevator pitch was created, as says Rosenzweig.

However, there is another known potential origin story that dates back prior to the story of Rosenzweig & Caruso. Philip Crosby, author of The Art of Getting Your Own Sweet Way (1972) & Quality Is Still Free (1996) suggested individuals should have a pre‑prepared speech that can deliver information regarding themselves or a quality that they can provide within a short period of time, namely the amount of time of an elevator ride for if an individual finds themselves on an elevator with a prominent figure. Essentially, an elevator pitch is meant to allow an individual to pitch themselves or an idea to a person who is high up in a company, with very limited time.

Crosby, who worked as a quality test technician, & then later as the Director of Quality at International Telephone & Telegraph, recounted how an elevator pitch could be used to push for change within the company. He planned a speech regarding the change he wanted to see & waited at the ITT headquarters elevator. Crosby stepped onto an elevator with the [CEO](mgmt.md) of the company in order to deliver his speech. Once they reached the floor in which the [CEO](mgmt.md) was getting off, Crosby was asked to deliver a full presentation on the topic at a meeting for all of the general managers.

The concept of an elevator pitch, as created by Crosby, did not receive much popularity until two statisticians, Gerry Hahn from General Electric & Tom Boardman, a professor at the University of Colorado, started to push the concept onto other statisticians. The two pushed this concept in order to help other statisticians in promoting what they have to offer. Boardman pushed the concept much further than Hahn, resulting in the elevator pitch ultimately being linked to Boardman & his professional life.

Another origin story for the elevator pitch is from Elisha Graves Otis, the inventor of a device meant for elevator safety, which dates back to the 1850s. In 1852, Otis began developing a device that would improve the safety of elevators, specifically related to the hoists used to lift heavy equipment to upper floors. The device was created out of steel wagon spring meshing & provided a backup catch & hold mechanism if the rope lifting the elevator gave way. In 1854, in the Crystal Palace Exposition in New York in front of a big audience, Otis revealed his device. He did so by lifting an elevator, like normal, but as it was positioned approximately halfway up the elevator shaft, the rope was cut. The elevator platform was held in place by Otis' device.



### Aspects
An elevator pitch is meant to last the duration of an elevator ride, which can vary in length from approximately 15 seconds to two minutes. Therefore, the main focus of an elevator pitch should be making it short & direct. According to the Idaho Business Review, the 1st two sentences of any elevator pitch are the most important, & should hook or grab the attention of the listener. Information in an elevator pitch, due to the limited amount of time, should be condensed in order to express the most important ideas or concepts within the allotted time.

Furthermore, the Idaho Business Review suggests individuals who use an elevator pitch deliver it using simple language, & avoiding statistics or other language that may disrupt the focus of the listener. Bloomberg Businessweek suggests that an important lesson to think about when giving an elevator pitch is to 「adjust the pitch to the person who is listening, & refine it as you & your business continue to grow & change」. When delivering an elevator pitch, individuals are encouraged to remain flexible & adaptable, & to be able to deliver the pitch in a genuine & fluent fashion. By doing so, the intended audience of the pitch will likely be able to follow the information, & will not interpret it as being too scripted.



### Advantages

Advantages to conducting an elevator pitch include convenience & simplicity. For instance, elevator pitches can be given on short notice & without much preparation due to the pre‑planning of the content being delivered within said pitch. making the listener more comfortable. Furthermore, elevator pitches allow the individual who is giving the pitch the ability to simplify the content & deliver it in a less complicated manner by providing the information in a cut‑down fashion that gets right to the point.



## (RU) Презентация для лифта
> <small>**Презентация для лифта** — русскоязычный термин. **Elevator pitch** — англоязычный эквивалент.</small>

**Презентация для лифта** (или **речь для лифта**) *(англ. Elevator Pitch или Elevator Speech)* — короткий рассказ о концепции продукта, проекта или сервиса. Термин отражает ограниченность по времени — длина презентации должна быть такой, чтобы она могла быть полностью рассказана за время поездки на лифте, то есть около 30 секунд или 100 ‑ 150 слов.

Термин обычно используется в контексте презентации антрепренёром концепции нового бизнеса партнеру венчурного фонда для получения инвестиций. Поскольку представители венчурных фондов стремятся как можно скорее принимать решение о перспективности или бесперспективности того или иного проекта или команды, первичным критерием отбора становится качество 「презентации для лифта」. Соответственно, качество этой речи и уровень её преподнесения имеют главенствующее значение и для руководителя стартапа, стремящегося найти финансирование.

Правильно составленная презентация для лифта отвечает на вопросы:

1. какой продукт предлагается,
1. какие преимущества имеет продукт,
1. информация о компании.

Тексты, которые можно называть 「презентацией для лифта」, часто используются и представителями других профессий — коммивояжёрами, продавцами, менеджерами проектов — то есть людьми, которым необходимо быстро донести основную идею для слушателя, хотя сам термин, как правило, в таком виде не используется. Разновидностью 「презентации для лифта」 можно считать краткие аннотации на обложках книг.



### Особенности презентации в лифте
**Зачем это нужно?:** Несмотря на то, что сегодня сложно себе представить историю, когда после яркой речи в лифте, инвестор сможет сразу же выдать необходимую сумму, формат 「elevator pitch」 используется для презентации бизнес проектов. Нужно это, чтобы понять насколько начинающие предприниматели сами осознают главные преимущества и выгоды своего проекта. Всё гениальное просто, в том числе и гениальная бизнес идея. Если предприниматель способен отделить зёрна от плевел и выдать информацию, которая интересна инвестору, то можно сделать вывод, что он способен ясно мыслить и правильно действовать в ведении своего бизнеса. Формат презентации в лифте хорошо показывает собранность и находчивость начинающего бизнесмена.

**Как приготовиться?:** Особое внимание стоит уделить формулированию и запоминанию ключевых мыслей и выводов. Кроме того, важно поработать над дикцией и интонацией, так как говорить придётся, скорее всего, быстрее обычного.

**Формат.** Формат презентации в лифте может зависеть от различных сценариев. **Во‑первых**, важно знать, для кого проводится презентация. Презентация в лифте должна строиться индивидуально. Для каждого инвестора есть свой крючок, на который его можно зацепить. Кому‑то интересна сама идея, её оригинальность и иновационность, кто‑то интересуется только финансовой стороной дела, с кем‑то можно говорить неформально, кому‑то важен именно официальный тон и т.д. **Во‑вторых**, формат презентации зависит от того, какие вспомогательные элементы у вас есть. Есть ли у вас с собой готовые образцы продукта вашей будущей фирмы, которые скажут всё за вас?: Есть ли у вас дополнительные материалы, подтверждающие ваши слова?: **В‑третьих**, важно, говорите ли вы в одиночку или вас несколько человек. Вдвоём проще воздействовать, но это требует более кропотливой подготовки и слаженности действий.

**Структура презентации.** В отличие от других видов выступления, презентация в лифте не имеет рекомендованной структуры. Подготовка к презентации в лифте является творческим процессом, и поэтому создавать её нужно опираясь на собственную креативность, о чём вы можете прочитать в разделе сайта, посвященном развитию творческого мышления. Несмотря на это, можно дать некоторые рекомендации по структуре 「элевэйтор спич」:

1. В начале желательно, максимально привлечь внимание слушателя, заставить его думать, над чем вы говорите. Например, можно привести какой‑то неожиданный вызывающий эмоции факт, который в дальнейшей речи будет подчеркивать важность вашего проекта для инвестора.
1. В основной части презентации в лифте важно, сказать, чем выгодно отличается ваш проект от всего остального, чем он привлекателен инвестору.
1. В конце, сделайте небольшое, щедрое предложение для инвестора. Однако важно не перехвалить себя, а наоборот сказать, о важности роли именно этого инвестора для вас и попросить помочь вам. Инвестор должен почувствовать свою силу и необходимость для вас, после того, как поймёт выгодность вашего проекта.


### Правила elevator pitch
В итоге, для презентации в лифте можно выделить следующие общие рекомендации:

1. Нужно как можно подробнее узнать, что интересно слушателю. Обращаться надо к нему лично, и говорить то, что он хочет услышать.
1. Речь должна быть чистой, эмоциональной, без лишних пауз и подбора нужных слов.
1. Говорить нужно быстрее обычного, но не слишком быстро.
1. Не нужно говорить о том, что не является ключевым фактором успеха вашего проекта. Если какую‑то информацию можно выкинуть — выкидывайте.
1. Используйте зрелищные элементы — демонстрация образцов, метафоры и сравнения.

**Что делать.** Ниже приведены правила проведения презентации в лифте перед инвестором.

1. Краткость. Презентация в лифте должна быть короткой. Она ограничена лишь одной‑двумя минутами времени инвестора. Иногда временные ограничения оказываются ещё более жёсткими.
1. Доступность. Речь в elevator pitch должна быть лёгкой для понимания. Часто у инвестора нет желания, времени и сил, чтобы понять заумные схемы, описываемые начинающим бизнесменом. Даже если зрителем презентации в лифте станет ребёнок, он должен понять, о чём идет речь в презентации.
1. Прибыльность. Проект, описанный в презентации в лифте должен вызвать жадность. В конце концов, инвесторы хотят заработать деньги, точнее много денег.
1. Неотразимость. И наконец, презентация в лифте должна быть неотразима. Если после презентации у инвестора остается больше вопросов, чем ответов, начинающему предпринимателю нужно будет снова вернуться за рабочий стол и доделать свою презентацию.

**Чего не делать:**

1. Нельзя смотреть в потолок и вспоминать, какие‑то важные факты.
1. Нельзя делать паузы, давая инвестору возможность уйти с вашей колеи и задать какой‑нибудь каверзный вопрос.
1. Нельзя не знать инвестора в лицо: кто это, что ему интересно. Вы можете просто потерять уйму времени, общаясь 「не с тем человеком」.
1. Незнание рынка. Вы должны точно знать несколько важнейших цифр о рынке, подтверждающих потенциальную успешность вашего проекта.
1. Не рассказать о себе. Инвестор должен видеть огонь в глазах, ощущать и какой‑то background команды проекта.
1. И наконец, совсем непростительно не знать, зачем вы пришли к инвестору, а именно какую сумму вы хотите от него получить и на что вы собираетесь её потратить.


## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[](.md)】**<br> <mark>NOCAT</mark>|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Elevator_pitch>
1. <https://4brain.ru/blog/презентация‑в‑лифте‑elevator-pitch>


## The End

end of file
