# Teamcenter
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → **[Soft](soft.md)**, [Control](control.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

**Teamcenter** — пакет масштабируемых программных решений для поддержки [жизненного цикла изделий](pl.md), созданный на основе открытой платформы PLM.



## Краткое описание
Решения Teamcenter предназначены для интенсификации создания разработок, ускорения вывода [изделия](unit.md) на рынок, обеспечения соответствия управленческим и законодательным требованиям, оптимизации использования ресурсов предприятия и поддержки сотрудничества со [смежниками](contact.md).

Teamcenter можно использовать для создания единой базы данных, процессов и изделий, получаемых из различных систем. Сотрудники получают возможность использовать этот ресурс для оперативного доступа к информации, необходимой для выполнения поставленных задач. Система обеспечивает совместную работу в распределённой среде: с её помощью удаленные группы специалистов устанавливают контакты, общаются и обмениваются информацией в режиме реального времени. Благодаря наличию открытого и функционального интерфейса можно интегрировать функции Teamcenter с уже имеющимися процессами.

Teamcenter основан на гибкой, четырёхуровневой сервис‑ориентированной архитектуре (SOA) и эффективно применяется как в малом бизнесе, так и крупнейшими мировыми компаниями. На базе Teamcenter были разработаны специализированные решения, адаптированные для различных отраслей — автомобильной, авиационной, космической и оборонной промышленности, высоких технологий и электроники, химической промышленности и фармацевтики, производству одежды и других отраслей.

**Функциональные возможности.**  
Решения Teamcenter по управлению данными на различных этапах жизненного цикла изделия для различных отраслей промышленности охватывают следующие области:

1. Встроенная визуализация
1. Отчёты и аналитика
1. Платформа управления знаниями предприятия
1. Сервисы расширения платформы
1. Средства совместной работы
1. Управление контентом и документами
1. Управление поставщиками
1. Управление проектами
1. Управление процессами проектирования
1. Управление процессами технологической подготовки производства
1. Управление расчётными данными
1. Управление рецептурой, упаковкой и брендами
1. Управление соответствием
1. Управление составом изделия
1. Управление требованиями
1. Управление электромеханическими данными
1. Эксплуатация, сервисное обслуживание и ремонт

**Применение.**

1. Пользователями Teamcenter являются: Boeing, Lockheed Martin Aeronautics, Snecma, Nissan, Hendrick Motorsports, 「Шанхайская автомобилестроительная корпорация」 (SAIC), Volkswagen и Audi, Royal Schelde Naval Shipyards и другие.
1. Среди российских заказчиков — компания 「Гражданские самолёты Сухого」, ОКБ Сухого Объединенная авиастроительная корпорация, ПКО 「Теплообменник」, ИТ ММПП 「Салют」, 「Уральский оптико‑механический завод」, корпорация 「Иркут」, 「Тихвинский вагоностроительный завод」, ОАО 「КАМАЗ」 и другие.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Software](soft.md)】**<br> [ASP](asp.md) ~~ [Blender](blender.md) ~~ [C](plang.md) ~~ [Cosmographia](cosmographia.md) ~~ [DOORS](doors.md) ~~ [DWG](cad_f.md) ~~ [GIMP](gimp.md) ~~ [Git](git.md) ~~ [IGES](cad_f.md) ~~ [ISIS](isis.md) ~~ [JT](cad_f.md) ~~ [NGT](neogeography_toolkit.md) ~~ [NX](nx.md) ~~ [Octave](gnu_octave.md) ~~ [OS](os.md) ~~ [PDF](pdf.md) ~~ [Python](plang.md) ~~ [R](plang.md) ~~ [SPICE](spice.md) ~~ [STEP](cad_f.md) ~~ [STL](stk.md) ~~ [SVG](cad_f.md) ~~ [Syncthing](syncthing.md) ~~ [SysML](sysml.md) ~~ [Teamcenter](teamcenter.md) ~~ [Valispace](valispace.md) ~~ [Система управления версиями](vcs.md) ~~ [ХРИП](adra.md)|
|**【[Control](Control.md)】**<br> [Ad hoc](ad_hoc.md) ~~ [Business travel](business_travel.md) ~~ [Chief designers council](cocd.md) ~~ [CML](trl.md) ~~ [Competence](competence.md) ~~ [Confident](confident.md) ~~ [Consp.theory](consp_theory.md) ~~ [Control sys. (CS)](cs.md) ~~ [Coordinate system](coord_sys.md) ~~ [Curator](curator.md) ~~ [Designer’s supervision](des_spv.md) ~~ [E‑sig](esig.md) ~~ [Engineer](se.md) ~~ [Errand](errand.md) ~~ [Federal law](fed_law.md) ~~ [Federal TP](fed_tp.md) ~~ [Federal SP](fed_sp.md) ~~ [GNC](gnc.md) ~~ [Gravity assist](gravass.md) ~~ [Industrial archaeology](ind_arch.md) ~~ [Instruction](instruction.md) ~~ [Lean manuf.](lean_man.md) ~~ [Lifetime](lifetime.md) ~~ [Manager](manager.md) ~~ [MBSE](se.md) ~~ [Meeting](meeting.md) ~~ [MCC](sc.md) ~~ [MIC](mic.md) ~~ [MML](mml.md) ~~ [MoU](contract.md) ~~ [Nav. & ballistics (NB)](nnb.md) ~~ [Nonprofit org.](nonprof_org.md) ~~ [NX](nx.md) ~~ [Oberth effect](oberth_eff.md) ~~ [Org.structure](orgstruct.md) ~~ [Outcomes commission](outccom.md) ~~ [Patent](patent.md) ~~ [Peter prin.](peter_principle.md) ~~ [Plan](plan.md) ~~ [PMBok](pmbok.md) ~~ [Quorum](quorum.md) ~~ [R&D management](mgmt.md) ~~ [R&D support](rnd_support.md) ~~ [Recursion](recurs.md) ~~ [Schulze_method](schulze_method.md) ~~ [Sci'N'Tech activities](st_act.md) ~~ [Sci'N'Tech council](satc.md) ~~ [Single-window system](sw_sys.md) ~~ [Situ.leadership](situ_leadership.md) ~~ [Skunk works](se.md) ~~ [State arm. plan](plan_sa.md) ~~ [Swamp](swamp.md) ~~ [Teamcenter](teamcenter.md) ~~ [Tennis racket theorem](tr_theorem.md) ~~ [TRIZ](triz.md) ~~ [TRL](trl.md) ~~ [V‑model](v_model.md) ~~ [Veto](veto.md) ~~ [Workflow](workflow.md) ~~ [Workgroup](wg.md)|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Teamcenter>
1. <http://www.plm.automation.siemens.com/ru_ru>


## The End

end of file
