# APSI
> 2022.01.08 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/a/apsi_logo1t.webp)](f/c/a/apsi_logo1.webp)|<question@apsi.co.kr>, +82(22)026-7777, Fax +82(22)026-7772;<br> *Flr. 9, Unit 2, 98, Gasan digital 2-ro, Geumcheon-gu, Seoul, Republic of Korea*<br> <https://www.apsi.co.kr>|
|:--|:--|
|**Business**|CubeSats R&D, EPS, space electronics, testing|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO, Founder — Jang-su Ryoo<br> ・CTO — Dae-il Oh|

**Asia Pacific Satellite Inc (APSI)** is a South Korean company aimed for satellite communication development, manufacturing, & sales business. Founded 2000.05.02.


**Major enterprises**

| | |
|:--|:--|
|**Satellite<br> Communication<br> Field**|・Development & production of satellite phones<br> ・Development of Baseband Modems (satellite phone, 5G ground/satellite, 6G ground/satellite)<br> ・Satellite Communication Service (facility based telecommunications business entity)<br> ・Development & production of TETRA Modems<br> ・Other satellite terminals|
|**Space<br> Technology<br> Field**|・Standard On-Board Computer (SOBC) for low earth orbit/geostationary orbit satellites<br> ・Space Data Recorder (SDR) for Observation Satellites<br> ・Data link subsystem (DLS)<br> ・Ultra-small Satellite Platform & Development Environment<br> ・Ultra-small satellite avionics<br> ・Electrical Propulsion System (EPS)<br> ・Electrical Ground Support Equipment (EGSE)<br> ・Satellite test service|

<p style="page-break-after:always"> </p>

## The End

end of file
