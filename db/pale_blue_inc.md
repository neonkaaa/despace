# Pale Blue Inc.
> 2020.07.18 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/p/pale_blue_inc_logo1t.webp)](f/c/p/pale_blue_inc_logo1.webp)|<contact@pale-blue.co.jp>, +81(4)7136-4942, Fax …;<br> *Room No. 610, Tokatsu Techno Plaza, 5-4-6 Kashiwanoha, Kashiwa-shi Chiba, Japan 277-0882*<br> <https://pale-blue.co.jp> ~~ [FB ⎆](https://www.facebook.com/PaleBlueInc) ~~ [LI ⎆](https://www.linkedin.com/company/pale-blue-inc) ~~ [X ⎆](https://twitter.com/PaleBlue_Inc)|
|:--|:--|
|**Business**|Consulting of Spacecraft Design & Integration. [Propulsion Systems](ps.md). [Rent Test Equipments](test.md)|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO, Founder — Jun Asakawa<br> ・CTO, Founder — Hiroyuki Koizumi|

**Pale Blue Inc.** is a Japanese company which mission is to provide the mobility for sustainable space development.

Pale Blue was founded in 2020 by reseachers in [The University of Tokyo](tokyo_univ.md) who wanted to prove that science & technology maximizes human well‑being. Drives small‑satellite business by using our small propulsion technology.



## The End

end of file
