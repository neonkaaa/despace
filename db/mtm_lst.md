# Magnetometer (a list)
> 2019.07.31 [🚀](../../index/index.md) [despace](index.md) → [Sensor](sensor.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

A list of [Magnetometers](sensor.md).

## Current



### SX-MAGWR (RU)

**SX‑MAGWR** — [магнитометр](sensor.md) — прибор для определения ориентации КА путём измерения характеристик магнитного поля Земли.  
Разработчик [Спутникс](sputnix.md). Разработано в <mark>TBD</mark> году …

|**Characteristics**|**(SX‑MAG‑03,<br> 3‑осевой<br> магнитометр)**|**(SX‑WR‑01,<br> 1‑осевой ДУС)**|**(ДУС SX‑WR‑03,<br> 3‑осевой)**|**(SX‑MAGWR‑01,<br> 3‑осевой ДУС и<br> 3‑осевой<br> магнитометр)**|
|:--|:--|:--|:--|:--|
|Composition| |3‑x Магнитометр|1‑x ДУС|3‑x ДУС|3‑x ДУС<br> 3‑x Магнитометр|
|Consumption, W|0.6|0.6|1.2|1.5|
|Dimensions, ㎜|34 × 38 × 25|34 × 38 × 25|34 × 38 × 66|34 × 38 × 66|
|[Interfaces](interface.md)|CAN2B или SpaceWire|CAN2B или SpaceWire|CAN2B или SpaceWire|CAN2B или SpaceWire|
|[Lifetime](lifetime.md), h(y)|… / …|… / …|… / …|… / …|
|Mass, ㎏|0.06|0.06|0.09|0.1|
|[Overload](vibration.md), Grms| | | | |
|[Radiation](ion_rad.md), ㏉(㎭)| | | | |
|[Reliability](qm.md)| | | | |
|[Thermal](tcs.md), ℃|–40 ‑ +60 ℃|–40 ‑ +60 ℃|–40 ‑ +60 ℃|–40 ‑ +60 ℃|
|[TRL](trl.md)| | | | |
|[Voltage](sps.md), V|5 и 12|5 и 12|5 и 12|5 и 12|
|**【Specific】**|~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |
|Angular velocity: measurement resolution, °/s|—|0.0005|0.0005|0.0005|
|Angular velocity: measuring range, °/s|—|± 250|± 250|± 250|
|Angular velocity: number of measuring axes|—|1|3|3|
|Angular velocity: random deviation (noise), °/s|—|± 0.005|± 0.005|± 0.005|
|Mag. field: measurement discreteness, nT|6.67|—|—|6.67|
|Mag. field: measuring range, nT|± 200 000|—|—|± 200 000|
|Mag. field: random deviation (noise), nT| ± 100|—|—|± 100|
|Transmitted telemetry|<small>Проекции вектора магнитного поля, проекции вектора угловой скорости, температура каждого измерителя.</small>|<small>Проекции вектора магнитного поля, проекции вектора угловой скорости, температура каждого измерителя.</small>|<small>Проекции вектора магнитного поля, проекции вектора угловой скорости, температура каждого измерителя.</small>|<small>Проекции вектора магнитного поля, проекции вектора угловой скорости, температура каждого измерителя.</small>|
|[![](f/sensor/s/sx_magwr_pic1t.webp)](f/sensor/s/sx_magwr_pic1.webp)| | | | |

**Notes:**

1. [Чертёж и основные характеристики ❐](f/sensor/s/sx_magwr_sputnix_ru.pdf)
1. **Applicability:** …



## Archive



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**`Магнитометр:`**<br> … <br>~ ~ ~ ~ ~<br> **РФ:** [SX-MAGWR](mtm_lst.md) (100)|

1. Docs: …
1. <…>


## The End

end of file
