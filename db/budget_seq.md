# Секвестр
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → **[ТЭО](fs.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Секвестр** — русскоязычный термин. **Budget sequestration** — англоязычный эквивалент.</small>

**Секве́стр** *(от лат. sequestro — ставлю вне, отделяю)*:

1. Секвестр (юриспруденция):
   1. Запрет или ограничение, устанавливаемые органами государственной власти на использование или распоряжение каким‑либо имуществом.
   1. Передача делимого имущества третьему лицу (управляющему или хранителю) в целях последующей передачи выигравшему судебный процесс лицу. Различают добровольный, по волеизъявлению сторон процесса, и принудительный, по решению суда, секвестр.
1. Секвестр (медицина) — омертвевший участок ткани, отделяющийся от здоровой. Секвестры удаляются самостоятельно, либо извлекаются при оперативном вмешательстве (секвестротомия).
1. Секвестр (экономика) — сокращение расходов при исполнении отдельных статей или всего государственного бюджета. Обусловливается разными причинами: просчётами при составлении бюджета, возникновением форс‑мажорных обстоятельств и др.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**`Технико‑экономическое обоснование (ТЭО):`**<br> [NICM](nicm.md) ~~ [Невозвратные затраты](sunk_cost.md) ~~ [Номинал](nominal.md) ~~ [Оценка стоимости работ на НПОЛ](lav.md) ~~ [Секвестр](budget_seq.md) ~~ [Стоимость аппарата в граммах](sc_price.md)|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Sequestration>


## The End

end of file
