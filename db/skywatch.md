# SkyWatch
> 2019.08.15 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/s/skywatch_logo1t.webp)](f/c/s/skywatch_logo1.webp)|<info@skywatch.com>, +1(833)711-2090 , Fax …;<br> *14 Erb St. Westm, Waterloo, ON, N2L1S7, Canada*<br> <https://www.skywatch.com>|
|:--|:--|
|**Business**|Providing commercial satellite imagery|
|**Mission**|To make satellite data accessible to the world|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**SkyWatch** provides a digital infrastructure for the distribution of Earth observation data & derived intelligence. EarthCache™ is a single access point to data & advanced processing algorithms for application developers.

**SkyWatch** is on a mission to make satellite data accessible to the world. Every day, trillions of pixels are captured by satellites orbiting our planet. With new applications for this data being discovered every week, the demand for this imagery is increasing across many industries. However, the current process for purchasing satellite imagery is tedious & time‑consuming. We experienced this problem 1st hand and, after weeks of trying to find the right data, we decided to come up with our own solution.



## The End

end of file
