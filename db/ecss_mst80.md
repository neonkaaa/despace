# ECSS‑M‑ST‑80C (31‑Jul‑2008)
> 2008.07.31 [🚀](../../index/index.md) [despace](index.md) → [Doc](doc.md), [ECSS](ecss.md), [SC](sc.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

## 1. Introduction & Scope

**「Space project management. Risk management.」**

This Standard is one of the series of ECSS Standards intended to be applied together for the management, engineering & product assurance in space projects & applications. ECSS is a cooperative effort of the European Space Agency, national space agencies & European industry associations for the purpose of developing & maintaining common standards. Requirements in this Standard are defined in terms of what shall be accomplished, rather than in terms of how to organize & perform the necessary work. This allows existing organizational structures & methods to be applied where they are effective, & for the structures & methods to evolve as necessary without rewriting the standards. This Standard has been prepared by the ECSS‑M‑ST‑80 Working Group, reviewed by the ECSS Executive Secretariat & approved by the ECSS Technical Authority.

ECSS does not provide any warranty whatsoever, whether expressed, implied, or statutory, including, but not limited to, any warranty of merchantability or fitness for a particular purpose or any warranty that the contents of the item are error‑free. In no respect shall ECSS incur any liability for any damages, including, but not limited to, direct, indirect, special, or consequential damages arising out of, resulting from, or in any way connected to the use of this Standard, whether or not based upon warranty, business agreement, tort, or otherwise; whether or not injury was sustained by persons or property or otherwise; & whether or not loss was sustained from, or arose out of, the results of, the item, or any services that may be provided by ECSS.

Risks are a threat to project success because they have negative effects on the project cost, schedule & technical performance, but appropriate practices of controlling risks can also present new opportunities with positive impact.

The objective of project risk management is to identify, assess, reduce, accept, & control space project risks in a systematic, proactive, comprehensive & cost effective manner, taking into account the project’s technical & programmatic constraints. Risk is considered tradable against the conventional known project resources within the management, programmatic (e.g. cost, schedule) & technical (e.g. mass, power, dependability, safety) domains. The overall risk management in a project is an iterative process throughout the project life cycle, with iterations being determined by the project progress through the different project phases, & by changes to a given project baseline influencing project resources.

Risk management is implemented at each level of the customer‑supplier network.

Known project practices for dealing with project risks, such as system & engineering analyses, analyses of safety, critical items, dependability, critical path, & cost, are an integral part of project risk management. Ranking of risks according to their criticality for project success, allowing management attention to be directed to the essential issues, is a major objective of risk management.

The project actors agree on the extent of the risk management to be implemented in a given project depending on the project definition & characterization.

This Standard defines the principles & requirements for integrated risk management on a space project; it explains what is needed to implement a project‑integrated risk management policy by any project actor, at any level (i.e. customer, first level supplier, or lower level suppliers).

This Standard contains a summary of the general risk management process, which is subdivided into 4 basic steps & 9 tasks.

The risk management process requires information exchange among all project domains, & provides visibility over risks, with a ranking according to their criticality for the project; these risks are monitored & controlled according to the rules defined for the domains to which they belong. The fields of application of this Standard are all the activities of all the space project phases. A definition of project phasing is given in [ECSS‑M‑ST‑10](ecss_mst10.md). This standard may be tailored for the specific characteristics & constraints of a space project in conformance with [ECSS‑S‑ST‑00](ecss_sst00.md).



## 2. Normative references & Bibliography

The following normative documents contain provisions which, through reference in this text, constitute provisions of this ECSS Standard. For dated references, subsequent amendments to, or revisions of any of these publications do not apply. However, parties to agreements based on this ECSS Standard are encouraged to investigate the possibility of applying the most recent editions of the normative documents indicated below. For undated references the latest edition of the publication referred to applies.

1. [ECSS‑M‑ST‑10](ecss_mst10.md) — Space project management — Project planning & implementation
2. [ECSS‑S‑ST‑00‑01](ecss_sst0001.md) — ECSS system — Glossary of terms

**Bibliography:**

1. ECSS‑E‑ST‑10 — Space engineering — System engineering general requirements
2. ECSS‑E‑ST‑20 — Space engineering — Electrical & electronic
3. ECSS‑E‑ST‑40 — Space engineering — Software general requirements
4. ECSS‑E‑ST‑50 — Space engineering — Communications
5. ECSS‑E‑ST‑70 — Space engineering — Ground systems & operations
6. ECSS‑M‑ST‑40 — Space project management — Configuration & information management
7. ECSS‑M‑ST‑60 — Space project management — Cost & schedule management
8. ECSS‑M‑ST‑70 — Space project management — Integrated logistic support
9. ECSS‑Q‑ST‑10 — Space product assurance — Product assurance management
10. ECSS‑Q‑ST‑20 — Space product assurance — Quality assurance
11. ECSS‑Q‑ST‑30 — Space product assurance — Dependability
12. ECSS‑Q‑ST‑40 — Space product assurance — Safety
13. ECSS‑Q‑ST‑60 — Space product assurance — Electrical, electronic & electromechanical (EEE) components
14. ECSS‑Q‑ST‑70 — Space product assurance — Materials, mechanical parts & processes
15. ECSS‑Q‑ST‑80 — Space product assurance — Software product assurance
16. ECSS‑S‑ST‑00 — ECSS system — Description, implementation & general requirements



## 3. Terms, definitions & abbreviated terms

**3.1. Terms from other standards.** For the purpose of this Standard, the terms & definitions from ECSS‑ST‑00‑01 apply:

1. risk
2. residual risk
3. risk management
4. risk management policy

**3.2. Terms specific to the present standard**

1. **acceptance of (risk)** — decision to cope with consequences, should a risk scenario materialize. (1) A risk can be accepted when its magnitude is less than a given threshold, defined in the risk management policy. (2) In the context of risk management, acceptance can mean that even though a risk is not eliminated, its existence & magnitude are acknowledged & tolerated.
2. **(risk) communication** — all information & data necessary for risk management addressed to a decision‑maker & to relevant actors within the project hierarchy
3. **(risk) index** — score used to measure the magnitude of the risk; it is a combination of the likelihood of occurrence & the severity of consequence, where scores are used to measure likelihood & severity
4. **individual (risk)** — risk identified, assessed, & mitigated as a distinct risk items in a project
5. **(risk) management process** — consists of all the project activities related to the identification, assessment, reduction, acceptance, & feedback of risks
6. **overall (risk)** — risk resulting from the assessment of the combination of individual risks & their impact on each other, in the context of the whole project. Overall risk can be expressed as a combination of qualitative & quantitative assessment
7. **(risk) reduction** — implementation of measures that leads to reduction of the likelihood or severity of risk. Preventive measures aim at eliminating the cause of a problem situation, & mitigation measures aim at preventing the propagation of the cause to the consequence or reducing the severity of the consequence or the likelihood of the occurrence
8. **resolved (risk)** — risk that has been rendered acceptable
9. **(risk) scenario** — sequence or combination of events leading from the initial cause to the unwanted consequence. The cause can be a single event or something activating a dormant problem
10. **(risk) trend** — evolution of risks throughout the life cycle of a project
11. **unresolved (risk)** — risk for which risk reduction attempts are not feasible, cannot be verified, or have proved unsuccessful: a risk remaining unacceptable

**3.3. Abbreviated terms.** For the purpose of this standard, the abbreviated terms of ECSS‑S‑ST‑00‑01 & the following apply:

1. IEC — International Electrotechnical Commission



## 4. Principles of risk management

**4.1. Risk management concept**

Risk management is a systematic & iterative process for optimizing resources in accordance with the project’s risk management policy. It is integrated through defined roles & responsibilities into the day‑to‑day activities in all project domains & at all project levels. Risk management assists managers & engineers by including risk aspects in management & engineering practices & judgements throughout the project life cycle, including the preparation of project requirements documents. It is performed in an integrated, holistic way, maximizing the overall benefits in areas such as:

1. design, manufacturing, testing, operation, maintenance, & disposal, together with their interfaces;
2. control over risk consequences;
3. management, cost, & schedule.

**4.2. Risk management process**

The entire spectrum of risks is assessed. Trade‑offs are made among different, & often competing, goals. Undesired events are assessed for their severity & likelihood of occurrence. The assessments of the alternatives for mitigating the risks are iterated, & the resulting measurements of performance & risk trend are used to optimize the tradable resources.

Within the risk management process, available risk information is produced & structured, facilitating risk communication & management decision making. The results of risk assessment & reduction & the residual risks are communicated to the project team for information & follow‑up.

**4.3. Risk management implementation in a project**

1. Risk management requires corporate commitment in each actor’s organization & the establishment of clear lines of responsibility & accountability from the top corporate level downwards. Project management has the overall responsibility for the implementation of risk management, ensuring an integrated, coherent approach for all project domains.
1. Independent validation of data ensures the objectiveness of risk assessment, performed as part of the risk management process.
1. Risk management is a continuous, iterative process. It constitutes an integral part of normal project activity & is embedded within the existing management processes. It utilizes the existing elements of the project management processes to the maximum possible extent.

**4.4. Risk management documentation**

The risk management process is documented to ensure that the risk management policies (see Ann.A) are well established, understood, implemented & maintained, & that they are traceable to the origin & rationale of all risk‑related decisions made during the life of the project.

The risk management documentation includes:

1. **Risk Management Policy**, which defines the organization’s attitude towards risk management, together with the project specific categorization of risk management, and provides a high‑level outline for the implementation of the risk management process
2. **Risk Management Plan** describing the implementation of the risk management process (see Ann.B)
3. **Risk Assessment Report** for communicating the identified & assessed risks as well as the subsequent follow‑up actions & their results (see Ann.C)


## 5. The risk management process

### 5.1. Overview of the risk management process

The iterative four‑step risk management process of a project is illustrated in Fig.5‑1. The tasks to be performed within each of these steps are shown in Fig.5‑2.

Step 1 comprises the establishment of the **risk management policy (Task 1)** & **risk management plan (Task 2)** in coordination with other project disciplines, such as system engineering, product assurance, production, & operations, to ensure coherent approach to risk management across the programme/project. The risk management process includes full coordination between the disciplines of the programme/project. E.g. System Engineering coordination, all engineering disciplines.

Product Assurance coordination, Quality Assurance, Safety & Dependability disciplines.

Management is responsible for overall coordination of all disciplines, including administration of business agreements & project control.

These tasks (1 & 2) are performed at the beginning of a project. The implementation of the risk management process consists of a number of 「risk management cycles」 over the project duration comprising the Steps 2 ‑ 4, subdivided into the seven Tasks 3 ‑ 9.

The period designated in the illustration with 「Risk management process」 comprises all the project phases of the project concerned. The frequency & project events at which cycles are required in a project (only three are shown in Fig.5‑1 for illustration purposes) depend on the needs & complexity of the project, & need to be defined during Step 1. Unforeseen cycles are required when changes to, for example, the schedule, technologies, techniques, & performance of the project baseline occur.

Risks at any stage of the project are controlled as part of the project management activities.

【**Fig.5‑1:** The steps & cycles in the risk management process】  
![](f/doc/ecss/ecss_mst80/f5_1.webp)

【**Fig.5‑2:** The tasks associated with the steps of the risk management process within the risk management cycle】  
![](f/doc/ecss/ecss_mst80/f5_2.webp)



### 5.2. Risk management steps & tasks

#### 5.2.1. Step 1: Define risk management implementation requirements

**5.2.1.1 Purpose.** To initiate the risk management process by defining the project risk management policy & preparing the project risk management plan.

**5.2.1.2 Task 1: Define the risk management policy.** Activities:

1. Identification of the set of resources with impact on risks.
2. Identification of the project goals & resource constraints.
3. Description of the project strategy for dealing with risks, such as the definition of margins & the apportionment of risk between customer & supplier.
4. Definition of scheme for ranking the risk goals according to the requirements of the project.
5. Establishment of scoring schemes for the severity of consequences & likelihood of occurrence for the relevant tradable resources as shown in the examples given in Fig.5‑3 & Fig.5‑4. In the examples, five categories are used for illustration only; more or fewer categories or designations are also possible.
6. Establishment of a risk index scheme to denote the magnitudes of the risks of the various risk scenarios as shown, for example in Fig.5‑5. (1) Establishment of scoring & risk index schemas is performed with the full coordination between the different project disciplines to ensure complete & consistent interpretation. (2) In the example, risk magnitude categorization (「Red」, 「Yellow」, 「Green」) is used for illustration only. Different designations are also possible
7. Establishment of criteria to determine the actions to be taken on risks of various risk magnitudes & the associated risk decision levels in the project structure (as in the example in Fig.5‑6). In the example, risk magnitude designation, acceptability, & proposed actions are used for illustration only. Project‑specific policy definitions can be different.
8. Definition of risk acceptance criteria for individual risks. The acceptability of likelihood of occurrence & severity of consequence are both programme dependent. For example, when a programme is advancing new research, technology development or management, a high probability of a consequence that quickly increase the cost can be acceptable.
9. Establishment of a method for the ranking & comparison of risks.
10. Establishment of a method to measure the overall risk.
11. Establishment of acceptance criteria for the overall risk.
12. Definition of the strategy for monitoring the risks & the formats to be used for communicating risk data to the decision‑makers & all relevant actors in the project hierarchy.
13. Description of the review, decision, & implementation flow within the project concerning all risk management matters.

【**Fig.5‑3:** Example of a severity‑of‑consequence scoring scheme】

|**Score**|**Severity**|**Severity of consequence: impact on (for example) cost**|
|:--|:--|:--|
|5|Catastrophic|Leads to termination of the project|
|4|Critical|Project cost increase > tbd %|
|3|Major|Project cost increase > tbd %|
|2|Significant|Project cost increase < tbd %|
|1|Negligible|Minimal or no impact|

【**Fig.5‑4:** Example of a likelihood scoring scheme】

|**Score**|**Likelihood**|**Likelihood of occurrence**|
|:--|:--|:--|
|E|Maximum|Certain to occur, will occur one or more times per project|
|D|High|Will occur frequently, about 1 in 10 projects|
|C|Medium|Will occur sometimes, about 1 in 100 projects|
|B|Low|Will seldom occur, about 1 in 1000 projects|
|A|Minimum|Will almost never occur, 1 of 10 000 or more projects|

【**Fig.5‑5:** Example of risk index & magnitude scheme】  
![](f/doc/ecss/ecss_mst80/f5_5.webp)

【**Fig.5‑6:** Example of risk magnitude designations & proposed actions for individual risks】  
![](f/doc/ecss/ecss_mst80/f5_6.webp)

**5.2.1.3 Task 2: Prepare the risk management plan.** Typically it contains the following data:

1. Description of the project risk management organization including its role & responsibility.
1. Summary of the risk management policy.
1. The risk management‑related documentation & follow‑up concept.
1. The scope of risk management over the project duration.


#### 5.2.2. Step 2: Identify & assess the risks

**5.2.2.1 Purpose.** To identify each of the risk scenarios, to determine then, based on the outputs from Step 1, the magnitude of the individual risks and, finally, to rank them. Data from all project domains are used (managerial, programmatic, technical). List of examples of possible risk items:

1. **Technical** — Technology maturity; definition status of requirements, internal/external interfaces, payloads, operations; availability of margins, support team, project team; etc.
1. **Cost** — Overall project cost definition status; cost margins; insurance costs; availability of funding, independent cost assessment, industrial offers; human resources aspects; etc.
1. **Schedule** — Procurement planning; availability of planning of phases & activities interfacing with third parties; etc.
1. **Others** — Internal organisational aspects; public image; political constraints; risk sharing between actors; etc.

**5.2.2.2 Task 3: Identify risk scenarios.** Activities:

1. Identification of the risk scenarios, including causes & consequences, according to the risk management policy.
1. Identification of the means of early warning (detection) for the occurrence of an undesirable event, to prevent propagation of consequences.
1. Identification of the project objectives at risk.

**5.2.2.3 Task 4: Assess the risks.** Activities:

1. Determination of the severity of consequences of each risk scenario.
1. Determination of the likelihood of each risk scenario.
1. Determination of the risk index for each risk scenario.
1. Utilisation of available information sources & application of suitable methods to support the assessment process.
1. Determination of the magnitude of risk of each risk scenario.
1. Determination of the overall project risk through an evaluation of identified individual risks, their magnitudes & interactions, & resultant impact on the project.

#### 5.2.3. Step 3: Decide & act

**5.2.3.1 Purpose.** To analyse the acceptability of risks & risk reduction options according to the risk management policy, & to determine the appropriate risk reduction strategy.

**5.2.3.2 Task 5: Decide if the risks may be accepted.** Activities:

1. Application of the risk acceptance criteria to the risks.
1. Identification of acceptable risks, the risks that will be subjected to risk reduction, & determination of the management decision level.
1. For accepted risks proceed directly to Step 4; for unacceptable risks proceed to Task 6.

**5.2.3.3 Task 6: Reduce the risks.** Activities:

1. Determination of preventative & mitigation measures/options for each unacceptable risk.
1. Determination of risk reduction success, failure, & verification criteria.
1. Determination of the risk reduction potential of each measure in conjunction with the optimization of tradable resources.
1. Selection of the best risk reduction measures & decision on priorities for implementation, at the appropriate decision making level in the project according to the risk management plan.
1. Verification of risk reduction.
1. Identification of the risks that cannot be reduced to an acceptable level & presentation to the appropriate management level for disposition.
1. Identification of the reduced risks for which risk reduction cannot be verified.
1. Identification of the risk reduction potential of all risk reduction efforts with respect to the overall risk.
1. Documentation of the successfully reduced risks in a resolved risks list; & the unsuccessfully reduced risks in an unresolved risks list: present the latter to the appropriate management level for disposition.

**5.2.3.4 Task 7: Recommend acceptance.** Activities:

1. Decision options for acceptance of risks.
1. Approval of acceptable & resolved risks.
1. Presentation of unresolved risks for further action.

#### 5.2.4. Step 4: Monitor, communicate, & accept risks

**5.2.4.1 Purpose.** To track, monitor, update, iterate, & communicate, & finally accept the risks.

**5.2.4.2 Task 8: Monitor & communicate the risks.** Activities:

1. Periodical assessment & review of all identified risks & updating of the results after each iteration of the risk management process.
1. Identification of changes to existing risks & initiation of new risk analysis needed in order to decrease uncertainties.
1. Verification of the performance & effect of corresponding risk reduction.
1. Illustration of the risk trend over the project evolution by identifying how the magnitudes of risk have changed over project time.
1. An example of a risk trend for technical risks, which are main risk contributors at the first project milestone, is provided in Figure 5 ‑7. S1, S2 & S3 are three risk scenarios. In the example, the evolution of S1 shows that, in spite of risk reduction efforts, risk trend can worsen before improvement.
1. Communication of the risks & the risk trend to the appropriate level of management.
1. Implementation of an alert system for new risks.

【**Fig.5‑7:** Example of a risk trend】  
![](f/doc/ecss/ecss_mst80/f5_7.webp)


**5.2.4.3 Task 9: Submit risks for acceptance.** Activities:

1. Submission of the risks for formal risk acceptance by the appropriate level of management.
1. Return to Task 6 for risks not accepted.


## 6. Risk management implementation

**6.1. General considerations**

1. Risk management is performed within the normal project management structure, ensuring a systematic risk identification, assessment & follow‑up of risks.
1. Risk management is implemented as a team effort, with tasks & responsibilities being assigned to the functions & individuals within the project organization with the most relevant expertise in the areas concerned by a given risk. It is a collaborative effort of all project actors from the different disciplines.
1. The results of risk management are considered in the routine project management process & in the decisions relative to the baseline evolution.
1. Risk management draws on existing documentation as much as possible.

**6.2. Responsibilities.** The responsibilities for risk management matters within the project organization are described in the risk management plan in accordance with the risk management policy. The following approach applies:

1. The project manager acts as the integrator of the risk management function across all concerned project domains. The project manager has overall responsibility for integrated risk management within a project & reports the results of the risk management task to the next higher level in the customer/supplier chain. The project manager defines who in the project is responsible for the control of the risks in their respective domains, & what their communication, information & reporting lines, & responsibilities are for risk management matters.
1. Each project domain (such as engineering, software, verification, & schedule control) manages the risks emanating from its domain or being assigned to its domain for treatment, under the supervision of the project manager.
1. Risks are formally accepted by the next higher level responsibility within the customer/supplier chain.

**6.3. Project life cycle considerations.** Risk management activities take place during all project phases. The following project activities are concerned with risk management:

1. Project feasibility studies, trades, & analyses (such as design, production, safety, dependability, & operations).
1. The allocation of tasks, manpower, & resources according to the ranking of risks.
1. The evolution of the technical concept through iterative risk assessment.
1. Evaluation of changes for risk impact.
1. The development, qualification, acceptance, & running of the project by using risk assessment as a diagnostic tool & for identifying corrective actions.
1. Assessment of the overall risk status of projects as part of all formal project reviews.

**6.4. Risk visibility & decision making**

1. Management processes & information flow within the project organization ensure a high visibility of the prevailing risk. Risk information is presented to support management decision making, including an alert system for new risks.
1. Action plans are prepared covering all outstanding risk items whose magnitudes are above the level specified in the project risk management policy to increase their visibility, to permit rapid decision making, & to ensure that their status is regularly reported to the relevant management level, & to all actors impacted by the risk consequences.
1. Information about all identified risks & their disposition is kept in a record.

**6.5. Documentation of risk management**

1. Risk management documents are maintained so that each step of the risk management process & the key risk management results & decisions are traceable & defensible.
1. The risk management process draws on the existing project data to the maximum extent possible, but documentation established specifically for risk management includes information on project‑specific risk management policy; objectives & scope; the risk management plan; the identified scenarios; likelihood of events; risk results; risk decisions; records of risk reduction & verification actions; risk trend data; & risk acceptance data.
1. The data emanating from risk management activities are recorded in a risk management database containing all data necessary to manage risks & document the evolution of risks over the whole duration of the project. The database is a living document, & is maintained current. Extracts from the database are presented at project meetings, reviews & milestones as required by the risk management plan. Items to be candidates for 「lessons learned」 are identified. The database is accessible to actors as appropriate. For example: the risk management database should support the efficient & effective management of critical areas of a program/project by:
   1. demonstrating that the risk management process is conducted in accordance with the defined process for project risk management;
   1. providing evidence of a systematic approach to risk identification & assessment;
   1. providing a record of risks;
   1. providing the decision makers with sufficient plans for approval;
   1. facilitating continuing monitoring & review of risk status;
   1. providing traceability;
   1. sharing & communicating required information within project actors;
   1. It includes all technical assessment by the various disciplines, as well as programmatic data.
   1. Example forms for the registration & ranking/logging of risk items are presented in Annex D to this Standard.


## 7. Risk management requirements

### 7.1. General

The requirements in this section are identified. Each identified requirement is composed of the wording of the requirement proper, & accompanied by an explanatory note attached to the general requirement.

### 7.2. Risk management process requirements

1. The basis for risk management shall be the four‑step process & nine tasks illustrated in Fig.5‑1 & Fig.5‑2 of this document. The starting point for risk management shall be the formulation of the risk management policy at the beginning of the project in conformance with the DRD in Annex A. The aim is to establish a risk management policy for the project concerned:
   1. meeting customer requirements;
   1. covering all domains (e.g. management, engineering, performance, schedule, cost);
   1. taking into account the project resources such as margins in schedule, cost, performance, & power;
   1. establishing scoring & risk ranking criteria allowing actions & decisions on the treatment of individual & overall risks;
   1. defining requirements for risk management.
1. A risk management plan shall be established by each supplier in conformance with the DRD in Annex B. The aim is to assemble in a single document all elements necessary to ensure implementation of a risk management commensurate with the project domains, organization, & management, while meeting customer requirements.
1. Risk scenarios shall be identified. The aim is to identify risk scenarios in a structured way for all domains (such as management, engineering, software, test, & operations), using available information sources such as:
   1. previous analysis, lessons learned, & historical data;
   1. expert interviews & experience data;
   1. data extrapolation;
   1. simulations, test data, & models;
   1. detailed safety & dependability analysis (see ECSS‑Q‑ST‑30 & ECSS‑Q‑ST‑40);
   1. analysis of all work breakdown structures & levels;
   1. comparison of goals & plans;
   1. analysis of resources;
   1. analysis of suppliers;
   1. analysis of proposed changes;
   1. test results;
   1. nonconformance reports;
   1. time‑frame consideration;
   1. criticality of technology & availability of back‑up solutions.
1. The risk scenarios shall be assessed. The aim is to facilitate understanding & comparison of the identified risk scenarios by applying the scoring method & scheme defined in the risk management policy.
1. The risk scenarios shall be analysed for their acceptability. (1) In the context of risk management, acceptance can mean that even though a risk is not eliminated, its existence & magnitude are acknowledged & tolerated. (2) The aim is to identify acceptable risks, which are not subject to risk reduction, & unacceptable risks subject to risk reduction.
1. Risks shall be reduced in accordance with the risk management policy. The aim is to reduce unacceptable risks to an acceptable level applying methods aiming at reducing the probabilities or severity of risk scenarios, or reducing the uncertainties in risk data, applying measures such as:
   1. modification of requirements or business agreement;
   1. change of design, baseline, or project structure;
   1. introduction of failure tolerance in accordance with ECSS‑Q‑ST‑documents;
   1. acquisition of additional resources or redirection of resources;
   1. augmentation of test or analysis.
1. The overall risk after consideration of the risk reduction shall be determined. The aim is to gain an understanding of the impact of potential risk mitigation actions.
1. Options for acceptance of resolved, acceptable & overall risks shall be defined where appropriate & presented to the appropriate management level, as defined in the risk management plan, for disposition. The aim is determination & implementation of the appropriate risk resolution options.
1. Unresolved risks shall be presented to the appropriate management level, as defined in the risk management plan, for further disposition. The aim is to arrive at a disposition of unresolved risks at the management level defined in the risk management plan.
1. Residual risks at the end of a risk management cycle shall be submitted to the appropriate management level, as defined in the risk management plan, for acceptance. The aim is formal acceptance of residual risks at the appropriate management level.
1. Risks shall be monitored, communicated, & results shall be displayed in conformance with risk assessment report DRD Annex C.
The aim is to ensure complete & systematic control of the implementation of risk management activities.



### 7.3. Risk management implementation requirements

1. Risk management shall be implemented at each level of the customer‑supplier network.
1. At each level of the customer‑supplier network, risk information received from lower level shall be integrated & assessed for reporting consolidated information. The aim is to provide coherent risk management within the customer‑supplier network.
1. Risk management shall be implemented in a cost‑effective manner, using the existing project organization to the maximum extent. The aim is to establish a coherent risk management structure, integrated into the project organization, with a view to obtaining benefits that outweigh the cost of risk management implementation.
1. The risk management process shall be monitored. The aim is to provide visibility of the risk management process within the organization.
1. Lessons‑learned exercise on the risk management process shall be performed. The aim is continuous improvement of the risk management process.
1. Recognized improvements to the risk management process shall be implemented with the project progress. The aim is to improve the risk management process.



## Annex A (normative) Risk management policy document (RMPD) — DRD

**A.1 DRD identification.** *Requirement identification & source* — ECSS‑M‑ST‑80, requirement 1.1.1.1.1a. *Purpose & objective* — The objective of the **risk management policy document (RMPD)** is to describe the objectives & principles of **risk management (RM)** in the context of the project & to give a high level outline of how we perform RM, & what are the criteria for classification & acceptance of risks.

**A.2 Expected response**

*Special remarks.* The response to this DRD **may be combined with the Risk Management Plan**, ECSS‑M‑ST‑80 Annex B.

*Scope & content:*

1. **Introduction.** The purpose & objective of the RMPD.
1. **Applicable & reference documents.** The applicable & reference documents in support to the generation of the document.
1. **Resources.** Describe the set of project resources that are affected by risk & thereby have an impact on the project objectives.
1. **Project goals & resource constraints.** Describe the project objectives & the resource constraints of the project & name the project’s critical success factors.
1. **Risk management strategy & approach.** ➀ An overview of the RM approach, to include the status of the RM effort, & a description of the project RM strategy consistently deriving from the project’s objectives. ➁ Margins should be stated & if relevant the apportionment of risk between customer & supplier.
1. **Ranking scheme for risk goals.** The definition of a ranking scheme for risk goals according to the requirements of the project.
1. **Scoring schemes.** State the scoring schemes for the severity of consequences & the likelihood of occurrence for the relevant tradable resources, e.g. as proposed in the standard.
1. **Risk index scheme.** The description of the method/tool by which the magnitudes of risks of the various risk scenarios are denoted.
1. **Action criteria.** The criteria to determine the actions to be taken on risks of various magnitudes & the associated risk decision levels in the project structure e.g. as proposed in the standard.
1. **Individual risk acceptance.** Describe the acceptance criteria for individual risks.
1. **Ranking & comparison of risks.** Describe the method for the ranking & comparison of identified risk items where the ranking reflects on the potential direct consequence & impact of the risk to other risk areas or processes.
1. **Overall risk.** The definition of the overall project risk, its measurement method & method of acceptance.
1. **Communication.** ➀ Describe the strategy & the formats for communicating risk data to the decision makers & for monitoring the risks. ➁ An escalation strategy should be described addressing how the information associated with each element of the RM process is determined & made available to the participants in the process.
1. **Risk management process & procedures.** ➀ Describe the RM process to be employed i.e. the review, decision & implementation flow within the project concerning the risk planning, identification, assessment & identification, handling, monitoring & documentation functions. ➁ Provide application guidance for each of the RM functions in the process allowing the project’s RM organization flexibility while ensuring a common & coordinated approach to RM & the coherence of the responsibilities & interfaces within the RM process.



## Annex B (normative) Risk management plan (RMP) — DRD

**B.1 DRD identification.** *Requirement identification & source* — ECSS‑M‑ST‑80, requirement 1.1.1.1.1b. *Purpose & objective* — The objective of the **risk management plan (RMP)** is to provide all elements necessary to ensure that the implementation of **risk management (RM)** commensurate with the project, organization, & management, while meeting customer requirements.

**B.2 Expected response**

*Special remarks.* The response to this DRD **may be combined with the Risk Management Policy** document ECSS‑M‑ST‑80 Annex A, & to the project management plan ECSS‑M‑ST‑10.

*Scope & content:*

1. **Introduction.** The purpose & objective of the RMP.
1. **Applicable & reference documents.** The applicable & reference documents, used to support the generation of the document.
1. **Organization.** Describe the RM organization of the project. List the responsibilities of each of the RM participants.
1. **Risk management policy.** A link to the applicable risk management policy document.
1. **Risk management documentation & follow‑up.** Describe the structure, the rules & the procedures used to document the results of the RM & the follow‑up process.
1. **Project summary.** A brief description of the project, incl. the project management approach.
1. **Description of risk management implementation.** Describe how the RM process is implemented.
1. **Risk identification & assessment.** Describe the identification & assessment process & procedures for examining the critical risk items & domains, & processes to identify & document the associated risks. Summarize the analysis process for each of the risk domain leading to the determination of an overall risk assessment. Include the identification of specific metrics for risk assessment. The RMP may include:
   1. Overview & scope of the identification & assessment process;
   1. Sources of information;
   1. Information to be reported & formats;
   1. Description of how risk information is documented;
   1. Assessment techniques & tools.
1. **Decide & act.** ➀ Describe the risk treatment, which uses the risk assessment report as input. ➁ Specify the criteria of risk acceptance beyond the risk management policy document & mitigation actions that can be used to determine & evaluate various risk handling options. ➂ Identify tools (i.e. name, version & date) that can assist in implementing the risk decision & acting process.
1. **Risk monitoring & communication.** ➀ Describe the operational approach that is followed to track, monitor update iterate & communicate the status of the various risks identified. ➁ Provide criteria for the selection of risks to be reported on, identify the reports to be prepared; specify the format; & assign responsibility for their preparation & the frequency of reporting. ➂ Operational escalation procedures should be stated in this clause ensuring a sufficient alert system & a structured manner of communication.



## Annex C (normative) Risk assessment report (RAR) — DRD

**C.1 DRD identification.** *Requirement identification & source* — ECSS‑M‑ST‑80, requirement 1.1.1.1.1k. *Purpose & objective* — The **risk assessment report (RAR)** is the basis for communicating the identified & assessed risks, as well as the subsequent follow‑up actions & their results.

**C.2 Expected response**

*Special remarks* — None.

*Scope & content:*

1. **Introduction.** The purpose & objective of the RAR, a brief description of what was done during the identification & assessment exercise, & its outcome, identification of organizations that contributed to the preparation of the document.
1. **Applicable & reference documents.** Applicable & reference documents, used to support the generation of the document.
1. **Overview.** Describe what was done during the identification & assessment exercise.
1. **Method of assessment.** Describe how the risks in question were identified, which inputs, method, tool(s) were used, & which people were involved.
1. **Principle.** Describe the basics of the identification & assessment method (e.g. interviewing method), including the justification for the method(s) selected.
1. **Consolidation.** Describe the consolidation approach for the overall risk assessment. Emphasize items of conflict & highlight the decisions that were taken for consideration in the overall assessment.
1. **Assessment.** An appraisal of identified individual risks & the overall project risk.
1. **Comparison with earlier assessments.** Describe results of the follow‑up actions that were taken in comparison with earlier assessment(s).
1. **Conclusions.** Describe the conclusions drawn from the identification & assessment, including any statements for future assessments & follow‑up actions.
1. **Annexes.** The RAR shall contain the following information:
   1. risk register (see ECSS‑M‑ST‑80 Annex D)
   1. ranked risk log
   1. rating scheme
   1. overall risk rating
   1. other analysis



## Annex D (informative) Risk register example & ranked risk log example

[Template file ❐](f/doc/ecss/ecss_mst80/risk_tables.odt)

![](f/doc/ecss/ecss_mst80/risk_table1.webp)

![](f/doc/ecss/ecss_mst80/risk_table2.webp)



## Annex E (informative) Contribution of ECSS Standards to the risk management process

**E.1 General**

Other ECSS Standards contain requirements relevant to the risk management process. The main domains covered in level 1 & 2 standards are listed below.

**E.2 ECSS‑M Standards**

1. ECSS‑M‑ST‑10:
   1. Partitioning the project into technical & manageable elements ensures that items or tasks at risk can be unambiguously identified & allocated, & interfaces contributing to risk identified.
   1. Partitioning the project into phases with reviews at critical project stages provides significant events for reviewing the identified risks & eventually assessing new risk scenarios evolving with the project progress, applying the risk assessment policy adopted for the project.
1. ECSS‑M‑ST‑40: The configuration & information management ensures that all documentation & data of relevance for the risk management process are available & controlled in a systematic manner.
1. ECSS‑M‑ST‑60: Controlling the schedule & cost of the project ensures that deviations with a bearing on identified risks are detected & remedied, or that risks can be re‑assessed in the light of these deviations.
1. ECSS‑M‑ST‑70: The logistics support analysis contributes to risk management by providing the data underlying the assessment of risks influenced by operations, maintenance & disposal of the project hardware & software items.

**E.3 ECSS‑Q Standards**

1. ECSS‑Q‑ST‑10, ECSS‑Q‑ST‑20: The control over product quality ensures that the products affected by risk management are controlled to meet their specifications.
1. ECSS‑Q‑ST‑30, ECSS‑Q‑ST‑40: The dependability & safety related activities apply where risks are linked to dependability & safety.
1. ECSS‑Q‑ST‑60, ECSS‑Q‑ST‑70: The choice of EEE components, material, mechanical parts & processes influence the function & dependability of the design & have therefore an impact on risks.
1. ECSS‑Q‑ST‑80: The correct functioning of software has an influence on risks related to the functioning of the system.

**E.4 ECSS‑E Standards**

1. ECSS‑E‑ST‑10: The engineering & system engineering processes provide a breakdown of engineering activities into manageable & controllable entities, & the demonstration of achievement of the customer’s technical requirements. They are essential for identifying & assessing technical risks, & the verification of requirements with a bearing on risk.
1. ECSS‑E‑ST‑20 to ECSS‑E‑ST‑70: The design of electronic & electrical, mechanical, communications, control & ground support systems & their software as well as of the overall system software has an influence on risks related to the functioning of the system.



## The End

end of file
