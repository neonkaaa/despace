# Руководитель
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [Оргструктура](orgstruct.md), **[Control](control.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Русский** — русскоязычный термин. **Executive / Manager** — англоязычный эквивалент.</small>

**Руководитель** (также **начальник**, **менеджер**) — тот, кто умеет:

1. определить работы;
1. подобрать персонал;
1. распределить работы;
1. создать персоналу комфортные условия для продуктивной работы;
1. не мешать персоналу работать;
1. поддерживать и изменять существующую систему;
1. отстаивать интересы своих сотрудников.

![](f/control/leader_01.webp)



## Менеджер
**Ме́неджер** *(англ. manager, происхождение от manage 「управлять」)* руководи́тель, управля́ющий — начальник, занятый управлением процессами и персоналом на определённом участке предприятия: может быть её владелец, но часто наёмный работник.

Менеджер, как правило, является должностным лицом в организации, в которой работает, и входит в средний и высший руководящий состав предприятия. Определяющим признаком управляющего является наличие подчинённых. Основная функция менеджера — управление, включающее процесс планирования, организации, мотивации и контроля. В зависимости от величины и количества объектов управления различают уровни управления, а, следовательно, и менеджеров.

Подвержены действию [принципа Питера](peter_principle.md) — подняться по карьерной лестнице до уровня своей [некомпетентности](competence.md) и оттуда некомпетентно руководить.

Общепринято выделять менеджеров младшего звена (в мировой практике — операционных управляющих), менеджеров среднего звена и менеджеров высшего звена:

1. **Менеджеры младшего звена** — управленцы начального уровня, находящиеся непосредственно над другими работниками (не управленцами). К ним относятся заведующие отделом (магазинов, учреждений), складом, кафедрой и лабораторией в учебных и научных заведениях, начальники участков и смен, менеджеры продаж, которым подчиняются специалисты по продажам, торговые консультанты, торговые представители, агенты и т.п. Вообще, большая часть менеджеров — это менеджеры младшего, т.е. начального звена. Большинство специалистов становятся управленцами именно в этом качестве.
1. **Менеджеры среднего звена** — управленцы среднего звена, руководящие менеджерами младшего звена. В зависимости от величины и структуры организации может быть несколько уровней таких менеджеров. Менеджерами среднего звена, как правило, являются руководители отделений, подразделений, департаментов, деканы факультетов, начальники цехов, складов, хозяйства.
1. **Менеджеры высшего звена** — топ‑менеджеры, высший административно‑управленческий состав. Это малочисленная группа менеджеров. Даже в самых крупных организациях их всего несколько человек. Типичными должностями здесь является генеральный директор предприятия, управляющий банком, ректор университета, председатель правления, председатель совета директоров.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Control](Control.md)】**<br> [Ad hoc](ad_hoc.md) ~~ [Business travel](business_travel.md) ~~ [Chief designers council](cocd.md) ~~ [CML](trl.md) ~~ [Competence](competence.md) ~~ [Confident](confident.md) ~~ [Consp.theory](consp_theory.md) ~~ [Control sys. (CS)](cs.md) ~~ [Coordinate system](coord_sys.md) ~~ [Curator](curator.md) ~~ [Designer’s supervision](des_spv.md) ~~ [E‑sig](esig.md) ~~ [Engineer](se.md) ~~ [Errand](errand.md) ~~ [Federal law](fed_law.md) ~~ [Federal TP](fed_tp.md) ~~ [Federal SP](fed_sp.md) ~~ [GNC](gnc.md) ~~ [Gravity assist](gravass.md) ~~ [Industrial archaeology](ind_arch.md) ~~ [Instruction](instruction.md) ~~ [Lean manuf.](lean_man.md) ~~ [Lifetime](lifetime.md) ~~ [Manager](manager.md) ~~ [MBSE](se.md) ~~ [Meeting](meeting.md) ~~ [MCC](sc.md) ~~ [MIC](mic.md) ~~ [MML](mml.md) ~~ [MoU](contract.md) ~~ [Nav. & ballistics (NB)](nnb.md) ~~ [Nonprofit org.](nonprof_org.md) ~~ [NX](nx.md) ~~ [Oberth effect](oberth_eff.md) ~~ [Org.structure](orgstruct.md) ~~ [Outcomes commission](outccom.md) ~~ [Patent](patent.md) ~~ [Peter prin.](peter_principle.md) ~~ [Plan](plan.md) ~~ [PMBok](pmbok.md) ~~ [Quorum](quorum.md) ~~ [R&D management](mgmt.md) ~~ [R&D support](rnd_support.md) ~~ [Recursion](recurs.md) ~~ [Schulze_method](schulze_method.md) ~~ [Sci'N'Tech activities](st_act.md) ~~ [Sci'N'Tech council](satc.md) ~~ [Single-window system](sw_sys.md) ~~ [Situ.leadership](situ_leadership.md) ~~ [Skunk works](se.md) ~~ [State arm. plan](plan_sa.md) ~~ [Swamp](swamp.md) ~~ [Teamcenter](teamcenter.md) ~~ [Tennis racket theorem](tr_theorem.md) ~~ [TRIZ](triz.md) ~~ [TRL](trl.md) ~~ [V‑model](v_model.md) ~~ [Veto](veto.md) ~~ [Workflow](workflow.md) ~~ [Workgroup](wg.md)|

1. Docs: …
1. <https://ru.wikipedia.org/wiki/Руководство>
1. <https://en.wikipedia.org/wiki/Management>
1. <http://lurkmore.to/Армейский_способ>
1. [Ситуационное лидерство](situ_leadership.md)
1. 2011.11.29 Хабр: О микроменеджменте (<https://habr.com/ru/post/133601>) — [archived ❐](f/archive/20111129_1.pdf) 2019.02.16
1. 2015.10.13 Хабр: Вертикаль корпоративной власти, трилогия (<https://habr.com/ru/post/295314>) — [archived ❐](f/archive/20151013_1.pdf) 2019.02.16
1. 2017.10.04 Почему заниженные зарплаты и переработки сотрудников неэффективны для стартапов (<https://habr.com/ru/company/wirex/blog/407025>) — [archived ❐](f/archive/20171004_2.pdf) 2019.02.16


## The End

end of file
