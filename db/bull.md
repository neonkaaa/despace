# Bull
> 2023.07.14 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/b/bull_space_logo1t.webp)](f/c/b/bull_space_logo1.webp)|<mark>noemail</mark>, <mark>noworkphone</mark>, Fax …;<br> *3 Chome-1-4 Central, Utsunomiya, Tochigi 320-0806, Japan*<br> <https://bull-space.com/>|
|:--|:--|
|**Business**|**[Space debris prevention](project.md).** Bull, together with JAXA developed a space debris prevention device using EDT (ElectroDynamic Tether), enabling prompt sats’ deorbit after their mission completion (Application of post‑mission disposal, PMD). Equipping sats with it forces them to rapidly lower their altitude & enter Earth’s atmo even if they don’t have power supply, preventing space debris accumulation.|
|**Mission**|Providing inexpensive/concise services in space, by utilizing "(Re --) Entry" technology into planets|
|**Vision**|Making the interplanetary travel "the norm" on and off the Earth|
|**Values**|Valuing a rough and ready stance like a bullfight|
|**[MGMT](mgmt.md)**|・CEO — Kyoji Uto<br> ・CTO — Masaaki Kawamura|

**Bull Co., Ltd** provides small [satellites](sc.md) R&D. Founded in 2022.

<small>**History.** In 2020 Bull has been tackling the problem of space debris by developing a device using a conductive tether (Electro Dynamic Tether, EDT) through a partnership with JAXA since 2019.</small>



## The End

end of file
