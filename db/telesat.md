# Telesat
> 2019.08.13 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/t/telesat_logo1t.webp)](f/c/t/telesat_logo1.webp)|<mark>noemail</mark>, +1(613)748-01-23, Fax +1(613)748-87-12;<br> *160 Elgin Street, Suite 2100, Ottawa, Ontario, K2P 2P7, Canada*<br> <https://www.telesat.com> ~~ [LI ⎆](https://www.linkedin.com/company/telesat.com) ~~ [X ⎆](https://twitter.com/Telesat) ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/Telesat)|
|:--|:--|
|**Business**|Satellite [communications](comms.md) & integration services, satellite operator|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Telesat** (formerly **Telesat Canada**) is a Canadian satellite communications company, the fourth-largest fixed satellite services provider in the world. Founded on 1969.03.02.

The company owns a fleet of satellites (created by other companies), with others under construction, & operates additional satellites for other entities. Telesat carries Canada's two major DBS providers signals: Bell Satellite TV & Shaw Direct, as well as more than 200 of Canada's television channels. Telesat's Anik F2 carries a Ka‑band spot beam payload for satellite Internet access for Wildblue users in the United States & Xplornet users in Canada. The KA band system uses spot beams to manage bandwidth concerns, linking to multiple satellite ground stations connected to the Internet.



## The End

end of file
