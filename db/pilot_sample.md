# Опытный образец
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [Test](test.md), [ЛИ](rnd_e.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Опытный образец изделия (ОО)** — русскоязычный термин. **Pilot sample** — англоязычный эквивалент.</small>

**Опытный образец изделия (опытное изделие)** — образец продукции, изготовленный (доработанный, модернизируемый) по вновь разработанной рабочей конструкторской и технологической документации для испытаний, в том числе [лётных испытаний](rnd_e.md), и проверки на соответствие его требованиям [ТТЗ (ТЗ)](tor.md) в процессе выполнения опытно‑конструкторских работ.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Test](test.md)】**<br> [JTAG](jtag.md) ~~ [Proto fligt model](pfm.md) ~~ [Безэховая камера](ach.md) ~~ [Валидация](vnv.md) ~~ [Класс чистоты](clean_lvl.md) ~~ [КПЭО](ctpr.md) ~~ [Перечень методик испытаний](list_tp.md) ~~ [Программа и методика испытаний](pmot.md) ~~ [Опытный образец](pilot_sample.md) ~~ [Циклограмма](obc.md) ~~ [Штатный образец](flight_unit.md) ~~ [ЭО](test.md) ~~ [Экспериментально‑теоретический метод](etetm.md)|

1. Docs: [РК‑11](const_rk.md), стр.18.
1. <…>


## The End

end of file
