# Orbspace
> 2021.12.03 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/o/orbspace_logo1t.webp)](f/c/o/orbspace_logo1.webp)|<aron.lentsch@orbspace.com>, <mark>nophone</mark>, Fax …;<br> *Startup Park Tsukuba, 2-5-1 Azuma, Tsukuba, Ibaraki, Japan*<br> <https://www.orbspace.com> ~~ [FB ⎆](https://www.facebook.com/orbspace.aerospace) ~~ [IG ⎆](https://www.instagram.com/orbspace.aerospace) ~~ [X ⎆](https://twitter.com/orbspace)|
|:--|:--|
|**Business**|Small reusable space tourism rocket|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・General manager, Founder — Aron Lentsch<br> ・VP, Founder — Zac Kostenko|

**Orbspace** is an aerospace startup company with its roots in the traditional LV industry. We develop 「Infinity」, a small, highly reusable rocket to carry 3 passengers to a 200 ㎞ suborbital space flight based on the principal design goals of simplicity & uncompromised passenger safety. Founded 2020.01.

We do so with professional expertise, using existing technologies, vertically integrated with a small hands‑on team & with a passion for manufacturing called 「Monotsukuri」 in Japanese. This frugal innovation approach compounded with economies of scale by producing Infinity & especially its clustered rocket engines in large numbers will enable a break‑though in cost & make spaceflight available to everyone.

- **Infinity — making suborbital space flights safer & cheaper.** We are developing Infinity, a small reusable rocket vehicle which will fly higher, longer, safer & most importantly for only half the ticket price than our competitors. We are going to build 100’s & 1 000’s of Infinity rockets & the more we build, the cheaper it will get. Our goal is to make spaceflight available for everyone!
- **Simplicity & daily aircraft‑like operations.** Vertical take‑off to a maximum altitude of 200 ㎞ & rocket powered vertical landing, refuelling, checkout & ready for the next flight — all within 24 h. To achieve highest reliability, safety & low operational cost our principal design philosophy is simplicity.
- **Frugal innovation — A small hands‑on team of experts.** 75 years of rocket development has generated a wealth of technologies. Our objective is to use these existing technologies & make them available at low cost. We do this with a small team & an 「all‑in‑house」 vertically integrated approach as well as a passion for machining & manufacturing called 「Monotsukuri」 in Japanese. Our approach is called Frugal innovation. Would you create a world‑class orchestra with amateur musicians? It’s the same for building rockets! We think our professional experience & network in launch vehicle development is an important ingredient for our project & one of our key assets.

<p style="page-break-after:always"> </p>

## The End

end of file
