# DATAFLUCT
> 2021.12.10 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/d/datafluct_logo1t.webp)](f/c/d/datafluct_logo1.webp)|<info@datafluct.com>, +81(3)6822-5590, Fax …;<br> *Daiichi Akatsuki Building, 6th floor, 1-19-9 Dogenzaka, Shibuya-ku, Tokyo 150-0043, Japan*<br> <https://datafluct.com> ~~ [FB ⎆](https://www.facebook.com/datafluct)|
|:--|:--|
|**Business**|Software for sat data analysis|
|**Mission**|Data Science for Every Business|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO — Hayato Kumemura|

**DATAFLUCT Co., Ltd.** is a Japanese company aimed maintain to full‑stack technology related to data utilization, & provide comprehensive support for data collection, analysis, & utilization. Founded 2019.01.29.

We are a data science business development group utilizing DATA LAKE. A data science startup studio that solves social & business issues with the power of data & science. A new impact on society, economy, & technology from buried data, such as the quick launch of multiple SaaS businesses through co‑creation with partners that transcend the boundaries of all industries & industries, & DX support for companies. We will continue to generate business value.

<p style="page-break-after:always"> </p>

## The End

end of file
