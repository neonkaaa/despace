# Space Engine Systems
> 2021.07.12 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/s/space_engine_systems_logo1t.webp)](f/c/s/space_engine_systems_logo1.webp)|<info@spaceenginesystems.com>, +1(780)430-9383 , Fax +1(780)457-1569;<br> *318 14032 - 23rd Ave, Edmonton, Alberta, T6R 3L6 Canada*<br> <https://www.spaceenginesystems.com> ~~ [FB ⎆](https://www.facebook.com/spaceenginesystems) ~~ [LI ⎆](https://www.linkedin.com/company/space-engine-systems) ~~ [X ⎆](https://twitter.com/SpaceEngSystem) ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/Space_Engine_Systems)|
|:--|:--|
|**Business**|SSTO [propulsion](ps.md), pumps, compressors, gear boxes, Permanent Magnet Motors|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Space Engine Systems** is a Canadian aerospace company that is pioneering the next generation of propulsion technologies for aerospace & space. Founded in 2012.

Our ultimate goal is the design of a reusable, single‑stage‑to‑orbit cruise vehicle that is capable of flight up to hypersonic speeds for use in aerospace & space applications. To achieve these targets, we are developing a light, reusable, multi‑fuel propulsion system. We have been working on this technology for more than two decades. Currently, we are entering a phase of aggressive testing to completely test the full engine prototype, which is expected to demonstrate all of the key technologies. In order to carry out this testing, Space Engine Systems has developed a multi‑fuel ground testing facility that is capable of simulating engine inlet temperatures for a variety of flight conditions. When not being used for our application, Space Engine Systems will also consider undertaking subcontract work at this facility for others who may be interested in testing liquid hydrogen, solid fuel, & other fuels for air breathing engines.

While all of us are interested in being part of such an exciting, technical project, we’re here because we have a passion for aerospace & space. We want to remove barriers to accessible supersonic & hypersonic flight, & make affordable space flight a reality.

<p style="page-break-after:always"> </p>

## The End

end of file
