# Space Shift Inc.
> 2022.04.19 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/s/space_shift_logo1t.webp)](f/c/s/space_shift_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *Inspired.Lab, 6th floor, Otemachi Building, 1-6-1 Otemachi, Chiyoda Ward, Tokyo 100-0004 Japan*<br> <http://spcsft.com> ~~ [LI ⎆](https://www.linkedin.com/company/spcsft)|
|:--|:--|
|**Business**|Software for sat data processing, consulting (marketing, software)|
|**Mission**|Unraveling the World with Space & AI|
|**Vision**|Aiming to realizea more‑efficient society|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO, Founder — Naruo Kanemoto|

**Space Shift Inc.** is a Japanese company aimed for development of software for satellite data analysis, business related to the analysis of satellite data, space‑related business consulting. Founded 2009.

Under the theme of 「Unraveling the World with Space & AI」, we at Space Shift are developing software that leverages the cognitive ability of AI (which surpasses that of humans) to analyze data as obtained from EO satellites so as to squeeze out every bit of available information & to automate various processes.

By extracting such levels of information from existing satellite data & by providing more‑accurate software, we can dramatically improve analysis accuracy, grasp production amounts & the total amounts of all things involved, & contribute to the realization of a more‑efficient society.

<p style="page-break-after:always"> </p>

## The End

end of file
