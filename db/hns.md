# Аппаратные и программные средства
> 2019.05.10 [🚀](../../index/index.md) [despace](index.md) → **[OE](sc.md)**, [ННК](sc.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Аппаратные и программные средства** — русскоязычный термин. **Hardware and software** — англоязычный эквивалент.</small>

**Аппаратные и программные средства (АПС)** — совокупность наземных (изредка бортовых) электронных вычислительных машин, программного обеспечения, сетей передачи информации и вспомогательного оборудования.

Также к АПС относится [компьютер (ЦВМ)](obc.md).



## Описание АПС
По факту АПС представляют собой группу серверов со специализированным ПО и каналы связи.

АПС входят в состав [ННК](sc.md).



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**`Бортовая аппаратура (БА):`**<br> [PDD](pdd.md) ~~ [Антенна](antenna.md) ~~ [АПС](hns.md) ~~ [БУ](eas.md) ~~ [ЗУ](ds.md) ~~ [Изделие](unit.md) ~~ [КЛЧ](clean_lvl.md) ~~ [ПЗР](fov.md) ~~ [ПО](soft.md) ~~ [Прототип](prototype.md) ~~ [Радиосвязь](comms.md) ~~ [СКЭ](elmsys.md) ~~ [ССИТД](tsdcs.md) ~~ [СИТ](etedp.md) ~~ [УГТ](trl.md) ~~ [ЭКБ](elc.md) ~~ [EMC](emc.md)|
|**`Наземный научный комплекс (ННК):`**<br> [АПС](hns.md)|

1. Docs: …
1. <…>


## The End

end of file
