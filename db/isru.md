# ISRU
> 2021.10.27 [🚀](../../index/index.md) [despace](index.md) → [](.md) <mark>NOCAT</mark>  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**In‑situ resource utilization (ISRU)** — EN term. **Использование местных ресурсов** — literal RU translation.</small>

**In situ resource utilization (ISRU)** is the practice of collection, processing, storing & use of materials found or manufactured on other astronomical objects (the [Moon](moon.md), [Mars](mars.md), asteroids, etc.) that replace materials that would otherwise be brought from Earth.

ISRU could provide materials for life support, propellants, construction materials, & energy to a SC payloads or space exploration crews. It’s now very common for SC & robotic planetary surface mission to harness the solar radiation found in situ in the form of solar panels. The use of ISRU for material production has not yet been implemented in a space mission, though several field tests in the late 2000s demonstrated various lunar ISRU techniques in a relevant environment.

ISRU has long been considered as a possible avenue for reducing the mass & cost of space exploration architectures, in that it may be a way to drastically reduce the amount of payload that must be launched from Earth in order to explore a given planetary body. According to NASA (<https://www.nasa.gov/centers/ames/research/technology-onepagers/in-situ_resource_Utiliza14.html>), 「in‑situ resource utilization will enable the affordable establishment of extraterrestrial exploration & operations by minimizing the materials carried from Earth」.


**Uses:**

1. **Water.** In the context of ISRU water is most often sought directly as fuel or as feedstock for fuel production. Applications include its use in life support either directly by drinking, for growing food, producing oxygen, or numerous other industrial processes, all of which require a ready supply of water in the environment & the equipment to extract it. Such extraterrestrial water has been discovered in a variety of forms throughout the solar system, & a number of potential water extraction technologies have been investigated. For water that is chemically bound to regolith, solid ice, or some manner of permafrost, sufficient heating can recover the water. However this is not as easy as it appears because ice & permafrost can often be harder than plain rock, necessitating laborious mining operations. Where there is some level of atmosphere, such as on Mars, water can be extracted directly from the air using a simple process such as WAVAR. Another possible source of water is deep aquifers kept warm by Mars’s latent geological heat, which can be tapped to provide both water & geothermal power.
1. **[Rocket propellant](ps.md).** Rocket propellant production has been proposed from the Moon’s surface by processing water ice detected at the poles. The likely difficulties include working at extremely low temperatures & extraction from the regolith. Most schemes electrolyse the water to produce hydrogen & oxygen & cryogenically store them as liquids. This requires large amounts of equipment & power to achieve. Alternatively, it may be possible to heat water in a nuclear or solar thermal rocket, which may be able to deliver a large mass from the Moon to low Earth orbit (LEO) in spite of the much lower specific impulse, for a given amount of equipment.<br> The monopropellant [hydrogen peroxide (H₂O₂)](hydrogen.md) can be made from water on Mars & the Moon.<br> Aluminum as well as other metals has been proposed for use as rocket propellant made using lunar resources, & proposals include reacting the aluminum with water.<br> For Mars, methane propellant can be manufactured via the Sabatier process. SpaceX has suggested building a propellant plant on Mars that would use this process to produce methane (CH₄) & liquid oxygen (O₂) from sub‑surface water ice & atmospheric CO₂.
1. **Solar cell production.** It has long been suggested that solar cells could be produced from the materials present in lunar soil. Silicon, aluminium, & glass, three of the primary materials required for solar cell production, are found in high concentrations in lunar soil & can be utilised to produce solar cells. In fact, the native vacuum on the lunar surface provides an excellent environment for direct vacuum deposition of thin‑film materials for solar cells.<br> Solar arrays produced on the lunar surface can be used to support lunar surface operations as well as satellites off the lunar surface. Solar arrays produced on the lunar surface may prove more cost effective than solar arrays produced & shipped from Earth, but this trade depends heavily on the location of the particular application in question.<br> Another potential application of lunar‑derived solar arrays is providing power to Earth. In its original form, known as the solar power satellite, the proposal was intended as an alternate power source for Earth. Solar cells would be launched into Earth orbit & assembled, with the resultant generated power being transmitted down to Earth via microwave beams. Despite much work on the cost of such a venture, the uncertainty lay in the cost & complexity of fabrication procedures on the lunar surface.
1. **Building materials.** The colonization of planets or moons will require obtaining local building materials, such as regolith. For example, studies employing artificial Mars soil mixed with epoxy resin & tetraethoxysilane, produce high enough values of strength, resistance, & flexibility parameters.<br> Asteroid mining could also involve extraction of metals for construction material in space, which may be more cost‑effective than bringing such material up out of Earth’s deep gravity well, or that of any other large body like the Moon or Mars. Metallic asteroids contain huge amounts of siderophilic metals, including precious metals.



## Mineral separation

**Beneficiation** mechanically separates the grains of ore minerals from the gangue (low value) minerals that surround them in order to produce a concentrate made up of useful minerals.

This separation can be done via the following methods. Unusable minerals, which are often discarded into dams, are called tailing.

1. physically:
   1. acoustics
   1. crushing
   1. electric pulse disaggregation (EPD)
   1. electromagnetic separation
   1. electrostatic separation
   1. gravity separation
   1. magnetic separation
   1. mass difference (requires liquid or gas, e.g. methylene iodide)
   1. spectrometry & robotics
1. chemically:
   1. electro winning
   1. froth flotation
   1. leaching


**Links:**

1. <https://www.steqtech.com/types-of-separation-methods-for-minerals>

### EPD

**Electric pulse disaggregation (EPD)** is a process of breaking rocks into clean composing minerals using high voltage electric discharges. The electric pulse travels through the rock along planes of weakness (e.g. grain boundaries) and creates extensional forces within the rock.

1. <https://www.ndsu.edu/pubweb/~sainieid/PPD/PPD.shtml> *(archived [2022.12.08 ❐](f/archive/20221208_02.pdf))*
1. <https://www.odm.ca/mineral-extraction-division> *(archived [2022.12.08 ❐](f/archive/20221208_01.pdf))*

Larger crystals, or crystals with imperfections (fractures, cleavage planes, inclusions) may break. In general, the most mineral grains are not broken, unless they had imperfections prior to processing. EPD cannot break strong connections (like alloys), and cannot extract a mineral that is fully covered by a solid cover of other mineral.

The most widely used environments to perform EPD are:

1. nitrogen
1. oil
1. water

**Q:** Can an electrical discharge be seen in space or in a vacuum?  
**A:** <https://www.quora.com/Can-an-electrical-discharge-be-seen-in-space-or-in-a-vacuum>  
Well, sort of.  
In air, electrical discharges are visible for two reasons.  
The air is ionized prior to the discharge,and the recombination of air molecules gives off visible light, and
If the discharge is energetic enough, the air is heated to incandescence. In a vacuum, there’s no air (obviously), so there’s nothing to be ionized or heated to incandescence. But electrons can travel across a vacuum, and under the right circumstances, the flow of electric current can create a visible pathway.  
In order to make electrons jump across a vacuum from a cathode to an anode, you need a lot of power. That’s because a hard vacuum is a good insulator. The large amount of energy causes the cathode to heat up, even to the point of incandescence. But that’s just the cathode, not the gap between them. As electrons leave the cathode they are accelerated at a terrific pace until they slam into the anode. The delivered energy causes parts of the anode to vaporize, and the metallic ions are drawn across the gap in the opposite direction as the electrons.  
The interactions between the electrons and ions give rise to a glow known as a vacuum arc. So in a sense, it is possible to create an electric arc across a vacuum. It just takes a huge amount of energy to do so.



## Docs/Links
|**Sections & pages**|
|:--|
|**【[](.md)】**<br> <mark>NOCAT</mark>|

1. Docs: …
1. <https://en.wikipedia.org/wiki/In_situ_resource_utilization> *(archived [2022.12.09 ❐](f/archive/2022.12.09_01.pdf))*
1. <https://www.nasa.gov/isru>


## The End

end of file
