# Зрение
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [Project](project.md), [Радиосвязь](comms.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Зрение** — русскоязычный термин. **View** — англоязычный эквивалент.</small>

**Зрение** — возможность живого существа или машины видеть окружающий мир в определённом [спектре](comms.md).

<mark>TBD</mark>



## Зрение человека


### Дальтонизм
**Дальтони́зм**, цветовая слепота, — наследственная, реже приобретённая, особенность зрения человека и приматов, выражающаяся в сниженной или полной неспособности различать цвета.

|Клинические проявления|
|:--|
|![](f/explore/vision_001.webp)|



## Зрение машины
<mark>TBD</mark>



## Как это можно использовать?:
Ниже приведены практические рекомендации использования вышеуказанных данных.
1. В связи с возможным дальтонизмом для визуализации 3D-моделей и презентаций рекомендуется использовать ограниченный набор цветов.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Project](project.md)】**<br> [Interferometer](interferometer.md) ~~ [NASA open](nasa_open.md) ~~ [NASA STI](nasa_sti.md) ~~ [NIH](nih.md) ~~ [Past, future and everything](pfaeverything.md) ~~ [PSDS](us_psds.md) [MGSC](mgsc.md) ~~ [Raman spectroscopy](spectroscopy.md) ~~ [SC price](sc_price.md) ~~ [SC typical forms](sc.md) ~~ [Spectrometry](spectroscopy.md) ~~ [Tech derivative laws](td_laws.md) ~~ [View](view.md)|
|**【[Communications](comms.md)】**<br> [CCSDS](ccsds.md) ~~ [Антенна](antenna.md) ~~ [АФУ](afdev.md) ~~ [Битрейт](bitrate.md) ~~ [ВОЛП](ofts.md) ~~ [ДНА](дна.md) ~~ [Диапазоны частот](comms.md) ~~ [Зрение](view.md) ~~ [Интерферометр](interferometer.md) ~~ [Информация](info.md) ~~ [КНД](directivity.md) ~~ [Код Рида‑Соломона](rsco.md) ~~ [КПДА](antenna.md) ~~ [КСВ](swr.md) ~~ [КУ](ку.md) ~~ [ЛКС, АОЛС, FSO](fso.md) ~~ [Несущий сигнал](carrwave.md) ~~ [ПНА, ПОНА, ПСНА](devd.md) ~~ [Помехи](emi.md) (EMI, RFI) ~~ [Последняя миля](last_mile.md) ~~ [Регламент радиосвязи](comms.md) ~~ [СИТ](etedp.md) ~~ [Фидер](feeder.md) <br>~ ~ ~ ~ ~<br> **РФ:** [БА КИС](ба_кис.md) (21) ~~ [БРК](brk_lav.md) (12) ~~ [РУ ПНИ](ру_пни.md) () ~~ [HSXBDT](comms_lst.md) (1.8) ~~ [CSXBT](comms_lst.md) (0.38) ~~ [ПРИЗЫВ-3](comms_lst.md) (0.17) *([ПРИЗЫВ-1](comms_lst.md) (0.075))**|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Visual_system>
1. <https://en.wikipedia.org/wiki/Color_blindness>


## The End

end of file
