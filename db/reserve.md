# Резерв
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [SGM](sc.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Резерв** — русскоязычный термин. **Reserve** — англоязычный эквивалент.</small>

**Резерв** (фр. réserve от лат. reservare — сберегать, сохранять):

1. Запас чего‑либо на случай надобности.
1. Источник, откуда черпаются новые средства, силы.
1. Резервирование — метод повышения надёжности систем. См. [Надёжность](qm.md).

В основном  слово 「резерв」 употребляется в отношении массы [КА](sc.md), его [OE](sc.md) и [конструкции](sc.md). При этом имеется два подхода к резервированию массы.

1. **Традиционный**. Масса резервируется в виде процентов от суммарной массы БА и конструкции; иногда включается и масса [ЦА](sc.md). При этом считается:
   1. [НИР](rnd_0.md): 30‑33 %;
   1. [АП](rnd_ap.md): 20‑30 %;
   1. [ЭП](rnd_ep.md): 10‑20 % (7 % — минимум).
1. **Прогрессивный**. Масса резервируется в виде процентов от массы каждой СЧ в отдельности. При этом на этапах [АП](rnd_ap.md) и [ЭП](rnd_ep.md) в зависимости от особенностей СЧ и её [TRL](trl.md) значение резерва массы СЧ колеблется от 0 <sup>(имеющий лётную квалификацию)</sup> до 20 % <sup>(новая разработка, TRL 0‑4)</sup>. Также указывается суммарный резерв КА, полученный в результате сложения резервов СЧ.

Резерв может считаться от:

1. массы КА незаправленного без учёта ЦА — этот вариант выглядит наиболее логичным, так как резерв берётся на массу космического аппарата, который должен доставить некую массу полезной нагрузки;
1. массы КА незаправленного с учётом ЦА.

Также резерв применяется для массы топлива по этапам полёта. Расчёт ведётся для худших условий запуска из диапазона выбранных дат. Применяется только **прогрессивный** подход (см. выше). При этом на этапах [АП](rnd_ap.md) и [ЭП](rnd_ep.md) в зависимости от этапа значения резерва массы топлива колеблется от 0 (перелёт, торможение и т.д.) до 20 % (основной импульс при посадке). Суммарный резерв массы топлива обычно не указывают ввиду малой информативности данной цифры.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Structures, gears, materials (SGM)](sc.md)】**<br> [Гермоконтейнер](гермоконтейнер.md) ~~ [Датчик](sensor.md) ~~ [Задел](margin.md) ~~ [Изделие](unit.md) ~~ [Испарение материалов](matc.md) ~~ [Кавитация](cavitation.md) ~~ [КЗУ](cinu.md) (ВБУ КТ) ~~ [КХГ](cgs.md) ~~ [Контейнеры для транспортировки](ship_contain.md) ~~ [Крейцкопф](crosshead.md) ~~ [Номинал](nominal.md) ~~ [ПУС](lag.md) ~~ [ПНА, ПОНА, ПСНА](devd.md) ~~ [Резерв](reserve.md) ~~ [Слайс](слайс.md) ~~ [ТСП](tsp.md) ~~ [Типичные формы КА](sc.md) ~~ [Толкатель](толкатель.md) ~~ [Унификация](commonality.md)|

1. Docs: …
1. <…>


## The End

end of file
