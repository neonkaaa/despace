# Электронная подпись
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [Doc](doc.md), [Control](control.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Электронная цифровая подпись (ЭЦП)** — русскоязычный термин. **Electronic signature (ESIG), Digital signature (DS)** — англоязычный эквивалент.</small>

**Электро́нная цифровая по́дпись (ЭЦП)**, реже **Электро́нная по́дпись (ЭП)** — реквизит электронного документа, полученный в результате криптографического преобразования информации с использованием закрытого ключа подписи и позволяющий проверить отсутствие искажения информации в эл. документе с момента формирования подписи (целостность), принадлежность подписи владельцу сертификата ключа подписи (авторство), а в случае успешной проверки подтвердить факт подписания электронного документа (неотказуемость).



## Использование
Федеральный закон от 6 апреля 2011 года № 63‑ФЗ 「Об электронной подписи」 вводит определение трёх видов электронных подписей:

1. **Простой электронной подписью** является электронная подпись, которая посредством использования кодов, паролей или иных средств подтверждает факт формирования электронной подписи определённым лицом.
1. **Усиленной неквалифицированной электронной подписью** является электронная подпись, которая:
   1. получена в результате криптографического преобразования информации с использованием ключа электронной подписи;
   1. позволяет определить лицо, подписавшее электронный документ;
   1. позволяет обнаружить факт внесения изменений в электронный документ после момента его подписания;
   1. создается с использованием средств электронной подписи.
1. **Усиленной квалифицированной электронной подписью** является электронная подпись, которая соответствует всем признакам неквалифицированной электронной подписи и следующим дополнительным признакам:
   1. ключ проверки электронной подписи указан в квалифицированном сертификате;
   1. для создания и проверки электронной подписи используются средства электронной подписи, получившие подтверждение соответствия требованиям, установленным в соответствии с № 63‑ФЗ.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Control](Control.md)】**<br> [Ad hoc](ad_hoc.md) ~~ [Business travel](business_travel.md) ~~ [Chief designers council](cocd.md) ~~ [CML](trl.md) ~~ [Competence](competence.md) ~~ [Confident](confident.md) ~~ [Consp.theory](consp_theory.md) ~~ [Control sys. (CS)](cs.md) ~~ [Coordinate system](coord_sys.md) ~~ [Curator](curator.md) ~~ [Designer’s supervision](des_spv.md) ~~ [E‑sig](esig.md) ~~ [Engineer](se.md) ~~ [Errand](errand.md) ~~ [Federal law](fed_law.md) ~~ [Federal TP](fed_tp.md) ~~ [Federal SP](fed_sp.md) ~~ [GNC](gnc.md) ~~ [Gravity assist](gravass.md) ~~ [Industrial archaeology](ind_arch.md) ~~ [Instruction](instruction.md) ~~ [Lean manuf.](lean_man.md) ~~ [Lifetime](lifetime.md) ~~ [Manager](manager.md) ~~ [MBSE](se.md) ~~ [Meeting](meeting.md) ~~ [MCC](sc.md) ~~ [MIC](mic.md) ~~ [MML](mml.md) ~~ [MoU](contract.md) ~~ [Nav. & ballistics (NB)](nnb.md) ~~ [Nonprofit org.](nonprof_org.md) ~~ [NX](nx.md) ~~ [Oberth effect](oberth_eff.md) ~~ [Org.structure](orgstruct.md) ~~ [Outcomes commission](outccom.md) ~~ [Patent](patent.md) ~~ [Peter prin.](peter_principle.md) ~~ [Plan](plan.md) ~~ [PMBok](pmbok.md) ~~ [Quorum](quorum.md) ~~ [R&D management](mgmt.md) ~~ [R&D support](rnd_support.md) ~~ [Recursion](recurs.md) ~~ [Schulze_method](schulze_method.md) ~~ [Sci'N'Tech activities](st_act.md) ~~ [Sci'N'Tech council](satc.md) ~~ [Single-window system](sw_sys.md) ~~ [Situ.leadership](situ_leadership.md) ~~ [Skunk works](se.md) ~~ [State arm. plan](plan_sa.md) ~~ [Swamp](swamp.md) ~~ [Teamcenter](teamcenter.md) ~~ [Tennis racket theorem](tr_theorem.md) ~~ [TRIZ](triz.md) ~~ [TRL](trl.md) ~~ [V‑model](v_model.md) ~~ [Veto](veto.md) ~~ [Workflow](workflow.md) ~~ [Workgroup](wg.md)|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Digital_signature>


## The End

end of file
