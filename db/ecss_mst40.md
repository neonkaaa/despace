# ecss_mst40c

[TOC]

---

## Annex A (normative) Configuration management plan — DRD

**A.1 DRD identification.** *Requirement ID & source* — ECSS‑M‑ST‑40, requirement 1.1.1.1.1a. *Purpose & objective* — the **configuration management plan (CMP)** is to provide all elements necessary to ensure that the implementation of **configuration management (CM)** meets customer requirements & is commensurate with the programme/project, organization, & management structure.

**A.2 Expected response**

*Special remarks.* The response to this DRD **may be combined with the Project Management Plan**, as per ECSS‑M‑ST‑10.

*Scope & content:*

1. **Introduction.** The purpose & objective prompting its preparation.
1. **Applicable & reference documents.** The applicable & reference documents in support of the generation of the document.
1. **Management**
   1. *Organization.* ➀ Describe the organizational context (technical & managerial) within which the prescribed CM activities shall be implemented. ➁ Describe the interface with other CM organizations (customer, supplier) & the CM relationship to other internal organization elements. ➂ Describe the interface for the information management activities.
   1. *Responsibilities.* The allocation of responsibilities & authorities for CM activities to organizations & individuals within the programme/project structure.
   1. *Policies, directives & procedures.* External constraints or requirements, placed on the CMP by other policies, directives, or procedures together with the consequences of applying these to the programme/project.
   1. *Security.* Customer & suppliers shall indicate within the CMP the classification classes to be applied & the relevant documentation set.
1. **Configuration Management**
   1. *General.* Identify all functions & processes (technical & managerial) required for managing the configuration of the programme/project; introduce the following, as a minimum: ➀ configuration identification, ➁ control, ➂ status accounting, ➃ audits & reviews, ➄ interface control, ➅ supplier control.
   1. *Configuration identification*
      1. Identify, name, & describe the documented physical & functional characteristics of the information to be maintained under CM control for the programme/project.
      1. Describe the following, as a minimum: ➀ product tree establishment (System decomposition), ➁ identification of configuration items, i.e. the selection process of the items to be controlled & their definitions as they evolve or are selected, ➂ naming of configuration items, i.e. the specification of the identification system for assigning unique identifiers to each item to be controlled, ➃ configuration baselines establishment & maintenance, ➄ interface identification, ➅ configuration documentation & data release procedures
      1. In case of software configuration, item & where a tool is used to build its baseline, describe the following: ➀ procedure to enter a software configuration item into a baseline, ➁ procedure to configure & establish a baseline, ➂ software products & records to define a baseline, ➃ procedure to approve a baseline, ➄ authority to approve a baseline
      1. In case software libraries are established, identify the following: ➀ library types, ➁ library locations, ➂ media used for each library, ➃ library population mechanism, ➄ number of identical libraries & the mechanism for maintaining parallel contents, ➅ library contents & status of each item included in, ➆ conditions for entering a SCI, including the status of maturity compatible with the contents required for a particular software library type, ➇ provision for protecting libraries from malicious & accidental harm & deterioration, ➈ software recovery procedures, ➉ conditions for retrieving any object of the library, ⑪ library access provisions & procedures
   1. *Configuration control.* Describe the configuration control process & data for implementing changes to the configuration items & identify the records to be used for tracking & documenting the sequence of steps for each change; describe the following, as a minimum: ➀ configuration control board functions, responsibilities, authorities, ➁ processing changes, ➂ change requests, ➃ change proposal, ➄ change evaluation, ➅ change approval, ➆ change implementation, ➇ processing planned configuration departures (deviations), ➈ processing unplanned configuration departures (product nonconformances, waivers).
   1. *Interface control.* Describe the process & data for coordinating changes to the programme/project CM items with changes to interfaces.
   1. *Supplier control.* ➀ For both subcontracted & acquired products (i.e. equipment, software, service) define the process & data to flow down the CM requirements & the programme monitoring methods to control the supplier. ➁ Define the process & data to incorporate the supplier developed items into programme/project CM & to coordinate changes to these items. ➂ Describe how the product is received, tested, & placed under configuration control.
   1. *Configuration status accounting.* Describe the process & data for reporting the status of configuration items. The following minimum info shall be tracked & reported for each CM item: ➀ status of the configuration baselines, ➁ design status of the configuration item, ➂ status of configuration documentation & configuration data sets, ➃ status of approval of changes & deviations & their implementation status, ➄ status of discrepancies & actions arising from technical reviews & configuration verification reviews
   1. *Configuration verification.* ➀ Describe the process & data to verify the current configuration status from which the configuration baselines are established. ➁ Describe verification plans, procedures & schedules. ➂ Identify how the recording, reporting & tracking of action items & incorporation of review recommendations are maintained.
   1. *Audits of CM system.* Describe the process, data & schedule for configuration audits to ensure that the CM of the programme/project is performed. As a minimum, the CMP shall enable that the CM process is properly defined, implemented & controlled, & the CM items reflect the required physical & functional characteristics.
   1. *Technical data management.* Describe the process & data to access & maintain text & CAD files, data & software repositories, & the implementation of any PDM system.
1. **Information/documentation  management**
   1. *Information identification.* ➀ Describe the main information/documentation categories (management information, contractual documentation or engineering data) to be established & used throughout the programme/project life cycle. ➁ Describe methods for information/document identification including versioning. ➂ List the codes for companies, information types, models, subsystems, etc. which are applied specifically in the identification method or are in general use during project execution. ➃ Identify the metadata structures of the main information categories.
   1. *Data formats.* ➀ Define for the various information categories the data formats to be used for content creation & update, distribution, archiving. ➁ Specify which format takes precedence in case a format conversion is applied.
   1. *Processes.* ➀ Describe the actors involved in, as well as the method & processes for, creating, collecting, reviewing, approving, storing, delivering & archiving information items. ➁ Describe the handling of legacy documentation & “off‑the‑shelf” documentation. ➂ Define the information retention period.
   1. *Information systems.* List the project information systems to be used for creating, reviewing, storing, delivering & archiving the main information categories (e.g.: ABC for schedule, & XYZ for engineering DB).
   1. *Delivery methods.* Describe the methods used to deliver TDPs.
   1. *Digital signature.* Define the procedures, methods & rules applicable to digital signatures. This comprises information about the following aspects: certificate type, management of signature key, time stamping, signing of PDF documents, multiple signatures per document.
   1. *Information status reporting.* Describe the process & content for reporting the status of information items. For documentation the following attributes shall be reported as a minimum: document identification, version, title, issue date, status, & document category.
1. **Schedule & resources**
   1. *Schedule.* Establish the sequence & coordination for all the CM activities & all the events affecting the CM plan’s implementation.
   1. *Resources.* Identify the tools, techniques, equipment, personnel, & training necessary for the implementation of CM activities.



## Annex B (normative) Configuration item list — DRD

**B.1 DRD identification.** *Requirement ID & source* — ECSS‑M‑ST‑40, requirement 1.1.1.1.1. *Purpose & objective* — the objective of the **configuration item list (CIL)** is to provide a reporting instrument defining the programme/project items subject to configuration management process.

**B.2 Expected response**

*Special remarks* — None.

*Scope & content:*

1. **Introduction.** The purpose & objective of the CIL.
1. **Applicable & reference documents.** The applicable & reference documents in support to the generation of the document.
1. **List.** Include for each **configuration item (CI)** the following information:
   1. identification code (derived from the product item code)
   1. models identification & quantity
   1. CI name
   1. CI category (developed, non­developed)
   1. CI supplier
   1. applicable specification



## Annex C (normative) Configuration item data list (CIDL) — DRD

**C.1 DRD identification.** *Requirement ID & source* — ECSS‑M‑ST‑40, requirement 1.1.1.1.1. *Purpose & objective* — the **configuration item data list (CIDL)** is a document generated from the central database giving the current design status of a **configuration item (CI)**, at any point of time in sufficient detail, providing its complete definition.

**C.2 Expected response**

*Special remarks* — None.

*Scope & content:*

1. **Introduction.** The purpose & objective of the CIDL.
1. **Applicable & reference documents.** The applicable & reference documents in support to the generation of the document.
1. **List.** Include the following (each entry in the CIDL shall relate to the applicable model(s) of the CI & be defined by its document number, title, issue & release date):
   1. *Cover sheets & scope*
   1. *Customer controlled documentation*, including:
      1. customer specifications & ICDs
      1. support specifications
   1. *Engineering or design documentation*, including:
      1. verification plans demonstrating compatibility with specified requirements
      1. special instructions or procedures (For example, transportation, integration, & handling)
      1. declared PMP lists
      1. lower level specifications
      1. lower level ICDs
      1. drawings & associated lists
      1. test specifications
      1. test procedures
      1. users manual or handbook
   1. *List of applicable changes not yet incorporated into the baselined documentation*, & deviations (including status, directly related to the document (e.g. specification) to which the change belongs to)
   1. *CI number breakdown*
      1. indentured breakdown by part numbers starting from the part number associated with the CI down to the lowest level of composition
      1. quantity of each part number used on the next higher assembly
      1. issue status of the drawings & associated lists applicable to each part number



## Annex D (normative) As‑­built configuration list — DRD

**D.1 DRD identification.** *Requirement ID & source* — ECSS‑M‑ST‑40, requirement 1.1.1.1.1. *Purpose & objective* — the objective of the **as‑­built configuration list (ABCL)** is to provide a reporting instrument defining the as­built status per each serial number of configuration item subject to formal acceptance.

**D.2 Expected response**

*Special remarks* — None.

*Scope & content:*

1. **Introduction.** The purpose & objective of the ABCL.
1. **Applicable & reference documents.** The applicable & reference documents in support to the generation of the document.
1. **List.** List all discrepancies between the as­‑designed configuration documented in the configuration item data list & the as­‑built configuration documented by nonconformance reports or waivers. The ABCL configuration item breakdown section, obtained from the equivalent configuration item data list section, shall be completed by adding the following information:
   1. serial number identification
   1. lot or batch number identification
   1. reference(s) of applicable nonconformance report(s) or request for waiver(s)



## Annex E (normative) Software configuration file (SCF) — DRD

**E.1 DRD identification.** *Requirement ID & source* — ECSS‑M‑ST‑40, requirement 1.1.1.1.1 & from ECSS‑E‑ST‑40 & ECSS‑Q‑ST‑80. *Purpose & objective* — the objective of the **software configuration file (SCF)** is to provide the configuration status of the **software configuration item (SCI)**. It controls its evolution during the programme/project life cycle. The SCF is a constituent of the design definition file (as per ECSS‑E‑ST‑10).

**E.2 Expected response**

*Special remarks* — None.

*Scope & content:*

1. **Introduction.** The purpose & objective of the SCF.
1. **Applicable & reference documents, Definitions & abbreviations.** The applicable & reference documents to support the generation of the document. Any additional terms, definition or abbreviated terms used.
1. **Software configuration item overview.** A brief description of the SCI. Provide the following information:
   1. how to get information about the SCI
   1. composition of the SCI: code, documents
   1. means to develop, modify, install, run the SCI
   1. differences from the reference or previous version
   1. status of software problem reports, software change requests, & software waivers & deviations related to the SCI
1. **Inventory of materials.** ➀ List all physical media & associated documentation released with the SCI (Example of physical media are listings, tapes, cards & disks). ➁ Define the instructions necessary to get information included in physical media (For example, to get files).
1. **Baseline documents.** Identify all the documents applicable to the delivered SCI version.
1. **Inventory of software configuration item.** Describe the content of the SCI. List all files constituting the SCI:
   1. source  codes with name, version, description
   1. binary codes with name, version, description
   1. associated data files necessary to run the software
   1. media labelling references
   1. checksum values
   1. identification & protection method & tool description
1. **Means necessary for the software configuration item.** Describe all items (i.e. hardware & software) that are not part of the SCI, & which are necessary to develop, modify, generate & run the SCI, including:
   1. items related to software development (For example, compiler name & version, linker, & libraries)
   1. build files & software generation process
   1. other SCIs
1. **Installation instructions.** Describe how to install the SCI version, its means & procedures necessary to install the product & to verify its installation.
1. **Change list.** List of all changes incorporated into the SCI version, with a cross reference to the affected SCI document, if any. Changes not incorporated yet but affecting the S/W CI shall also be listed & include.
   1. software problem reports
   1. software change requests & proposals
   1. contractual change notices
   1. software waivers & deviations
1. **Auxiliary information.** Include any auxiliary information to describe the software configuration.
1. **Possible problems &  known errors.** Identify any possible problems or known errors with the SCI version & any steps being taken to resolve the problems or errors.



## Annex F (normative) Configuration status accounting reports (CSAR) — DRD

**F.1 DRD identification.** *Requirement ID & source* — ECSS‑M‑ST‑40, requirement 1.1.1.1.1. *Purpose & objective* — the purpose of **configuration status accounting reports (CSAR)** is to provide a reliable source of configuration information to support all programme/project activities. They provide the knowledge base necessary for performing configuration management.

**F.2 Expected response**

*Special remarks* — None.

*Scope & content:*

1. **Introduction.** The purpose & objective of the CSAR.
1. **Applicable & reference documents.** The applicable & reference documents in support to the generation of the document.
1. **Description**
   1. Documents index & status list
      1. document identification
      1. issue & revision
      1. issue & revision date
      1. title
      1. remarks
      1. identification of obsolete documents
   1. Drawing index & status list
      1. drawing identification
      1. issue & revision
      1. issue & revision date
      1. title
      1. remarks
      1. model applicability of the drawing
   1. Request for deviation index & status list
      1. deviation document number
      1. issue & revision
      1. issue & revision date
      1. title
      1. document or requirement affected
      1. configuration item(s) affected
      1. status of approval
   1. Request for waiver index & status list
      1. waiver document number
      1. issue & revision
      1. issue & revision date
      1. title
      1. document or requirement affected
      1. NCR originating the waiver
      1. serial number of configuration item(s) affected
      1. status of approval
   1. Change proposal (CP) index & status list
      1. CP document number
      1. issue & revision
      1. issue & revision date
      1. title
      1. change request originating the change
      1. configuration item(s) affected
      1. status of approval
   1. Review item discrepancies (RID) index & status list
      1. design review identification
      1. RID identification
      1. title of the discrepancy
      1. affected document number & issue
      1. affected paragraph
      1. RID status
      1. identification of action assigned
      1. RID closure date
      1. remarks
