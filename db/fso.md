# Free-space optics
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [Comms](comms.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---
> <small>**Free-space optics (FSO)** — EN term. **Лазерная космическая связь (ЛКС)** — RU analogue.</small>

**Free-space optics (FSO)** — methods of transmitting information in space using beams [optical range](comms.md) in space.

**FSO** (also wireless optics, WO; rus. атмосферная оптическая линия связи, сокр. **АОЛС**) — a type of optical communications using electromagnetic waves [optical band](comms.md) (usually infrared) transmitted through the atmosphere. In English, the term also includes transmission through a vacuum or outer space.

The principle of operation is identical to the principle of operation of all other wireless radio devices, only optical waves are used. The wavelength in most systems implemented varies between 700‑950 ㎚ or 1 550 ㎚; frequency up to 1.5㎓.

The laser type of communication is resistant to interference in the form of terrestrial rain, while other ranges are resistant to fog.

**Usage.**

Wireless optics are considered as a solution:

1. on sections of the last mile in urban development (for communication between multi‑storey buildings, business centers & network nodes);
1. for the organization of communication from the operator’s communication centers to the base stations of cellular networks with large volumes of transmitted digital traffic (3G, LTE);
1. for communication of objects, when cable laying is impossible (industrial zones, mountainous terrain, railway) or the cost of this laying is high;
1. as a temporary communication channel, as well as in cases where it is urgently necessary to organize a communication channel (hot reserve);
1. when you need a secure communication channel is required that is not susceptible to radio interference & does not create it (airports, the proximity of radars, power lines);
1. if it is necessary to reduce delays in comparison with cable lines.
1. **In space technology.** At present, the successful transmission of an optical (laser) signal over a distance of several hundred thousand kilometers has been carried out. In particular, a record achievement in this sense is the reception of a laser signal from the SC MESSENGER. The signal of the on‑board laser emitter (IR diode neodymium laser) was successfully received by the ground receiver at a distance of 24 000 000 ㎞. At [LADEE](ladee.md), the speed of data transmission in the Moon‑Earth link was ~622 Mbit/s was demonstrated, but only 1/50 communication session was recognized as successful.



## Docs/Links
|**Sections & pages**|
|:--|
|**【[Communications](comms.md)】**<br> [CCSDS](ccsds.md) ~~ [Антенна](antenna.md) ~~ [АФУ](afdev.md) ~~ [Битрейт](bitrate.md) ~~ [ВОЛП](ofts.md) ~~ [ДНА](дна.md) ~~ [Диапазоны частот](comms.md) ~~ [Зрение](view.md) ~~ [Интерферометр](interferometer.md) ~~ [Информация](info.md) ~~ [КНД](directivity.md) ~~ [Код Рида‑Соломона](rsco.md) ~~ [КПДА](antenna.md) ~~ [КСВ](swr.md) ~~ [КУ](ку.md) ~~ [ЛКС, АОЛС, FSO](fso.md) ~~ [Несущий сигнал](carrwave.md) ~~ [ПНА, ПОНА, ПСНА](devd.md) ~~ [Помехи](emi.md) (EMI, RFI) ~~ [Последняя миля](last_mile.md) ~~ [Регламент радиосвязи](comms.md) ~~ [СИТ](etedp.md) ~~ [Фидер](feeder.md) <br>~ ~ ~ ~ ~<br> **РФ:** [БА КИС](ба_кис.md) (21) ~~ [БРК](brk_lav.md) (12) ~~ [РУ ПНИ](ру_пни.md) () ~~ [HSXBDT](comms_lst.md) (1.8) ~~ [CSXBT](comms_lst.md) (0.38) ~~ [ПРИЗЫВ-3](comms_lst.md) (0.17) *([ПРИЗЫВ-1](comms_lst.md) (0.075))**|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Free-space_optical_communication>
1. <https://en.wikipedia.org/wiki/Laser_communication_in_space>
1. <https://en.wikipedia.org/wiki/MESSENGER>


## The End

end of file
