# Zephyr
> .. [🚀](../../index/index.md) [despace](index.md) → **[](.md)** <mark>NOCAT</mark>  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**EN** — EN term. **RU** — RU analogue.</small>

> <small>**EN** — EN term. **RU** — rough RU analogue.</small>

> <small>**EN** — EN term. **RU** — literal RU translation.</small>

> <small>**RU** — RU term w/o analogues in English. **EN** — literal EN translation.</small>

<mark>TBD</mark>



## Description
<mark>TBD</mark>

1. Gina Benigno — 2012 concept participant
1. Samira Motiwala — 2012 concept participant



## Docs/Links
|**Sections & pages**|
|:--|
|**【[Rover](robot.md)】**<br> **Mars:** … ┆ **Moon:** … ┆ **Venus:** [AREE](aree.md), [Zephyr](zephyr.md)|

1. Docs: …
1. <…>


## The End

end of file
