# Space Walker
> 2020.07.20 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/s/spacewalker_logo1t.webp)](f/c/s/spacewalker_logo1.webp)|<yasuda@space-walker.co.jp>, <mark>nophone</mark>, Fax …;<br> *〒105-0004 3-chome 16-12 Shinbashi, Minato-ku, Tokyo, Japan*<br> <https://space-walker.co.jp>・ <https://aerospacebiz.jaxa.jp/en/spacecompany/spacewalker>|
|:--|:--|
|**Business**|SW realizes a future for everyone to enjoy space flight like an airplane. 「Space travel in no longer a dream.」 As the 1st step, SW designs & develops Reusable suborbital winged space planes & provides Services for Scientific Research such as Astronomical Observation & Microgravity Research. SW plans to launch a suborbital spaceplane for Scientific Research in 2022, for small Satellite Launch in 2024 & for Space tourism in 2027.|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**SPACE WALKER Inc. (SW)** is a Japanese university start‑up aiming at manned & unmanned [suborbital spaceplanes](sc.md). Management team has various backgrounds & experience. Founded 2017.12.25.

Especially SW has a team of engineers who have been researching winged space vehicle for over 40 years. SW inherits heritages of HIMES (ISAS of Ministry of Education) & HOPE-X (NAL & NASDA) & enhances WIRES of Tokyo University Of science (TUS) / [Kyushu Institute Of Technology (Kyutech)](kyutech.md). SW transfers the research results & technologies from the university & starts the suborbital spaceplane development concluding the partnership agreement with TUS, [JAXA](jaxa.md), [IHI Corporation](ihi.md), IHI Aerospace Corporation, [Kawasaki Heavy Industries Ltd.](kawasaki_hvi.md) & other non‑aerospace industries.



## The End

end of file
