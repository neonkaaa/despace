# ITAR
> 2019.08.10 [🚀](../../index/index.md) [despace](index.md) → [Doc](doc.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**International Traffic in Arms Regulations (ITAR)** — EN term. **Правила международной торговли оружием** — literal RU translation.</small>

**International Traffic in Arms Regulations (ITAR)** is a U.S. regulatory regime to restrict & control the export of defense & military related technologies to safeguard U.S. national security & further U.S. foreign policy objectives.

For practical purposes, ITAR regulations dictate that information & material pertaining to defense & military related technologies (items listed on the U.S. Munitions List) may only be shared with U.S. Persons unless authorization from the Department of State is received or a special exemption is used. U.S. Persons (including organizations; see legal personality) can face heavy fines if they have, without authorization or the use of an exemption, provided foreign persons with access to ITAR‑protected defense articles, services or technical data.

**Classification of Defense Articles.** The ITAR regulate defense articles & defense services. Defense articles can be broken down into 2 categories: ⒜ physical items (often referred to as 「commodities」) & ⒝ technical data. The ITAR contain a list of defense articles called the US Munitions List (「USML」), which can be found at 22 CFR §121.1. The USML is broken down into the following categories:

1. Firearms, Close Assault Weapons & Combat Shotguns
1. Materials, Chemicals, Microorganisms, & Toxins
1. Ammunition/Ordnance
1. Launch Vehicles, Guided Missiles, Ballistic Missiles, Rockets, Torpedoes, Bombs & Mines
1. Explosives & Energetic Materials, Propellants, Incendiary Agents & Their Constituents
1. Vessels of War & Special Naval Equipment
1. Tanks & Military Vehicles
1. Aircraft & Associated Equipment
1. Military Training Equipment
1. Protective Personnel Equipment
1. Military Electronics
1. Fire Control, Range Finder, Optical & Guidance & Control Equipment
1. Auxiliary Military Equipment
1. Toxicological Agents, Including Chemical Agents, Biological Agents, & Associated Equipment
1. Spacecraft Systems & Associated Equipment
1. Nuclear Weapons, Design & Testing Related Items
1. Classified Articles, Technical Data & Defense Services Not Otherwise Enumerated
1. Directed Energy Weapons
1. …
1. Submersible Vessels, Oceanographic & Associated Equipment
1. Articles, Technical Data, & Defense Services Not Otherwise Enumerated



## Docs/Links
|**Sections & pages**|
|:--|
|**【[Documents](doc.md)】**<br> **Схема:** [КСС](ксс.md) ~~ [ПГС](пгс.md) ~~ [ПЛИС](плис.md) ~~ [СхД](draw.md) ~~ [СхО](draw.md) ~~ [СхПЗ](draw.md) ~~ [СхЧ](draw.md) ~~ [СхЭ](draw.md)<br> [Interface](interface.md) ~~ [Mission proposal](proposal.md)|

1. Docs: …
1. <https://en.wikipedia.org/wiki/International_Traffic_in_Arms_Regulations>


## The End

end of file
