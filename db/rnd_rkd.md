# РКД (РРД)
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [R&D](rnd.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

|**Phase**| | |**Design**| | | | |**Mass prod.:**| |
|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|
|**[R&D phases](rnd.md)**|0 (pre‑A)|A|≈ B|≈ B|≈ C|≈ C/D|≈ E|…|F|
|**[НИОКР](rnd.md)**|[НИР](rnd_0.md)|[АП](rnd_ap.md)|[ЭП](rnd_ep.md)|[ТП](rnd_tp.md)|[РКД (РРД)](rnd_rkd.md)|[Макеты, НЭО](test.md)|[ЛИ](rnd_e.md)|ПСП → СП → ПЭ|Вывод|
| |**[NIR](rnd_0.md)**|**[AP](rnd_ap.md)**|**[EP](rnd_ep.md)**|**[TP](rnd_tp.md)**|**[RKD (RRD)](rnd_rkd.md)**|**[Models, Tests](test.md)**|**[LI](rnd_e.md)**|**PSP → SP → PE**|**Closeout**|

> <small>**Разработка конструкторской документации (РКД), разработка рабочей документации (РРД)** — русскоязычный термин. **Phase C** — примерный англоязычный эквивалент.</small>

**Разработка конструкторской документации (РКД), разработка рабочей документации (РРД).** Рассмотрение и утверждение [ТП](rnd_tp.md) ([ЭП](rnd_ep.md), если не было ТП). Разработка [КД](doc.md), предназначенной для изготовления и [испытания](test.md) опытного образца (партии), без присвоения литеры. Работы на этапе РКД (РРД) проводятся в целях:

1. разработки полного комплекта КД для изготовления и испытаний опытных [изделий](unit.md) комплекса и макетов (в т.ч. [КПЭО](ctpr.md) комплекса и его изделий и при необходимости документации на конструкторско‑технологические макеты, [ТУ](specification.md), [ЭД](doc.md) в соответствии с составленным при разработке ТП перечнем, программы и методики испытаний);
1. разработки в установленном порядке и утверждения заказчиком (или организацией заказчика по его указанию) перечня документации, который определяет конкретную номенклатуру документов (в том числе ТУ, перечня критичных изделий комплекса (элементов) и критичных технологических процессов изготовления этих изделий (элементов), программ испытаний, отчётов по экспериментальной отработке изделий комплекса), подлежащих согласованию с ПЗ (ВП МО) на последующих этапах создания комплекса, с указанием этих этапов и сроков согласования;
1. разработки [ТД](doc.md) для изготовления опытных образцов и макетов и программ (планов) подготовки производства;
1. разработки программной документации.

**Phase C.** Final design & fabrication.

| | |
|:--|:--|
|**Вх. данные**|・[ТЗ](tor.md)<br> ・[контракт](contract.md)<br> ・материалы предыдущего этапа<br> ・прочие [ИД](init_data.md) от Заказчика|
|**Вых. данные**|[КД](doc.md) и РД|
|**Итог**|документы без присвоения литеры|
|**[НД](doc.md)**|・[РК‑11](const_rk.md)<br> ・[ГОСТ 2.102](гост_2_102.md) 「ЕСКД. Виды и комплектность КД」<br> ・[ГОСТ 2.103](гост_2_103.md) 「ЕСКД. Стадии разработки」<br> ・[ГОСТ 2.105](гост_2_105.md) 「ЕСКД. Общие требования к текстовым документам」<br> ・[ГОСТ 2.106](гост_2_106.md) 「ЕСКД. Текстовые документы」<br> ・[ГОСТ 2.902](гост_2_902.md) 「ЕСКД. Текстовые документы」<br> ・[ГОСТ 15.201](гост_15_201.md) 「ТТЗ на ОКР」<br> ・[ГОСТ 15.203](гост_15_203.md) 「Порядок выполнения ОКР」<br> ・[ГОСТ 15.208](гост_15_208.md) 「Единый сквозной план」<br> ・[ГОСТ 27.1.02](гост_27_1_02.md) 「ПОН」<br> ・[ГОСТ 27.310](гост_27_310.md) 「АВПКО」<br> ・[ГОСТ 51540](гост_51540.md) 「Военная техника. Термины и определения」<br> ・[ГОСТ 8.573](гост_8_573.md) 「Метрологическая экспертиза」|
|**Процесс**|[формирование](faq.md#Словоблудие) материалов РКД (РРД)|
|**[УЗК](trl.md)**|отсутствует|



## Документация РКД (РРД)
Работы и документацию к.510 на данном этапе ㎝. [на соответствующей странице](lav.md).

На этапе [ТП](rnd_tp.md) (по [ГОСТ 2.902](гост_2_902.md), п.5.3.7) или же на этапе [ЭП](rnd_ep.md) (по [РК‑11](const_rk.md), п.3.2.1) должен быть разработан [Перечень КД](list_ddoc.md), в котором определяется большая часть выпускаемой документации по каждой СЧ.

**Таблица.** Типовая комплектность документации РКД (РРД).

<small>

|**№**|**Наименование документа РКД (РРД)**|**Соглас.**|**Утвержд.**|**Примечание**|**Основание**|
|:--|:--|:--|:--|:--|:--|:--|
|1|**[АВПКО](fmeca.md)**| | |При необходимости|[ГОСТ 27.310](гост_27_310.md)|
|2|**[Ведомости](liod.md)**| | | |[ГОСТ 2.102](гост_2_102.md), [ГОСТ 2.601](гост_2_601.md)|
|3|**[Инструкции](инструкции.md)**| | | |[ГОСТ 2.102](гост_2_102.md), [ГОСТ 2.601](гост_2_601.md)|
|4|**Каталожные описания**| | |Необходимость по СЧ определяется в ТТЗ|[ГОСТ 15.203](гост_15_203.md) п.5.4.4, [ГОСТ 2.601](гост_2_601.md)|
|5|**[КПЭО](ctpr.md)**| | | |[РК‑11](const_rk.md) п.3.2.5|
|6|**Нормы расхода**| | |Зап.частей, материалов|[ГОСТ 2.601](гост_2_601.md)|
|7|**Отчёты по экспер. отработке**| | | |[РК‑11](const_rk.md) п.3.2.1|
|8|**Паспорт**| | | |[ГОСТ 2.601](гост_2_601.md)|
|9|**Перечень аппаратуры, СИ и оборудования**| | | |[ГОСТ 2.902](гост_2_902.md) п.5.3.2|
|10|**Перечень [критичных элементов и процессов](sens_elem.md)**| | | |[РК‑11](const_rk.md) п.3.2.1|
|11|**[ПОНп](qm.md)**| | | |[ГОСТ 20.39.302](гост_20_39_302.md) п.5.2|
|12|**[ПОСТ](qm.md)**| | | |[ГОСТ 20.39.302](гост_20_39_302.md) п.4.2|
|13|**[Программы и методики](pmot.md)**| | |Испытания, контроль, проверки|[ГОСТ 2.102](гост_2_102.md)|
|14|**[Расчёты](calc.md)**| | |Надёжность, прочность, рад.стойкость|[ГОСТ 2.102](гост_2_102.md)|
|15|**Руководство по эксплуатации**| | | |[ГОСТ 2.601](гост_2_601.md)|
|16|**[Спецификации](specification.md)**| | | |[ГОСТ 2.102](гост_2_102.md)|
|17|**[Схемы](doc.md)**| | | |[ГОСТ 2.102](гост_2_102.md)|
|18|**[ТУ](specification.md)**| | | |[ГОСТ 2.102](гост_2_102.md)|
|19|**Формуляр**| | | |[ГОСТ 2.601](гост_2_601.md)|
|20|**[Чертежи и модели](draw.md)**| | | |[ГОСТ 2.102](гост_2_102.md)|
|21|**Этикетки**| | | |[ГОСТ 2.601](гост_2_601.md)|

</small>



## Рабочий процесс
1. [Адаптация средств выведения](асв.md) (АСВ)

<mark>TBD</mark>

На данном этапе проводятся следующие [Design Review](design_review.md):

1. **EQSR** — в процессе, если не было на [ЭП](rnd_ep.md);
1. **PDR** — в начале этапа, если не было на [ЭП](rnd_ep.md);
1. **CDR** — по окончании этапа.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[R&D](rnd.md)】**<br> [Design review](design_review.md) ~~ [Management](mgmt.md) ~~ [MBSE](se.md) ~~ [Proposal](proposal.md) ~~ [Test](test.md) ~~ [V‑model](v_model.md) ~~ [Validation, Verification](vnv.md)<br> [АП](rnd_ap.md) ~~ [ЛИ](rnd_e.md) ~~ [Макеты, НЭО](test.md) ~~ [НИР](rnd_0.md) ~~ [РКД (РРД)](rnd_rkd.md) ~~ [ТП](rnd_tp.md) ~~ [ЭП](rnd_ep.md)|

1. Docs: …
1. <…>


## The End

end of file
