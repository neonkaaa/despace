# Space NTK Co., Ltd.
> 2021.12.03 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/s/space_ntk_logo1t.webp)](f/c/s/space_ntk_logo1.webp)|<mark>noemail</mark>, +81(3)6314-6917, Fax …;<br> *3-2-15-104, Nakamuraminami, Nerima-ku, Tokyo 176-0025, Japan*<br> <https://www.space-ntk.com> ~~ [FB ⎆](https://www.facebook.com/space1059) ~~ [IG ⎆](https://www.instagram.com/spacentk)|
|:--|:--|
|**Business**|Space burial|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・Representative Director — Tomoko Kasai|

**Space NTK Co., Ltd.** is a Japanese company aimed to provide services for placing ones ashes on the Earth orbit. Founded 2017.02.

**Burial service.** The ashes are placed on an artificial satellite & launched into orbit around the Earth by a rocket. It’s a new form of funeral that will come true in the space you admire.

<p style="page-break-after:always"> </p>

## The End

end of file
