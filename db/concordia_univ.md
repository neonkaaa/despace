# Concordia University
> 2020.06.28 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/c/concordia_univ_logo1t.webp)](f/c/c/concordia_univ_logo1.webp)|<mark>noemail</mark>, +1(514)848-2424, Fax …;<br> *Sir George Williams Campus, 1455 De Maisonneuve Blvd. W., Montreal, Quebec, Canada, H3G 1M8*<br> <http://www.concordia.ca> ~~ [LI ⎆](https://www.linkedin.com/school/concordia-university) ~~ [X ⎆](https://twitter.com/Concordia)|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Concordia University** is a next-generation university, one that reimagines the future of higher education on a continual basis. Located in the vibrant and multicultural city of Montreal, Concordia is among the most innovative universities in its approach to experiential learning, research and online education.

See also [Space Concordia](space_concordia.md).



## The End

end of file
