# GSLV
> 2019.06.25 [🚀](../../index/index.md) [despace](index.md) → [LV](lv.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**GSLV** — англоязычный термин, не имеющий аналога в русском языке. **ГСЛВ** — дословный перевод с английского на русский.</small>

**Geosynchronous Satellite Launch Vehicle (GSLV)**, рус. **ракета‑носитель для запусков геосинхронных спутников** — семейство индийских одноразовых жидкостных ракет‑носителей среднего класса.

|**Version**|**Description**|**Activity**|
|:--|:--|:--|
|GSLV Mk.I|Базовый вариант.|Экспл. завершена (2001 ‑ 2010)|
|╟ GSLV Mk.II|Модификация GSLV Mk.I.|**Активен** (2010 ‑ …)|
|╙ GSLV Mk.III  (LVM3)|Модификация GSLV Mk.I.|**Активен** (2009 ‑ …)|

[![](f/lv/gslv/gslv_rf_01t.webp)](f/lv/gslv/gslv_rf_01.webp)



---



## GSLV Mk.II
**Geosynchronous Satellite Launch Vehicle Mark II (GSLV Mk.II)** — индийская одноразовая жидкостная ракета‑носитель среднего класса. Разработка на основе GSLV Mk.I, только 3‑я ступень (Cryogenic Upper Stage, 「CUS」) производства Индии, а не России.

|**Characteristic**|**[Value](si.md)**|
|:--|:--|
|Активность|**Активен** (2010.04.15 ‑ …)|
|[Аналоги](analogue.md)|[Delta‑4 Medium](delta.md) (США) ~~ [H‑IIA](h2.md) (Япония) ~~ [LongMarch‑3B](long_march.md) (Китай)|
|Длина/диаметр|49.13 м (с ГЧ) / 2.8 м|
|[Космодромы](spaceport.md)|[Sriharikota](spaceport.md)|
|Масса старт./сух.|414 750 ㎏ / 53 450 ㎏|
|Разраб./изготов.|[ISRO](isro.md) (Индия) / [ISRO](isro.md) (Индия)|
|Ступени|3|
|[Fuel](ps.md)|[АТ + НДМГ](nto_plus.md) (ускорители, 2 ступ.), [HTPB](ps.md) (1 ступ.), [Кислород + Водород](o_plus.md) (3 ступ.)|
| |[![](f/lv/gslv/gslv_mk2t.webp)](f/lv/gslv/gslv_mk2.webp)|

**Выводимые массы.**

|**Космодром**|**РН**|<small>*Масса,<br> [НОО](nnb.md), т*</small>|<small>*Масса,<br> [ГСО](nnb.md), т*</small>|<small>*Масса к<br> [Луне](moon.md), т*</small>|<small>*Масса к<br> [Венере](venus.md), т*</small>|<small>*Масса к<br> [Марсу](mars.md), т*</small>|**Примечания**|
|:--|:--|:--|:--|:--|:--|:--|:--|
|[Sriharikota](spaceport.md)|GSLV Mk.II|5|1.2|…|…|…|Пуск — $ 47 млн (2019 г);<br> ПН 1.20 % от ст.массы|

<small>Примечания:<br> **1)** Указана масса для наихудших условий старта.<br> **2)** В скобках указана масса для наилучших условий старта.</small>



## GSLV Mk.III
**Geosynchronous Satellite Launch Vehicle Mark III (GSLV Mk.III)** — индийская одноразовая жидкостная ракета‑носитель среднего класса.

|**Characteristic**|**[Value](si.md)**|
|:--|:--|
|Активность|**Активен** (2009.09.10 ‑ …)|
|[Аналоги](analogue.md)|[Arian 5](arian.md) (Европа) ~~ [Atlas 5](atlas.md) (США) ~~ [Delta 4 Heavy](delta.md) (США) ~~ [Falcon‑9](falcon.md) (США) ~~ [H‑IIB](h2.md) (Япония) ~~ [LongMarch‑3B (5)](long_march.md) (Китай) ~~ [Ангара‑А5](angara.md) (РФ) ~~ [Зенит‑3SL](zenit.md) (РФ) ~~ [Протон‑М](proton.md) (РФ) ~~ [Союз‑5.1](soyuz.md) (РФ)|
|Длина/диаметр|43.5 м (с ГЧ) / 4 м|
|[Космодромы](spaceport.md)|[Sriharikota](spaceport.md)|
|Масса старт./сух.|644 750 ㎏ / 76 000 ㎏|
|Разраб./изготов.|[ISRO](isro.md) (Индия) / [ISRO](isro.md) (Индия)|
|Ступени|3|
|[Fuel](ps.md)|[HTPB](ps.md) (ускорители), [АТ + НДМГ](nto_plus.md) (1 ступ.), [Кислород + Водород](o_plus.md) (2 ступ.)|
| |[![](f/lv/gslv/gslv_mk3t.webp)](f/lv/gslv/gslv_mk3.webp) [![](f/lv/gslv/gslv_mk3_launchert.webp)](f/lv/gslv/gslv_mk3_launcher.webp)|

**Выводимые массы.**

|**Космодром**|**РН**|<small>*Масса,<br> [НОО](nnb.md), т*</small>|<small>*Масса,<br> [ГСО](nnb.md), т*</small>|<small>*Масса к<br> [Луне](moon.md), т*</small>|<small>*Масса к<br> [Венере](venus.md), т*</small>|<small>*Масса к<br> [Марсу](mars.md), т*</small>|**Примечания**|
|:--|:--|:--|:--|:--|:--|:--|:--|
|[Sriharikota](spaceport.md)|GSLV Mk.III|8 <small>(600 ㎞)</small>|2.1|…|…|…|Пуск — $ 46‑62 млн (2017 г);<br> ПН 1.24 % от ст.массы|

<small>Примечания:<br> **1)** Указана масса для наихудших условий старта.<br> **2)** В скобках указана масса для наилучших условий старта.</small>



## Архивные

### GSLV Mk.I
**Geosynchronous Satellite Launch Vehicle Mark I (GSLV Mk.I)** — индийская одноразовая жидкостная ракета‑носитель среднего класса. Имела криогенную третью ступень российского производства: кислородно‑водородный блок 「12КРБ」 с жидкостным двигателем [КВД1](engine_lst.md). После присоединения в 1995 году к 「Соглашению о контроле за распространением ракет и ракетных технологий」 поставки были прекращены. В связи с чем ISRO сделало аналогичную 3‑ю ступень и итоговую РН назвали GSLV Mk.II.

|**Characteristic**|**[Value](si.md)**|
|:--|:--|
|Активность|Экспл. завершена (2001.04.18 ‑ 2010.12.25)|
|[Аналоги](analogue.md)|[Delta‑4 Medium](delta.md) (США) ~~ [H‑IIA](h2.md) (Япония) ~~ [LongMarch‑3B](long_march.md) (Китай)|
|Длина/диаметр|49.13 м (с ГЧ) / 2.8 м|
|[Космодромы](spaceport.md)|[Sriharikota](spaceport.md)|
|Масса старт./сух.|414 750 ㎏ / 53 450 ㎏|
|Разраб./изготов.|[ISRO](isro.md) (Индия) / [ISRO](isro.md) (Индия)|
|Ступени|3|
|[Fuel](ps.md)|[АТ + НДМГ](nto_plus.md) (ускорители, 2 ступ.), [HTPB](ps.md) (1 ступ.), [Кислород + Водород](o_plus.md) (3 ступ.)|
| |[![](f/lv/gslv/gslvt.webp)](f/lv/gslv/gslv.webp)|

**Выводимые массы.**

|**Космодром**|**РН**|<small>*Масса,<br> [НОО](nnb.md), т*</small>|<small>*Масса,<br> [ГСО](nnb.md), т*</small>|<small>*Масса к<br> [Луне](moon.md), т*</small>|<small>*Масса к<br> [Венере](venus.md), т*</small>|<small>*Масса к<br> [Марсу](mars.md), т*</small>|**Примечания**|
|:--|:--|:--|:--|:--|:--|:--|:--|
|[Sriharikota](spaceport.md)|GSLV Mk.I|5|1.1|…|…|…|Пуск — $ 33 млн (2010 г);<br> ПН 1.20 % от ст.массы|

<small>Примечания:<br> **1)** Указана масса для наихудших условий старта.<br> **2)** В скобках указана масса для наилучших условий старта.</small>



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Launch vehicle (LV)](lv.md)】**<br> [ICBM](icbm.md) ~~ [Integrated payload unit](lv.md) ~~ [Non‑rocket spacelaunch](nrs.md) ~~ [Throw weight](throw_weight.md)<br>~ ~ ~ ~ ~<br> **China:** [Long March](long_march.md) ┊ **EU:** [Arian](arian.md), [Vega](vega.md) ┊ **India:** [GSLV](gslv.md), [PSLV](pslv.md) ┊ **Israel:** [Shavit](shavit.md) ┊ **Japan:** [Epsilon](epsilon.md), [H2](h2.md), [H3](h3.md) ┊ **Korea N.:** [Unha](unha.md) ┊ **Korea S.:** *([Naro‑1](kslv.md))* ┊ **RF,CIF:** [Angara](angara.md), [Proton](proton.md), [Soyuz](soyuz.md), [Yenisei](yenisei.md), [Zenit](zenit.md) *([Energia](energia.md), [Korona](korona.md), [N‑1](n_1.md), [R‑1](r_7.md))* ┊ **USA:** [Antares](antares.md), [Atlas](atlas.md), [BFR](bfr.md), [Delta](delta.md), [Electron](electron.md), [Falcon](falcon.md), [Firefly Alpha](firefly_alpha.md), [LauncherOne](launcherone.md), [New Armstrong](new_armstrong.md), [New Glenn](new_glenn.md), [Minotaur](minotaur.md), [Pegasus](pegasus.md), [Shuttle](shuttle.md), [SLS](sls.md), [Vulcan](vulcan.md) *([Saturn](saturn_lv.md), [Sea Dragon](sea_dragon.md))**|

1. Docs:
   1. [GSLV manual ❐](f/lv/gslv/gslv_manual_issue0_rev0_2004.djvu) (2004, issue 0 rev. 0)
1. <https://en.wikipedia.org/wiki/Comparison_of_orbital_launch_systems>
1. <https://en.wikipedia.org/wiki/Geosynchronous_Satellite_Launch_Vehicle>
1. <https://en.wikipedia.org/wiki/Geosynchronous_Satellite_Launch_Vehicle_Mark_III>
1. <https://www.isro.gov.in/launchers/gslv-mk-iii>
1. <https://www.isro.gov.in/launchers/gslv>
1. <http://ecoruspace.me/GSLV+Mk+II.html>
1. <http://ecoruspace.me/GSLV+Mk+III.html>


## The End

end of file
