# ECSS‑E‑ST‑10‑24C (01‑Jun‑2015)
> 2015.06.01 [🚀](../../index/index.md) [despace](index.md) → [Doc](doc.md), [ECSS](ecss.md), [SC](sc.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

## 1. Intro

**「Space engineering. Interface management.」**

This Standard is one of the series of ECSS Standards intended to be applied together for the management, engineering & product assurance in space projects & applications. ECSS is a cooperative effort of the European Space Agency, national space agencies & European industry associations for the purpose of developing & maintaining common standards. Requirements in this Standard are defined in terms of what shall be accomplished, rather than in terms of how to organize & perform the necessary work. This allows existing organizational structures & methods to be applied where they are effective, & for the structures & methods to evolve as necessary without rewriting the standards.

This Standard has been prepared by the ECSS E‑ST‑10‑24C Working Group, reviewed by the ECSS Executive Secretariat & approved by the ECSS Technical Authority.

ECSS does not provide any warranty whatsoever, whether expressed, implied, or statutory, including, but not limited to, any warranty of merchantability or fitness for a particular purpose or any warranty that the contents of the item are error‑free. In no respect shall ECSS incur any liability for any damages, including, but not limited to, direct, indirect, special, or consequential damages arising out of, resulting from, or in any way connected to the use of this Standard, whether or not based upon warranty, business agreement, tort, or otherwise; whether or not injury was sustained by persons or property or otherwise; & whether or not loss was sustained from, or arose out of, the results of, the item, or any services that may be provided by ECSS.

The management & control of interfaces is crucial to the success of space programmes & projects. Interface management is a process to assist in controlling product development when efforts are divided amongst different parties (e.g. agencies, contractors, geographically dispersed technical teams). Interface control is also needed to define, achieve & maintain compliance between products & actors that interoperate.

The application of this standard to a project is expected to bring the following benefits:

1. a consistent, coherent & commonly used approach — including documentation — throughout industry & across different projects;
2. effective & efficient product interface management;
3. minimize the risk of interface incompatibilities;
4. high confidence in achieving successful product operations for the intended use.

The objective of interface management is to achieve functional & physical compatibility amongst all interrelated items in the product tree. The goal of this standard is to define a common & systematic process to meet the objective.

This standard describes a standard process & methodology for interface management throughout the life cycle, in terms of identification, requirements specification, definition, approval & control, implementation, verification & validation of interfaces, within a space programme or project & in accordance with the other relevant ECSS standards.

In line with the definition of the Space System breakdown in Figure 2‑1 of ECSS‑S‑ST‑00‑01, this standard is applicable to the following interfaces, where a contractual relationship exist among parties:

1. within the Space Segment
2. within the Ground Segment
3. between the Space Segment & the Ground Segment
4. between Space Segment & Launch Segment only for ICD aspects in conformance to the launcher user manual

This standard does not ensure that all the specificities of interfaces within the Launch Segment are covered.

This standard is applicable to development of products at all different levels in the product tree. It is applicable to both the customer & the supplier of the product during all project phases (0 to F) & follows the generic ECSS customer/supplier pattern.

This standard may be tailored for the specific characteristics & constrains of a space project in conformance with ECSS‑S‑ST‑00.



## 2. Normative references & Bibliography

The following normative documents contain provisions which, through reference in this text, constitute provisions of this ECSS Standard. For dated references, subsequent amendments to, or revision of any of these publications do not apply. However, parties to agreements based on this ECSS Standard are encouraged to investigate the possibility of applying the more recent editions of the normative documents indicated below. For undated references, the latest edition of the publication referred to applies.

1. ECSS‑S‑ST‑00‑01 ECSS — Glossary of terms
2. ECSS‑E‑ST‑10 Space engineering — System engineering general requirements
3. ECSS‑E‑ST‑10‑02 Space engineering — Verification
4. ECSS‑E‑ST‑10‑06 Space engineering — Technical requirements specification
5. ECSS‑M‑ST‑10 Space project management — Project planning & implementation
6. ECSS‑M‑ST‑40 Space project management — Configuration & information management
7. ECSS‑Q‑ST‑10‑09 Space product assurance — Nonconformance control system

**Bibliography**

1. ECSS‑S‑ST‑00 ECSS system — Description, implementation & general requirements


## 3. Terms, definitions & abbreviated terms

**Terms from other standards**

For the purpose of this Standard, the terms & definitions from ECSS‑S‑ST‑00‑01 apply, in particular for the following terms:

1. approval
2. baseline
3. configuration baseline
4. customer
5. interface
6. supplier

For the purpose of this Standard, the following term & definition from ECSS‑E‑ST‑10‑06 applies:

1. verification requirements

For the purpose of this Standard, the following terms & definitions from ECSS‑M‑ST‑40 apply:

1. change request
2. change proposal

Terms specific to the present standard:

1. **controlled ICD** — ICD formally issued subject to configuration control process
2. **external interface** — interface between items under different programme responsibilities
3. **frozen ICD** — ICD formally issued subject to configuration control process & signed by interface responsible & all the involved actors. (1) A 「frozen」 ICD reflects the design baseline that is considered, for the interface related aspects, to be final & complete allowing start of manufacturing, integration, implementation & testing activities. (2) Change of a 「frozen」 ICD can occur but it usually implies a major cost or schedule impact.
4. **interface actor** — <CONTEXT:role> responsible for the design, development & verification of one interface end. The interface actors are all parties involved in the interface ends definition, design, development.
5. **interface control document (ICD)** — document defining the design of the interface(s)
6. **interface definition document (IDD)** — document defining the design of one interface end
7. **interface end** — one side of an interface. An interface end is the point of interaction of one of the elements of an interface.
8. **interface identification document** — document defining the index of all identified interfaces.
9. **interface plane** — plane that distinguishes the two interface ends that interface with each other.
10. **interface requirement document (IRD)** — document defining the requirements for an interface or a collection of interfaces.
11. **interface responsible** — <CONTEXT:role> responsible for the requirement specification, definition, development & verification of the interface. The interface responsible is the customer or his delegate, as an example for space segment to launch segment interface, it is the entity procuring both or its delegates.
12. **internal interface** — interface between items within the same programme responsibility.
13. **preliminary ICD** — draft ICD circulated & iterated during interface definition phase before issuing a controlled ICD

**Abbreviated terms.** For the purpose of this Standard, the abbreviated terms from ECSS‑S‑ST‑00‑01 & the following apply:

1. **CCSDS** — Consultative Committee for Space Data Systems
2. **CR** — change request
3. **CP** — change proposal
4. **ECSS** — European Cooperation for Space Standardization
5. **ICD** — interface control document
6. **IDD** — interface definition document
7. **IID** — interface identification document
8. **IRD** — interface requirements document
9. **OTS** — off‑the‑shelf

**Nomenclature**

The following nomenclature applies throughout this document:

1. The word **「shall**」 is used in this Standard to express requirements. All the requirements are expressed with the word 「shall」.
2. The word **「should」** is used in this Standard to express recommendations. All the recommendations are expressed with the word 「should」.

It is expected that, during tailoring, recommendations in this document are either converted into requirements or tailored out.

1. The words **「may」** & **「need not」** are used in this Standard to express positive & negative permissions, respectively. All the positive permissions are expressed with the word 「may」. All the negative permissions are expressed with the words 「need not」.
2. The word **「can」** is used in this Standard to express capabilities or possibilities, & therefore, if not accompanied by one of the previous words, it implies descriptive text.

In ECSS 「may」 & 「can」 have completely different meanings: 「may」 is normative (permission), & 「can」 is descriptive.

The present & past tenses are used in this Standard to express statements of fact, & therefore they imply descriptive text.


## 4. Principles

### 4.1. Type of interfaces

In a Space System there can be three major types of interfaces.

1. interfaces within the Space Segment, Ground Segment or Launch Segment.
2. interfaces between the different Segments of the Space System.
3. interfaces between the Support Segment & the Space Segment, Ground Segment or Launch Segment.

Refer to Figure 2‑1 of ECSS‑S‑ST‑00‑01 for details on Space System breakdown.

In addition, a distinction can be made between internal & external interfaces.

The notion of internal or external depends on the position & role of an actor in the customer supplier chain.

An internal interface is an interface under the control of a given actor.

An external interface is an interface outside the control of a given actor.

For example, an interface between two suppliers of the same customer is considered external by the suppliers & internal by the customer.


### 4.2. Interface management process

**General description**

The interface management process is applied at all levels of the supplier/customer chain.

The standard describes the process at one level, between one customer & its lower tier suppliers.

The customer or his delegate is responsible for the definition, development & verification of the interface.

In addition to the interface responsible, the interface actors are all the parties involved in the interface end definition, design, development.

This process can impact the similar activities done at higher or lower levels.

As per ECSS‑S‑ST‑00‑01, the term 「product」 is used in the standard as a generic term which defines any component, equipment or element.

Annex E provides a non‑exhaustive list of interface data, that can be used as a basis for interface specification, definition & control.

Figure 41 provides an overview of the interface management process.

【**Fig.41:** Interface management process — overview of the main process steps】

1. System/element requirements specification(s) containing higher level interface requirements
2. System/produce functional/logical/physical architectural decomposition
3. Interface management:
    1. Interface management planning
    2. Interface identification
    3. Interface requirements specification
    4. Interface definition
    5. Interface approval & control
    6. Interface verification & validation

**Interface management planning**

At the beginning of the project, each customer defines the approach, the requirements, the responsibilities & the planning for the management of the interfaces.

**Interface identification**

At the beginning of the project, each customer identifies the interfaces under his responsibility.

This process is repeated by each actor at each level of the customer/supplier chain.

The interface identification is based on product architecture definition, i.e.:

1. Product functions identification/definition
2. Product decomposition into elements
3. Functions allocation to elements

Then the interface identification is further detailed according to the product architecture decomposition.

The identified interfaces can be compiled into a list, including identification of the involved suppliers, as well as the references to the applicable technical documentation.

The output of the interface identification process can be documented in an Interface Identification Document (IID). An example of an IID is given in the informative Annex D.

The IID is a living document which is populated & updated during the interface life cycle, with the references to IRD, ICD, IDD, CR when they become baselined.

The IID becomes therefore the repository that defines & governs the interface baseline status & their unique identification.

**Interface requirements specification**

Following the interfaces identification, each customer defines the requirements for each interface.

The establishment of interface requirements is part of the requirement engineering process as defined in ECSS‑E‑ST‑10 clause 5.2.

The interface requirements on the identified interfaces are derived from the higher level requirements & functional, logical & physical architectural decomposition, as well as the verification requirements.

An interface requirement defines the functional, performance, electrical, environmental, human, & physical requirements that exist at a common boundary between two or more products.

When the interface requirements specification is completed & baselined, it defines all the design requirements to be adhered to by the supplier who is responsible for the design, development & verification of the interface ends.

The output of the interface requirements specification process is documented in IRDs or in technical requirement specifications.

An IRD applies to the entire interface, including all interface ends.

For each interface requirement, applicability for involved interface end is specified (e.g. one interface end, all interface ends).

The use of an IRD as a self‑standing document is not mandatory, however it facilitates consistency of the interface requirements among all involved actors.

IRDs are useful when separate actors are developing components of the system or when the system places requirements on other systems outside programme/project control.

**Interface definition**

Interface definition is the process of developing a design solution compliant with the applicable interface requirements that ensures compatibility between the involved products.

The definition of an interface is the result of the design activities performed by the customer or his delegate who is responsible for the interface as a whole & by the actors who are responsible for their interface ends.

The inputs for this activity are the IRD(s) provided by the customer & the various interface end definitions of the products as provided by the suppliers.

The definition of the product interface end can be provided in the form of Interface Definition Document (IDD) or 「single‑end」 Interface Control Document (ICD).

The 「single‑end」 ICD is sometimes referred as 「unit ICD」 or 「equipment ICD」.

The interface definition process is an iterative & converging process, where the number of modifications decreases over time.

The output of the interface definition process is documented in ICD ready to be formally agreed by all parties.

The interfaces can be grouped according to contractual, discipline or product decomposition (product tree).

As part of this process, evolutions may happen leading to an update of IRD(s)

**Interface approval & control**

Following the interface definition process, the interface control is formalized by issuing the ICD(s) in two steps:

1. controlled ICD
2. frozen ICD

The controlled ICD reflects an evolving interface definition, which converges from supplier preliminary design to final one.

The frozen ICD reflects interface baseline considered to be final & complete allowing start of manufacturing, integration, implementation activities.

Both steps are formally issued & subject to configuration control process.

The controlled ICD is signed by the interface responsible only, while the frozen ICD is signed by the interface responsible & all the involved actors, to reflect acceptance.

Signature of all involved parties can be anticipated on controlled ICD.

Any evolution needs to be controlled, & modification approved by all actors.

Since the controlled ICD reflects an evolving interface definition, the evolutions can be controlled in a less formal way, avoiding unnecessary delays & unjustified programmatic impact.

The changes to the IRD & ICD are managed through the generic change control process defined in ECSS‑M‑ST‑40, as follows.

A Change Request (CR) is generated by the interface responsible & used to support the Interface Change Management process.

In case the modification is initiated by a supplier:

1. When the ICD is in controlled status (not yet signed), this initiation is done by any means agreed between the supplier & the customer.
2. When the ICD is in frozen status (signed), this initiation is done through a Change Proposal (CP), to be referenced in the CR.

The CR defines in detail a technical change proposal to an existing & baseline ICD (including any already approved CRs affecting the interface).

The CR description includes a graphic or textual description of the change in sufficient detail to permit a clear evaluation, including:

1. Modification from original to new content
2. Deletion of existing content
3. Addition of new content
4. Effectivity (i.e. point in time or in production series when the change becomes effective)
5. Urgency, indication of whether this change is critical or routine (if dedicated procedures are     defined by the project to manage urgency)

The CR is analyzed & discussed by all involved actors.

For the controlled ICD, the change process is considered completed by the signature of the CR by interface responsible & all the involved actors.

For the controlled ICD, there is no need that suppliers produce a CP unless it is justified, since the ICD modification is normally the results of detailed design activities, which improve interface data definition, rather than a real design change.

For the frozen ICD, each supplier provides their impacts by a CP, to be approved by the Customer before implementation.

When agreed & signed by all parties, CR becomes an accepted modification of the interface baseline, to be incorporated in a new issue of the ICD.

Until the new ICD issue is released, the interface definition baseline consists of the current ICD plus the agreed CRs.

A typical workflow interface change management process is shown in Figure 42.

【**Fig.42:** Interface Change Management Process implementation】  
![](f/doc/ecss/ecss_est1024/42.png)


**Interface verification & validation**

Interface requirements are subject to the same verification process as defined in ECCS‑E‑ST‑10‑02, as any other requirements.

This process is done in three steps:

1. Early verification of compatibility of the ICD with the interface requirements, resulting in a signed version of the ICD by both supplier & customer (part of the approval process).
2. Stand‑alone interface end demonstration of compliance with the ICD taking into account the verification requirements associated to interface requirements, performed under the responsibility of the supplier of the relevant product (to be done prior to the delivery of each product).
3. Joint verification of the interface in terms of functions & performances, involving the different interfacing products, performed under responsibility of the customer (with supplier support) during the integration of the various products, taking into account the verification requirements from higher levels.

The verification logic can include any combination of joint & standalone verification activities.

Interface validation is the activity to demonstrate that the interface is able to accomplish its intended use in the intended operational environment, & can be performed as part of interface management or as part of any higher level product validation activity.



### 4.3. Interface management life cycle

**Generic interface management life cycle**

Figure 43 shows a generic life cycle referred to a typical interface involving customer & two suppliers.

In case more than two levels of customer/supplier are involved, this process is repeated recursively by each actor at each level of the customer/supplier chain.

This life cycle applies to product categories C & D as defined in ECSS‑E‑ST‑10‑02 Table 5‑1.

The life cycle starts with the identification of the interfaces by the customer (typically between customer SRR & PDR).

After Customer PDR, the Interfaces Requirements applicable to Suppliers are identified & specified by the customer either in a self‑standing IRD or embedded in a technical specification applicable to supplier, prior to suppliers’ contract start.

After the supplier's requirements are baselined, the customer establishes the preliminary interface definition, circulating it among the involved actors.

The suppliers provide IDDs & relevant technical design data.

The preliminary ICD is consolidated, formally issued & becomes the controlled ICD for Suppliers’ PDR.

After this stage, changes of the controlled ICD are possible in accordance with the simplified interface control process defined in clause 5.5.

Prior to the Suppliers’ CDRs, under the responsibility of the customer, suppliers & customer consolidate the controlled ICDs to become frozen, & approved by all parties

The approved frozen ICD is considered the final ICD, ready to permit the suppliers to start manufacturing, integration, implementation, or verification activities.

After reaching this stage, changes of the frozen ICD are possible in accordance with the interface control process defined in clause 5.5.

【**Fig.43:** Generic interface management life cycle】  
![](f/doc/ecss/ecss_est1024/43.png)


**Space element — Launch segment interface management life cycle**

Figure 44 shows a life cycle referred to a typical Space to Launch segment interface life cycle.

The life cycle starts with the selection of the launcher by the space segment responsible, on the basis of available user manuals.

The user manual specifies the minimum duration between kick‑off of launcher contract & launch.

After launcher contract kick‑off meeting, the space segment responsible, taking into account the boundary conditions specified by the launcher user manual, prepares an IRD that contains:

1. the mission requirements as specified by the space segment responsible,
2. the interface definition & constraint coming from the space segment element supplier through the IDD.

On the above data, the launch segment service supplier establishes the preliminary interface definition (Issue 0 of ICD), circulating it among the involved actors.

The preliminary ICD is consolidated, formally issued & becomes the controlled ICD Issue 1 as result of the Preliminary Mission Analysis Review (PMAR).

As results of the Final Mission Analysis Review, the controlled ICD will become the frozen version for flight, approved by all parties.

【**Fig.44:** Typical space to launch segment interface life cycle】  
![](f/doc/ecss/ecss_est1024/44.png)


**Space segment — Ground segment interface management life cycle**

Figure 45 shows a life cycle referred to a typical interface involving space segment & ground segment.

The life cycle starts with the identification of the interfaces by the space system responsible (typically between space system SRR & PDR).

The Interfaces Requirements applicable to space segment — ground segment interface are identified & specified by the space system responsible. either in a self‑standing IRD or embedded in a specification.

After the interface requirements are baselined, the space system responsible establishes the preliminary interface definition, circulating it among the involved actors.

The segments responsibles provide IDDs & relevant technical design data.

The preliminary ICD is consolidated, formally issued & becomes the controlled ICD for ground & space segment PDRs.

After this stage, changes of the controlled ICD are possible in accordance with the simplified interface control process defined in clause 5.5.

Prior to the ground & space segments CDRs, under the responsibility of the space system responsible, the controlled ICDs is consolidated to become frozen, & approved by all parties.

The approved frozen ICD is considered the final ICD, ready to permit the segments suppliers to start manufacturing, integration, implementation, or verification activities.

After reaching this stage, changes of the frozen ICD are possible in accordance with the interface control process defined in clause 5.5.

【**Fig.45:** Typical space to ground segment interface life cycle】  
![](f/doc/ecss/ecss_est1024/45.png)


**Interface management life cycle involving OTS products**

Figure 46 shows the life cycle of an interface involving a category A or B off‑the‑shelf product & a category C or D product. Refer to ECSS‑E‑ST‑10‑02 Table 5‑1 for the definition of the product categories.

The life cycle starts with the identification of the interfaces by the customer (typically between customer SRR & PDR) & the selection of the off‑the‑shelf product.

At the Equipment Qualification Status Review (EQSR), the interface end definition of the off‑the‑shelf product is captured in the off‑the‑shelf product IDD. The IDD will be then incorporated into the IRD & ICD.

For the item interfacing with the off‑the‑shelf product, the generic process of clause 4.3.1 applies, ensuring no modification of off‑the‑shelf product interface definition.

At the CDR of the item interfacing with the off‑the‑shelf product, the approved frozen ICD is considered the final ICD, ready to permit suppliers to start manufacturing, integration, implementation, or verification activities.

After reaching this stage, changes of the frozen ICD are possible in accordance with the interface control process defined in clause 5.5.


【**Fig.46:** Typical interface management life cycle involving OTS】  
![](f/doc/ecss/ecss_est1024/46.png)



## 5. Requirements

### 5.1. Interface management planning

An interface management plan shall be established by the interface responsible to describe the Interface Management approach.

The interface management plan may be part of the Project Management Plan (ECSS‑M‑ST‑10 Annex A) or part of the System Engineering Plan (ECSS‑E‑ST‑10 Annex D) or a self‑standing document, subject to agreement between the interface responsible & its customer.

The chosen approach depends on the complexity of the project.

The interface management plan shall define the applicability of this standard.

The interface management plan shall define tailoring & adaptation of this standard to Project needs & life cycle.

While interface management steps are mandatory, the way in which each step is documented can be tailored according to the rules defined in each clause of this standard.
The interface management plan shall define at least the:

1. applicability domain,
2. management logic,
3. responsibility & approval authorities,
4. interface consolidation approach & life cycle,
5. schedule,
6. documentation tree,
7. internal documentation organization approach (contractual, discipline or product decomposition),
8. change management approach.


### 5.2. Interface identification

The supplier shall list all the interfaces involved within his product.

The interfaces list of 5.2a shall include:

1. all interfaces crossing contractual boundaries,
2. any interface identified by the customer.

Criteria used by the customer to identify additional interface to be managed within the supplier product, are for example reuse, technical risk, criticality.

For each interface identified in the list of 5.2b, the following shall be provided:

1. involved interface ends,
2. contractual responsibilities,
3. technical documentation & their version specifying & defining the interface.

The information in the list of 5.2b, shall be updated & distributed to the involved actors every time that any applicable document is updated or a baseline established. This interface information can be in the form of an Interface Identification Document (IID), as defined in Annex D.


### 5.3. Interface requirements specification

The customer shall define the interface requirements in conformance with the IRD DRD in Annex A. (1) Information regarding the expected delivery of the document for each project review is provided in Annex A of ECSS‑E‑ST‑10. (2) As indicated in DRD Annex A, the IRD content can be merged together with Technical Specification.

The complete set of interface requirements shall be specified by the customer prior to the start of the supplier’s contract.

The precedence of the IRD with respect to other project documents shall be defined.

Suppliers shall provide their compliance status to the interface requirements.

Nonconformance to interface requirements shall be treated as major NCR in accordance with the NCR process as defined in ECSS‑Q‑ST‑10‑09.


### 5.4. Interface definition

The interface responsible shall perform the following tasks:

1. collection of interface end data from involved actors;
2. sharing of interface data with involved actors;
3. coordination of the interface definition compliance with respect to interface requirements of clause 5.3;
4. checking of interface end data from suppliers, for:
    1. consistency,
    2. mutual compatibility.
5. baselining of the interface definition;
6. communicating interface changes among involved actors.

The actors shall be responsible of the interface end, by means of:

1. contributing to interface definition, providing an IDD, an one‑end ICD, or relevant technical design data, in conformance with the IDD DRD in Annex C;
2. supporting the iterations of the interface definition, in order to achieve an agreed baseline;
3. ensuring interface end compatibility with respect to: interface requirement, interface definition.

For each identified & specified interface, the interface responsible with the contribution of the involved actors shall define interface characteristics in terms of:

1. interface plane definition;
2. interface behaviour identification;
3. data affecting the interface definition.

Examples for 5.4c.1 are: mounting surface (between hardware products), Application Program Interface (between software products), communication network (between data systems). Examples for 5.4c.2 are: static, dynamic, nominal/off nominal, "state machine". Examples for 5.4c.3 are: dimensions, tolerances, coordinates, voltage, data format, temperature, load, heat flow, material, surface treatment, external standard reference.

To define the interfaces, the interface responsible shall provide an ICD in conformance with the ICD DRD in Annex B.

The ICD shall be put under configuration control prior to interface end suppliers PDR.

Nonconformance to ICD shall be treated as major NCR in accordance with the NCR process as defined in ECSS‑Q‑ST‑10‑09.


### 5.5. Interface control & approval

Modifications to an ICD shall be managed in conformance with ECSS‑M‑ST‑40, adapting the process as defined in Clause 4.2.6.

The interface responsible shall communicate the approval or non‑approval of an interface definition change with a rationale to all the actors.

The interface definition change shall be managed through a CR.

The CR shall be agreed & signed by each of the actors & by the interface responsible before it becomes applicable for implementation.

The interface definition agreed change shall be listed, until relevant CR content is incorporated in a new version of a baseline ICD. Interface definition agreed change is as documented in the agreed CR.

The ICD shall be consolidated to the frozen status, prior to the CDR of the first product involved in the interface.

The frozen ICD shall be approved (signed) by all involved parties.



### 5.6. Interface verification & validation

Interface requirement verification shall be performed in conformance with ECSS‑E‑ST‑10‑02.

The approach for verifying interfaces shall be defined by the customer, identifying the type & method of verification activity. One of the following types of verification, or a combination: (1) Stand‑alone verification activity under supplier responsibility, (2) Joint verification activity, performed under customer responsibility with supplier support.

The customer shall define the supplier support needed for any joint verification activity.

Any stand‑alone verification shall be completed successfully prior to the joint verification applicable to the same interface.

The customer & the suppliers shall agree whether interface validation is included or not.

If validation is included, the customer & suppliers shall agree on the interface validation approach.



## Annex A (normative) Interface Requirements Document (IRD) — DRD

**A.1 DRD identification.** *Requirement ID & source* — ECSS‑E‑ST‑10‑24, requirement 1.1.1.1.1j. *Purpose & objective* — for a product, the **interface requirements document (IRD)** is a specific type of technical requirements specification that defines the requirements for an interface or a collection of interfaces. The IRD is a document either included in or called up by a technical requirements specification (TS) as per ECSS‑E‑ST‑10‑06.

**A.2 Expected response**

*Special remarks.* The content of the IRD **may be merged with the Technical Requirements Specification** (as per ECSS‑E‑ST‑10‑06 Annex A) of the product.

*Scope & content:*

1. **Introduction.** The purpose, objective, content & the reason prompting its preparation.
1. **Applicable & reference documents, Definitions & abbreviations.** The applicable & reference documents in support to the generation of the document. Clearly define the relationship & precedence of the IRD to other project documents. Include the following references: ➀ Product tree (as per ECSS‑M‑ST‑10 Annex B), ➁ Specification tree (as per ECSS‑E‑ST‑10), ➂ References to requirements specified in other Technical requirement specifications or Standards which are applicable for a particular Interface (for example, an IRD can refer to individual requirements coming from Technical Requirement Specification such as GDIR (General Design & Interface Requirements) or SSS (System Support Specification) or Standards such as MIL‑STD‑1553 Bus). Confirm that the IRD does not include duplicate or conflicting requirements already specified in another Technical requirements specification applicable to any interface end. Define the applicable dictionary or glossary & the meaning of specific terms or abbreviation utilized in IRD.
1. **Interface requirements**
   1. Define the physical, functional, procedural & operational interface requirements between 2+ items in the Product tree & ensure hardware & software compatibility. (NOTE Interface requirements include for example, physical measures, definitions of sequences, of energy or information transfer, design constraints, & all other significant interactions between items)
   1. Specify Interface Requirements in accordance with ECSS‑E‑ST‑10‑06.
   1. Accompanie each interface requirements by its verification requirements.
   1. Define the verification responsibility for each verification requirement as per A.2.1<4>f (this can be joint responsibility, stand‑alone responsibility or any combination of them).
   1. Specify the requirements of an interface, for all identified interface ends.
   1. Specify for each requirement the applicability to each interface end.
   1. In case of an existing interface end design, the interface requirements shall address: ➀ interface plane definition, ➁ interface behaviour definition, ➂ data affecting the interface definition.
   1. In addition to the general requirements defined in ECSS‑E‑ST‑10‑06, for each interface requirement specify the applicability to each interface end.
   1. In case the IRD addresses more than one interface, group the interface requirements either by interface end pair, by nature of interface, or by contractual party. (An example of grouping by nature of interface is arranging the requirements by discipline such as mechanical, electrical, thermal & software/data; see also Annex E)
1. **Items to be addressed**
   1. Address for each interface, the following aspects: ➀ interface description using tables, figures, or drawings, ➁ units of measure including scale of measure, ➂ tolerances or required accuracies, ➃ in case of using non‑SI quantities or units, conversions & conventions, ➄ interface plane specification, ➅ coordinate system specification, in case of interfaces involving geometric aspects, ➆ interface behavior specification, including limitation of use. (Example of interface plane is the separation plane between launch adapter & spacecraft; example of limitation of use is decrease in capability)
   1. Interface requirements shall be built starting from the Reference Interface Data List of Annex E & adding all data necessary to specify the interface. It’s good practice to group the interface requirements by the interface natures as per Annex E.



## Annex B (normative) Interface Control Document (ICD) — DRD

**B.1 DRD identification.** *Requirement ID & source* — ECSS‑E‑ST‑10‑24, requirement 1.1.1.1.1r. *Purpose & objective* — the **Interface Control Document (ICD)** is to define the design of the interface(s) ensuring compatibility among involved interface ends by documenting form, fit, & function. The ICD can be, either: ➀ a single self‑standing document defining the interface; ➁ a single document, referencing other documents (e.g. IDD, single‑end ICD, interface drawings), which define separately the involved interface ends; ➂ a single document Annexing a set of IDDs (or single‑end ICDs), made integrally & jointly applicable to all the involved interface ends. In this case, the entire set of documents (issued by the suppliers, coordinated by the customer & approved by all the involved actors) is integrally & jointly controlling the interface: therefore, the rules & requirement hereafter specified are intended to be applicable either to the single ICD or jointly to the entire set of documents, depending on the selected approach. The ICD is managed by the customer (or his delegate) & concurred by all the involved actors. The ICD is used: ➀ to document the interface definition, ➁ to control the evolution of the interface, ➂ to document the design solutions to be adhered to, for a particular interface, ➃ as one of the means to ensure that the supplier design (and subsequent implementation) are consistent with the interface requirements, ➄ as one of the means to ensure that the designs (& subsequent implementation) of the participating interface ends are compatible.

**B.2 Expected response**

*Special remarks* — None.

*Scope & content:*

1. **Introduction.** The purpose, objective, content & the reason prompting its preparation.
1. **Applicable & reference documents, Definitions & abbreviations.** The applicable & reference documents & standards in support to the generation of the document. Clearly define the relationship & precedence of the ICD to other programme documents. Include the following references: Product tree (as per ECSS‑M‑ST‑10 Annex B), Specification tree (as per ECSS‑E‑ST‑10). Define the applicable dictionary or glossary & the meaning of specific terms or abbreviation utilized in ICD.
1. **Responsibility.** State the responsibilities of the interfacing organizations for development of the ICD. Define the document approval authority (including change approval authority). State the interface ends responsible.
1. **Interface definition.** The interface responsible shall establish a complete & exhaustive list of technical characteristics of the interface to be reflected in the interface definition/data. NOTE Informative Annex E lists a number of example aspects to be considered in interface definition. The list is meant to be used as a starting point but cannot be considered exhaustive.
   1. For each identified & specified interface, the interface responsible accounting for the contribution of the involved actors shall define interface characteristics in terms of:
      1. interface plane definition (Examples: mounting surface (between hardware products), Application Program Interface (between software products), communication network (between data systems))
      1. interface behaviour identification, including limitation of use (Examples: static, dynamic, nominal/off nominal, state machine. Possible limitation of use is coming from a decrease of capability)
      1. interface description using tables, figures, drawings, models, data base as appropriate (Examples: mechanical drawing, interface 3D CAD model, electrical circuit schematic, software API, software architectural or detailed design document)
      1. data affecting the interface definition (Examples: dimensions, tolerances, coordinates, voltage, data format, temperature, load, heat flow, material, surface treatment, external standard reference)
      1. units of measure including scale of measure
      1. tolerances or required accuracies
      1. in case of using non‑SI quantities or units, conversions & conversions.
   1. Detail the interface definition between two (or more) interface ends.
   1. Group the ICD by contractual, discipline or product decomposition (product tree).
   1. Each ICD version shall identify the incorporated approved CR.
   1. Group the interface definitions per pair of interface ends having a common interface plane.
   1. In case the ICD addresses more than one interface, the interface definition shall be grouped either per physical/logical interface, or per nature of interface.
   1. An example of physical/logical grouping is arranging the interface data by a pair of interface ends (see Figure B.1). An example of grouping by nature is arranging the interface data by discipline such as mechanical, electrical, thermal & software/data (see Figure B.1). It’s a good practice to group the interface requirements by the interface natures defined in Annex E.

【**Figure B.1** — Examples of interface data grouping in ICDs】

1. **A**  <= α,β =>  **B** <= α,β =>  **C**
   1. Interface data groupling by phisycal/logical interface
      1. A <= => B
         1. Interface data 1 (interface nature α)
         1. Interface data 2 (interface nature β)
         1. …
      1. B <= => C
         1. Interface data 3 (interface nature α)
         1. Interface data 4 (interface nature β)
         1. …
   1. Interface data grouping by interface nature
      1. Interface nature α
         1. Interface data 1 (interface nature α)
         1. Interface data 3 (interface nature α)
         1. …
      1. Interface nature β
         1. Interface data 2 (interface nature β)
         1. Interface data 4 (interface nature β)
         1. …



## Annex C (normative) Interface Definition Document (IDD) or Single‑end Interface Control Document — DRD

**C.1 DRD identification.** *Requirement ID & source* — This DRD is called from ECSS‑E‑ST‑10‑24, requirement 5.4b.1. The supplier of an interface end is responsible for the associated IDD.  *Purpose & objective* — The purpose of the IDD is to document the current design of an interface end. Often, the IDD is called 「Single‑end ICD」. In such case, there is no need to change the name of this document. The 」Single‑end ICD」 is sometimes referred as 「unit ICD」 or 「equipment ICD」. When interface is controlled by a set of IDDs/Single‑end ICDs, it is suggested if possible, to change the name of IDD in 「Single‑end ICD」 at interface baselining. The IDD is a unilateral document issued & controlled by the interface end supplier. The IDD is used: to document the interface end definition, to control the evolution of the interface end, to document the design solutions to be adhered to, for a particular interface end, as one of the means to ensure that the supplier design (and subsequent implementation) are consistent with the interface requirements & interface definition, as one of the means to ensure that the designs (and subsequent implementation) of the participating interface ends are compatible.

**B.2 Expected response**

*Special remarks* — Form.

1. The IDD may take different forms depending on the kind of interface end it documents. Examples of different forms are: a mechanical drawing, interface 3D CAD model, electrical circuit schematic, software API, software architectural or detailed design document.
2. The IDD may be a container with unambiguous references into the supplier's Design Definition File (DDF). IDD covers the formerly called "interface inputs" from the supplier to the customer.

*Scope & content:*

1. **Introduction.** The IDD shall contain a description of the purpose, objective, content & the reason prompting its preparation.
2. **Responsibility.** The IDD shall be issued & controlled by the interface end supplier. The document approval authority (including change approval authority) shall be defined.
3. **Applicable & reference documents.** The IDD shall list the applicable & reference documents & standards in support to the generation of the document. The relationship & precedence of the IDD to other programme documents shall be clearly defined.
4. **Definition of terms & abbreviated terms.** The applicable dictionary or glossary & the meaning of specific terms or abbreviation utilized in IDD shall be defined.
5. **Interface end definition.** The IDD shall contain:
    1. interface end description & data definition using tables, figures, drawings, models, data base as appropriate,
    2. units of measure including scale of measure,
    3. tolerances or required accuracies,
    4. in case of using non‑SI quantities or units, conversions & conventions,
    5. interface plane implementation,
    6. interface end behaviour,
    7. interface end data. Interface end data shall be built starting from the Reference Interface Data List of Annex E & further identify all data necessary to document the design of the interface end. It is recommended to group the interface end data by the interface natures defined in Annex E.


## Annex D (informative) Proposed content of an Interface Identification Document (IID)

**Purpose & objective** — The interface identification document (IID), as proposed by the Note of requirement 5.2d, contains the index of all identified interfaces. For completeness, the IID also list the external interfaces. For each interface the IID lists the references to the applicable IRD, IDD, ICD & change requests, including version & responsible for each document The IID also lists reference to any standard that is applicable for all interfaces it describes.

*Special remarks:*

1. The IID is intended to be a living document that is actively maintained to be up‑to‑date for the duration of a programme or project. Therefore the IID can be implemented in the form of a web / database application.
2. The interface records can be grouped by discipline, by contract or WBS element or by item within the product tree.
3. Examples for interface record are:
    1. Grouped by item within the product tree:
        1. Mission Control System <‑> Ground Station
        2. Space Segment <‑> Mission Control Centre
        3. XB Antenna Adapter <‑> Platform
        4. Payload <‑> EGSE
        5. UNIT 1 <‑> UNIT 2
    2. Grouped by discipline:
        1. UNIT 1 <‑> UNIT 2 Electrical Interface Signalling
        2. UNIT 1 <‑> UNIT 2 Electrical Interface Power Supply
        3. UNIT 1 <‑> UNIT 2 Mechanical Interface
        4. UNIT 1 <‑> UNIT 2 Data Interface Commanding
        5. UNIT 1 <‑> UNIT 2 Data Interface Telemetry
    3. Grouped by contract or WBS element:
        1. UNIT 1 <‑> UNIT 2 Company 1 Interface (electrical, mechanical)
        2. UNIT 1 <‑> UNIT 2 Company 2 Interface (data)

*Scope & content:*

1. **Introduction.** The IID describes the purpose, objective, content & the reason prompting its preparation. The IID describes/illustrates the interface context at the appropriate decomposition level, i.e. showing the items being interfaced, by referencing the applicable Product & architecture documentation or including a context interface diagram.
2. **Applicable & reference documents.** The IID lists the applicable & reference documents in support to the generation of the document. The IID includes the following references: Product tree (as defined in ECSS‑M‑ST‑10 Annex B), Standards applicable (e.g. ECSS, CCSDS, Project defined standards).
3. **Interface list.** The identified interfaces are listed as a series of interface records. Each interface record contains as a minimum the following fields:
    1. unique interface identifier,
    2. reference to the participating interface ends,
    3. interface responsible,
    4. interface actors responsible of each interface end,
    5. references to applicable documents, i.e. interface requirements baseline, ICD, IDD, CR, which defines the baseline of the interface addressed in the record. (1) Initially, the list of references can be empty. It will be populated over the life cycle of the interface. (2) Use specific references, i.e. include document identifier, version identifier, relevant section number.



## Annex E (informative) Reference interface data list

### E.1. Introduction

This annex describes the reference list of interface data to take into account when creating or updating an IRD, ICD or IDD.

As it is impossible to produce an exhaustive list that is valid for any space project, this list can be used as a reference & starting point & is by definition not exhaustive. However where applicable it is good practice to use the terms & grouping of this list, in order to promote the uniformity of interface specifications & definitions.

The reference list of interface data is described in this annex at two levels:

1. Clause E.2 contains interface data characterization & the description of the list, identifying the interface natures & the ECSS disciplines to which they belong.
2. Clause E.3 expands this information, listing the interface data, grouped by natures as identified in clause E.2.

This annex has been built to have data relevant listed under the header of the discipline but avoiding as much as possible repetition of data (e.g. geometry is listed under mechanical but is needed for many other disciplines). Being listed under one discipline does not exclude the relevance of the data for other disciplines.


### E.2. Interface nature & data characterisation

**E.2.1. Interface nature**

The interface data are grouped by interface nature, with further hierarchical decomposition into subgroups of related data where deemed useful.

The nature of an interface designates a grouping of related interface data, typically in an engineering discipline oriented way.

Table E‑1 lists the identified interface natures. The order follows the numbering of the main ECSS discipline.

【**Table E‑1:** Identified interface natures & corresponding ECSS disciplines】

|**Interface nature**|**Corresponding ECSS discipline(s)**|
|:--|:--|
|Coordinate System & Time E‑10 — System engineering
Space environment E‑10 — System engineering
Man‑machine E‑10 — System engineering
Electrical E‑20 — Electrical & optical engineering
Optical E‑20 — Electrical & optical engineering
Thermal control E‑30 — Mechanical engineering
Structural E‑30 — Mechanical engineering
Mechanisms E‑30 — Mechanical engineering
Propulsion E‑30 — Mechanical engineering
Aerothermodynamics E‑30 — Mechanical engineering
Hydraulics E‑30 — Mechanical engineering
Data representation E‑40 — Software engineering
E‑50 — Communications
E‑70 — Ground systems & operation
Software E‑40 — Software engineering
Communications E‑50 — Communications
Control E‑60 — Control engineering
Operations E‑70 — Ground systems & operation
Materials Q‑70 — Materials, mechanical parts & processes

Interface data characterization
Interface data are characterized by specifying:
the applicable engineering units, scales & conversion rules for any numerical characteristic (parameter).
the applicable value(s) for any numerical characteristic (parameter), including:
nominal value or value range
accuracy
uncertainty
tolerance
margin (relative or absolute)
permissible error
probabilistic & distribution characteristics (e.g. kind of distribution, standard deviation, power spectral density, energy spectral density).
the applicable values for the relevant modes, for any characteristic that is mode dependent.
Interface data list
Coordinate system & time
Coordinate system (see ECSS‑E‑ST‑10‑09 for details):
Inertial Coordinate Systems
Orbital Coordinate Systems
Launcher Coordinate Systems
Satellite‑fixed Coordinate System (generic platform & payload)
Body‑fixed Rotation (planet) Coordinate Systems
Topocentric Coordinate Systems
Test facility Coordinate Systems
Simulator Coordinate Systems
Processing / Product Coordinate Systems (e.g. equipment, unit, sub assembly, part).
Euler angle definition (see ECSS‑E‑ST‑10‑09 for details).
Attitude quaternion convention (see ECSS‑E‑ST‑10‑09 for details).
Time reference:
Reference
Format
Adjustment update & reset
Relationship with external time sources
Relationship between time systems.
Space environment
Particle radiation fluxes:
Mission average electron energy spectrum
Mission average proton energy spectrum
Solar proton fluence spectrum
Peak electron energy spectrum
Peak proton energy spectrum
Peak solar proton flux spectrum.
Mission dose‑depth curve:
Ionising dose
Non‑ionising dose
Effective dose equivalent (radiobiological).
LET spectra
Damage‑equivalent fluences for solar cells
Plasma environment:
Worst‑case surface charging environment (bi‑maxwellian temperatures & densities)
Mean plasma environment
Cold plasma density, temperature, composition, relative velocity (high‑voltage interactions & ram‑wake effects).
Micro‑particle environment (size & velocity distribution):
Micro‑debris (non‑trackable population)
Micrometeoroids.
Planetary magnetic field:
Mean field strength, direction
Short term variations & activity indices.
Solar activity:
Solar irradiance spectrum (including UV)
Activity indices (F10.7, Sunspot number)
Minimum, mean & maximum constant values for short & long periods
Reference values over one solar cycle.
Earth atmosphere:
Total & constituent (e.g. atomic oxygen) densities for minimum, mean & maximum solar & geomagnetic activity 
Composition for minimum, mean & maximum solar & geomagnetic activity.
Planetary atmosphere:
Climate database (global distributions of atmospheric parameters).
Induced permanent deposit.
Contamination from purge.
Solar panels contamination.
Droplets contamination.
Man‑machine
Anthropometry & bio‑mechanics:
Body size
Joint Motion
Reach
Neutral Body Posture
Body Surface Area
Body Volume
Body Mass Properties 
Strength.
Sensation & perception:
Vision
Auditory System
Olfaction & Taste
Vestibular system
Kinaesthesia
Reaction Time
Coordination.
Environment:
Atmosphere
Microgravity
Acceleration
Acoustics
Vibration
Radiation
Thermal Environment.
Human‑computer interface:
Data Display
Text
Tables
Graphics
Coding
Window Displays
Format Design
Information Display Rate
User Computer Dialogues
Movement within User Interfaces
Manipulating Data
User Guidance.
Electrical
Power interface data:
Power consumption
Input & Output Power interfaces:
Minimum / maximum voltage
Impedance
Undervoltage switch‑off
Overvoltage protection
Output voltage on/off time (rise/fall time)
Current (maximum operating, inrush, inrush duration, current rate of change)
Over‑current protection (type, nominal current, current limitation & duration)
Drop‑out limits.
Grounding & insulation:
Grounding concept
Internal grounding diagram
Insulation.
Signal interface data:
Signal type (e.g. analogue, discrete, bi‑level, multi‑level)
Signal function (e.g. command, measurement, status, clock) 
Circuit diagram / signal level diagram
Electrical characteristics (depending on the technology, e.g. static, dynamic & protection):
Signal characteristics
Signalling system (e.g. single ended, differential)
Signal transfer (e.g. DC‑coupled)
Signal level (interface voltage low / high)
Transfer function for analogue signal
Interface resistance (open / closed condition)
Logical representation (e.g.: 0 = low = false; 1 = high = true)
Driving capability
Operating current
Minimum open circuit voltage
Impedance (line‑line)
Ripple & signal‑to‑noise ratio
Common mode voltage (CMV, CMRR)
Overvoltage protection
Fault voltage emission
Fault current emission
Current limitation
DC isolation (to structure)
Interface behaviour specifics in OFF condition.
Harness interfaces:
Connector interface data — per connector:
Specific requirements or remarks
Identifier (Jxx / Pxx)
Function
Part number
Coding (e.g. key, colour)
Number of pins
Gender (male/female)
Specification
Lockers /Backshell part number & material.
Connector interface data — per pin:
Pin number
Component type / specification
Function
Polarity (P = plus, M = minus, R = reversible).
Wiring:
Type of cable (e.g. single wire, twisted shielded pair, coax)
Wire gauge
Insulation
Shielding / grounding
Impedance
Maximum/minimum cable length
Coding (e.g. colour).
Bundle:
Composition
Protection/Coating/Shielding
Coding
Constraint (e.g. minimum bending radius, maximum unsupported length).
Telemetry / telecommand list & description.
Data bus interface:
Physical interface:
Standard interface (e.g. Mil‑1553‑STD, Spacewire, EIA485)
Detailed description.
Data interface:
Data bus architecture (star or bus topology)
Coding (encoding, decoding)
Bit rate / data rate
Bit error rate
Data protocol / structure / framing (e.g. address, data fields, checksum)
Word length (number of bits per word)
First bit in transfer (MSB, LSB)
Data packet description
Signal / data representation
Communication / data traffic rules (master / slave, broadcast messages, acknowledgement of receipt)
Synchronisation (synchronous / asynchronous data transfer)
Timing
Start / end delimiters
Response time
Jitter
Error handling
Bus protocol (e.g. Ethernet, CCSDS).
Optical
Optical characteristics of surfaces (e.g. body, solar arrays, radiators, MLI, coatings lenses, windows):
Reflectivity
Absorptivity
Emissivity
Transmissivity
Specularity
Brightness definition, coefficients.
Spectrum (band centre, bandwidth, distribution)
Light sources
Reflectors
Video target (layout, characteristics)
Optical sensors passive or active (layout, characteristics, field of view)
Coatings & surface finishes
Ranging cues (layout, characteristics)
Thermal control
Baseplate thermal contact area
Special thermal mounting constraints (e.g. thermal fillers, washers, thermal insulation)
Heat transfer mode / environment (conduction, radiation, convection)
Temperature reference point(s) 
Temperature range for each mode (e.g. operational, non‑operational, storage, transport), & minimum switch‑on temperature
Temperature stability at temperature reference point(s)
Thermal gradients
Heat capacity
Solar absorptance for exposed surfaces (BOL, EOL)
Infra‑red emittance for surfaces (BOL, EOL)
Contact area coplanarity
Contact area flatness
Contact area roughness
Minimum, nominal, maximum dissipated power (e.g. from unit, from heater) & relevant time‑profile for each phase/configuration/life condition (e.g. BOL, EOL)
Maximum power density (e.g. for heater, for heat pipes)
Authorized zones definition, external location for application of thermal hardware (e.g. MLI, heaters, thermostats, thermistors, Velcro) 
Interface thermal mathematical models
Humidity & condensation (for Environmental Control & Life Support, materials & equipment)
Interface heat flux & heat flux limits at all thermal interfaces
Structural
Reference coordinate system definition & reference hole
Envelope dimensions
Mass (nominal, uncertainty, dispersion)
Centre of gravity (nominal, uncertainty, dispersion)
Moments of inertia (nominal, uncertainty, dispersion)
Mounting hardware definition (standard)
Mounting holes size & location
Mounting pads thickness for screw length definition
Geometrical tolerances
Venting holes location
Special mounting & maintenance geometrical constraints
RF ports geometry
RF ports location & orientation
Connector locations
Torques applied 
Recommended torque for mobile part
Bonding, grounding studs, type identification & fixing point location
Alignment devices location & definition
Adjustment & alignment information
Contact area materials, coatings & finishing
Contact surface flatness & roughness
External material & coating per zone
Interface description for special handling & installation
Pneumatic interface geometrical definition
Hydraulic interface geometrical definition
Configuration & layout (including stay‑in & stay‑out zones) 
Related loose parts (bolts, nuts, washers, …)
Static loads, dynamic loads, shock, acoustic vibration
Acoustic noise (frequency, spectrum, level)
Interface mechanical mathematical models
Mechanisms
Mobile parts & motion
Static geometrical envelopes (stowed & deployed)
Deployment envelope (kinematic)
Operational envelope
Lubrication
Design life (Sequence & timing, number of operations & activations)
Propulsion
Propulsion overall:
Performance tables
Performances at the propulsion reference point
Nominal performance ranges for the pressure regulated domain
Global performance ranges for the whole nominal operating domain
Global performance ranges in off‑nominal situation
Thrusters use limits
Modes
Monitoring
Operations
Propellant level
Propellant budget
Thrusters:
Maximum thrust
Minimum thrust
Utilization
Commanding
Induced plume environment:
Thrusters plume model
Thrusters configuration
Thrusters firing scenarios
Contamination
Pressure
Aerothermodynamics
Reference surface
Time history of aerodynamics & aerothermodynamics databases variables & uncertainties 
Hydraulics
Geometrical definition (e.g. tube ends, flanges, holes)
Applicable fitting/disconnect standard PN
Fitting envelope constraints
Transported fluid
Filtration requirements
Other receive/supply fluid quality
Liquid/gas interface temperatures
Interface pressures
Interface flow rates
Allowed pressure drop/ required pressure rise
Contact area materials & finishing constraints
Allowed liquid/gas loop leakages
Loop isolation requirements
Liquid/gas exchange conditions
Data representation
Data representation
Bit numbering
Endianness (big, little)
Data transmission ordering
Unused words & bits
Data types definition
Software
Operating system
Operating hardware
Required software libraries
Required compiler, build environment
API definition (i.e. function prototype/signature, input/output parameter format & type, exceptions, not implemented/not allowed functionality, non‑functional related effects, etc.)
Errors & warnings returned
Timing constraints
Shared memory location & content
Semaphores & locks
Memory map
Symbol map
Map of registers — including name, physical address & read/write instructions)
Map of inputs/outputs — including name, physical address & read/write instructions
List of interrupts
Telecommand interface & data
Telemetry interface & data
List of available services/functions
Thread‑safe compliance
Memory load, dump & check procedures/services
Smallest addressable unit
Communications
Radio frequency links:
Frequency,
Spectrum
Band
Transmitted (total) power
Noise spectral density
Receiver band sensitivity
Receiver allowed maximum input power in OFF condition
Gain
Radiation pattern
EIRP
Angular beam‑width
Signal polarisation characteristics
Cross polarisation leakage
Attenuation
System noise temperature
Link budget
Modulation & coding of RF links:
Modulation (e.g. analogue, digital, FM, AM, Phase modulation: QPSK, 8PSK)
Channel encoding (e.g. block, convolutional)
Error correction (e.g. RS)
Scrambling (e.g. polynomial)
Interleaving
CADU structure (ASM, Codeblock)
Transfer & segmentation layer on space links:
Transfer frame format:
Transfer Frame size
Transfer frame header
Transfer frame trailer
Virtual Channel list
Master Channel List
Multiplexing
Transfer frame error control
Segmentation
Idle frame
Space packet layer:
Space Packets Format:
Packets size
Packet header
Data field header
APID list 
Packet error control
Segmentation
Idle packet
Time formats specification
PUS Tailoring
Encryption
Encryption protocol management (e.g. keys management)
Commanding:
Use of BD/AD
Authentication
CCSDS packet acceptance & acknowledgement
Use of COP‑1/FOP‑1/FARM‑1
Criticality
Bus link protocols:
Logical layer (message composition & format, time features of, response time, no‑response time‑out, word interface)
Synchronous Packet Transfer Layer (cycles, subframe time division for message transmission)
Transport of nominal CCSDS packets over busses
Discrete lines
Functional/Operational type of communication:
Commands (e.g. list, function)
Telemetry/housekeeping data (e.g. type, name, unit, identifier)
Synchronisation logic
Link control per type of communication (on/off)
Data interface data:
Type of exchange (e.g. physical media, network)
File format
File naming
Format validation (e.g. XML Schema)
Data encoding
Data encryption (e.g. type, key type, key length)
Frequency of transfer (e.g. daily, weekly, intermittent, 1 Hz)
Timeliness of the transfer
Transfer protocol (e.g. FTP, HTTP, TCP, RTSP)
Error handling
Clean‑up policies (e.g. read & delete, read‑only, delete & write)
Retention Time
Permission
Physical media (e.g. DVD, LTO4, Hexabyte, CD, Hard Disk)
Formatting of physical media (e.g. ISO, NTFS, Joliet, HFS+)
Labelling & indexing
Initiator of transfer
Control
Reference guidance & navigation frame & attitude convention
Local orbital frame (TLOF)
Operations
Schedule/process
Events
Operational scenarios (nominal, alternative, contingency)
Modes (e.g. nominal operational, standby, hibernations, safe) 
Format of exchanged operation data (e.g. planning, instructions, requests, reports, acknowledgments, confirmations, alarms)
Actors (e.g. operator, operation engineer, operation manager)
Contact details (e.g. phone, e‑mail, address)
Materials
Material compatibility / constraints
Materials outgassing
Material Humidity



## The End

end of file
