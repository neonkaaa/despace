# Gilmour Space Technologies
> 2022.03.30 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/g/gilmour_st_logo1t.webp)](f/c/g/gilmour_st_logo1.webp)|<info@gspacetech.com>, +61 7 5549 2370, Fax …;<br> *62 Millaroo Drive, Helensvale QLD 4212, Australia*<br> <https://www.gspacetech.com> ~~ [FB ⎆](https://www.facebook.com/gilmourspacetech) ~~ [IG ⎆](https://www.instagram.com/gilmourspace) ~~ [LI ⎆](https://www.linkedin.com/company/gilmour-space-technologies) ~~ [X ⎆](https://twitter.com/gilmourspace) ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/Gilmour_Space_Technologies)|
|:--|:--|
|**Business**|Develop / launch low cost hybrid LV & engines for small sats to LEO|
|**Mission**|To provide affordable space launch services to the world’s fast‑growing small satellite industry|
|**Vision**|All orbits. All planets.|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO, Founder — Adam Gilmour<br> ・Head of Launch Operations, Co‑Founder — James Gilmour|

**Gilmour Space Technologies** (also known as **Gilmour Space**) is a venture‑funded Australian space company  that is developing hybrid‑engine rockets & associated technology to support the development of a low-cost space launch vehicle. Founded 2012.07.17.

We recognise that launch is bottleneck, & that launch costs & availability are two of the biggest hurdles for smallsat customers who are developing, testing & deploying New Space technologies.

<p style="page-break-after:always"> </p>

## The End

end of file
