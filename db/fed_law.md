# Федеральный закон
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [Doc](doc.md), [Control](control.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Федера́льный закóн** — русскоязычный термин. **Federal law** — англоязычный эквивалент.</small>

**Федера́льный закóн (ФЗ)** — закон, установленный федеральными законодательными органами федеративного государства.



## Федеральный закон Российской Федерации
**Федера́льный закóн** — федеральный законодательный акт Российской Федерации, принимаемый в соответствии с Конституцией Российской Федерации по предметам ведения Российской Федерации и по предметам совместного ведения Российской Федерации и её субъектов.

Федеральные конституционные законы и федеральные законы, принятые по предметам ведения Российской Федерации, имеют прямое действие на всей территории Российской Федерации. По предметам совместного ведения Российской Федерации и субъектов Российской Федерации издаются федеральные законы и принимаемые в соответствии с ними законы и иные нормативные правовые акты субъектов Российской Федерации.



## Федеральный конституционный закон Российской Федерации
**Федера́льный конституцио́нный зако́н Росси́йской Федера́ции** — разновидность федеральных законодательных актов, принимаемых в соответствии с Конституцией Российской Федерации по вопросам, предусмотренным Конституцией. Федеральный конституционный закон обладает повышенной юридической силой по сравнению с федеральным законом — федеральные законы не должны противоречить федеральным конституционным законам.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Control](Control.md)】**<br> [Ad hoc](ad_hoc.md) ~~ [Business travel](business_travel.md) ~~ [Chief designers council](cocd.md) ~~ [CML](trl.md) ~~ [Competence](competence.md) ~~ [Confident](confident.md) ~~ [Consp.theory](consp_theory.md) ~~ [Control sys. (CS)](cs.md) ~~ [Coordinate system](coord_sys.md) ~~ [Curator](curator.md) ~~ [Designer’s supervision](des_spv.md) ~~ [E‑sig](esig.md) ~~ [Engineer](se.md) ~~ [Errand](errand.md) ~~ [Federal law](fed_law.md) ~~ [Federal TP](fed_tp.md) ~~ [Federal SP](fed_sp.md) ~~ [GNC](gnc.md) ~~ [Gravity assist](gravass.md) ~~ [Industrial archaeology](ind_arch.md) ~~ [Instruction](instruction.md) ~~ [Lean manuf.](lean_man.md) ~~ [Lifetime](lifetime.md) ~~ [Manager](manager.md) ~~ [MBSE](se.md) ~~ [Meeting](meeting.md) ~~ [MCC](sc.md) ~~ [MIC](mic.md) ~~ [MML](mml.md) ~~ [MoU](contract.md) ~~ [Nav. & ballistics (NB)](nnb.md) ~~ [Nonprofit org.](nonprof_org.md) ~~ [NX](nx.md) ~~ [Oberth effect](oberth_eff.md) ~~ [Org.structure](orgstruct.md) ~~ [Outcomes commission](outccom.md) ~~ [Patent](patent.md) ~~ [Peter prin.](peter_principle.md) ~~ [Plan](plan.md) ~~ [PMBok](pmbok.md) ~~ [Quorum](quorum.md) ~~ [R&D management](mgmt.md) ~~ [R&D support](rnd_support.md) ~~ [Recursion](recurs.md) ~~ [Schulze_method](schulze_method.md) ~~ [Sci'N'Tech activities](st_act.md) ~~ [Sci'N'Tech council](satc.md) ~~ [Single-window system](sw_sys.md) ~~ [Situ.leadership](situ_leadership.md) ~~ [Skunk works](se.md) ~~ [State arm. plan](plan_sa.md) ~~ [Swamp](swamp.md) ~~ [Teamcenter](teamcenter.md) ~~ [Tennis racket theorem](tr_theorem.md) ~~ [TRIZ](triz.md) ~~ [TRL](trl.md) ~~ [V‑model](v_model.md) ~~ [Veto](veto.md) ~~ [Workflow](workflow.md) ~~ [Workgroup](wg.md)|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Federal_law_(disambiguation)>
1. <https://ru.wikipedia.org/wiki/Федеральный_закон_Российской_Федерации>
1. <https://ru.wikipedia.org/wiki/Федеральный_конституционный_закон_Российской_Федерации>



## The End

end of file
