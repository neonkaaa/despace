# KT SAT
> 2020.07.25 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/k/ktsat_logo1t.webp)](f/c/k/ktsat_logo1.webp)|<ktsat@kt.com>, +82-2-1577-7726, Fax …;<br> *13~14F KT Seonleung Tower, 422 Teheran-ro, Gangnam-gu, Seoul, 06193, Korea*<br> <http://ktsat.net> ~~ [LI ⎆](https://www.linkedin.com/company/ktsat)|
|:--|:--|
|**Business**|Telecomms|
|**Mission**|To ensure that no matter where you go on this planet & beyond, you will never lose touch with those who matter.|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**KT sat** is a South Korean satellite service provider. Founded 2012.12.04.

We provide seamless telecommunication & broadcasting service to Asia, Africa, & the Middle East as well as airspace, maritime regions, & isolated locations.  We operate 5 satellites incl. the KOREASAT-7 we launched through Arianespace & the KOREASAT-5A launched through SpaceX. Our satellites were built by [Thales Alenia Space](thales_as.md).



## The End

end of file
