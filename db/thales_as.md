# Thales Alenia Space
> 2019.08.05 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/t/thales_as_logo1t.webp)](f/c/t/thales_as_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *Канны, Франция*<br> <http://www.thalesgroup.com/en/worldwide/space?:LangType=2057>|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Thales Alenia Space**, также **Аления**, они же **Саления** — франко‑итальянский производитель в области аэрокосмической техники.



## The End

end of file
