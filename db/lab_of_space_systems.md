# Laboratory of Space Systems
> 2020.07.15 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/l/lab_of_space_systems_logo1t.webp)](f/c/l/lab_of_space_systems_logo1.webp)|<spacesystem@mech-hm.eng.hokudai.ac.jp>, <mark>nophone</mark>, Fax …;<br> *Nishi8-Chome, Kita13-jo, Kita-ku, Sapporo-shi, Hokkaido 060-8628, Japan*<br> <https://mech-hm.eng.hokudai.ac.jp/~spacesystem/index_e.html>|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Laboratory of Space Systems** is a division of Mechanical & Space Engineering, Graduate School of Engineering, Hokkaido University, Japan.

Projects:

- CAMUI Hybrid Rocket Engine
- Liquid Droplet Radiator
- Pulse Detonation Engine
- SOTV (The solar orbit transfer vehicle)
- The staged combustion hybrid rocket



## The End

end of file
