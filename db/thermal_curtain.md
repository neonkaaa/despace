# Тепловая шторка
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → **[СОТР](tcs.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Шторка / Жалюза** — русскоязычный термин. **Thermal curtain** — англоязычный эквивалент.</small>

**Шторка** (оно же жалюзи) ─ элемент СОТР, предназначенный для закрытия радиаторов для защиты их от попадающего на них солнца.

Визуально представляют собой открывающиеся/закрывающиеся жалюзи. Могут быть как автоматическими (сделанными из металла с памятью), либо полуавтоматическими (двигающимися посредством привода).



## Применение
Шторки применялись на следующих КА:

1. [SELENE](selene.md), 2007 год, Япония.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【Thermal control system (TCS)】**<br> [Thermal characteristics](thermal_chars.md) ~~ [Гермоконтейнер](гермоконтейнер.md) ~~ [Насосы для СОТР](сотр_насос.md) ~~ [Покрытия для СОТР](сотр_покрытия.md) ~~ [Радиатор](радиатор.md) ~~ [РИТ (РИТЭГ)](rtg.md) ~~ [Стандартные условия](sctp.md) ~~ [Тепловая труба](hp.md) ~~ [ТЗП](hs.md) ~~ [Тепловой аккумулятор](heat_bank.md) ~~ [ТСП](tsp.md) ~~ [Шторка](thermal_curtain.md) ~~ [ЭВТИ](mli.md)|

1. Docs: …
1. <…>


## The End

end of file
