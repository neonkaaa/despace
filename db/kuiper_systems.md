# Kuiper Systems
> 2022.04.01 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/k/kuiper_systems_logo1t.webp)](f/c/k/kuiper_systems_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *…, Redmond, Washington, U.S.*<br> [Wiki ⎆](https://en.wikipedia.org/wiki/Kuiper_Systems)|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・President — Rajeev Badyal|

**Kuiper Systems LLC** is a subsidiary of Amazon that was set up in 2019 to deploy a large broadband satellite internet constellation to provide broadband internet connectivity. The deployment is also referred to by its project name 「Project Kuiper」.

The satellites are projected to use an orbit with a height between 590 & 630 ㎞. Kuiper is planned to work in concert with Amazon’s previously announced large network of 12 satellite ground station facilities (the 「AWS Ground Station unit」) announced in November 2018. Amazon filed communications license documents with the U.S. regulatory authorities the FCC in July 2019, which included information that the wholly owned Amazon subsidiary that intended to deploy the satellite constellation was Kuiper Systems LLC, based in Seattle, Washington. As of April 2021, the Kuiper System is planned to consist of 3 236 satellites operating in 98 orbital planes in 3 orbital shells, one each at 590 ㎞, 610 ㎞, & 630 ㎞ orbital altitude.

The president of Kuiper Systems is Rajeev Badyal, a former vice president of SpaceX’s Starlink satellite internet constellation who was fired in 2018. In December 2019, Amazon announced that the team were expected to move headquarters to a larger R&D facility in Redmond, Washington, in 2020. However, an update continues to remain pending on the final move.

<p style="page-break-after:always"> </p>

## The End

end of file
