# TelePIX
> 2022.12.20 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/t/telepix_logo1t.webp)](f/c/t/telepix_logo1.webp)|<telepix@telepix.net>, +82-51-731-2884, Fax …;<br> *#301, 435-1 Haeyang-ro, Yeongdo-gu, Busan, Korea*<br> <https://telepix.net> ~~ [IG ⎆](https://www.instagram.com/telepix_official) ~~ [FB ⎆](https://www.facebook.com/Telepix-108234921714905) ~~ [LI ⎆](https://www.linkedin.com/company/telepix-korea) ~~ [X ⎆](https://twitter.com/TelePIX_)|
|:--|:--|
|**Business**|Sat images, data analytics, ground station operations|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・Managing Director — Seonghui Kim|

**TelePIX Co., Ltd.** is a South Korean company creating optical systems, CubeSats, and providing ground system management. Founded 2019.06.



## The End

end of file
