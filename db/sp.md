# Solar battery
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [SPS](sps.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Solar battery (SB), Solar panel (SP)** — EN term. **Батарея солнечная (БС)** — RU analogue.</small>

**Solar battery (SB)**, sometimes **Solar panels (SP)** — a combination of [photoelectric converters (photovoltaic cells)](sp.md) — semiconductor devices that directly convert solar energy into direct electric current, in contrast to solar collectors, producing heating of the heat carrier material.

**Application area**

|**[Mercury](mercury.md)**|**[Venus](venus.md)**|**[Earth](earth.md)**|**[Moon](moon.md)**|**[Mars]( mars.md)**|**[And further](index.md)**|
|:--|:--|:--|:--|:--|:--|
|+|in orbit|+|+|+|—|

**BS composition:**  
░╟ Construction  
░╙ Solar cells

【**Table.** Manufacturers】 Solar cells are manufactured in volume in Japan, Germany, China, Taiwan, Malaysia & the United States, whereas Europe, China, the U.S., & Japan have dominated (94 % or more as of 2013) in installed systems. Other nations are acquiring significant solar cell production capacity.

| | |
|:--|:--|
|**AE**|…|
|**AU**|…|
|**CA**|…|
|**CN**|…|
|**EU**|…|
|**IL**|…|
|**IN**|…|
|**JP**|・[Mitsubishi](mitsubishi.md)<br> ・[NEC](nec.md)|
|**KR**|…|
|**RU**|・[PAO Saturn](pao_saturn.md)|
|**SA**|…|
|**SG**|…|
|**US**|…|
|**VN**|…|



## Solar cell
> <small>**Solar cell (SC), Photoelectric converter (PEC)** — EN term. **Фотоэлектрический преобразователь (ФЭП)** — RU analogue.</small>

**Photocell**, also **photoelectric converters (PEC)** — an electronic device that converts the energy of photons into electrical energy. **Solar cell (SС)** is a direct converter of photon energy into electrical energy.

[![](f/sps/solar_cell_4inch_polyt.webp)](f/sps/solar_cell_4inch_poly.webp)  
*Photocell based on multicrystalline silicon*

Subdivided into vacuum & semiconductor photocells. The operation of the device is based on photoelectron emission or internal photoelectric effect.

The most effective, from the power engineering point of view, devices for converting solar energy into electrical energy are semiconductor PECs, since this is a direct one-step energy transfer. The efficiency of conventional PEC for terrestrial conditions is on average 16 %, for the best samples — up to 25 %; in laboratory conditions, an efficiency of more than 44 % has been achieved. The efficiency of PEC is influenced by:

1. Degradation of materials:
   1. often taken at 3 ‑ 5 %/year for the 1st 5 years (for 2021)
1. Losses in wires
1. Photocell contamination
1. Sun angle:
   1. deviations from ⊥ by 0 ‑ 30 °, the generated current decreases as per the angle’s cosine
   1. deviations from ⊥ by 30 ‑ 90 °, the generated current decreases exponentially
1. Temperature — the higher the ambient temperature, the less electricity is generated; at T ≥ +60 ℃, the PEC may not generate electricity at all

**Varieties:**

1. Gallium arsenide (GaAs) is a compound of gallium & arsenic. An important semiconductor, the third most widely used after Si & Ge. More efficient & lighter than Si, but more expensive. Efficiency 28.5 % for the SC [Luna-26](luna_26.md).
1. Perovskite.
1. Silicon (Si).

【**Table.** Resulting power of a solar cell, W/m² (in 1 year ・ in 3 years ・ in 5 years)】  
*The average filling factor is 83 % (For 2020). PEC degradation is 3 ‑ 5 %/y for the 1st 5 years. Average solar power is 1 361 W/m².*

|**Planet**|**PEC type**|**Theoretical**|**In orbit**|**On the surface**|
|:--|:--|:--|:--|:--|
|[Earth](earth.md)|GaAs (eff. 28.5 %)|389|311 ・ 275 ・ 254|… ・ … ・ …|
| |Si (eff. 21.3 %)|290|232 ・ 205 ・ 189|… ・ … ・ …|
|[Mars](mars.md)|GaAs (eff. 28.5 %)| |133 ・ 118 ・ 108|… ・ … ・ …|
| |Si (eff. 21.3 %)| |99 ・ 87 ・ 81|… ・ … ・ …|
|[Mercury](mercury.md)|GaAs (eff. 28.5 %)| |1 829 ・ 1 618 ・ 1 492|… ・ … ・ …|
| |Si (eff. 21.3 %)| |1 367 ・ 1 209 ・ 1 115|… ・ … ・ …|
|[Moon](moon.md)|GaAs (eff. 28.5 %)| |311 ・ 275 ・ 254|285 ・ 252 ・ 232|
| |Si (eff. 21.3 %)| |232 ・ 205 ・ 189| |213 ・ 188 ・ 174|
|[Venus](venus.md)|GaAs (eff. 28.5 %)| |610 ・ 540 ・497|… ・ … ・ …|
| |Si (eff. 21.3 %)| |456 ・ 403 ・ 372|… ・ … ・ …|



### Miura fold

The **Miura fold** (ミウラ折り, Miura‑ori) is a method of folding a flat surface such as a sheet of paper into a smaller area. The fold is named for its inventor, JP astrophysicist Kōryō Miura.

|**Crease pattern for a Miura fold. The parallelograms of this example have 84° & 96° angles.**|**Video displaying the folding & unfolding of a Miura‑creased material**|
|:--|:--|
|![](f/sps/miura_fold_pic01.webp)|![](f/sps/miura_fold_pic02.webp)|

The crease patterns of the Miura fold form a tessellation of the surface by parallelograms. In one direction, the creases lie along straight lines, with each parallelogram forming the mirror reflection of its neighbor across each crease. In the other direction, the creases zigzag, & each parallelogram is the translation of its neighbor across the crease. Each of the zigzag paths of creases consists solely of mountain folds or of valley folds, with mountains alternating with valleys from one zigzag path to the next. Each of the straight paths of creases alternates between mountain & valley folds. The Miura fold is related to the Kresling fold, the Yoshimura fold & the Hexagonal fold, & can be framed as a generalization of these folds.

The Miura fold is a form of rigid origami, meaning that the fold can be carried out by a continuous motion in which, at each step, each parallelogram is completely flat. This property allows it to be used to fold surfaces made of rigid materials, making it distinct from the Kresling fold & Yoshimura fold which cannot be rigidly folded & require panel deformations to compress to a compact state. For instance, large solar panel arrays for space satellites in the Japanese space program have been Miura folded before launch & then spread out in space. A folded Miura fold can be packed into a compact shape, its thickness reflecting only the thickness of the folded material. Folded material can be unpacked in one motion by pulling on its opposite ends, & likewise folded by pushing the two ends together. In the solar array application, this property reduces the number of motors required to unfold this shape, reducing weight & complexity.

Applications:

1. The 1996 Space Flyer Unit deployed the 2D Array from a Miura folded configuration.
1. The inflatable membrane structure of the SPROUT satellite is carried into space in the Miura‑folded state, & then deployed using inflatable tubes themselves carried into space in the Octagon‑folded state.
1. Other potential applications of this fold include surgical devices such as stents & flat‑foldable furniture.
1. Researchers at the University of Fribourg used the Miura fold to stack hydrogel films, generating electricity similarly to electric eels. The Miura fold is used to cause many parts of the stack to contact each other simultaneously.



### MJ, ZTJ
> <small>**Multi‑junction (MJ) solar cells** — EN term. **Многокаскадный солнечный элемент** — RU analogue.</small>  
> <small>**3rd Generation Triple-Junction (ZTJ)** — EN term. **3-е поколение трёхкаскадных солнечных элементов** — RU analogue.</small>

**Multi‑junction (MJ) solar cells** are solar cells with multiple p–n junctions made of different semiconductor materials. Each material's p‑n junction will produce electric current in response to different wavelengths of light. The use of multiple semiconducting materials allows the absorbance of a broader range of wavelengths, improving the cell's sunlight to electrical energy conversion efficiency.

Traditional single‑junction cells have a maximum theoretical efficiency of 33.16 %. Theoretically, an infinite number of junctions would have a limiting efficiency of 86.8 % under highly concentrated sunlight. Currently, the best lab examples of traditional crystalline silicon (c‑Si) solar cells have efficiencies of 20 ‑ 25 %, while lab examples of multi‑junction cells have demonstrated performance over 46 % under concentrated sunlight. Commercial examples of tandem cells are widely available at 30 % under one‑sun illumination, & improve to around 40 % under concentrated sunlight. However, this efficiency is gained at the cost of increased complexity & manufacturing price. To date, their higher price & higher price‑to‑performance ratio have limited their use to special roles, notably in aerospace where their high power‑to‑weight ratio is desirable. In terrestrial applications, these solar cells are emerging in concentrator photovoltaics (CPV), with a growing number of installations around the world.

Tandem fabrication techniques have been used to improve the performance of existing designs. In particular, the technique can be applied to lower cost thin‑film solar cells using amorphous silicon, as opposed to conventional crystalline silicon, to produce a cell with ~10 % efficiency that is lightweight & flexible. This approach has been used by several commercial vendors, but these products are currently limited to certain niche roles, like roofing materials.



## Docs/Links
|**Sections & pages**|
|:--|
|**【[Spacecraft power system (SPS)](sps.md)】**<br> [Charge eff.](charge_eff.md) ~~ [EAS](eas.md) ~~ [EB](eb.md) ~~ [EMI, RFI](emi.md) ~~ [NR](nr.md) ~~ [Rotor](iu.md) ~~ [RTG](rtg.md) ~~ [Solar cell](sp.md) ~~ [SP](sp.md) ~~ [SPB/USPB](suspb.md) ~~ [Voltage](sps.md) ~~ [WT](wt.md)<br>~ ~ ~ ~ ~<br> **RF/CIF:** [BAK‑01](eas_lst.md) ~~ [KAS‑LOA](eas_lst.md)|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Solar_panel>
1. <https://en.wikipedia.org/wiki/Gallium_arsenide>
1. <https://en.wikipedia.org/wiki/Solar_cell>
1. <https://en.wikipedia.org/wiki/Multi-junction_solar_cell>
1. <https://en.wikipedia.org/wiki/Miura_fold>


## The End

end of file
