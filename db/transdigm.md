# TransDigm
> 2019.08.14 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/t/transdigm_logo1t.webp)](f/c/t/transdigm_logo1.webp)|<mark>noemail</mark>, +1(216)706-29-60, Fax …;<br> *The Tower at Erieview, 1301 East 9th St., Suite 3000, Cleveland, OH 44114, USA*<br> <https://www.transdigm.com>|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**TransDigm Group Incorporated** develops, distributes & manufactures commercial & military aerospace components such as mechanical actuators & ignition systems.



## The End

end of file
