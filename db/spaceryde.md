# SpaceRyde
> 2021.07.12 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/s/spaceryde_logo1t.webp)](f/c/s/spaceryde_logo1.webp)|<info@spaceryde.com>, <mark>nophone</mark>, Fax …;<br> *400 Spinnaker Way, Unit 4, Concord, ON, L4K 5Y9 Canada*<br> <https://www.spaceryde.com>・[IG ⎆](https://www.instagram.com/spaceryde)・[LI ⎆](https://www.linkedin.com/company/spaceryde)・[X ⎆](https://twitter.com/Space_Ryde)|
|:--|:--|
|**Business**|Affordable, on‑schedule, dedicated [launch](lv.md) for CubeSats using balloons|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**SpaceRyde** uses balloons to launch small satellites to space. Founded in 2018.

This is the 1st affordable taxi to space for small satellites. Our disruptive balloon/rocket launch technology allows on-demand launch from any place around the world. We customize missions for each customer without the need to ride share with others, and we do that at an unbeatable price. The space economy is projected to be in the trillions in the near future, and SpaceRyde’s affordable taxi will enable this massive growth.

SpaceRyde’s rocket is lifted with a balloon to 30 ㎞. The proprietary software plans a launch path, & points the rocket in the right direction. The rocket  is then launched mid-air from the balloon to take small satellites to space. SpaceRyde’s 1st sub-scale prototype successfully flew from Canada in June 2019. This was the 1st permit Canada issued for a rocket launch in 21 years.

<p style="page-break-after:always"> </p>

## The End

end of file
