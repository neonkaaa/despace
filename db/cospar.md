# COSPAR
> 2022.01.01 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/c/cospar_logo1t.webp)](f/c/c/cospar_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *…*<br> <https://cosparhq.cnes.fr>・ <https://www.cospar-assembly.org> ~~ [FB ⎆](https://www.facebook.com/CommitteeOnSpaceResearch) ~~ [X ⎆](https://twitter.com/cosparhq?:lang=fr) ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/Committee_on_Space_Research)|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

The **Committee on Space Research (COSPAR)** was established by the International Council for Science (ICSU) in 1958. The Principles that COSPAR adheres to in pursuit of its Mission are:

- COSPAR promotes scientific research in space at an international level, with emphasis on the exchange of results, information, & opinions, & provides a forum, open to all scientists.
- COSPAR endeavors to ensure that a vibrant international space research effort can be conducted without impediment from geopolitical tensions or differences.
- COSPAR requires that presentations at its meetings & publications in its journals are the result of scientific research that was conducted with the highest ethical standards.
- COSPAR discloses any financial support that might be perceived as influencing its activities or positions it might advocate.
- COSPAR promotes diversity & gender equality in all of its activities, & will not tolerate any form of discrimination or harassment.
- COSPAR encourages meaningful roles in all activities for younger scientists, who are the future of international space research.

Among COSPAR’s objectives are the promotion of scientific research in space on an international level, with emphasis on the free exchange of results, information, & opinions, & providing a forum, open to all scientists, for the discussion of problems that may affect space research. These objectives are achieved through the organization of symposia, publication, & other means. COSPAR has created a number of research programmes on different topics, a few in cooperation with other scientific Unions. The long‑term project COSPAR international reference atmosphere started in 1960; since then it has produced several editions of the high‑atmosphere code CIRA. The code 「IRI」 of the URSI‑COSPAR working group on the International Reference Ionosphere was 1st edited in 1978 & is yearly updated.

Every second year, COSPAR calls for a **General Assembly** (also called Scientific Assembly). These are conferences currently gathering more than a thousand participating space researchers. The most recent assemblies are listed in the table below.

<small>

|**№**|**Year**|**Place**|
|:--|:--|:--|
|44|2022|Greece, Athens|
|43|2020|Australia, Sydney|
|42|2018|US, Pasadena|
|41|2016|Turkey, Istanbul *(cancelled)**|
|40|2014|Russia, Moscow|
|39|2012|India, Mysore|
|38|2010|Germany, Bremen|
|37|2008|Canada, Montreal|
|36|2006|China, Beijing|
|35|2004|France, Paris|
|34|2002|US, Houston|
|33|2000|Poland, Warsaw|
|32|1998|Japan, Nagoya|
|31|1996|Great Britain, Birmingham|
|30|1994|Germany, Hamburg|
|29|1992|UK, Washington, DC|
|28|1990|Netherlands, The Hague|
|27|1988|Finland, Espoo|
|26|1986|France, Toulouse|
|25|1984|Austria, Graz|
|24|1982|Canada, Ottawa|
|23|1980|Hungary, Budapest|
|22|1979|India, Bangalore|
|21|1978|Austria, Innsbruck|
|20|1977|Israel, Tel-Aviv|
|19|1976|US, Philadelphia, PA|
|18|1975|Bulgaria, Varna|
|17|1974|Brazil, Sao Paulo|
|16|1973|Germany, Konstanz|
|15|1972|Spain, Madrid|
|14|1971|US, Seattle, WA|
|13|1970|USSR, Leningrad|
|12|1969|Czechoslovakia, Prague|
|11|1968|Japan, Tokyo|
|10|1967|Great Britain, London|
|9|1966|Austria, Vienna|
|8|1965|Argentina, Mar del Plata|
|7|1964|Italy, Florence|
|6|1963|Poland, Warsaw|
|5|1962|US, Washington, DC|
|4|1961|Italy, Florence|
|3|1960|France, Nice|
|2|1959|Netherlands, The Hague|
|1|1958|Great Britain, London|

</small>

## The End

end of file
