# ElevationSpace
> 2021.12.02 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/e/elevationspace_logo2t.webp)](f/c/e/elevationspace_logo1.webp)|<info@elevation-space.com>, <mark>nophone</mark>, Fax …;<br> *1 Chome-4-9 enspace, Kokubuncho, Aoba Ward, Sendai, 〒980-0803 Miyagi, Japan*<br> <https://elevation-space.com> ~~ [FB ⎆](https://www.facebook.com/ElevationSpace) ~~ [LI ⎆](https://www.linkedin.com/company/elevationspace-inc) ~~ [X ⎆](https://twitter.com/ELS_inc_PR)|
|:--|:--|
|**Business**|Space constructions, bases, transportations, experiments|
|**Mission**|To create a world where everyone can live in space & enrich the future of people|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・Representative Director, CEO — Ryohei Kobayashi<br> ・Director, CTO — Toshinori Kuwahara|

**ElevationSpace Inc.** is a Japanese space startup from Tohoku University that aims to enrich the future of people. Founded 2021.

Utilizing the knowledge of many small artificial satellites that the Yoshida-Hibara laboratory has developed so far, we are developing the ELS-R, a small space utilization/recovery platform that enables experiments & manufacturing within artificial satellites.

At ElevationSpace, our mission is to create a world where everyone can live in space & enrich the future of people. The 1st step to achieve is the small space utilization & recovery platform ELS-R. Taking advantage of the micro‑gravity environment, which is the greatest feature of the universe, we will not only conduct scientific research but also manufacture high‑quality materials that cannot be made on the Earth to improve people’s lives.

Then, based on the technology cultivated in ELS-R, we will develop into the space construction business such as space hotels & lunar bases, & the space transportation business that carries people & goods, & people will be sustainable in orbit & on the Moon & Mars. Create a future where you can live in.

Through our business, ElevationSpace will enrich the future of humankind & give dreams & hopes to many people.

**Management:**

- **Ryohei Kobayashi** (Representative Director, CEO) — while attending Akita National College of Technology, he encountered space architecture & changed his life. After graduating, he transferred to Tohoku University & majored in architecture & space engineering. While attending university, he engaged in artificial satellite development projects & research on next‑generation space buildings, & won 1st place in Japan & second place in the world in space architecture. He also set up the Tohoku Space Community & is working to revitalize Tohoku in space. After working as an intern at multiple companies incl. a space venture, he started ElevationSpace Inc. He has won various program contests such as the Start Next Innovator sponsored by the Ministry of Economy, Trade & Industry / JETRO.
- **Toshinori Kuwahara** (Director, CTO) — after completing the master’s program at Kyushu University Graduate School, he obtained a doctorate (aerospace engineering) from the University of Stuttgart Graduate School in Germany. Since 2010, he has been appointed as a specially appointed assistant professor in the Department of Aerospace Engineering, Graduate School of Engineering, Tohoku University. Currently, as an associate professor, he researches & develops a wide range of technologies related to micro‑satellite, & has developed & operates more than 10 artificial satellites so far. He co‑founded ElevationSpace Inc. & is in charge of the development team as CTO. He is a technical advisor of Nakashimada Iron Works Co., Ltd. & ALE Co., Ltd., & also serves as the chairman of UNISEC (University Space Engineering Consortium).</small>

<p style="page-break-after:always"> </p>

## The End

end of file
