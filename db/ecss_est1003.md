# ecss_est1003

[TOC]

---


## Annex A (normative) Assembly, integration & test plan (AIT Plan) — DRD

**A.1 DRD identification.** *Requirement ID & source* — ECSS‑E‑ST‑10‑03, requirement 4.3.3.2a. *Purpose & objective* — the **assembly integration & test plan (AIT Plan)** is the master plan for the product AIT process. It describes the complete AIT process & demonstrates together with the verification plan how the requirements are verified by inspection & test. It contains the overall AIT activities & the related verification tools (GSE & facilities), the involved documentation, the AIT management & organization. It also contains the AIT schedule. It is one of the major inputs to the project schedule & is used to provide the customer a basis for review & evaluation of the effectiveness of the AIT programme & its proposed elements. An AIT Plan is prepared for the different verification levels covering in detail the AIT activities at that level & outlining the necessary lower level aspects. The AIT Plan is complementary to the verification plan. It takes into account the test standards defined in the Customer requirements. The availability of the verification plan is a prerequisite to the preparation of the AIT Plan.

**A.2 Expected response**

*Special remarks* — None.

*Scope & content:*

1. **Introduction.** The purpose, objective, content & the reason prompting its preparation. State & describe open issues, assumptions & constraints relevant to this document.
1. **Applicable & reference documents, Definitions & abbreviations.** The applicable & reference documents in support to the generation of the document. List the applicable dictionary or glossary & the meaning of specific terms or abbreviations utilized in the document.
1. **Product presentation.** Briefly describe the selected models & their built status with reference to the verification plan (see ECSS‑E‑ST‑10‑02).
1. **Assembly, integration & test programme**
   1. Document the AIT activities & associated planning.
   1. Include test matrix(ces) that link the various tests with the test specifications, test procedures, test blocks & hardware model.
   1. Detail AIT programmes (including inspections) through dedicated activity sheets.
   1. Activity sheets shall include descriptions of the activity including the tools & GSE to be used, the expected duration of the activity, & the relevant safety or operational constraints.
   1. The sequencing of activities should be presented as flow charts.
1. **GSE & AIT facilities.** List & describe the GSE, test software & AIT facilities to be used. Describe the logistics & list the major transportations.
1. **AIT documentation.** Describe the AIT documents to be produced & their content.
1. **Organization & management**
   1. Describe the responsibility & management tools applicable to the described AIT process with reference to ECSS‑E‑ST‑10‑02.
   1. Describe the responsibilities within the project team, the relation to product assurance, quality control & configuration control (tasks with respect to AIT) as well as the responsibility sharing with external partners. NOTE Tasks with respect to AIT include for example, anomaly handling, change control, safety, & cleanliness.
   1. State the planned reviews & the identified responsibilities.
1. **AIT schedule.** Provide the AIT schedule as reference.



## Annex B (normative) Test specification (TSPE) — DRD

**B.1 DRD identification.** *Requirement ID & source* — ECSS‑E‑ST‑10‑03, requirement 4.3.3.3. *Purpose & objective* — the **test specification (TSPE)** describes in detail the test requirements applicable to any major test activity. In particular, it defines the purpose of the test, the test approach, the item under test & the set‑up, the required GSE, test tools, test instrumentation & measurement uncertainties, test conditions with tolerances, test sequence, test facility, pass/fail criteria, required documentation, participants & test schedule. Since major test activities often cover multiple activity sheets, the structure of the TSPE is adapted accordingly. The TSPE is used as an input to the test procedures, as a requirements document for booking the environmental test facility & to provide evidence to the customer on certain details of the test activity in advance of the activity itself. The TSPE is used at each level of the space system decomposition (i.e. equipment, space segment element). The TSPE provides the requirements for the activities identified in the AIT Plan (as per ECSS‑E‑ST‑10‑03 Annex A). The TSPE is used as a basis for writing the relevant test procedures (as per ECSS‑E‑ST‑10‑03 Annex C) & test report (as per ECSS‑E‑ST‑10‑02 Annex C). In writing the test specification potential overlaps with the test procedure is minimized (i.e. the test specification gives emphasis on requirements, the test procedure on operative step by step instructions). For simple tests, merging TSPE & TPRO is acceptable.

**B.2 Expected response**

*Special remarks* — None.

*Scope & content:*

1. **Introduction.** The purpose, objective, content & the reason prompting its preparation. State & describe open issues, assumptions & constraints relevant to this document.
1. **Applicable & reference documents, Definitions & abbreviations.** The applicable & reference documents in support to the generation of the document. List the applicable dictionary or glossary & the meaning of specific terms or abbreviations utilized in the document.
1. **Requirements to be verified.** List the requirements to be verified (extracted from the VCD) in the specific test & provides traceability where in the test the requirement is covered.
1. **Test approach & test requirements.** Summarize the approach to the test activity & the associated requirements as well as the prerequisites to start the test.
1. **Test description.** Summarize the configuration of the item under test, the test set‑up, the necessary GSE, the test tools, the test conditions & the applicable constraints.
1. **Test facility.** Describe the applicable test facility requirements together with the instrumentation & measurement uncertainties, data acquisition & test space segment equipment to be used.
1. **Test sequence.** Describe the test activity flow & the associated requirements. When constraints are identified on activities sequence, specify them including necessary timely information between test steps.
1. **Pass/fail criteria.** List the test pass/fail criteria in relation to the inputs & output.
1. **Test documentation.** List the requirements for the involved documentation, including test procedure, test report & PA & QA records.
1. **Test organization.** Describe the overall test responsibilities, participants to be involved & the schedule outline (NOTE Participation list is often limited to organisation & not individual name).



## Annex C (normative) Test procedure (TPRO) — DRD

**C.1 DRD identification.** *Requirement ID & source* — ECSS‑E‑ST‑10‑03, requirement 4.3.3.4. *Purpose & objective* — the **Test Procedure (TPRO)** gives directions for conducting a test activity in terms of description, resources, constraints & step‑by‑step procedure, & provides detailed step‑by‑step instructions for conducting test activities with the selected test facility & set‑up in agreement with the relevant AIT Plan & the test requirements. It contains the activity objective, the applicable documents, the references to the relevant test specification & the test facility configuration, the participants required, the list of configured items under test & tools & the step‑by‑step activities. The TPRO is used & filled‑in as appropriate during the execution & becomes the “as‑run” procedure. The TPRO is prepared for each test to be conducted at each verification level. The same procedure can be used in case of recurring tests. It incorporates the requirements of the test specification (Annex B) & uses detailed information contained in other project documentation (e.g. drawings, ICDs). Several procedures often originate from a single test specification. In certain circumstances involving a test facility (for example during environmental tests) several test procedures can be combined in an overall integrated test procedure. The “as‑run” procedure becomes part of the relevant test report (see ECSS‑E‑ST‑10‑02). Overlaps with the test specification are minimized (see Annex B).

**C.2 Expected response**

*Special remarks* — None.

*Scope & content:*

1. **Introduction.** The purpose, objective, content & the reason prompting its preparation. State & describe open issues, assumptions & constraints relevant to this document.
1. **Applicable & reference documents, Definitions & abbreviations.** The applicable & reference documents in support to the generation of the document. List the applicable dictionary or glossary & the meaning of specific terms or abbreviations utilized in the document.
1. **Requirements mapping w.r.t. the TSPE.** Provide a mapping matrix to the TSPE giving traceability towards the test requirement.
1. **Item under test.** Describe the item under test configuration, including any reference to the relevant test configuration list, & any deviation from the specified standard. Identify the software version of the item under test.
1. **Test set‑up.** Describe the test set‑up to be used.
1. **GSE & test tools required.** Identify the GSE & test tools to be used in the test activity including test script(s), test software & database(s) versioning number.
1. **Test instrumentation.** Identify the test instrumentation, with measurement uncertainties, to be used, including fixtures.
1. **Test facility.** Identify the applicable test facility & any data handling system.
1. **Test conditions.** List the applicable standards, the applicable test conditions, in terms of levels, duration & tolerances, & the test data acquisition & reduction.
1. **Documentation.** Describe how the applicable documentation is used to support the test activity.
1. **Participants.** List the allocation of responsibilities & resources.
1. **Test constraints & operations.** Identify special, safety & hazard conditions, operational constraints, rules for test management relating to changes in procedure, failures, reporting & signing off procedure. Describe QA & PA aspects applicable to the test. Contain a placeholder for identifying: procedure variations, together with justification, & anomalies.
1. **Step‑by‑step procedure**
   1. Provide detailed instructions, including expected results, with tolerances, pass/fail criteria, & identification of specific steps to be witnessed by QA personnel.
   1. The step‑by‑step instructions may be organized in specific tables.
   1. When the procedure is automated, documents the listing of the automated procedure to a level allowing consistency check with the TPRO & the TPSE.
