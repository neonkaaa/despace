# MSSS, 11th
> 2020.07.03 [🚀](../../index/index.md) [despace](index.md) → [MSSS](msss.md), **[Events](event.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Одинадцатый московский симпозиум по солнечной системе** — русскоязычный термин, не имеющий аналога в английском языке. **The 11th Moscow Solar System Symposium** — дословный перевод с русского на английский.</small>

The **Tenth Moscow international Solar System Symposium (10M‑S³)** will be held in [Space Research Institute](iki_ras.md), Moscow, Russia, 2020.10.05‑09.

Subject matter of this symposium will cover many problems of the Solar system science with the central topic 「Moon, Mars and Venus research」. This topic relates to scientific problems of several missions: 「Mars Express」, 「Venus Express」, the missions under development in Russia: 「Luna-Glob」, 「Luna‑resource」, 「ExoMars 2016」 and 「ExoMars 2020」 (Roscosmos-ESA).

|**MSSS 11**|<https://ms2020.cosmos.ru>|
|:--|:--|
|Date|October 5 till 9, 2020|
|Venue|[Space Research Institute](iki_ras.md), Moscow, Russia|
|Host Org.|[Space Research Institute](iki_ras.md)|
|Co‑host Org.| |
|Supported by|Russian Foundation for Basic Research;<br> Vernadsky Institute of Geochemistry & Analytical Chemistry RAS, Russia;<br> Brown University, USA;<br> Schmidt Institute of Physics of the Earth RAS, Russia;<br> [Keldysh Institute of Applied Mathematics RAS](keldysh_ipm.md), Russia;<br> Kotelnikov Institute of Radio-engineering & Electronics RAS, Russia;<br> Sternberg Astronomical institute, Moscow State University, Russia|
|Accepted by| |
|Content|Face-to-face and virtual, Oral & Poster presentations|
|Focus| |
|Reg. Fee| |
|Attendees| |
|Related Mtg.| |
|Contact|<ms2020@cosmos.ru>|

**Local Organizing Committee (LOC).**  
Chair: [Zakharov, A.V.](person.md) (Chair)  
Secretary: Antonenko, Elena, <antonenko@iki.rssi.ru>  
Members: Antonenko, E.A., Davydov, V.M., [Korablev, O.I.](person.md), Korableva, E.O., Roste, O.Z., Smirnova, A.V., Ustinov, A.N., Zharkova, T.D.

**Program Committee.**  
Chair: [Zelenyi, L.M.](person.md) (IKI RAS, RU)  
Secretary: Roste, O.Z. (IKI RAS, RU), <ms3@iki.rssi.ru>  
Members:

1. Bazilevskiy, A.T. (GEOHI RAS, RU)
1. Bibring, J.-P. (IAS, CNRS, France, EU)
1. Bolton, S.J. (SWRI, US)
1. Borovin, G.K. (Keldysh AMI, RU)
1. Chicarro, A. (ESTEC, EU)
1. Duxbury, T. (George Mason University, US)
1. [Head, J.](person.md)  (Brown University, US)
1. [Korablev, O.I.](person.md) (IKI RAS, RU)
1. [Kostitsyn, Y.A.](person.md) (GEOHI RAS, RU)
1. [Marov, M.Ya.](person.md) (GEOHI RAS, RU)
1. [Mitrofanov, I.G.](person.md) (IKI RAS, RU)
1. Rodin, A.V. (IKI RAS, RU)
1. Shevchenko, V.V.  (GAISH MSU, RU)
1. Smirnov, V.M. (IRE RAS, RU)
1. [Svedhem, H.](person.md) (ESTEC, EU)
1. [Vaisberg, O.L.](person.md) (IKI RAS, RU)
1. Vorobyova, E.A. (MSU, RU)
1. Witasse, O. (ESTEC, EU)
1. Wu, Ji (China)
1. Zharkov, V.N. (IFZ RAS, RU)
1. [Zakharov, A.V.](person.md) (IKI RAS, RU)
1. [Zasova, L.V.](person.md) (IKI RAS, RU) — Chair of the Venus session



## Описание

The following sessions will be held during the symposium:

1. OPENING SESSION
1. Session. Mars
1. Session. Venus
1. Session. Moon and Mercury
1. Session. Small Bodies (including cosmic dust)
1. Session. Giant planets
1. Session. Extrasolar planets
1. Session. Solar wind interactions with planets and small bodies
1. Session. Astrobiology

**План:**

1. March 1, 2020: Registration and abstract submission start
1. JULY 1, 2020: Registration deadline for people with a visa application
1. Please send your visa request by E-mail to ms2020@cosmos.ru (indicate VISA in the Subject of email)
1. JULY 10, 2020: Deadline for abstract submission
1. September 7, 2020: Program released
1. September 18, 2020: Final registration deadline
1. October 3-4, 2020: Arrival to Moscow
1. October 5-9, 2020: Symposium in Moscow
1. October 10-11, 2020: Departure from Moscow



<p style="page-break-after: always"> </p>

## Итоги

**Таблица.** Количество участников, докладов и постеров. (упорядочено по убыванию)

|**Страна**|**Участники**|**Презентации**|**Постеры**|
|:--|:--|:--|:--|
|США|… <small>(… [NASA](nasa.md)/JPL, … университеты)</small>|…|…|
|Япония|… <small>(… JAXA, … университеты)</small>|…|…|
|Европа|…|…|…|
|Россия|… <small>(… НПОЛ, … университеты)</small>|…|…|
|**Итого**|**…**|**…**|**…**|

<mark>краткий отчёт</mark>

<p style="page-break-after: always"> </p>

**Таблица.** Общие участники с ближайшими конференциями.

|**Конференция**|**Участники**|
|:--|:--|
|**[IVC 2019](ivc_2019.md)**|Anastasia Kosenkova, Alexey Martynov|
|**[2019 VD Workshop](vdws2019.md)**|[Andrey Belov](belov2.md), [Richard Ernst](person.md), [Mikhail Ivanov](person.md), Anastasia Kosenkova, [Oleg Kotsyurbenko](person.md), [Margarita Kruchkova](kruchkova1.md), Alexey Martynov, Pavel Pisarenko, [Ludmila Zasova](person.md)|
|**[VEXAG, 17th](vexag_2019.md)**| |

**Таблица.** Презентации, постеры и прочие участники в порядке выступления.  
<small>(Толстым выделены новые интересные личности)</small>

<small>

|°|**Имя**|**Орг.**|**Название**|
|:--|:--|:--|:--|
|—|—|—|**`Session. Mars`**|
| |[James Head](person.md)| |Toward an Understanding of Early Mars Climate History: New Themes, Directions & Tests|
| |Ashley Palumbo| |Volcanism on Early Mars: Exploring the Influence of the SO2 Plume on Localized & Short-Term Climate Change|
| |Ashley Palumbo & [James Head](person.md)| |Rainfall as an erosive mechanism on Noachian Mars: Rain splash, infiltration, & surface runoff|
| |Ben Boatwright & [James Head](person.md)| |Fluvial Geology of the Northwest Hellas Region, Mars|
| |[Hakan Svedhem](person.md)| |ExoMars TGO status & future activities|
| |[Oleg Korablev](person.md)| |The Atmospheric Chemistry Suite (ACS) Experiment on Board the ExoMars Trace Gas Orbiter: the 1st Results|
| |Alexey Malakhov| |Martian subsurface hydrogen measured with high resolution by FREND onboard TGO. Results half way through the schience phase|
| |Jordanka Semkova| |Radiation environment in the interplanetary space & Mars’ orbit according FREND’s Liulin-MO dosimeter aboard ExoMars TGO data|
| |Imant Vinogradov & the M-DLS team| |M-DLS experiment for the ExoMars-2020 mission Stationery Landing Platform: instrument calibration results|
| |[Alexander Kosov](person.md), Veronique Dehant| |LaRa (Lander Radioscience) on the ExoMars-2020 Surface Platform – VLBI & Doppler Positioning measurement|
| |Anatoly Manukin| |Measurement of the seismic activity of Mars using the device SEM in the framework of the program "ExoMars"|
| |Diego Rodríguez Díaz| |AMR instrument for ExoMars 2020 mission: Scientific goals concerning the martian magnetic field|
| |José Luis Mesa Uña| |Development of magnetic instrumentation for planetary applications: Evolution of the NEWTON magnetic susceptometer|
| |Mikhail Verigin & Galina Kotova| |Measurements of the Martian crust magnetization 25 years before its discovery|
| |Tamara Gudkova| |On the zones of potencial seismicity on Mars|
| |Salvador Jimenez| |Crustal & non crustal magnetic fields in Mars ionosphere from MARSIS data|
| |Marina Díaz Michelena| |• Magnetic investigations of terrestrial analogues of Mars.<br> ・Magnetic measurements to investigate the origin of the Phobos & Deimos.|
| |Jessica Flahaut & C. Brustel| |News views of Mars crust|
| |Boris Ivanov| |Traces of atmospheric shock waves near new Martian craters|
| |[Mikhail Ivanov](person.md)| |Acidalia Mensa on Mars: A test of the mars ocean hypothesis|
| |Ekaterina Grishakina| |Western Utopia plain: geological characterization (clarification) & cryological processes|
| |Anatoliy Pavlov| |A novel mechanism for rapid methane destruction by cosmic rays on Mars|
| |Vladimir Ogibalov & G.M. Shved| |Effect of aerosol scattering on radiative transfer in the CO2 & CO infrared bands in the daytime Martian atmosphere under breakdown of vibrational LTE|
| |Maria Pilar Velasco| |The Martian atmospheric dust dynamic through fractional differential models & simulations|
| |Luis Vázquez| |Solar Radiation & Dust in the Martian Atmosphere|
| |Yulia Izvekova| |Convective dust vortices near the surface of the Earth & Mars|
| |Jose Luis Vazquez-Poletti| |Serverless Computing for Mars Exploration Applications|
| |Jinsong Ping| |Open loop doppler between Earth-Mars for Martian rotation research|
| |Alexandra Bermejo| |Metamaterials technology for space applications|
| |Ekaterina Grishakina| |Martian soil simulant for mission ExoMars|
| |Manuel Dominguez| |Control of sensors for optimal performance: application to planetary missions|
|•|・  •   •   •   •   •   •   •|・ •  •  •  •  •  •|・ •  •|
|—|—|—|**`Session. Venus`**|
| |[Ludmila Zasova](person.md)| |Venera-D: a perspective planetary mission|
| |[Richard Ernst](person.md)| |Geological tests of global warming models on Venus|
| |Evgeniya Guseva| |Morphological analysis of the coronae of Venus|
| |Vladimir Zharkov & Tamara Gudkova| |On the structure of the gravitational field for the earth‑like interior sructure of Venus|
| |Vladimir N. Gubenko & Ivan A. Kirillovich| |Internal waves characteristics in the Venus’s atmosphere revealed from the Magellan & Venus Express radio occultation data by two independent methods|
| |Vladimir Gromov & [Alexander Kosov](person.md)| |A model of millimeter wave atmospheric absorption of the sulfur dioxide & the sulphuric acid vapour for the radiometric experiment in the Venera-D mission|
| |[Leonid Ksanfomality](person.md)| |Hypothetical living forms on planet Venus & their possible nature|
|•|・  •   •   •   •   •   •   •|・ •  •  •  •  •  •|・ •  •|
|—|—|—|**`Session. Extrasolar planets`**|
| |Shingo Kameda| |Exoplanets Exosphere Detecting Ultraviolet Spectrograph (UVSPEX) onboard World Space Observatory Ultraviolet (WSO-UV)|
| |Valery Shematovich| |Statistical characteristics of exoplanets|
| |Vladislava Ananyeva| |Power Laws for Exoplanet Mass Distribution by Different Spectral Classes Parent Stars|
| |Alexander Tavrov| |Stellar Coronagraph for Exoplanets Direct Imaging onboard World Space Observatory Ultraviolet (WSO-UV)|
| |Andrey Yudaev| |Nulling Rotational-Shear Interferometer for Ground-based Telescopes Aiming High Contrast Imaging Towards Exoplanets|
| |Ildar Shaikhislamov| |3d gasdynamic modeling of transiting hot exoplanets|
| |[Leonid Ksanfomality](person.md)| |Exo-rings of 1708.04600 type as satellites of the KIC 8462852 Kepler object|
| |Valery I. Shematovich| |Atmospheric mass loss of close-in neptunes & super-earths|
| |Marina Rumenskikh| |Numerical simulation of processes occuring in the space environment of Gliese-436b Session. Solar wind interactions with planets & small bodies|
| |Vladimir V. Busarev| |Solar wind interaction with the surface of primitive asteroids & their sublimation activity|
| |Stas Barabash| |ENA imaging on an interstellar prob|
| |Valery I. Shematovich| |Kinetic models of electron & proton aurorae at Mars|
| |Natalia Bulatova| |Features of the Sun’s influence on the Earth lithosphere in periods of minimum activity|
|•|・  •   •   •   •   •   •   •|・ •  •  •  •  •  •|・ •  •|
|—|—|—|**`Session. Moon & Mercury`**|
| |Johannes Benkhoff|ESA|The status of the BepiColombo mission|
| |Alexander Bazilevskiy| |Potential lunar base on Mons Malapert on Mons Malapert: topographic, geologic & trafficability consideration|
| |[Evgenyi Slyuta](sluta1.md)| |Formation of the scientific program of research of the Moon: from priority scientific tasks to scientific equipment|
| |Vladislav Shevchenko| |The far side of the Moon - 60 years of history (1959 – 2019)|
| |[James Head](person.md) & L. Wilson| |Rethinking Lunar Mare Basalt Regolith Formation: New Concepts of Lava Flow Protolith & Evolution of Regolith Thickness & Internal Structure|
| |Jessica Flahaut, J. N. Schnuriger| |Long-term, complex volcanic history of the Arago region of the Moon|
| |Ariel Deutsch| |Distribution of surface water ice on the Moon: An analysis of host crater ages provides insights into the ages & sources of ice at the lunar south pole|
| |[James Head](person.md)| |Volcanically-Induced Transient Atmospheres on the Moon: Assessment of Duration & Significance|
| |Olga Nosova| |Problems of choice of the Lunokhod route for research & exploration of the volatile components in the south polar region of Moon|
| |Sergey Voropaev & A.Yu.Dnestrovskii| |Features of the Fossil Tidal Bulge Formation for the Early Moon|
| |Sergey Voropaev| |Experimental Study of degassing of the early Earth & Moon during accretion|
| |A.G. Fatyanov| |The focusing effect of p-wave in the Moon’s & Earth’s low‑velocity core. Analytical Solution|
| |V. Yu. Burmin| |On the nature of the seismic ringing of the Moon. Analytical modeling|
| |Arthur Zagidullin| |Рhysical libration of the moon|
| |Susanne Schröder| |LIBS for in-situ geochemical investigation of extraterrestrial surfaces of atmosphereless bodies|
| |Ariel Deutsch| |Investigating diurnal changes in the normal albedo of the lunar surface at 1064 nm: A new analysis with the Lunar Orbiter Laser Altimeter|
| |Jinsong Ping| |Low frequency radio astronomy experiment on the Moon|
| |Mingyuan Wang| |Radio frequency interference of earth based on low‑frequency detection of Chang'E 4|
| |Imant Vinogradov & the DLS-L team| |DLS-L optical sensor for investigation of chemical content & isotope ratio of Moon soil volatiles for the gas chromatography analytical suite of the Luna-27 mission|
| |Ute Böttger| |Ramanspectrometer for in-situ geochemical investigation of extraterrestrial surfaces of atmosphereless bodies|
| |Vladislav Makovchuk| |Functional & statistical testing of Thermo-LR surface thermal sensors|
| |Alexander Stark| |Lunar Rotation measurement with Laser Altimeter|
| |Olga Turchinskaya| |Selection & mapping of various concentration of titanium on the Moon aсcording to spacecraft data|
| |Ivan Agapkin| |Problems of studying the physico-mechanical properties of lunar soil in the TERMO-LR experiment for the Luna‑resource-1 project|
| |Svetlana Pugacheva| |Studies of the composition & structure of the soil of the South Pole of the Moon by spacecraft|
| |Egor Sorokin| |Experimental simulating of a micrometeorite impact on the Moon|
| |Olga Yushkova| |Geometric problems in simulation of the Moon bistatic sounding|
| |Andrey Kharitonov| |Magnetic & Gravity fields of the Moon from spacecraft Apollo data|
| |Gennady Kochemasov| |A new lunar phenomenon - widespread fine cm‑sized rippling of its surface discovered by the Chang’s 3 & 4|
| |Alexander Gusev| |Geological exploration of the Moon|
| |Natalia Kozlova| |Digital Moon: development of scientific basis, methods & tools for planetary data processing & analysis|
| |Boris Ivanov| |Small lunar crater formation & evolution|
| |Mikhail Shpekin| |The state of matter & the shock-wave processes in lunar craters|
| |Igor Zavyalov| |Implementation of Lunar crater catalogue for morphometric studies of the craters (diameter 1-10 km)|
| |Mariya Kolenkina| |Using relief approximation methods to study the surface of the Moon|
| |Natalia Kozlova| |Methods & technologies of geodetic & cartographic support of Global Lunar Navigation System|
| |Mariya Kolenkina| |Application of the geoportal for preservation & popularization of results of the Russian space missions|
| |Zanna Rodionova| |Topographic Features of the lunar maria & bazins|
| |Azariy Barenbaum & Mikhail Shpekin| |Problems of interpretation of crater data in the Solar System|
| |Svetlana Pugacheva| |The migration of volatiles in polar region of Mercury|
| |Anastasia Zharkova| |Thermal stability of volatiles in low‑latitude traps on Mercury|
| |Anastasia Zharkova & Mariya Kolenkina| |Patterns in distribution of morphometric parameters of the Moon & Mercury surfaces: mapping at the global level|
| |Anastasia Zharkova & Mikhail Kreslavsky| |Boulders on the Moon & Mercury: comparative morphological analysis at detail level|
|•|・  •   •   •   •   •   •   •|・ •  •  •  •  •  •|・ •  •|
|—|—|—|**`Session. Small Bodies (including cosmic dust)`**|
| |Jing Sun| |The research on radar astronomical observations to the asteroids|
| |Daniel Hestroffer| |BIRDY deep-space CubeSat to probe the internal structure of small bodies|
| |Vladimir V. Busarev| |Modeling of small bodies' migration from the formation zone of Jupiter to the main asteroid belt|
| |Sergey Voropaev & Y. Jianguo| |Small Bodies’ Strength: Failure Model|
| |Sergey Ipatov & Mikhail [Marov](person.md)| |Migration of planetesimals from different distances outside Mars’ orbit to the terrestrial planets & the Moon|
| |Sergey Ipatov| |Probabilities of collisions of bodies from different zones of the feeding zone of the terrestrial planets with the forming planets, the Moon, & their embryos|
| |Daniel Hestroffer| |Gaia & dynamics of Solar System Objects|
| |[Evgenyi Slyuta](sluta1.md)| |Gravitational deformation of small rocky bodies|
| |Anna Kartashova| |Analysis of meteor characteristics by multi-technique observations|
| |Ilan Roth| |The puzzle of meteorite abundance: anomalous enhancement of Mg-26 in Ca-Al inclusions|
| | | |Silicate & iron spherules in regolite of the Moon: origin & characteristic features|
| |A. I. Bakhtin| | |
| |[Leonid Ksanfomality](person.md)| |Physics of comets Hale-Bopp C/1995 O1 & 1P/Halley as different stages of the evolution of giant comet nuclei|
| |Dmitry Shestopalov| |Polarimetric properties of asteroid (3200) Phaethon|
| |Gennady Kochemasov| |• Octahedron tectonism of cosmic bodies including Earth.<br> ・Small bodies - Broken asteroid Ultima Thule.|
| |Sergey I. Popel| |Dusty plasmas at Phobos & Deimos: Effects of meteroids|
| |Sergey Ipatov| |Migration of interplanetary dust particles from different distances from the Sun to the terrestrial planets & the Moon|
| |Tatiana Salnikova| |Existence of elusive Kordylewski cosmic dust clouds|
| |Alexey Demyanov & V.V. Vysochkin| |Analyzer of Space Dust|
|•|・  •   •   •   •   •   •   •|・ •  •  •  •  •  •|・ •  •|
|—|—|—|**`Session. Giant planets`**|
| |Scott Bolton, Jack Connerney| |Juno’s Surprising Results at Jupiter|
| |Elena Belenkaya| |Jupiter’s auroras associated with Galilean moons & the main ovals|
| |Victor Tejfel| |Latitudinal & zonal variations of the ammonia absorption bands on Jupiter|
| |Ivan Pensionerov| |Model of Jupiter’s current sheet with a piecewise current density|
| |TBD| |(3-4 presentations from US) Jupiter & Saturn: the results of Cassini & Juno|
| |Valery Kotov| |Rotation of giant planets|
| |Erica Nathan| |Icy Moon Evolution: Experiments with Freezing Water Spheres|
|•|・  •   •   •   •   •   •   •|・ •  •  •  •  •  •|・ •  •|
|—|—|—|**`Session. Astrobiology`**|
| |Richard Hoover| |Advances in Astrobiology|
| |Sohan Jheeta| |Synthesis of the basic ‘building blocks’ of life|
| |Sergey Bulat| |Microbial life under thick glacier sheets: Lessons from the subglacial Antarctic Lake Vostok exploration|
| |Richard Hoover & Alexei Rozanov| |New Evidence for Indigenous Microfossils in Carbonaceous Chondrites|
| |Mikhail Kapralov| |Astrobiological research in Dubna|
| |Vladimir Cheptcov| |Survival of Radioresistant Bacteria on Europa’s Surface after Pulse Ejection of Subsurface Ocean Water|
| |[Andrey Belov](belov2.md)| |Edaphic bacterial communities of the extreme-arid Mojave desert: astrobiological implication|
| |E.A. Deshevaya| |Space Experiment "Test". ISS as an Outpost of the Knowledge of the Universe|
| |Andrey Kharitonov| |The Galactic cycles & Biological changes|
| |[Oleg Kotsyurbenko](person.md)| |Methanogens as the model microbial group for astrobiology|
| |[Margarita Kruchkova](kruchkova1.md)| |How do the fungal communities from desert soils react to the impact of proton irradiation?: Astrobiological model experiment|

</small>



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Events](event.md)】**<br> **Meetings:** [AGU](agu.md) ~~ [CGMS](cgms.md) ~~ [COSPAR](cospar.md) ~~ [DPS](dps.md) ~~ [EGU](egu.md) ~~ [EPSC](epsc.md) ~~ [FHS](fhs.md) ~~ [IPDW](ipdw.md) ~~ [IVC](ivc.md) ~~ [JpGU](jpgu.md) ~~ [LPSC](lpsc.md) ~~ [MAKS](maks.md) ~~ [MSSS](msss.md) ~~ [NIAC](niac_program.md) ~~ [VEXAG](vexag.md) ~~ [WSI](wsi.md) ┊ ··•·· **Contests:** [Google Lunar X Prize](google_lunar_x_prize.md)|

1. Docs: …
1. <https://ms2020.cosmos.ru>


## The End

end of file
