# NASA Parts Selection List
> 2019.07.31 [🚀](../../index/index.md) [despace](index.md) → **[](.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**NASA Parts Selection List** — англоязычный термин, не имеющий аналога в русском языке. **Список NASA по выбору составных частей** — дословный перевод с английского на русский.</small>

Selection of parts for inclusion in the device listings in this document is based on a review of technical data by the NASA EEE Parts Assurance Group (NEPAG) for quality and reliability trends. The major criteria used to evaluate candidate parts are:

1. Quality system and assurance level the products are produced under
1. Product performance
1. Product workmanship assessments
   1. Destructive Physical Analysis results
   1. Failure histories
   1. Reliability trends
   1. GIDEP alert histories of the product and manufacturer
   1. Qualification and screening test results
1. Product availability
1. Manufacturer audit and survey results
1. Manufacturer responsiveness to corrective actions
1. Manufacturer delivery histories

Participation by the manufacturers in a quality program such as QPL, QML, and ISO 9000. will not automatically qualify their products for listing in the NPSL. Listings will be based on results from assessments of all the major criteria listed above. EEE parts-related activities throughout NASA provide recommendations for listing parts in this document.

NOTE: Unless specifically stated within the parts selection tables of the NPSL, listing of a device technology herein does NOT imply/guarantee Radiation Hardness Assurance (RHA). Applications concerned with a device’s ability to tolerate exposure to various forms of space radiation (e.g., total ionizing dose, single event effects, etc.) should be reviewed and have the device assessed by the Program’s radiation assurance experts. The following resources may also be consulted for initial guidance:



## NASA Parts Levels

The NPSL lists products based on three quality levels defined by NEPAG: Level 1, Level 2, and Level 3. The definitions for each Level and the criteria used to list a part in a particular Level are not part approvals nor is this document a project approved parts list. The NPSL will not provide information on whether or not a part meets individual project flight requirements. Instead, it provides a list of products and associated manufacturers that meet recognized quality assurance baselines, qualification test regimens, and screening requirements necessary for space flight acceptance based on levels of risk. The part selected must be assessed independently by the project or the NASA center or OEM’s parts organization to determine if it meets the requirements for the project. The parts engineering organizations at the NASA centers will assist users in making this determination. The Levels herein are not directly related to mission classification, cost, or schedule and users should make the appropriate Level tradeoffs when considering which parts to choose from the list.


### Level 1

Level 1 is the highest product assurance class assigned to parts listed in this document. Level 1 parts are those produced under assurance classes recognized by NASA as providing the highest possible level of quality and reliability (e.g. QML Class V & K, JANS for discrete semiconductors, QPL Class S, Failure Rate Level (FRL) S), from NASA approved manufacturing sources, and meeting NASA space level parts and packaging program assessment criteria. The technical assessment results for Level 1 products will show that no known trends exist which have a negative impact on the quality, reliability, or performance for space flight applications. The Level 1 criteria is summarized as follows:

1. The supplier’s facility(s) must be certified under a recognized quality assurance system (e.g. QML, QPL, ISO 9000) and produce products to the space industry recognized highest assurance classes (e.g. QML V, JANS for discrete semiconductors, QPL Class S, FRL S, GSFC S311 specification) or equivalent.  There are exceptions to these levels where this preferred part reliability level is unavailable; these exceptions are shown in the individual part listings.
1. A Defense Logistics Agency (DLA) Land and Maritime audit or a NASA program manufacturer survey to the highest assurance classes must have been successfully completed within the past 2 years.
1. A part procurement specification, containing the highest assurance class requirements, must exist. Parts must have been procured previously by a NASA project using this specification.
1. Historical DPA and other parts analysis data on the manufacturer’s products must be available and not reveal poor workmanship trends or rejection trends.
1. Failure analyses history for the manufacturers products should not reveal problem trends attributed to part quality and reliability.
1. No recent unresolved GIDEP Alerts (past 3 years) exist that have a major impact on the Level 1 products quality or reliability. No GIDEP Alert or NASA Parts Advisory trends exist on the manufacturer or product.
1. Available data on manufacturer performance must show no trend for late delivery of products to NASA projects.
1. Qualification to the requirements of the procurement specification must have been successfully completed. No qualification issues exist and no problem trends from previous qualifications exist.


### Level 2

Level 2 is the second highest product assurance class assigned to parts listed in this document. Level 2 parts are those produced under assurance classes recognized by NASA to have a high level of quality and reliability (e.g. QML Q & H, QPL Class B, JANTXV for discrete semiconductors, FRL  R or P), from NASA approved manufacturing sources, and meeting NASA space level parts and packaging program assessment criteria. The Level 2 criteria is summarized as follows:

1. The supplier’s facility(s) must be certified under a recognized quality assurance system (e.g. QML, QPL, ISO 9000) and produce products to space industry recognized high assurance classes (e.g. QML Q, QPL B, JANTXV for discrete semiconductors, FRL R or P, GSFC S311 specification) or equivalent.   Any exceptions to these levels where the preferred part reliability level is unavailable, are shown in the individual part listings.
1. A Defense Logistics Agency (DLA) Land and Maritime audit or a NASA program manufacturer survey must have been successfully completed within the past 2 years.
1. A part procurement specification, containing the high assurance class requirements, must exist. Parts must have been procured previously by a NASA project using this specification.
1. DPA and other parts analysis data on the manufacturer’s products must be available and must not reveal any significant problems due to poor workmanship and must show minimal reject rates.
1. Failure analyses history for the manufacturer’s products should not reveal problem trends attributed to part quality and reliability.
1. No unresolved GIDEP Alert trends exist that have a major impact on the Level 2 products quality or reliability. No GIDEP Alert or NASA Parts Advisory trends exist on the manufacturer or product.
1. Available data on manufacturer performance must show consistent on-time delivery of products to NASA projects.
1. Qualification to the requirements of the procurement specification must have been successfully completed. Qualification issues and problems from previous qualifications must have been resolved (not by waiver).


### Level 3

Level 3 is the minimum product assurance class assigned to parts listed in this document. Level 3 contains many advanced electronic functions (from a space flight applications standpoint) and has been created to provide a technology insertion path into NASA flight projects. Parts listed are those produced by reputable manufacturers under a recognized quality assurance system (QML, QPL, ISO 9000) or their equivalent. Typically, only a limited amount of information is available to NEPAG for these parts and NASA has minimal visibility into the manufacturing and testing of Level 3 product. The parts are usually available commercially and have the capability to be used in space applications. The intent of Level 3 listings is to provide products that are newer, have greater functionality and enhanced performance characteristics, and provide higher levels of integration. Because the product has little or no heritage in space flight application and data is unavailable or scarce, these parts are considered higher risk than the Level 1 and Level 2 parts. While the price of these parts may be less than the traditional Levels, more engineering evaluation may be needed to qualify the part for the project’s application. The overall reliability and cost of ownership should be considered when selecting these parts. The Level 3 criteria is summarized as follows:

1. The manufacturer has supplied and qualified parts for several NASA space projects within the past 2 years.
1. A NASA, DoD, or other space agency procurement specification (e.g. ESA SCC or JAXA QPL/QML) exists.
1. Available data on the manufacturer shows no significant problem trends such as GIDEP Alerts or NASA Parts Advisories, a low DPA rejection rate for the manufacturer’s products in general, and no significant failures attributable to product quality and/or reliability.

NEPAG recommends selecting a Level 3 product when a higher Level part does not exist and/or enhanced functionality is required to meet system design requirements. Parts in this Level are not recommended for use in mission critical applications. Selecting these parts may require further engineering evaluation and approval by the project, but some heritage exists. Additionally, having more projects use these parts helps NEPAG acquire the technical data necessary for moving the parts into the higher Levels.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[](.md)】**<br> <mark>NOCAT</mark>|

1. Docs: …
1. <https://nepp.nasa.gov/npsl/npsl_UsePolicy.htm>


## The End

end of file
