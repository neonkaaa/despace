# Euglena Co., Ltd.
> 2021.12.08 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/e/euglena_logo1t.webp)](f/c/e/euglena_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *Minato-ku, Tokyo*<br> <https://www.euglena.jp> ~~ [FB ⎆](https://www.facebook.com/euglena.co.jp) ~~ [X ⎆](https://twitter.com/euglena_jp)|
|:--|:--|
|**Business**|Fuel & food production based on euglena|
|**Mission**|…|
|**Vision**|Make people & the Earth healthy|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO — Mitsuru Izumo|

**Euglena Co., Ltd.** is a Japanese company that succeeded in outdoor mass cultivation of microalgae 「Euglena」 in 2005. We currently operate a health care business for marketing 「Euglena」 functional foods/cosmetics & an energy & environment business promoting R&D for biofuel production.

<p style="page-break-after:always"> </p>

## The End

end of file
