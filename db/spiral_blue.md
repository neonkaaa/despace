# Spiral Blue
> 2022.01.24 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/s/spiral_blue_logo1t.webp)](f/c/s/spiral_blue_logo1.webp)|<info@spiralblue.space>, +61(4)3199-3269, Fax …;<br> *1/39 Wellington St, Chippendale NSW 2008, Australia*<br> <https://www.spiralblue.space> ~~ [FB ⎆](https://www.facebook.com/spiralblueptyltd) ~~ [LI ⎆](https://www.linkedin.com/company/spiralblue)|
|:--|:--|
|**Business**|OBC|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO, Founder, Space Engineer — Taofiq Huq|

**Spiral Blue PTY LTD** is an Australian company aimed for developing of [onboard computing systems](obc.md) for Earth observation satellites. Founded 2017.

Spiral Blue develops onboard processing & computing systems for Earth observation satellites that process images in real‑time. It utilises remote sensing & AI technology to convert raw image data into information, as well as to reduce image size. It eliminates bandwidth issues, economises the process & improves lead time for satellite image‑based services.

<p style="page-break-after:always"> </p>

## The End

end of file
