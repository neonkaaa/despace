# Proto flight model
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [Test](test.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Протолётная модель (ПЛМ)** — русскоязычный термин. **Proto flight model (PFM)** — англоязычный эквивалент.</small>

**Протолётная модель (ПЛМ)** — *по [ГОСТ 56469](гост_56469.md)* — предназначенный для запуска автоматический [космический аппарат](sc.md), на котором перед полётом проводятся частичные или полные протолётные квалификационные [испытания](test.md).



## Описание

This term is defined in ECSS-S-ST-00-01C as flight model on which a partial or complete protoflight qualification test campaign is performed before flight.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Test](test.md)】**<br> [JTAG](jtag.md) ~~ [Proto fligt model](pfm.md) ~~ [Безэховая камера](ach.md) ~~ [Валидация](vnv.md) ~~ [Класс чистоты](clean_lvl.md) ~~ [КПЭО](ctpr.md) ~~ [Перечень методик испытаний](list_tp.md) ~~ [Программа и методика испытаний](pmot.md) ~~ [Опытный образец](pilot_sample.md) ~~ [Циклограмма](obc.md) ~~ [Штатный образец](flight_unit.md) ~~ [ЭО](test.md) ~~ [Экспериментально‑теоретический метод](etetm.md)|

1. Docs:
   1. [ГОСТ 56469](гост_56469.md)
1. <https://ecss.nl/glossary/protoflight-model>
1. <https://www.eetimes.com/document.asp?:doc_id=1318780>


## The End

end of file
