# Цифровая вычислительная машина
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [GNC](gnc.md), [ЦВМ](obc.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Цифровая вычислительная машина (ЦВМ)** — русскоязычный термин. **On-board computer (OBC)** — англоязычный эквивалент.</small>

**Цифровая вычислительная машина (ЦВМ)**, также **Компью́тер** *(англ. computer, МФА: [kəmˈpjuː.tə(ɹ)] — 「вычислитель」)* — устройство или система, способная выполнять заданную, чётко определённую, изменяемую последовательность операций. Это чаще всего операции численных расчётов и манипулирования данными, однако сюда относятся и операции ввода‑вывода. Описание последовательности операций называется **программой**.

**Электронно‑вычислительная машина (ЭВМ)** — один из способов реализации компьютера. Подразумевает использование электронных компонентов в качестве её функциональных узлов, однако компьютер может быть устроен и на других принципах — он может быть механическим, биологическим, оптическим, квантовым и т.п., работая за счёт перемещения механических частей, движения электронов, фотонов или эффектов других физических явлений. Кроме того, по типу функционирования вычисл. машина может быть **цифровой (ЦВМ)** и **аналоговой (АВМ)**. С другой стороны, термин 「компьютер」 предполагает возможность изменения выполняемой программы (перепрограммирования), что возможно не для всех видов ЭВМ.

**Notes:**

1. В [ИСС](iss_r.md) ЦВМ состоит из 2 блоков: вычислительного блока (скажем, [БИВК-МР](obc_lst.md)) и блока [ТМС](tms.md).
2. Все разновидности компьютера относятся к [АПС](hns.md).

Varieties:

1. Central processing unit (CPU) — for general use, runs most of the code of the operating system & apps.
2. Graphics processing unit (GPU) — handles graphics-related tasks, such as visualizing an app’s user interface & gaming.
3. Image processing unit (ISP) — can be used to speed up common tasks done by image processing applications.
4. Digital signal processor (DSP) — handles more mathematically intensive functions than a CPU, includes decompressing music files.
5. Neural processing unit (NPU) — used in high-end smartphones to accelerate machine learning (A.I.) tasks, these include voice recognition & camera processing.
6. Video encoder/decoder — handles the power-efficient conversion of video files & formats.
7. Secure Enclave — encryption, authentication, & security.

| |**[Фирма](contact.md)**|**Актуальные (масса, ㎏)**|
|:--|:--|:--|
|**EU**|[Bradford](bradford_eng.md)|[OBC](obc_lst.md) (0.5)|
| |[KP Labs](kplabs.md)|[Antelope](obc_lst.md) (0.8)|
|•|• • • • • • • • • • • • •|~ ~ ~ ~ ~ |
|**RU**|[МОКБ Марс](mars_mokb.md)|[МАРС 4](obc_lst.md) (8) ~~ [МАРС 7](obc_lst.md) (6)|
| |[НПП Антарес](npp_antares.md)|[МПК-003](obc_lst.md) (9) ~~ [МПК-002](obc_lst.md) (3.9)|
| |[НТЦ Модуль](ntc_module.md)|[БИВК-МН](бивк‑мн.md) () ~~ [БИВК-МР](obc_lst.md) (8) ~~ [БИВК-Р](obc_lst.md) (7.1) ~~ [ЦВМ-12](obc_lst.md) (2.2)|
| |НТИЦ 「ТЕХКОМ」|…<br> 【Archive: [ЦВМ22](obc_lst.md) (2.1)】|
| |[Спутникс](sputnix.md)|[БКУ_SXPA](obc_lst.md) (0.35)|

**Процессоры:**

1. [GR712RC](obc_lst.md)

**Manufacturers:**

| | |
|:--|:--|
|**AE**|…|
|**AU**|…|
|**CA**|・[Canadensys](canadensys.md)<br> ・[Xiphos](xiphos.md)|
|**CN**|…|
|**EU**|…|
|**IL**|…|
|**IN**|…|
|**JP**|・[Space Cubics](spacecubics.md)|
|**KR**|…|
|**RU**|・[ИРЗ](irz.md)<br> ・[ИСС](iss_r.md)<br> ・[НПЦАП](npcap.md)<br> ・<http://www.spacemicro.com/products/digital-systems.html>|
|**SA**|…|
|**SG**|…|
|**US**|・[DDC](ddc.md)|
|**VN**|…|


## Average characteristics


|*CPU*|*DMIPS/㎒*|*DMIPS Total*|
|:--|:--|:--|
|ARM Cortex M3 STM32F2, ARMv7-M (32-bit), 100 ㎒|1.25|125|
|RAD750 (BAE Systems), PowerPC 750 (32-bit), 110-200 MHz| |240-400|
|LEON3FT (Cobham Gaisler), SPARC V8 (32-bit), 100 MHz| |86|
|GR740 (Cobham Gaisler), SPARC V8 (32-bit), 250 MHz| |1000|
|Virtex-5QV (Xilinx), FPGA with embedded PowerPC 440 (32-bit), 400 MHz| |1000-2000 depends on FPGA|

Memory and storage:

1. RAM — 64 MB to 2 GB in common. For example, the RAD750 typically supports 256 MB - 1 GB of rad-hardened SDRAM.
2. Storage — Space-grade storage is also limited. Flash memory used in space may have capacities in the range of 1-64 GB. For example, the Curiosity rover has 256 MB of DRAM and 2 GB of flash memory.


## DMIPS & FLOPS

**DMIPS**

**Dhrystone MIPS** is a standardized speed test with a specific workload mix.

Different CPU architectures have different strengths and weaknesses. For example, a 75 ㎒ SPARC easily beats a 400 ㎒ PentiumII at AES, but loses in zlib decompression, so there is no way to tell which one is faster.

The Whetstone and Dhrystone tests are both synthetic tests that are useful mostly as a benchmark for scientific computing because of their composition, but there is no consensus on anything that is closer to a "realistic" workload, especially as realistic workloads for embedded devices will consist of a lot of idle time.

The result from a Dhrystone test is expressed in a unit of calculations per second, which does not necessarily correspond to actual CPU instructions per second, but is a tiny bit more comparable.

**Dhrystone MIPS** is the name of a standardised a very old benchmark software, it gives as result the measured number of MIPS, where MIPS is Million Instructions Per Second. The figure was adjusted from long gone computers, but it doesn't matter.

1.25 DMIPS/㎒ means that your CPU will be 125 MIPS at 100 ㎒. For example, it will be slower than a 1.8 DMIPS/㎒ CPU running at 80 ㎒.

DMIPS is a very flawed measurement method for modern high performance CPUs, but it is still a bit relevant for small microcontrollers. (there is also CoreMark)

**FLOPS**

**Floating point operations per second (FLOPS, flops or flop/s)** is a measure of computer performance in computing, useful in fields of scientific computations that require floating-point calculations.

<https://en.wikipedia.org/wiki/FLOPS>



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Guidance, Navigation & Control (GNC)](gnc.md)】**<br> [CAN](can.md) ~~ [LVDS](lvds.md) ~~ [MIL‑STD‑1553](mil_std_1553.md) (МКО) ~~ [OS](os.md) ~~ [RS‑232, 422, 485](rs_xxx.md) ~~ [SpaceWire](spacewire.md) ~~ [АСН, САН](ans.md) ~~ [БНО](nnb.md)[MIL‑STD‑1553](mil_std_1553.md) (МКО)[БАППТ](eas.md) ~~ [БКС](cable.md) ~~ [БУ](eas.md) ~~ [БШВ](time.md) ~~ [Гироскоп](iu.md) ~~ [Дальномер](doppler.md) (ИСР) ~~ [ДМ](iu.md) ~~ [ЗД](sensor.md) ~~ [Компьютер](obc.md) (ЦВМ, БЦВМ) ~~ [Магнитометр](sensor.md) ~~ [МИХ](mic.md) ~~ [МКО](mil_std_1553.md) ~~ [ПО](soft.md) ~~ [ПНА, ПОНА, ПСНА](devd.md) ~~ [СД](sensor.md) ~~ [Система координат](coord_sys.md) ~~ [СОСБ](devd.md)|
|**【[On-board computer (OBC)](obc.md)】**<br> … <br>~ ~ ~ ~ ~<br> **RU:** [OS](os.md) ~~ [МПК-003](obc_lst.md) (9) ~~ [БИВК-МР](obc_lst.md) (8) ~~ [МАРС 4](obc_lst.md) (8) ~~ [БИВК-Р](obc_lst.md) (7.1) ~~ [МАРС 7](obc_lst.md) (6) ~~ [МПК-002](obc_lst.md) (3.9) ~~ [ЦВМ-12](obc_lst.md) (2.2) ~~ [БКУ_SXPA](obc_lst.md) (0.35) ~~ [БИВК-МН](бивк‑мн.md) () *([ЦВМ22](obc_lst.md) (2.1))**|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Computer>



## The End

end of file
