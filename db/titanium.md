# Titanium
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [SGM](sc.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Титан** — русскоязычный термин. **Titanium** — англоязычный эквивалент.</small>

**Titanium** is a chemical element with the symbol **Ti** and atomic number 22. Its atomic weight is 47.867 measured in daltons. It is a lustrous transition metal with a silver color, low density, and high strength. Titanium is resistant to corrosion in sea water, aqua regia, and chlorine.

The two most useful properties of the metal are corrosion resistance and strength‑to‑density ratio, the highest of any metallic element. In its unalloyed condition, titanium is as strong as some steels, but less dense.

Density is 4 540 ㎏/m³.



## ВТ23
> <small>**ВТ23** — русскоязычный термин. **VT23** — англоязычный эквивалент.</small>

**ВТ23** — титановый деформируемый сплав.

Титановый деформируемый сплав ВТ23 с высокой прочностью и коррозионной стойкостью. Относится к сильнолегированным сплавам титана, где основные элементы — алюминий, молибден, ванадий, железо и хром. Из других примесей следует отметить цирконий, кремний, кислород, углерод, азот и водород. Сплав ВТ22 имеет кристаллическую решетку смешанного класса по структуре α + β. Материал ограниченно сваривается, поддается термической обработке в виде отжига, закалки и старения. Титан ВТ23 используется для изготовления кавитационно стойких деталей, работающих при средних и высоких температурах. Он широко востребован в различных отраслях современного машиностроения.

Химический состав марки ВТ23 в % согласно ОСТ 1-90013-81:

1. Ti (титан) 84.1 ‑ 89.3;
1. Fe (железо) 0.4 ‑ 1;
1. C (углерод) до 0.1;
1. Si (кремний) до 0.15;
1. Cr (хром) 0.8 ‑ 1.4;
1. Mo (молибден) 1.5 ‑ 2.5;
1. V (ванадий) 4 ‑ 5;
1. N (азот) до 0.05;
1. Al (алюминий) 4 ‑ 6.3;
1. Zr (цирконий) до 0.3;
1. O (кислород) до 0.15;
1. H (водород) до 0.015.

|**Раздел**|**[Value](value.md) (incl. [comparison](matc.md))**|
|:--|:--|
|Марка|ВТ23|
|Классификация|Титановый деформируемый сплав.|
|Применение|для изготовления кавитационно‑стойких изделий; класс по структуре α+β.|
|Зарубежные<br> [аналоги](analogue.md)| |

**Физ. свойства при +20 ℃**

|**Параметр**|**[Value](si.md)**|**Примечание**|
|:--|:--|:--|
|Плотность, ㎏/m³|4 430|**(4 540 у титана)**|
|Теплоёмкость, J/K| |
|Уд. теплоёмкость, Дж/(кг·град)| |
|Уд. эл. сопротивление, Ом·м| |

**Мех. свойства при +20 ℃**

|**Прокат**|**Толщина<br> или ⌀, ㎜**|**E, ㎬**|**G, ㎬**|**σ₋₁,<br> ㎬**|**σ<sub>в</sub>,<br> (㎫)**|**σ₀.₂,<br> (㎫)**|**δ₅,<br> (%)**|**σ<sub>сж</sub>,<br> ㎫**|**KCU,<br> (кДж/m²)**|**KCV,<br> (кДж/m²)**|
|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|
|Лист отожжённый| | | | |1 100‑1 200| |10‑13| | | |
|Лист закалка+старение| | | | |1 450‑1 600| |4‑6| | | |



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Structures, gears, materials (SGM)](sc.md)】**<br> [Гермоконтейнер](гермоконтейнер.md) ~~ [Датчик](sensor.md) ~~ [Задел](margin.md) ~~ [Изделие](unit.md) ~~ [Испарение материалов](matc.md) ~~ [Кавитация](cavitation.md) ~~ [КЗУ](cinu.md) (ВБУ КТ) ~~ [КХГ](cgs.md) ~~ [Контейнеры для транспортировки](ship_contain.md) ~~ [Крейцкопф](crosshead.md) ~~ [Номинал](nominal.md) ~~ [ПУС](lag.md) ~~ [ПНА, ПОНА, ПСНА](devd.md) ~~ [Резерв](reserve.md) ~~ [Слайс](слайс.md) ~~ [ТСП](tsp.md) ~~ [Типичные формы КА](sc.md) ~~ [Толкатель](толкатель.md) ~~ [Унификация](commonality.md)|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Titanium>
1. <http://metallicheckiy-portal.ru/marki_metallov/tit/VT23>
1. <http://splav-kharkov.com/mat_start.php?:name_id=1523>


## The End

end of file
