# TALOS
> 2020.04.04 [🚀](../../index/index.md) [despace](index.md) → [NASA](nasa.md), [PS](ps.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---
> <small>**Thruster Advancement for Low-temperature operation in space (TALOS)** — EN term. **Улучшения двигателей для низкотемпературной работы в космосе (ТАЛОС)** — literal RU translation.</small>

**Thruster Advancement for Low‑temperature operation in space (TALOS)** project by NASA’s Space technology mission directorate aims to meet these low‑cost, lightweight material needs by developing superior deep space thrusters. Project was active in 2019 ‑ ….



## Description
[NASA](nasa.md) is poised to return to the Moon and then farther out to deep space. These goals will require spacecraft propulsion systems that are high‑performance, lightweight, and compact. In addition, these systems need to be developed more quickly and for a lower cost than state of the art propulsion systems in order to support multiple lunar missions within a short period of time.

[Frontier Aerospace](frontier_as.md) of Simi Valley California is developing these high‑performing thrusters for the TALOS project. Five of these thrusters will be delivered to Astrobotic for use on its Peregrine lunar lander that will deliver science and technology payloads to the Moon for NASA in 2021.

Qualifying the TALOS thrusters for space flight on this lunar demonstration mission will help to enable a reliable, commercial‑off‑the‑shelf option for future science and exploration missions. Before the Astrobotic lunar mission, the TALOS project will perform qualification tests representative of a planned mission duty cycle for an autonomous lander in a lunar demonstration mission

TALOS runs on mixed oxides of nitrogen and monomethyl hydrazine propellant (MON‑25/MMH)is and is lighter and costs less than accessible thrusters in comparable thrust classes. Because MON‑25 operates at lower temperatures, less power is needed for propellant conditioning for in‑space propulsion applications, especially long duration and deep‑space missions. TALOS offers enhanced affordability through improved designs, modern materials, and advanced manufacturing processes, which lower thruster unit cost for missions and reduce propulsion system costs.

Frontier is scheduled to complete development hot‑fire testing of two 150‑lbf thrusters and two 10‑lbf thrusters in 2020 for delivery to [Astrobotic](astrobotic.md). TALOS technology is key to the enhanced affordability of future science and space exploration missions.

**Team:**

1. Principal Technologist — Ron Litchford (<ron.litchford@nasa.gov>)
1. Project Manager — Greg Barnet (<gregory.l.barnett@nasa.gov>)


### News
**2020.03.24 <https://www.nasa.gov/centers/marshall/news/releases/2020/moon-thrusters-withstand-over-60-hot-fire-tests.html>** Moon Thrusters Withstand Over 60 Hot‑Fire Tests

<small>Future Artemis lunar landers could use next‑generation thrusters, the small rocket engines used to make alterations in a spacecraft’s flight path or altitude, to enter lunar orbit and descend to the surface. Before the engines make the trip to the Moon, helping deliver new science instruments and technology demonstrations, they’re being tested here on Earth.<br> NASA and Frontier Aerospace of Simi Valley, California, performed roughly 60 hot‑fire tests on two thruster prototypes over the course of 10 days. The tests concluded March 16 and took place in a vacuum chamber that simulates the environment of space at Moog‑ISP in Niagara Falls, New York. While replicating mission flight operations, engineers collected multiple data streams, including the pressure and stability of the combustion chamber and the pressure and temperature of the feed system, which delivers propellant from tanks to the thruster.<br> Being developed under NASA’s Thruster for the Advancement of Low‑temperature Operation in Space (TALOS) project, the thrusters are designed to reduce spacecraft cost, mass and power — three things that constrain every space mission. Astrobotic Technology of Pittsburgh plans to use the new thrusters aboard their Peregrine lunar lander.<br> 「TALOS is about leveraging the benefits of MON‑25, which will reduce the amount of power needed for spacecraft when operating in extremely low temperatures,」 said TALOS Project Manager Greg Barnett at NASA’s Marshall Space Flight Center in Huntsville, Alabama.<br> The thrusters burn mixed oxides of nitrogen and monomethyl hydrazine propellants (MON‑25/MMH), which are capable of operating at low temperatures for an extended period of time without freezing. Although MON‑25 has been tested since the 1980s, no spacecraft currently uses the propellant. TALOS is capable of operating at a wide propellant temperature range, between −40 and +80 ℉ (−40 ‑ +26 ℃). That’s compared to state‑of‑the‑art thrusters of the same size that generally operate between 45 and 70 ℉ (+7 ‑ +21 ℃).<br> Because MON‑25 does not need to be conditioned at extreme temperatures like other mixed oxides of nitrogen propellants, it will reduce power requirements for spacecraft operating in low temperatures, resulting in smaller, lighter and less expensive systems. Reducing power requirements for the spacecraft could potentially reduce the number of batteries and the size of solar panels needed to maintain the spacecraft.<br> 「NASA will soon verify this versatile thruster design for space so that the agency and commercial companies can easily implement the technology in future missions,」 said Barnett. 「Astrobotic plans to use this thruster design on their lunar lander that will deliver science and technology payloads to the Moon for NASA in 2021.」<br> The TALOS project is slated to perform engine qualification testing in late summer to ready the thruster design for use on Astrobotic’s Peregrine lander. Astrobotic is one of several American companies working with NASA to deliver science and technology to the lunar surface through the Commercial Lunar Payload Services (CLPS) initiative, as part of the Artemis program.<br> In addition to sending instruments to study the Moon, NASA’s Artemis lunar exploration program will land the 1st woman and next man on the lunar surface by 2024 and establish a sustained presence by 2028. The agency will leverage its Artemis experience and technologies to prepare for the next giant leap — sending astronauts to Mars.<br> The TALOS thruster is being developed by Frontier Aerospace. The project is led and managed by NASA’s Marshall Space Flight Center in Huntsville, Alabama. Once the TALOS design has been qualified for flight, Frontier Aerospace will build the thrusters for Astrobotic’s lunar lander under a project called Frontier Aerospace Corporation Engine Testing (FACET). The Game Changing Development program within NASA’s Space Technology Mission Directorate funds the technology development project.</small>



## Docs/Links
|**Sections & pages**|
|:--|
|**`Двигательная установка (ДУ):`**<br> [HTAE](htae.md) ~~ [TALOS](talos.md) ~~ [Баки топливные](fuel_tank.md) ~~ [Варп‑двигатель](ps.md) ~~ [Газовый двигатель](ps.md) ~~ [Гибридный двигатель](гбрд.md) ~~ [Двигатель Бассарда](ps.md) ~~ [ЖРД](ps.md) ~~ [ИПТ](ing.md) ~~ [Ионный двигатель](иод.md) ~~ [Как считать топливо?](si.md) ~~ [КЗУ](cinu.md) ~~ [КХГ](cgs.md) ~~ [Номинал](nominal.md) ~~ [Мятый газ](exhsteam.md) ~~ [РДТТ](ps.md) ~~ [Сильфон](сильфон.md) ~~ [СОЗ](соз.md) ~~ [СОИС](соис.md) ~~ [Солнечный парус](солнечный_парус.md) ~~ [ТНА](turbopump.md) ~~ [Топливные мембраны](топливные_мембраны.md) ~~ [Топливные мешки](топливные_мешки.md) ~~ [Топливо](ps.md) ~~ [Тяговооружённость](ttwr.md) ~~ [ТЯРД](тярд.md) ~~ [УИ](ps.md) ~~ [Фотонный двигатель](фотонный_двигатель.md) ~~ [ЭРД](ps.md) ~~ [Эффект Оберта](oberth_eff.md) ~~ [ЯРД](ps.md)|

1. Docs: …
1. <https://gameon.nasa.gov/projects/thruster-advancement-for-low‑temperature-operation-in-space-talos>


## The End

end of file
