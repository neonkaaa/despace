# Nihon University
> 2020.07.18 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/n/nihon_univ_logo1t.webp)](f/c/n/nihon_univ_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *4-chōme-8-24 Kudanminami, Chiyoda City, Tōkyō-to 102-0074, Japan*<br> <http://www.nihon-u.ac.jp> ~~ [LI ⎆](https://www.linkedin.com/school/nihon-college) ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/Nihon_University)|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Nihon University** (日本大学, Nihon Daigaku), abbreviated as Nichidai (日大), is a private research university in Japan.

College of Science & Technology (1920-; Chiyoda, Tokyo & Funabashi, Chiba):

- Aerospace Engineering
- Architecture
- Civil Engineering
- Electrical Engineering
- Electronics & Computer Science
- Materials & Applied Chemistry
- Mathematics
- Mechanical Engineering
- Oceanic Architecture & Engineering
- Physics
- Precision Machinery Engineering
- Transportation Engineering & Socio‑Technology

Notable space departments:

- [Department of Aerospace Engineering ⎆](https://www.cst.nihon-u.ac.jp/en/graduate/g07_aerospace.html)



## The End

end of file
