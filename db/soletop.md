# SOLETOP
> 2022.01.08 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/s/soletop_logo1t.webp)](f/c/s/soletop_logo1.webp)|<support@soletop.com>, +82(42)867-7440, Fax +82(42)867-7445 ;<br> *409, Expo-ro, Yuseong-gu, Daejeon, 34051, South Korea*<br> <https://soletop.com> ~~ [FB ⎆](https://www.facebook.com/soletopkorea) ~~ [LI ⎆](https://www.linkedin.com/company/soletop)|
|:--|:--|
|**Business**|CubeSat R&D (one‑stop solutions), geospatial info, unmanned system, instruments, software|
|**Mission**|Lead the global market with the development of innovative technology in order to offer the best solutions.|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**SOLETOP Co., Ltd** is a South Korean company aimed for remote sensing & control technology. Founded 1995.

Office in US: OSSEN Solutions, Inc. 6081 Dale st, STE D, Buena Park, CA 90621. Phone +1-714-404-1213. Fax +1-844-404-1213. <info@osseninc.com>

Circumstantially space business has been growing from Satellite control systems software to Remote sensing & to UAV control & environmental monitoring products, for instance, SOLETOP continues growing steadily on the space, remote sensing & telepresence sectors. Back in 2014, SOLETOP added more core technology products to expand its portfolio & solutions have more for the present market, covering the demanding necessities of our customers through the customized solutions. If the solution does not exist our professional team is ready to take the challenge to make things happen for them. We are always ready!

**Our name & logo.** Soletop’s founder created our name by combining Spanish & English words. 「Sol」 is a Spanish word that stands for Sun & 「top」 means the highest point of the cosmos! 「S」 stands for Sky or Space, 「O」 stands for Ocean, 「L」 stands for land & 「E」 stands for Earth as a whole. 「Top」 means the best! Our logo clearly shows the meaning of each word by color & alphabet. SOL & TOP are in dark blue while 「E」 is in magenta which stands for green Earth, our home, Planet Earth!

<p style="page-break-after:always"> </p>

## The End

end of file
