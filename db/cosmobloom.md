# Cosmobloom
> 2023.07.30 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/c/cosmobloom_logo1t.webp)](f/c/c/cosmobloom_logo1.webp)|<info@cosmo-bloom.com>, <mark>nophone</mark>, Fax …;<br> *229 ROKUGO BASE, 3-10-16 Minami Rokugo, Ota-ku, Tokyo 144-0045, Japan*<br> <https://cosmo-bloom.com/> ~~ [FB ⎆](https://www.facebook.com/profile.php?id=100094228578602) ~~ [LI ⎆](https://www.linkedin.com/company/株式会社cosmobloom) ・ [X ⎆](https://twitter.com/cosmobloom_Inc)|
|:--|:--|
|**Business**|Solutions in the numerical analysis, design, & development of deployable flexible structures & the space mission using them.|
|**Mission**|Out team will continue to find the solutions by pursuing deployable space structure|
|**Vision**|Keep challenging, create together, bring hope for all|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO & Co‑founder — Momoko Fukunaga<br> ・CTO & Co‑founder — Hiroyuki Ono|

**Cosmobloom Inc.** is a JP company aimed to perform analysis & design of foldable space structures.

What we do:

1. Analysis tool — We provide the SaaS of the nonlinear elasto‑dynamic analysis code “NEDA” that we have originally developed.
1. Design support — We support the analysis & design of deployable structure including extremely flexible members such as membranes & cables.
1. Consulting — We provide the solutions for the design, development, & utilization of deployable flexible structures.



## The End

end of file
