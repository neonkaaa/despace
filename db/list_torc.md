# Перечень ТЗ, согласуемых с Заказчиком
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → **[Док.](doc.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Перечень ТЗ, согласуемых с Заказчиком** — русскоязычный термин, не имеющий аналога в английском языке. **List of terms of reference agreed with the customer** — дословный перевод с русского на английский.</small>

**Перечень технических заданий, подлежащих согласованию с Заказчиком и ГНИО**, также в обиходе **Перечень ТЗ, согласуемых с Заказчиком** — документ, определяющий перечень [ТЗ](tor.md), которые должны быть отдельно согласованы с Заказчиком и прочими организациями по его усмотрению.

Документ разрабатывается на этапе [ЭП](rnd_ep.md), согласуется с [4116 ВП](milro.md), с [ЦНИИмаш](tsniimash.md), со всеми перечисленными в нём организациями, утверждается у Заказчика.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[](.md)】**<br> <mark>NOCAT</mark>|

1. Docs: …
1. <…>


## The End

end of file
