# Конструктивное совершенство
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [Качество](qm.md), [SGM](sc.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Конструктивное совершенство (КСО)** — русскоязычный термин, не имеющий аналога в английском языке. **Constructive perfection** — дословный перевод с русского на английский.</small>

**Конструктивное совершенство** изделия — комплексная характеристика изделия, позволяющая сравнивать изделие с аналогами и конкурентами. Включает в себя:

1. сложность изделия (чем проще, тем лучше);
1. массу изделия (чем меньше, тем лучше);
1. габариты изделия (чем меньше, тем лучше);
1. затраты дорогих или дефицитных материалов (чем меньше, тем лучше);
1. технологичность конструкции;
1. КПД (чем выше, тем лучше).

Простота, малый вес и расход дорогих материалов влияют на стоимость оборудования, а значит на величину капитальных затрат.

Под технологичностью конструкции обычно понимают дешевизну, лёгкость, удобство изготовления, которые достигаются за счёт простоты формы, уменьшения величины обрабатываемой поверхности, правильно выбранных допусков, применения стандартных деталей или нормализованных деталей и узлов. Технологичность особенно важна для серийной продукции.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**`Качество:`**<br> [Bus factor](bus_factor.md) ~~ [Way](faq.md) ~~ [АВПКО](fmeca.md) ~~ [Авторский надзор](des_spv.md) ~~ [Бережливое производство](lean_man.md) ~~ [Валидация, верификация](vnv.md) ~~ [Класс чистоты](clean_lvl.md) ~~ [Конструктивное совершенство](con_vel.md) ~~ [Крит. технологии](kt.md) ~~ [Крит. элементы](sens_elem.md) ~~ [Метрология](metrology.md) ~~ [Надёжность](qm.md) ~~ [Нештатная ситуация](emergency.md) ~~ [Номинал](nominal.md) ~~ [Ошибки](faq.md) ~~ [Система менеджмента качества](qms.md) ~~ [УГТ](trl.md)/[TRL](trl.md)|

1. Docs: …
1. <…>


## The End

end of file
