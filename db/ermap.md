# Программа эргономического обеспечения
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [Док.](doc.md), **[Эргономика](hfe.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Программа эргономического обеспечения (ПЭО)** — русскоязычный термин, не имеющий аналога в английском языке. **Ergonomic management program** — дословный перевод с русского на английский.</small>

**Программа эргономического обеспечения (ПЭО)**.

Содержание:

1. Введение
1. Общие положения
1. Содержание эргономического обеспечения
1. Методическое обеспечение
1. Порядок контроля выполнения и корректировки ПЭО



## Разработка и согласование
|<small>*Этап*</small>|<small>*Наим. документа*</small>|<small>*Разраб.*</small>|<small>*Согл.*</small>|<small>*Утвер.*</small>|<small>*Основание*</small>|
|:--|:--|:--|:--|:--|:--|:--|
|[НИР](rnd_0.md)|—|—|—|—| |
|[АП](rnd_ap.md)|—|—|—|—| |
|[ЭП](rnd_ep.md)|ПЭО|<small>Исполнитель.</small>|ГИ.|<small>Исполнитель.</small>|<small>ГОСТ 15.203, табл.А.2, п.8;<br> РК‑11‑КТ, п.3.1.5.</small>|
|[ТП](rnd_tp.md)| | | | | |
|[РКД](ркд.md)| | | | | |
|[НЭО](test.md)|—|—|—|—|—|
|[ЛИ](rnd_e.md)| | | | | |



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**`Эргономика:`**<br> [ПЭО](ermap.md)|

1. Docs: …
1. <…>


## The End

end of file
