# Charge efficiency
> 2019.09.19 [🚀](../../index/index.md) [despace](index.md) → [EB](eb.md), [SPS](sps.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Charge efficiency, Electrical output** — EN term. **Электрическая отдача** — RU analogue.</small>

**Charge efficiency** — the ratio of the electrical energy charged in [EB](eb.md) to the energy delivered by this HPS under nominal conditions.



## Docs/Links
|**Sections & pages**|
|:--|
|**【[Spacecraft power system (SPS)](sps.md)】**<br> [Charge eff.](charge_eff.md) ~~ [EAS](eas.md) ~~ [EB](eb.md) ~~ [EMI, RFI](emi.md) ~~ [NR](nr.md) ~~ [Rotor](iu.md) ~~ [RTG](rtg.md) ~~ [Solar cell](sp.md) ~~ [SP](sp.md) ~~ [SPB/USPB](suspb.md) ~~ [Voltage](sps.md) ~~ [WT](wt.md)<br>~ ~ ~ ~ ~<br> **RF/CIF:** [BAK‑01](eas_lst.md) ~~ [KAS‑LOA](eas_lst.md)|
|**【[Chemical source of electricity (CSE), Electric battery (EB)](eb.md)】**<br> [Charge efficiency](charge_eff.md) <br>~ ~ ~ ~ ~<br> **EU:** [8S8P](eb_lst.md) (30)  ▮  **RU:** [8LI-70](eb_lst.md) (70) ~~ [12LI-120](eb_lst.md) (120)|

1. Docs: …
1. <…>


## The End

end of file
