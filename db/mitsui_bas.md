# Mitsui Bussan Aerospace Co., Ltd.
> 2021.10.26 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/m/mitsui_bussan_as_logo1t.webp)](f/c/m/mitsui_bussan_as_logo1.webp)|<spacebiz@mb-aero.co.jp>, +81-3-4586-1915, Fax 8-2, Marunouchi 1-Chome, Chiyoda-ku, Tekko Building 22F, Tokyo, 100-0005 - Japan;<br> *…*<br> <https://mba-space.com/en>|
|:--|:--|
|**Business**|Sat R&D, launch, deployment, operation|
|**Mission**|We will support the realization of a safe & prosperous society by contributing to the common good through our work in the field of aerospace.|
|**Vision**|As part of the Mitsui & Co., Ltd. Group, we aim to become a solution provider respected & trusted by both our domestic & international customers, adhering strictly to the standards of society.|
|**Values**|➀ Always striving to take up challenges in new aerospace fields, we shall dynamically create business related to security, law enforcement, disaster relief, lifesaving & international contributions.<br> ➁ We shall provide our customers with both the high value‑added products they require & responsible support, on a respectful & law‑abiding basis.<br> ➂ We will carry out work in the fields of logistics & marketing in good faith, cultivating a sense of professional responsibility.<br> ➃ We shall develop dynamic & responsible human resources within the organization by developing the imagination & ingenuity of our predecessors & providing our employees with the opportunity for self‑improvement & self‑fulfillment.<br> ➄ We shall always take into consideration & coordinate the interests of all our stakeholders.|
|**[MGMT](mgmt.md)**|…|

**Mitsui Bussan Aerospace Co., Ltd.** (a part of Mitsui & Co., Ltd.). Established 02.04.1982.

- Development — satellite R&D based on your ideas
- Experimental Opportunity — We can introduce experimental opportunity at the ISS, such as Japanese experimental module KIBO, & Bartolomeo provided by Airbus.
- Launch & Deployment — launch & deploy your satellite
- One Stop Service — We provide all necessary service in accordance with the requirements from each customer, such as development of satellite, launch & deployment & operation. Customer easily reaches out the best solution to avoid any stress by only contact to us. We arrange & negotiateswith satellite vendors for price, contract term, export control & other process on behalf of customer.
- Operation — acting for the operation of the satellite & provide data to you

<p style="page-break-after:always"> </p>

## The End

end of file
