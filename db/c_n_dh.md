# Command & Data-handling
> 2020.11.07 [🚀](../../index/index.md) [despace](index.md) → **[](.md)** <mark>NOCAT</mark>  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**RU** — RU term w/o analogues in English. **EN** — literal EN translation.</small>

The **Command and Data Handling (C&DH)** subsystem is essentially the 「brains」 of the spacecraft and controls all spacecraft functions. The C&DH system handles all data sent and received by the spacecraft, including science data and spacecraft or payload operations. The system is connected to the RF transmitter and receiver units provide a communication channel between the spacecraft and the ground operators. The basic data flow over a space link is made of Telemetry (TM) and Telecommand (TC) data.

Command & Data-handling Systems system:

1. manages all forms of data on the spacecraft;
1. carries out commands sent from Earth;
1. prepares data for transmission to Earth;
1. manages collection of solar power and charging of the batteries;
1. collects and processes information about all subsystems and payloads;
1. keeps and distributes the spacecraft time;
1. calculates the spacecraft's position in orbit around the planet;
1. carries out commanded maneuvers;
1. autonomously monitors and responds to a wide range of onboard problems that might occur.

The key parts of this system are:

1. **[Data storage system](ds.md).** The science data is stored on this recorder until it is ready for transmission to Earth, and then is overwritten with new science data.
1. **[Flight Software](soft.md).** The Flight Software is an integral part of the Space Flight Computer, and includes many applications running on top of an operating system. Common operating system is VxWorks.
1. **[Space Flight Computer](obc.md) (On-board computer, OBC)**


░░░╟ Бортовой комплекс управления (БКУ)  
░░░║░╟ Высотомер  
░░░║░╟ Гироскоп  
░░░║░╟ Датчик звёздный (ЗД)  
░░░║░╟ Датчик контроля электризации  
░░░║░╟ Датчик относительной ориентации (GPS, GLONASS)  
░░░║░╟ Датчик солнечный (СД)  
░░░║░╟ Двигатель‑маховик (ДМ)  
░░░║░╟ Запоминающее устройство (ЗУ)  
░░░║░╟ Привод антенны  
░░░║░╟ Привод солнечных панелей (ПСП)  
░░░║░╟ Программное обеспечение (ПО)  
░░░║░╙ Цифровая вычислительная машина (ЦВМ)

【**Table.** Manufacturers】

| | |
|:--|:--|
|**AE**|…|
|**AU**|…|
|**CA**|・[Xiphos](xiphos.md) — CPUs (OBCs), Firmware & Software|
|**CN**|…|
|**EU**|…|
|**IL**|…|
|**IN**|…|
|**JP**|…|
|**KR**|…|
|**RU**|…|
|**SA**|…|
|**SG**|…|
|**US**|…|
|**VN**|…|



## Docs/Links
|**Sections & pages**|
|:--|
|**【[](.md)】**<br> <mark>NOCAT</mark>|

1. Docs: …
1. <https://mars.nasa.gov/mro/mission/spacecraft/parts/command>
1. <https://www.ruag.com/en/products-services/space/electronics/satellite-and-launcher-computers/command-and-data-handling-systems>


## The End

end of file
