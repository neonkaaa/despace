# DOORS
> 2019.12.16 [🚀](../../index/index.md) [despace](index.md) → **[Soft](soft.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Rational Dynamic Object Oriented Requirements System (DOORS)** — англоязычный термин, не имеющий аналога в русском языке. **Динамическая объектно‑ориентированная система управления требованиями (ДОРС)** — дословный перевод с английского на русский.</small>

**Rational Dynamic Object Oriented Requirements System (DOORS)** is a requirement management tool. It is a client–server application, with a Windows‑only client and servers for Linux, Windows, and Solaris. There is also a web client, DOORS Web Access. Produced in 2 variants:

1. DOORS Classic — client‑server software.
1. DOORS Next Gen — SaaS.

[![](f/soft/doors_pic01t.webp)](f/soft/doors_pic01.webp)



## Описание
Rational DOORS Next Generation is now developed on the IBM Jazz platform. The Jazz platform leverages Open Services for Lifecycle Collaboration (OSLC).

In order to complete its functionality, Rational DOORS has an open architecture that supports third‑party plugins.

DOORS was originally published by Quality Systems and Software Ltd (QSS) in 1991. Telelogic acquired QSS in mid‑2000.

Ключевые функции:

1. **Централизованное управление требованиями.** DOORS обеспечивает централизованное управление документацией, содержащей описание требований. Клиент для настольных ПК обеспечивает доступ к функциям редактирования, настройки, анализа и отчётов. Поддерживается формат обмена требованиями, что даёт возможность партнёрам добавлять документы, разделы или атрибуты с требованиями для сопоставления с основной БД. DOORS также сохраняет и выводит текстовую информацию, изображения, таблицы и другие элементы, описывающие требования.
1. **Трассируемость для связывания требований с элементами проекта.** Данное решение поддерживает функцию перетаскивания для связывания требований с элементами проекта, тестовыми наборами и пр. Требования можно выбрать в списке или ввести номер требования в качестве атрибута, чтобы DOORS автоматически создал ссылки. Подробные отчёты о трассируемости в едином представлении помогают разработчикам определить приоритеты и график поставки. Также поддерживаются внешние ссылки, которые позволяют напрямую связывать требования с информацией, хранящейся за пределами DOORS.
1. **Масштабируемость в соответствии с меняющимися потребностями.** Иерархическое представление папок и проектов в DOORS упрощает навигацию. Настраиваемые представления и общий доступ к файлам позволяют пользователям одновременно работать над одним документом.
1. **Инструменты отслеживания тестов в неавтоматизированных средах тестирования.** С помощью этого комплекта инструментов можно создавать связи между требованиями и тестовыми сценариями. Можно определять тестовые сценарии, записи и сравнивать результаты тестов. Данное решение позволяет удостовериться в том, что тестовые сценарии охватывают все требования.
1. **Интегрированные инструменты для управления изменениями требований.** Данное решение предусматривает два варианта управления изменениями требований: простая готовая система предложений изменений либо более детальный настраиваемый поток операций контроля изменений с решениями для управления изменениями, в частности IBM Rational Quality Manager, IBM Rational Rhapsody и IBM Rational Focal Point. Решение также интегрируется с HP QualityCenter для наглядного представления требований с возможностью создания тестовых сценариев для обеспечения трассируемости на основе Microsoft Team Foundation Server (TFS).
1. **Дополнительные модули, расширяющие возможности Rational DOORS.** DOORS Web Access и DOORS Analyst расширяют функциональные возможности этого решения для управления требованиями. DOORS Web Access предоставляет веб‑интерфейс для создания, проверки, редактирования и обсуждения требований, хранящихся в базе данных IBM DOORS. Дополнительный модуль IBM DOORS Analyst предоставляет средства моделирования требований, с помощью которых можно создавать модели, изображения и диаграммы с требованиями для IBM DOORS.



## DOORS Extension Language
Rational DOORS has its own programming language called DOORS eXtension Language (DXL).

DOORS Extension Language (DXL) is a scripting language used to extend the functionality of IBM’s Rational DOORS. This programming language is somewhat similar to C and C++. This language is specific to DOORS, and it has its own syntax, declaration, a forum to discuss the development, in addition to the specific reference manuals. DXL scripts were utilized to help the users of DOORS. Much of the native DOORS GUI is written in DXL.

DXL is used in the client‑server based "DOORS Classic"; it is not used in the newer web‑based "DOORS Next Generation".



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Software](soft.md)】**<br> [ASP](asp.md) ~~ [Blender](blender.md) ~~ [C](plang.md) ~~ [Cosmographia](cosmographia.md) ~~ [DOORS](doors.md) ~~ [DWG](cad_f.md) ~~ [GIMP](gimp.md) ~~ [Git](git.md) ~~ [IGES](cad_f.md) ~~ [ISIS](isis.md) ~~ [JT](cad_f.md) ~~ [NGT](neogeography_toolkit.md) ~~ [NX](nx.md) ~~ [Octave](gnu_octave.md) ~~ [OS](os.md) ~~ [PDF](pdf.md) ~~ [Python](plang.md) ~~ [R](plang.md) ~~ [SPICE](spice.md) ~~ [STEP](cad_f.md) ~~ [STL](stk.md) ~~ [SVG](cad_f.md) ~~ [Syncthing](syncthing.md) ~~ [SysML](sysml.md) ~~ [Teamcenter](teamcenter.md) ~~ [Valispace](valispace.md) ~~ [Система управления версиями](vcs.md) ~~ [ХРИП](adra.md)|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Rational_DOORS>
1. <https://en.wikipedia.org/wiki/DOORS_Extension_Language>
1. <https://www.ibm.com/us-en/marketplace/rational-doors>
1. <https://www.ibm.com/ru-ru/marketplace/requirements-management>



## The End

end of file
