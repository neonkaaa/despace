# Астрономические периоды
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → **[Space](index.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

Вкратце:

1. сидерический период — год;
1. синодический период — новолуние выбранной планеты;
1. период вращения — сутки.

【**Таблица.** Сидерические периоды планет Солнечной системы】  
В таблицу также включены показатели для Луны, астероидов главного пояса, карликовых планет и Седны.

|**Планета**|**Сидерический период,<br> лет (Земных суток)**|**Синод. период<br> отн. Земли, лет**|**Период вращения,<br> Земных суток**|
|:--|:--|:--|:--|
|[Солнце](sun.md)| | |25.379995|
|[Меркурий](mercury.md)|0.24 (87.97)|0.317|58.6462|
|[Venus](venus.md)|0.615 (224.7)|1.599|243.0187 ретроградное|
|[Земля](earth.md)|1 (365.2564)| |0.99726968|
|[Луна](moon.md) (вокруг Земли)|0.0748 (27.322)| |27.321661|
|[Марс](mars.md)|1.88|2.135|1.02595675 (24 ч 39 м 35.24409 с)|
|Астероиды (в среднем)|4.6| | |
|[Юпитер](jupiter.md)|11.86|1.092|0.41354|
|[Сатурн](saturn.md)|29.46|1.035|0.44401|
|[Уран](uranus.md)|84.02|1.012|0.71833 ретроградное|
|[Нептун](neptune.md)|164.78|1.006|0.67125|
|[Плутон](pluto.md)|248.09| |6.38718 ретроградное|
|[Хаумеа](haumea.md)|285| | |
|[Макемаке](makemake.md)|309.88| | |
|[Эрида](eris.md)|557| | |
|[Седна](sedna.md)|12 059| | |



## Сидерический период
**Сидери́ческий пери́од обраще́ния** (от лат. *sidus*, звезда; род. падеж *sideris*) — промежуток времени, в течение которого какое‑либо небесное тело‑спутник совершает вокруг главного тела полный оборот относительно звёзд. Понятие 「сидерический период обращения」 применяется к обращающимся вокруг Земли телам — Луне (сидерический месяц) и искусственным спутникам, а также к обращающимся вокруг Солнца планетам, кометам и др.

Сидерический период также называют годом. Например, Меркурианский год, Юпитерианский год, и т.п. При этом не следует забывать, что словом 「год」 могут называться несколько понятий. Так, не следует путать земной сидерический год (время одного оборота Земли вокруг Солнца) и год тропический (время, за которое происходит смена всех времён года), которые различаются между собой примерно на 20 минут (эта разница обусловлена, главным образом, прецессией земной оси).



## Синодический период
**Синоди́ческий пери́од обраще́ния** (от греч. σύνοδος — соединение) — промежуток времени между двумя последовательными соединениями Луны или какой‑нибудь планеты Солнечной системы с Солнцем при наблюдении за ними с Земли. При этом соединения планет с Солнцем должны происходить в фиксированном линейном порядке, что существенно для внутренних планет: например, это будут последовательные верхние соединения, когда планета проходит за Солнцем.

Синодический период Луны равен промежутку времени между двумя новолуниями или двумя любыми другими одинаковыми последовательными фазами.

**Связь с сидерическим периодом**

Формула связи между сидерическими периодами обращения двух планет (за одну из них принимаем Землю) и синодического периода S одной относительно другой:  
`1/S = 1/Z − 1/T` (для внешних планет)  
`1/S = 1/T − 1/Z` (для внутренних планет),  
где Z — сидерический период Земли (1 год), Т — сидерический период планеты.



## Период вращения
**Период вращения космического объекта** — это период времени, который требуется объекту для совершения полного оборота вокруг своей оси относительно звёзд.

Период вращения (физический термин) — промежуток времени, в течение которого точка совершает полный оборот, двигаясь по окружности.

Период вращения Земли относительно точки весеннего равноденствия называется звёздными сутками.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[](.md)】**<br> <mark>NOCAT</mark>|

1. Docs: …
1. <http://ru.wikipedia.org/wiki/Сидерический_период>
1. <http://ru.wikipedia.org/wiki/Синодический_период>
1. <https://en.wikipedia.org/wiki/Rotation_period>


## The End

end of file
