# Атмосфера
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [ВВФ](ef.md), [СИ и формулы](si.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small> ・**Атмосфера** — русскоязычный термин. **Atmosphere** — англоязычный эквивалент.<br> ・**[Атмосфера Земли](earth.md)** — русскоязычный термин. **Atmosphere of Earth** — англоязычный эквивалент.</small>

**Атмосфера** *(от. др.‑греч. ἀτμός — 「пар」 и σφαῖρα — 「сфера」)* — газовая оболочка небесного тела, удерживаемая около него гравитацией. Поскольку не существует резкой границы между атмосферой и межпланетным пространством, то обычно атмосферой принято считать область вокруг небесного тела, в которой газовая среда вращается вместе с ним как единое целое. Толщина атмосферы некоторых планет, состоящих в основном из газов (газовые планеты), может быть очень большой.

**Атмосфера** — внесистемная единица измерения давления, приблизительно равная атмосферному давлению на поверхности Земли на уровне Мирового океана. См. [СИ, формулы](si.md). 101 325 ㎩ ≈ 101 ㎪ = 760 ㎜ рт ст.

Атмосфера [Земли](earth.md) содержит кислород, используемый большинством живых организмов для дыхания, и диоксид углерода, потребляемый растениями и цианобактериями в процессе фотосинтеза. Атмосфера также является защитным слоем планеты, защищая её обитателей от солнечного ультрафиолетового излучения и метеоритов.

Атмосфера есть у всех массивных тел — планет земного типа, газовых гигантов.

Также единица давления, ㎝. [СИ](si.md).



## Атмосфера Земли

Атмосферные газы Земли рассеивают синие длины волн лучше других, поэтому если смотреть из космоса, то вокруг нашей планеты имеется голубое гало, а если смотреть с Земли, то видно голубое небо.

|Голубое небо|[Атмосфера Земли](earth.md)|
|:--|:--|
|[![](f/aob/earth/atmosphere_top_of_atmospheret.webp)](f/aob/earth/atmosphere_top_of_atmosphere.webp)|[![](f/aob/earth/atmosphere_earth_atmosfeert.webp)](f/aob/earth/atmosphere_earth_atmosfeer.webp)|



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[External factors (EF)](ef.md)】**<br> [Astro.object](aob.md) ~~ [Atmosphere](atmosphere.md) ~~ [Atmosphere of Earth](earth.md) ~~ [Cosmic rays](ion_rad.md) ~~ [EMI](emi.md) ~~ [Grav.waves](gravwave.md) ~~ [Ion.radiation](ion_rad.md) ~~ [Radio frequency](comms.md) ~~ [Solar phenomena](solar_ph.md) ~~ [Space debris](sdeb.md) ~~ [Standart conditions](sctp.md) ~~ [Time](time.md) ~~ [VA radiation belts](ion_rad.md)|
|**`СИ, формулы:`**<br> [Атмосфера](atmosphere.md) ~~ [Квази](quasi.md) ~~ [Параллакс](parallax.md) ~~ [Парсек](parsec.md) ~~ [Ускорение свободного падения](g.md)|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Atmosphere>
1. <https://en.wikipedia.org/wiki/Atmosphere_of_Earth>


## The End

end of file
