# Межконтинентальная баллистическая ракета
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [LV](lv.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Межконтинентальная баллистическая ракета (МБР)** — русскоязычный термин. **Intercontinental ballistic missile (ICBM)** — англоязычный эквивалент.</small>

**Межконтинентальная баллистическая ракета (МБР)** — управляемая баллистическая ракета класса 「земля‑земля」, дальностью не менее 5 500 ㎞.

Ракеты этого класса, как правило, оснащаются ядерными боевыми частями и предназначены для поражения стратегически важных объектов противника, расположенных на больших расстояниях и на удалённых континентах.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Launch vehicle (LV)](lv.md)】**<br> [Безракетный космический запуск](nrs.md) ~~ [Выводимая масса](throw_weight.md) ~~ [Космическая головная часть](lv.md) ~~ [㎆Р](icbm.md) ~~ [РКН](lv.md)<br>~ ~ ~ ~ ~<br> **Европа:**  [Arian](arian.md) ~~ [Vega](vega.md) / **Израиль:** [Shavit](shavit.md) / **Индия:** [GSLV](gslv.md) ~~ [PSLV](pslv.md) / **Китай:** [Long March](long_march.md) / **Корея с.:** [Unha](unha.md) / **Корея ю.:** *([Naro-1](kslv.md))* / **РФ, СНГ:** [Ангара](angara.md) ~~ [Протон](proton.md) ~~ [Союз](soyuz.md) ~~ [СТК](yenisei.md) ~~ [Зенит](zenit.md) *([Корона](korona.md) ~~ [Н-1](n_1.md) ~~ [Р-1](r_7.md) ~~ [Энергия](energia.md))* / **США:** [Antares](antares.md) ~~ [Atlas](atlas.md) ~~ [BFR](bfr.md) ~~ [Delta](delta.md) ~~ [Electron](electron.md) ~~ [Falcon](falcon.md) ~~ [Firefly Alpha](firefly_alpha.md) ~~ [LauncherOne](launcherone.md) ~~ [New Armstrong](new_armstrong.md) ~~ [New Glenn](new_glenn.md) ~~ [Minotaur](minotaur.md) ~~ [Pegasus](pegasus.md) ~~ [Shuttle](shuttle.md) ~~ [SLS](sls.md) ~~ [Vulcan](vulcan.md) *([Saturn](saturn_lv.md) ~~ [Sea Dragon](sea_dragon.md))* / **Япония:** [Epsilon](epsilon.md) ~~ [H2](h2.md) ~~ [H3](h3.md)|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Intercontinental_ballistic_missile>


## The End

end of file
