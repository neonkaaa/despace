# ABLab
> 2022.12.15 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/a/ablab_logo1t.webp)](f/c/a/ablab_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *Fukushima Building 7F, 1-5-3 Hashimurocho, Japan, Chuo-ku, Tokyo*<br> <https://ablab.space> ~~ [FB ⎆](https://www.facebook.com/ablabSS) ~~ [X ⎆](https://twitter.com/ABLab321)|
|:--|:--|
|**Business**|Community management that contributes to the development of the space‑related industry, business support for space‑related companies, consulting, space‑related information dissemination, etc.|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO, Representative Director — Masayuki Ito<br> ・Directors — Keiji Hagiwara, Hisami Katayama, Kazuki Tanada|

**ABLab** is a Japanese community of practice on the theme of space business. People who aspire to space will gather, make friends, learn from each other, grow, & work to grasp the future. Everyone’s goals are different, such as taking on new business challenges, career advancement, making friends, hobbies, & goals, but you should not wait for instructions. It is a place for people who can think & act for themselves.

**Advisors:**

- **Masahiro Ono.** He is a researcher at NASA’s Jet Propulsion Laboratory (JPL). After graduating from the Department of Aeronautics & Astronautics at the University of Tokyo in 2005, he began studying at the Massachusetts Institute of Technology (MIT) in September of the same year. In 2012, he completed a doctoral program in aerospace engineering & a master’s degree in the Technology Policy Program. From April 2012 to March 2013, he was an assistant professor at the Faculty of Science & Technology, Keio University. He assumed his current position in May 2013. His major books are 「Is There Life in the Universe?」 (SB Shinsho) & 「Crossing the Sea Aiming for Space」 (Toyo Keizai Inc.). In addition to developing artificial intelligence algorithms for the Mars 2020 rover & the Europa lander, we are conducting various research aimed at making future space probes autonomous. Hanshin fan. My favorite food is Takuan.

> **CEO’s Message (Masayuki Ito)**<br> Why created ABLab?<br> I’m Ito, representative of ABLab. I started studying space in 2018 at the age of 34. It all started because my son, who was in the 1st grade at the time, loved space. After that, I started meeting people who aspired to be in space, & there was one thing that impressed me. It is the height of their perspective. Many people who aspire to space are thinking about the world, thinking about humanity, & thinking about the future. I fell in love with them & I really enjoyed being with them. There were many challenges in the space industry in Japan, but it seemed more like an opportunity.<br> However, what can each of us do in the vast realm of space? 「Power」 is required. Organizations exist to make people stronger. The same is true of companies, where organizations are created to accomplish things that one person would never be able to do alone. That’s why I decided to create an organization for us to make us stronger, in order to create the future we envision. That is the community 「ABLab」.


## The End

end of file
