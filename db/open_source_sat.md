# Open Source Satellite
> 2022.03.04 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/o/open_source_sat_logo1t.webp)](f/c/o/open_source_sat_logo1.webp)|<info@opensourcesatellite.org>, <mark>nophone</mark>, Fax …;<br> *Building A2, Cody Technology Park, Old Ively Road, Farnborough, Hampshire, GU14 0LX, UK*<br> <https://opensourcesatellite.org> ~~ [LI ⎆](https://www.linkedin.com/company/open-source-satellite) ~~ [X ⎆](https://twitter.com/SatelliteOpen)|
|:--|:--|
|**Business**|R&D for small open source satellites|
|**Mission**|Development of a highly capable, flexible and affordable open source microsatellite platform|
|**Vision**|To make Space more accessible, and to promote the responsible and sustainable utilisation of Space|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Open Source Satellite** is a UK space company aimed to create open source small satellites.

Our mission is to develop a fully open source flexible small satellite platform, developed, built and operated using open source systems and technologies, to create:

- A performant, capable and modular microsatellite platform.
- A design that can be readily tailored for a given mission.
- A sustainable approach using readily available parts, processes and tools.
- A platform that can be upgraded and reconfigured after launch.
- A solution that can operate with multiple ground station networks.
- We are creating an ecosystem for sharing knowledge and ideas

<p style="page-break-after:always"> </p>

## The End

end of file
