# TRL
> 2019.04.27 [🚀](../../index/index.md) [despace](index.md) → **[OE](sc.md)**, [Качество](qm.md), [Риск](qm.md), [Control](control.md), [SE](se.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Technology readiness levels (TRL, TRLs)** — EN term. **Уровень готовности технологии (УГТ)** — RU analogue.</small>

**Technology readiness levels** *(TRL, TRLs)* are measures used to assess the maturity of evolving technologies (materials, components, software, processes, etc.) during their development & maybe during early operations.

Actually, when a new technology is 1st invented or conceptualized, it’s not suitable for immediate application. Instead, new technologies are usually subjected to experimentation, refinement, & increasingly realistic testing. Once the technology is sufficiently proven, it can be incorporated into a system. Created in NASA in the 1970s as a list of 7 points; extended in the 1980s to the 8 & 9 points by [John Mankins](person.md).



## TRL definitions

### ESA TRL chart

1. Instruments & spacecraft systems are classified according to a TRL on a scale of 1 ‑ 9.
2. Levels 1 ‑ 4 — creative & innovative technologies before/during the mission assessment [phase](rnd.md).
3. Levels 5 ‑ 9 — existing technologies & missions in the definition phase.

**TRL levels (ESA):**

1. Basic principles observed & reported
2. Technology concept and/or application formulated
3. Analytical & experimental critical function and/or characteristic proof‑of‑concept
4. Component and/or breadboard validation in lab environment
5. Component and/or breadboard validation in relevant environment
6. System model/prototype demonstration in a relevant environment (ground/space)
7. System prototype demonstration in a space environment
8. Actual system completed & **Flight Qualified** through test & demonstration (ground/space)
9. Actual system **Flight Proven** through successful mission operations



### NASA TRL chart

1. **Basic principles observed & reported.** Scientific research begins to be translated into applied [R&D](rnd.md).
2. **Technology concept and/or application formulated.** Once basic physical principles are observed, then practical applications of those characteristics can be invented/identified. At TRL 2, the application is speculative: there’s no experimental proof or detailed analysis to support the conjecture.
3. **Analytical & experimental critical function and/or characteristic proof of concept.** Active [R&D](rnd.md) is initiated. This must include both analytical studies to set the technology into an appropriate context & lab‑based studies to physically validate that the analytical predictions are correct. These studies & experiments should constitute 「proof‑of‑concept」 validation of the applications/concepts formulated at TRL 2.
4. **Component and/or breadboard validation in a lab environment.** Basic technological elements must be integrated to establish that the pieces’ll work together to achieve concept‑enabling levels of performance for a component/breadboard. This validation must be devised to support the concept formed earlier, & should be consistent with the requirements of potential system applications; it’s 「low‑fidelity」 compared to the eventual system: it could be composed of ad hoc discrete components in a lab.
5. **Component and/or breadboard validation in a relevant environment.** The fidelity of the component/breadboard being tested has to increase significantly. The basic tech elements must be integrated with reasonably realistic supporting elements so that the total applications (component/system‑level) can be tested in a 「simulated」 or realistic environment.
6. **System model/prototype demonstration in a relevant environment (ground/space).** A representative model/prototype system or system — which would go well beyond ad hoc, 「patch‑cord」 or discrete component level breadboarding — would be tested in a relevant environment (if the only one is the space, then it must be demonstrated in space).
7. **System prototype demonstration in a space environment.** TRL 7 requiring an actual system prototype demonstration in a space environment. The prototype should be near or at the scale of the planned operational system & the demonstration must be in space.
8. **Actual system completed & 「flight qualified」 through test & demonstration (ground/space).** In almost all cases, the end of true 「system development」 for most technology elements. This might include integration of new technology into an existing system.
9. **Actual system 「flight proven」 through successful mission operations.** In almost all cases, the end of last 「bug fixing」 aspects of true 「system development」. This might include the integration of new technology into an existing system. This TRL doesn’t include planned product improvement of ongoing or reusable systems.

Separated into hardware & software:

|**TRL**|**Hardware**|**Software**|**Exit criteria**|
|:--|:--|:--|:--|
|1|Scientific knowledge generated underpinning hardware technology concepts/applications.|Scientific knowledge generated underpinning basic properties of SW architecture & mathematical formulation.|Peer reviewed publication of research underlying the proposed concept/application.|
|2|Invention begins, practical application is identified but is speculative, no experimental proof or detailed analysis is available to support the conjecture.|Practical application is identified but is speculative, no experimental proof or detailed analysis is available to support the conjecture. Basic properties of algorithms, representations & concepts defined. Basic principles coded. Experiments performed with synthetic data.|Documented description of the application/concept that addresses feasibility & benefit.|
|3|Analytical studies place the technology in an appropriate context & laboratory demonstrations, modeling & simulation validate analytical prediction.|Development of limited functionality to validate critical properties & predictions using non‑integrated SW components.|Documented analytical/experimental results validating predictions of key parameters.|
|4|A low fidelity system/component breadboard is built & operated to demonstrate basic functionality & critical test environments, & associated performance predictions are defined relative to the final operating environment.|Key, functionally critical, SW components are integrated, & functionally validated, to establish interoperability & begin architecture development. Relevant Environments defined & performance in this environment predicted.|Documented test performance demonstrating agreement with analytical predictions. Documented definition of relevant environment.|
|5|A medium fidelity system/component brassboard is built & operated to demonstrate overall performance in a simulated operational environment with realistic support elements that demonstrates overall performance in critical areas. Performance predictions are made for subsequent development phases.|End‑to‑end SW elements implemented & interfaced with existing systems/simulations conforming to target environment. End‑to‑end SW system, tested in relevant environment, meeting predicted performance. Operational environment performance predicted. Prototype implementations developed.|Documented test performance demonstrating agreement with analytical predictions. Documented definition of scaling requirements.|
|6|A high fidelity system/component prototype that adequately addresses all critical scaling issues is built & operated in a relevant environment to demonstrate operations under critical environmental conditions.|Prototype implementations of the SW demonstrated on full‑scale realistic problems. Partially integrate with existing HW/SW systems. Limited documentation available. Engineering feasibility fully demonstrated.|Documented test performance demonstrating agreement with analytical predictions.|
|7|A high fidelity engineering unit that adequately addresses all critical scaling issues is built & operated in a relevant environment to demonstrate performance in the actual operational environment & platform (ground, airborne, or space).|Prototype SW exists having all key functionality available for demonstration & test. Well integrated with operational HW/SW systems demonstrating operational feasibility. Most SW bugs removed. Limited documentation available.|Documented test performance demonstrating agreement with analytical predictions.|
|8|The final product in its final configuration is successfully demonstrated through test & analysis for its intended operational environment & platform (ground, airborne, or space).|All SW has been thoroughly debugged & fully integrated with all operational HW & SW systems. All user documentation, training documentation, & maintenance documentation completed. All functionality successfully demonstrated in simulated operational scenarios. Verification & Validation (V&V) completed.|Documented test performance verifying analytical predictions.|
|9|The final product is successfully operated in an actual mission.|All SW has been thoroughly debugged & fully integrated with all operational HW/SW systems. All documentation has been completed. Sustaining SW engineering support is in place. System has been successfully operated in the operational environment.|Documented mission operational results.|



### Simplified

1. What if there were Unicorns
2. We`ve drawn a Unicorn
3. unicorn_v8_final_final.cad
4. We`ve placed a horn on a horse in our lab
5. We took the horse outside
6. We`re now calling the horse a Unicorn
7. We`re pretty sure the Unicorn might survive if we launch it into space
8. OMG it survived
9. Our reference design incorporates high‑heritage Space Unicorns

![](f/trl/trl_pic1.webp)



### Using of TRL
The primary purpose of using TRL is to help management in making decisions concerning the development & transitioning of technology. It should be viewed as one of several tools that are needed to manage the progress of research & development activity within an organization. Current TRL models tend to disregard negative & obsolescence factors. There have been suggestions made for incorporating such factors into assessments.

Among the advantages of TRLs:

1. provides a common understanding of technology status;
1. [risk management](qm.md);
1. to make decisions concerning technology [funding](fs.md);
1. to make decisions concerning transition of technology.

Some of the characteristics of TRL that limit their utility:

1. readiness does not necessarily fit with appropriateness or technology maturity;
1. a mature product may possess a greater or lesser degree of readiness for use in a particular system context than one of lower maturity;
1. numerous factors must be considered, including the relevance of the products’ operational environment to the system at hand, as well as the product‑system architectural mismatch.



## Other readiness levels

There is evidence that other Readiness Levels are used across NASA, but their usage tends to be Center- and/or Project-specific. There is no accepted or official Agency definitions and/or guidance for any of these other Readiness Levels. Examples of other Readiness Levels utilized within NASA include: Integration Readiness Level (IRL), MRL, SRL, Concept Maturity Level (CML), Operability Assessment Scale/Cooper-Harper, & others.

Due to limited resources, the TRA team did not develop recommendations on guidance or standards for other readiness levels.



### CML (Concept maturity levels)

> <small>**Уровни зрелости концепта (УЗК)** — русскоязычный термин. **Concept maturity levels (CML)** — англоязычный эквивалент.</small>

**Уровни зрелости концепта (УЗК)** — степень развития разрабатываемого концепта с целью её внедрения в конечный продукт. Предложена [JPL](jpl.md).

1. Docs:
   1. [Из блога на reqexperts ❐](f/control/cml_blog_001.pdf)
   1. [Материалы с reqexperts ❐](f/control/cml_concept_001.pdf)
1. <https://reqexperts.com/2016/05/15/concept-maturity-levels-an-introduction>

Created by [Mark Adler](person.md), [JPL](jpl.md). Inspired by [TRL](trl.md) scale. Covers the phase of mission concept formulation from 「cocktail napkin」 to NASA PDR (PDR = CML 8). Embraced by NASA PSD & now an integral part of JPL’s formulation approach. CMLs provide a standardized method to allow management to:

1. determine how much work (funding, resources, & effort) has been placed into the definition & maturation of a system concept;
2. compare competing project system concepts in terms of relevance to meeting the organization’s strategic goals, objectives, & ROI with acceptable risk;
3. determine which system concepts have had the same level of work & can be compared on the same terms;
4. understand the maturity of critical technologies needed to meet the project’s goals & objectives;
5. understand how much future work a system concept will require to get to a subsequent level of maturity;
6. have the information needed to determine when a proposed project’s system concept is mature enough to proceed to the next stage of system development.

|**CML**|**R&D**|**TRL**|
|:--|:--|:--|
|1. Cocktail Napkin, Idea Generation, Overview & Advocacy|0|**0**|
|2. Initial Feasibility|0|1. Basic principles observed & reported|
|3. [Trade Space](tradespace.md)|0|1|
|4. Point Design, Architecture selected within Trade Space|0|1|
|5. Baseline Concept|**A**|**2. Technology concept and/or application formulated**|
|6. Integrated Concept|**A**|**2**|
|7. Preliminary Implementation Baseline|B|**2**|
|8. Project Baseline|B|3. Analytical & experimental critical function &/or characteristic proof‑of‑concept|
|—|**C**|**4. Component &/or breadboard validation in laboratory environment**|
|—|D|5. Component &/or breadboard validation in relevant environment|
|—|D|**6. System model/prototype demo in a relevant environment (ground/space)**|
|—|**E,F**|7. System prototype demo in a space environment|
|—|F|**8. Actual system completed & Flight Qualified through test & demonstration (ground/space)**|
|—|F|9. Actual system Flight Proven through successful mission operations|

![](f/control/cml_pic01.webp)

[![](f/control/cml_pic03t.webp)](f/control/cml_pic01.webp)

![](f/control/cml_pic02.webp)

1. **Cocktail Napkin, Idea Generation, Overview & Advocacy.** Used to launch a new concept. In the overview, the problem (or opportunity) is defined, communicating why this project is worthwhile. The essence of what makes the concept unique & meaningful is clearly stated. The project vision & 「Need」 statement is defined showing the top level reason for developing the system & providing a focus for the team. The overview lays out a broad outline of the intention of the project, high level benefits, overall advantages, but provides limited details of cost, schedule, or other programmatic or design information. Think of the overview as the 「30 second」 [elevator speech](elevator_pitch.md) addressing what the problem being solved or opportunity being pursued, is & what the Need is — in other words, the desired end result at the end of the project once the system is operational.<br> From an advocacy viewpoint, information needed to sell the idea is documented. Key stakeholders are identified, the benefits, advantages, & ROI that result from pursuing this concept are defined for the stakeholders & for the organization. Needed capabilities are defined in terms of people, process, & products along with what needs to be done to provide these capabilities. A rudimentary 「back of the napkin」 sketch of the system concept is developed.<br> The advocacy perspective expands on the overview, going into more detail as to both what needs to be done to meet the Need & how management will know if the project is successful. Preliminary goals & objectives are defined including preliminary measures of effectiveness (MOEs) & Key Performance Parameters (KPPs) including preliminary key figures of merit concerning safety, reliability, sustainability, affordability, extensibility/evolvability, agility/robustness, effectiveness, & innovation.<br> Assumptions are documented & system level drivers & constraints are identified including cost, schedule, standards, regulations, technology, resource availability, interaction with existing systems, & higher level requirements allocated to the system of interest.
1. **Initial Feasibility.** The system concept is expanded & assessed on the basis of feasibility (cost, schedule, technology) from ROI, technical, & programmatic viewpoints. Preliminary system life‑cycle concepts are documented including concepts for development, procurement, system verification, system validation, manufacturing, transportation, deployment, operations, maintenance, upgrades, & disposal. Enabling systems required during development & operations are identified.<br> The system concept is expanded to address how the proposed system will fit within the macro system of which it is a part. A preliminary functional architecture of the macro system & system of interest is developed. How the systems that make up the macro system are related (connectivity, interaction, & flow of information) to each other & to the system of interest is defined. External interfaces are identified. High‑level schedule, resource, & cost estimates are developed. Key risks are identified.<br> Outside influences the system needs to take into account are addressed. Need, Goals, & Objectives (NGOs), MOEs, & KPPs are refined based on the information gained at this level.
1. **Trade Space.** The system functional architecture is transformed into an initial physical architecture. This physical architecture is used to access the options of the system to be developed meeting the NGOs within the defined drivers & constraints.<br> The study/design team will perform architecture trades between the system, subsystems, support systems, critical technologies, & enabling systems with elaboration to better understand the relationship between the parts of the architecture & impacts to ROI, cost, schedule, & risk.<br> The system (product, device, application, etc.) concept is demonstrated to be viable within the defined drivers & constraints, technology maturity, proposed macro architecture, & operating environment.<br> Objectives, MOEs, & KPPs are refined based on the information gained at this level. Measures of Performance (MOPs) are defined. Key risks are investigated, updated, & possible mitigation strategies outlined.<br> A key consideration at this stage is understanding the partials, that is how ROI changes as a function of some system parameter (e.g., mass, data volume, communication bandwidth, computing power, operating environment, technologies used, etc.). How will the ability of the system to meet the NGOs, MOEs, MOPs, & KPPs be affected as a function of changes in cost, schedule, technology, or other key parameter?: How will the cost , schedule, technology be effected by a change in an KPP?:<br> For complex systems where many of the parameters across the system architecture have a dependency (directly proportional or inversely proportional) it is very hard to track & understand the partials 「manually」. For complex systems, we recommend the study/design team use Systems Engineering (SE) software tools to develop a model of the system including the subsystems that make up the system as well as the macro environment in which the system will operate. With such a model, the team can make changes to the MOPs & KPPs as well as external constraint parameters to access the impact of the change to meeting the NGOs & other key system parameters. This knowledge will be of great benefit in choosing or optimizing the system architecture within the defined drivers & constraints.<br> Another key consideration when performing architectural trade studies is the [TRL](trl.md) of the candidate parts of the architecture. Trade studies will guide which technologies are needed to best meet the project’s objectives. Critical technologies needed to meet the goals & objectives, MOEs, & KPPs are identified. An initial Technology Readiness Assessment (TRA) is performed per the processes outlined in the GAO Technology Readiness Assessment Guide & the resulting TRA Report is generated. If the current capability to meet an MOP or KPP is beyond current state‑of‑the‑art for that part, then new parts/technology with the potential to provide the desired capability will have to be considered. Best practices & guidance from the Government Accountability Office (GAO) state that the system architecture should not rely on parts/technology with a TRL less than 3 at the time of scope baseline (CML 5) & a plan needs to be in place to mature the technology to at least TRL 6 by the time of the Preliminary Design Review (CML 8) & TRL 7 by the time of the Critical Design Review (CML 9).
1. **Point Design, Architecture selected within Trade Space.** Candidate system architectures are identified that will implement the system concept selected to meet the project NGOs & achieve the desired ROI providing the needed capabilities, functionality, performance, & quality within the specified drivers, constraints, & trade space. These architectures are defined to the level of major subsystems with acceptable cost, schedule, risk, & resource margins & reserves.<br> Subsystems’ trades are performed. Descope & backup options are defined. The objectives, MOEs, MOPs, KPPs, cost, schedule, & risks are further refined based on the information gained at this level.<br> Operational scenarios and/or use cases are defined (nominal, alternate nominal, & off‑nominal) & the system concept & candidate architectures are assessed based on their ability to successfully address each of the operational scenarios and/or use cases. Stakeholder roles & functions are defined & how various stakeholders (operators, maintenance & software update personnel, etc.) will interact with the system during operations are documented. The focus in on roles & functions — what the stakeholders do with & how they will use the system once it is operational. A 「day‑in‑the‑life」 of the system from the stakeholder’s viewpoint is addressed. How does data & information flow between architectural elements?: How are the capabilities of the overall system used to accomplish its intended purpose in the operational environment?:<br> Organizations may use 「rapid prototyping」 of potential physical solutions of a single component, subsystem, or system. With advances in 3D printing, study/design teams can develop prototypes quickly to be used as part of the architectural trade activities. When prototypes are developed, various configurations can be made available to the actual users. These prototypes can be functional, non‑functional, or just 「form & fit」. A prototype is useful to help the study/design team understand the user needs & expectations which will drive the system requirements. Prototypes are developed based on the currently known requirements. By using prototypes, the users can get an 「actual feel」 of the system in representative operational environments.<br> Also referred to as 「discovery learning」, user interactions with the prototypes can enable both the user & study/design team to better understand the requirements for the desired system. Rapid prototyping allows for user feedback much earlier in the development lifecycle concerning errors & problems, such as missing functions, substandard performance, safety issues, & confusing or difficult user interfaces. This is an interactive process that allows users to understand, modify, & eventually approve a working physical model of the system that meets their needs.<br> From the operational scenarios & user interactions with system prototypes, a draft set of system requirements are developed including core functional requirements & associated performance requirements, requirements to implement defined objectives & MOPs, requirements addressing defined drivers & constraints, & system level interface requirements.<br> The TRA & resultant TRA report are updated. Based on the results of the TRA a Technology Maturation Plan (TMP) is created per the processes outlined in the GAO Technology Readiness Assessment Guide to mature the critical technologies needed to meet the goals & objectives, MOEs, & KPPs.<br> Based on the knowledge gained to this point & the activities completed, the study/design team needs to determine whether or not they are ready to proceed with the activities associated with CML 5 or go back & repeat activities associated with CML 2 ‑ 4 to further refine the system concept.<br> **Note:** it is a myth that design work shouldn’t start until after the scope & requirements are baselined. A key outcome of the Scope Review (CML 5) is that there is at least one feasible concept that will result in the NGOs being achieved within the given drivers & constraints with acceptable risk. Lessons learned from key programs indicate that design work during the early concept maturation phases can identify important issues that are difficult to determine until a design concept is of sufficient maturity to evaluate a more complete set of development, test, operations, & evaluation considerations. Using the knowledge gained from prototypes & trade studies, a design concept should be developed, as early as possible in the project formulation phase, to allow a more complete understanding of programmatic implications & development challenges & risks. The design work at this stage enables validation of the draft requirements, providing analysis to determine where something is missing or perhaps not needed. This validation is a key part in accessing feasibility of the concept. Prototyping results in a system concept that is much more than just an artist rendering or a parametric model.
1. **Baseline Concept.** The implementation approach is defined including project management, systems engineering, contracting mode, integration, system verification & system validation approaches, cost & schedule. Relationships & dependencies, partnering, key risks, mitigation plans, & system 「buy (from an external source), build/code (make internally), reuse/modify (existing systems)」 or buy/try/decide approaches are defined. The TRA is completed verifying a minimum of TRL 3 for all parts of the architecture & a plan for advancing the TRLs to at least TRL 6 by PDR is complete, for systems going operational. (The purpose of some projects may be to advance the TRL. In this case advancing to TRL 6, may be the goal of the project.) A viable, feasible, & stable system concept is defined that will meet the NGOs, MOEs, MOPs, & KPPs within the defined drivers & constraints with acceptable risk.<br> The proposed system concept along with operational scenarios/and or use cases is documented in a system concept document or model. Project management & systems engineering documentation is developed to the maturity required by the organization for this lifecycle stage of the product development. The TMP is baselined. For a two‑step project funding process, this is the maturity that the system concept needs to be at for a Step 1 funding proposal as discussed earlier. By baselining scope, the project has demonstrated a system concept that is mature enough to proceed to the next lifecycle stage & document its requirements.<br> **Note:** In the above text I referred to a 「system concept document」 rather than an Operational Concept or Concept of Operations Document. For those moving away from a document‑centric approach & towards a data‑centric systems engineering approach, the system concept may be documented in a model developed using a systems engineering modeling tool. Using this approach, requirements & other systems engineering lifecycle artifacts can be linked to the model. With a data‑centric systems engineering approach, documents are generated from the data in the form of 「reports’, where the 「ground truth」 data is maintained & managed within the model. These documents can then be shared with other internal & external organizations.<br> Traditionally, projects focus on developing an operational concept or concept of operations document that is baselined as part of the scope review. Too often the focus is on writing this document. This is a mistake. The focus shouldn’t be on the document but on maturing your system concept to the point you can document a feasible concept that has reached CML 5 & is ready to be baselined. Developing & documenting a system concept is not an exercise in writing, rather it is an exercise in systems engineering!
1. **Integrated Concept.** Requirements are finalized & baselined. Design & planning commensurate for a SRR is complete. From the baselined system concept, stakeholder needs & expectations are transformed into 「design‑to」 requirements. In this context, requirements resulting from this transformation represent an agree‑to obligation of what is expected by the customer & other stakeholders & communicates clearly these expectations to the design/development team.<br> Using SE tools, requirements are traced to their source & allocated to the subsystem level, schedules for the subsystem development defined, & system verification & validation approach defined. Interfaces are identified & reflected in interface requirements documented in the set of requirements being baselined. Expanded details on the technical, management, cost, risks, & other elements of the system concept have been defined & documented.<br> Project management & systems engineering documentation is developed to the maturity required by the organization for this lifecycle stage of the product development. The system concept document or model is updated as necessary. The TRA, resultant TRA report, & TMP are updated. The requirement set is baselined.
1. **Preliminary Implementation Baseline.** Preliminary Implementation Baseline. Design & planning commensurate for a SDR is complete. System architecture trades have been completed & a design approach to meet the baselined system requirements has been defined & is ready for baseline. Preliminary subsystem‑level requirements & analyses, demonstrated (and acceptable) margins & reserves, prototyping & technology demonstrations, risk assessments & mitigation plans are completed. Preliminary integrated cost‑schedule‑design is baselined.<br> The system concept document or model is updated as necessary. The Preliminary Project Plan is ready for review. Other project management & systems engineering documentation is developed to the maturity required by the organization for this lifecycle stage of the product development. The TRA, resultant TRA report, & TMP are updated. For a two‑step project funding process, this is the maturity that your concept needs be at for a Step 2 project funding proposal as discussed earlier.<br> **Note:** Some organizations do not have a standalone SDR. Some may combine the SDR with the SRR or with the PDR. For those cases, criteria for determining whether or not your concept is at CML 7 would combined with criteria for either CML 6 or CML 8 depending on how your organization defines their gate reviews.
1. **Project Baseline.** Design & planning commensurate for a PDR is complete. System design documentation, 「build‑to」 requirements & drawings are 10 ‑ 20 % complete. Interfaces are defined in Interface Control Documents (ICDs). Final integrated cost‑schedule‑design is baselined. All parts/technologies have reached an TRL of at least 6. The system concept document or model is updated as necessary. The Project Plan is baselined including budget & schedule. Other project management & systems engineering documentation is developed to the maturity required by the organization for this lifecycle stage of the product development. The TRA, resultant TRA report, & TMP are updated.



## (RU) Уровень готовности технологий
**Уровень готовности технологий (УГТ)** — по ГОСТ 56861 — степень развития разрабатываемой технологии с целью её внедрения в конечный продукт. УГТ оценивают по шкале в зависимости от специфики продукта. Термин является адаптацией англоязычного [TRL](trl.md). Уровни УГТ соответствуют таковым в TRL.

|**УГТ**|**Описание по ГОСТ 58048**|
|:--|:--|
|1|Выявлены фундаментальные принципы. Сформулирована идея решения физической/технической проблемы, произведено её теоретическое/экспериментальное обоснование.|
|2|Сформулированы технологическая концепция и/или применение возможных концепций для перспективных объектов. Обоснованы необходимость и возможность создания новой технологии или технического решения, в которых используются физические эффекты и явления, подтвердившие уровень УГТ 1. Подтверждена обоснованность концепции/тех. решения, доказана эффективность использования идеи (технологии) в решении прикладных задач на базе предварительной проработки на уровне расчётных исследований и моделирования.|
|3|Даны аналитические и экспериментальные подтверждения по важнейшим функциональным возможностям и/или характеристикам выбранной концепции. Проведено расчётное и/или экспериментальное (лабораторное) обоснование эффективности технологий, продемонстрирована работоспособность концепции новой технологии в экспериментальной работе на мелкомасштабных моделях устройств. На УГТ 3 в проектах предусматривается отбор работ для дальнейшей разработки технологий.<br> Критерии отбора — демонстрация работы технологии на мелкомасштабных/расчётных моделях, учитывающих ключевые особенности разрабатываемой технологии, или эффективность использования комплекса новых технологий в решении прикладных задач на базе более детальной проработки концепции на уровне экспериментальных разработок по ключевым направлениям, детальных комплексных расчётных исследований и моделирования.|
|4|В лабораторных условиях: проверены компоненты и/или макеты, продемонстрированы работоспособность и совместимость технологий на подробных макетах.|
|5|Компоненты и/или макеты систем испытаны в условиях, близких к реальным. Основные технологические компоненты интегрированы с подходящими другими (「поддерживающими」) элементами, и технология испытана в моделируемых условиях. Достигнут уровень промежуточных/полных масштабов разрабатываемых систем, которые могут быть исследованы на стендовом оборудовании и в условиях, приближённых к условиям эксплуатации. Испытывают не прототипы, а только детализированные макеты разрабатываемых устройств.|
|6|Модель/прототип системы продемонстрированы в условиях, близких к реальным. Прототип системы содержит все детали разрабатываемых устройств. Доказаны реализуемость и эффективность технологий в условиях эксплуатации или близких к ним условиях и возможность интеграции технологии в компоновку разрабатываемой конструкции, для которой данная технология должна продемонстрировать работоспособность. Возможна полномасштабная разработка системы с реализацией требуемых свойств и уровня характеристик.|
|7|Прототип системы прошёл демонстрацию в эксплуатационных условиях. Прототип отражает планируемую штатную систему или близок к ней. На УГТ 7 решают вопрос о возможности применения технологии на объекте и целесообразности запуска в серийное производство.|
|8|Создана штатная система и освидетельствована (квалифицирована) посредством испытаний и демонстраций. Технология проверена на работоспособность в своей конечной форме и в ожидаемых условиях эксплуатации в составе технической системы (комплекса). Часто УГТ 8 соответствует окончанию разработки подлинной системы.|
|9|Продемонстрирована работа реальной системы в условиях реальной эксплуатации. Технология подготовлена к серийному производству.|



## Docs/Links
|**Sections & pages**|
|:--|
|**`Бортовая аппаратура (БА):`**<br> [PDD](pdd.md) ~~ [Антенна](antenna.md) ~~ [АПС](hns.md) ~~ [БУ](eas.md) ~~ [ЗУ](ds.md) ~~ [Изделие](unit.md) ~~ [КЛЧ](clean_lvl.md) ~~ [ПЗР](fov.md) ~~ [ПО](soft.md) ~~ [Прототип](prototype.md) ~~ [Радиосвязь](comms.md) ~~ [СКЭ](elmsys.md) ~~ [ССИТД](tsdcs.md) ~~ [СИТ](etedp.md) ~~ [УГТ](trl.md) ~~ [ЭКБ](elc.md) ~~ [EMC](emc.md)|
|**`Качество:`**<br> [Bus factor](bus_factor.md) ~~ [Way](faq.md) ~~ [АВПКО](fmeca.md) ~~ [Авторский надзор](des_spv.md) ~~ [Бережливое производство](lean_man.md) ~~ [Валидация, верификация](vnv.md) ~~ [Класс чистоты](clean_lvl.md) ~~ [Конструктивное совершенство](con_vel.md) ~~ [Крит. технологии](kt.md) ~~ [Крит. элементы](sens_elem.md) ~~ [Метрология](metrology.md) ~~ [Надёжность](qm.md) ~~ [Нештатная ситуация](emergency.md) ~~ [Номинал](nominal.md) ~~ [Ошибки](faq.md) ~~ [Система менеджмента качества](qms.md) ~~ [УГТ](trl.md)/[TRL](trl.md)|
|**【[Control](Control.md)】**<br> [Ad hoc](ad_hoc.md) ~~ [Business travel](business_travel.md) ~~ [Chief designers council](cocd.md) ~~ [CML](trl.md) ~~ [Competence](competence.md) ~~ [Confident](confident.md) ~~ [Consp.theory](consp_theory.md) ~~ [Control sys. (CS)](cs.md) ~~ [Coordinate system](coord_sys.md) ~~ [Curator](curator.md) ~~ [Designer’s supervision](des_spv.md) ~~ [E‑sig](esig.md) ~~ [Engineer](se.md) ~~ [Errand](errand.md) ~~ [Federal law](fed_law.md) ~~ [Federal TP](fed_tp.md) ~~ [Federal SP](fed_sp.md) ~~ [GNC](gnc.md) ~~ [Gravity assist](gravass.md) ~~ [Industrial archaeology](ind_arch.md) ~~ [Instruction](instruction.md) ~~ [Lean manuf.](lean_man.md) ~~ [Lifetime](lifetime.md) ~~ [Manager](manager.md) ~~ [MBSE](se.md) ~~ [Meeting](meeting.md) ~~ [MCC](sc.md) ~~ [MIC](mic.md) ~~ [MML](mml.md) ~~ [MoU](contract.md) ~~ [Nav. & ballistics (NB)](nnb.md) ~~ [Nonprofit org.](nonprof_org.md) ~~ [NX](nx.md) ~~ [Oberth effect](oberth_eff.md) ~~ [Org.structure](orgstruct.md) ~~ [Outcomes commission](outccom.md) ~~ [Patent](patent.md) ~~ [Peter prin.](peter_principle.md) ~~ [Plan](plan.md) ~~ [PMBok](pmbok.md) ~~ [Quorum](quorum.md) ~~ [R&D management](mgmt.md) ~~ [R&D support](rnd_support.md) ~~ [Recursion](recurs.md) ~~ [Schulze_method](schulze_method.md) ~~ [Sci'N'Tech activities](st_act.md) ~~ [Sci'N'Tech council](satc.md) ~~ [Single-window system](sw_sys.md) ~~ [Situ.leadership](situ_leadership.md) ~~ [Skunk works](se.md) ~~ [State arm. plan](plan_sa.md) ~~ [Swamp](swamp.md) ~~ [Teamcenter](teamcenter.md) ~~ [Tennis racket theorem](tr_theorem.md) ~~ [TRIZ](triz.md) ~~ [TRL](trl.md) ~~ [V‑model](v_model.md) ~~ [Veto](veto.md) ~~ [Workflow](workflow.md) ~~ [Workgroup](wg.md)|
|**【[Systems engineering](se.md)】**<br> [Competence](competence.md) ~~ [Coordinate system](coord_sys.md) ~~ [Designer’s supervision](des_spv.md) ~~ [Industrial archaeology](ind_arch.md) ~~ [Instruction](instruction.md) ~~ [Lean manuf.](lean_man.md) ~~ [Lifetime](lifetime.md) ~~ [MBSE](se.md) ~~ [MML](mml.md) ~~ [Nav. & ballistics (NB)](nnb.md) ~~ [NASA SEH](book_nasa_seh.md) ~~ [Oberth effect](oberth_eff.md) ~~ [PMBok](pmbok.md) ~~ [Quorum](quorum.md) ~~ [R&D management](mgmt.md) ~~ [R&D support](rnd_support.md) ~~ [Recursion](recurs.md) ~~ [Schulze_method](schulze_method.md) ~~ [Sci'N'Tech activities](st_act.md) ~~ [Sci'N'Tech council](satc.md) ~~ [Skunk works](se.md) ~~ [SysML](sysml.md) ~~ [Tennis racket theorem](tr_theorem.md) ~~ [TRIZ](triz.md) ~~ [TRL](trl.md) ~~ [V‑model](v_model.md) ~~ [Workflow](workflow.md) ~~ [Workgroup](wg.md)|

1. Docs: …
   1. УГТ:
      1. ГОСТ 56861 Система управления жизненным циклом. Разработка концепции изделия и технологий. Общие положения
      1. ГОСТ 58048 Трансфер технологий. Методические указания по оценке уровня зрелости технологий
2. TRL:
   1. <http://en.wikipedia.org/wiki/Technology_readiness_level>
   2. <https://www.canada.ca/en/department-national-defence/programs/defence-ideas/solution-readiness-level.html>
   3. <https://www.nasa.gov/wp-content/uploads/2017/12/458490main_trl_definitions.pdf>
3. УГТ:
   1. [Уровень зрелости концепта](trl.md)


## The End

end of file
