# Columbiad
> 2020.06.28 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/c/columbiad_logo1t.webp)](f/c/c/columbiad_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *…*<br> <https://www.columbiad.ca> ~~ [LI ⎆](https://www.linkedin.com/company/columbiad)|
|:--|:--|
|**Business**|[Launch services](lv.md)|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Columbiad Launch Services** is a commercial space launch company dedicated to providing low‑cost access to space. We are developing a high‑volume Industrial Sounding System based on gun propulsion technology.



## The End

end of file
