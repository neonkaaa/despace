# SIAA
> 2022.04.05 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/s/siaa_logo1t.webp)](f/c/s/siaa_logo1.webp)|<operations@spaceindustry.com.au>, <mark>nophone</mark>, Fax …;<br> *National Space Industry Hub, 4 Cornwallis Street, Eveleigh New South Wales 2015, Australia*<br> <https://www.spaceindustry.com.au> ~~ [LI ⎆](https://www.linkedin.com/company/spaceindustryaustralia) ~~ [X ⎆](https://twitter.com/spaceindustryoz)|
|:--|:--|
|**Business**|Promote the growth of the Australian space industry|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO — James Brown|

The **Space Industry Association of Australia (SIAA)** is the national peak body for the space industry in Australia. SIAA & its member companies work closely with Australian governments, international partners, academia, & industry to advance Australia’s space industry & economy.

The SIAA is run by a Chief Executive Officer, Mr James Brown. The CEO reports direct to a Board of seven Directors, who are elected by financial members of the SIAA for a term of 2 years, with half of the positions of the Board declared open annually.

<p style="page-break-after:always"> </p>

## The End

end of file
