# Zielona Univ.
> 2019.08.09 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/z/zielona_univ_logo1t.webp)](f/c/z/zielona_univ_logo1.webp)|<rektorat@uz.zgora.pl>, (+48 68) 3282202, 3282460, Fax (+48 68) 3270735;<br> *Uniwersytet Zielonogórski, ul. Licealna 9, 65-417 Zielona Góra, Poland*<br> <http://www.uz.zgora.pl/index.php> ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/University_of_Zielona_Góra)|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Зеленогурский университет (польск. Uniwersytet Zielonogórski, англ. University of Zielona Góra)** — главный государственный университет города Зелёна‑Гура, Польша. Основан 1 сентября 2001 года в результате объединения Зеленогурской Политехники (которая существовала с 1965 года) и Государственной высшей педагогической школы (с 1971 года). Имеет 10 факультетов. Университет является членом Ассоциации университетов Европы и евросоюзого проекта 「Socrates-Erasmus」.



## The End

end of file
