# SE4
> 2020.07.20 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/s/se4_logo1t.webp)](f/c/s/se4_logo1.webp)|<contact@se4.space>, <mark>nophone</mark>, Fax …;<br> *No.8 Kikuboshi Tower, 3 Chome-20-18 Asakusabashi, Taito City, Tokyo 111-0053, Japan*<br> <https://www.se4.space> ~~ [LI ⎆](https://www.linkedin.com/company/se4)|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|To combine AI & VR effectively to enable off‑world robot driven industry to increase the prosperity & long term survivability of humanity.|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**SE4 inc.** is a startup robotics company specializing in remote robotics through SEMANTIC CONTROL™ & ORCHESTRATED AUTONOMY™. SE4’s vision is to combine [AI & VR](soft.md) effectively, to enable contributions from off‑world robot‑driven industries to a prosperous long term survival strategy for humanity. VR, Computer Vision, Machine Learning, AI, Physics, Mechanical, & Electrical Engineering & much, much more. Founded 2018.11.01.



## The End

end of file
