# E-Space
> 2022.04.04 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/e/e_space_logo1t.webp)](f/c/e/e_space_logo1.webp)|<contact@e-space.com>, <mark>nophone</mark>, Fax …;<br> *…, Toulouse, Midi-Pyrenees, France*<br> <https://www.e-space.com> ~~ [LI ⎆](https://www.linkedin.com/company/e-space-group)|
|:--|:--|
|**Business**|Telecom sats|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・Chairman, Founder — Greg Wyler|

**E-Space** provides satellite constellation deployments with higher capabilities & lower cost. E-Space is democratizing space with a mesh network of secure multi‑application satellites that empowers businesses & governments to access the power of space to solve problems on Earth. Founded in 2021.

**E-Space SAS** is wholly French‑operated. **E-Space INC** is wholly American‑operated.

<p style="page-break-after:always"> </p>

## The End

end of file
