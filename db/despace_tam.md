# TAM Project
> 2025.01.18 [🚀](../../index/index.md) [despace](index.md) → [Project](project.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

## TAM — Tasks module

Aeverything is about processes and documents. Documents are inputs and outputs for processes. Well, in that sense, everyhing is a document. Just some documents contain processes (tasks) to perform.

The software parse (eventually and per demand) suitable files, looking for specific words and tags. Mark them with specific tags (see the IDs section), and add them to specific user's todo file with the "todo.txt" markup with additional TAM-related tags.

User can arrange tasks by the todo.txt syntax.

Words to search (case-sensitive):

1. TASK
2. TBD
3. TBC
4. TODO
5. Excluded: TODOIST

Every actiivity is a task. A task is a task. A requirement is a task. A goal is a task.

~ ~ ~ ~ ~

We utilize the todo.txt format.

We expand it with suitable tags in the tags section in the end of the 1st line. Comments are the HTML tags: \<!-- -->.

If the tags section contains a specific tag for multi-lined task, then there shall be a closing tag somewhere after that.

The software looks for specific words in any document (or message): TBD, TODO. And then adds tags and update the lists for people.


## The End

end of file.
