# Запоминающее устройство
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [OE](sc.md), **[ЗУ](ds.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Запоминающее устройство (ЗУ)** — русскоязычный термин. **Data storage** — англоязычный эквивалент.</small>

**Запоминающее устройство (ЗУ)** ─ устройство, предназначенное для записи и хранения данных. Устройство, реализующее компьютерную память.

**Технологии:**

1. **[5D optical data storage](ds.md)** (sometimes known as **Superman memory crystal**) is a nanostructured glass for permanently recording 5‑D digital data using femtosecond laser writing process. The memory crystal is capable of storing up to 360 TB worth of data for billions of years. The concept was experimentally demonstrated in 2013. As of 2018 the technology is in production use by the Arch Mission Foundation.
1. <mark>TBD</mark>

| |**[Фирма](contact.md)**|**Модели ЗУ (масса, ㎏)**|
|:--|:--|:--|
|**EU**|[AAC CS](aac_cs.md)|[Sirius TCM](ds_lst.md) (0.14)|
| |[ADS](ads.md)|[SSR](ds_lst.md) (8)|
|•|• • • • • • • • • •|~ ~ ~ ~ ~ |
|**RU**|[НИИ Гуськова](niimp.md)| |
| |[НПО ИТ](npoit.md)|[СЗИ-М](ds_lst.md) (11.8)|
| |[НПП Саит](sait_ltd.md)|[OBMU](ds_lst.md) (1.8)|
| |[ОКБ 5](okb5.md)| |

【**Table.** Manufacturers】

| | |
|:--|:--|
|**AE**|…|
|**AU**|…|
|**CA**|…|
|**CN**|…|
|**EU**|…|
|**IL**|…|
|**IN**|…|
|**JP**|…|
|**KR**|・[Satrec Initiative](satreci.md)|
|**RU**|…|
|**SA**|…|
|**SG**|…|
|**US**|・[DDC](ddc.md)|
|**VN**|…|


## 5D optical data storage
> <small>**5D optical data storage** — англоязычный термин, не имеющий аналога в русском языке. **5‑мерное хранилище информации** — дословный перевод с английского на русский.</small>

**5D optical data storage** (sometimes known as **Superman memory crystal**) is a nanostructured glass for permanently recording 5‑D digital data using femtosecond laser writing process. The memory crystal is capable of storing up to 360 TB worth of data for billions of years. The concept was experimentally demonstrated in 2013. As of 2018 the technology is in production use by the Arch Mission Foundation.

**Technical design**

The concept is the bulk storing of data optically in non‑photosensitive transparent materials such as fused quartz, which is renowned for its high chemical stability & resistance. Writing into it using a femtosecond‑laser was 1st proposed & demonstrated in 1996. The storage media consists of fused quartz where the spatial dimensions, intensity, polarization, & wavelength is used to modulate data. By introducing gold or silver nanoparticles embedded in the material, their plasmonic properties can be exploited.

Up to 18 layers have been tested using optimized parameters with a light pulse energy of 0.2 μJ, a duration of 600 fs & a repetition rate of 500 ㎑. Assuming 100 % efficient laser that is 1 W power consumption for (at most) 0.5 Mbit/s data rate. For a data rate of 100 ㎆/s that adds up to 1 600 W. Testing the durability using accelerated aging measurements shows that the decay time of the nanogratings is 3 × 10²⁰ years at room temperature (30 ℃). At an elevated temperature of 189 ℃ the extrapolated decay time is comparable to the age of the Universe (13.8 × 10⁹ years). By recording data with a numerical aperture objective of 1.4 NA & a wavelength of 250 ‑ 350 nm, a capacity of 360 TB can be achieved.

The format has a unique method of storing data, called 「5‑dimensional」 more for marketting purposes than because of any extra‑dimensional properties. The device has 3 physical dimensions & has no exotic higher dimensional properties. The fractal/holographic nature of its data storage is also purley 3‑dimensional. According to the University of Southampton:

> The 5‑dimensional discs [have] tiny patterns printed on 3 layers within the discs. Depending on the angle they are viewed from, these patterns can look completely different. This may sound like science fiction, but it’s basically a really fancy optical illusion. In this case, the 5 dimensions inside of the discs are the size & orientation in relation to the 3‑dimensional position of the nanostructures. The concept of being 5‑dimensional means that one disc has several different images depending on the angle that one views it from, & the magnification of the microscope used to view it. Basically, each disc has multiple layers of micro & macro level images.

It can be read with a combination of an optical microscope & a polarizer.

The technique was 1st demonstrated in 2010 by Kazuyuki Hirao’s laboratory at the Kyoto University. Further, the technology was developed by Peter Kazansky’s research group at the Optoelectronics Research Centre, University of Southampton.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**`Бортовая аппаратура (БА):`**<br> [PDD](pdd.md) ~~ [Антенна](antenna.md) ~~ [АПС](hns.md) ~~ [БУ](eas.md) ~~ [ЗУ](ds.md) ~~ [Изделие](unit.md) ~~ [КЛЧ](clean_lvl.md) ~~ [ПЗР](fov.md) ~~ [ПО](soft.md) ~~ [Прототип](prototype.md) ~~ [Радиосвязь](comms.md) ~~ [СКЭ](elmsys.md) ~~ [ССИТД](tsdcs.md) ~~ [СИТ](etedp.md) ~~ [УГТ](trl.md) ~~ [ЭКБ](elc.md) ~~ [EMC](emc.md)|
|**`Запоминающее устройство (ЗУ):`**<br> [5D optical data storage](ds.md) <br>~ ~ ~ ~ ~<br> **Европа:** [SSR](ds_lst.md) (8) ~~ [Sirius TCM](ds_lst.md) (0.14)  ▮  **РФ:** [СЗИ-М](ds_lst.md) (11.8) ~~ [OBMU](ds_lst.md) (1.8)|

1. Docs:
   1. [ЗУ, template](templates.md)
1. <https://en.wikipedia.org/wiki/Data_storage>
1. <https://en.wikipedia.org/wiki/5D_optical_data_storage>


## The End

end of file
