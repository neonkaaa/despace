# UNIBE
> 2019.08.05 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/u/unibe_logo1t.webp)](f/c/u/unibe_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *…*<br> <http://www.unibe.ch> ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/University_of_Bern)|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Бернский университет** (нем. **Universität Bern (UNIBE)**) — учебное заведение в столице Швейцарии Берне. Основан в 1834 году на базе Бернской Академии, которая, в свою очередь, была создана в 1805 году на базе богословской высшей школы (*Hohe Schule*, существует с 1500 года).

Прибор 「НГМС」 на ОКР [Luna‑27](luna_27.md).



## The End

end of file
