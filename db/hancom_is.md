# HANCOM inSPACE
> 2022.05.24 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/h/hancom_is_logo1t.webp)](f/c/h/hancom_is_logo1.webp)|<prime@inspace.re.kr>, +82-42-862-2735, Fax +82-42-862-2736;<br> *(34103) 3F, 20-62, Yuseong-daero 1312beon-gil, Yuseong-gu, Daejeon, South Korea*<br> <http://www.inspace.co.kr>|
|:--|:--|
|**Business**|Ground station, its software, sat observations, sat R&D|
|**Mission**|…|
|**Vision**|No.1 AI Security & Surveillance Reconnaissance Company|
|**Values**|…|
|**[MGMT](mgmt.md)**|・Founder — Myungjin Choi|

**HANCOM inSPACE** is a Korean aerospace tech company providing satellite ground station & its software (InStation), AI-based Satellite/Drone image analysis services (HANCOM Satellite/Drone), & fully automated unmanned drone operating system (DroneSAT). Founded in 2012.02.

**Why now**  
Space is no longer the sole domain of the governments.  
Innovative commercial companies like MAXAR & AIRBUS are combining machine learning, autonomous robotics, reusable rockets, & satellites of every size & advanced capability to put the best technology on Earth into orbit, opening new, large, growing markets.  
Most of all, a strong global market demand for imagery & geospatial analytics for defense, intelligence, & commercial.

**Why HANCOM inSPACE**  
As a combined team of over 50 aerospace engineers, geospatial analysts, weather & ocean experts, software developers, data scientists, & developers engineers, InSpace apply disruptive technologies & our unique intellectual property to both national security & commercial problems.  
Pervasive data-gathering sensors, open source software, cloud computing, machine learning, & big data analytics have provided the means to reveal critical insights at an unprecedented global scale.



## The End

end of file
