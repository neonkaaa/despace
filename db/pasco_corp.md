# PASCO Corporation
> 2020.07.18 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/p/pasco_corp_logo1t.webp)](f/c/p/pasco_corp_logo1.webp)|<mark>noemail</mark>, +81-3-6412-2978, Fax …;<br> *1-1-2 Higashiyama, Meguro-ku, Tokyo 153-0043, Japan*<br> <https://www.pasco.co.jp>|
|:--|:--|
|**Business**|Geospatial info|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO — Hideki Shimamura|

**PASCO CORPORATION** is a Japanese company which provides geospatial Information: collecting, processing, providing. Founded 1953.10.27.

Nowadays geospatial information is recognized as an important social infrastructure, as it serves as fundamental information for the national land & for corporate management.

PASCO CORPORATION is constantly in pursuit of the most advanced technologies in the areas of the acquisition & processing of geospatial information. Based on the results obtained through the active utilization of these technologies, the PASCO Group provides products & services that underpin secure & comfortable lives for the people around the world.



## The End

end of file
