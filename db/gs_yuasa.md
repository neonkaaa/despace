# GS Yuasa
> 2020.07.21 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/g/gs_yuasa_logo1t.webp)](f/c/g/gs_yuasa_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *〒601-8520 Kyōto-fu, Kyōto-shi, Minami-ku, Kisshōin Nishinoshō Inobabachō, Kyōto, 601 8520, Japan*<br> <https://www.gs-yuasa.com> ~~ [LI ⎆](https://www.linkedin.com/company/gs-yuasa) ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/GS_Yuasa)|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**GS Yuasa Corporation** (株式会社ジーエス・ユアサ コーポレーション, Kabushiki-gaisha GS Yuasa Kōporēshon) is a Japanese company that makes lead acid automobile & motorcycle batteries. It also develops & produces advanced battery technology for various aerospace & defense applications. Founded 2004.04.01.

- Automotive/Motorcycle Batteries & Chargers. GS YUASA offers the most extensive link of motorcycle batteries in the world.
- Traction Batteries & Chargers. Batteries & Chargers for forklift.
- Industrial Batteries, Power Supply Systems. Lead-acid Stationary Batteries, Alkaline Batteries & Lithium-ion Batteries for Industry.
- [Lithium-ion Batteries](eb.md). Lithium-ion Batteries for Automobiles, Industry & Aerospace.
- Lighting Equipment, Ultraviolet Systems. Street Lamps, Ultraviolet lamps, etc.



## The End

end of file
