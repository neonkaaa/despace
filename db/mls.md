# MLS
> 2019.08.14 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/m/mls_logo1t.webp)](f/c/m/mls_logo1.webp)|<admin@maritimelaunch.com>, <mark>nophone</mark>, Fax …;<br> *Suite 900, 1959 Upper Water Street, Halifax, Nova Scotia  B3J 3N2, Canada*<br> <https://www.maritimelaunch.com> ~~ [LI ⎆](https://www.linkedin.com/company/maritimelaunch) ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/Maritime_Launch_Services)|
|:--|:--|
|**Business**|[Launch services](lv.md) using Cyclone-4M LV in Nova Skotia|
|**Mission**|To develop an extremely safe, efficient, & cost-effective launch facility where we will help clients by meeting all their orbital access needs to LEO|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Maritime Launch Services (MLS)** is a Canadian space transport services company founded in 2016 & headquartered in Nova Scotia, Canada. MLS will rely on Ukrainian Cyclone-4M rockets by Yuzhnoye to launch polar & sun synchronous orbit from [Canso, Nova Scotia](spaceport.md). MLS is a joint venture of three U.S.‑based firms.

On 2017.03.14 MLS selected Canso, Nova Scotia as MLS’s launch site. MLS has applied to lease 15 hectares of land outside the town from the provincial Department of Natural Resources, & construction is slated to begin in 2018. The $110 million rocket spaceport will be used to launch commercial satellites into space as early as 2020 with a goal for up to eight launches annually by 2022. The site will include a 10 ‑ 15 metre‑tall control centre & rocket assembly facility, with a launch pad positioned 2.4 ㎞ away, linked by a custom rail system for rocket transportation. It will be the only operational spaceport in Canada, after the abandonment of the Churchill Rocket Research Range in the 1990s, & the 1st commercial spaceport for orbital launches in the country.

The proposed launch site is approximately 3.5 ㎞ south of Canso at 45.303559°N 60.982891°W, with the Vehicle Processing Facility located approximately 2 ㎞ south‑west of Canso at 45.317235°N 61.010000°W.

MLS hopes to launch eight rockets annually by 2022 with two southward launch options. *Option 1* is a Sun‑synchronous orbit launch between 600 ‑ 800 ㎞, for smaller satellites, with a payload up to 3 350 ㎏ for US$45 million. *Option 2* is a Low Earth Orbit launch, below 600 ㎞ in altitude, that will allow a payload up to 5 000 ㎏ also for US$45 million.

MLS relies on Ukrainian 2‑stage Cyclone-4M rockets by Yuzhnoye Design Office. It uses a Zenit‑derived 1st stage powered by four unspecified Ukrainian‑built Kerosene/LOX engines & upper stage stack developed for the original hypergolic Cyclone 4 rocket.



## The End

end of file
