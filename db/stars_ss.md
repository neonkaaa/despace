# STARS Space Service
> 2021.12.02 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/s/stars_ss_logo1t.webp)](f/c/s/stars_ss_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *1844 Ose-cho, Higashi-ku, Hamamatsu, Shizuoka, Japan*<br> <https://stars.co.jp> ~~ [IG ⎆](https://www.instagram.com/stars.space.service)|
|:--|:--|
|**Business**|Debris removal & recycling|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO, Founder — Makiko Yukishita<br> ・CTO — Masahiro Nohmi<br> ・PM — Koki Matsuo|

**STARS Space Service Co.** (or **SSS**) is a space recycling venture originating in Hamamatsu City, Shizuoka Prefecture, we’re aiming to become the 1st company in the world to recycle debris that has been left behind covering the Earth since mankind started space development. Founded 2019.12.25.

In 2019, we will launch STARS Space Service to lead the field of recycling technology for the debris that has been left behind. Masahiro Nohmi, who has already launched 5 units (as of 2021) in the R&D of space recycling technology since 2005, & Makiko Yukishita, who is promoting recycling business on the Earth as the representative director of Chubu Nippon Plastic, are working on a universal environmental service business for human life. While there’re many satellites for communication purposes, there’re few examples of robotic satellites that work in space, so the SSS aims to establish tethered robotic satellite technology & use that technology for space recycling.

**It may be a small step for humanity.** We started the development of the space elevator in 2005. Starting with a successful demonstration experiment in 2009, we have developed 5 sats & conducted experiments in space over the past 10 years while continuing our daily research. This fact means that the technology that makes it possible to safely capture, recover, & deliver space debris to the space station is literally on track. Our goal is not only to capture & retrieve debris & send it to the ISS, or to erase it, but also to repair/reuse debris that has lost its signal & is drifting in place, to return & retrieve it to Earth, & to transport it to the Moon & planets, thus eliminating the concept of debris as 「garbage」 floating in space. We’ve just taken the 1st small step.

**Between Research, Technology, & Realization. Step by step, steady progress.** Will the development of new technologies be enough to solve the problem of space waste? By combining know‑how & skills cultivated over the course of human history with new technologies SGDs on Earth (Waste Management), & develop methods that minimize risk & comply with international space law. The main feature is a 3D mobile elevator that combines multiple modules & moves autonomously through space by extending & bending wires & ropes to reach its destination. This minimizes the amount of fuel needed for the attitude control thrusters & also allows the ship to travel to the very edge of the atmosphere. In addition, the debris capture is performed by releasing a net from one of the multiple modules, which eliminates the need to grasp the shape of the debris itself & the precision of the work operation, which also helps to eliminate the debris from the debris removal satellite itself.

**Traveling the Universe With Experience.** This research phase of space waste recycling technology allow us to share the experience of being in outer space with you. Of course, it’s a small satellite, so it can’t carry everyone to space. However, the satellites’re equipped with high‑precision cameras, GPS, & other facilities that connect the ground & space, making it possible for people to experience space travel as a part of their daily lives, instead of an imaginary event or something that has been reserved for a select few. Real‑time information & communication technology is also built in, so we can see the Earth as it is right now, & we can communicate messages from Space to Earth & from Earth to Space. The word as you know. This is REUSE of the development of space debris recycling technology.

**Thinking about the Future together.** We’re also looking for team members to work with us on research & development. We’re looking for people who’re interested in space, people who’re interested in recycling activities, & people who can think about the future of space, the earth, & humanity together. We also cooperate with people who’re engaged in activities that lead to Kaizen of the space environment & the global environment. We also provide consulting services for satellite development & support for the preparation of application documents & other services. Contact us for anything related to space recycling & space business.

<p style="page-break-after:always"> </p>

**Our Team:**

- **雪下 真希子 Makiko Yukishita, Founder** — representative Director of Chubu Nippon Plastic Co. While studying art in the U.S., she returned to Japan to help with the business of her father's company due to facing a financial crisis. At the age of 30, she became the representative director of the company & increased its sales tenfold. In order to protect the global environment, we’re currently striving to achieve recycling not only on Earth, but also in space.
- **能見 公博 Dr. Masahiro Nohmi, CTO** — professor, Faculty of Engineering, Shizuoka University. He is leading the STARS project to study space elevators & space debris removal. While there’re many satellites for communication purposes, there’re few examples of robotic satellites that work in space, so the SSS aims to establish tethered robotic satellite technology & use that technology for space recycling.
- **松尾 講輝 Koki Matsuo, Project manager** — while working as a human resources manager at Chubu Nippon Plastics, he was involved in the founding of SSS & was in charge of the corporate department.In STARS-EC development, he is in charge of safety review handling & governmental procedures, & project management of satellite development.


## The End

end of file
