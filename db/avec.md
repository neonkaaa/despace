# AVEC
> 2020.01.23 [🚀](../../index/index.md) [despace](index.md) → [JHU APL](jhuapl.md), [Venus](venus.md), **[Test](test.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**APL’s Venus Environment Chamber (AVEC)** — англоязычный термин, не имеющий аналога в русском языке. **Камера условий на Венере Лаборатории прикладной физики (АВЕК)** — дословный перевод с английского на русский.</small>

**APL’s Venus Environment Chamber (AVEC)** — камера, имитирующая условия атмосферы [Венеры](venus.md). Расположена в [JHU APL](jhuapl.md).

|**AVEC**|
|:--|
|[![](f/tests/avec_pic01t.webp)](f/tests/avec_pic01.webp)|



## Описание
| |**AVEC**|
|:--|:--|
|**Газовых потоков**| |
|**Давление**|10 ㎫ (98.6 атм) номинал,<br> 27+ ㎫ (267+ атм) макс.|
|**Масса**| |
|**Объём**|0.7 л (0.0007 m³, 60 × 230 ㎜)|
|**Температура**|470 ℃ номинал,<br> 500+ ℃ макс.|

1. Возможность подавать внутрь заданную газовую смесь.
1. Доступна для интернационального использования. Контакт в JHU APL — [Noam Izenberg](person.md).
1. Длительность испытания не более недели.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Test](test.md)】**<br> [JTAG](jtag.md) ~~ [Proto fligt model](pfm.md) ~~ [Безэховая камера](ach.md) ~~ [Валидация](vnv.md) ~~ [Класс чистоты](clean_lvl.md) ~~ [КПЭО](ctpr.md) ~~ [Перечень методик испытаний](list_tp.md) ~~ [Программа и методика испытаний](pmot.md) ~~ [Опытный образец](pilot_sample.md) ~~ [Циклограмма](obc.md) ~~ [Штатный образец](flight_unit.md) ~~ [ЭО](test.md) ~~ [Экспериментально‑теоретический метод](etetm.md)|

1. Docs:
   1. [Презентация с LPSC2019 ❐](f/tests/avec_doc01.pdf)
1. <…>


## The End

end of file
