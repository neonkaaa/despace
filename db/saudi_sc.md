# Saudi Space Commission
> 2020.03.20 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/s/saudi_sc_logo1t.webp)](f/c/s/saudi_sc_logo1.webp)|<info@ssc.gov.sa>, +966-11-2443999, Fax +966-11-2443888;<br> *8461 Ushba – Al Shohda Dist., Unit Number: 1, RIYADH 13241 – 3512*<br> <https://saudispace.gov.sa> ~~ [LI ⎆](https://www.linkedin.com/company/saudi-space-commission) ~~ [X ⎆](https://twitter.com/saudispace) ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/Saudi_Space_Commission)|
|:--|:--|
|**Business**|Saudi Space Agency|
|**Mission**|To lead the space sector to realize the Saudi Vision 2030 by developing and organizing the sector and providing enablers to realize ground-breaking achievements|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO — Mohammed Saud ALTamaimi|

**Saudi Space Commission (SSC)** (Arabic: الهيئة السعودية للفضاء) is an Saudi independent government entity established by a royal order on 2018.12.27. The commission is chaired by HRH Prince Sultan bin Salman, who flew aboard the U.S. Space Shuttle Discovery in the 1980s as the 1st Arab in space.



## The End

end of file
