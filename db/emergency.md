# Нештатная ситуация
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [Качество](qm.md), [Риск](qm.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Нештатная ситуация (НШС)** — русскоязычный термин. **Emergency** — примерный англоязычный эквивалент.</small>

**Нештатная ситуация (НШС)** — сочетание условий и обстоятельств при эксплуатации технических систем, отличающихся от предусмотренных проектами, нормами и регламентами и ведущих к возникновению опасных состояний в технических системах.



## Описание



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**`Качество:`**<br> [Bus factor](bus_factor.md) ~~ [Way](faq.md) ~~ [АВПКО](fmeca.md) ~~ [Авторский надзор](des_spv.md) ~~ [Бережливое производство](lean_man.md) ~~ [Валидация, верификация](vnv.md) ~~ [Класс чистоты](clean_lvl.md) ~~ [Конструктивное совершенство](con_vel.md) ~~ [Крит. технологии](kt.md) ~~ [Крит. элементы](sens_elem.md) ~~ [Метрология](metrology.md) ~~ [Надёжность](qm.md) ~~ [Нештатная ситуация](emergency.md) ~~ [Номинал](nominal.md) ~~ [Ошибки](faq.md) ~~ [Система менеджмента качества](qms.md) ~~ [УГТ](trl.md)/[TRL](trl.md)|

1. Docs: …
1. <…>


## The End

end of file
