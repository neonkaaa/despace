# SAGA Space Architects
> 2022.01.28 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/s/saga_sa_logo1t.webp)](f/c/s/saga_logo1.webp)|<studio@saga.dk>, <mark>nophone</mark>, Fax …;<br> *SAGA Studio ApS, Bådehavnsgade 38 port 1, 2450 Copenhagen, Denmark*<br> <https://saga.dk> ~~ [FB ⎆](https://www.facebook.com/sagaspacearchitects) ~~ [IG ⎆](https://instagram.com/saga_space_architects) ~~ [LI ⎆](https://www.linkedin.com/company/saga-space-architects)|
|:--|:--|
|**Business**|Space architectures|
|**Mission**|…|
|**Vision**|Space habitation & extra-terrestrial living is vital for human development. If we stop exploring we stagnate. We approach exploration from a human perspective, where mental well-being & social sustainability is part of the life-support equation.|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CTO — Simon D.H. Kristensen|

**SAGA Space Architects** is a Denmark company aimed for design architecture for outer space with a focus on wellbeing inspired by the natural environment & human needs from an evolutionary point of view. Also we make technology driven architecture, here for planet Earth, using state of the art tools & methods learned from working with space.

<p style="page-break-after:always"> </p>

## The End

end of file
