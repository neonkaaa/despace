# ГОСТ ИСО 9001
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [Качество](qm.md), **[НД](doc.md#НД)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

**ГОСТ Р ИСО 9001‑2015 「Системы менеджмента качества. Требования.」**

Устанавливает требования к [системе менеджмента качества](qms.md), которые могут использоваться для внутреннего применения организациями, в целях сертификации или заключения контрактов. Он направлен на результативность системы менеджмента качества при выполнении требований потребителей.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[](.md)】**<br> <mark>NOCAT</mark>|

1. Docs:
   1. [ГОСТ Р ИСО 9001-2015 ❐](f/doc/gost_iso_9001.pdf)
1. <https://ru.wikipedia.org/wiki/ГОСТ_Р_ИСО_9001>


## The End

end of file
