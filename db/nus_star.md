# STAR@NUS
> 2022.07.06 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/n/nus_star_logo1t.webp)](f/c/n/nus_star_logo1.webp)|<star@nus.edu.sg>, <mark>nophone</mark>, Fax …;<br> *Satellite Technology And Research Centre, Level 3, 5 Sports Drive, Singapore 117292*<br> <https://online.ece.nus.edu.sg/star> ~~ [FB ⎆](http://www.facebook.com/nus.singapore) ~~ [LI ⎆](https://www.linkedin.com/company/star-nus) ~~ [X ⎆](http://twitter.com/NUSingapore)|
|:--|:--|
|**Business**|Small sat dev|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

The **Satellite Technology And Research Centre (STAR)** is set up in National University of Singapore to be a world‑class centre for advanced distributed satellite systems using multiple satellites flying in formation, swarm or constellation.

<p style="page-break-after:always"> </p>

## The End

end of file
