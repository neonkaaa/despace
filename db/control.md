# Control
> 2019.04.06 [🚀](../../index/index.md) [despace](index.md) → [Control](control.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

**Управление:**

1. **Управление** — структурное подразделение в организации (например, 「Управление сельского хозяйства Минобороны России」). **Administration** — англоязычный эквивалент.
1. **Управление** (в [организации](contact.md)) — синоним понятия менеджмент. **Management** — англоязычный эквивалент.
1. **Управление** — это процесс прогнозирования, планирования, организации, мотивации, координации и контроля, необходимый для того, чтобы сформулировать и достичь цели организации. **Control** — англоязычный эквивалент.

Некоторые факты об управлении:

1. Мы, которые не хуже тебя, клянёмся тебе, который не лучше нас, принять тебя нашим королём и повелителем, если ты будешь блюсти наши вольности; а если нет, то нет. <small>*(присяга кортесов Арагонскому королю)*</small>
1. Каждая уступка с твоей стороны должна быть 「оплачена」 партнёром или клиентом.
1. Исполнитель должен исполнять задачу только если согласился на неё.
1. Исполнитель задачи должен быть наделён:
   1. **власть** — возможность убеждаться, что будет результат;
      1. 1‑я часть власти — право наказывать и поощрять за результаты,
      1. 2‑я — возможность убеждать без задействования других рычагов, т.е. авторитет;
   1. **[компетентность](competence.md)** — знания для принятия решения; этим можно наделить, но лучше, чтобы это было сразу;
   1. **ответственность** — когда человек полностью отвечает за результат своего решения;
   1. **полномочия** — когда человек имеет право принять решение;
   1. **ресурсы**.

Для создания космической техники используется [системная инженерия](se.md) и специалисты. Чем обычно стараются управлять в НИОКР:

1. ожиданиями
1. [рисками](qm.md)



## Knowledge management

Why share knowledge?

With the growth of the volume of documents, experience & equipment, the threshold for entry, which must be overcome by beginners in order to fully immerse in the project, also grows. Over time, something is forgotten, & people may no longer remember how some of the mechanisms work in an existing system. In the natural processes of staff turnover, it also happens that experts leave the company, & newcomers have not yet had time to acquire critical knowledge. In this case, the old team will 「die」 without sharing knowledge: newcomers will not be able to understand the legacy & will not know why this or that decision was made, & this will lead to alterations and/or refusals. To prevent this, it’s necessary to organize the exchange of knowledge at the level of the company, teams & individuals & constantly maintain this culture. Below are the methods of exchange & a subjective assessment of their effectiveness & complexity of implementation.

**Text ways:**

1. **Chats on a narrow topic.** The knowledge base for a process/project can be a chat. The info is less structured, but, as a rule, you can always look at the list of already sent photos, links, files, & based on them, you can deeper understand the topic or find an expert. The main thing is not to allow conversations outside the subject of the chat. To avoid losing important information, use pinned messages.
1. **Creation of wiki‑pages.** You can create a knowledge base in the corporate Wiki. The disadvantage is associated with maintaining the relevance of the information. Typically, the most useful info about a project is UML diagrams & the most basic concepts.
1. **Databases on incidents.** The results of the analysis of incidents can be systematized on the Wiki & used to teach newcomers from the experience already gained, & it will be useful for senior employees to audit this knowledge to identify basic information & take systemic measures.
1. **List of resources & collections.** The method allows you to find the most interesting topics for study, as well as understand with whom these topics can be discussed. It’s advisable to write a mini‑review or a couple of words for each item in the selection, this will help you better understand the new info.
1. **Writing articles.** As a rule, the search for material & research when writing an article give a strong impetus to the development of the author’s horizons, & the experience gained can be useful & relevant for colleagues. For example, a topic for an article may be some kind of 「pain」 in the project or a description of the experience gained. So that the writing process does not drag on indefinitely, it’s better to come up with some kind of deadlines or create them artificially. If articles are published on external sites & strengthen the company’s HR brand, then it’s worth considering options for encouraging authors to maintain motivation.

**Live communication.** You can’t force the exchange of knowledge, but you can create the conditions for people to want to share experiences. Live communication also protects against burnout & promotes team building.

1. **Architectural committees.** To make complex & long‑term engineering decisions, so‑called 「architectural committees」 can be assembled. At these meetings, standards are developed that will be followed by the entire company (as a rule, these standards will be mandatory only for new projects, & old ones are brought into line with accepted standards as necessary), or the architecture of new services is discussed. It’s recommended to allow anyone who wishes to speak at the architectural committee, while the right to vote should be left only to experts in their field (games of democracy do not work when making important technical decisions).
1. **Conducting a vote on topics of interest.** To understand what questions are asked by employees interested in their development, you can create a vote with a list of topics, or propose to name the topics yourself. It’s possible there’re no proper mentors within the company, then it makes sense to find an outside expert & gain knowledge.
1. **Creation of communities & guilds for different technologies.** Such communities are highly rely on leaders who are ready to develop an area of ​​interest to them on their own enthusiasm, and, as a rule, they need additional support from the management at the start.
1. **Joint research.** If a problem cannot be solved by one person, then joint research can help. In addition to pooling knowledge & increasing the chances of finding the best solution, this process is valuable through an intensive exchange of experience.
1. **Participation in the analysis of incidents.** In case of major incidents, the expertise of 1 or 2 people may not be enough to resolve them. In such cases, it makes sense to use all available resources for the parsing & the fastest possible solution. During such analyzes, it will be useful for beginners to write down incomprehensible moments & study them in a quiet time. Perhaps the knowledge gained will help with the next incident or will help to prevent it. For experts, parsing also has a certain benefit: it’s the accumulation of experience in applying knowledge in stressful situations. In addition, the rule works: if today you help in a critical situation, then tomorrow they will help you.
1. **Platforms for discussion of books read.** The best way to dig deeper into the material you have read is to discuss it with like‑minded people. Regular meetings are great for this, & topics can be moderated in advance.
1. **Platforms for training reports.** Even experienced speakers need to give a presentation several times & collect feedback to improve the material, & for the listeners this can provide new knowledge & push to presentations. The motivation for presentations on external sites can be the possibility of creating & developing a personal brand.
1. **Prototyping, MVP, feature team participation.** One way to quickly prototype a new process in large companies is to unite free developers from several teams into a so‑called 「feature team」. As a rule, the MVP of a product is created in a short time 「in a startup mode」, & this is a good test of the strength of the skills previously acquired by the developer. The exchange of knowledge & experience goes on as intensively as possible, & skills are quickly transformed into working documents or models. Upon reaching the result, the feature team is disbanded (the project is given to the support & development of one of the existing teams), but pleasant memories & established connections remain as a bonus. It’s often impossible to use this method of writing new services, because the increased workload leads to employee burnout.
1. **Service duties, releases & bug analysis.** In large teams, the roles of 「release engineer」, 「bug attendant」 & 「off‑hours support engineer」 can be distinguished. Since for these roles it’s necessary to know well all the services supported by the team, the developer quite quickly realizes what knowledge he lacks & takes the missing pieces of information from the experts.
1. **Training within the company (internal courses).** If there are many newcomers in the company, or a new technology needs to be massively installed, then the easiest way to get the missing knowledge is to centrally organize training. An employee of the company can act as a teacher, then the students will receive a lot of valuable information specific to their daily work.
1. **Writing articles in co‑authorship with another employee.** Finding a co‑author can be tricky, but the results from teamwork can exceed expectations. The impetus for writing an article may be the same desire to understand some complex topic.

**Ways of obtaining knowledge outside of companies:**

1. **Hackathons.** The method is somewhat similar to participation in a 「feature team」. The advantage of the hackathon is its limited time & focus on a specific problem. Hackathons can be a good shake‑up for bored employees, helping them not to burn out in our difficult profession. preferable to choose those projects that you yourself regularly use, or that are in demand by the employer.
1. **Passage of interviews** allows you to find out which topics are in demand for the industry, the employee doesn’t know, & to study them in a calm atmosphere. But not every manager can recommend this method, only if he is sure that the company is competitive in the market & a possible offer will be outbid by a more profitable offer.
1. **Working in open source.** When working on Open Source, an engineer improves his skills by creating new or improving existing matter. Feedback from experienced specialists on pull requests can provide an incentive to gain new knowledge & read professional literature. For development, it’s

**A table.** Ways of exchange & accumulation of knowledge [⁽¹⁾](https://habr.com/ru/company/domclick/blog/578822)

| |**Effec-<br>tive-<br>ness (E)**|**Complexity of<br> implemen-<br>tation (C)**|**Total<br> score<br> (E×(10−C))**|
|:--|:--|:--|:--|
|Creation of communities & guilds for different technologies|7|10|0|
|Training within the company (internal courses)|8|9|8|
|Platforms for training reports|3|7|9|
|Writing articles in co‑authorship with another employee|8|8|16|
|Passage of interviews|8|8|16|
|Writing articles|6|7|18|
|Hackathons|9|8|18|
|Prototyping, MVP, feature team participation|10|8|20|
|Platforms for discussion of books read|5|6|20|
|Working in open source|10|8|20|
|Databases on incidents|7|7|21|
|Participation in the analysis of incidents|7|7|21|
|Service duties, releases & bug analysis|8|7|24|
|Chats on a narrow topic|3|2|24|
|Joint research|8|6|32|
|Conducting a vote on topics of interest|5|3|35|
|Creation of wiki‑pages|5|3|35|
|Architectural committees|9|6|36|
|List of resources & collections|6|4|36|



## Система управления
> <small>**Система управления (СУ)** — русскоязычный термин. **Сontrol system** — англоязычный эквивалент.</small>

**Систе́ма управле́ния (СУ)** — систематизированный (строго определённый) набор средств сбора сведений о подконтрольном объекте и средств воздействия на его поведение, предназначенный для достижения определённых целей. Объектом системы управления могут быть как технические объекты, так и люди. Объект системы управления может состоять из других объектов, которые могут иметь постоянную структуру взаимосвязей.


## Термины
| | |
|:--|:--|
|**Наука**|область человеческой деятельности, направленная на выработку и систематизацию объективных знаний о действительности. Основой этой деятельности является сбор фактов, их постоянное обновление и систематизация, критический анализ и, на этой основе, синтез новых знаний или обобщений, которые не только описывают наблюдаемые природные или общественные явления, но и позволяют построить причинно‑следственные связи с конечной целью прогнозирования. Те теории и гипотезы, которые подтверждаются фактами или опытами, формулируются в виде законов природы или общества. Наука в широком смысле включает в себя все условия и компоненты соответствующей деятельности: ➀ разделение и кооперацию научного труда; ➁ научные учреждения, экспериментальное и лабораторное оборудование; ➂ методы научно‑исследовательской работы; ➃ понятийный и категориальный аппарат; ➄ систему научной информации; ➅ всю сумму накопленных ранее научных знаний.|
|**Теория**|(греч. θεωρία — рассмотрение, исследование) — учение, система идей или принципов. Является совокупностью обобщённых положений, образующих науку или её раздел. Теория выступает как форма синтетического знания, в границах которой отдельные понятия, гипотезы и законы теряют прежнюю автономность и становятся элементами целостной системы. В теории одни суждения выводятся из других суждений на основе некоторых правил логического вывода. Теории формулируются, разрабатываются и проверяются в соответствии с научным методом. Способность прогнозировать — важное следствие теоретического построения.|
|**Концепция**|(от лат. conceptio — понимание, система) — определённый способ понимания, трактовки каких‑либо явлений, основная точка зрения, руководящая идея для их освещения; система взглядов на явления в мире, в природе, в обществе; ведущий замысел, конструктивный принцип в научной, художественной, технической, политической и других видах деятельности; комплекс взглядов, связанных между собой и вытекающих один из другого, система путей решения выбранной задачи;способ понимания, различения и трактовки каких‑либо явлений, порождающие присущие только для данного способа соображения и выводы. Концепция определяет стратегию действий. Различным концепциям соответствует свой терминологический аппарат.|
|**Концепции системной инженерии**|общие абстрактные представления, связанные с пониманием предмета системной инженерии, которые направляют мышление системного инженера.|
|**Подход**|комплекс структур и механизмов в познании и/или практике, характеризующий конкурирующие между собой (или исторически сменяющие друг друга) стратегии и программы в философии, науке, политике или в организации жизни и деятельности людей. Подход определяет приёмы работы (в том числе приёмы мышления), которые могут быть разработаны в рамках одной дисциплины и перенесены в какие‑то другие области. Очень часто 「подход」 является синонимом 「практики」 или даже 「метода」. Слово 「подход」 означает обычно, что какие‑то 「практики」 или даже целый 「метод」 работы были отработаны в какой‑то одной предметной области, отрефлектированы (т.е. явно описаны в отрыве от предметной области, на объектах которой они отрабатывались), а затем перенесены туда, где раньше они не использовались.|
|**Практика**|(practice) — основа описания деятельности, без привязки деятельности ко времени и без привязки деятельностей друг к другу. Практика это описание того, как справиться с каким‑то отдельным аспектом (но не всеми!) дисциплин инженерного проекта.<br> ・**Практика** = Дисциплины + Технология.|
|**Метод**|это сочетание дисциплины и набора практик для достижения какой‑то цели.<br> **Метод** — композиция практик, формирующее (на желаемом уровне абстракции) описание того, как выполняется деятельность (endevour). Метод команды описывает способ работы команды, помогает и направляет команду при выполнении ее задач. Ведение деятельности по разработке заключается в использовании экземпляра метода. Этот экземпляр содержит экземпляры альф, рабочих продуктов, действий и т.п., которые являются результатом реальной работы, выполняемой в процессе разработки. Используемый экземпляр метода включает ссылку на определенный экземпляр метода, который выбран в качестве метода, которому необходимо следовать (OMG Essence).<br> ・**Метод** существенно отличается от 「процесса」: метод включает в себя всё нужное для выполнения работы: ➀ продукты (артефакты), ➁ процессы (жизненные циклы и используемые в них практики), ➂ организацию (люди и инструменты), ➃ используемые в работе языки и нотации.|
|**Методология**|система практик, методов, процедур и правил, используемых в определенной дисциплине (PMI PMBoK).<br> ・**Методология** — совокупрость правил и инструкций (представленных в текстовой форме в виде компьютерных программ, специальных инструментов и т.д.), которые предназначены для оказания последовательной, поэтапной, пошаговой помощи пользователю (ISO 15704).|
|**Дисциплина**|самостоятельная отрасль науки либо знаний, преподаваемых в учебном заведении.<br> ・**Истинная дисциплина** имеет свои собственные концепции (например, 「требования」, 「системная архитектура」), и поэтому 「истинные части」 (типа 「инженерии требований」 и 「инженерии системной архитектуры」). Разбиение истинных дисциплин — это отношение состава (часть‑целое, композиция).<br> ・**Дисциплина** — набор альф, уровень учебника (「дисциплина грузится в голову」).|
|**Технология**|поддержанный необходимыми рабочими продуктами и инструментами способ работы (way of working).|



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Control](Control.md)】**<br> [Ad hoc](ad_hoc.md) ~~ [Business travel](business_travel.md) ~~ [Chief designers council](cocd.md) ~~ [CML](trl.md) ~~ [Competence](competence.md) ~~ [Confident](confident.md) ~~ [Consp.theory](consp_theory.md) ~~ [Control sys. (CS)](cs.md) ~~ [Coordinate system](coord_sys.md) ~~ [Curator](curator.md) ~~ [Designer’s supervision](des_spv.md) ~~ [E‑sig](esig.md) ~~ [Engineer](se.md) ~~ [Errand](errand.md) ~~ [Federal law](fed_law.md) ~~ [Federal TP](fed_tp.md) ~~ [Federal SP](fed_sp.md) ~~ [GNC](gnc.md) ~~ [Gravity assist](gravass.md) ~~ [Industrial archaeology](ind_arch.md) ~~ [Instruction](instruction.md) ~~ [Lean manuf.](lean_man.md) ~~ [Lifetime](lifetime.md) ~~ [Manager](manager.md) ~~ [MBSE](se.md) ~~ [Meeting](meeting.md) ~~ [MCC](sc.md) ~~ [MIC](mic.md) ~~ [MML](mml.md) ~~ [MoU](contract.md) ~~ [Nav. & ballistics (NB)](nnb.md) ~~ [Nonprofit org.](nonprof_org.md) ~~ [NX](nx.md) ~~ [Oberth effect](oberth_eff.md) ~~ [Org.structure](orgstruct.md) ~~ [Outcomes commission](outccom.md) ~~ [Patent](patent.md) ~~ [Peter prin.](peter_principle.md) ~~ [Plan](plan.md) ~~ [PMBok](pmbok.md) ~~ [Quorum](quorum.md) ~~ [R&D management](mgmt.md) ~~ [R&D support](rnd_support.md) ~~ [Recursion](recurs.md) ~~ [Schulze_method](schulze_method.md) ~~ [Sci'N'Tech activities](st_act.md) ~~ [Sci'N'Tech council](satc.md) ~~ [Single-window system](sw_sys.md) ~~ [Situ.leadership](situ_leadership.md) ~~ [Skunk works](se.md) ~~ [State arm. plan](plan_sa.md) ~~ [Swamp](swamp.md) ~~ [Teamcenter](teamcenter.md) ~~ [Tennis racket theorem](tr_theorem.md) ~~ [TRIZ](triz.md) ~~ [TRL](trl.md) ~~ [V‑model](v_model.md) ~~ [Veto](veto.md) ~~ [Workflow](workflow.md) ~~ [Workgroup](wg.md)|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Governance>
1. [Ситуационное лидерство](situ_leadership.md)
1. <https://en.wikipedia.org/wiki/Control_system>
1. <https://en.wikipedia.org/wiki/Knowledge_management>
1. <http://sewiki.ru>
1. <https://habr.com/ru/company/domclick/blog/578822>


## The End

end of file
