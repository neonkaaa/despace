# Hanwha
> 2020.07.24 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/h/hanwha_logo1t.webp)](f/c/h/hanwha_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *Seongsan, Changwon, Gyeongsangnam-do, South Korea*<br> <http://www.hanwhatechwin.com>・ <https://hanwhaaerospace.com> ~~ [LI 1 ⎆](https://www.linkedin.com/company/hanwhaaerospace) & [LI 2 ⎆](https://www.linkedin.com/company/hanwha-aerospace) ~~ [Wiki 1 ⎆](https://en.wikipedia.org/wiki/Hanwha_Techwin) & [Wiki 2 ⎆](https://en.wikipedia.org/wiki/Hanwha_Aerospace)|
|:--|:--|
|**Business**|Engines|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Hanwha Aerospace Co., Ltd.**, a subsidiary of Hanwha Group, is an aerospace industrial company. The company is Korea’s only gas turbine [engine](ps.md) manufacturer, and specializes in the development, production and maintenance of aircraft engines. It was established in 1977 as Samsung Precision.



## The End

end of file
