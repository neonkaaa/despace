# Вето
> 2019.01.09 [🚀](../../index/index.md) [despace](index.md) → [Control](control.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Вето** — русскоязычный термин. **Veto** — англоязычный эквивалент.</small>

**Ве́то** (от лат. veto — 「запрещаю」) — право, означающее полномочие лица или группы лиц в одностороннем порядке заблокировать принятие того или иного решения.

Вето является механизмом поддержания статус‑кво, а не формирования политики.



## Описание
Право вето как общественный институт появилось в Древнем Риме. В Римской республике каждый из двух консулов мог заблокировать приведение в исполнение военного или гражданского решения другого консула. Также, каждый народный трибун имел право единолично отклонить закон, принятый Сенатом.

Вето может быть абсолютным (например, как в Совете Безопасности ООН, где постоянные члены могут заблокировать любое решение, принятое Советом) или суспензивным (отлагательным) (например, в законодательной системе США, где Палата представителей и Сенат ⅔ голосов могут преодолеть вето президента, или в Польше, где для преодоления вето требуется ⅗ голосов).

Среди относительного вето различают 「сильное」 и 「слабое」 вето.

1. При слабом вето парламент обязан лишь повторно рассмотреть законопроект. Для его преодоления достаточно такого же большинства, как и для принятия обычного закона. Так, президенты Франции и Италии имеют слабое суспензивное вето.
1. Сильное вето может быть преодолено лишь квалифицированным большинством, иногда с соблюдением усложнённой процедуры. Так, Президент США обладает сильным вето. Вето позволяет блокировать принятие решения и останавливать изменения, но никаким образом не может быть использовано для проведения каких‑либо реформ.

В ряде государств (Германия, Япония) право отклонять законы, принятые парламентом, у главы государства отсутствует, хотя закон должен быть утверждён главой государства.

Право вето имеет и народ, которое принято обозначать народным вето. В контексте современных конституционно‑правовых трансформаций народное вето следует рассматривать как конституционно‑правовую форму влияния граждан на принятие нормативно‑правовых актов, где ключевую роль играет не столько само проведение отклоняющего референдума по определённому нормативно‑правового акта, хотя это также имеет важное значение, сколько осознание представительным органом публичной власти реальности отмены его решения в рамках этой конституционной процедуры.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Control](Control.md)】**<br> [Ad hoc](ad_hoc.md) ~~ [Business travel](business_travel.md) ~~ [Chief designers council](cocd.md) ~~ [CML](trl.md) ~~ [Competence](competence.md) ~~ [Confident](confident.md) ~~ [Consp.theory](consp_theory.md) ~~ [Control sys. (CS)](cs.md) ~~ [Coordinate system](coord_sys.md) ~~ [Curator](curator.md) ~~ [Designer’s supervision](des_spv.md) ~~ [E‑sig](esig.md) ~~ [Engineer](se.md) ~~ [Errand](errand.md) ~~ [Federal law](fed_law.md) ~~ [Federal TP](fed_tp.md) ~~ [Federal SP](fed_sp.md) ~~ [GNC](gnc.md) ~~ [Gravity assist](gravass.md) ~~ [Industrial archaeology](ind_arch.md) ~~ [Instruction](instruction.md) ~~ [Lean manuf.](lean_man.md) ~~ [Lifetime](lifetime.md) ~~ [Manager](manager.md) ~~ [MBSE](se.md) ~~ [Meeting](meeting.md) ~~ [MCC](sc.md) ~~ [MIC](mic.md) ~~ [MML](mml.md) ~~ [MoU](contract.md) ~~ [Nav. & ballistics (NB)](nnb.md) ~~ [Nonprofit org.](nonprof_org.md) ~~ [NX](nx.md) ~~ [Oberth effect](oberth_eff.md) ~~ [Org.structure](orgstruct.md) ~~ [Outcomes commission](outccom.md) ~~ [Patent](patent.md) ~~ [Peter prin.](peter_principle.md) ~~ [Plan](plan.md) ~~ [PMBok](pmbok.md) ~~ [Quorum](quorum.md) ~~ [R&D management](mgmt.md) ~~ [R&D support](rnd_support.md) ~~ [Recursion](recurs.md) ~~ [Schulze_method](schulze_method.md) ~~ [Sci'N'Tech activities](st_act.md) ~~ [Sci'N'Tech council](satc.md) ~~ [Single-window system](sw_sys.md) ~~ [Situ.leadership](situ_leadership.md) ~~ [Skunk works](se.md) ~~ [State arm. plan](plan_sa.md) ~~ [Swamp](swamp.md) ~~ [Teamcenter](teamcenter.md) ~~ [Tennis racket theorem](tr_theorem.md) ~~ [TRIZ](triz.md) ~~ [TRL](trl.md) ~~ [V‑model](v_model.md) ~~ [Veto](veto.md) ~~ [Workflow](workflow.md) ~~ [Workgroup](wg.md)|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Veto>


## The End

end of file
