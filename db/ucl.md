# UCL
> 2019.08.08 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/u/ucl_logo1t.webp)](f/c/u/ucl_logo1.webp)|<mark>noemail</mark>, +44(0)20-7679-2000 , Fax …;<br> *University College London, Gower Street, London, WC1E 6BT, UK*<br> <http://www.ucl.ac.uk> ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/University_College_London)|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Университетский колледж Лондона** *(англ. University College London, UCL)* — университет города Лондон, входящий в состав Лондонского университета. Расположен в самом центре столицы, на Gower Street. Основанный в 1826 году как Лондонский университет (London University), UCL стал самым первым университетом Лондона.



## The End

end of file
