# August IST
> 2020.06.28 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c//_logo1t.webp)](f/c//_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *…*<br> <http://www.interstellar.ca> ~~ [LI ⎆](https://www.linkedin.com/company/august-interstellar-technologies)|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**August Interstellar Technologies** is a space infrastructure development company based in Toronto, Ontario. Dedicated to space exploration through research & development, education & commercialization of innovative solutions.



## The End

end of file
