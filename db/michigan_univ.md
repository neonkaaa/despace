# Michigan Univ.
> 2019.08.11 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/m/michigan_univ_logo1t.webp)](f/c/m/michigan_univ_logo1.webp)|<mark>noemail</mark>, +1(734)764-18-17, Fax …;<br> *500 S State St, Ann Arbor, MI 48109, USA*<br> <https://umich.edu> ~~ [FB ⎆](https://www.facebook.com/universityofmichigan) ~~ [IG ⎆](https://www.instagram.com/uofmichigan) ~~ [LI ⎆](https://www.linkedin.com/edu/school?:trk=edu-ca-head-title&id=18633) ~~ [X ⎆](https://twitter.com/umich) ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/University_of_Michigan)|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Мичига́нский университе́т (англ. University of Michigan; часто сокращается до U of M)** — публичный (государственный) исследовательский университет, расположенный в городе Энн‑Арбор в штате Мичиган, США. Основан в 1817 году в Детройте, но спустя двадцать лет (в 1837 году) переехал в Энн‑Арбор.

Мичиганский университет является старейшим университетом в штате, входит в состав так называемой 「Общественной лиги плюща[en]」 (англ. Public Ivy) и Ассоциации американских университетов, а также имеет два дополнительных кампуса, расположенных в Дирборне и во Флинте. Число ныне живущих выпускников Мичиганского университета превышает 460 000 — это один из самых высоких показателей среди университетов мира. Мичиганский университет является одним из ведущих учебных и научных центров США.



## The End

end of file
