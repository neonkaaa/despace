# AlbertaSat
> 2020.06.28 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c//_logo1t.webp)](f/c//_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *…*<br> <http://www.albertasat.ca> ~~ [IG ⎆](https://www.instagram.com/alberta_sat) ・ [LI ⎆](https://www.linkedin.com/company/albertasat) ~~ [X ⎆](https://twitter.com/albertasat)|
|:--|:--|
|**Business**|[CubeSats](sc.md) R&D. Student society|
|**Mission**|To build a better future through the development of space technology. We believe that this will allow Alberta to augment many aspects of Albertan industry & society, & move towards a province capable of contributing to a worldwide push for global peace, prosperity & expansion into the solar system.|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**AlbertaSat** is made up of close to 50 undergraduate & graduate students who work with the assistance of several faculty advisors at the University of Alberta. We represent a large range of backgrounds & capabilities with a common goal of a better future for humanity. We are human beings looking for a brighter tomorrow — a tomorrow that exists with the journey of humanity to the stars.

AlbertaSat built the Experimental Albertan #1 (Ex-Alta 1) Satellite as a part of the QB50 consortium mission.  Along with 37 other cube satellites in the QB50 mission, Ex-Alta 1 measures patterns of space weather on a completely new scale of science.  Ex-Alta 1 launched to the International Space Station April 18, 2017 from Cape Canaveral Air Force Station & was deployed May 26,2017. More importantly, Ex-Alta 1 represents an opportunity for the University of Alberta & the rest of the province to demonstrate a capability for space technology & betterment of humankind on a global stage.



## The End

end of file
