# SCJ
> 2020.07.03 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/s/scj_logo1t.webp)](f/c/s/scj_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *7-22-34, Roppongi, Minato-ku, Tokyo 106-8555, Japan*<br> <http://www.scj.go.jp/en/index.html> ~~ [LI ⎆](https://www.linkedin.com/company/science-council-of-japan/about) ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/Science_Council_of_Japan)|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Science Council of Japan (SCJ)** is the representative organization of Japanese scientist community ranging over all fields of sciences subsuming humanities, social sciences, life sciences, natural sciences, and engineering.



## The End

end of file
