# UEF SAV
> 2019.08.05 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/u/uef_sav_logo1t.webp)](f/c/u/uef_sav_logo1.webp)|<sekr@saske.sk>, +421-55-792-2201, Fax +421-55-633-62-92;<br> *Watsonova 47, 040 01 Košice, Slovenská republika*<br> <http://uef.saske.sk> ・ <https://ru.wikipedia.org/wiki/Словацкая_академия_наук>|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Ústav experimentálnej fyziky Slovenskej akadémie vied (UEF SAV)**, рус. **Институт экспериментальной физики Словацкой академии наук (ИЭФ САН)** — фундаментальные исследования в области физики конденсированных сред, ядерной и субатомной физике, физике космоса и биофизики. Институт экспериментальной физики в Кошице был создан 1 января 1969 г.



## The End

end of file
