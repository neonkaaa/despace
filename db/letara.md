# Letara
> 2022.12.15 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/l/letara_logo1t.webp)](f/c/l/letara_logo1.webp)|<info@letara.space>, <mark>nophone</mark>, Fax …;<br> *Hokudai Business Spring room 307, North 21 West 12, 2, Kita-ku, Sapporo, HOKKAIDO, 〒001-0021*<br> <https://www.letara.space> ~~ [FB ⎆](https://www.facebook.com/letara.space) ~~ [IG ⎆](https://www.instagram.com/letara_space) ~~ [LI ⎆](https://www.linkedin.com/company/letara) ~~ [X ⎆](https://twitter.com/letara_space)|
|:--|:--|
|**Business**|Developing & providing propulsion systems for small satellites|
|**Mission**|With our high thrust, safe, & inexpensive propulsion system technology for small satellites, we will promote groundbreaking advances in space propulsion & contribute to the improvement of Earth orbital activities & future human economic activities that will reach deep space.|
|**Vision**|**Beyond the Earth, Faster & Further.** We envision a future where large numbers of people & goods are transported freely through space to their final destinations from major space transportation hubs, incl. the lunar orbiting space stations & the lunar surface.|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO — Landon Kamps<br> ・COO — Shota Hirai|

**Letara Ltd.** is a Japanese company aimed to build space engines & propulsion systems. Founded 2020.06.23.

Concerning our Mission. The human economic sphere is being extended into Earth orbit along with the growing demand for small spacecraft. Space stations may be built on the Moon or Mars in the near future. When this happens, a new transportation in space will be needed is. Transportation from space hubs to local destinations, i.e., 「last‑mile transportation」, will be critical to the future space economy. Unfortunately, existing space propulsion technologies can only utilize very dangerous propellants, or are time‑consuming low‑power propulsion systems. The key technology to achieve last‑mile transportation in space is a propulsion system that is compact, safe, affordable, fast, storable, & controllable (throttling capable). We believe that Letara’s technology will not only improve existing Earth orbital activities, but will also be a mainstay of future human economic activity extending into deep space.

Letara was established in 2020 by members of  Hokkaido University’s Laboratory of Space Systems to implement hybrid propulsion technologies under development at Hokkaido University. Letara made its public debut by winning the Asia-Oceania prize at S-Booster 2021, & becoming a Hokkaido University Startup in 2022. Letara was founded by Japan’s foremost group of hybrid propulsion experts. We are creating the world’s 1st safe & high thrust propulsion systems by using plastic as rocket fuel. Our propriety ignition & thrust control technologies make space propulsion systems using plastic as fuel possible for the 1st time. Our 1st product lineup will be DARUMA, a modular propulsion system for small satellite & spacecraft developers. Our plan is to provide propulsion systems aligned with the needs of a wide range of satellites, from satellites you can hold in the palm of your hand, to satellites large enough for future passenger astronauts to fit inside of.

**Members:**

- **Founder & CEO — Landon Kamps, ​ケンプス ランドン —** When he deployed to Afghanistan in 2012 as a U.S. Army Intelligence Officer, he saw a gap in access to space infrastructure due to the availability of technologies via military satellites. He believes that safe & fast propulsion is the key to realizing a future world where all countries & people enable to access to space.
- **Founder & COO — Shota Hirai, ​平井 翔大 —** He is original founder of Letara, & pioneer of Letara’s cutting‑edge hybrid propulsion re‑ignition technologies. He has always wanted to contribute his knowledge & experience to society, & founded Letara with the hope of creating a world where everyone can go to space freely & safely.
- **Founder & CTO — Harunori Nagata, ​永田 晴紀 —** Professor of  Hokkaido University’s Graduate School of Engieering. He has a rich history of innovation & design within the hybrid propulsion industry, incl. the invention of the CAMUI‑type hybrid rocket, axial‑injection end‑burning‑type hybrid rocket, & low‑concentration hydrogen peroxide hybrid rocket.
- **Director & CLO — Keisuke Sakurai, ​櫻井 恵介 —** With about decade of experience as president of Sakurai Legal office in Sapporo, he is passionate about developing the space industry in Hokkaido, & building the support structure necessary for innovative startups like Letara to succeed. As the president of Uematsu Electric Co., Ltd. & passionate educator, he leads Japan in various space‑related activities such as small rocket development, microgravity experiments, small satellite development, & joint projects with U.S. private space companies.
- **What is 「Letara」**. Letara means 「pure white」 in the language of the Ainu, the indigenous people of Hokkaido. Letara is based in Sapporo, Hokkaido, & the name reflects the image of our 「snowy white」 landscape in Winter. The name also implies the newness of a start‑up company, & the ability to take on the appearance of other colors in the future.


## The End

end of file
