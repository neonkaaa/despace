# PolyOrbite
> 2019.08.13 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/p/polyorbite_logo1t.webp)](f/c/p/polyorbite_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *2500 Chemin de Polytechnique local m-3008, Montreal, QC H3T 1J4, Canada*<br> <https://polyorbite.ca> ・ [X ⎆](https://twitter.com/polyorbits) ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/Montreal_Student_Space_Associations)|
|:--|:--|
|**Business**|CubeSat R&D. Student society|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**PolyOrbite** is a technical society from Polytechnique Montréal. Within it, 70 qualified students at the undergraduate & graduate level work to attend a common goal: build & launch the 1st CubeSat made by students in Quebec. It is also a very experienced organization which has already succeed in large-scale projects. As a matter of fact, Polyorbite rose two times to the podium of the Canadian Satellite Design Challenge.

PolyOrbite was officially recognized by Polytechnique Montreal in 2013 & remains the only technical society within the university specialized in space technologies. We have developed a significant expertise in the design & manufacture of nano satellites.



## The End

end of file
