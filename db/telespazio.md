# Telespazio
> 2021.11.25 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/t/telespazio_logo1t.webp)](f/c/t/telespazio_logo1.webp)|<mark>noemail</mark>, <+39(06)407-91>, Fax +39(06)407-91;<br> *Via Tiburtina 965, 00156 Rome, Italy*<br> <https://www.telespazio.com> ~~ [IG ⎆](https://www.instagram.com/telespazio_company) ~~ [LI ⎆](https://www.linkedin.com/company/telespazio/?trk=company_name) ~~ [X ⎆](https://twitter.com/Telespazio)|
|:--|:--|
|**Business**|R&D of SC, launch services, comms, sat control, Earth observation services|
|**Mission**|To continue to serve the clients in our core space market, while exploiting space assets, technologies & processes (such as satellite communications, earth observation data, meteorology & navigation applications) in other market sectors such as defence, transport & energy|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO — Luigi Pasquali|

**Telespazio Spa** is a European spaceflight services company founded in 1961. It’s a joint venture owned by [Leonardo](leonardo.md) (67 %) & [Thales Group](contact/thales_as.md) (33 %) headquartered in Rome.

Telespazio provides services that include the design & development of space systems, the management of launch services & in‑orbit satellite control, Earth observation services, integrated communications, satellite navigation & localization & scientific programmes. The company manages space infrastructure, such as the Fucino Space Centre — & is involved in programmes incl. Galileo, EGNOS, Copernicus, COSMO-SkyMed, SICRAL & Göktürk.

Telespazio is present in France through Telespazio France; in Belgium with Telespazio Belgium; in Germany with Telespazio Germany, GAF & Spaceopal (a joint venture with the German Space Agency DLR), in the United Kingdom with Telespazio UK; in Spain with Telespazio Ibérica; & in Romania with Rartel. The company operates in South America through Telespazio Brasil & Telespazio Argentina.

<p style="page-break-after:always"> </p>

## The End

end of file
