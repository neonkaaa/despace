# Кворум
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [Control](control.md), [SE](se.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Кворум** — русскоязычный термин. **Quorum** — англоязычный эквивалент.</small>

**Кворум** *(лат. quorum praesentia sufficit 「которых присутствие достаточно」)* — установленное законом, уставом организации или регламентом число участников собрания (заседания), достаточное для признания данного собрания правомочным принимать решения по вопросам его повестки дня.



## Описание

Кворум может определяться числом присутствующих или процентным отношением количества присутствующих к общему количеству членов какого‑либо органа.

К наиболее часто применяемым вариантам кворума относятся:

1. Собрание правомочно рассматривать какие‑либо вопросы, если на нём присутствует более половины (или более ⅔) от его состава.
1. Для принятия какого‑либо решения требуется, чтобы в голосовании приняли участие более половины от всех имеющих право голоса.

Для различных вопросов повестки дня собрания возможно установление различных требований к кворуму.

Кворум может определяться следующими способами:

1. только в начале заседания при регистрации участников; после регистрации счётная комиссия докладывает о явке и о наличии (отсутствии) кворума;
1. по требованию участников;
1. постоянный контроль (учёт входящих‑выходящих участников счётной комиссией или с помощью электронной системы).

При отсутствии кворума, как правило, заседание не начинается или прерывается. Однако, регламенты некоторых парламентов разрешают при отсутствии кворума принимать решения по вопросам обеспечения явки депутатов. Также в некоторых парламентах, например, в Палате представителей США, во время обсуждений законопроектов кворум не проверяется, даже если его отсутствие очевидно.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Control](Control.md)】**<br> [Ad hoc](ad_hoc.md) ~~ [Business travel](business_travel.md) ~~ [Chief designers council](cocd.md) ~~ [CML](trl.md) ~~ [Competence](competence.md) ~~ [Confident](confident.md) ~~ [Consp.theory](consp_theory.md) ~~ [Control sys. (CS)](cs.md) ~~ [Coordinate system](coord_sys.md) ~~ [Curator](curator.md) ~~ [Designer’s supervision](des_spv.md) ~~ [E‑sig](esig.md) ~~ [Engineer](se.md) ~~ [Errand](errand.md) ~~ [Federal law](fed_law.md) ~~ [Federal TP](fed_tp.md) ~~ [Federal SP](fed_sp.md) ~~ [GNC](gnc.md) ~~ [Gravity assist](gravass.md) ~~ [Industrial archaeology](ind_arch.md) ~~ [Instruction](instruction.md) ~~ [Lean manuf.](lean_man.md) ~~ [Lifetime](lifetime.md) ~~ [Manager](manager.md) ~~ [MBSE](se.md) ~~ [Meeting](meeting.md) ~~ [MCC](sc.md) ~~ [MIC](mic.md) ~~ [MML](mml.md) ~~ [MoU](contract.md) ~~ [Nav. & ballistics (NB)](nnb.md) ~~ [Nonprofit org.](nonprof_org.md) ~~ [NX](nx.md) ~~ [Oberth effect](oberth_eff.md) ~~ [Org.structure](orgstruct.md) ~~ [Outcomes commission](outccom.md) ~~ [Patent](patent.md) ~~ [Peter prin.](peter_principle.md) ~~ [Plan](plan.md) ~~ [PMBok](pmbok.md) ~~ [Quorum](quorum.md) ~~ [R&D management](mgmt.md) ~~ [R&D support](rnd_support.md) ~~ [Recursion](recurs.md) ~~ [Schulze_method](schulze_method.md) ~~ [Sci'N'Tech activities](st_act.md) ~~ [Sci'N'Tech council](satc.md) ~~ [Single-window system](sw_sys.md) ~~ [Situ.leadership](situ_leadership.md) ~~ [Skunk works](se.md) ~~ [State arm. plan](plan_sa.md) ~~ [Swamp](swamp.md) ~~ [Teamcenter](teamcenter.md) ~~ [Tennis racket theorem](tr_theorem.md) ~~ [TRIZ](triz.md) ~~ [TRL](trl.md) ~~ [V‑model](v_model.md) ~~ [Veto](veto.md) ~~ [Workflow](workflow.md) ~~ [Workgroup](wg.md)|
|**【[Systems engineering](se.md)】**<br> [Competence](competence.md) ~~ [Coordinate system](coord_sys.md) ~~ [Designer’s supervision](des_spv.md) ~~ [Industrial archaeology](ind_arch.md) ~~ [Instruction](instruction.md) ~~ [Lean manuf.](lean_man.md) ~~ [Lifetime](lifetime.md) ~~ [MBSE](se.md) ~~ [MML](mml.md) ~~ [Nav. & ballistics (NB)](nnb.md) ~~ [NASA SEH](book_nasa_seh.md) ~~ [Oberth effect](oberth_eff.md) ~~ [PMBok](pmbok.md) ~~ [Quorum](quorum.md) ~~ [R&D management](mgmt.md) ~~ [R&D support](rnd_support.md) ~~ [Recursion](recurs.md) ~~ [Schulze_method](schulze_method.md) ~~ [Sci'N'Tech activities](st_act.md) ~~ [Sci'N'Tech council](satc.md) ~~ [Skunk works](se.md) ~~ [SysML](sysml.md) ~~ [Tennis racket theorem](tr_theorem.md) ~~ [TRIZ](triz.md) ~~ [TRL](trl.md) ~~ [V‑model](v_model.md) ~~ [Workflow](workflow.md) ~~ [Workgroup](wg.md)|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Quorum>


## The End

end of file
