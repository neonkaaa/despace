# Endurosat
> 2021.10.27 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/e/endurosat_logo1t.webp)](f/c/e/endurosat_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *16 Tundzha Str., Sofia, Bulgaria, 1606*<br> <https://www.endurosat.com> ~~ [FB ⎆](https://www.facebook.com/EnduroSat) ~~ [IG ⎆](https://www.instagram.com/endurosat) ~~ [LI ⎆](https://www.linkedin.com/company/endurosat) ~~ [X ⎆](https://twitter.com/endurosat)|
|:--|:--|
|**Business**|We provide exceptional NanoSats & space services to business, exploration, & science teams|
|**Mission**|Empower humans to connect & progress through space technology|
|**Vision**|We believe that universal access to space will inherently improve human life|
|**Values**|・**Simplicity.** Our team believes that simplicity is the ultimate form of sophistication. We strive for it every single day. It permeates all aspects of our operations - from the designing & creation of our products & satellites to the customer experience we deliver.<br> ・**Courage.** Success in space requires more than a boastful attitude & audacious speculations. That is why EnduroSat cultivates & practices a different kind of courage - the everyday courage of self-discipline, meticulous work ethic, & honesty.<br> ・**Expertise.** EnduroSat’s expertise in the field of space technologies & nanosats is a combination of experience, professionalism, & the ability to avoid thinking like an expert when needed. Our expert approach is one of critical thinking, adaptability, & constant improvement.<br> ・**Communication.** Clear & honest communication is a crucial element of achieving success in space. At EnduroSat, we believe that successful communication consists not only in conveying clear & concise information to our clients, but also in actively listening to them.|
|**[MGMT](mgmt.md)**|・CEO & Founder — Raycho Raychev<br> ・CFO — Yordan Kanchev<br> ・CPO — Vanya Buchova<br> ・CTO — Viktor Danchev|

**Endurosat** engineer, build, and operate exceptional NanoSats.

<p style="page-break-after:always"> </p>

## The End

end of file
