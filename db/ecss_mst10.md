# ECSS‑M‑ST‑10C Rev.1 (6‑Mar‑2009)
> 2009.03.06 [🚀](../../index/index.md) [despace](index.md) → [Doc](doc.md), [ECSS](ecss.md), [SC](sc.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

## 1. Introduction & Scope

**「Space project management. Project planning & implementation.」**

This Standard is one of the series of ECSS Standards intended to be applied together for the management, engineering & product assurance in space projects & applications. ECSS is a cooperative effort of the European Space Agency, national space agencies & European industry associations for the purpose of developing & maintaining common standards. **Requirements in this Standard are defined in terms of what shall be accomplished, rather than in terms of how to organize & perform the necessary work.** This allows existing organizational structures & methods to be applied where they are effective, & for the structures & methods to evolve as necessary without rewriting the standards. This Standard has been prepared by the ECSS‑M‑ST‑10 Working Group, reviewed by the ECSS Executive Secretariat & approved by the ECSS Technical Authority.

ECSS does not provide any warranty whatsoever, whether expressed, implied, or statutory, including, but not limited to, any warranty of merchantability or fitness for a particular purpose or any warranty that the contents of the item are error‑free. In no respect shall ECSS incur any liability for any damages, including, but not limited to, direct, indirect, special, or consequential damages arising out of, resulting from, or in any way connected to the use of this Standard, whether or not based upon warranty, contract, tort, or otherwise; whether or not injury was sustained by persons or property or otherwise; & whether or not loss was sustained from, or arose out of, the results of, the item, or any services that may be provided by ECSS.

Project planning & implementation is the project function, encompassing a coherent set of processes for all aspects of project management & control. This is done by:

1. establishing the project requirements & constraints derived from the mission statement.
2. defining phases & formal milestones enabling the progress of the project to be controlled with respect to cost, schedule & technical objectives (i.e. project control function).
3. defining project breakdown structures, which constitute the common & unique reference system for the project management to:
    1. identify the tasks & responsibilities of each actor;
    2. facilitate the coherence between all activities of the whole project;
    3. perform scheduling & costing activities.
4. setting up a project organization to perform all necessary activities on the project.

The scope of this ECSS Standard is limited to describing the key elements of project planning & implementation & identifying the top level requirements & products that together provide a coherent & integrated project planning across the  3  ECSS branches.

Where other ECSS management, engineering, or product assurance standards contain more specific & detailed requirements related to project planning, references are provided to identify where these can be found within the ECSS system.

This standard may be tailored for the specific characteristic & constrains of a space project in conformance with ECSS‑S‑ST‑00.



## 2. Normative references & Bibliography

The following normative documents contain provisions which, through reference in this text, constitute provisions of this ECSS Standard. For dated references, subsequent amendments to, or revision of any of these publications do not apply, However, parties to agreements based on this ECSS Standard are encouraged to investigate the possibility of applying the more recent editions of the normative documents indicated below. For undated references, the latest edition of the publication referred to applies.

1. ECSS‑S‑ST‑00‑01 — ECSS system — Glossary of terms
2. ECSS‑M‑ST‑40 — Space project management — Configuration & information management

**Bibliography:**

1. ECSS‑S‑ST‑00 — ECSS system — Description, implementation & general requirements
2. ECSS‑E‑ST‑10 — Space engineering — System engineering general requirements
3. ECSS‑E‑ST‑40 — Space engineering — Software general requirements
4. ECSS‑E‑ST‑70 — Space engineering — Ground systems & operations
5. ECSS‑M‑ST‑10‑01 — Space project management — Organization & conduct of reviews
6. ECSS‑M‑ST‑60 — Space project management — Cost & schedule management
7. ECSS‑M‑ST‑70 — Space project management — Integrated logistic support
8. [ECSS‑M‑ST‑80](ecss_mst80.md) — Space project management — Risk management
9. ECSS‑Q‑ST‑10 — Space product assurance — Product assurance management



## 3. Terms & definitions

**3.1. Terms defined in other standards**

For the purpose of this Standard, the terms & definitions from ECSS‑S‑ST‑00‑01 apply.

**3.2. Terms specific to the present standard**

1. **Discipline** — Specific area of expertise within a general subject. NOTE The name of the discipline normally indicates the type of expertise (e.g. in the ECSS System, system engineering, mechanical engineering, software & communications are disciplines within the Engineering domain)
2. **Domain** — General area of interest or influence covering a number of inter‑related topics or sub‑areas. NOTE The name of a domain normally indicates the area of interest (e.g. in the ECSS System, the Management, Engineering, & Product Assurance branches represent 3 different domains)
3. **Function** — Combination & interaction of a number of operations or processes, which together achieve a defined objective

**3.3. Abbreviated terms**

For the purposes of this Standard, the abbr. terms from ECSS‑S‑ST‑00‑01 & the following apply.

1. **AR** — acceptance review
2. **B/L** — baseline
3. **CBCP** — current baseline cost plan
4. **CDR** — critical design review
5. **CRR** — commissioning result review
6. **DRL** — document requirements list
7. **EAC** — estimate at completion
8. **EGSE** — electrical ground support equipment
9. **ELR** — end‑of‑life review
10. **ETC** — estimate to completion
11. **FRR** — flight readiness review
12. **GSE** — ground support equipment
13. **ILS** — integrated logistic support
14. **ITT** — invitation to tender
15. **LRR** — launch readiness review
16. **MCR** — mission close‑out review
17. **MDR** — mission definition review
18. **MGSE** — mechanical ground support equipment
19. **N/A** — not applicable
20. **OBCP** — original baseline cost plan
21. **OBS** — organizational breakdown structure
22. **ORR** — operational readiness review
23. **PDR** — preliminary design review
24. **PMP** — project management plan
25. **PRD** — project requirements documents
26. **PRR** — preliminary requirements review
27. **QR** — qualification review
28. **RFP** — request for proposal
29. **RFQ** — request for quote
30. **SRR** — system requirements review
31. **WBS** — work breakdown structure
32. **WP** — work package


## 4. Principles

### 4.1. Project planning

**4.1.1. Introduction**

Project planning & implementation encompasses all of the processes carried out to plan & execute a space project from initiation to completion at all levels in the customer‑supplier chain in a coordinated, efficient and structured manner. It’s a project wide activity receiving inputs from all project disciplines & involving close co‑operation across the project domains.

A space project typically comprises a **space segment** & a **ground segment** which are implemented in parallel (see ECSS‑E‑ST‑70). They rely on, & have interfaces with the **launch service segment**. These 3 segments comprise a space system.

In principle, a proposal to initiate a space project can be raised by any party. However, the most common initiators are:

1. individual governments, or co‑operation between a number of governments;
1. national, or international space agencies, either singly or collectively;
1. national or international scientific communities;
1. operators of commercial space systems.

In this ECSS standard, the top level customer is defined as the organization responsible for generating the top level space segment & ground segment business agreements & for interface arrangements with other external space system elements.

The following clauses 4.1.2 to 4.1.11 describe the key elements to be addressed, assessed, & balanced when planning a project.

| | |
|:--|:--|
|4.1.2| **Purpose & objectives of the project**.<br> The purpose & objectives of the project are defined by the project initiator in the mission statement which includes key performance parameters & technical & programmatic constraints to be applied to the project. They are normally coordinated with the top level customer, if one has been assigned.|
|4.1.3| **Availability of & need to develop new technologies**.<br> This is an assessment carried out jointly by the customer & supplier to identify the availability of scientific & technological know‑how & the technology needed to implement the project. The result of this assessment, which can be a significant cost & schedule driver, is a major input to the assessment of required resources & facilities & to the subsequent technical & programmatic risk assessment.|
|4.1.4| **Availability of & need to reuse existing equipments/products**.<br> This is an assessment of the feasibility of reusing existing products & is typically carried out as a direct response to a customer requirement. The result of this assessment, which also can have a significant influence on cost & schedule is a major input to the assessment of required resources & facilities & to the subsequent technical & programmatic risk assessment.|
|4.1.5| **Availability of & need for human resources, skills & tech. facilities**.<br> This is an assessment carried out jointly by the customer & supplier of the resources, skills & facilities required to implement the project. The result of this assessment shows if required resources, skills & facilities are adequate, or if additional skills, resources, or facilities are needed to complete the project.|
|4.1.6| **Risk assessment**.<br> The initial assessments of the technical & programmatic risks of a project are carried out by the customer, based on the project initiator’s inputs with respect to the purpose & objectives of the project, together with the identified technical & programmatic constraints to be applied to the project. The initial assessment is subsequently regularly expanded to include other relevant parameters as they become available, & as the project matures. Comprehensive risk assessments are conducted at each major project review.|
|4.1.7| **Development approach**.<br> The development approach for a project is jointly defined by the customer & supplier to comply with the project initiator’s mission statement, requirements & constraints, & balancing these with the outcome of paragraphs 4.1.3 to 4.1.6 above.|
|4.1.8| **Project deliverables**.<br> The customer has the responsibility for defining the deliverable products, needed to meet the project initiator’s mission statement, taking into account the assessments noted in clauses 4.1.4 to 4.1.7 above.|
|4.1.9| **Customer requirements & constraints**.<br> Customer requirements & constraints are prepared by the customer based on the outputs from 4.1.2 to 4.1.8 above & put into a format suitable for direct application in an invitation to tender (ITT). They address technical & programmatic requirements, as well as political, commercial, & industrial constraints to be applied to the project & collectively represent the project requirements documents (PRD).|
|4.1.10| **Project requirements documents (PRD)**.<br> The project requirements documents are an integral part of an ITT, request for proposal (RFP), or request for quote (RFQ) prepared & released by a customer to potential suppliers. The PRD typically comprise:<br> ・Statement of work.<br> ・Technical requirements documented in Technical Requirements Specification, as per ECSS‑E‑ST‑10‑06.<br> ・Management requirements.<br> ・Engineering requirements.<br> ・Product assurance requirements.<br> ・Programmatic requirements.<br> ・Other, project specific requirements (e.g. geographical distribution, model philosophy to be applied).<br> ・Documents requirements list (DRL).<br> ・Tender requirements.<br> Under the ECSS system, management, engineering & product assurance requirements are contained in the M, E, & Q standards, progressively tailored by each customer in the customer‑supplier chain to reflect the type & phase of the project covered by the business agreement, as well as the scope of the suppliers’ tasks required by the PRD.|
|4.1.11| **Project management plan**.<br> The top level project plan is the project management plan which defines the project management approach & methodology to be used throughout the life cycle of the project, together with an overview of all elements of project management disciplines. It includes the definition of the system engineering & product assurance management approach or provides references to separate system engineering & product assurance plans which together make up the total planning documentation used to implement a project.|



### 4.2. Project organization

1. **Introduction** — The establishment of a well structured & coherent organizational structure for implementing a project at all levels in the customer‑supplier chain is a key factor for ensuring an effective & efficient management approach. At each level in the customer‑supplier chain a project organization can be built as a self‑standing project team containing all necessary disciplines within the team structure or, more often, can be built around a core project team containing key project functions with other necessary functions being provided from outside the project team as external support. Irrespective of the organizational approach followed for a project, the elements summarized below are relevant at all levels in the customer‑supplier chain.
2. **Organizational structure** — It’s essential that the project organizational structure is arranged to include all disciplines essential to implement the project with well defined functions as well as clear reporting lines, inter‑relationships & interfaces. All project actors below the top level customer & above the lowest level supplier(s) have the roles of suppliers & customers, & their organizational structures are constructed to accommodate both roles. The organizational structure provides a clear & unambiguous definition & allocation of individual roles & responsibilities together with the necessary authority to implement these within the internal project set‑up as well as towards project external interfaces.
3. **Communication & reporting** — Effective means of communication are essential tools for ensuring clear & efficient inter‑action between all project actors, as well as between the project team & its external interfaces. Information technology is the primary means for the exchange of information. Communication serves initially to provide clarity about the project’s goals & objectives & subsequently, to support the day to day work of the project team. Regular reporting is an important tool for exchanging information concerning the progress of the project.
4. **Audits** — Audits are independent examinations to determine whether processes & procedures achieve the specified objective. They are an essential tool to identify problem areas.



### 4.3. Project breakdown structures

**4.3.1. Introduction**

Project breakdown structures provide the basis for creating a common understanding between all actors. They break the project down into manageable elements as described in the following clauses 4.3.2 to 4.3.7.

| | |
|:--|:--|
|4.3.2| **Function tree**.<br> The function tree is the breakdown of the system performances into functions. Each function is decomposed into sub‑functions independent of the type of products involved. The 「function」 approach is applied during project start‑up or during the system definition phase. More details about the function tree are given in ECSS‑E‑ST‑10, function tree DRD.|
|4.3.3| **Specification tree**.<br> The specification tree defines the hierarchical relationship of all technical requirements specifications for the different elements of a system or product. More details about the specification tree are given in ECSS‑E‑ST‑10, specification tree DRD.|
|4.3.4| **Product tree**.<br> The product tree is the breakdown of the project into successive levels of hardware & software products or elements, articulated to perform the functions identified in the function tree. However, the function & the product tree do not necessarily mirror each other. The product tree includes the development models, the GSE, the integration tools & test equipment, & external items necessary to validate the end product & ILS items. It includes items submitted to customer configuration control & items that are the subject of a technical requirements specification. The product tree forms the basis for the elaboration of the project work breakdown structure. An example of a product tree is shown in Fig.4‑1.<br><br>  【**Fig.4‑1:** Product tree example】<br> **Space System**<br> ░║<br> ░╟ **Space Segment**<br> ░║░╟ *GSE*<br> ░║░║░╟ EGSE<br> ░║░║░╙ MGSE<br> ░║░╟ *Payload*<br> ░║░║░╟ Instrument 1<br> ░║░║░╟ Instrument 2<br> ░║░║░╙ Instrument 3<br> ░║░╙ *Platform*<br> ░║░░░╟ Attitude control<br> ░║░░░╟ Data handling<br> ░║░░░╟ On‑board power supply<br> ░║░░░╟ Structure<br> ░║░░░╙ Thermal control<br> ░║<br> ░╙ **Ground Segment**<br> ░░░╟ Communications System<br> ░░░╟ Mission Control Center<br> ░░░╙ Payload Control Center|
|4.3.5| **Work breakdown structure (WBS)**.<br>The WBS is the principal structure used in managing a project & provides a framework for managing cost, schedule & technical content. It divides the project into manageable work packages, organized according to the nature of the work by breaking down the total work to be performed into increasing levels of detail.<br> The WBS is derived from the product tree, selected elements of which are extended to include support functions (i.e. management, engineering, product assurance) & associated services (e.g. test facilities). An example of a WBS is shown in Fig.4‑2.<br><br> 【**Fig.4‑2:** WBS example】<br> **Space System** 【 】<br> ░║<br> ░╟ **Space Segment** 【 】<br> ░║░╟ *GSE* 【 】<br> ░║░║░╟ EGSE 【 】<br> ░║░║░╙ MGSE 【 】<br> ░║░╟ *Payload* 【 】<br> ░║░║░╟ Instrument 1 【 】<br> ░║░║░╟ Instrument 2 【 】<br> ░║░║░╙ Instrument 3 【 】<br> ░║░╙ *Platform* 【 】<br> ░║░░░╟ Attitude control 【 】<br> ░║░░░╟ Data handling 【 】<br> ░║░░░╟ On‑board power supply 【 】<br> ░║░░░╟ Structure 【 】<br> ░║░░░╙ Thermal control 【 】<br> ░║<br> ░╙ **Ground Segment** 【 】<br> ░░░╟ Communications System 【 】<br> ░░░╟ Mission Control Center 【 】<br> ░░░╙ Payload Control Center 【 】<br> *Support function extensions — 【Engineering tasks, Management tasks, Product assurance tasks】*<br> ![](f/doc/ecss/ecss_mst10/f4_2.webp)|
|4.3.6| **Work package (WP)**.<br>A WP can be any element of the WBS down to the lowest level that can be measured & managed for planning, monitoring, & control.<br> Control work packages are identified by the supplier at the level in the WBS where visibility & control is required, & for which reporting is to be performed. The control work packages represent the total work‑scope & are agreed by the customer.<br> The work of each supplier is explicitly identified in the work breakdown structure by at least one control work package.|
|4.3.7| **Organization breakdown structure (OBS)**.<br> The OBS depicts the proposed project organization, including the interface & contractual responsibilities, as opposed to company organization breakdown structure, which depicts the functional aspects of the company. The project OBS shows the key personnel & the assigned responsible parties for each work package in the WBS.|



### 4.4. Project phasing

#### 4.4.1. Introduction

The life cycle of space projects is typically divided into 7 phases, as follows:

1. Phase 0 — Mission analysis/needs identification
1. Phase A — Feasibility
1. Phase B — Preliminary Definition
1. Phase C — Detailed Definition
1. Phase D — Qualification & Production
1. Phase E — Utilization
1. Phase F — Disposal

Project phases are closely linked to activities on system & product level. Depending on the specific circumstances of a project & the acceptance of involved risk, activities can overlap project phases. At the conclusion of the major activities & the related project reviews configuration baselines are established (see ECSS‑M‑ST‑40). A typical project life cycle is illustrated in Fig.4‑3.

【**Fig.4‑3:** Typical project life cycle】  
![](f/doc/ecss/ecss_mst10/f4_3.webp)

1. **Phases 0, A, & B** are focused mainly on:
   1. the elaboration of system functional & technical requirements & identification of system concepts to comply with the mission statement, taking into account the technical & programmatic constraints identified by the project initiator & top level customer.
   1. the identification of all activities & resources to be used to develop the space & ground segments of the project,
   1. the initial assessments of technical & programmatic risk,
   1. initiation of pre‑development activities.
1. **Phases C & D** comprise all activities to be performed to develop & qualify the space & ground segments & their products.
1. **Phase E** comprises all activities to be performed to launch, commission, utilize, & maintain the orbital elements of the space segment & utilize & maintain the associated ground segment.
1. **Phase F** comprises all activities to be performed to safely dispose all products launched into space as well as ground segment.

Each of the above project phases includes end milestones in the form of project review(s), the outcome of which determines readiness of the project to move forward to the next phase.

Requirements on organization & conduct of reviews are provided in ECSS‑M‑ST‑10‑01.

With the exception of the MDR which normally involves only the project initiator, & the top level customer, all other project reviews up to & including the AR are typically carried out by all project actors down to the lowest level supplier in the customer‑supplier chain involved in the project phases containing these reviews.

From the PRR to the PDR, the sequence of the reviews is 「top down」, starting with the top level customer & his top level supplier, & continuing down the customer‑supplier chain to the lowest level supplier. From the CDR to the AR, the sequence of reviews is reversed to 「bottom up」, starting with the lowest level supplier & its customer & continuing up through the customer‑supplier chain to the 1st level supplier & the top level customer. This so called 「V model」 is illustrated in Fig.4‑4.

【**Fig.4‑4:** Review life cycle】  
![](f/doc/ecss/ecss_mst10/f4_4.webp)



#### 4.4.2. Relationship between business agreements & project phases

A business agreement can cover a single project phase, a sequential grouping of project phases or sub‑phases (e.g. phase B1/B2; phase B2/C1/C2), depending on such factors as funding availability, competitive tendering, schedule, perceived risk. **Irrespective of the approach used for defining the scope of individual business agreements, all space projects essentially follow the classical project phases in sequence & include all of the major objectives & key milestones of each of these phases.**


#### 4.4.3. Project phases

**4.4.3.1. General.** The clause 4.4.3 provides an introduction & overview of the typical major tasks, associated review milestones, & review objectives for each of the phases in a project life cycle.

**4.4.3.2. Phase 0 (Mission analysis/needs identification).** This is mainly an activity conducted by the project initiator, the top level customer & representatives of the end users.

1. *Major tasks*:
   1. Elaborate the mission statement in terms of identification & characterization of the mission needs, expected performance, dependability & safety goals & mission operating constraints with respect to the physical & operational environment.
   1. Develop the preliminary technical requirements specification.
   1. Identify possible mission concepts.
   1. Perform preliminary assessment of programmatic aspects supported by market & economic studies as appropriate.
   1. Perform preliminary risk assessment.
1. *Associated review:*
   1. The **mission definition review (MDR)** at the end of phase 0. The outcome is to judge the readiness of the project to move into phase A. The primary objective is to release the mission statement & assess the preliminary technical requirements specification & programmatic aspects.

**4.4.3.3. Phase A (Feasibility).** This is mainly an activity conducted by the top level customer & one or several first level suppliers with the outcome being reported to the project initiator, & representatives of the end users for consideration.

1. *Major tasks*:
   1. Establish the preliminary management plan, system engineering plan & product assurance plan for the project.
   1. Elaborate possible system & operations concepts & system architectures & compare these against the identified needs, to determine levels of uncertainty & risks.
   1. Establish the function tree.
   1. Assess the technical & programmatic feasibility of the possible concepts by identifying constraints relating to implementation, costs, schedules, organization, operations, maintenance, production & disposal.
   1. Identify critical technologies & propose pre‑development activities.
   1. Quantify & characterize critical elements for technical & economic feasibility.
   1. Propose the system & operations concept(s) & technical solutions, including model philosophy & verification approach, to be further elaborated during Phase B.
   1. Elaborate the risk assessment.
1. *Associated review:*
   1. The **preliminary requirements review (PRR)** at the end of Phase A. The outcome is to judge the readiness of the project to move into Phase B. The primary objectives are:
      1. Release of preliminary management, engineering & product assurance plans.
      1. Release of the technical requirements specification.
      1. Confirmation of the technical & programmatic feasibility of the system concept(s).
      1. Selection of system & operations concept(s) & technical solutions, including model philosophy & verification approach, to be carried forward into Phase B.

**4.4.3.4. Phase B (Preliminary definition)**

1. *Major tasks*:
   1. Finalize the project management, engineering & product assurance plans.
   1. Establish the baseline master schedule.
   1. Elaborate the baseline cost at completion.
   1. Elaborate the preliminary organizational breakdown structure (OBS).
   1. Confirm technical solution(s) for the system & operations concept(s) & their feasibility with respect to programmatic constraints.
   1. Conduct 「trade‑off」 studies & select the preferred system concept, together with the preferred technical solution(s) for this concept.
   1. Establish a preliminary design definition for the selected system concept & retained technical solution(s).
   1. Determine the verification program including model philosophy.
   1. Identify & define external interfaces.
   1. Prepare the next level specification & related business agreement documents.
   1. Initiate pre‑development work on critical technologies or system design areas when it is necessary to reduce the development risks.
   1. Initiate any long‑lead item procurement required to meet project schedule needs.
   1. Prepare the space debris mitigation plan & the disposal plan.
   1. Conduct reliability & safety assessment.
   1. Finalize the product tree, the work breakdown structure & the specification tree.
   1. Update the risk assessment.
1. *Associated review:*
   1. The **system requirements review (SRR)** held during the course of Phase B. The primary objectives are:
      1. Release of updated technical requirements specifications.
      1. Assessment of the preliminary design definition.
      1. Assessment of the preliminary verification program.
   1. The **preliminary design review (PDR)** at the end of Phase B. The outcome is to judge the readiness of the project to move into Phase C. The primary objectives are:
      1. Verification of the preliminary design of the selected concept & technical solutions against project & system requirements.
      1. Release of final management, engineering & product assurance plans.
      1. Release of product tree, work breakdown structure & specification tree.
      1. Release of the verification plan (including model philosophy).

**4.4.3.5. Phase C (Detailed definition)**

1. *Major tasks* — The scope & type of tasks undertaken during this phase are driven by the model philosophy selected for the project, as well as the verification approach adopted.
   1. Completion of the detailed design definition of the system at all levels in the customer‑supplier chain.
   1. Production, development testing & pre‑qualification of selected critical elements & components.
   1. Production & development testing of engineering models, as required by the selected model philosophy & verification approach.
   1. Completion of assembly, integration & test planning for the system & its constituent parts.
   1. Detailed definition of internal & external interfaces.
   1. Issue of preliminary user manual.
   1. Update of the risk assessment.
1. *Associated review:*
   1. The **critical design review (CDR)** at the end of phase C. The outcome is to judge the readiness of the project to move into phase D. The primary objectives are:
      1. Assess the qualification & validation status of the critical processes & their readiness for deployment for phase D.
      1. Confirm compatibility with external interfaces.
      1. Release the final design.
      1. Release assembly, integration & test planning.
      1. Release flight hardware/software manufacturing, assembly & testing.
      1. Release of user manual.

**4.4.3.6. Phase D (Qualification & production)**

1. *Major tasks*:
   1. Complete qualification testing & associated verification activities.
   1. Complete manufacturing, assembly & testing of flight hardware/software & associated ground support hardware/software.
   1. Complete the interoperability testing between the space & ground segment.
   1. Prepare acceptance data package.
1. *Associated review:*
   1. The **qualification review (QR)** held during the course of the phase. The primary objectives are:
      1. To confirm that the verification process has demonstrated that the design, including margins, meets the applicable requirements.
      1. To verify that the verification record is complete at this & all lower levels in the customer‑supplier chain.
      1. To verify the acceptability of all waivers & deviations.
      1. Where development encompasses the production of one or several recurring products, the QR is completed by a functional configuration verification during which:
      1. The first article configuration is analyzed from the viewpoint of reproducibility.
      1. The production master files for the series productions are released.
      1. The series production go‑ahead file is accepted by the customer.
   1. The **acceptance review (AR)** he end of the phase. The outcome of this review is used to judge the readiness of the product for delivery.
      1. To confirm that the verification process has demonstrated that the product is free of workmanship errors & is ready for subsequent operational use.
      1. To verify that the acceptance verification record is complete at this & all lower levels in the customer‑supplier chain.
      1. To verify that all deliverable products are available per the approved deliverable items list.
      1. To verify the 「as‑built」 product & its constituent components against the required 「as designed」 product & its constituent components.
      1. To verify the acceptability of all waivers & deviations.
      1. To verify that the Acceptance Data Package is complete.
      1. To authorize delivery of the product.
      1. To release the certificate of acceptance.
   1. The **operational readiness review (ORR)**, the end of the phase. The primary objectives are:
      1. To verify readiness of the operational procedures & their compatibility with the flight system.
      1. To verify readiness of the operations teams.
      1. To accept & release the ground segment for operations.

**4.4.3.7. Phase E (Operations/utilization)**

1. *Major tasks* — The major tasks for this phase vary widely as a function of the type of project concerned. Therefore, only a general overview of activities during this phase is provided here.
   1. Perform all activities at space & ground segment level to prepare the launch.
   1. Conduct all launch & early orbital operations.
   1. Perform on‑orbit verification (including commissioning) activities.
   1. Perform all on‑orbit operations to achieve the mission objectives.
   1. Perform all ground segment activities to support the mission.
   1. Perform all other ground support activities to support the mission.
   1. Finalize the disposal plan.
1. *Associated review:*
   1. The **flight readiness review (FRR)** is held prior to launch. FRR is conducted prior to launch. The objective of this review is to verify that the flight & ground segments including all supporting systems such as tracking systems, communication systems & safety systems are ready for launch.
   1. The **launch readiness review (LRR)**, held immediately prior to launch. LRR is conducted just prior to launch. The objective of this review is to declare readiness for launch of the launch vehicle, the space & ground segments including all supporting systems such as tracking systems, communication systems & safety systems & to provide the authorization to proceed for launch.
   1. The **commissioning result review (CRR)**, held after completion of the on‑orbit commissioning activities. CRR at the end of the commissioning as part of the in‑orbit stage verification. It allows declaring readiness for routine operations/utilization. This Review is conducted following completion of a series of on‑orbit tests designed to verify that all elements of the system are performing within the specified performance parameters. Successful completion of this review is typically used to mark the formal handover of the system to the project initiator or to the system operator.
   1. The **end‑of‑life review (ELR)** held at the completion of the mission. ELR is to verify that the mission has completed its useful operation or service; to ensure that all on‑orbit elements are configured to allow safe disposal.

**4.4.3.8 Phase F (Disposal)**

1. *Major tasks* — Implement the disposal plan.
1. *Associated review* — The mission close‑out review (MCR) at the end of this phase. Main review objectives: to ensure that all mission disposal activities are adequately completed.


#### 4.4.4. Project specific reviews

In addition to the project reviews identified above, & depending on the type of project, the applicable business agreement & the overall implementation approach adopted, additional reviews can be inserted into the project planning against agreed sub‑milestones/additional milestones to meet particular project needs. Typical examples of such reviews are (these reviews are not further addressed in this standard.):

1. Detailed design review (software specific review, addressed in ECSS‑E‑ST‑40)
1. In orbit operations review (addressed in ECSS‑E‑ST‑70)
1. First article configuration review (serial production specific review)
1. System design review
1. System qualification review at ground level (launcher specific review)
1. System qualification review at flight level (launcher specific review)



## 5. Requirements

### 5.1. Project planning

Project planning requirements are applicable to all actors of a project from the top level customer down to the lowest level supplier. Project actors who have the role of a customer & a supplier carry the responsibilities associated with both roles. The scope & detail of requirements made applicable is a function of the level of each actor in the customer‑supplier chain, with the full scope of all requirements applicable to the top level supplier. The overall scope made applicable reduces, down through the customer‑supplier chain but becomes more specific as a function of the role played by each of these actors & the type of product(s) to be developed & delivered by them.

**Requirements on customers:**

1. Each customer shall prepare business agreements (ITT’s, RFP’s, or RFQ’s), including the project requirements documents (PRD) to be made applicable between him & his supplier(s).
1. Each customer shall use the ECSS standards to establish the project management, engineering & product assurance requirements applicable for the project.
1. Each customer shall make applicable to his supplier(s) only those ECSS standards relevant to the type & phase(s) of the project addressed by the business agreement.
1. Each customer shall specify by tailoring the minimum requirements within the applicable standards necessary for his supplier(s) to achieve the project objectives.
1. Each customer shall approve the project management plans & key personnel of his supplier(s).
1. Each customer shall verify compliance of his supplier(s) with all project requirements & constraints.
1. Each customer below the top level customer in the customer‑supplier chain shall in addition ensure that planning for his suppliers is consistent with the planning requirements imposed on him by his customer.

**Requirements on suppliers:**

1. Each supplier in the customer‑supplier chain shall prepare a project management plan (PMP) in conformance with Ann.A.
1. Each supplier in the customer‑supplier chain shall submit the PMP to his customer for approval.



### 5.2. Project organization

#### 5.2.1. Organizational structure

**Requirements on customers & suppliers:**

1. Each customer & supplier shall define the authority for project management & business agreement signing.
1. If the project has links with other projects, each customer & supplier shall define the responsibilities relating to the definition & the management of interfaces.
1. Where a customer, or supplier, employs consultants or other specialists to assist him in performing his duties, then the roles, responsibilities & authority of these consultants & specialists shall be defined.
1. When a customer supplies a product to a supplier he shall have the responsibility of a supplier in respect of that product.

**Requirements on suppliers:**

1. The supplier shall set up the project management organization in such a way that adequate resources are allocated to the project to ensure timely completion of the business agreement.
1. The supplier shall nominate a project manager with a project team under his authority & reporting directly to him.
1. The supplier shall ensure that the project manager has the authority to execute all tasks needed under the business agreement with direct access to his company management hierarchy to resolve conflicts at the appropriate level.
1. The supplier shall identify the key personnel to be deployed on the work, & include them in the project organization.
1. The supplier shall demonstrate that the key personnel have the necessary qualification, skills & experience to perform the task for which they are allocated.
1. The supplier’s project management organization shall exercise an active monitoring & control over its own & lower tier supplier’s activities & lead its lower tier supplier’s in the execution of subcontracted activities to ensure that their services conform to the customer’s requirements.
1. If a supplier is responsible for more than one business agreement within a project, & the business agreements have different customers, then each business agreement shall be clearly identified & accomplished according to the appropriate relationships.
1. The first level supplier shall establish, maintain & distribute a project directory including key personnel, as a minimum.


#### 5.2.2. Communication & reporting

**Requirements on customers & suppliers:**

1. The top level customer shall:
   1. specify a reporting system for the project
   1. specify an action monitoring system for the project
1. Each customer & supplier in the customer‑supplier chain shall implement & maintain the project reporting an action monitoring systems.
1. Formal meetings between the customer & his supplier(s) shall be held to review progress against approved project planning & address major deviations or changes proposed to the project requirements documents.
1. The frequency, location & schedule of customer‑supplier project meetings shall be agreed by all participating parties.
1. Customer‑supplier meetings shall be based on an agreed written agenda.
1. A chairperson & secretary shall be designated at the beginning of the meeting.
1. The results of the meeting shall be documented in the agreed minutes signed by all parties involved in the meeting.
1. Agreed actions shall be documented in an action item list.
1. Each action shall be allocated
   1. a unique identification
   1. the identification of the origin (e.g. meeting)
   1. the initiator
   1. the description of the action (clear & concise)
   1. the person responsible for the action
   1. the close‑out date
   1. the current status
   1. the close‑out reference (e.g. document, letter)
1. Action items shall be reviewed at each meeting.
1. Any matters documented in the minutes of meeting having impact on the business agreement shall be subject to the contract change procedure for implementation.

**Requirements on suppliers:**

1. The supplier shall prepare & submit progress reports to his customer in conformance with Ann.E.
1. The supplier shall prepare minutes of progress meetings for approval of the customer.
1. The supplier shall notify the customer within an agreed period of time of any event that could significantly affect the achievement of the business agreement objectives in terms of cost, technical performance & schedule, & any situation resulting in a substantial schedule or planning change demanding immediate customer involvement.


#### 5.2.3. Audits

**General requirements:**

1. Every audit performed shall be followed by a report prepared by the auditor & containing the views of both parties.
1. The conclusions of the audit & the draft report shall be discussed with the supplier, before finalization & release.
1. In the event of disagreement with any of the audit conclusions, the supplier may add his observations & comments.
1. The final audit report shall not be divulged without the agreement of the audited supplier.

**Requirements on customers:**

1. The customer shall notify the supplier in due time of:
   1. his intention to perform an audit;
   1. the objectives & the scope of the audit;
   1. the designated auditor & his terms of reference;
   1. the audit schedule.

**Requirements on suppliers:**

1. The supplier shall accept to be audited by the customer or by a third party agreed between the customer & the supplier in the framework of the business agreement.
1. The supplier shall have the right to demand that the audit be performed by a third party, & that the third party obtain authorization each time the audit necessitates access to information concerning patent rights or confidentiality associated with defence secrecy.
1. The supplier shall perform audits of his own project activities & of the project activities of his lower tier supplier(s) to verify conformance with project requirements.
1. The supplier shall provide right of access for participation by the customer in his own audits & in audits of his lower tier suppliers.
1. The supplier shall provide his customer access to his facilities & data which are relevant in the frame of the business agreement.



### 5.3. Project breakdown structures

1. The supplier shall develop the product tree for his products down to the deliverable end items, incorporating the product trees of each of his lower tier suppliers, in conformance with Ann.B.
1. The product tree shall be established at start of phase B & finalized not later than PDR.
1. The rules for product item identification shall be uniform within the project.
1. A unique identification shall be assigned to each item within the product tree.
1. The identification shall remain unchanged during the product lifetime, unless a modification causes discontinuation of interchangeability.
1. The product tree shall be subject to customer approval.
1. The supplier shall maintain the product tree up‑to‑date under configuration control.
1. The supplier shall establish the work breakdown structure (WBS) for his work share, incorporating the WBS of each of his lower tier suppliers, in conformance with Ann.C.
1. Work related to manufacturing, assembly, integration & test of all product models shall be shown in the WBS.
1. Support functions (management, engineering, product assurance) shall be identifiable in connection with its related product tree elements.
1. The WBS shall be subject to customer approval.
1. Each supplier shall maintain up‑to‑date the WBS for his work share in the project.
1. The supplier shall identify control work packages based on the approved WBS, & the level of visibility required by the customer.
1. The supplier shall establish work package (WP) descriptions for each work package shown in his WBS in conformance with Ann.D.
1. Each WP shall have a single responsible WP manager.
1. The supplier shall establish a project organization breakdown structure (OBS), which includes.
    * the interface & contractual responsibilities amongst the involved parties
    * the key personnel & the assigned responsible parties for each work package in the WBS
1. The project OBS shall be submitted to the customer for approval.
1. The supplier shall maintain the approved OBS, keep it up‑to‑date, & issue it to the lower tier suppliers & the customer.



### 5.4. Project phasing

1. The customer shall organize the project into sequential phases which include all project specific reviews & decision milestones. NOTE Phases & reviews are defined in clause 4.4.3.
1. The customer shall prepare project review procedures for all project reviews.
1. The customer shall ensure that the project reviews of his supplier(s) are in line with the top down / bottom up sequence of the overall project review planning.
1. The customer shall take the decision to move from one phase to the next on the basis of the outcome of the associated 「end of phase」 review. (1) Information concerning the expected delivery of ECSS management branch documents per review is provided in Ann.F. (2) Information concerning additional documents which are defined as outputs of the management standards requirements & which are not part of review data packages is provided in Ann.G.



## Annex A (normative) Project management plan (PMP) — DRD

**A.1 DRD identification.** *Requirement ID & source* — ECSS‑M‑ST‑10, requirement 5.1.3a. *Purpose & objective* — to state the purpose & provide a brief introduction to the project management system. It covers all aspects of the latter.

**A.2 Expected response**

*Special remarks* — None.

*Scope & content:*

1. **Introduction.** The purpose, objective & the reason prompting its preparation (e.g. program or project reference & phase).
1. **Applicable & reference documents.** The applicable & reference documents used in support of the generation of the document.
1. **Objectives & constraints of the project.** Describe the objective & constraints of the project in conformance with the project requirements documents.
1. **Project organization.** Describe the project organization approach in conformance with the requirements as per clause 5.2.
1. **Project breakdown structures.** Describe the project breakdown structures approach in conformance with the project breakdown structure requirements as defined in clause 5.3 & identify the title of individual documents called up by these requirements.
1. **Configuration, information & documentation management.** Describe the configuration, information & documentation management approach, as per ECSS‑M‑ST‑40 Ann.A. If this approach is contained in a rolled‑out configuration management plan, the PMP may include only a brief description together with a reference to the configuration, information & documentation management plan.
1. **Cost & schedule management.** Describe the cost & schedule management approach, as per ECSS‑M‑ST‑60. If this approach is described in a rolled‑out cost & schedule management plan, the PMP may include only a brief description together with a reference to the cost & schedule management plan.
1. **Integrated logistic support.** Describe the integrated logistic support approach, as per ECSS‑M‑ST‑70.
1. **Risk management.** Briefly describe the risk management approach which is described in more detail in a rolled‑out risk management policy & plan, as defined in [ECSS‑M‑ST‑80](ecss_mst80.md), Ann. A & B.
1. **Product assurance management.** Describe the product assurance management approach, including the proposed breakdown into PA disciplines & the interfaces between these disciplines, as per ECSS‑Q‑ST‑10 Ann.A. If the product assurance management approach is described in a rolled‑out PA plan, the PMP may include only a brief description together with a reference to the product assurance plan.
1. **Engineering management.** Describe the engineering management approach, including the proposed breakdown into engineering disciplines & the interfaces between these disciplines, as per ECSS‑E‑ST‑10 Ann.D. If the engineering management approach is described in a rolled‑out system engineering plan, the PMP may include only a brief description together with a reference to the system engineering plan.



## Annex B (normative) Product tree — DRD

**B.1 DRD identification.** *Requirement ID & source* — ECSS‑M‑ST‑10, requirement 5.3a. *Purpose & objective* — to describe the hierarchical partitioning of a deliverable product down to a level agreed between the customer & supplier. It’s the starting point for selecting configuration items (as specified in ECSS‑M‑ST‑40) & establishing the work breakdown structure. It’s a basic structure to establish the Specification tree (as per ECSS‑E‑ST‑10).

**B.2 Expected response**

*Special remarks.* The product tree **is part of the design definition file (DDF)**.

*Scope & content:*

1. **Introduction.** The purpose, objective & the reason prompting its preparation (e.g. program/project reference & phase).
1. **Applicable & reference documents.** The applicable & reference documents used in support of the generation of the document.
1. **Tree structure.**
   1. Provide the breakdown of lower level products constituting the deliverable product.
   1. For each item identified in the product tree, provide: ➀ identification code, ➁ item name, ➂ item supplier, ➃ applicable specification.
   1. Present the PT either as a graphical diagram or an indentured structure where the product (i.e. at the top level of the tree) is decomposed into lower level products.
   1. Identify in the PT each product item selected as configuration item.
   1. Identify in the PT which recurrent products from previous space projects are used.



## Annex C (normative) Work breakdown structure (WBS) — DRD

**C.1 DRD identification.** *Requirement ID & source* — ECSS‑M‑ST‑10, requirement 5.3h. *Purpose & objective* — To provide a framework for project in cost & schedule management activities (as per ECSS‑M‑ST‑60) & for managing technical content. It assists projectʹs actors in: conducting tender comparisons & business agreement negotiations; optimizing the distribution of work amongst the different suppliers; monitoring the schedule of the project. The WBS divides the project into manageable work packages, organized by nature of work. It identifies the total work to be performed down to a level of detail agreed between the customer & supplier. Information concerning the determination of the appropriate WBS level of detail is provided in ECSS‑M‑ST‑10 Ann.H.

**C.2 Expected response**

*Special remarks* — None.

*Scope & contents*:

1. **Introduction.** The purpose, objective & the reason prompting its preparation (e.g. program or project reference & phase).
1. **Applicable & reference documents.** The applicable & reference documents used in support of the generation of the document.
1. **Tree structure**
   1. Provide a logical & exhaustive breakdown of the product tree elements, that includes the customer’s defined support functions (e.g. project management, engineering, product assurance support) necessary to produce the end item deliverables (development & flight models) & the necessary services as appropriate for the project
   1. A coding scheme for WBS elements that represents the hierarchical structure when viewed in text format shall be used. ((1) A common coding system facilitates communications among all project actors. (2) E.g.: to each WBS element is assigned a code used for its identification throughout the life of the project. It can be a simple decimal or alphanumeric coding system that logically indicates the level of an element & related lower‑level subordinate elements)
   1. Identify all control work‑packages
   1. The control work‑packages may be further broken down by the supplier in several more detailed work‑packages
   1. All defined work‑packages together shall cover the total work scope
   1. Present the WBS either as a graphical diagram or an indentured structure



## Annex D (normative) Work package (WP) description — DRD

**D.1 DRD identification.** *Requirement ID & source* — ECSS‑M‑ST‑10, requirement 5.3n. *Purpose & objective* — to describe the detailed content of each element of the WBS as per ECSS‑M‑ST‑10 Ann.C.

**D.2 Expected response**

*Special remarks* — None.

*Scope & content:*

The WP description shall contain the following elements:

1. project name & project phase
2. WP title
3. unique identification of each WP & issue number
4. supplier or entity in charge of the WP performance
5. WP manager’s name & organization
6. supplier’s country
7. product to which the tasks of the WP are allocated (link to the product tree)
8. description of the objectives of the WP
9. description of the tasks
10. list of the inputs necessary to achieve the tasks
11. interfaces or links with other tasks or WP’s
12. list of constraints, requirements, standards, & regulations
13. list of the expected outputs
14. list of deliverables
15. location of delivery
16. start event identification including date
17. end event identification including date
18. excluded tasks



## Annex E (normative) Progress report — DRD

**E.1 DRD identification.** *Requirement ID & source* — ECSS‑M‑ST‑10, requirement 5.2.2.2a. *Purpose & objective* — to provide all actors with actual information concerning the status of the project.

**E.2 Expected response**

*Special remarks* — None.

*Scope & content:*

The progress report shall contain the following information:

1. The project manager’s assessment of the current situation in relation to the forecasts & risks, at a level of detail agreed between the relevant actors.
2. The status of the progress of work being performed by the supplier.
3. Status & trends of agreed key performance & engineering data parameters.
4. Adverse trends in technical & programmatic performance & proposals for remedial actions.
5. Planning for implementation of remedial actions.
6. A consolidated report derived from the lower tier suppliers status reports.
7. Progress on all actions since the previous report.



## Annex F (informative) ECSS management branch documents delivery per review

Table F‑1 provides the information concerning the expected delivery of ECSS management branch documents per review. NOTE This table constitutes a first indication for the data package content at various reviews. The full content of such data package is established as part of the business agreement, which also defines the delivery of the document between reviews.

The various crosses in a row indicate the increased levels of maturity progressively expected versus reviews. The last cross in a row indicates that at that review the document is expected to be completed & finalized.

【**Table F‑1:** Management Documents Delivery per Review】

|**#**|Phase →|**0**|**a**|**B**|**B**|**c**|**D**|**D**|**e**|**e**|**e**|**e**|**e**|**F**| |
|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|
| |**Document ↓**|**M**|**P**|**S**|**P**|**C**|**Q**|**A**|**O**|**F**|**L**|**C**|**E**|**M**|**DRD Ref.**|
| | |**D**|**R**|**R**|**D**|**D**|**R**|**R**|**R**|**R**|**R**|**R**|**L**|**C**| |
| | |**R**|**R**|**R**|**R**|**R**| | |**R**|**R**|**R**|**R**|**R**|**R**| |
|1|Project management plan| |✘|✘|✘| | | | | | | | | |ECSS‑M‑ST‑10 Ann.A|
|2|Product tree| |✘|✘|✘|✘|✘| | | | | | | |ECSS‑M‑ST‑10 Ann.B|
|3|Work breakdown structure| |✘|✘|✘| | | | | | | | | |ECSS‑M‑ST‑10 Ann.C|
|4|Work package description| |✘|✘|✘| | | | | | | | | |ECSS‑M‑ST‑10 Ann.D|
|5|Schedule|✘|✘|✘|✘|✘|✘|✘|✘|✘| | | | |[ECSS‑M‑ST‑60](ecss_mst60.md) Ann.B|
|6|Cost estimate report| |✘|✘|✘| | | | | | | | | |[ECSS‑M‑ST‑60](ecss_mst60.md) Ann.G|
|7|Configuration management plan| |✘|✘|✘| | | | | | | | | |[ECSS‑M‑ST‑40](ecss_mst40.md) Ann.A|
|8|Configuration item list| | | |✘|✘| | | | | | | | |[ECSS‑M‑ST‑40](ecss_mst40.md) Ann.B|
|9|Configuration item data list| | | |✘|✘|✘|✘| | | | |, | |[ECSS‑M‑ST‑40](ecss_mst40.md) Ann.C|
|10|As‑built configuration list| | | | | |✘|✘| | | | | | |[ECSS‑M‑ST‑40](ecss_mst40.md) Ann.D|
|11|Software configuration list| | | |✘|✘|✘|✘| | | | | | |[ECSS‑M‑ST‑40](ecss_mst40.md) Ann.E|
|12|Configuration status accounting reports| | | |✘|✘|✘|✘| | | | | | |[ECSS‑M‑ST‑40](ecss_mst40.md) Ann.F|
|13|Risk management policy document|✘|✘|✘|✘| | | | | | | | | |[ECSS‑M‑ST‑80](ecss_mst80.md) Ann.A|
|14|Risk management plan|✘|✘|✘|✘| | | | | | | | | |[ECSS‑M‑ST‑80](ecss_mst80.md) Ann.B|
|15|Risk assessment report| |✘|✘|✘|✘|✘|✘|✘|✘| | | | |[ECSS‑M‑ST‑80](ecss_mst80.md) Ann.C|



## Annex G (informative) Management documents delivery (periodic or incident triggered)

Table G‑1 lists the documents which are defined as outputs of the management standards requirements & which are not part of review data packages.

【**Table G‑1:** Management documents delivery (periodic or incident triggered)】

|**Document Title DRD ref.**|**DRD Ref.**|
|:--|:--|
|Cost breakdown structure|ECSS‑M‑ST‑60 Ann.A|
|Schedule progress report|ECSS‑M‑ST‑60 Ann.C|
|Company Price Breakdown Forms|ECSS‑M‑ST‑60 Ann.D|
|Geographical Distribution Report|ECSS‑M‑ST‑60 Ann.E|
|Cost Estimating Plan|ECSS‑M‑ST‑60 Ann.F|
|Milestone Payment Plan|ECSS‑M‑ST‑60 Ann.H|
|Inventory Record|ECSS‑M‑ST‑60 Ann.I|
|Cost & Manpower Report|ECSS‑M‑ST‑60 Ann.J|
|OBCP & CBCP for Cost Reimbursement|ECSS‑M‑ST‑60 Ann.K|
|OBCP & CBCP for Fixed Price|ECSS‑M‑ST‑60 Ann.L|
|EAC & ETC for Cost Reimbursement|ECSS‑M‑ST‑60 Ann.M|
|EAC for Fixed Price|ECSS‑M‑ST‑60 Ann.N|
|Contract Change Notice|ECSS‑M‑ST‑60 Ann.O|
|Change request|ECSS‑M‑ST‑40 Ann.G|
|Change proposal|ECSS‑M‑ST‑40 Ann.H|
|Request for deviation|ECSS‑M‑ST‑40 Ann.I|
|Request for waiver|ECSS‑M‑ST‑40 Ann.J|



## Annex H (informative) Determination of the appropriate WBS level of detail

The main challenge associated with developing the **work breakdown structure (WBS)** is to determine the balancing between the project definition aspects of the WBS & the requirements for data collecting & reporting. One has to keep in mind that the WBS is a tool designed to assist the project manager when decomposing the project only to the levels necessary to meet the needs of the project, the nature of the work, & the confidence of the team.

An excessive WBS levels can lead to unrealistic levels of maintenance & reporting, & consequently to an inefficient & over costly project. The theory that more management data equates to better management control has been proven false many times over in the last decades when assessing systems performance. On the other hand, if not detailed enough it makes the element difficult to manage or the risk unacceptable.

Among the different questions arising when developing a WBS, an important one is: should the WBS be decomposed further?

To help answering this question, we propose the following list of questions. If most of the questions can be answered YES, then the WBS element analyzed should be decomposed. On the contrary, if most of the questions can be answered NO, then this is not necessary. If the answers are approximately 50/50, then additional judgment is needed.

1. Is there a need to improve the assessment of the cost estimates or progress measuring of the WBS element?
1. Is there more than one individual responsible for the WBS element? Often a variety of resources are assigned to a WBS element, a unique individual is assigned the overall responsibility for the deliverable created during the completion of the WBS element.
1. Does the WBS element content include more than one type of work process or produces more than one deliverable at completion?
1. Is there a need to assess the timing of work processes that are internal to the WBS element?
1. Is there a need to assess the cost of work processes or deliverables that are internal to the WBS element?
1. Are there interactions between deliverables within a WBS element to another WBS element?
1. Are there significant time gaps in the execution of the work processes that are internal to the WBS element?
1. Do resource requirements change over time within a WBS element?
1. Are there acceptance criteria, leading to intermediate deliverable(s), applicable before the completion of the entire WBS element?
1. Are there identified risks that require specific attention to a subset of the WBS element?
1. Can a subset of the work to be performed within the WBS element be organized as a separate unit?



## The End

end of file
