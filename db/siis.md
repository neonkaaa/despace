# SI Imaging Services
> 2021.05.25 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/s/siis_logo1t.webp)](f/c/s/siis_logo1.webp)|<PublicRelations@si-imaging.com>, +82-70-7835-1922, Fax …;<br> *SI Imaging Services, 169-84, Gwahak-ro, Yuseong-gu, Dae-jeon, 34133, Republic of Korea*<br> <https://www.si-imaging.com> ~~ [LI ⎆](https://www.linkedin.com/company/si-imaging-services) ~~ [X ⎆](https://twitter.com/si_imaging)|
|:--|:--|
|**Business**|Sat imagery provider|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**SI Imaging Services (SIIS)** is the exclusive worldwide marketing and sales representative of KOMPSAT series KOMPSAT-2, KOMPSAT-3, KOMPSAT-3A and KOMPSAT-5. A part of [Satrec Initiative (SI)](satreci.md).

SIIS contributes Remote Sensing and Earth observation industries societies by providing very high resolution optical and SAR images through over 137 sales partners worldwide. Customers from industries as well as government and international agencies are using KOMPSAT imagery for their missions and researches and achieve good results in several remote sensing applications such as mapping, agriculture, disaster management, and so on. SIIS started its business as a satellite image and service provider and extended its business to KOMPSAT operation.



## The End

end of file
