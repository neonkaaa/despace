# Canadian Space Mining Corporation
> 2021.10.27 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/c/csmc_logo1t.webp)](f/c/c/csmc_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *30 Commercial Road, Toronto, Ontario, M4G 1Z4, CA*<br> <https://csmc-scms.ca> ~~ [LI ⎆](https://www.linkedin.com/company/canadian-space-mining-corporation)|
|:--|:--|
|**Business**|Space mining, resource extraction & exploration|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO & Co‑Founder — Daniel Sax<br> ・President & Co‑Founder — Dieter MacPherson|

**Canadian Space Mining Corporation (CSMC)** is devoted to exploration & extraction of space bound resources. Founded in 2021.

<p style="page-break-after:always"> </p>

## The End

end of file
