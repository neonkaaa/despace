# JPL
> 2019.08.05 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/j/jpl_logo1t.webp)](f/c/j/jpl_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax +1(818)354-43-21;<br> *4800 Oak Grove Dr, Pasadena, CA 91109, USA*<br> <http://www.jpl.nasa.gov> ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/Jet_Propulsion_Laboratory)|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Лаборатория реактивного движения** *(**ЛРД**; англ. **Jet Propulsion Laboratory** или **JPL**)* — научно‑исследовательский центр [НАСА](nasa.md), расположенный рядом с городами Пасадина и Каньяда Флинтридж (англ. La Cañada Flintridge) около г. Лос‑Анджелеса США. Управляется Калифорнийским технологическим институтом (Калтех), занимается созданием и обслуживанием АКА для НАСА. Основана 31 октября 1936.

Включает в себя в том числе:

- [JPL Innovation Foundry](jpl_if.md)
- Поддержка [NICM](nicm.md), incl. data collection, methodology development, maintenance, & training.

**Программы:**

- <https://www.jpl.nasa.gov/missions/?:type=current>
- Программа 「Эксплорер」
- Программа 「Рейнджер」
- Программа 「Сервейер」
- Маринер
- Программа 「Пионер」
- Программа 「Викинг」
- Программа 「Вояджер」
- Магеллан
- Галилео
- Deep Space 1
- Mars Global Surveyor
- Mars Climate Orbiter
- Кассини‑Гюйгенс
- Стардаст
- Марс Одиссей
- Mars Pathfinder
- Программа Mars Exploration Rover (марсоходы Спирит и Оппортьюнити)
- Спитцер
- Mars Reconnaissance Orbiter
- Gravity Recovery And Climate Experiment
- CloudSat
- Феникс
- OSTM
- Orbiting Carbon Observatory
- Wide‑Field Infrared Survey Explorer
- Mars Science Laboratory (марсоход Кьюриосити)



## The End

end of file
