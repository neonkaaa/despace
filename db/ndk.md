# Nihon Dempa Kogyo
> 2020.07.20 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/n/ndk_logo1t.webp)](f/c/n/ndk_logo1.webp)|<bio-m@ndk.com>, <mark>nophone</mark>, Fax …;<br> *Tokyo, Japan*<br> <https://www.ndk.com/en/index.html>・ <https://aerospacebiz.jaxa.jp/en/spacecompany/ndk>|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**NIHON DEMPA Kogyo Co., LTD. (NDK)** has been providing crystal devices since the foundation, & is one of [JAXA](jaxa.md)’s qualified electrical parts manufacturer. Utilizing its accumulated crystal technology, the company started selling outgas analysis system with crystal sensor known as 「QTGA (QCM Thermogravimetric Gas Analysis)」 system in 2019.

To prevent damages on space equipment, selection of organic materials with less outgas is very important. NDK contributes for the material selections with its high accurate QTGA systems.

Main products:

- Outgas analysis system (QTGA system),
- Crystal-Related products such as Crystal devices (e.g. Crystal Units, Crystal Oscillators, Crystal filters),
- Ultrasonic Transducers,
- Synthetic Quartz & Crystal Blank.



## The End

end of file
