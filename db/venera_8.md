# Venera 8
> 2019.12.13 [🚀](../../index/index.md) [despace](index.md) → [Venus](venus.md), **[Project](project.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Венера‑8** — RU term w/o analogues in English. **Venera 8** — English equivalent.</small>

**Venera 8** (**Венера‑8**) was a spacecraft in the Soviet Venera program for the exploration of Venus and was the second lander to conduct a successful landing on the surface of Venus. The Venera 8 landing was planned on the day side in order to prepare for image acquisition by the next missions, it landed on the day side 500 km from the morning terminator. Transmitted information from the surface for 50 min. The illumination was measured and obtained as 350 ± 150 Lux (extrapolated to noon is 1 000 – 3 000 Lux – sufficient for the operation of television systems). The wind speed was measured at 50 - 60 ㎧ at an altitude of 50 ㎞, and 0 – 2 ㎧ at the surface, i.e. super rotation was detected. The 1st measurements were made of the content of natural radioactive elements in the soil (potassium, uranium, thorium).

![](f/project/v/venera_8/pic01.webp) ![](f/project/v/venera_8/pic02.webp)

[![](f/project/v/venera_8/pic03t.webp)](f/project/v/venera_8/pic03.webp)



|**Type**|**[Param.](si.md)**|
|:--|:--|
|**【Mission】**|~ ~ ~ ~ ~ |
|Cost|… or … ㎏ of [gold](sc_price.md)|
|Development|…|
|Duration|Travel: 117 days<br> Lander: 50 min|
|Launch|27 March 1972, 04:15:01 UTC, Rocket: Molniya-M/MVL|
|Operator|Lavochkin|
|Programme|Venera programme|
|Similar to|・Proposed: [Venera 9 and 10](venera_9_10.md), [Vega 1 and 2](vega_1_2.md) <br> ・Current: …<br> ・Past: [Venera 5 and 6](venera_5_6.md), [Venera 7](venera_7.md)|
|Target|Exploring the planet Venus, studying Venus from inside the atmosphere and on the surface.|
|[Type](sc.md)|Atmospheric spacecraft; lander spacecraft|
|**【Spacecraft】**|~ ~ ~ ~ ~ |
|Comms|A funnel‑shaped antenna; a resetable antenna|
|Composition|Orbiter, lander|
|Contractor|…|
|[ID](spaceid.md)|NSSDC ID (COSPAR ID): 1972-021A (<https://nssdc.gsfc.nasa.gov/nmc/spacecraft/display.action?:id=1972-021A>), SCN: 5912 (<http://www.n2yo.com/satellite/?:s=05912>)|
|Manufacturer|Lavochkin|
|Mass|Lander: 495 ㎏ ([minisatellite](sc.md), [EVN‑070](venus.md))|
|Orbit / Site|Heliocentric|
|Payload|Temperature, pressure, and light sensors as well as an altimeter, gamma ray spectrometer, gas analyzer, and radio transmitters.|
|Power|…|

Achieved targets & investigations:

1. **T** — technical; **C** — contact research; **D** — distant research; **F** — fly‑by; **H** — manned; **S** — soil sample return; **X** — technology demonstration
1. **Sections of measurement and observation:**
   1. Atmospheric/climate — **Ac** composition, **Ai** imaging, **Am** mapping, **Ap** pressure, **As** samples, **At** temperature, **Aw** wind speed/direction.
   1. General — **Gi** planet’s interactions with outer space.
   1. Soil/surface — **Sc** composition, **Si** imaging, **Sm** mapping, **Ss** samples.

<small>

|**EVN‑XXX**|**T**|**EN**|**Section of m&o**|**D**|**C**|**F**|**H**|**S**|
|:--|:--|:--|:--|:--|:--|:--|:--|:--|
|EVN‑001| |Atmosphere: preliminary model.| |D|C|F| | |
|EVN‑002| |Surface: preliminary map.| |D|C| | | |
|EVN‑003|T|Exploration: from inside of atmosphere.| |D|C| | | |
|EVN‑006|T|Exploration: from surface.| | |C| | | |
|EVN‑010| |Atmosphere: vertical model.| |D| | | | |
|EVN‑011| |Atmosphere: common circulation model.| |D| | | | |
|EVN‑013| |Atmosphere: illumination of the surface & the atmo layers.| |D| | | | |
|EVN‑014| |Atmosphere: composition.| |D| | |F| |
|EVN‑017| |Atmosphere: structure.| |D| | | | |
|EVN‑026| |Surface: elemental composition.| | |C| | | |
|EVN‑070|T|Exploration with [satellites](sc.md): minisatellites.| |D| |F| | |
|EVN‑074| |Meteorological model.| |D| |F| | |

</small>



## Mission
The spacecraft took 117 days to reach Venus with one mid-course correction on 6 April 1972, separating from the orbiter and entering the atmosphere on 22 July 1972 at 08:37 UTC. Taking into account new tasks, the ballistics flight scheme of Venera 8 has changed. Unlike [Venera 4](venera_4.md), [-5, -6](venera_5_6.md), [-7](venera_7.md), the lander had to land on the day side of the planet.

A refrigeration system attached to the orbiter was used to pre‑chill the descent lander’s interior prior to atmospheric entry in order to prolong its life on the surface. Descent speed was reduced from 41 696 ㎞/h to about 900 ㎞/h by aerobraking. The 2.5 m diameter parachute opened at an altitude of 60 ㎞.

Venera 8 carried out accurate direct (in situ) measurements of atmospheric temperature and pressure, securely tied to the altitude ([EVN‑001](venus.md), [EVN‑010](venus.md)) above the surface and on the surface itself ([EVN‑003](venus.md), [EVN‑006](venus.md)). The chemical composition of the atmosphere was clarified ([EVN‑014](venus.md)). For the 1st time, the descending luminous flux was measured in the range from 55 ㎞ to the surface. These 1st high‑altitude profiles of sunlight flux proved to be sufficient to explain the nature of high temperature due to the greenhouse effect ([EVN‑001](venus.md)). They also made it possible for the 1st time to estimate the location of clouds in the atmosphere of Venus and to make the assumption about the presence of a sub-cloud haze ([EVN‑001](venus.md)). The illumination was measured on the surface ([EVN‑013](venus.md)), which was the basis for planning future experiments for taking panoramas. Altitude profiles of horizontal wind speed and direction from 55 ㎞ and to the surface were obtained by Doppler measurements ([EVN‑010](venus.md), [011](venus.md), [074](venus.md)).

**Winds**

|**Heigth**|**Wind speed**|
|:--|:--|
|50 ㎞|100 ㎧|
|45 ㎞|70 – 40 ㎧|
|45 – 20 ㎞|40 – 20 ㎧|
|10 – 0 ㎞|1 ㎧|

Measurements of the horizontal wind speed, almost coinciding with the speed of the ultraviolet inhomogeneities at the top of the clouds, indicated the presence of a superrotation of the atmosphere of Venus ([EVN‑034](venus.md)). Detailed study of high‑altitude temperature profiles and variations of the frequency signal allowed to obtain estimates of the nature of turbulence in the lower atmosphere of Venus ([EVN‑010](venus.md)).

Radio altimeter data were obtained at various altitudes. During the descent, the lander drifted 60 ㎞ horizontally. The radio altimeter obtained a surface profile ([EVN‑002](venus.md)) on which two mountains of 1 000 and 2 000 m high, a depression of 2 000 m deep and a gentle slope ascending to the landing site were found. Two intensity profiles of the echo signal were obtained, from which it was possible to calculate the permittivity and surface density, which turned out to be equal to 1.4 g/㎝³. According to the measurements of the photometer, the level of illumination monotonously decreased in the range from 50 to 35 ㎞ as the spacecraft passed through the clouds. Venera 8 was the 1st to detect three main optical regions in the atmosphere: the main thick cloud layer between 65 and 49 ㎞, a less dense layer of haze (fog) between 49 ㎞ and 32 ㎞, and a relatively clear cloudless atmosphere ([EVN‑017](venus.md)).

On the surface, the light level was almost constant, which supported the conclusion of a relatively clean atmosphere below the clouds. At the landing site, the illumination was comparable to twilight on a cloudy day on Earth. The weak brightness of the surface indicated that only 1 % of the incident sunlight reached the surface ([EVN‑013](venus.md)) (the Sun was 5° above the horizon).

Measurements using gas analyzers allowed us to conclude that the atmosphere contains 97 % carbon dioxide, 2 % nitrogen, 0.9 % water vapor and less than 0.15 % oxygen ([EVN‑014](venus.md)). Although the ammonia test gave a positive result of 0.1 % to 0.01 % at altitudes of 44 to 32 ㎞, this result was due to sulfuric acid, which also gave a positive reaction to the indicator used. An important circumstance was that the gas analyzer for the 1st time pointed out the possibility of the presence of sulfuric acid in the clouds. This explained why the clouds were so waterless but could still form droplets. And the fact that such droplets effectively reflected sunlight explained why the planet had such a high albedo.

The radio altimeter accurately measured the distance to the planet; from this data, a section of the terrain was built ([EVN‑002](venus.md)).

The on-board gamma ray spectrometer measured the uranium/thorium/potassium ratio of the surface rock, indicating it was similar to Alkali basalt ([EVN‑026](venus.md)).

Venera 8 reliably measured the atmosphere on the planet’s surface. Direct measurements showed that the surface pressure is 93 ± 1.5 ㍴ and temperature 470 ± 8 ℃, confirming the measurements of Venera 7 ([EVN‑074](venus.md)).

The lander continued to send back data for 50 minutes, 11 seconds after landing ([EVN‑006](venus.md)) before failing due to the harsh surface conditions.



## Science goals & payload
The purpose of the launch of the automatic station Venera 8 was to deliver the lander to the surface of Venus, to explore the planet Venus, to study Venus from inside the atmosphere and on the surface.

**Scientific tasks:**

1. **Orbital**
   1. Assessment of the radiation situation on the Earth − Venus flight path − **КС-18-4М**;
   1. Measurement of hydrogen (H₂) and deuterium (H₃) density in the upper atmosphere − **ЛА-4**.
1. **Lander**
   1. Measurement of the pressure of the planet’s atmosphere during parachute descent and on the surface after landing − **ИТД**, **МД**;
   1. Measuring the temperature of the planet’s atmosphere during parachute descent and on the surface after landing − **ИТД**;
   1. Determination of the maximum acceleration at the lander’s entry into the atmosphere − **ДОУ1-М**;
   1. Determination of the surface type of rocks of the planet (gamma spectrometer) − **ГС-4М**;
   1. Measurement of illumination at altitude and on the surface of the planet − **ИОВ-72**;
   1. Registration of ammonia in the cloud layer (verification of hypotheses about the possible presence) − **ИАВ-72**;
   1. Estimation of wind speed in the atmosphere − the onboard radio complex of the lander and ground installations were used.

|**Location**|**Instrument**|**Function**|**Mass, ㎏**|**Power, W**|
|:--|:--|:--|:--|:--|
|Orbiter|**КС-18-4М**|Instrument to study the flows of cosmic particles|≤1.6|2.0|
|Orbiter|**ЛА-4**|Measuring the density of H₂ and H₃|≤2.0|≤2.0|
|Lander|**ИТД**, **МД**|Temperature and pressure sensors|≤0.7|—|
|Lander|**ИТД**|Temperature and pressure sensors|0.2|4.0|
|Lander|**ДОУ1-М**|Accelerometer|0.25|—|
|Lander|**ГС-4М**|Gamma ray spectrometer|8.3|≤5.0|
|Lander|**ИОВ-72**|Cadmium sulfide Photometers|≤1.4|2.0 ‑ 3.0|
|Lander|**ИАВ-72**|Ammonia analyser|≤0.7|≤2.0|
|Lander|The onboard radio complex of the lander and ground installations|Radio Doppler experiment|—|—|

1. Radar altimeter;



## Spacecraft
The spacecraft based on the 3MV (Venera 3V; V-72) and consisted of a lander and an orbiter.

In order for the lander to get on the illuminated side, due to the restrictions imposed by the laws of celestial mechanics, it was necessary to recycle the radio system. A new funnel‑shaped antenna was installed (suitable for a low position above the Earth’s horizon). Also, the landing was equipped with a resetable antenna, providing communication after landing (flat disk with spiral antennas on each side). After landing, the resetable antenna is ejected from the parachute compartment, the petals open and do not allow it to stand on the edge. The ends of the disk are emitters, and the gravity switch installed inside it, after fixing the antenna to the radiation, turns on the upper side. The main funnel‑shaped antenna worked only in the descent area.

The data obtained by Venera 7 were taken into account in the design of  Venera 8 lander. In the lower part of the compartment, the wall thickness was reduce from 25 to 12 ㎜, in the upper part from 8.7 to 5.7 ㎜, which made it possible to reduce weight of the lander by 38.5 ㎏ compared Venera 7. The lander mass was 495 ㎏.

Porous composite material was used as the main thermal insulation of the lander. To improve the thermal conditions, beryllium shells were installed on the inner side of the titanium body of the lander, and fiberglass gaskets were introduced in the nodes of fastening the frame to the body of the instrument compartment. Maintaining the thermal regime inside the lander was carried out with the help of 「thermal batteries」, which were used as salt (lithium nitrate trihydrate). Outwardly projecting instruments have been closed with covers made of composite material.

In order to determine the illumination of the surface of the planet, necessary for photographing surface of Venus on the next generation lander, the complex of scientific equipment of the lander was supplemented with a photometer.



## Community, library, links

**PEOPLE:**

1. **Leaders:**
   1. [Georgiy Babakin](person.md) (Георгий Николаевич Бабакин) — chief design engineer.
   1. [Mstislav Keldysh](person.md) (Мстислав Всеволодович Келдыш) — a Soviet scientist in the field of mathematics and mechanics. He was part of the management of the flight of Venera programme.
1. **Members:**
   1. [Vladimir Dolgopolov](person.md) (Владимир Павлович Долгополов)

**COMMUNITY:**



## Docs/Links
|**Sections & pages**|
|:--|
|**【[](.md)】**<br> <mark>NOCAT</mark>|

1. Docs:
   1. П. С. Шубин — Венера. Неукротимая планета. Издание второе, расширенное и дополненное. М.: Издательство 「Голос‑Пресс」; Кемерово: издатель П. С. Шубин, 2018. – 352 стр.
1. <https://en.wikipedia.org/wiki/Venera_8>
1. <https://galspace.spb.ru/index491.html>


## The End

end of file
