# Space Robotics Lab
> 2022.03.23 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/s/srl_tohoku_logo1t.webp)](f/c/s/srl_tohoku_logo1.webp)|<yoshida@astro.mech.tohoku.ac.jp>, <mark>nophone</mark>, Fax …;<br> *980-8579 Aoba 6-6-01, Sendai, Japan*<br> <http://www.astro.mech.tohoku.ac.jp/e> ~~ [GitHub ⎆](https://github.com/Space-Robotics-Laboratory)|
|:--|:--|
|**Business**|R&D robotic space systems|
|**Mission**|Robotics for Space Science & Exploration Missions|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・Professor — Kazuya Yoshida|

The **Space Robotics Laboratory (Space Exploration Lab)** from Tohoku University, led by Professor Yoshida, is dedicated to the research & development of the robotic systems for space science & exploration missions. The lab has contributed to the Engineering Test Satellite-VII (launched in 1997 for orbital robotics experiments) & "Hayabusa" asteroid sample-return probe (launched in 2003 & expected to return in 2010). Today one of our focuses is put on the mechanics & control of lunar exploration rovers. Technologies for remote planetary exploration (such as mapping & localization in the unstructured environment, rough terrain mobility, & teleoperation with time delay) can also be applied to the robots for search & rescue missions.

<p style="page-break-after:always"> </p>

## The End

end of file
