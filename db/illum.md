# Освещённость
> 2019.10.05 [🚀](../../index/index.md) [despace](index.md) → [EF](ef.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Освещённость** — русскоязычный термин. **Illuminance** — англоязычный эквивалент.</small>

**Освещённость** — световая величина, равная отношению светового потока, падающего на малый участок поверхности, к его площади.



## Описание
Единицей измерения освещённости в Международной системе единиц (СИ) служит **люкс** (1 люкс = 1 люмену на m²), в СГС — фот (1 фот = 10 000 люкс). В отличие от освещённости, выражение количества света, отражённого поверхностью, называется светимостью.

Освещённость прямо пропорциональна силе света источника света. При удалении его от освещаемой поверхности её освещённость уменьшается обратно пропорционально квадрату расстояния (Закон обратных квадратов).

Когда лучи света падают наклонно к освещаемой поверхности, освещённость уменьшается пропорционально косинусу угла падения лучей.

Аналогом освещённости в системе энергетических фотометрических величин является облучённость.

Освещённость в фототехнике определяют с помощью экспонометров и экспозиметров, в фотометрии — с помощью люксметров.



## Примеры
|**Описание**|**Освещённость, лк**|
|:--|:--|
|Вне атмосферы на среднем расстоянии Земли от Солнца|135 000|
|Наибольшая солнечная освещённость при чистом небе|100 000|
|Обычная освещённость летом в средних широтах в полдень|17 000|
|В облачную погоду летом в полдень|12 000|
|При киносъёмке в студии|10 000|
|Обычная освещённость зимой в средних широтах|5 000|
|На футбольном стадионе (искусственное освещение)|1 200|
|На открытом месте в пасмурный день|1 000 ‑ 2 000|
|Восход и заход Солнца в ясную погоду|1 000|
|В светлой комнате вблизи окна|1 000|
|На рабочем столе для тонких работ|400 ‑ 500|
|На экране кинотеатра|85 ‑ 120|
|Необходимое для чтения|30 ‑ 50|
|В море на глубине 50 ‑ 60 м|до 20|
|Ночью в полнолуние|0.2|
|В безлунную ночь|0.001 ‑ 0.002|
|В безлунную ночь при сплошной облачности|до 0.0002|



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[External factors (EF)](ef.md)】**<br> [Astro.object](aob.md) ~~ [Atmosphere](atmosphere.md) ~~ [Atmosphere of Earth](earth.md) ~~ [Cosmic rays](ion_rad.md) ~~ [EMI](emi.md) ~~ [Grav.waves](gravwave.md) ~~ [Ion.radiation](ion_rad.md) ~~ [Radio frequency](comms.md) ~~ [Solar phenomena](solar_ph.md) ~~ [Space debris](sdeb.md) ~~ [Standart conditions](sctp.md) ~~ [Time](time.md) ~~ [VA radiation belts](ion_rad.md)|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Illuminance>


## The End

end of file
