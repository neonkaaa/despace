# SKY Perfect JSAT
> 2020.07.20 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/s/sky_perfect_jsat_logo1t.webp)](f/c/s/sky_perfect_jsat_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *Tokyo, Japan*<br> <https://www.skyperfectjsat.space>・ <https://www.jsat.net/en> ~~ [LI ⎆](https://www.linkedin.com/company/sky-perfect-jsat-holdings-inc/about) ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/SKY_Perfect_JSAT)|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

The **SKY Perfect JSAT Group** is a Japanese corporate group that claims to be Asia's largest [satellite](sc.md) communication & multi‑channel pay TV company. It owns the SKY PerfecTV! satellite broadcasting service & the SKY Perfect Well Think content studio, among other businesses.

**SKY Perfect JSAT Holdings Inc.** (株式会社スカパーJSATホールディングス, Kabushiki-gaisha Sukapā JSAT Holdings) (TYO: 9412) is the holding company for the group, & holds 100 % of the shares of SKY Perfect JSAT Corporation (スカパーJSAT株式会社, Sukapā JSAT Kabushiki-gaisha), the group's main operating company.

JSAT & SKY Perfect transferred their stock to a joint holding company, SKY Perfect JSAT Corporation, on 2007.04.02. SKY Perfect JSAT Corporation changed its name to SKY Perfect JSAT Holdings Inc. on 2008.06.27. The SKY Perfect JSAT Corporation name was re‑adopted by JSAT Corporation, SKY Perfect Communications & Space Communications Corporation when they merged on 2008.10.01.

Satellite fleet

- The JSAT constellation is a communication & broadcasting satellite constellation currently operated by SKY Perfect JSAT Group. It has become the most important commercial constellation in Japan, & the fifth of the world. It has practically amalgamated all private satellite operators in Japan, with only B-SAT left as a local competitor.
- It began in 1985 with the opening of the communication markets in Japan & the founding of Japan Communications Satellite Company, Satellite Japan Corporation, Space Communications Corporation. It grew by own investment, mergers & acquisitions of the parent companies. As of August 2016, in includes the fleets of three previously mentioned companies, Horizons Satellite & NTT DoCoMo & the DSN military network.



## The End

end of file
