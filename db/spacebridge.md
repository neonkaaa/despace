# SpaceBridge Inc.
> 2021.07.22 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/s/spacebridge_logo1t.webp)](f/c/s/spacebridge_logo1.webp)|<info@spacebridge.com>, +1(514)420-0045, Fax …;<br> *6300 Chemin de la Côte-de-Liesse, Saint-Laurent, QC H4T 1E3, Canada*<br> <https://www.spacebridge.com> ~~ [FB ⎆](https://www.facebook.com/SpaceBridgeCA) ~~ [LI ⎆](https://www.linkedin.com/company/spacebridgeinc) ~~ [X ⎆](https://twitter.com/SpaceBridgeCA)|
|:--|:--|
|**Business**|Satellite network equipment & services, incl. VSAT HUBs & Terminals for Point‑to‑Point, Point‑to‑Multi‑Point, & Mesh typologies, as well as SCPC & broadcast [modems](comms.md) for GEO & NGSO satellite constellations|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO — David Gelerman|

**SpaceBridge Inc.**, is an established supplier & global market leader in broadband satellite communications systems technology. Founded in 1988.

SpaceBridge Inc. also provides Cloud‑Based autonomous managed services for its customers, helping them to eliminate initial large CapEx investments & save on network management OpEX, while speeding time‑to‑market.

SpaceBridge Inc.’s diverse portfolio includes the ASAT™ product line, which serves different verticals with various technologies & applications.  Key areas of focus are: Cellular Backhaul for 2G/3G/4G & 5G, Industrial Internet of Things‑IIoT, Commercial & Military Satcoms‑On‑The‑Move, High‑Speed Broadband, Multicast IPTV, Voice‑over‑IP, Videoconferencing, L2/L3 VPN, Virtual Network Operator, & HD/UHD TV Broadcasting.

SpaceBridge Inc. 1st introduced its ASAT™  Wave Switch™ technology to the market in 2015.  This innovative, award‑winning technology enables dynamic return link selection & switching to the most‑appropriate waveform, whether MF‑TDMA, ASCPC or SCPC, thereby optimizing satellite resource usage for satellite networks & operators.

As new NGSO/LEO/MEO satellite constellations revolutionize the satellite market,  SpaceBridge Inc. is working in close partnership with New‑Space players to advance the satellite communications landscape by developing VSAT technology to exploit next‑gen leaps in capacity, & deliver 4G, 5G backhauling, IIoT & many other applications for our customers.

<p style="page-break-after:always"> </p>

## The End

end of file
