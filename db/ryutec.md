# RyuTeC
> 2022.06.29 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/r/ryutec_logo1t.webp)](f/c/r/ryutec_logo1.webp)|<hiro.nakamura@ryutec.co.jp>, 090-3802-3765>, Fax …;<br> *2-14 Wakabacho, Kashiwa City, Chiba 277-0024*<br> <http://www.ryutec.co.jp>|
|:--|:--|
|**Business**|Stratospheric solutions|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・Director — Yasuhiro Nakamura |

**RyuTeC Co., Ltd.** is a Japanese company aimed to provide stratospheric solutions. Founded 2003.

RyuTeC Co., Ltd. is a preparatory company for STRATOS Co., Ltd. (rocket launch venture), which aims to launch a satellite by launching a small rocket in the air from a stratospheric balloon. We aim to eliminate bottlenecks (access to space) in the space business.

<p style="page-break-after:always"> </p>

## The End

end of file
