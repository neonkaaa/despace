# External factors & Environment
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [EF](ef.md), [Interface](interface.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small> ・**External conditions** — EN term. **Внешние условия (ВУ)** — RU analogue.<br> ・**External factors (EF)** — EN term. **Внешние воздействующие факторы (ВВФ)** — RU analogue.</small>

**External factors (EF)** — mechanical, climatical, biological factors, ionizing radiation, outer environment during the operations, including such as:

**General**

1. Acoustic influence
1. Atmosphere
1. Catalysts
1. Pressure & Vacuum

**Electrical & electro­magnetic**

1. Electrical discharges
1. Electrical fields
1. [EMI](emi.md)
1. Gravity
1. [Illumination](illum.md)
1. [Magnetic field](mag_field.md)

**Mechanical**

1. Dust
1. Flowing liquids
1. Friction
1. Liquids
1. [Meteorites](aob.md)
1. Overloads, [Vibration & Shocks & Hits](vibration.md)
1. Surface roughness
1. Winds

**Radiation, Cosmic rays**

1. [Radiation](ion_rad.md)

**Thermal**

1. [Thermal](tcs.md) (Cold / Heat & their switches)



## Docs/Links
|**Sections & pages**|
|:--|
|**【[External factors (EF)](ef.md)】**<br> [Astro.object](aob.md) ~~ [Atmosphere](atmosphere.md) ~~ [Atmosphere of Earth](earth.md) ~~ [Cosmic rays](ion_rad.md) ~~ [EMI](emi.md) ~~ [Grav.waves](gravwave.md) ~~ [Ion.radiation](ion_rad.md) ~~ [Radio frequency](comms.md) ~~ [Solar phenomena](solar_ph.md) ~~ [Space debris](sdeb.md) ~~ [Standart conditions](sctp.md) ~~ [Time](time.md) ~~ [VA radiation belts](ion_rad.md)|

1. Docs: …
1. <…>


## The End

end of file
