# Гравитационные волны
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → **[](.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Гравитационные волны** — русскоязычный термин. **Gravitational wave** — англоязычный эквивалент.</small>

**Гравитацио́нные во́лны** — изменения гравитационного поля, распространяющиеся подобно волнам. Излучаются движущимися массами, но после излучения отрываются от них и существуют независимо от этих масс. Математически связаны с возмущением метрики пространства‑времени и могут быть описаны как 「рябь пространства‑времени」.



## Описание
<mark>TBD</mark>



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[External factors (EF)](ef.md)】**<br> [Astro.object](aob.md) ~~ [Atmosphere](atmosphere.md) ~~ [Atmosphere of Earth](earth.md) ~~ [Cosmic rays](ion_rad.md) ~~ [EMI](emi.md) ~~ [Grav.waves](gravwave.md) ~~ [Ion.radiation](ion_rad.md) ~~ [Radio frequency](comms.md) ~~ [Solar phenomena](solar_ph.md) ~~ [Space debris](sdeb.md) ~~ [Standart conditions](sctp.md) ~~ [Time](time.md) ~~ [VA radiation belts](ion_rad.md)|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Gravitational_wave>
1. 2017.09.28 Хабр: Обсерватории LIGO и Virgo зарегистрировали ещё одну гравитационную волну (<https://habr.com/ru/post/370855>) — [archived ❐](f/archive/20170928_1.pdf) 2019.02.12


## The End

end of file
