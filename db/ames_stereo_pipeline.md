# Ames Stereo Pipeline
> 2019.09.13 [🚀](../../index/index.md) [despace](index.md) → **[Soft](soft.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Ames Stereo Pipeline (ASP)** — англоязычный термин, не имеющий аналога в русском языке. **Стереотруба центра Эймса** — дословный перевод с английского на русский.</small>

The **NASA Ames Stereo Pipeline (ASP)** is a suite of free & open source automated geodesy & stereogrammetry tools designed for processing stereo imagery captured from satellites (around Earth & other planets), robotic rovers, aerial cameras, & historical imagery, with & without accurate camera pose information. It produces cartographic products, including [digital elevation models (DEMs)](draw.md), ortho‑projected imagery, 3D models, & bundle‑adjusted networks of cameras. ASP’s data products are suitable for science analysis, mission planning, & public outreach.

The Stereo Pipeline is part of the NASA [NeoGeography Toolkit](neogeography_toolkit.md).



## Описание
The Ames Stereo Pipeline (ASP) was developed by the Intelligent Robotics Group (IRG), in the Intelligent Systems Division at the National Aeronautics and Space Administration (NASA) Ames Research Center in Moffett Field, CA. It builds on over ten years of IRG experience developing surface reconstruction tools for terrestrial robotic field tests and planetary exploration.

Project Lead

1. Dr. Ross Beyer (NASA/SETI Institute)

Development Team

1. Oleg Alexandrov (NASA/Stinger-Ghaffarian Technologies)
1. Scott McMichael (NASA/Stinger-Ghaffarian Technologies)

Former Developers

1. Zachary Moratto (NASA/Stinger-Ghaffarian Technologies)
1. Michael J. Broxton (NASA/Carnegie Mellon University)
1. Dr. Ara Nefian (NASA/Carnegie Mellon University)
1. Matthew Hancher (NASA)
1. Mike Lundy (NASA/Stinger-Ghaffarian Technologies)
1. Vinh To (NASA/Stinger-Ghaffarian Technologies)

Contributing Developer & Former IRG Terrain Reconstruction Lead

1. Dr. Laurence Edwards (NASA)



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Software](soft.md)】**<br> [ASP](asp.md) ~~ [Blender](blender.md) ~~ [C](plang.md) ~~ [Cosmographia](cosmographia.md) ~~ [DOORS](doors.md) ~~ [DWG](cad_f.md) ~~ [GIMP](gimp.md) ~~ [Git](git.md) ~~ [IGES](cad_f.md) ~~ [ISIS](isis.md) ~~ [JT](cad_f.md) ~~ [NGT](neogeography_toolkit.md) ~~ [NX](nx.md) ~~ [Octave](gnu_octave.md) ~~ [OS](os.md) ~~ [PDF](pdf.md) ~~ [Python](plang.md) ~~ [R](plang.md) ~~ [SPICE](spice.md) ~~ [STEP](cad_f.md) ~~ [STL](stk.md) ~~ [SVG](cad_f.md) ~~ [Syncthing](syncthing.md) ~~ [SysML](sysml.md) ~~ [Teamcenter](teamcenter.md) ~~ [Valispace](valispace.md) ~~ [Система управления версиями](vcs.md) ~~ [ХРИП](adra.md)|

1. Docs: …
1. <https://ti.arc.nasa.gov/tech/asr/groups/intelligent-robotics/ngt/stereo>



## The End

end of file
