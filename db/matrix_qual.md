# Матрица квалификация
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → **[Док.](doc.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Матрица квалификации** — русскоязычный термин. **Qualification matrix** — англоязычный эквивалент.</small>

**Матрица квалификации**, иногда неверно **матрица [верификации](vnv.md)** — [документ](doc.md), наглядно отражающий поэтапно состояние и результаты создания и отработки [КА и его СЧ](sc.md), соответствие проведённых работ и их результатов требованиям [ТЗ](tor.md) и контракта, содержащимся в документе 「Требования по квалификации и приёмке」.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[](.md)】**<br> <mark>NOCAT</mark>|

1. Docs:
   1. [ГОСТ ИСО 9001](gost_iso9001.md)
   1. [Основы управления надёжностью КА с ДСЭ ❐](f/doc/osnovy_upravleniya_nadejnostiu_2015.pdf), со стр. 144
1. <…>


## The End

end of file
