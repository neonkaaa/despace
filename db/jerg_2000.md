# JERG-2-000
> 2013.05.29 [🚀](../../index/index.md) [despace](index.md) → [Doc](doc.md), [R&D](rnd.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**JERG-2-000 Spacecraft Design Standard** — EN term. **ЖЕРГ-2-000 Стандарт проектирования космического аппарата** — literal RU translation.</small>

JERG-2-000A  
SPACECRAFT DESIGN STANDARD  
March 29, 2013 Revision A  
[Japan Aerospace Exploration Agency](jaxa.md)

This is an English translation of JERG-2-000. Whenever there is anything ambiguous in this document, the original document (the Japanese version) shall be used to clarify the intent of the requirement.

*Disclaimer*  
The information contained herein is for general informational purposes only. JAXA makes no warranty, express or implied, including as to the accuracy, usefulness or timeliness of any information herein. JAXA will not be liable for any losses relating to the use of the information.

Published by  
Japan Aerospace Exploration Agency  
Safety & Mission Assurance Department  
2-1-1 Sengen Tsukuba-shi, Ibaraki 305-8505, Japan


## JAXA Spacecraft Design Standards Tree
> <small>**JAXA Spacecraft Design Standards Tree** — EN term. **Список стандартов проектирования космических аппаратов от ДЖАКСА** — literal RU translation.</small>

1. **Level 1**: [JERG-2-000](jerg_2000.md) — Spacecraft design
   1. **Level 2**: JERG-2-100 — Spacecraft system design
      1. **Level 3**: JERG-2-120 — Protection for single point of failure
      1. JERG-2-130 — General test standard for spacecraft
      1. JERG-2-141 — Space environment
      1. JERG-2-142 — General environment (for spacecraft)
      1. JERG-2-143 — Space environment effects mitigation design
      1. JERG-2-144 — Evaluation for protection against inpact of tiny debris
      1. JERG-2-151 — Mission and orbit design
      1. JERG-2-152 — Management standard of spcecraft internal disturbances
   1. JERG-2-200 — Electrical design
      1. JERG-2-211 — Spacecraft charging and discharging
      1. JERG-2-212 — Wire derating
      1. JERG-2-213 — Spacecraft insulation design
      1. JERG-2-214 — Power subsystem design
      1. JERG-2-215 — Solar array paddle design
      1. JERG-2-241 — EMC design
   1. JERG-2-300 — Mechanical design
      1. JERG-2-310 — Spacecraft thermal control system
      1. JERG-2-311 — Prevention for detaching of the MLI
      1. JERG-2-320 — Structural design
      1. JERG-2-330 — Mechanical design
      1. JERG-2-340 — Spacecraft propulsion design
   1. [JERG-2-400](jerg_2400.md) — Communication design
      1. JERG-2-410 — RF communication design
      1. JERG-2-420 — RF telecommunication link design
   1. [JERG-2-500](jerg_2500.md) — Control design
      1. JERG-2-510 — Attitude control system design
   1. JERG-2-600 — Software development
      1. JERG-2-610 — Spaceraft software development
   1. JERG-2-700 — Spacecraft operation



## 1. Scope

The primary purpose of this standard is to provide the technical information necessary for designing & developing satellites & probes in the [Japan Aerospace Exploration Agency](jaxa.md) (hereinafter referred to as 「JAXA」). Much of the technical information on the design & development of spacecraft program generally does not depend on the development [organizations or companies](contact.md). Therefore, this standard was established with the expectation of being applied not merely in JAXA but in other organizations & companies for the [designing & development](rnd.md) of [spacecraft](sc.md) programs.

This standard refers to the purpose of the mission, establishment & system design of requirements & specification, product definition, manufacturing, verification, operation & termination of the JAXA‑developed spacecraft systems. As these activities are implemented not only by JAXA but also by the cooperative institutes & contractors, it is vital to properly conform to the activities performed by JAXA such as [systems engineering](se.md), & safety & mission assurance etc.

This standard was developed based on the technical information obtained from accumulated experience, data & research achievements in JAXA (former NASDA, ISAS & NAL) with the intention to be consistent with the Aerospace Standard promoted by the International Organization for Standardization (ISO) as much as possible.



## 2. Related documents

**2.1 Applicable Documents**

Safety & Mission Assurance Department Director Notification 16‑1 Management procedure for the technical standards.



## 3. Definition of terms

1. **[Spacecraft](sc.md)** — The general term of unmanned satellites & space probes.
1. **[Design Standard](doc.md)** — A document providing the standard technical information for designing spacecrafts. The technical information is gathered through past experience, research & analysis & is most rational at the time.
1. **[Mission](project.md)** — The purpose of launching spacecrafts.
1. **System** — An assembly of hardware & software to achieve the specified mission. It may include human elements.
   1. **Subsystem** — An element of a system. A subsystem is an assembly of hardware & software which provides a certain specified function or performance.
      1. **Component** — An element of system or subsystem. A component is composed of some parts, devices & structures to have a certain function.
         - **Part** — A single or a combination of multiple parts which generally could not be disassembled without destroying it.
1. **[Mission Assurance](qm.md)** — An operation action performed throughout the development & operation of spacecraft in order to ensure the mission success.
1. **[Reliability](qm.md)** — A property that achieves mission success within the specified mission period under the space environment.
1. **[Safety](qm.md)** — The property that prevents spacecrafts & its components from inflicting damage on human during the development, launch & operation of the spacecrafts & components.
1. **[Systems Engineering](engineeer.md)** — A series of technical methodology & activity to define the spacecraft mission & the spacecraft system which can achieve the specified mission, & to promote the development under the constraints of launch period & development budget.
1. **Program** — An overall systematic program & set of projects to achieve specific purposes or missions.
1. **[Project](project.md)** — A fixed‑term activity set as a means to achieve the mission & implemented by the time‑limited organization with the specific resources & time constraints.
1. **[Design Review](design_review.md)** — A formal review performed at an appropriate time during the design work or evaluation tests in order to confirm that the design appropriately meets the requirements & to ensure the transition to the next stage.
1. **[Development Specification](specification.md)** — A specification created in the beginning of design phase in order to specify the requirements for the design, manufacturing, test & evaluation of the item.
1. **[Termination](rnd.md)** — The act of disposal or collection of spacecraft appropriately implemented at the end of its operation.



## 4. Relationship between lifecycle of spacecraft & design standard

The life cycle of a spacecraft starts with defining its mission, then moves into the design, manufacturing & launch of the spacecraft to realize the mission, followed by the operation in space & finally ends with the termination after operation.

Designing is an activity to analyze the defined mission requirements & determine the physical & functional properties of various elements such as spacecraft systems, subsystems & components to realize the mission under the constraints of launch period & development budget. Among these design activities, design standard aims to be utilized in the system definition & the design of subsystems & components. For designing the subsystems & components, it is important to give due consideration to the subsequent activities of manufacturing, tests & operation, mission accomplishment, ensuring of safety & reliability & mission assurance.



## 5. Meaning & structure of design standard
A design standard is a document providing the standard technical information for designing spacecrafts. The technical information is gathered through past experience, research & analysis & is most rational at the time. In other words, it is not just a document structure but a scheme to correct & analyze information & keep giving feedbacks after the necessary tests & evaluations. Therefore, there shall be a scheme to reflect the experience, development, investigation, evaluation & acquired results from individual projects into the design standard.

As the design standard was created based on past experiences & research achievements, the understanding of the standard will avoid overlapping investment in data acquisition & prevent the occurrence of similar failures, which accordingly contributes to the reduction of development period & cost.

The document structure of design standard is organized into four levels as described below.

1. **Level 1**: A top level document defining the overall policy & structure of whole design standards. The identification number is JERG-2-000.
1. **Level 2**: The level 2 documents define the general requirements, structure & specific technical requirements in the specific technical fields. The design standards for the specific technical fields shall be as follows.
   1. System Design Standard (ID No.: JERG-2-100)
   1. Electrical Design Standard (ID No.: JERG-2-200)
   1. Mechanical System Design Standard (ID No.: JERG-2-300)
   1. Communication Design Standard (ID No.: [JERG-2-400](jerg_2400.md))
   1. Control System Design Standard (ID No.: [JERG-2-500](jerg_2500.md))
   1. Software Development Standard (ID No.: JERG-2-600)
   1. Operational Design Standard (ID No.: JERG-2-700)
1. **Level 3**: The level 3 documents define requirements for specific technical matters. ID numbers shall be assigned by adding numbers specific for each technical matter to the ID numbers assigned for the level 2 documents.
1. **Level 4**: The level 4 documents provide explanations, procedures, tools, data & related information of specific technical matters to be the compliment of the above documents. ID number shall be assigned as specified in level 3.



## 6. Application of design standard
As specified in chapters 3 & 5, the design standards provide the most rational technical information to be a design guideline or engineering method for the spacecraft development. Therefore, it shall be properly utilized for the development of spacecraft programs. More specifically, the technical information contained in design standards shall be fully understood, including its backgrounds & limitations, & utilized in the manner most appropriate for the development conditions of each project.

During the activities in the definition phase at the early stage of project (prior to [System Design Review (SDR)](design_review.md)), it is important to consider the utilization of design standards. Especially, it is essential to understand the useful information to determine whether the development is achievable with the existing technology or to define which technology shall be newly developed & acquired.

After the project starts (following to the SDR), development shall be promoted considering the utilization of design standards in accordance with the development policy. Since the required mission & its scale differ from project to project, it is important to select specific design standards or part of the standard appropriate for each project based on the requirements of mission or others.

In particular, if the application of design standards (or part of the standards) is decided after considering the utilization of each design standards, the design standards shall be specified as applicable documents in the preceding documents in the project document structure such as development specification, development specification (draft) or system design document (hereinafter correctively referred to as 「development specifications」). Alternatively, if the design requirement, design criterion or other equivalent document (hereinafter correctively referred to as 「design criteria to be specified as applicable document in the development documents are prepared in each project, the technical information contained in the design standard shall be properly reflected in the design criteria. If there are any adequate design grounds (i.e. other design standards or new insights given by recent experiments) other than the standards in this design standard structure, they can be reflected in the design criteria used in the project. It shall be recognized that following up the process for utilizing design standards in writing is valuable in promoting projects or maintaining the design standards based on the latest technologies.

During design reviews conducted in each phase by JAXA or contractor involved in projects, the adequacy of the application of the design standard or its utilization through the design criteria shall be reviewed.

In the review at the termination stage, the original design is reviewed as well as the accuracy of its operation. As a result of the application of the design standard or its operation through the design criteria, the revision of existing design standards or establishment of new design standards shall be proposed. This proposal is essential activities to promote & maintain the proper utilization of design standards.



## Docs/Links
|**Sections & pages**|
|:--|
|**【[Documents](doc.md)】**<br> **Схема:** [КСС](ксс.md) ~~ [ПГС](пгс.md) ~~ [ПЛИС](плис.md) ~~ [СхД](draw.md) ~~ [СхО](draw.md) ~~ [СхПЗ](draw.md) ~~ [СхЧ](draw.md) ~~ [СхЭ](draw.md)<br> [Interface](interface.md) ~~ [Mission proposal](proposal.md)|

1. Docs: [JERG-2-000 (PDF) ❐](f/doc/jp/jerg_2000.pdf), [Spacecraft Design Standards Tree (PDF) ❐](f/doc/jp/jerg_scguide.pdf)
1. <…>


## The End

end of file
