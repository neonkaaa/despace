# CCERA
> 2020.06.28 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/c/ccera_logo1t.webp)](f/c/c/ccera_logo1.webp)|<info@ccera.ca>, <mark>nophone</mark>, Fax …;<br> *Canadian Centre for Experimental Radio Astronomy, 361 Queen Street, Building 510, Suite 204, Smiths Falls, Ontario, K7A 0A6, Canada. Postal address: P.O. Box 862, Smiths Falls, ON, K7A 4W7*<br> <http://www.ccera.ca>・ <https://github.com/ccera-astro>|
|:--|:--|
|**Business**|Supporting education & research in radio astronomy techniques & applications|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Canadian Center for Experimental Radio Astronomy (CCERA)** is a membership association which supports education & research in radio astronomy techniques & applications targeted at smaller institutions & interested individuals. We are federally incorporated as a not‑for‑profit in Canada. We operate an observatory & research centre near Smiths Falls, Ontario, Canada. We have a scientific advisory board who provide direction & advice on an ad‑hoc basis.

**Advisory Board:**

- **Marcus Leech, Director.** Marcus has worked in the technology development industry in both software & hardware, since 1979. He spent 19 years at Nortel, incl. 8 years in the office of the [CTO](mgmt.md) as an advisor on network security standards. He was involved in the IETF for many years, incl. chairing several working groups, & spent four years as security‑area director in that organization. He is the author of several IETF RFCs.<br> Marcus has several patents in the areas of cryptography & network security.<br> He has been involved in small‑scale radio astronomy since the late 1980s, & in 2004 began investigating & 「evangelizing」 Software Defined Radio for use in radio astronomy experiments. His papers on the subject can be found in the proceedings of our sister organization, The Society of Amateur Radio Astronomers.<br> Marcus is a licensed amateur radio operator, with callsign VE3MDL
- **Gary Atkins, Director.** Gary is a veteran of the aerospace industry, having spent most of his career in Space Hardware Integration & Testing activities for the [Canadian Space Agency](csa.md). He was involved in a range of areas from Space Simulation to Structural Qualification testing on most Canadian space programs & numerous international programs.<br> Programs ranged from Anik C series of satellites to the Radarsat series, & from MOST & SCISAT to the fine‑guidance system of the James Webb Space Telescope.<br> Gary was the driving force behind getting access to a large earth‑station dish, owned by the CSA, & which was the centerpiece of the joint project at Shirleys Bay. The project came to an end in 2013 when the CSA decided to dismantle that dish, to make room for other tenants of the same campus.<br> Currently Gary is pursuing Astronomy, Radio Astronomy, Meteorite tracking, Astrogeology, geology & early mineral exploration as interests in his retirement.



## The End

end of file
