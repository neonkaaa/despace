# CSIRO
> 2021.04.23 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/c/csiro_logo1t.webp)](f/c/c/csiro_logo1.webp)|<mark>noemail</mark>, +61 3 9545 2176 , Fax …;<br> *Building 101, Clunies Ross Street, Black Mountain ACT 2601, Australia*<br> <http://www.csiro.au> ~~ [FB ⎆](https://www.facebook.com/CSIROnews) ~~ [IG ⎆](https://www.instagram.com/csirogram) ~~ [LI ⎆](https://www.linkedin.com/company/csiro) ~~ [X ⎆](https://twitter.com/csiro) ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/CSIRO)|
|:--|:--|
|**Business**|Australian national science agency|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

The **Commonwealth Scientific & Industrial Research Organisation (CSIRO)** is an Australian Government agency responsible for scientific research.


<p style="page-break-after:always"> </p>


## The End

end of file
