# OSTIn
> 2022.04.04 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/o/ostin_logo1t.webp)](f/c/o/ostin_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *250 North Bridge Road #28-00 Raffles City Tower, 179101 Singapore*<br> [FB ⎆](https://www.facebook.com/OSTInSingapore) ~~ [LI ⎆](https://sg.linkedin.com/company/ostinsingapore)|
|:--|:--|
|**Business**|Singapore space program|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・Senior Vice President & Head — David TAN Cheow Beng|

The **Office for Space Technology & Industry (OSTIn)** is Singapore’s national space office. IAF member since 2021.

OSTIn is responsible for:

- Nurturing the development of space technologies to serve national imperatives;
- Growing a globally competitive space industry in Singapore;
- Fostering an enabling regulatory environment for Singapore’s space activities;
- Establishing international partnerships & contributing to the development of multilateral norms on space;
- Supporting the development of Singapore’s future workforce through space‑based STEM outreach.

<p style="page-break-after:always"> </p>

## The End

end of file
