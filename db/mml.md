# Уровни зрелости управления
> 2019.07.25 [🚀](../../index/index.md) [despace](index.md) → [Control](control.md), [SE](se.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Modeling Maturity Levels (MML)** — EN term. **Уровни зрелости управления (УЗУ)** — RU analogue.</small>

**Уровни зрелости управления** (англ. Modeling Maturity Levels) — этапы развития организации в соответствии со стандартизованными моделями оценки уровня зрелости управления. Проходятся каждой организацией последовательно и определяются различными характеристиками, включающими миссию, ценности, стратегию, организационную структуру. Переходы с уровня на уровень делают организацию более конкурентоспособной.

1. Универсальные модели оценки уровня зрелости управления:
   1. CMMI — Capability Maturity Model Integration — набор моделей (методологий) совершенствования процессов в организациях разных размеров и видов деятельности
1. В сфере информационных технологий:
   1. CMM — Capability Maturity Model — модель зрелости возможностей создания ПО
   1. ГОСТ Р ИСО/МЭК 15504-xx Информационные технологии. Оценка процесса (англ.: ISO/IEC 15504),
   1. Серия стандартов на базе ГОСТ Р ИСО/МЭК 33001 "Информационные технологии - оценка Процесса" (англ.: ISO/IEC 33001),
1. В сфере управления проектами:
   1. P3M3 — Portfolio, Programme and Project Management Maturity Model — Модель зрелости управления портфелями, программами и проектами
   1. OPM3 — Organizational Project Management Maturity Model — Модель зрелости организационного управления проектами

**Modeling Maturity Levels**

|**Level**|**1) Ad Hoc, Chaotic**|**2) Managed**|**3) Standardized**|**4) Semi-Integrated**|**5) Integrated & Optimizing**|
|:--|:--|:--|:--|:--|:--|
|**Focus**|Just get it done|Plan & manage|Standard, repeatable process|Reduce costs|Automate & integrate|
|**Strategy**|Individual heroics|Outside counsel manages many vendors|Vetted & trusted vendor(s)|Legal manages blend of vendor and in‑house resources|Legal & Tech manage in-house with strategic vendor use|
|**Expertise**|None|Expert at outside counsel|Expert at trusted vendor|Single expert in either legal or tech|Expert team of legal & tech|
|**Costs**|Surprising|Overruns, unexpected costs|Controlled|Targeted reductions|Shared costs, mainly with tech|



## Список уровней

Совершенствование процессов — поэтапный/плавный процесс. В CMMI, CMM, ISO 15504, P3M3 существует 5 уровней зрелости процессов организации. **「+」** — означает, что по мере повышения уровня зрелости управления, органично добавляется соответствующий дополнительный стиль управления, при этом, добавляемый стиль управления базируется на предыдущих стилях управления, не противоречит им, не исключает их, но накладывает дополнительные ограничения и условия.

|**Уровень зрелости управления**|**0. Отсутст&shy;вующий**|**1. Начальный**|**2. Управля&shy;емый, Повторя&shy;емый**|**3. Определя&shy;емый, Стандарти&shy;зуемый**|**4. Измеряемый**|**5. Улучша&shy;емый, Оптими&shy;зируемый**|
|:--|:--|:--|:--|:--|:--|:--|
|**Описание с позиции процессов**|Процессы непредска&shy;зуемые, неконтроли&shy;руемые. Не появляются в ответ на определённые события.|Процессы непредска&shy;зуемые, слабо контроли&shy;руемые. Появляются в ответ на определённые различные события.|Процессы определены на уровне проектов. Зачастую появляются в ответ на определённые события|Процессы определены на уровне организации. Исполняются заранее. Конструи&shy;руются от начала (от источников) к результату (к потребителю)|Процессы измеряются и контролируются. Конструи&shy;руются 「наоборот」 — от ожидаемого результата (от потребителя) к началу (к источникам)|Фокус на совершен&shy;ствование процессов|
|**Предска&shy;зуемость результата**|Не может достичь результата|Может достичь результата|Может достичь результата в срок|Может достичь результата<br> в срок и качественно|Может в срок, качественно и в рамках заранее определяемого бюджета|Может в срок, качественно, в рамках заранее определяемого бюджета, с долгосрочным перспективным лидерством фирмы на рынке|
|**Возможные риски**|Макс. риск|Возможно не в срок, некачественно, с превышением бюджета|Возможно некачественно, с превышением бюджета|Возможно с превышением бюджета|Мин. риски в тактической перспективе. Возможны — в стратегической|Мин. риски в тактической и стратегической перспективах|
|**Кредитный рейтинг**|D|C, CC|CCC, B|BB, BBB|A, AA|AAA, AAA+|
|**Характ. методы управления**|нет|+ Ситуационное управление<br> (「[Ad hoc](ad_hoc.md)」)<br>+ Контроль исполнения поручений|+ Управление проектами<br> + Тайм‑менеджмент|+ Управление процессами<br> + Управление качеством|+ Управление по целям (Стратегическое управление)<br> + Управление по показателям|+ Управление знаниями<br> + Управление инновациями<br> + Управление изменениями|
|**Характ. орг. структура**|нет|+ Иерархи&shy;ческая структура|+ Проектные команды|+ Конвейеры|+ Матричная орг. структура|+ Сетевая орг. структура|
|**Характ. вопросы**|нет|Кто (делает)?|Где (делать)?<br> Когда (делать)?|Как (делать)?<br> Что (обраба&shy;тывается, должно получиться)?|Зачем (всё это делать)?<br> Сколько (делать)?|Почему (это происходит)?<br> Как иначе (это сделать)?<br> Что модерни&shy;зировать?|
|**Характ. мотивация**|нет|Кто главнее?|Кто быстрее?|Кто качественнее?|Кто эффективнее?|Кто изобрета&shy;тельнее?|



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Control](Control.md)】**<br> [Ad hoc](ad_hoc.md) ~~ [Business travel](business_travel.md) ~~ [Chief designers council](cocd.md) ~~ [CML](trl.md) ~~ [Competence](competence.md) ~~ [Confident](confident.md) ~~ [Consp.theory](consp_theory.md) ~~ [Control sys. (CS)](cs.md) ~~ [Coordinate system](coord_sys.md) ~~ [Curator](curator.md) ~~ [Designer’s supervision](des_spv.md) ~~ [E‑sig](esig.md) ~~ [Engineer](se.md) ~~ [Errand](errand.md) ~~ [Federal law](fed_law.md) ~~ [Federal TP](fed_tp.md) ~~ [Federal SP](fed_sp.md) ~~ [GNC](gnc.md) ~~ [Gravity assist](gravass.md) ~~ [Industrial archaeology](ind_arch.md) ~~ [Instruction](instruction.md) ~~ [Lean manuf.](lean_man.md) ~~ [Lifetime](lifetime.md) ~~ [Manager](manager.md) ~~ [MBSE](se.md) ~~ [Meeting](meeting.md) ~~ [MCC](sc.md) ~~ [MIC](mic.md) ~~ [MML](mml.md) ~~ [MoU](contract.md) ~~ [Nav. & ballistics (NB)](nnb.md) ~~ [Nonprofit org.](nonprof_org.md) ~~ [NX](nx.md) ~~ [Oberth effect](oberth_eff.md) ~~ [Org.structure](orgstruct.md) ~~ [Outcomes commission](outccom.md) ~~ [Patent](patent.md) ~~ [Peter prin.](peter_principle.md) ~~ [Plan](plan.md) ~~ [PMBok](pmbok.md) ~~ [Quorum](quorum.md) ~~ [R&D management](mgmt.md) ~~ [R&D support](rnd_support.md) ~~ [Recursion](recurs.md) ~~ [Schulze_method](schulze_method.md) ~~ [Sci'N'Tech activities](st_act.md) ~~ [Sci'N'Tech council](satc.md) ~~ [Single-window system](sw_sys.md) ~~ [Situ.leadership](situ_leadership.md) ~~ [Skunk works](se.md) ~~ [State arm. plan](plan_sa.md) ~~ [Swamp](swamp.md) ~~ [Teamcenter](teamcenter.md) ~~ [Tennis racket theorem](tr_theorem.md) ~~ [TRIZ](triz.md) ~~ [TRL](trl.md) ~~ [V‑model](v_model.md) ~~ [Veto](veto.md) ~~ [Workflow](workflow.md) ~~ [Workgroup](wg.md)|
|**【[Systems engineering](se.md)】**<br> [Competence](competence.md) ~~ [Coordinate system](coord_sys.md) ~~ [Designer’s supervision](des_spv.md) ~~ [Industrial archaeology](ind_arch.md) ~~ [Instruction](instruction.md) ~~ [Lean manuf.](lean_man.md) ~~ [Lifetime](lifetime.md) ~~ [MBSE](se.md) ~~ [MML](mml.md) ~~ [Nav. & ballistics (NB)](nnb.md) ~~ [NASA SEH](book_nasa_seh.md) ~~ [Oberth effect](oberth_eff.md) ~~ [PMBok](pmbok.md) ~~ [Quorum](quorum.md) ~~ [R&D management](mgmt.md) ~~ [R&D support](rnd_support.md) ~~ [Recursion](recurs.md) ~~ [Schulze_method](schulze_method.md) ~~ [Sci'N'Tech activities](st_act.md) ~~ [Sci'N'Tech council](satc.md) ~~ [Skunk works](se.md) ~~ [SysML](sysml.md) ~~ [Tennis racket theorem](tr_theorem.md) ~~ [TRIZ](triz.md) ~~ [TRL](trl.md) ~~ [V‑model](v_model.md) ~~ [Workflow](workflow.md) ~~ [Workgroup](wg.md)|

1. Docs: …
1. [Руководитель](manager.md)
1. <https://en.wikipedia.org/wiki/Modeling_Maturity_Levels>


## The End

end of file
