# Amanogi Co., Ltd.
> 2021.12.06 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/a/amanogi_logo1t.webp)](f/c/a/amanogi_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *1-1-13, Kitamagome, Ota-ku, Tokyo, Japan*<br> <https://amanogi.space> ~~ [X ⎆](https://twitter.com/AmanogiPR)|
|:--|:--|
|**Business**|Sensors & advanced sat data analysis solutions|
|**Mission**|…|
|**Vision**|Utilization of Space data by applying Edge Computing to satellites|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO, Founder — Yu Kudo|

**Amanogi Co., Ltd.** (also **Amanogi Space**) is a Japanese company aimed to design & production of space components, modeling devices, & visual/sound devices, R&D of data analysis & visualization technology, education services. Founded 2016.

We developed a STT (STar Tracker) that combines a high‑performance camera with a computing module.  The camera photographs the stars in the sky from the point of view of the satellite in space. The computing module extracts the stars from each image & finds accurate matches in the star coordinate catalog to estimate the attitude of the satellite in the star coordinate frame. We aim to reduce the price & development time to less than half that of conventional systems, & bring it to market as a turn‑key solution.

- **Development of AI Satellite.** Research & Development of Edge Computing that will be realize small & low power for AI & Data analyses.
- **Autonomous operation.** The Satellite itself can detect the target, it enables observation automation & tracking of moving objects.
- **Real‑time analysis & work saving.** Acquisition data can be immediately analyzed on the satellite, & further analysis results can be transmitted.
- **Technology demonstration using short‑term & long‑term observational statistical data.** The satellite recognizes the ground station, & attitude control for realizing optical communication & means for avoiding clouds are realized by AI satellite.

<p style="page-break-after:always"> </p>

## The End

end of file
