# Kyocera
> 2020.07.22 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/k/kyocera_logo1t.webp)](f/c/k/kyocera_logo1.webp)|<mark>noemail</mark>, +81-75-604-3500, Fax +81-75-604-3501;<br> *6 Takeda Tobadono-cho, Fushimi-ku, Kyoto-shi, Kyoto, 612-8501 Japan*<br> <https://global.kyocera.com> ~~ [LI ⎆](https://www.linkedin.com/company/kyocera-global) ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/Kyocera)|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Kyocera Corporation** (京セラ株式会社, Kyōsera Kabushiki-gaisha, pronounced kʲoːseɾa) is a Japanese multinational ceramics & electronics manufacturer headquartered in Kyoto, Japan. Founded in 1959.

It manufactures industrial ceramics, solar power generating systems, telecommunications equipment, office document imaging equipment, electronic components, semiconductor packages, cutting tools, & components for medical & dental implant systems.

- Main Products (Corporate Fine Ceramics Group):
   - machine parts,
   - parts for information & communication equipment,
   - ceramics for electronic parts,
   - parts for ultra-high vacuum devices,
   - parts for medical devices, & more;
- Main Astronautics Equipment Produced:
   - terminals for lithium-ion batteries used on the Hayabusa, an asteroid space probe, & the [Akatsuki](akatsuki.md), a space probe for exploring Venus,
   - Fuel tank penetration flange for H-I rockets.



## The End

end of file
