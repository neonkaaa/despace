# Despace ECSS Guide #1: R&D Phases, Schedule & Tasks
> 2024.01.03 [🚀](../../index/index.md) [despace](index.md) → [Doc](doc.md), [SC](sc.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---


## General intro

The schedule philosophy for each phase/review is the following:

1. Every phase is logically divided into 2 parts: development & review.
2. At the end of each “development part” there is an informal kick‑off for the next phase. From this checkpoint there are 2 versions of documents — the 1st one is for the Stakeholder’s RIDs, in which only RIDs are implemented, & the 2nd one is for the next phase, in which both RIDs & changes for the next phase are implemented.
3. At the end of each “review part” there is a formal kick‑off for the next phase.

The list of documents from ECSS‑E‑ST‑10C & ECSS‑M‑ST‑10C.


### Matters

1. Need
2. Goal
3. Objective
4. Expectation
5. Milestone
6. Deliverable
7. Task
8. Deadline


### Documents list

|**Document**|**May be a part of**|
|:--|:--|
|001 Mission description document (MDD)|−|
|002 AIT QM/FM plan|−|
|003 As‑built configuration list (ABCL)|−|
|004 Configuration status accounting reports|−|
|005 Configuration item data list|−|
|006 Configuration item list|−|
|007 Configuration management plan (CMP)|Project Management Plan|
|008 Coordinate system document (CSD)|SE Plan|
|009 Correlation report|−|
|010 Cost estimate report (CER)|−|
|011 Design definition file (DDF)|−|
|012 Design justification file (DJF)|−|
|013 Function tree (FT)|Design Definition File|
|014 GSE Data packages|−|
|015 GSE Specifications|−|
|016 Inspection report|−|
|017 Interface control document (ICD)|−|
|018 Interface requirements document (IRD)|Technical Requirements Specification|
|019 Mathematical model description (MMD)|−|
|020 Product tree (PT)|Design Definition File|
|021 Product user manual / User Manual (PUM/UM)|−|
|022 Project management plan (PMP) + Product Assurance Plan (PAP)|−|
|023 Requirement justification file (RJF)|Design Justification File|
|024 Requirement traceability matrix w.r.t. next lower level|−|
|025 Review of design report|−|
|026 Risk assessment report (RAR)|−|
|027 Risk management plan (RMP)|−|
|028 Risk management policy document (RMPD)|Risk Management Plan|
|029 Schedule|−|
|030 Software configuration list (SCL)|−|
|031 Space debris mitigation plan (SDMP)|−|
|032 Specification tree (ST)|Design Definition File|
|033 System concept report (SCR)|−|
|034 System engineering plan (SEP)|−|
|035 Technical budget (TB)|Design Definition File|
|036 (Preliminary) Technical requirements specification (TS)|−|
|037 Technology matrix (TM)|Design Justification File|
|038 Technology plan (TP) + Technology readiness status list|SE Plan|
|039 Test procedure|−|
|040 Test report|−|
|041 Test specification (TS)|−|
|042 Trade‑off reports (TOR)|−|
|043 Verification control document (VCD)|−|
|044 Verification plan (VP)|AIT Plan|
|045 Verification report (VR)|−|
|046 Work breakdown structure (WBS)|−|
|047 Work package description (WPD)|−|
|048 Abbreviations file|−|
|Design definition file for next lower level|−|
|Design justification file for next lower level|−|
|Other related plans (from ECSS‑E‑ST‑10 Ann.D)|−|
|Technical requirements specifications for next lower level|−|



## Phase 0

### Phase 0 Schedule

|**Schedule, Phase 0**|**Date (YY.MM.DD)**|
|:--|:--|
|**~ ~ ~ ~ ~ MDR — Development ~ ~ ~ ~ ~**|~ ~ ~ ~ ~ |
|Kick‑off for Phase 0|TBD|
|Payload requirements Soft deadline, contact industry (partners)|TBD|
|Soft deadline|TBD|
|Validation of the docs readiness, booking the Review with Stakeholder|TBD|
|Get information from industry (partners)|TBD|
|Inner review|TBD|
|Hard deadline|TBD|
|Kick‑off for Phase A (informal)|TBD|
|**~ ~ ~ ~ ~ MDR — Review ~ ~ ~ ~ ~**|~ ~ ~ ~ ~ |
|Configure & send to Stakeholder|TBD|
|Stakeholder preliminary review|TBD|
|Stakeholder review|TBD|
|Handling RIDs, configure & send to Stakeholder|TBD|
|Stakeholder final review & approval|TBD|
|Kick‑off for Phase A (formal)|TBD|
|Finalizing everything|TBD|

### Phase 0 Tasks

|**ID**|**Tasks, Phase 0**|**Source**|**State**|
|:--|:--|:--|:--|
|1|Elaborate the Mission Statement. Identify customer’s & your needs, goals, objectives, expectations|MDD, SCR, SEP, TOR, TS|NEW|
|2|Identify Mission Concepts: Baseline & Options for the missions & important units/technologies. Compare Concepts between each other & with similar ones, identify the strengths & weaknesses, & make a weighted choice. Define how the baseline will evolve|MDD, SEP, SCR, TOR|NEW|
|3|Obtain any relevant contribution from lower level suppliers, other projects, & apply it for the final concept|MDD, SCR, SEP|NEW|
|4|Identify customers & areas of applicability|MDD, SCR, TS|NEW|
|5|Identify Technical Requirements (incl. performance & its drivers, main events, dependability, safety, constraints w.r.t. the physical & operational environment) for the needs & system sustainability. Make them linked to each other, traceable, & state how to verify them; justify your choice|MDD, TS, RJF|NEW|
|6|Assessment of programmatic aspects supported by market & economic studies|MDD, RMP|NEW|
|7|Define Risk management policy to use, & plan how to deal with risks|RMP, RMPD, SEP|NEW|
|8|Identify risks (technical, programmatic, debris) & how to handle them|MDD, RMP|NEW|
|9|Identify architecture of the mission, & (sub)systems. Build a working system of everything|MDD, TOR|NEW|
|10|Identify ConOps & functions, & how the system works/function in each mission phase|MDD, SEP|NEW|
|11|Identify technical budgets & partners|Schedule, SCR, TOR, TS|NEW|
|12|Identify space, ground, & user segments|SEP|NEW|
|13|Identify critical technologies|RMP, SCR, TOR|NEW|
|14|Identify reachability of requirements, needs, & other matters, & possible impact|MDD|NEW|
|15|Define how the Concept, architecture, maturity, TRL, technologies, approaches, tools, models etc. can be reused & standardized|SEP, TP|NEW|
|16|Define managing & organizational approaches/constraints (internal & external). Define the procurement approach|Schedule, SEP|NEW|
|17|Define standards & regulations to use/follow, reviews & milestones|Schedule, SEP|NEW|
|18|Identify steps/activities from Phase 0 to Launch, build schedule, define how to coordinate all technical activities|Schedule, SEP, TS|NEW|
|19|Define the capacity for verification & validation|SEP|NEW|
|20|List the internal/external means & facilities (e.g. equipment, software, & premises)|SEP|NEW|
|21|Define the model philosophy & the number of models|SEP|NEW|
|22|Define margin policy|SEP|NEW|
|23|Define the team & partners involved|MDD, SEP|NEW|
|24|Define technology & engineering evolutions|SEP, TP|NEW|
|25|Identify flaws you have or may have|MDD, SCR|NEW|
|26|Perform the Mission Definition Review (MDR) & implement its actions|ECSS-E-ST-10C|NEW|



### Phase 0 Documents list

**MDR:**

1. 【NEW】 001 Mission description document (MDD)
2. 【NEW】 023 Requirement justification file (RJF)
3. 【NEW】 027 Risk management plan (RMP)
4. 【NEW】 028 Risk management policy document (RMPD)
5. 【NEW】 029 Schedule
6. 【NEW】 031 Space debris mitigation plan (SDMP)
7. 【NEW】 033 System concept report (SCR)
8. 【NEW】 034 System engineering plan (SEP)
9. 【NEW】 036 (Preliminary) Technical requirements specification (TS)
10. 【NEW】 038 Technology plan (TP) + Technology readiness status list
11. 【NEW】 043 Trade‑off reports
12. 【NEW】 Final presentation



## Phase A

### Phase A Schedule

|**Schedule, Phase A**|**Date (YY.MM.DD)**|
|:--|:--|
|**~ ~ ~ ~ ~ PRR — Development ~ ~ ~ ~ ~**|~ ~ ~ ~ ~ |
|Kick‑off for Phase A (informal from Phase 0)|TBD|
|Kick‑off for Phase A (formal from Phase 0)|TBD|
|Payload requirements Soft deadline, contact industry (partners)|TBD|
|Validation of the docs readiness, booking the Review with Stakeholder|TBD|
|Docs Soft deadline|TBD|
|Get information from industry (partners)|TBD|
|Docs Inner review|TBD|
|Docs Hard deadline|TBD|
|Kick‑off for Phase B (informal)|TBD|
|**~ ~ ~ ~ ~ PRR — Review ~ ~ ~ ~ ~**|~ ~ ~ ~ ~ |
|Configure & send to Stakeholder|TBD|
|Stakeholder preliminary review|TBD|
|Stakeholder review|TBD|
|Handling RIDs, configure & send to Stakeholder|TBD|
|Stakeholder final review & approval|TBD|
|Kick‑off for Phase B (formal)|TBD|
|Finalizing everything|TBD|

### Phase A Tasks

|**ID**|**Tasks, Phase A**|**Source**|**State**|
|:--|:--|:--|:--|
|1|**Finalize** customer’s & your needs, goals, objectives, expectations (NGOE)|MDD, SCR, SEP, TOR, TS|✔|
|2|**Update** Mission **Solutions**: Baseline & Options for the missions & important units/technologies. Compare Solutions between each other & with similar ones, identify the strengths & weaknesses, & make a weighted choice. Define how the Baseline evolve. **Define the system & operations concept(s) & technical solutions. Justify your choice**|**DDF, DJF**, MDD, SEP, SCR, TOR|✔|
|3|Obtain any relevant contribution from lower level suppliers, other projects, & apply it for the final **Solution**|MDD, SCR, SEP|✔|
|4|**Update** customers & areas of applicability|MDD, SCR, TS|✔|
|5|**Update** Technical Requirements (incl. performance & its drivers, **interfaces,** main events, dependability, safety, constraints w.r.t. the physical & operational environment) for the needs & system sustainability. Make them linked to each other, traceable, & state how to verify them; justify your choice. **Finalize the validation of the requirements against the needs together with the customer**|**DDF, IRD**, MDD, RJF, **RTM, TB**, TS, **VCD**|✔|
|6|**Update** of programmatic aspects supported by market & economic studies|MDD, RMP|✔|
|7|Plan how to deal with risks, perform the risk assessment|**RAR**, RMP, RMPD, SEP|✔|
|8|**Update** risks (technical, programmatic, debris) & how to handle them|MDD, RMP|✔|
|9|**Update** architecture & design **(i.e. the arrangement of elements, their decomposition, interfaces internal/external, physical constraints)** of the mission, & (sub)systems. Build a working system of everything. **Define where/how to store engineering matters. Justify your**|**DDF, DJF**, MDD, TOR|✔|
|10|**Update** ConOps & functions, **operational modes/states**, how the system works/function in each mission phase, **& which units perform which functions**|**DDF**, MDD, SEP|✔|
|11|**Update** technical budgets **(mass, comms, power, memory, dV)**, **margins & deviations**, & partners|Schedule, **DDF**, SCR, TOR, **TB**, TS|✔|
|12|**Update** space, ground, & user segments|SEP|✔|
|13|**Update** critical technologies/**items**; **pre‑development activities**|RMP, SCR, TOR|✔|
|14|**Update** reachability of requirements, **NGOEs**, & other matters, & possible impact **via proposed technologies**|MDD, **TM**|✔|
|15|**Update** how the **Solution**, architecture, maturity, TRL, technologies, approaches, tools, models etc. can be reused & standardized|SEP, TP|✔|
|16|**Update** managing & organizational approaches/constraints (internal & external). Define the procurement approach|Schedule, SEP|✔|
|17|**Update** standards & regulations to use/follow, reviews & milestones|Schedule, SEP|✔|
|18|**Update** steps/activities from Phase 0 to Launch, build schedule, define how to coordinate all technical activities|Schedule, SEP, TS|✔|
|19|**Update** the capacity for verification & validation (V&V), **V&V strategies, Plan, tools, control, approach to elaborate during Phase B. Qualification Plan**|**DJF**, SEP, **VCD, VP**|✔|
|20|**Update** the internal/external means & facilities (e.g. equipment, software, & premises)|SEP|✔|
|21|**Update** the model philosophy & the number of models|SEP, **VP**|✔|
|22|**Update** margin policy|**DJF**, SEP, **TB**|✔|
|23|**Update** the team & partners involved|MDD, SEP|✔|
|24|**Update** technology & engineering evolutions|SEP, TP|✔|
|25|**Update** flaws you have or may have|MDD, SCR|✔|
|27|Establish the Management plan, System Engineering plan, & Product Assurance plan for the project|PMP, SEP|NEW|
|28|Assess the technical & programmatic feasibility of the possible concepts by identifying constraints relating to implementation, costs, schedules, organization, operations, maintenance, production & disposal. Quantify & characterize critical elements for technical & economic feasibility|DDF|NEW|
|29|Perform 3D modeling & units installation|TB|NEW|
|30|Define the configuration management approach, resources, & plan|CMP|NEW|
|31|Define coordinate systems (axles, time) for mission/units, & their interlinks & evolutions|CSD|NEW|
|32|Define costs for the mission, project, systems, units for the baseline & options; define related risks|CER, RAR|NEW|
|33|Define (based on NGOE) function hierarchical decomposition (Function tree) for the requirements specs. Then hierarchical partitioning of a deliverable product (Product tree) for WBS. Then a draft of Specification tree|DDF, FT, PT, TS|NEW|
|34|Define management matters (plan, organization, breakdown, configuration/info/docs management, cost & schedule, integrated logistics, risk, product assurance, engineering management)|PMP|NEW|
|35|Define the work to be done, break down to packages & hierarchical structures|WBS, WPD|NEW|
|36|Perform the Preliminary Requirement Review (PRR) & implement its actions|ECSS-E-ST-10C|NEW|
|26|Perform the Mission Definition Review (MDR) & implement its actions|✔|Del.|



### Phase A Documents list

**PRR:**

1. 001 Mission description document (MDD)
2. 023 Requirement justification file (RJF)
3. 027 Risk management plan (RMP)
4. 028 Risk management policy document (RMPD)
1. 029 Schedule
1. 031 Space debris mitigation plan (SDMP)
1. 033 System concept report (SCR)
1. 034 System engineering plan (SEP)
1. 036 (Preliminary) Technical requirements specification (TS)
1. 038 Technology plan (TP) + Technology readiness status list
1. 042 Trade‑off reports (TOR)
1. Final presentation
1. 【NEW】 007 Configuration management plan (CMP)
1. 【NEW】 008 Coordinate system document (CSD)
1. 【NEW】 010 Cost estimate report (CER)
1. 【NEW】 011 Design definition file (DDF)
1. 【NEW】 012 Design justification file (DJF)
1. 【NEW】 013 Function tree (FT)
1. 【NEW】 018 Interface requirements document (IRD)
1. 【NEW】 021 Product tree (PT)
1. 【NEW】 022 Project management plan (PMP)
1. 【NEW】 025 Requirement traceability matrix w.r.t. next lower level
1. 【NEW】 027 Risk assessment report (RAR)
1. 【NEW】 035 Technical budget (TB)
1. 【NEW】 037 Technology matrix (TM)
1. 【NEW】 043 [Verification](vnv.md) control document (VCD)
1. 【NEW】 044 [Verification](vnv.md) plan (VP)
1. 【NEW】 046 Work breakdown structure (WBS)
1. 【NEW】 047 Work package description (WPD)
1. 【NEW】 CAD model



## Phase B

### Phase B Schedule

|**Schedule, Phase A**|**Date (YY.MM.DD)**|
|:--|:--|
|**~ ~ ~ ~ ~ SRR — Development ~ ~ ~ ~ ~**|~ ~ ~ ~ ~ |
|Kick‑off for Phase B (informal from Phase A)|TBD|
|Kick‑off for Phase B (formal from Phase A)|TBD|
|Requirements Soft deadline, contact industry (partners)|TBD|
|Docs Soft deadline|TBD|
|Get information from industry (partners)|TBD|
|Validation of the docs readiness, booking the Review with Stakeholder|TBD|
|Docs Inner review|TBD|
|Docs Hard deadline|TBD|
|Kick‑off for PDR (informal)|TBD|
|**~ ~ ~ ~ ~ SRR — Review ~ ~ ~ ~ ~**|~ ~ ~ ~ ~ |
|Configure & send to Stakeholder|TBD|
|Stakeholder preliminary review|TBD|
|Stakeholder review|TBD|
|Handling RIDs, configure & send to Stakeholder|TBD|
|Stakeholder final review & approval|TBD|
|Kick‑off for PDR (formal)|TBD|
|Finalizing everything|TBD|
|**~ ~ ~ ~ ~ PDR — Development ~ ~ ~ ~ ~**|~ ~ ~ ~ ~ |
|Requirements Soft deadline, contact industry (partners)|TBD|
|Docs Soft deadline|TBD|
|Get information from industry (partners)|TBD|
|Validation of the docs readiness, booking the Review with Stakeholder|TBD|
|Docs Inner review|TBD|
|Docs Hard deadline|TBD|
|Kick‑off for Phase C (informal)|TBD|
|**~ ~ ~ ~ ~ PDR — Review ~ ~ ~ ~ ~**|~ ~ ~ ~ ~ |
|Configure & send to Stakeholder|TBD|
|Stakeholder preliminary review|TBD|
|Stakeholder review|TBD|
|Handling RIDs, configure & send to Stakeholder|TBD|
|Stakeholder final review & approval|TBD|
|Kick‑off for Phase C (formal)|TBD|
|Finalizing everything|TBD|

### （TBD) Phase B Tasks

1. establish the system definition for the system solution selected at end of Phase A.
1. demonstrate that the solution meets the technical requirements according to the schedule, the target cost & the customer requirements.
1. define development approach & plan of engineering activities.
1. Finalize the project management, engineering & product assurance plans.
1. Establish the baseline master schedule.
1. Elaborate the baseline cost at completion.
1. Elaborate the organizational breakdown structure (OBS).
1. Confirm technical solution(s) for the system & operations concept(s) & their feasibility with respect to programmatic constraints.
1. Conduct “trade‑off” studies & select the preferred system concept, together with the preferred technical solution(s) for this concept.
1. Establish a design definition for the selected system concept & retained technical solution(s).
1. Determine the verification program incl. model philosophy.
1. Identify & define external interfaces.
1. Prepare the next level specification & related business agreement documents.
1. Initiate pre‑development work on critical technologies or system design areas when it is necessary to reduce the development risks.
1. Initiate any long‑lead item procurement required to meet project schedule needs.
1. Prepare the space debris mitigation plan & the disposal plan.
1. Conduct reliability & safety assessment.
1. Finalize the product tree, the work breakdown structure & the specification tree.
1. Update the risk assessment.

|**ID**|**Task (Phase A)**|**Source**|**State**|
|:--|:--|:--|:--|
|1|**Finalize** customer’s & your needs, goals, objectives, expectations (NGOE)|MDD, SCR, SEP, TOR, TS|✔|
|2|**Update** Mission **Solutions**: Baseline & Options for the missions & important units/technologies. Compare Solutions between each other & with similar ones, identify the strengths & weaknesses, & make a weighted choice. Define how the Baseline evolve. **Define the system & operations concept(s) & technical solutions. Justify your choice**|**DDF, DJF**, MDD, SEP, SCR, TOR|✔|
|3|Obtain any relevant contribution from lower level suppliers, other projects, & apply it for the final **Solution**|MDD, SCR, SEP|✔|
|4|**Update** customers & areas of applicability|MDD, SCR, TS|✔|
|5|**Update** Technical Requirements (incl. performance & its drivers, **interfaces,** main events, dependability, safety, constraints w.r.t. the physical & operational environment) for the needs & system sustainability. Make them linked to each other, traceable, & state how to verify them; justify your choice. **Finalize the validation of the requirements against the needs together with the customer**|**DDF, IRD**, MDD, RJF, **RTM, TB**, TS, **VCD**|✔|
|6|**Update** of programmatic aspects supported by market & economic studies|MDD, RMP|✔|
|7|Plan how to deal with risks, perform the risk assessment|**RAR**, RMP, RMPD, SEP|✔|
|8|**Update** risks (technical, programmatic, debris) & how to handle them|MDD, RMP|✔|
|9|**Update** architecture & design **(i.e. the arrangement of elements, their decomposition, interfaces internal/external, physical constraints)** of the mission, & (sub)systems. Build a working system of everything. **Define where/how to store engineering matters. Justify your**|**DDF, DJF**, MDD, TOR|✔|
|10|**Update** ConOps & functions, **operational modes/states**, how the system works/function in each mission phase, **& which units perform which functions**|**DDF**, MDD, SEP|✔|
|11|**Update** technical budgets **(mass, comms, power, memory, dV)**, **margins & deviations**, & partners|Schedule, **DDF**, SCR, TOR, **TB**, TS|✔|
|12|**Update** space, ground, & user segments|SEP|✔|
|13|**Update** critical technologies/**items**; **pre‑development activities**|RMP, SCR, TOR|✔|
|14|**Update** reachability of requirements, **NGOEs**, & other matters, & possible impact **via proposed technologies**|MDD, **TM**|✔|
|15|**Update** how the **Solution**, architecture, maturity, TRL, technologies, approaches, tools, models etc. can be reused & standardized|SEP, TP|✔|
|16|**Update** managing & organizational approaches/constraints (internal & external). Define the procurement approach|Schedule, SEP|✔|
|17|**Update** standards & regulations to use/follow, reviews & milestones|Schedule, SEP|✔|
|18|**Update** steps/activities from Phase 0 to Launch, build schedule, define how to coordinate all technical activities|Schedule, SEP, TS|✔|
|19|**Update** the capacity for verification & validation (V&V), **V&V strategies, Plan, tools, control, approach to elaborate during Phase B. Qualification Plan**|**DJF**, SEP, **VCD, VP**|✔|
|20|**Update** the internal/external means & facilities (e.g. equipment, software, & premises)|SEP|✔|
|21|**Update** the model philosophy & the number of models|SEP, **VP**|✔|
|22|**Update** margin policy|**DJF**, SEP, **TB**|✔|
|23|**Update** the team & partners involved|MDD, SEP|✔|
|24|**Update** technology & engineering evolutions|SEP, TP|✔|
|25|**Update** flaws you have or may have|MDD, SCR|✔|
|27|Establish the Management plan, System Engineering plan, & Product Assurance plan for the project|PMP, SEP|✔|
|28|Assess the technical & programmatic feasibility of the possible concepts by identifying constraints relating to implementation, costs, schedules, organization, operations, maintenance, production & disposal. Quantify & characterize critical elements for technical & economic feasibility|DDF|✔|
|29|Perform 3D modeling & units installation|TB|✔|
|30|Define the configuration management approach, resources, & plan|CMP|✔|
|31|Define coordinate systems (axles, time) for mission/units, & their interlinks & evolutions|CSD|✔|
|32|Define costs for the mission, project, systems, units for the baseline & options; define related risks|CER, RAR|✔|
|33|Define (based on NGOE) function hierarchical decomposition (Function tree) for the requirements specs. Then hierarchical partitioning of a deliverable product (Product tree) for WBS. Then a draft of Specification tree|DDF, FT, PT, TS|✔|
|34|Define management matters (plan, organization, breakdown, configuration/info/docs management, cost & schedule, integrated logistics, risk, product assurance, engineering management)|PMP|✔|
|35|Define the work to be done, break down to packages & hierarchical structures|WBS, WPD|✔|
| |Perform the System Requirements Review (SRR) & implement its actions|ECSS-E-ST-10C|NEW|
| |Perform the Preliminary Design Review (PDR) & implement its actions|ECSS-E-ST-10C|NEW|
|36|Perform the Preliminary Requirement Review (PRR) & implement its actions|ECSS-E-ST-10C|Del.|


### Phase B Documents list

**SRR:**

1. 007 Configuration management plan (CMP)
2. 008 Coordinate system document (CSD)
3. 010 Cost estimate report (CER)
4. 011 Design definition file (DDF)
5. 012 Design justification file (DJF)
6. 013 Function tree (FT)
7. 018 Interface requirements document (IRD)
8. 020 Product tree (PT)
9. 022 Project management plan (PMP) + Product Assurance Plan (PAP)
10. 023 Requirement justification file (RJF)
11. 024 Requirement traceability matrix w.r.t. next lower level
12. 026 Risk assessment report (RAR)
13. 027 Risk management plan (RMP)
14. 028 Risk management policy document (RMPD)
15. 029 Schedule
16. 031 Space debris mitigation plan (SDMP)
17. 034 System engineering plan (SEP)
18. 035 Technical budget (TB)
19. 036 ~~(Preliminary)~~ Technical requirements specification (TS)
20. 037 Technology matrix (TM)
21. 038 Technology plan (TP) + Technology readiness status list
22. 042 Trade‑off reports (TOR)
23. 043 [Verification](vnv.md) control document (VCD)
24. 044 [Verification](vnv.md) plan (VP)
25. 046 Work breakdown structure (WBS)
26. 047 Work package description (WPD)
27. CAD model
28. Final presentation
29. 【NEW】 017 Interface control document (ICD)
30. 【NEW】 019 Mathematical model description (MMD)
31. 【NEW】 032 Specification tree (ST)
32. 【NEW】 Technical requirements specifications for next lower level
33. （Deleted） 001 Mission description document (MDD)
34. （Deleted） 033 System concept report (SCR)

**PDR:**

1. 007 Configuration management plan (CMP)
1. 008 Coordinate system document (CSD)
1. 010 Cost estimate report (CER)
1. 011 Design definition file (DDF)
1. 012 Design justification file (DJF)
1. 013 Function tree (FT)
1. 017 Interface control document (ICD)
1. 018 Interface requirements document (IRD)
1. 019 Mathematical model description (MMD)
1. 020 Product tree (PT)
1. 022 Project management plan (PMP) + Product Assurance Plan (PAP)
1. 023 Requirement justification file (RJF)
1. 024 Requirement traceability matrix w.r.t. next lower level
1. 026 Risk assessment report (RAR)
1. 027 Risk management plan (RMP)
1. 028 Risk management policy document (RMPD)
1. 029 Schedule
1. 031 Space debris mitigation plan (SDMP)
1. 032 Specification tree (ST)
1. 034 System engineering plan (SEP)
1. 035 Technical budget (TB)
1. 037 Technology matrix (TM)
1. 038 Technology plan (TP) + Technology readiness status list
1. 042 Trade‑off reports (TOR)
1. 043 [Verification](vnv.md) control document (VCD)
1. 044 [Verification](vnv.md) plan (VP)
1. 046 Work breakdown structure (WBS)
1. 047 Work package description (WPD)
1. Technical requirements specifications for next lower level
1. CAD model
1. Final presentation
1. 【NEW】 002 AIT QM/FM plan
1. 【NEW】 004 Configuration status accounting reports
1. 【NEW】 005 Configuration item data list
1. 【NEW】 006 Configuration item list
1. 【NEW】 015 GSE Specifications
1. 【NEW】 030 Software configuration list (SCL)
1. 【NEW】 Design definition file for next lower level
1. （Deleted） 036 Technical requirements specification (TS)

## Phase C

### Phase C Schedule

|**Schedule, Phase C**|**Date (YY.MM.DD)**|
|:--|:--|
|**~ ~ ~ ~ ~ CDR — Development ~ ~ ~ ~ ~**|~ ~ ~ ~ ~ |
|Kick‑off for Phase C (informal from Phase B)|TBD|
|Kick‑off for Phase C (formal from Phase B)|TBD|
|Requirements Soft deadline, contact industry (partners)|TBD|
|Docs Soft deadline|TBD|
|Get information from industry (partners)|TBD|
|Validation of the docs readiness, booking the Review with Stakeholder|TBD|
|Docs Hard deadline|TBD|
|Kick‑off for Phase D (informal)|TBD|
|**~ ~ ~ ~ ~ CDR — Review ~ ~ ~ ~ ~**|~ ~ ~ ~ ~ |
|Configure & send to Stakeholder|TBD|
|Stakeholder preliminary review|TBD|
|Stakeholder review|TBD|
|Handling RIDs, configure & send to Stakeholder|TBD|
|Stakeholder final review & approval|TBD|
|Kick‑off for Phase D (formal)|TBD|
|Finalizing everything|TBD|

### （TBD) Phase C Tasks

1. **Engineering:**
   1. establish the system detailed definition.
   1. demonstrate the capability to meet the technical requirements of the system technical requirements specification.
   1. support the Critical Design Review (CDR) & ensures implementation of the CDR actions.
1. **Management:**
   1. Completion of the detailed design definition of the system at all levels in the customer‑supplier chain.
   1. Production, development testing & pre‑qualification of selected critical elements & components.
   1. Production & development testing of engineering models, as required by the selected model philosophy & verification approach.
   1. Completion of assembly, integration & test planning for the system & its constituent parts.
   1. Detailed definition of internal & external interfaces.
   1. Issue of (preliminary) user manual.
   1. Update of the risk assessment.

### Phase C Documents list

**CDR:**

1. 002 AIT QM/FM plan
1. 004 Configuration status accounting reports
1. 005 Configuration item data list
1. 006 Configuration item list
1. 008 Coordinate system document (CSD)
1. 011 Design definition file (DDF)
1. 012 Design justification file (DJF)
1. 015 GSE Specifications
1. 017 Interface control document (ICD)
1. 019 Mathematical model description (MMD)
1. 020 Product tree (PT)
1. 026 Risk assessment report (RAR)
1. 029 Schedule
1. 030 Software configuration list (SCL)
1. 031 Space debris mitigation plan (SDMP)
1. 032 Specification tree (ST)
1. 034 System engineering plan (SEP)
1. 035 Technical budget (TB)
1. 042 Trade‑off reports (TOR)
1. 043 Verification control document (VCD)
1. 044 Verification plan (VP)
1. CAD model
1. Design definition file for next lower level
1. Final presentation
1. 【NEW】 009 Correlation report
1. 【NEW】 014 GSE Data packages
1. 【NEW】 016 Inspection report
1. 【NEW】 021 Product user manual / User Manual (PUM/UM)
1. 【NEW】 025 Review of design report
1. 【NEW】 039 Test procedure
1. 【NEW】 040 Test report
1. 【NEW】 041 Test specification (TS)
1. 【NEW】 045 Verification report (VR)
1. 【NEW】 Design justification file for next lower level
1. （Deleted） 007 Configuration management plan (CMP)
1. （Deleted） 010 Cost estimate report (CER)
1. （Deleted） 013 Function tree (FT)
1. （Deleted） 018 Interface requirements document (IRD)
1. （Deleted） 022 Project management plan (PMP) + Product Assurance Plan (PAP)
1. （Deleted） 023 Requirement justification file (RJF)
1. （Deleted） 024 Requirement traceability matrix w.r.t. next lower level
1. （Deleted） 027 Risk management plan (RMP)
1. （Deleted） 028 Risk management policy document (RMPD)
1. （Deleted） 037 Technology matrix (TM)
1. （Deleted） 038 Technology plan (TP) + Technology readiness status list
1. （Deleted） 046 Work breakdown structure (WBS)
1. （Deleted） 047 Work package description (WPD)
1. （Deleted） Technical requirements specifications for next lower level


## Phase D

### Phase D Schedule

|**Schedule, Phase D**|**Date (YY.MM.DD)**|
|:--|:--|
|**~ ~ ~ ~ ~ QR — Development ~ ~ ~ ~ ~**|~ ~ ~ ~ ~ |
|Kick‑off for Phase D (informal from Phase C)|TBD|
|Kick‑off for Phase D (formal from Phase C)|TBD|
|Requirements Soft deadline, contact industry (partners)|TBD|
|Docs Soft deadline|TBD|
|Get information from industry (partners)|TBD|
|Validation of the docs readiness, booking the Review with Stakeholder|TBD|
|Docs Hard deadline|TBD|
|Kick‑off for AR (informal)|TBD|
|**~ ~ ~ ~ ~ QR — Review ~ ~ ~ ~ ~**|~ ~ ~ ~ ~ |
|Configure & send to Stakeholder|TBD|
|Stakeholder preliminary review|TBD|
|Stakeholder review|TBD|
|Handling RIDs, configure & send to Stakeholder|TBD|
|Stakeholder final review & approval|TBD|
|Kick‑off for AR (formal)|TBD|
|Finalizing everything|TBD|
|**~ ~ ~ ~ ~ AR — Development ~ ~ ~ ~ ~**|~ ~ ~ ~ ~ |
|Requirements Soft deadline, contact industry (partners)|TBD|
|Docs Soft deadline|TBD|
|Get information from industry (partners)|TBD|
|Validation of the docs readiness, booking the Review with Stakeholder|TBD|
|Docs Hard deadline|TBD|
|Kick‑off for Phase E (informal)|TBD|
|**~ ~ ~ ~ ~ AR — Review ~ ~ ~ ~ ~**|~ ~ ~ ~ ~ |
|Configure & send to Stakeholder|TBD|
|Stakeholder preliminary review|TBD|
|Stakeholder review|TBD|
|Handling RIDs, configure & send to Stakeholder|TBD|
|Stakeholder final review & approval|TBD|
|Kick‑off for Phase E (formal)|TBD|
|Finalizing everything|TBD|
|**~ ~ ~ ~ ~ ORR — Development ~ ~ ~ ~ ~**|~ ~ ~ ~ ~ |
|Requirements Soft deadline, contact industry (partners)|TBD|
|Docs Soft deadline|TBD|
|Get information from industry (partners)|TBD|
|Validation of the docs readiness, booking the Review with Stakeholder|TBD|
|Docs Inner review|TBD|
|Docs Hard deadline|TBD|
|Kick‑off for FRR (informal)|TBD|
|**~ ~ ~ ~ ~ ORR — Review ~ ~ ~ ~ ~**|~ ~ ~ ~ ~ |
|Configure & send to Stakeholder|TBD|
|Stakeholder preliminary review|TBD|
|Stakeholder review|TBD|
|Handling RIDs, configure & send to Stakeholder|TBD|
|Stakeholder final review & approval|TBD|
|Kick‑off for FRR (formal)|TBD|
|Finalizing everything|TBD|

### （TBD) Phase D Tasks

1. **Engineering:**
   1. finalize the development of the system by qualification & acceptance.
   1. finalize the preparation for operations & utilization.
   1. support Qualification Review (QR) & Acceptance Review (AR) & ensures implementation of the QR & AR actions.
1. **Management:**
   1. Complete qualification testing & associated verification activities.
   1. Complete manufacturing, assembly & testing of flight hardware/software & associated ground support hardware/software.
   1. Complete the interoperability testing between the space & ground segment.
   1. Prepare acceptance data package.

### Phase D Documents list

**QR:**

1. 002 AIT QM/FM plan
1. 004 Configuration status accounting reports
1. 005 Configuration item data list
1. 008 Coordinate system document (CSD)
1. 009 Correlation report
1. 011 Design definition file (DDF)
1. 012 Design justification file (DJF)
1. 014 GSE Data packages
1. 015 GSE Specifications
1. 016 Inspection report
1. 017 Interface control document (ICD)
1. 019 Mathematical model description (MMD)
1. 021 Product user manual / User Manual (PUM/UM)
1. 025 Review of design report
1. 026 Risk assessment report (RAR)
1. 029 Schedule
1. 030 Software configuration list (SCL)
1. 031 Space debris mitigation plan (SDMP)
1. 032 Specification tree (ST)
1. 034 System engineering plan (SEP)
1. 035 Technical budget (TB)
1. 039 Test procedure
1. 040 Test report
1. 041 Test specification (TS)
1. 043 Verification control document (VCD)
1. 044 Verification plan (VP)
1. 045 Verification report (VR)
1. Design definition file for next lower level
1. Design justification file for next lower level
1. Final presentation
1. 【NEW】 003 As‑built configuration list (ABCL)
1. （Deleted） 006 Configuration item list
1. （Deleted） 020 Product tree (PT)
1. （Deleted） 042 Trade‑off reports (TOR)

**AR:**

1. 002 AIT QM/FM plan
1. 003 As‑built configuration list (ABCL)
1. 004 Configuration status accounting reports
1. 005 Configuration item data list
1. 014 GSE Data packages
1. 015 GSE Specifications
1. 016 Inspection report
1. 017 Interface control document (ICD)
1. 021 Product user manual / User Manual (PUM/UM)
1. 026 Risk assessment report (RAR)
1. 029 Schedule
1. 030 Software configuration list (SCL)
1. 031 Space debris mitigation plan (SDMP)
1. 032 Specification tree (ST)
1. 034 System engineering plan (SEP)
1. 035 Technical budget (TB)
1. 039 Test procedure
1. 040 Test report
1. 041 Test specification (TS)
1. 043 Verification control document (VCD)
1. 044 Verification plan (VP)
1. 045 Verification report (VR)
1. Design definition file for next lower level
1. Design justification file for next lower level
1. Final presentation
1. （Deleted） 008 Coordinate system document (CSD)
1. （Deleted） 009 Correlation report
1. （Deleted） 011 Design definition file (DDF)
1. （Deleted） 012 Design justification file (DJF)
1. （Deleted） 019 Mathematical model description (MMD)
1. （Deleted） 025 Review of design report

**ORR:**

1. 017 Interface control document (ICD)
1. 021 Product user manual / User Manual (PUM/UM)
1. 026 Risk assessment report (RAR)
1. 029 Schedule
1. 031 Space debris mitigation plan (SDMP)
1. 032 Specification tree (ST)
1. 039 Test procedure
1. 040 Test report
1. 041 Test specification (TS)
1. 043 Verification control document (VCD)
1. 045 Verification report (VR)
1. Final presentation
1. （Deleted） 002 AIT QM/FM plan
1. （Deleted） 003 As‑built configuration list (ABCL)
1. （Deleted） 004 Configuration status accounting reports
1. （Deleted） 005 Configuration item data list
1. （Deleted） 014 GSE Data packages
1. （Deleted） 015 GSE Specifications
1. （Deleted） 016 Inspection report
1. （Deleted） 030 Software configuration list (SCL)
1. （Deleted） 034 System engineering plan (SEP)
1. （Deleted） 035 Technical budget (TB)
1. （Deleted） 044 Verification plan (VP)
1. （Deleted） Design definition file for next lower level
1. （Deleted） Design justification file for next lower level



## Phase E

### Phase E Schedule

|**Schedule, Phase E**|**Date (YY.MM.DD)**|
|:--|:--|
|**~ ~ ~ ~ ~ FRR — Development ~ ~ ~ ~ ~**|~ ~ ~ ~ ~ |
|Kick‑off for Phase E (informal from Phase D)|TBD|
|Kick‑off for Phase E (formal from Phase D)|TBD|
|Queue‑1 Soft deadline|TBD|
|Get information from industry (partners)|TBD|
|Validation of the docs readiness, booking the Review with Stakeholder|TBD|
|Queue‑1 Inner review|TBD|
|Queue‑1 Hard deadline|TBD|
|Kick‑off for LRR (informal)|TBD|
|**~ ~ ~ ~ ~ FRR — Review ~ ~ ~ ~ ~**|~ ~ ~ ~ ~ |
|Configure & send to Stakeholder|TBD|
|Stakeholder preliminary review|TBD|
|Stakeholder review|TBD|
|Handling RIDs, configure & send to Stakeholder|TBD|
|Stakeholder final review & approval|TBD|
|Kick‑off for LRR (formal)|TBD|
|Finalizing everything|TBD|
|**~ ~ ~ ~ ~ LRR — Development ~ ~ ~ ~ ~**|~ ~ ~ ~ ~ |
|Queue‑1 Soft deadline|TBD|
|Get information from industry (partners)|TBD|
|Validation of the docs readiness, booking the Review with Stakeholder|TBD|
|Queue‑1 Inner review|TBD|
|Queue‑1 Hard deadline|TBD|
|Kick‑off for CRR (informal)|TBD|
|**~ ~ ~ ~ ~ LRR — Review ~ ~ ~ ~ ~**|~ ~ ~ ~ ~ |
|Configure & send to Stakeholder|TBD|
|Stakeholder preliminary review|TBD|
|Stakeholder review|TBD|
|Handling RIDs, configure & send to Stakeholder|TBD|
|Stakeholder final review & approval|TBD|
|Kick‑off for CRR (formal)|TBD|
|Kick‑off for launch (formal)|TBD|
|Finalizing everything|TBD|
|**~ ~ ~ ~ ~ Launch ~ ~ ~ ~ ~**|TBD|
|**~ ~ ~ ~ ~ CRR — Development ~ ~ ~ ~ ~**|~ ~ ~ ~ ~ |
|Queue‑1 Soft deadline|TBD|
|Get information from industry (partners)|TBD|
|Validation of the docs readiness, booking the Review with Stakeholder|TBD|
|Queue‑1 Inner review|TBD|
|Queue‑1 Hard deadline|TBD|
|Kick‑off for ELR (informal)|TBD|
|**~ ~ ~ ~ ~ CRR — Review ~ ~ ~ ~ ~**|~ ~ ~ ~ ~ |
|Configure & send to Stakeholder|TBD|
|Stakeholder preliminary review|TBD|
|Stakeholder review|TBD|
|Handling RIDs, configure & send to Stakeholder|TBD|
|Stakeholder final review & approval|TBD|
|Kick‑off for ELR (formal)|TBD|
|Finalizing everything|TBD|
|**~ ~ ~ ~ ~ ELR — Development ~ ~ ~ ~ ~**|~ ~ ~ ~ ~ |
|Queue‑1 Soft deadline|TBD|
|Get information from industry (partners)|TBD|
|Validation of the docs readiness, booking the Review with Stakeholder|TBD|
|Queue‑1 Inner review|TBD|
|Queue‑1 Hard deadline|TBD|
|Kick‑off for Phase F (informal)|TBD|
|**~ ~ ~ ~ ~ ELR — Review ~ ~ ~ ~ ~**|~ ~ ~ ~ ~ |
|Configure & send to Stakeholder|TBD|
|Stakeholder preliminary review|TBD|
|Stakeholder review|TBD|
|Handling RIDs, configure & send to Stakeholder|TBD|
|Stakeholder final review & approval|TBD|
|Kick‑off for Phase F (formal)|TBD|
|Finalizing everything|TBD|

### （TBD) Phase E Tasks

1. **Engineering:**
   1. support the launch campaign.
   1. support the entity in charge of the operations & utilization following the terms of a business agreement.
   1. support the Flight Readiness Review (FRR), Operations Readiness Review (ORR), Launch Readiness Review (LRR), Commissioning Results Review (CRR), End‑of‑Life Review (ELR), & recurring products AR, & ensure implementation of the actions of those reviews.
   1. support the execution of all SE activities & provision of documents in support to anomaly investigations & resolutions. (NOTE Phase E & its reviews as presented in A.1.1.1.<1>Table A‑1 refer only to mission level. In case of lower level product, activities to be considered by the SE function are only related to maintenance & anomaly investigations.)
1. **Management:**
   1. Perform all activities at space & ground segment level to prepare the launch.
   1. Conduct all launch & early orbital operations.
   1. Perform on‑orbit verification (incl. commissioning) activities.
   1. Perform all on‑orbit operations in order to achieve the mission objectives.
   1. Perform all ground segment activities in order to support the mission.
   1. Perform all other ground support activities in order to support the mission.
   1. Finalize the disposal plan.

### Phase E Documents list

**FRR:**

1. 017 Interface control document (ICD)
1. 021 Product user manual / User Manual (PUM/UM)
1. 026 Risk assessment report (RAR)
1. 029 Schedule
1. 031 Space debris mitigation plan (SDMP)
1. 032 Specification tree (ST)
1. 039 Test procedure
1. 040 Test report
1. 041 Test specification (TS)
1. 043 Verification control document (VCD)
1. 045 Verification report (VR)
1. Final presentation

**LRR:**

1. 017 Interface control document (ICD)
1. 021 Product user manual / User Manual (PUM/UM)
1. 031 Space debris mitigation plan (SDMP)
1. 032 Specification tree (ST)
1. 040 Test report
1. 041 Test specification (TS)
1. 043 Verification control document (VCD)
1. 045 Verification report (VR)
1. Final presentation
1. （Deleted） 026 Risk assessment report (RAR)
1. （Deleted） 029 Schedule
1. （Deleted） 039 Test procedure

**CRR:**

1. 021 Product user manual / User Manual (PUM/UM)
1. 032 Specification tree (ST)
1. 040 Test report
1. 041 Test specification (TS)
1. 043 Verification control document (VCD)
1. 045 Verification report (VR)
1. Final presentation
1. （Deleted） 031 Space debris mitigation plan (SDMP)

**ELR:**

1. 021 Product user manual / User Manual (PUM/UM)
1. 032 Specification tree (ST)
1. 040 Test report
1. 041 Test specification (TS)
1. 043 Verification control document (VCD)
1. 045 Verification report (VR)
1. 【NEW】 031 Space debris mitigation plan (SDMP)
1. （Deleted） 017 Interface control document (ICD)



## Phase F

### Phase F Schedule

1. …

### （TBD) Phase F Tasks

1. …

### Phase F Documents list

**MCR:**

1. 021 Product user manual / User Manual (PUM/UM)
1. 031 Space debris mitigation plan (SDMP)
1. 032 Specification tree (ST)
1. 041 Test report
1. 042 Test specification
1. 044 Verification control document
1. 046 Verification report


## Documents for ECSS



### As‑­built configuration list — DRD

**D.1 DRD identification.** **Requirement ID & source** — ECSS‑M‑ST‑40 §1.1.1.1.1. **Purpose & objective** — the objective of the **as‑­built configuration list (ABCL)** is to provide a reporting instrument defining the as­built status per each serial number of configuration item subject to formal acceptance.

**D.2 Expected response**

**Special remarks** — None.

**Scope & content:**

1. **Introduction.** The purpose & objective of the ABCL.
1. **Applicable & reference documents.** The applicable & reference documents in support to the generation of the document.
1. **List.** List all discrepancies between the as­‑designed configuration documented in the configuration item data list & the as­‑built configuration documented by nonconformance reports or waivers. The ABCL configuration item breakdown section, obtained from the equivalent configuration item data list section, shall be completed by adding the following information:
   1. serial number identification
   1. lot or batch number identification
   1. reference(s) of applicable nonconformance report(s) or request for waiver(s)



### Assembly, integration & test plan (AIT Plan) — DRD

**A.1 DRD identification.** **Requirement ID & source** — ECSS‑E‑ST‑10‑03 §4.3.3.2a. **Purpose & objective** — the **assembly integration & test plan (AIT Plan)** is the master plan for the product AIT process. It describes the complete AIT process & demonstrates together with the verification plan how the requirements are verified by inspection & test. It contains the overall AIT activities & the related verification tools (GSE & facilities), the involved documentation, the AIT management & organization. It also contains the AIT schedule. It is one of the major inputs to the project schedule & is used to provide the customer a basis for review & evaluation of the effectiveness of the AIT programme & its proposed elements. An AIT Plan is prepared for the different verification levels covering in detail the AIT activities at that level & outlining the necessary lower level aspects. The AIT Plan is complementary to the verification plan. It takes into account the test standards defined in the Customer requirements. The availability of the verification plan is a prerequisite to the preparation of the AIT Plan.

**A.2 Expected response**

**Special remarks** — None.

**Scope & content:**

1. **Introduction.** The purpose, objective, content & the reason prompting its preparation. State & describe open issues, assumptions & constraints relevant to this document.
1. **Applicable & reference documents, Definitions & abbreviations.** The applicable & reference documents in support to the generation of the document. List the applicable dictionary or glossary & the meaning of specific terms or abbreviations utilized in the document.
1. **Product presentation.** Briefly describe the selected models & their built status with reference to the verification plan (see ECSS‑E‑ST‑10‑02).
1. **Assembly, integration & test programme**
   1. Document the AIT activities & associated planning.
   1. Include test matrix(ces) that link the various tests with the test specifications, test procedures, test blocks & hardware model.
   1. Detail AIT programmes (including inspections) through dedicated activity sheets.
   1. Activity sheets shall include descriptions of the activity including the tools & GSE to be used, the expected duration of the activity, & the relevant safety or operational constraints.
   1. The sequencing of activities should be presented as flow charts.
1. **GSE & AIT facilities.** List & describe the GSE, test software & AIT facilities to be used. Describe the logistics & list the major transportations.
1. **AIT documentation.** Describe the AIT documents to be produced & their content.
1. **Organization & management**
   1. Describe the responsibility & management tools applicable to the described AIT process with reference to ECSS‑E‑ST‑10‑02.
   1. Describe the responsibilities within the project team, the relation to product assurance, quality control & configuration control (tasks with respect to AIT) as well as the responsibility sharing with external partners. NOTE Tasks with respect to AIT include for example, anomaly handling, change control, safety, & cleanliness.
   1. State the planned reviews & the identified responsibilities.
1. **AIT schedule.** Provide the AIT schedule as reference.



### Configuration item data list (CIDL) — DRD

**C.1 DRD identification.** **Requirement ID & source** — ECSS‑M‑ST‑40 §1.1.1.1.1. **Purpose & objective** — the **configuration item data list (CIDL)** is a document generated from the central database giving the current design status of a **configuration item (CI)**, at any point of time in sufficient detail, providing its complete definition.

**C.2 Expected response**

**Special remarks** — None.

**Scope & content:**

1. **Introduction.** The purpose & objective of the CIDL.
1. **Applicable & reference documents.** The applicable & reference documents in support to the generation of the document.
1. **List.** Include the following (each entry in the CIDL shall relate to the applicable model(s) of the CI & be defined by its document number, title, issue & release date):
   1. **Cover sheets & scope**
   1. **Customer controlled documentation**, including:
      1. customer specifications & ICDs
      1. support specifications
   1. **Engineering or design documentation**, including:
      1. verification plans demonstrating compatibility with specified requirements
      1. special instructions or procedures (For example, transportation, integration, & handling)
      1. declared PMP lists
      1. lower level specifications
      1. lower level ICDs
      1. drawings & associated lists
      1. test specifications
      1. test procedures
      1. users manual or handbook
   1. **List of applicable changes not yet incorporated into the baselined documentation**, & deviations (including status, directly related to the document (e.g. specification) to which the change belongs to)
   1. **CI number breakdown**
      1. indentured breakdown by part numbers starting from the part number associated with the CI down to the lowest level of composition
      1. quantity of each part number used on the next higher assembly
      1. issue status of the drawings & associated lists applicable to each part number



### Configuration item list — DRD

**B.1 DRD identification.** **Requirement ID & source** — ECSS‑M‑ST‑40 §1.1.1.1.1. **Purpose & objective** — the objective of the **configuration item list (CIL)** is to provide a reporting instrument defining the programme/project items subject to configuration management process.

**B.2 Expected response**

**Special remarks** — None.

**Scope & content:**

1. **Introduction.** The purpose & objective of the CIL.
1. **Applicable & reference documents.** The applicable & reference documents in support to the generation of the document.
1. **List.** Include for each **configuration item (CI)** the following information:
   1. identification code (derived from the product item code)
   1. models identification & quantity
   1. CI name
   1. CI category (developed, non­developed)
   1. CI supplier
   1. applicable specification



### Configuration management plan — DRD

**A.1 DRD identification.** **Requirement ID & source** — ECSS‑M‑ST‑40 §1.1.1.1.1a. **Purpose & objective** — the **configuration management plan (CMP)** is to provide all elements necessary to ensure that the implementation of **configuration management (CM)** meets customer requirements & is commensurate with the programme/project, organization, & management structure.

**A.2 Expected response**

**Special remarks.** The response to this DRD **may be combined with the Project Management Plan**, as per ECSS‑M‑ST‑10.

**Scope & content:**

1. **Introduction.** The purpose & objective prompting its preparation.
1. **Applicable & reference documents.** The applicable & reference documents in support of the generation of the document.
1. **Management**
   1. **Organization.** ➀ Describe the organizational context (technical & managerial) within which the prescribed CM activities shall be implemented. ➁ Describe the interface with other CM organizations (customer, supplier) & the CM relationship to other internal organization elements. ➂ Describe the interface for the information management activities.
   1. **Responsibilities.** The allocation of responsibilities & authorities for CM activities to organizations & individuals within the programme/project structure.
   1. **Policies, directives & procedures.** External constraints or requirements, placed on the CMP by other policies, directives, or procedures together with the consequences of applying these to the programme/project.
   1. **Security.** Customer & suppliers shall indicate within the CMP the classification classes to be applied & the relevant documentation set.
1. **Configuration Management**
   1. **General.** Identify all functions & processes (technical & managerial) required for managing the configuration of the programme/project; introduce the following, as a minimum: ➀ configuration identification, ➁ control, ➂ status accounting, ➃ audits & reviews, ➄ interface control, ➅ supplier control.
   1. **Configuration identification**
      1. Identify, name, & describe the documented physical & functional characteristics of the information to be maintained under CM control for the programme/project.
      1. Describe the following, as a minimum: ➀ product tree establishment (System decomposition), ➁ identification of configuration items, i.e. the selection process of the items to be controlled & their definitions as they evolve or are selected, ➂ naming of configuration items, i.e. the specification of the identification system for assigning unique identifiers to each item to be controlled, ➃ configuration baselines establishment & maintenance, ➄ interface identification, ➅ configuration documentation & data release procedures
      1. In case of software configuration, item & where a tool is used to build its baseline, describe the following: ➀ procedure to enter a software configuration item into a baseline, ➁ procedure to configure & establish a baseline, ➂ software products & records to define a baseline, ➃ procedure to approve a baseline, ➄ authority to approve a baseline
      1. In case software libraries are established, identify the following: ➀ library types, ➁ library locations, ➂ media used for each library, ➃ library population mechanism, ➄ number of identical libraries & the mechanism for maintaining parallel contents, ➅ library contents & status of each item included in, ➆ conditions for entering a SCI, including the status of maturity compatible with the contents required for a particular software library type, ➇ provision for protecting libraries from malicious & accidental harm & deterioration, ➈ software recovery procedures, ➉ conditions for retrieving any object of the library, ⑪ library access provisions & procedures
   1. **Configuration control.** Describe the configuration control process & data for implementing changes to the configuration items & identify the records to be used for tracking & documenting the sequence of steps for each change; describe the following, as a minimum: ➀ configuration control board functions, responsibilities, authorities, ➁ processing changes, ➂ change requests, ➃ change proposal, ➄ change evaluation, ➅ change approval, ➆ change implementation, ➇ processing planned configuration departures (deviations), ➈ processing unplanned configuration departures (product nonconformances, waivers).
   1. **Interface control.** Describe the process & data for coordinating changes to the programme/project CM items with changes to interfaces.
   1. **Supplier control.** ➀ For both subcontracted & acquired products (i.e. equipment, software, service) define the process & data to flow down the CM requirements & the programme monitoring methods to control the supplier. ➁ Define the process & data to incorporate the supplier developed items into programme/project CM & to coordinate changes to these items. ➂ Describe how the product is received, tested, & placed under configuration control.
   1. **Configuration status accounting.** Describe the process & data for reporting the status of configuration items. The following minimum info shall be tracked & reported for each CM item: ➀ status of the configuration baselines, ➁ design status of the configuration item, ➂ status of configuration documentation & configuration data sets, ➃ status of approval of changes & deviations & their implementation status, ➄ status of discrepancies & actions arising from technical reviews & configuration verification reviews
   1. **Configuration verification.** ➀ Describe the process & data to verify the current configuration status from which the configuration baselines are established. ➁ Describe verification plans, procedures & schedules. ➂ Identify how the recording, reporting & tracking of action items & incorporation of review recommendations are maintained.
   1. **Audits of CM system.** Describe the process, data & schedule for configuration audits to ensure that the CM of the programme/project is performed. As a minimum, the CMP shall enable that the CM process is properly defined, implemented & controlled, & the CM items reflect the required physical & functional characteristics.
   1. **Technical data management.** Describe the process & data to access & maintain text & CAD files, data & software repositories, & the implementation of any PDM system.
1. **Information/documentation  management**
   1. **Information identification.** ➀ Describe the main information/documentation categories (management information, contractual documentation or engineering data) to be established & used throughout the programme/project life cycle. ➁ Describe methods for information/document identification including versioning. ➂ List the codes for companies, information types, models, subsystems, etc. which are applied specifically in the identification method or are in general use during project execution. ➃ Identify the metadata structures of the main information categories.
   1. **Data formats.** ➀ Define for the various information categories the data formats to be used for content creation & update, distribution, archiving. ➁ Specify which format takes precedence in case a format conversion is applied.
   1. **Processes.** ➀ Describe the actors involved in, as well as the method & processes for, creating, collecting, reviewing, approving, storing, delivering & archiving information items. ➁ Describe the handling of legacy documentation & “off‑the‑shelf” documentation. ➂ Define the information retention period.
   1. **Information systems.** List the project information systems to be used for creating, reviewing, storing, delivering & archiving the main information categories (e.g.: ABC for schedule, & XYZ for engineering DB).
   1. **Delivery methods.** Describe the methods used to deliver TDPs.
   1. **Digital signature.** Define the procedures, methods & rules applicable to digital signatures. This comprises information about the following aspects: certificate type, management of signature key, time stamping, signing of PDF documents, multiple signatures per document.
   1. **Information status reporting.** Describe the process & content for reporting the status of information items. For documentation the following attributes shall be reported as a minimum: document identification, version, title, issue date, status, & document category.
1. **Schedule & resources**
   1. **Schedule.** Establish the sequence & coordination for all the CM activities & all the events affecting the CM plan’s implementation.
   1. **Resources.** Identify the tools, techniques, equipment, personnel, & training necessary for the implementation of CM activities.



### Configuration status accounting reports — DRD

**F.1 DRD identification.** **Requirement ID & source** — ECSS‑M‑ST‑40 §1.1.1.1.1. **Purpose & objective** — the purpose of **configuration status accounting reports (CSAR)** is to provide a reliable source of configuration information to support all programme/project activities. They provide the knowledge base necessary for performing configuration management.

**F.2 Expected response**

**Special remarks** — None.

**Scope & content:**

1. **Introduction.** The purpose & objective of the CSAR.
1. **Applicable & reference documents.** The applicable & reference documents in support to the generation of the document.
1. **Description**
   1. Documents index & status list
      1. document identification
      1. issue & revision
      1. issue & revision date
      1. title
      1. remarks
      1. identification of obsolete documents
   1. Drawing index & status list
      1. drawing identification
      1. issue & revision
      1. issue & revision date
      1. title
      1. remarks
      1. model applicability of the drawing
   1. Request for deviation index & status list
      1. deviation document number
      1. issue & revision
      1. issue & revision date
      1. title
      1. document or requirement affected
      1. configuration item(s) affected
      1. status of approval
   1. Request for waiver index & status list
      1. waiver document number
      1. issue & revision
      1. issue & revision date
      1. title
      1. document or requirement affected
      1. NCR originating the waiver
      1. serial number of configuration item(s) affected
      1. status of approval
   1. Change proposal (CP) index & status list
      1. CP document number
      1. issue & revision
      1. issue & revision date
      1. title
      1. change request originating the change
      1. configuration item(s) affected
      1. status of approval
   1. Review item discrepancies (RID) index & status list
      1. design review identification
      1. RID identification
      1. title of the discrepancy
      1. affected document number & issue
      1. affected paragraph
      1. RID status
      1. identification of action assigned
      1. RID closure date
      1. remarks



### Coordinate Systems Document (CSD) — DRD

**A.1 DRD identification.** **Requirement ID & source** — ECSS‑E‑ST‑10‑09 §5.2.2a. **Purpose & objective** — proper documentation & maintenance of coordinate systems & their inter‑relationships, throughout the product tree & the project/mission life, shall be initiated with sufficient detail. Standard requirements for the document & related database would help to regularise conventions even earlier.

**A.2 Expected response**

**Special remarks.** The CSD **can be part of the SEP** (as per ECSS‑E‑ST‑10).

**Scope & content:**

1. **Introduction.** The purpose, objective & the reason prompting its preparation (e.g. programme, project & phase).
1. **Applicable & reference documents.** The applicable & reference documents supporting the generation of the document.
1. **Convention & Notation**
   1. Specify the international conventions used within the project for coordinate systems & transformations.
   1. Define the naming, notation & acronym rules for coordinate systems, as well as for transformations.
   1. Define notation conventions for the transfer of coordinates & transformation data.
1. **Units.** Specify the units pertaining to the coordinate systems & parameterisations used within the project.
1. **Time standards.** Specify the time standards used within the project & the relationship between them.
1. **Coordinate system, overview.** The overview of the various coordinate systems. Refer to the theory & conventions applied in between the various coordinate systems (e.g. precession, nutation, polar motion). Include a brief description of at least the following coordinate systems, if applicable:
   1. Inertial Coordinate Systems
   1. Orbital Coordinate Systems
   1. Launcher Coordinate Systems
   1. Satellite‑fixed Coordinate System (generic platform & payload)
   1. Body‑fixed Rotation (planet) Coordinate Systems
   1. Topocentric Coordinate Systems
   1. Test facility Coordinate Systems
   1. Simulator Coordinate Systems
   1. Processing/Product Coordinate Systems
1. **Parameterisations.** Describe all parameterisations used within a coordinate system (e.g. azimuth, elevation), including the applicable type of coordinate systems where this parameterisation is allowed. Describe all parameterisations used within a transformation (e.g. quaternions, Euler angles), including the applicable type of coordinate systems where this parameterisation is allowed.
1. **Diagrams.**
   1. Describe in a graphical representation the top‑level chain of transformations for the project.
   1. The diagram (previous sentence) shall include any additional constraints between coordinate systems, as well as measurements.
   1. Describe in a graphical representation the lower level chains of transformations.
   1. Each individual coordinate system, identified in <9> below, shall be  graphically represented.
   1. These diagrams shall contain any additional constraints between coordinate systems, as well as measurements.
1. **Coordinate systems, details**
   1. Give the detailed description of all Coordinate Systems used in the project.
   1. The transformation between coordinate systems shall be defined in the one that is the parent coordinate system.
   1. Parameterisations within a coordinate system shall be identified & specified (e.g. right ascension & declination).
   1. The parameterisation of a transformation shall be identified & specified (e.g. quaternions).
   1. Any source document for a coordinate system shall be identified in the CSD.



### Correlation report — DRD

**C.1 DRD identification.** **Requirement ID & source** — ECSS‑E‑ST‑31 §1.1.1.1.1 & 1.1.1.1.1. **Purpose & objective** — the TCS analysis report is produced by the thermal control subsystem supplier to gather & document the results of analysis. The first issue is produced for the Phase A & updated throughout the project as further thermal control subsystem analysis & testing is carried out. The present DRD is also used to present test prediction & correlation with respect to major subsystem tests, such as Thermal Balance Test as well as for thermal control system flight prediction. The results of all analysis including test prediction & correlation carried out at subsystem level are reported in TCS analysis report. The documents are therefore critical for tracking the development of the thermal control subsystem throughout the project, ensuring that the TCS continues to meet the functional & performance requirements as the design & implementation are elaborated.

**C.2 Expected response**

**Special remarks** — None.

**Scope & content:**

1. **Introduction.** The purpose, objective, content, assumption & constraint & the reason prompting its preparation. Any open issue relevant to the TCS analysis report shall be stated & described..
1. **Applicable & reference documents, Definitions & abbreviations.** The applicable & reference documents in support to the generation of the document. List the applicable directory or glossary & the meaning of specific terms or abbreviations utilized in this document.
1. **Subsystem status.** Each issue of the TCS analysis report shall define the underlying design status of the TCS (development‑, test‑, qualification‑, protoflight‑, flight‑ model). Each issue of the TCS analysis report shall identify / evaluate analysis constraints with respect to in‑orbit configuration.
1. **Requirements review.** A list of subsystem requirements to be met by analysis.
1. **Overview of analysis approach.** An overview of the analysis approach applied to the TCS. Describe the different analysis techniques used. Include a description of tools used to carry out the analysis. Refer to GMM & TMM used for each analysis case.
1. **Description of thermal analysis cases.** Include a list of assumptions made concerning the thermal control subsystem & its environment during the analysis. Include a list of all input data including starting conditions for the analysis cases. Include a justification of assumed uncertainties.
1. **Results of analysis.** Include the results of the analyses represented as diagrams, temperature mapping, detailed comparison to requirements, heat balance. Include a compliance matrix for applicable requirements. The results of the analysis shall be presented as electronic file as agreed with the system authority.
1. **Assessment & conclusions**
   1. Summarize the analysis results & the comparison with the requirements.
   1. Provide an overall assessment.
   1. The requirement closeout may be summarized in dedicated tables to be prepared for each involved requirement or a group of requirements. (For examples see ECSS‑E‑HB‑10‑02)
   1. Any open issue shall be clearly stated & described.
   1. Based on analysis results recommendations for design changes & adaptations at subsystem & system level shall be formulated.


### Cost estimate report — DRD

**G.1 DRD identification.** **Requirement ID & source** — ECSS‑M‑ST‑60 §1.1.1.1.1cc. **Purpose & objective** — the objective of the **cost estimate report (CER)** is to provide the project or programme cost estimate, along with the details how the estimates were determined, what are the main cost & risk drivers & a list of recommendations that need to be taken into consideration at project/programme management level.

**G.2 Expected response**

**Special remarks.** Sections [4 ‑ 8] shall be repeated for each option or alternates. Section [9] shall synthesize findings for baseline & all options & alternates.

**Scope & content:**

1. **Introduction.** The purpose & objective of the CER; a brief description of what was done during the cost estimate exercise, & its outcome; identification of organizations that contributed to the preparation of the document.
1. **Applicable & reference documents.** The applicable & reference documents, used in support to the generation of the document.
1. **Quality of estimate.** Characterize the expected quality level & features of the estimate. This may be done using references to codes & standards. (For example: AACE International recommended best practice No 17R‑97)
1. **Hypothesis used.** Describe what the hypothesis were made to perform the cost estimating analysis, including:
   1. Economic conditions
   1. Currency
   1. Inclusions & exclusions
   1. Heritage/Technology Readiness Level
   1. Design status
   1. Engineering difficulty
   1. Hardware matrix
   1. Spares philosophy
   1. Identification of main cost drivers
   1. Market situation (e.g. monopoly, & open competition)
   1. Cost sharing
1. **Method & Cost model(s) description.** The supplier shall indicate for each of the cost items which method was used.
1. **Cost breakdown tables.** The customer & the supplier shall mutually agree in advance on the template format & the level of details to be produced.
1. **Cost sensitivity analysis.** Identify, rank & describe how the main cost drivers influence the global cost.
1. **Cost Risk assessment.** This assessment shall be based on the risk register. If absent at the time of the assessment, a register shall be constituted at this stage & for this purpose.
   1. Identify the main cost risk items & the associated mitigations proposed.
   1. The supplier shall refer to its cost estimating plan (as per ECSS‑M‑ST‑60 Ann.F) & to ECSS‑M‑ST‑80.
   1. The supplier shall provide cost figures associated with confidence level. Unless otherwise agreed with the customer, the supplier shall present ranges at agreed confidence level with the customer.
   1. In case of stochastic analysis, the supplier shall explain how the distribution profiles are chosen & parameters fixed for the different cost items.
1. **Recommendations.** Summarize the findings, restate the accuracy & confidence level to be given to the analysis, & make all necessary recommendations for the sake of the proper execution of the project/programme. Based on the cost sensitivity analysis & the identified risk drivers, the cost estimating report shall identify any recommendation that need to be taken into consideration at project/programme management level.



### Design definition file (DDF) — DRD

**G.1. DRD identification.** **Requirement ID & source** — ECSS‑E‑ST‑10 §5.3.1.1c., §5.3.1.1f., §5.4.1.1b. & §5.4.1.4a. **Purpose & objective** — the objective of the **design definition file (DDF)** is to establish the technical definition of a system or product that complies with its technical requirements specification (as per ECSS‑E‑ST‑10‑06 Ann.A). The DDF is a basic structure referring to all information relative to the functional & physical architectures & characteristics of a system or product, necessary for its identification, manufacturing, utilization, support, configuration management & removal from service. The DDF is a collection of all documentation that establishes the system or product characteristics such as lower level technical specifications, design & interface description, drawings, electrical schematics, specified constraints (e.g. on materials, manufacturing, processes, & logistic). It details the as‑designed configuration baseline (as per ECSS‑M‑ST‑40) of the system or product & is built up & updated under the responsibility of the team in charge of SE. It’s the technical baseline for the production, assembly, integration & test, operations & maintenance of the product. The DDF, the technical requirements specification, & the Design Justification File (as per ECSS‑E‑ST‑10 Ann.K) are the basic documents used for product development. They are interrelated such as: the design (i.e. DDF) is the response to the requirements stated in the TS, the justification (i.e. DJF) demonstrates the conformance of the design (i.e. DDF) to the requirements stated in the TS. (NOTE The DDF is a logical file covering all TS disciplines required for the considered system. In general, the elements of the DDF are “rolled out” as separate documents.)

**G.2. Expected response**

**Special remarks** — None.

**Scope & content:**

1. **Introduction.** The purpose, objective & the reason prompting its preparation (e.g. programme or project reference & phase).
1. **Applicable & reference documents.** List of applicable & reference documents, used in support to the generation of the document. Refer to DDFs of next higher & lower level products. Include the reference to the following applicable documents: ➀ Business agreement, ➁ System engineering plan as per ECSS‑E‑ST‑10 Ann.D, ➂ Coordinate system document as per ECSS‑E‑ST‑10‑09 Ann.A, ➃ Technical requirements specification, ➄ Product assurance plan, ➅ Configuration management plan as per ECSS‑M‑ST‑40, ➆ Configuration status report as per ECSS‑M‑ST‑40.
1. **Summary of the project & technical requirements**
   1. A brief description of the product & of the main technical requirements throughout its life cycle phases (e.g. launch, deployed, operations, end‑of‑life).
   1. The description of the system or product design documentation, based on the Product Tree (as per ECSS‑M‑ST‑10) & also include, or refer to, the Specifications Tree (as per ECSS‑E‑ST‑10 Ann.J).
   1. A system shall contain at least the technical requirements specifications of the elements in which the system is broken down.
1. **Functional description**
   1. **Functional architecture.** The description of the functional architecture of the system or product i.e. the arrangement of functions, their sub‑functions & interfaces (internal & external), & the performance requirements to satisfy the requirements of the TS. Present the data & their flow interchanged between the different functions, the conditions for control, & the execution sequencing for the different operational modes & states. (NOTE The Functional Architecture is an output of the functional analysis process (as per ECSS‑E‑ST‑10 §5.3.1.1a.))
   1. **Function tree.** Include or refer to the system or product Function Tree, the latter conforming to ECSS‑E‑ST‑10 Ann.H.
   1. **Description of functional chains.** Describe the functional chains that contribute to the realization of the functional requirements of the TS & their contributing functions, consider the different operational modes & states, & indicate the selected physical implementation for each of the functions.
1. **Physical description**
   1. **Physical architecture.** The description of physical architecture of the system or product i.e. the arrangement of elements, their decomposition, interfaces (internal & external), & physical constraints, which form the basis of a system or product design to satisfy the functional architecture & the technical requirements. (NOTE The Physical Architecture is an output of the preliminary design definition activities (as per §5.3.1.1f.).)
   1. **Product tree.** Include or refer to the product tree of the system or product, as per ECSS‑M‑ST‑10 Ann.B. (NOTE The Product Tree is a breakdown of the Physical Architecture)
   1. **Specification tree.** Include or refer to the specification tree of the system or product, the latter conforming to Ann.J.
   1. **Description of elements of the physical architecture.** Reference any documentation containing detailed technical descriptions & associated matrices to ensure overall consistency & completeness. Provide:
      1. the nomenclature of the system or product,
      1. the overall system or product drawings,
      1. for each element of the system, the description of the different constituents of the physical architecture,
      1. the characteristics of the respective elements,
      1. their configuration management identifier (e.g. hardware part number, software version number, drawings number, electrical schematics numbers).
   1. **Description of interfaces.** Describe the physical & functional characteristics of the internal & external interfaces of the system & refer to the relevant IRD & ICD, conforming to ECSS‑E‑ST‑10‑24 Ann.A.
1. **System technical budget, margins & deviations.** Present the budget allocation of the technical parameters of the system & provide the actual status of the system margins, & deviations. Include or refer to the system or product technical budget, the latter conforming to Ann.I.
1. **System design constraints**
   1. **Constraints for production.** Present the constraints induced by the system or product design definition on the production activities e.g. operational allowable envelopes, restrictions on assembling sequences, procedures & testing modes, exclusion zones, manufacturing environmental conditions, & conditions for procurement.
   1. **Constraints for operation.** Present the constraints induced by the system or product design definition on the implementation of the operations e.g. operational allowable envelopes, restrictions on operating modes, & exclusion zones.
   1. **Constraints for transportation & storage.** Present the constraints induced by the system or product design definition on the transportation activities & during the periods of storage of the product e.g. allowable envelopes, restrictions on transportation & storage, exclusion zones, packaging, shock levels, temperature environments, humidity, cleanliness, regulations, & dangerous materials.
   1. **Constraints for maintainability.** Present the constraints induced by the system or product design definition on the maintenance activities & procedures e.g. operational allowable envelopes, accessibility, tooling, support materials, parts availability, & deliveries.
1. **Engineering data repository.** Provide the information on the system or product engineering data repository that contains the complete set of design parameters, or a reference to it. (NOTE Information about the set of design parameters is provided in ECSS‑E‑TM‑10‑10)
1. **Conclusion.** List & summarize all deviations of the design with respect to the technical specifications & constraints induced by the system or product design definition.



### Design justification file (DJF) — DRD

**K.1. DRD identification.** **Requirement ID & source** — ECSS‑E‑ST‑10 §5.3.1.1d, §5.3.1.1g, §5.3.1.1h & §5.4.1.4b. **Purpose & objective** —  to present the rationale for the selection of the design solution, & to demonstrate that the design meets the baseline requirements. The DJF is progressively prepared during the detailed design process & according to the system engineering plan (SEP) (as per ECSS‑E‑ST‑10 Annex D), & serves the following purposes: ➀ it provides access to the necessary justification information, ➁ it presents a review of all acquired justifications, ➂ it constitutes a basic element for decision taking concerning the product definition qualification. The DJF together with the Design Definition File (DDF) (as per Annex G) & the technical requirements specification (TS) (as per ECSS‑E‑ST‑10‑06 Annex A) are the basic documents used for the development of the product. These documents are used to monitor the evolution of the design. The DJF is a collection of all documentation that traces the evolution of the design during the development & maintenance of the product. The DJF is updated according to the evolution of the DDF, in accordance with the above‑mentioned objectives. The DJF provides also access to coherent & substantiated information which can be used to support decision‑making in the analysis of change requests for the management of non conformances. The DJF contains results obtained during the evolution of the design as a consequence of the activities performed along the design process: ➀ analysis & trade‑off reports concerning the evaluation of alternative design solutions & the justification of the choice; ➁ all results obtained during the verification of the design as a consequence of the activities performed along the verification process; ➂ test Reports on engineering model, structural & thermal model & qualification model (e.g. Protoflight Models). The DJF is a logical file covering all technical disciplines required for the considered system. In general, the elements of the DJF are “rolled out” as separate documents.

**K.2. Expected response**

**Special remarks** — None.

**Scope & content:**

1. **Introduction.** The purpose, objective, content & the reason prompting its preparation.
1. **Applicable & reference documents.** The applicable & reference documents in support to its generation. Refer to the relevant product specifications, the relevant DDF & SEP, &: ➀ Trade‑Off‑Reports, as defined Ann.L, ➁ Analysis Reports, e.g. requirements allocation analysis, functional analysis, ➂ Requirements Traceability Matrix, as per Ann.N with link to analysis, ➃ Verification Control Document, ➄ All verification documentation, such as: Analysis Reports (e.g. reports w.r.t. qualification aspects), Test/Inspection Reports, ROD Reports, Verification Reports
1. **Design description.** A description of the expected product, its intended mission, architecture & design, & the functioning principles on which it is based. Define the requirement criteria levels for qualification & acceptance verification of the product.
1. **Design Justification File Synthesis.** Present status of the design justification in response to requirements, with emphasis on the driving requirements that have a big impact on the system design, production & maintainability (see also K.2.1<8.2.4>a.). Present an overall system qualification status synthesis, including:
   1. the list of requirements which have not been met (e.g. nonconformances), including proposed actions
   1. the list of all critical points, & how criticalities have been or are intended to be resolved
   1. the identification of requirements which have not been justified yet, & associated risks analysis, with emphasis on those that can have an impact at system level
1. **Justification of the Functional Architecture.** Demonstrate that all requirements of the preliminary technical requirements specification are allocated to functional blocks of the system functional architecture. Where requirements assigned to functional blocks do not have their origin within any of the customer preliminary technical specifications, these requirements shall be justified.
1. **Justification of the Physical Architecture.** Demonstrate that the system design conforms to the requirements of the technical specification, & identify products which are reused (e.g. COTS). Provide the justification for the choice of architectural elements at the next lower level, or lower levels in case of system critical elements. Where requirements do not have their origin within any of the upper level technical specifications, these shall be justified.
1. **Development activities & synthesis of development results**
   1. Present the development activities (e.g. assessments, analyses, tests, & trade‑offs) & the design drivers, which lead to & justify the design as per the DDF, in line with the development approach identified in the SEP.
      1. The justification shall concern all the engineering disciplines contributing to design & development of the product (including its operational modes & scenarios).
      1. Include the status of DJF of lower level products. (NOTE Activities related to verification are dealt with in section K.2.1<8>.)
   1. For the system & each discipline, following information shall be produced:
      1. Activity inputs, such as requirements, operational modes, assumptions, analysis cases, boundary conditions, model descriptions & limitations.
      1. Activity results, such as: (a) raw results, (b) evaluation of results, (c) evaluation of margins with respect to the technical requirements contained in the TS, (d) identification of any marginal areas.
      1. Activity synthesis, such as: (a) evidence of compliance to the technical requirements contained in the TS, (b) list of technical requirements which have not been met, including proposed actions, (c) list of all critical points, & how criticalities have been or are intended to be resolved, (d) identification of aspects of the design, which are not yet justified, & assessment of inherent risks.
   1. Refer to the requirements traceability matrix, e.g. w.r.t. building up the justification of a considered system top level requirements in terms of the various elements contributing to it, including where relevant contribution from other disciplines (e.g. pointing as a function of thermal, structures, & AOCS).
1. **Verification activities & synthesis of results**
   1.  **Verification plan.** Integrate or refer to the document that conforms to the verification plan DRD defined in ECSS‑E‑ST‑10‑02 Ann.A. (NOTE The verification activities are detailed in the Verification Plan (VP), which also contains the justification of the verification strategy (as per ECSS‑E‑ST‑10‑02).)
   1. **Qualification verification & synthesis of results**
      1. Qualification evidence. Present the evidence of the qualification of the design in conformance to the applicable technical requirements & proper qualification margins. (NOTE This is done in line with the qualification approach identified in the VP). Cover the system & all disciplines relevant to the product in all its operational modes & scenarios, addressing all applicable technical requirements & proper qualification margins. (NOTE 1 This is done in line with the system verification matrix. NOTE 2 The formal compliance with the qualification requirements is recorded in the VCD, together with references to the close‑out documents.)
      1. Implementation of the qualification plan. Present the implementation of the qualification plan & the status thereof, addressing the detailed definition of qualification activities (e.g. analysis, test, ROD, & inspection), including the detailed definition of the tests, the prediction of expected test results, test success criteria, test specifications, & model validations. (NOTE Details on test specifications are provided in ECSS‑E‑ST‑10‑03 Ann.B)
   1. **Validation of models.** Provide all evidence (e.g. analyses, test results, & model descriptions & correlations) regarding the suitability & validation of all models used for the analysis of the system.
   1. **Requirements status log.** Include a requirement status log addressing each requirement in turn, & including the:
      1. reference to relevant elements of the verification plan,
      1. synthesis of the justifications acquired, calling up references to the supporting activities & evidence (e.g. Technical Notes listed in section K.2.1<4>),
      1. list of justifications to be acquired & related activities,
      1. conclusion / action flag.
   1. **Manufacturing process status log.** Include a requirement status log, addressing design relevant aspects of manufacturing processes, & recording their characteristics in regard to qualification.
   1. **Acceptance verification.** Present the implementation of the acceptance verification & the status thereof, addressing the detailed definition of acceptance activities (e.g. inspection, test, analysis), including the detailed definition of the tests, the prediction of expected test results, test success criteria, & test specifications. (NOTE Details on test specifications are provided in ECSS‑E‑ST‑10‑03 Ann.B). Cover the system & all disciplines relevant to the product, addressing all acceptance verification activities in line with the system verification Plan (VP).
1. **Justification of System Technical Budgets & Margins.** The DJF shall present a synthesis of all technical budgets & margins for specific parameters according to the functional & physical architectures. (NOTE For technical budgets & margins, see ECSS‑E‑ST‑10 Ann.I)
1. **Justification of Constraints imposed by the System Design**
   1. **Design constraints on the production.** Present the justification of constraints induced by the system or product design definition on the production activities e.g. operational allowable envelopes, restrictions on assembling sequences, procedures & testing modes, exclusion zones, manufacturing environmental conditions, & conditions for procurement.
   1. **System design constraints for operation.** Present the justification of constraints induced by the system or product design definition on the implementation of the operations e.g. operational allowable envelopes, restrictions on operating modes, & exclusion zones.
   1. **System design constraints for transportation & storage.** Present the justification of constraints induced by the system or product design definition on the transportation activities & during the periods of storage of the product e.g. allowable envelopes, restrictions on transportation & storage, exclusion zones, packaging, shock levels, temperature environments, humidity, cleanliness, regulations, & dangerous materials.
   1. **System design constraints for maintainability.** Present the justification of constraints induced by the system or product design definition on the maintenance activities & procedures e.g. operational allowable envelopes, accessibility, tooling, support materials, parts availability, & deliveries.
1. **Constituent documents.** Include or refer to the DJF of lower level elements of the product. Integrate or refer to the documents that conform to the:
   1. ECSS‑E‑ST‑10 Ann.L, Trade‑Off‑Report — DRD
   1. ECSS‑E‑ST‑10 Ann.Q, Analysis Report — DRD
   1. ECSS‑E‑ST‑10 Ann.O, Requirement Justification File — DRD
   1. ECSS‑E‑ST‑10 Ann.N, Requirements Traceability Matrix — DRD
   1. ECSS‑E‑ST‑10‑02 Ann.B, Verification Control Document — DRD
   1. ECSS‑E‑ST‑10‑02 Ann.C, Test Report — DRD
   1. ECSS‑E‑ST‑10‑02 Ann.D — Review Of Design Report — DRD
   1. ECSS‑E‑ST‑10‑02 Ann.E, Inspection Report — DRD
   1. ECSS‑E‑ST‑10‑02 Ann.F, Verification Report — DRD



### Function tree (FT) — DRD

**H.1. DRD identification.** **Requirement ID & source** — ECSS‑E‑ST‑10 §5.3.1.1b. **Purpose & objective** — to describe the hierarchical decomposition of a system/product capabilities into successive level of functions & sub‑functions. It’s the starting point for the establishment of the Product Tree (as per ECSS‑M‑ST‑10) & is a basic structure to establish Preliminary Technical Requirements Specification(s) (as per ECSS‑E‑ST‑10‑06 Ann.A).

**H.2. Expected response**

**Special remarks.** The FT **is part of the Design Definition File**. The FT shall be coherent with other functional descriptions of the system/product (e.g. functional architecture, functional block diagram).

**Scope & content:**

1. **Introduction.** The purpose, objective & the reason prompting its preparation.
1. **Applicable & reference documents.** List of applicable & reference documents, used in support to the generation of the document.
1. **Project summary & user’s need presentation.** A brief description of the project & of the key user’s needs.
1. **Tree structure**
   1. The complete list of functions that the system/product shall perform, & contain a graphical representation where the main specified function(s) (i.e. at the top level of the tree) is/are decomposed into lower level functions.
   1. When recurrent products from previous space projects are used, identify the product’s functions in the tree structure, & addition, every necessary function by the system/product that is not under the supplier’s responsibility identified in the tree structure.



### Interface Control Document (ICD) — DRD

**B.1 DRD identification.** **Requirement ID & source** — ECSS‑E‑ST‑10‑24 §1.1.1.1.1r. **Purpose & objective** — the **Interface Control Document (ICD)** is to define the design of the interface(s) ensuring compatibility among involved interface ends by documenting form, fit, & function. The ICD can be, either: ➀ a single self‑standing document defining the interface; ➁ a single document, referencing other documents (e.g. IDD, single‑end ICD, interface drawings), which define separately the involved interface ends; ➂ a single document Annexing a set of IDDs (or single‑end ICDs), made integrally & jointly applicable to all the involved interface ends. In this case, the entire set of documents (issued by the suppliers, coordinated by the customer & approved by all the involved actors) is integrally & jointly controlling the interface: therefore, the rules & requirement hereafter specified are intended to be applicable either to the single ICD or jointly to the entire set of documents, depending on the selected approach. The ICD is managed by the customer (or his delegate) & concurred by all the involved actors. The ICD is used: ➀ to document the interface definition, ➁ to control the evolution of the interface, ➂ to document the design solutions to be adhered to, for a particular interface, ➃ as one of the means to ensure that the supplier design (and subsequent implementation) are consistent with the interface requirements, ➄ as one of the means to ensure that the designs (& subsequent implementation) of the participating interface ends are compatible.

**B.2 Expected response**

**Special remarks** — None.

**Scope & content:**

1. **Introduction.** The purpose, objective, content & the reason prompting its preparation.
1. **Applicable & reference documents, Definitions & abbreviations.** The applicable & reference documents & standards in support to the generation of the document. Clearly define the relationship & precedence of the ICD to other programme documents. Include the following references: Product tree (as per ECSS‑M‑ST‑10 Ann.B), Specification tree (as per ECSS‑E‑ST‑10). Define the applicable dictionary or glossary & the meaning of specific terms or abbreviation utilized in ICD.
1. **Responsibility.** State the responsibilities of the interfacing organizations for development of the ICD. Define the document approval authority (including change approval authority). State the interface ends responsible.
1. **Interface definition.** The interface responsible shall establish a complete & exhaustive list of technical characteristics of the interface to be reflected in the interface definition/data. NOTE Informative Annex E lists a number of example aspects to be considered in interface definition. The list is meant to be used as a starting point but cannot be considered exhaustive.
   1. For each identified & specified interface, the interface responsible accounting for the contribution of the involved actors shall define interface characteristics in terms of:
      1. interface plane definition (Examples: mounting surface (between hardware products), Application Program Interface (between software products), communication network (between data systems))
      1. interface behaviour identification, including limitation of use (Examples: static, dynamic, nominal/off nominal, state machine. Possible limitation of use is coming from a decrease of capability)
      1. interface description using tables, figures, drawings, models, data base as appropriate (Examples: mechanical drawing, interface 3D CAD model, electrical circuit schematic, software API, software architectural or detailed design document)
      1. data affecting the interface definition (Examples: dimensions, tolerances, coordinates, voltage, data format, temperature, load, heat flow, material, surface treatment, external standard reference)
      1. units of measure including scale of measure
      1. tolerances or required accuracies
      1. in case of using non‑SI quantities or units, conversions & conversions.
   1. Detail the interface definition between two (or more) interface ends.
   1. Group the ICD by contractual, discipline or product decomposition (product tree).
   1. Each ICD version shall identify the incorporated approved CR.
   1. Group the interface definitions per pair of interface ends having a common interface plane.
   1. In case the ICD addresses more than one interface, the interface definition shall be grouped either per physical/logical interface, or per nature of interface.
   1. An example of physical/logical grouping is arranging the interface data by a pair of interface ends (see Figure B.1). An example of grouping by nature is arranging the interface data by discipline such as mechanical, electrical, thermal & software/data (see Figure B.1). It’s a good practice to group the interface requirements by the interface natures defined in Annex E.

【**Figure B.1** — Examples of interface data grouping in ICDs】

1. **A**  <= α,β =>  **B** <= α,β =>  **C**
   1. Interface data groupling by phisycal/logical interface
      1. A <= => B
         1. Interface data 1 (interface nature α)
         1. Interface data 2 (interface nature β)
         1. …
      1. B <= => C
         1. Interface data 3 (interface nature α)
         1. Interface data 4 (interface nature β)
         1. …
   1. Interface data grouping by interface nature
      1. Interface nature α
         1. Interface data 1 (interface nature α)
         1. Interface data 3 (interface nature α)
         1. …
      1. Interface nature β
         1. Interface data 2 (interface nature β)
         1. Interface data 4 (interface nature β)
         1. …



### Interface Requirements Document (IRD) — DRD

**A.1 DRD identification.** **Requirement ID & source** — ECSS‑E‑ST‑10‑24 §1.1.1.1.1j. **Purpose & objective** — for a product, the **interface requirements document (IRD)** is a specific type of technical requirements specification that defines the requirements for an interface or a collection of interfaces. The IRD is a document either included in or called up by a technical requirements specification (TS) as per ECSS‑E‑ST‑10‑06.

**A.2 Expected response**

**Special remarks.** The content of the IRD **may be merged with the Technical Requirements Specification** (as per ECSS‑E‑ST‑10‑06 Annex A) of the product.

**Scope & content:**

1. **Introduction.** The purpose, objective, content & the reason prompting its preparation.
1. **Applicable & reference documents, Definitions & abbreviations.** The applicable & reference documents in support to the generation of the document. Clearly define the relationship & precedence of the IRD to other project documents. Include the following references: ➀ Product tree (as per ECSS‑M‑ST‑10 Annex B), ➁ Specification tree (as per ECSS‑E‑ST‑10), ➂ References to requirements specified in other Technical requirement specifications or Standards which are applicable for a particular Interface (for example, an IRD can refer to individual requirements coming from Technical Requirement Specification such as GDIR (General Design & Interface Requirements) or SSS (System Support Specification) or Standards such as MIL‑STD‑1553 Bus). Confirm that the IRD does not include duplicate or conflicting requirements already specified in another Technical requirements specification applicable to any interface end. Define the applicable dictionary or glossary & the meaning of specific terms or abbreviation utilized in IRD.
1. **Interface requirements**
   1. Define the physical, functional, procedural & operational interface requirements between 2+ items in the Product tree & ensure hardware & software compatibility. (NOTE Interface requirements include for example, physical measures, definitions of sequences, of energy or information transfer, design constraints, & all other significant interactions between items)
   1. Specify Interface Requirements in accordance with ECSS‑E‑ST‑10‑06.
   1. Accompanie each interface requirements by its verification requirements.
   1. Define the verification responsibility for each verification requirement as per A.2.1<4>f (this can be joint responsibility, stand‑alone responsibility or any combination of them).
   1. Specify the requirements of an interface, for all identified interface ends.
   1. Specify for each requirement the applicability to each interface end.
   1. In case of an existing interface end design, the interface requirements shall address: ➀ interface plane definition, ➁ interface behaviour definition, ➂ data affecting the interface definition.
   1. In addition to the general requirements defined in ECSS‑E‑ST‑10‑06, for each interface requirement specify the applicability to each interface end.
   1. In case the IRD addresses more than one interface, group the interface requirements either by interface end pair, by nature of interface, or by contractual party. (An example of grouping by nature of interface is arranging the requirements by discipline such as mechanical, electrical, thermal & software/data; see also Annex E)
1. **Items to be addressed**
   1. Address for each interface, the following aspects: ➀ interface description using tables, figures, or drawings, ➁ units of measure including scale of measure, ➂ tolerances or required accuracies, ➃ in case of using non‑SI quantities or units, conversions & conventions, ➄ interface plane specification, ➅ coordinate system specification, in case of interfaces involving geometric aspects, ➆ interface behavior specification, including limitation of use. (Example of interface plane is the separation plane between launch adapter & spacecraft; example of limitation of use is decrease in capability)
   1. Interface requirements shall be built starting from the Reference Interface Data List of Annex E & adding all data necessary to specify the interface. It’s good practice to group the interface requirements by the interface natures as per Annex E.



### Inspection report — DRD

**E.1 DRD identification.** **Requirement ID & source** — ECSS‑E‑ST‑10‑02 §5.3.2.4b. **Purpose & objective** — the **inspection report (IR)** describes each verification activity performed for inspecting hardware or software. The IR contains proper evidence that the relevant requirements are verified & the indication of deviations.

**E.2 Expected response**

**Special remarks.** The IR **may be a part of the Test Report if** the verification by Inspection is carried‑out in combination with Testing.

**Scope & content:**

1. **Introduction.** The purpose, objective, content & the reason prompting its preparation. State & describe open issues, assumptions & constraints relevant to this document.
1. **Applicable & reference documents, Definitions & abbreviations.** The applicable & reference documents in support to the generation of the document. The applicable dictionary or glossary & the meaning of specific terms or abbreviations utilized in the document with the relevant meaning.
1. **Inspection summary.** Describe the product configuration data of the inspected item.
1. **Conclusions.** Summarize the following (clearly state & describe open issues):
   1. inspection results, including: ➀ the list of the requirements to be verified (in correlation with the VCD), ➁ traceability to used documentation, ➂ inspection event location & date, ➃ expected finding, ➄ conformance or deviation including proper references & signature & date
   1. comparison with the requirements
   1. verification close‑out judgment



### Mathematical model description & delivery (MMDD) — DRD

**I.1 DRD identification.** **Requirement ID & source** — ECSS‑E‑ST‑32 §1.1.1.1.1. **Purpose & objective** — a mathematical model is associated with its **mathematical model description & delivery document (MMDD)**, which contributes to a correct use of the model & to the understanding of its results. The MMDD is fundamental for traceability of the mathematical models & indicates & lists all the changes of the delivered model. The MMDD provides a description of the structural mathematical model (named below “mathematical model” or simply “model”) & of the performed quality checks.

**I.2 Expected response**

**Special remarks** — None.

**Scope & content:**

1. **Introduction.** The purpose, objective, applicability, content & the reason prompting its preparation.
1. **Applicable & reference documents, Definitions & abbreviations.** The applicable & reference documents in support to the generation of the document. The terms & definitions, abbreviated terms, & symbols used.
1. **Structure (or structure component) description**
   1. **Unit system.** Indicate the consistent unit system of measures used. The units for mass, force, length, time, temperature & angles shall be explicitly documented.
   1. **General description, drawings.** Describe the structure & introduce to the terminology for major structure components. Reference shall be made to the available drawings. Reference the set of the applied drawings in order to explain the actual status of the design & to define the structure components to be analyzed.
1. **Coordinate system.** Describe all the coordinate systems used in the model, by giving, for each coordinate system, the following information; include a brief description to explain the use of the coordinate system (e.g. by listing the model items using the coordinate system): ➀ the label number, ➁ the type (rectangular, cylindrical, spherical), ➂ origin position & axes orientation, ➃ data card used to define it.
1. **Mathematical model outline**
   1. **Assumption, idealizations & limitations.** Summarize & include a justification of the modelling assumptions & methodology; include the following: ➀ justification of used element types, spring & rigid connections, rigid body  & relationships, ➁ model adequacy to study specific structure behaviour (e.g. non‑linear phenomena, local‑global buckling, & contact), ➂ model limitations (e.g. limitation on type of analyses can be performed or on output can be found), ➃ significant model output: stress, frequencies
   1. **Numbering.** If any special rule is applied for model numbering, it shall be reported, with reference to: ➀ nodes, ➁ elements, ➂ element properties, ➃ materials, ➄ constraints, ➅ forces, ➆ analysis cases
   1. **Mathematical model summary.** Include a mathematical model summary as a table that summarizes model data, showing the total number of each type of data.
   1. **Analysis code compatibility.** Indicate the analysis code, (e.g. NASTRAN, SAMCEF, ABAQUS.), which the model is designed for. If the model can be used with more than a specific code, these shall be indicated.
   1. **Recommended analysis parameters.** If special parameters are required to correctly use the model with a specific code, then these parameters shall be specified.
   1. **Pre‑ & post processors compatibility.** Indicate the pre & post‑processor used for modelling, by underlying software limitations & recommendations to properly handle the model.
   1. **Compliance with model requirements.** Summarize the requirements, if any, that the model is compliant with, e.g. maximum number of nodes, numbering ranges, & recommendations in using specific elements.
1. **Finite element modelling**
   1. **General & information for each major structural item.**
      1. Give the detailed description of the mathematical model.
      1. Each of the major structural items into which the product can be split shall be described independently.
      1. The description of each of the major structural items into which the product can be split shall include the information in <7.2> until <7.7>.
      1. The MMDD shall give a brief description of the each item, with reference to the previous clause "General description of the structure" & to available project documents.
      1. A figure or drawing shall be included, showing the physical structural item.
   1. **Modelling assumption.** Underline if any assumptions of relevant significance have been introduced. If no significant assumptions have been used in the modelling of the item, this shall be explicitly written, by introducing the following sentence: “No special assumptions are to be underlined”.
   1. **Idealization.** The mathematical model of each structural item shall be described in detail, indicating the: ➀ type of the used elements, ➁ number of nodes & elements, ➂ rigid connections, ➃ interfaces with other items. Include a figure reproducing the each idealized item, in order to direct compare the idealized & the physical structural item.
   1. **Model: nodes & elements.** Indicate label ranges of nodes k elements of each item model. Declare logic applied for node & element labelling, if any. Produce plots of each element & model, showing node & element labels.
   1. **Model: properties & material.** Indicate properties & materials related to the item k. Reproduce code input card (e.g. property & material input), including relevant comments.
   1. **Critical parameters.** Highlight & comment any data parameter specific of each item k if deemed critical for model performance & reliability (e.g. damping coefficient in the frequency response analysis, large mass in transient analysis).
   1. **Interfaces with other items.** Describe the interfaces between each item k & other items, in terms of common nodes & connecting elements.
1. **Masses**
   1. **Density of structural masses.** Indicate structural masses included in the model & related mass density (Some plots can be used to show the parts of the structure with the same mass density). If no structural mass is included in the model, declare this explicitly.
   1. **Lumped masses.** Describe lumped masses & reproduce related input data. Indicate the position of the masses also by plotting the model & labelling the masses. If lumped masses are not included in the model, declare this explicitly.
   1. **Distributed non‑structural masses.** If distributed non‑structural masses are present in the model, a description shall be provided. Report the value of the distributed mass. Use model plots to indicate where the distributed non‑structural masses are smeared. If distributed non structural masses are not included in the model, declare this explicitly.
   1. **Global inertia properties.** Report Centre of Gravity (COG) position, total mass & other inertia properties computed by the analysis code.
   1. **Source documents of mass distribution.** Provide reference to the documents used to establish the mass distribution of the mathematical model.
1. **Loads.** Describe the model load sets. Each load set shall be independently described, including the following: ➀ List types of load included in each load set k (e.g. forces, line distributed loads, pressure, gravity) & related values shall be indicated, ➁ Give total resultant forces & moment (w.r.t. a specified point), in code output format.
1. **Multi‑point constraints & single‑point constraints**
   1. **General.** Describe the set of point constraints & relationships. Describe each set independently, including the information from below.
   1. **Set information**
      1. Multi‑point constraint (relationships between DOFs): Describe each multi‑point constraint set in terms of connected nodes & degrees of freedom. Compare the model to the physical structure & explain modelling assumptions. Use model plots & comparison with structure drawings, as a mean of presentation.
      1. Single‑point constraint: Describe each single‑point constraint set in terms of constrained nodes & degrees of freedom. Compare the model to the physical structure & explain modelling assumptions. Use model plots & comparison with structure drawings, as a mean of presentation.
1. **Analysis cases.** Describe the analysis cases (An analysis case is defined by associating a constraint set to an analysis set).
1. **Miscellaneous model topics.** Collect any other topic of interest.
1. **Model checks**
   1. **Model geometry checks.** Report the results of dedicated checks performed to assess the geometry correctness.
   1. **Elements topology checks.** Report the results of dedicated checks performed to assess the elements topology correctness.
   1. **Rigid‑body‑motion strain energy check.** Report the results of dedicated checks performed in order to ensure that neither strain energy nor nodal residual forces arise due to rigid body motions of the model. Report value of strain energy & residual forces due to rigid body motions at different set of DOFs:
      1. At the set including all model DOFs.
      1. At the set obtained by removing all dependent DOFs in the multi‑point constraints.
      1. At the set obtained by removing also DOFs constrained by single‑point constraints.
   1. **Static analysis check.** Report the results of dedicated checks performed to assess model adequacy to perform static analysis.
   1. **Thermal‑elastic analysis check.** Report the results of dedicated checks performed to assess adequacy to perform thermal stress analysis.
   1. **Normal mode analysis check.** Report the results of dedicated checks performed to assess the adequacy of the model to perform normal mode related dynamic analyses.
1. **Mathematical model changes.** If a model changes, describe the changes. The clauses affected by model changes shall be indicated & updated. If additional model checks have been performed on the delivered model, then document, number & address the check results.
1. **Conclusion**
   1. **Mathematical model use in structural analysis.** On the basis of the model checks that have been performed, include a clarification if the model satisfies the purposes for which it has been created, by indicating the analysis types the model is capable of performing.
   1. **Mathematical model limitations.** A concluding remark on the limits of the model shall underlined the: ➀ analyses that cannot be performed, ➁ behaviour of the structure that cannot be studied, ➂ responses that cannot be given.
   1. **Suggested future implementation.** Provide suggestions to improve the responses of the model, including: ➀ how to modify the model to study other effects, ➁ how to increase the accuracy of the analysis modifying particular areas.



### Mission description document (MDD) — DRD

**B.1. DRD identification.** **Requirement ID & source** — ECSS‑E‑ST‑10 §5.3.1a. **Purpose & objective** — to provide input for the later selection of the best concept meeting the mission statement (MS) in iteration with the preparation of the preliminary technical requirements specification (TS) (as per ECSS‑E‑ST‑10‑06 Ann.A). It’s prepared in Phase 0/A for each possible concept, as per §5.3.1a. Links & chronology amongst the MS, TS, MDD, SE plan, project management plan & system concept report are provided on the Figure B‑1. The MDD is produced by the top‑level customer (as described in ECSS‑S‑ST‑00 §6.1 & figure 6‑1) (typically an Agency or other institutional actors) & defines a concept that aims at satisfying the preliminary technical requirements specification, & presents how the objectives, operation profile, major system events & capabilities, contingencies & performance standards are expected to be achieved. For each mission concept, the MDD is a complete description of that concept. And to each MDD a SEP evaluating the related SE effort & a report evaluating the related programmatic aspect are associated. The system concept report assesses the different concepts from a technical, programmatic & risk point of view, includes a trade‑off including weighting factors which bears some management aspects, followed by a system concept selection.

【**Figure B‑1:** Relationship between documents】

1. Step 1: Analyse Mission statement
1. Step 2: Identify Preliminary Technical Requirements
1. Step 3: Identify Mission Concepts
1. Step 4: Prepare for each Concept: MDD, System Engineering Plan, Project Management Plan
1. Step 5: Put the results of the above into the System Concept Report

**B.2. Expected response**

**Special remarks** — None.

**Scope & content:**

1. **Introduction.** The purpose, objective, content & the reason prompting its preparation (e.g. logic, organization, process or procedure).
1. **Applicable & reference documents.** The applicable & reference documents in support to the generation of the document, & include, as a minimum, the current preliminary technical requirements specification.
1. **Preliminary technical requirements specification summary.** A summary of the preliminary technical requirements specification objectives & list the design driving requirements, derived from the current initial specification.
1. **Concept description.**
   1. Overview of the concept
   1. Mission analysis
   1. System description, element by element. (For example: For a spacecraft, its ground control segment, & a user segment, e.g.. Space Segment: Payload, Platform, Launch Vehicle, Orbit related aspects, On‑Board Data Handling, Reference Operation Scenarios / Observation characteristics, Operability / Autonomy Requirements. Ground Segment: Functional Requirements & Major Elements, Monitoring & Control Segment, Data Processing Segment. User segment: Functional Requirements & Major Elements, Monitoring requirements.)
   1. Description of how the system works in each mission phase. (For example: For a spacecraft, the following phase: Launch preparation, Launch & Early Orbit Phase, In Orbit Commissioning, Nominal Operations, Spacecraft Disposal)
      1. Performance drivers
      1. Constraints
      1. Main events
      1. Operations scenarios
1. **Assessment of the performance.** Assessment against the current preliminary technical requirements specification requirements. Identification of non‑compliances, & their impact on the current preliminary technical requirements specification.
1. **Identification of risk areas.** A list of identified risk related to the concept, including as a minimum technology, contingencies handling, & programmatic aspects.
1. **Conclusion.** Summarize the strengths & weaknesses of the concept.



### Product tree (PT) — DRD

**B.1 DRD identification.** **Requirement ID & source** — ECSS‑M‑ST‑10 §5.3a. **Purpose & objective** — to describe the hierarchical partitioning of a deliverable product down to a level agreed between the customer & supplier. It’s the starting point for selecting configuration items (as specified in ECSS‑M‑ST‑40) & establishing the work breakdown structure. It’s a basic structure to establish the Specification tree (as per ECSS‑E‑ST‑10).

**B.2 Expected response**

**Special remarks.** The product tree **is part of the design definition file (DDF)**.

**Scope & content:**

1. **Introduction.** The purpose, objective & the reason prompting its preparation (e.g. program/project reference & phase).
1. **Applicable & reference documents.** The applicable & reference documents used in support of the generation of the document.
1. **Tree structure.**
   1. Provide the breakdown of lower level products constituting the deliverable product.
   1. For each item identified in the product tree, provide: ➀ identification code, ➁ item name, ➂ item supplier, ➃ applicable specification.
   1. Present the PT either as a graphical diagram or an indentured structure where the product (i.e. at the top level of the tree) is decomposed into lower level products.
   1. Identify in the PT each product item selected as configuration item.
   1. Identify in the PT which recurrent products from previous space projects are used.



### Progress report — DRD

**E.1 DRD identification.** **Requirement ID & source** — ECSS‑M‑ST‑10 §5.2.2.2a. **Purpose & objective** — to provide all actors with actual information concerning the status of the project.

**E.2 Expected response**

**Special remarks** — None.

**Scope & content:**

The progress report shall contain the following information:

1. The project manager’s assessment of the current situation in relation to the forecasts & risks, at a level of detail agreed between the relevant actors.
1. The status of the progress of work being performed by the supplier.
1. Status & trends of agreed key performance & engineering data parameters.
1. Adverse trends in technical & programmatic performance & proposals for remedial actions.
1. Planning for implementation of remedial actions.
1. A consolidated report derived from the lower tier suppliers status reports.
1. Progress on all actions since the previous report.


### Product Assurance Plan (PAP) — DRD

**A.1 DRD identification.** **Requirement ID & source** — ECSS‑Q‑ST‑10 §5.1.3.1b. **Purpose & objective.** The objective of the **Product Assurance Plan (PAP)** is to describe the activities to be performed by the supplier to assure the quality of the space product with regard to the specified mission objectives & to demonstrate compliance to the applicable PA requirements.

**A.2 Expected response**

**Special remarks.** The response to this DRD **may be combined with the Project Management Plan**, as per ECSS‑M‑ST‑10. The response to this DRD may be performed by reference to separate discipline plans addressing some of the above clauses of this DRD.

**Scope & content:**

1. **Introduction.** The purpose, objective & the reason prompting its preparation. (For example: programme or project reference & phase)
1. **Applicable & reference documents.** The applicable & reference documents in support of the generation of the document.
1. **Product assurance management**
   1. **PA Planning.** Describe the organization (including responsibilities & authorities), the activities, processes & procedures to be applied by the supplier to fulfil the applicable product assurance planning requirements defined in §5.1 of ECSS‑Q‑ST‑10.
   1. **PA implementation.** Describe the activities, processes & procedures to be applied by the supplier to fulfil the applicable product assurance implementation requirements defined in §5.2 of ECSS‑Q‑ST‑10.
1. **Quality assurance.** Describe the activities, processes & procedures to be applied by the supplier to fulfil the applicable quality assurance requirements.
1. **Dependability.** Describe the activities, processes & procedures to be applied by the supplier to fulfil the applicable dependability requirements.
1. **Safety.** Describe the activities, processes & procedures to be applied by the supplier to fulfil the applicable safety requirements.
1. **EEE components.** Describe the activities, processes & procedures to be applied by the supplier to fulfil the applicable EEE Component requirements.
1. **Materials & processes.** Describe the activities, processes & procedures to be applied by the supplier to fulfil the applicable Material & Processes requirements.
1. **Software product assurance.** Describe the activities, processes & procedures to be applied by the supplier to fulfil the applicable Software product assurance requirements.
1. **Other PA requirements.** Describe the activities, processes & procedures to be applied by the supplier to fulfil all other applicable PA requirements not covered in the §A.2.1.1.<3> to §A.2.1.1.<9>. (The order of the sections is not mandatory. For example: Security, Planetary protection, Off‑The‑Shelf, Customer furnished equipment, Integrated logistic support, Production preparation, Launch system exploitation, Involvement of relevant Surveillance Authority Representatives, National Surveillance Organizations for Launch Segment)



### Product user manual (PUM or UM) — DRD

**P.1. DRD identification.** **Requirement ID & source** — ECSS‑E‑ST‑10 §5.4.1.4c. **Purpose & objective** — the objective of the **product user manual (PUM)** is to provide information on design, operations & data of the product that is required by the user to handle, install, operate, maintain & dispose the product during its life time.

**P.2. Expected response**

**Special remarks** — Where the objective is to allow for the accommodation of equipment designed a posteriori w.r.t an existing platform or vehicle, the following docs shall be part of the UM: the accommodation handbook describing the location, mounting, all interfaces & clearances of equipment in a platform or vehicle; the installation plan describing the approach, methods, procedures, resources & organization to install, commission, & check the operation of the equipment in its fixed operational environment.

**Scope & content:**

1. **Introduction.** Describe the purpose & objective of the PUM.
1. **Applicable & reference documents.** The applicable & reference documents in support of the generation of the document.
1. **Product function definition**
   1. **Product expected functions.** Provide a general description of the expected functions of the product during its lifetime in expected operational context & environment.
   1. **Product functional constraints.** Describe all product functional constraints.
   1. **Life time phases & purposes**
      1. Address the whole product life cycle & all its modes: Handling, Storage, Installation, Operations (nominal & contingency), Maintenance, Disposal
      1. Consider potential consequences of the environment on those sequences (e.g. sensor blinding, eclipses)
1. **Product description**
   1. **Design summary.** Include the following:
      1. summary of the product design, showing the definition of the product, its constituents, the distribution of functions & the major interfaces;
      1. block diagram of the product;
      1. top‑level description of the product software architecture;
      1. description of nominal product operations scenarios & constraints e.g. mutually exclusive modes of operation, power or resource sharing.
   1. **Product level autonomy.** Include the following:
      1. description of product‑level autonomy provisions in the areas of fault management (FDIR);
      1. definition, for each autonomous function, of the logic or rules used & of its internal (product constituents) & external interfaces.
   1. **Product configurations.** Include the following:
      1. drawings of the overall product configuration in all product modes;
      1. definition of the product reference axes system(s);
      1. drawings of the product layouts.
   1. **Product budgets.** Provide the distribution (or allocation) of the following budgets, per product constituent, or per operating mode, as appropriate:
      1. mass properties;
      1. alignment;
      1. power consumption for all operational modes;
      1. thermal budget & constraints & predictions;
      1. Description of interfaces & related budgets. (e.g. RF links);
      1. telemetry & telecommand date rates;
      1. memory;
      1. timing.
   1. **Interface specifications.** Provide a cross‑reference to the applicable version of the ICD.
   1. **Handling.**
      1. Describe the conditions & procedures for the handling of the product, be it integrated or stand‑alone.
      1. Describe the specific design features, transport & environmental conditions, required GSE, & limitations for the handling of the product.
   1. **Storage**
      1. Describe the conditions & procedures for the storage of the product, be it integrated or stand‑alone.
      1. Describe the specific design features, environmental conditions, required GSE, monitoring requirements, life‑limited items, health maintenance procedures (activation, monitoring) & limitations for the storage of the product.
   1. **Installation**
      1. Describe the conditions & procedures for the installation of the product, be it integrated or stand‑alone.
      1. Describe the specific design features, required GSE, modes, environmental conditions, & limitations for the installation of the product.
   1. **Product operations**
      1. General. Include timelines, modes & procedures, constraints to operate the product during its life cycle in nominal & contingency conditions, & highlight critical operations. (NOTE 1 When the product is a space segment, the product operations aspects are included in a specific part of the UM called Flight Operations Manual (FOM). NOTE 2 The implementation of the FOM by the ground segment responsible organisation is contained in the Mission Operations Plan (MOP, as per ECSS‑E‑ST‑70 Ann.G).)
      1. Timelines.
         1. Include: Baseline event timelines for all nominal & contingency modes & phases; Related constraints.
         1. Each timeline shall contain a detailed description (i.e. down to the level of each single operational action) of the complete sequence of operations to be carried out, including a description of the rationale behind the chosen sequence of events, a definition of any constraints (e.g. absolute timing, relative timing) & the interrelationships between operations in the sequence.
      1. Product modes
         1. Describe all nominal & contingency modes, including:
            1. their purpose (i.e. circumstances under which they are used),
            1. the related procedures,
            1. operational constraints,
            1. resource utilization,
            1. the definition of the associated modes, and
            1. monitoring requirements.
         1. Describe the allowable mode transitions & the operations procedure corresponding to each such transition.
         1. Appropriate cross‑reference shall be made to product constituent modes & procedures.
      1. Product failure analysis
         1. Provide the results of the product failure modes, effects & criticality analysis (FMECA) & the resulting list of single point failures.
         1. Potential product failures shall be identified by means of a fault‑tree analysis (FTA).
   1. **Maintenance.** Describe the conditions, procedures & logistics for the maintenance of the product, be it integrated or stand‑alone. NOTE The description can refer to the document that conforms to the Integrated Logistic Support Plan in conformance with ECSS‑M‑ST‑70.
   1. **Disposal**
      1. Describe the conditions & procedures for the disposal of the product, be it integrated or stand‑alone.
      1. The procedures shall include passivation, as relevant.
      1. Identify the risks during & after disposal.
1. **Products constituents description**
   1. **General.** The information specified in P.2.1<5.2> to P.2.1<5.9> shall be provided for each product constituent.
   1. **Product constituent design summary.** Describe the product constituent including:
      1. the overall functions of the product constituent & the definition of its operational modes during the different mission phases;
      1. description of any product constituent management functions, fault management concept & redundancy provisions;
      1. a summary description of the component units/equipment & software including the functions which each supports;
      1. product constituent functional block diagrams & a diagram showing the source of telemetry outputs & the sink of telecommand inputs;
      1. interfaces;
      1. budgets.
   1. **Product constituent design definition.** The following shall be provided for each product constituent:
      1. a detailed design description, including block diagrams, functional diagrams, logic & circuit diagrams;
      1. physical characteristics including location & connections to the support structure, axes definition & alignment where relevant, dimensions & mass properties;
      1. principle of operation & operational constraints of the product constituent;
      1. lower level of breakdown for products composed of many complex elements.
   1. **Software**
      1. Include:
         1. description of software design,
         1. product constituent software,
         1. application process service software, and
         1. memory map.
      1. Describe the organization of the software & its physical mapping onto hardware.
      1. Describe the details of each software component i.e. scheduler, interrupt handler, I/O system, telecommand packet handling system, telemetry packet handling system, including for each component its functions, component routines, input/output interfaces, timing & performance characteristics, flowcharts & details of any operational constraints.
      1. For the application process service software, the PUM shall:
         1. describe the services implemented making cross‑reference to ECSS‑E‑ST‑70‑41 “Telemetry & telecommand packet utilization”, as tailored for the mission;
         1. summarize all telemetry & telecommand structures (e.g. packets) including the conditions under which they are generated, the generation frequency, content & interpretation.
      1. For each memory block, a map shall be provided showing RAM & ROM address areas, areas allocated for program code, buffer space & working parameters (e.g. content of protected memory).
   1. **Product component performance.** Describe all relevant product constituent performance characteristics, define the expected performance degradation as a function of time during the mission, & identify the resulting impact in terms of modifications to operational requirements or constraints.
   1. **Product component telemetry & telecommand lists**
      1. For each product constituent, the following lists shall be provided: a list of the housekeeping telemetry parameters; a list of the telecommands.
      1. Each housekeeping telemetry shall have a functional description with validity conditions, telecommand relationship, & all technical information necessary for using it.
      1. Each telecommand shall have a functional description with utilization conditions (e.g. pre‑transmission validity, criticality level), command parameters (syntax & semantics) & execution verification in telemetry.
   1. **Product component failure analysis.** Describe:
      1. Identification of potential product constituent failures by means of a systematic failure analysis (including a subsystem FMECA & FTA).
      1. Identification of the methods by which the higher levels can identify a failure condition from analysis of the telemetry data & isolate the source of the failure.
   1. **Product components operations**
      1. Describe:
         1. product constituent modes;
         1. nominal operational procedures;
         1. contingency procedures.
      1. product constituent modes shall be defined for all distinct nominal & back‑up modes of the subsystem including:
         1. purpose (i.e. conditions under which each is used);
         1. operational constraints;
         1. resource utilization;
         1. the definition of the associated modes for each product constituent & its software functions;
         1. higher level monitoring requirements;
         1. identification of the allowable mode transitions & any product constituent operational constraints.
      1. Nominal operational procedures shall be defined for each nominal mode transition identified under P.2.1<5.8>6..
      1. For each procedure described in P.2.1<5.8>c.., the following shall be provided:
         1. an introduction describing the purpose of the procedure & the phase(s) or conditions when applicable;
         1. the body of the procedure, structured according to operational steps, including:
            1. pre‑conditions for the start of the step defining, where applicable: product or product constituent level pre‑requisites (e.g. configuration & resource requirements, such as power, fuel); external interfacing products pre‑requisites.
            1. telecommands to be sent;
            1. telemetry data to be monitored to verify correct execution of the step;
            1. interrelationships between steps (e.g. conditional branching within the procedure, timing requirements or constraints, hold & check points);
            1. conditions for completion of the step.
         1. Contingency procedures shall be defined for each failure case identified in the product constituent failure analysis (FMECA/FTA). NOTE This can utilize a nominal operational procedure already identified in P.2.1<5.8>c.. above.
         1. For contingency procedures, the same details shall be provided as for nominal operational procedures in P.2.1<5.8>d.. above.
         1. Where the recovery method for a failure or group of failures is mode, mission, or phase dependent, separate procedures shall be described for each mode/mission phase.
      1. **Product component data definition**
         1. For each operational mode of the product constituent, sensor output data, conditions under which they are generated, their contents, & data rate shall be described.
         1. Required on‑board processing performed on sensor data & algorithms used for this shall be described.



### Project management plan (PMP) — DRD

**A.1 DRD identification.** **Requirement ID & source** — ECSS‑M‑ST‑10 §5.1.3a. **Purpose & objective** — to state the purpose & provide a brief introduction to the project management system. It covers all aspects of the latter.

**A.2 Expected response**

**Special remarks** — None.

**Scope & content:**

1. **Introduction.** The purpose, objective & the reason prompting its preparation (e.g. program or project reference & phase).
1. **Applicable & reference documents.** The applicable & reference documents used in support of the generation of the document.
1. **Objectives & constraints of the project.** Describe the objective & constraints of the project in conformance with the project requirements documents.
1. **Project organization.** Describe the project organization approach in conformance with the requirements as per §5.2.
1. **Project breakdown structures.** Describe the project breakdown structures approach in conformance with the project breakdown structure requirements as defined in §5.3 & identify the title of individual documents called up by these requirements.
1. **Configuration, information & documentation management.** Describe the configuration, information & documentation management approach, as per ECSS‑M‑ST‑40 Ann.A. If this approach is contained in a rolled‑out configuration management plan, the PMP may include only a brief description together with a reference to the configuration, information & documentation management plan.
1. **Cost & schedule management.** Describe the cost & schedule management approach, as per ECSS‑M‑ST‑60. If this approach is described in a rolled‑out cost & schedule management plan, the PMP may include only a brief description together with a reference to the cost & schedule management plan.
1. **Integrated logistic support.** Describe the integrated logistic support approach, as per ECSS‑M‑ST‑70.
1. **Risk management.** Briefly describe the risk management approach which is described in more detail in a rolled‑out risk management policy & plan, as defined in [ECSS‑M‑ST‑80](ecss_mst80.md), Ann. A & B.
1. **Product assurance management.** Describe the product assurance management approach, including the proposed breakdown into PA disciplines & the interfaces between these disciplines, as per ECSS‑Q‑ST‑10 Ann.A. If the product assurance management approach is described in a rolled‑out PA plan, the PMP may include only a brief description together with a reference to the product assurance plan.
1. **Engineering management.** Describe the engineering management approach, including the proposed breakdown into engineering disciplines & the interfaces between these disciplines, as per ECSS‑E‑ST‑10 Ann.D. If the engineering management approach is described in a rolled‑out system engineering plan, the PMP may include only a brief description together with a reference to the system engineering plan.



### Requirements justification file (RJF) — DRD

**O.1. DRD identification.** **Requirement ID & source** — ECSS‑E‑ST‑10 §5.2.1.1e. **Purpose & objective** — the **requirement justification file (RJF)** is a generic title referring to all documentation which: records & describes the needs & the associated constrains resulting from the different trade‑offs; demonstrates how the requirements of the technical requirements specification (TS) (as per ECSS‑E‑ST‑10‑06 Ann.A) at each level can satisfy the needs & the constraints of the TS of the level above.

**O.2. Expected response**

**Special remarks.** A top level RJF document is established at the upper level of the project structure that is, according to ECSS‑M‑ST‑10, the first‑level customer situated at level 0 of the customer supplier network. For other levels, the RJF **is part of the design justification file (DJF)** (as per Ann.K).

**Scope & content:**

1. **Introduction.** The purpose, objective, content & the reason prompting its preparation
1. **Applicable & reference documents.** The applicable & reference documents in support to the generation of the document. Include the reference to the technical requirements specification.
1. **Selected concept/solution justification.** Present the rationale for the selection of a concept for the technical requirements specification. (NOTE For justification, reference can be made to the system concept report of the considered project)
1. **Life profile justification for the selected concept/solution.** Present & justify the life profile situations for the concept presented in the technical requirements specification. (NOTE For justification, reference can be made to the system concept report of the considered project where relevant)
1. **Environments & constraints justification.** Present & justify the different environments & constraints for each life profile situations for the concept presented in the technical requirements specification. (NOTE For justification, reference can be made to the system concept report of the considered project where relevant)
1. **Technical requirements justification.** List all the technical requirements, & their identifier, expressed by the corresponding TS as they are organized in these documents. For each technical requirement the following information shall be provided:
   1. the justification of the requirement (i.e. justified source),
   1. the Entity or owner responsible for the requirement,
   1. if one technical requirement is induced by several sources, the reason of the specified performance,
   1. the justification of the quantified performance (such as the range, the approach used to determine the level, e.g. measure, estimation),
   1. the justification of the selected verification method.
1. **Requirement traceability.** Present the requirement traceability between the technical requirements of the TS, & their justified source.
1. **Compliance matrix for COTS.** For COTS, the RJF shall contain a compliance matrix between the technical specification/characteristics of the COTS & the technical requirement expressed by the TS.



### Requirements traceability matrix (RTM) — DRD

**N.1. DRD identification.** **Requirement ID & source** — ECSS‑E‑ST‑10 §5.2.2.1b. **Purpose & objective** — to define the relationships between the requirements of a deliverable product defined by a technical requirements specification & the apportioned requirements of the product's lower level elements. The point is to help verify that all stated & derived requirements are allocated to system components & other deliverables (forward trace). The matrix is also used to determine the source of requirements (backward trace). Requirements traceability includes tracing any information that satisfy the requirements such as capabilities, design elements, & tests. The RTM is also used to ensure that all requirements are met & to locate affected system components when there is a requirements change. The ability to locate affected components allows the impact of requirements changes on the system to be determined, facilitating cost, benefit, & schedule determinations.

**N.2. Expected response**

**Special remarks** — None.

**Scope & content:**

1. **Introduction.** The purpose, objective, content & the reason prompting its preparation.
1. **Applicable & reference documents.** The applicable & reference documents in support to the generation of the document. Include the following:
   1. Technical requirements specification (as per ECSS‑E‑ST‑10‑06 Ann.A) of the product & its lower level elements
   1. Product tree (as per ECSS‑M‑ST‑10 Ann.B)
   1. Specification tree (as per ECSS‑E‑ST‑10 Ann.J).
1. **Requirement traceability**
   1. List all the technical requirement of the product TS.
   1. List all the lower level elements constituting the product & their technical requirements (contained in the lower‑level element TS).
   1. The requirement identification shall be identical in the RTM & the different TS.
   1. Each technical requirement of the product shall be linked to at least one requirement of a lower level element. (NOTE The required visibility of the traceability down the elements of the product tree is depending on the criticality of some lower level elements w.r.t. the product requirements)
   1. Each technical requirement of a lower level element should be linked to a technical requirement of the product.
   1. When a technical requirement of a lower level element is not linked to a technical requirement of the product, this requirement shall be justified & an evaluation of its existence or removal on the product shall be agreed between the customer & the supplier.



### Review‑of‑design report (RDR) — DRD

**D.1 DRD identification.** **Requirement ID & source** — ECSS‑E‑ST‑10‑02 §5.3.2.3b. **Purpose & objective** — the **review‑­of‑­design report (RDR)** describes each verification activity performed for reviewing documentation. The RDR contains proper evidence that the relevant requirements are verified & the indication of deviations.

**D.2 Expected response**

**Special remarks** — None.

**Scope & content:**

1. **Introduction.** The purpose, objective, content & the reason prompting its preparation. State & describe open issues, assumptions & constraints relevant to this document.
1. **Applicable & reference documents, Definitions & abbreviations.** The applicable & reference documents in support to the generation of the document. The applicable dictionary or glossary & the meaning of specific terms or abbreviations utilized in the document with the relevant meaning.
1. **Review‑of‑design summary.** Describe the review‑of‑design activity in terms of method & procedures used.
1. **Conclusions.** Summarize the review‑of‑design results, including:
   1. the list of the requirements to be verified (in correlation with the VCD)
   1. traceability to used documentation
   1. conformance or deviation including references & signature & date
   1. the comparison with the requirements
   1. the verification close‑out judgment
   1. clearly state & describe open issues



### Risk assessment report (RAR) — DRD
**C.1 DRD identification.** **Requirement identification & source** — ECSS‑M‑ST‑80 §1.1.1.1.1k. **Purpose & objective** — The **risk assessment report (RAR)** is the basis for communicating the identified & assessed risks, as well as the subsequent follow‑up actions & their results.

**C.2 Expected response**

**Special remarks** — None.

**Scope & content:**

1. **Introduction.** The purpose & objective of the RAR, a brief description of what was done during the identification & assessment exercise, & its outcome, identification of organizations that contributed to the preparation of the document.
1. **Applicable & reference documents.** Applicable & reference documents, used to support the generation of the document.
1. **Overview.** Describe what was done during the identification & assessment exercise.
1. **Method of assessment.** Describe how the risks in question were identified, which inputs, method, tool(s) were used, & which people were involved.
1. **Principle.** Describe the basics of the identification & assessment method (e.g. interviewing method), including the justification for the method(s) selected.
1. **Consolidation.** Describe the consolidation approach for the overall risk assessment. Emphasize items of conflict & highlight the decisions that were taken for consideration in the overall assessment.
1. **Assessment.** An appraisal of identified individual risks & the overall project risk.
1. **Comparison with earlier assessments.** Describe results of the follow‑up actions that were taken in comparison with earlier assessment(s).
1. **Conclusions.** Describe the conclusions drawn from the identification & assessment, including any statements for future assessments & follow‑up actions.
1. **Annexes.** The RAR shall contain the following information:
   1. risk register (see ECSS‑M‑ST‑80 Ann.D)
   1. ranked risk log
   1. rating scheme
   1. overall risk rating
   1. other analysis

**Risk register example & ranked risk log example**

[Template file ❐](f/doc/ecss/ecss_mst80/risk_tables.odt)

![](f/doc/ecss/ecss_mst80/risk_table1.webp)

![](f/doc/ecss/ecss_mst80/risk_table2.webp)



### Risk management plan (RMP) — DRD

**B.1 DRD identification.** **Requirement identification & source** — ECSS‑M‑ST‑80 §1.1.1.1.1b. **Purpose & objective** — The objective of the **risk management plan (RMP)** is to provide all elements necessary to ensure that the implementation of **risk management (RM)** commensurate with the project, organization, & management, while meeting customer requirements.

**B.2 Expected response**

**Special remarks.** The response to this DRD **may be combined with the Risk Management Policy** document ECSS‑M‑ST‑80 Annex A, & to the project management plan ECSS‑M‑ST‑10.

**Scope & content:**

1. **Introduction.** The purpose & objective of the RMP.
1. **Applicable & reference documents.** The applicable & reference documents, used to support the generation of the document.
1. **Organization.** Describe the RM organization of the project. List the responsibilities of each of the RM participants.
1. **Risk management policy.** A link to the applicable risk management policy document.
1. **Risk management documentation & follow‑up.** Describe the structure, the rules & the procedures used to document the results of the RM & the follow‑up process.
1. **Project summary.** A brief description of the project, incl. the project management approach.
1. **Description of risk management implementation.** Describe how the RM process is implemented.
1. **Risk identification & assessment.** Describe the identification & assessment process & procedures for examining the critical risk items & domains, & processes to identify & document the associated risks. Summarize the analysis process for each of the risk domain leading to the determination of an overall risk assessment. Include the identification of specific metrics for risk assessment. The RMP may include:
   1. Overview & scope of the identification & assessment process;
   1. Sources of information;
   1. Information to be reported & formats;
   1. Description of how risk information is documented;
   1. Assessment techniques & tools.
1. **Decide & act.** ➀ Describe the risk treatment, which uses the risk assessment report as input. ➁ Specify the criteria of risk acceptance beyond the risk management policy document & mitigation actions that can be used to determine & evaluate various risk handling options. ➂ Identify tools (i.e. name, version & date) that can assist in implementing the risk decision & acting process.
1. **Risk monitoring & communication.** ➀ Describe the operational approach that is followed to track, monitor update iterate & communicate the status of the various risks identified. ➁ Provide criteria for the selection of risks to be reported on, identify the reports to be prepared; specify the format; & assign responsibility for their preparation & the frequency of reporting. ➂ Operational escalation procedures should be stated in this clause ensuring a sufficient alert system & a structured manner of communication.



### Risk management policy document (RMPD) — DRD

**A.1 DRD identification.** **Requirement identification & source** — ECSS‑M‑ST‑80 §1.1.1.1.1a. **Purpose & objective** — The objective of the **risk management policy document (RMPD)** is to describe the objectives & principles of **risk management (RM)** in the context of the project & to give a high level outline of how we perform RM, & what are the criteria for classification & acceptance of risks.

**A.2 Expected response**

**Special remarks.** The response to this DRD **may be combined with the Risk Management Plan**, ECSS‑M‑ST‑80 Annex B.

**Scope & content:**

1. **Introduction.** The purpose & objective of the RMPD.
1. **Applicable & reference documents.** The applicable & reference documents in support to the generation of the document.
1. **Resources.** Describe the set of project resources that are affected by risk & thereby have an impact on the project objectives.
1. **Project goals & resource constraints.** Describe the project objectives & the resource constraints of the project & name the project’s critical success factors.
1. **Risk management strategy & approach.** ➀ An overview of the RM approach, to include the status of the RM effort, & a description of the project RM strategy consistently deriving from the project’s objectives. ➁ Margins should be stated & if relevant the apportionment of risk between customer & supplier.
1. **Ranking scheme for risk goals.** The definition of a ranking scheme for risk goals according to the requirements of the project.
1. **Scoring schemes.** State the scoring schemes for the severity of consequences & the likelihood of occurrence for the relevant tradable resources, e.g. as proposed in the standard.
1. **Risk index scheme.** The description of the method/tool by which the magnitudes of risks of the various risk scenarios are denoted.
1. **Action criteria.** The criteria to determine the actions to be taken on risks of various magnitudes & the associated risk decision levels in the project structure e.g. as proposed in the standard.
1. **Individual risk acceptance.** Describe the acceptance criteria for individual risks.
1. **Ranking & comparison of risks.** Describe the method for the ranking & comparison of identified risk items where the ranking reflects on the potential direct consequence & impact of the risk to other risk areas or processes.
1. **Overall risk.** The definition of the overall project risk, its measurement method & method of acceptance.
1. **Communication.** ➀ Describe the strategy & the formats for communicating risk data to the decision makers & for monitoring the risks. ➁ An escalation strategy should be described addressing how the information associated with each element of the RM process is determined & made available to the participants in the process.
1. **Risk management process & procedures.** ➀ Describe the RM process to be employed i.e. the review, decision & implementation flow within the project concerning the risk planning, identification, assessment & identification, handling, monitoring & documentation functions. ➁ Provide application guidance for each of the RM functions in the process allowing the project’s RM organization flexibility while ensuring a common & coordinated approach to RM & the coherence of the responsibilities & interfaces within the RM process.



### Schedule — DRD

**B.1 DRD identification.** **Requirement ID & source** — ECSS‑M‑ST‑60 requirement 1.1.1.1.1 for baseline & current working schedule. **Purpose & objective** — the objective of the schedule is twofold as follows: ➀ For the baseline schedule, establish a reference data base agreed between both contractual parties (the baseline schedule contains all reference data for the purpose of schedule control); ➁ For the current working schedule, provide an overview of the on‑going activities & of the milestone status (for activities not yet completed, the current working schedule reflects the most probable prediction given by the supplier to the customer).

**B.2 Expected response**

**Special remarks.** The schedule should be presented in the form of a Gantt‑Chart.

**Scope & content:**

1. Provide the following information, for both the baseline & current working schedules (when defining the schedule, the total float for each activity with respect to the agreed milestones shall be available):
   1. identification of the activities
   1. identification of the critical path activities
   1. duration of activities
   1. flow of the activities (logical links between activities)
   1. key milestones
   1. lower suppliers planned end dates
   1. main & key inspection points (MIP/KIP)
   1. start/finish date of activities



### Software configuration file (SCF) — DRD

**E.1 DRD identification.** **Requirement ID & source** — ECSS‑M‑ST‑40 §1.1.1.1.1 & from ECSS‑E‑ST‑40 & ECSS‑Q‑ST‑80. **Purpose & objective** — the **software configuration file (SCF)** is to provide the configuration status of the **software configuration item (SCI)**. It controls its evolution during the programme/project life cycle. The SCF is a constituent of the design definition file (as per ECSS‑E‑ST‑10).

**E.2 Expected response**

**Special remarks** — None.

**Scope & content:**

1. **Introduction.** The purpose & objective of the SCF.
1. **Applicable & reference documents, Definitions & abbreviations.** The applicable & reference documents to support the generation of the document. Any additional terms, definition or abbreviated terms used.
1. **Software configuration item overview.** A brief description of the SCI. Provide the following information:
   1. how to get information about the SCI
   1. composition of the SCI: code, documents
   1. means to develop, modify, install, run the SCI
   1. differences from the reference or previous version
   1. status of software problem reports, software change requests, & software waivers & deviations related to the SCI
1. **Inventory of materials.** ➀ List all physical media & associated documentation released with the SCI (Example of physical media are listings, tapes, cards & disks). ➁ Define the instructions necessary to get information included in physical media (For example, to get files).
1. **Baseline documents.** Identify all the documents applicable to the delivered SCI version.
1. **Inventory of software configuration item.** Describe the content of the SCI. List all files constituting the SCI:
   1. source  codes with name, version, description
   1. binary codes with name, version, description
   1. associated data files necessary to run the software
   1. media labelling references
   1. checksum values
   1. identification & protection method & tool description
1. **Means necessary for the software configuration item.** Describe all items (i.e. hardware & software) that are not part of the SCI, & which are necessary to develop, modify, generate & run the SCI, including:
   1. items related to software development (For example, compiler name & version, linker, & libraries)
   1. build files & software generation process
   1. other SCIs
1. **Installation instructions.** Describe how to install the SCI version, its means & procedures necessary to install the product & to verify its installation.
1. **Change list.** List of all changes incorporated into the SCI version, with a cross reference to the affected SCI document, if any. Changes not incorporated yet but affecting the S/W CI shall also be listed & include.
   1. software problem reports
   1. software change requests & proposals
   1. contractual change notices
   1. software waivers & deviations
1. **Auxiliary information.** Include any auxiliary information to describe the software configuration.
1. **Possible problems &  known errors.** Identify any possible problems or known errors with the SCI version & any steps being taken to resolve the problems or errors.



### Space Debris Mitigation Plan (SDMP) — DRD

**A.1. DRD identification.** **Requirement ID & source** — ECSS‑U‑AS‑10. **Purpose & objective** — to define the approach, methods, procedures, resources & organization to co‑ordinate & manage all technical activities necessary to mitigate/eliminate debris generation.

**A.2. Expected response**

**Special remarks** — None.

**Scope & content:**

1. **Introduction.** The purpose, objective, content & the reason prompting its preparation (e.g. logic, organization, process or procedure).
1. **Applicable & reference documents.** The applicable & reference documents in support to the generation of the document.
1. **Plan.** Describe the approach, events & risks leading to debris generation, & plan of their mitigation.



### Specification tree — DRD

**J.1. DRD identification.** **Requirement ID & source** — ECSS‑E‑ST‑10 §5.2.3.1c. **Purpose & objective** — the objective of the specification tree document is to define the hierarchical relationship of all technical requirements specifications for the different elements of a system or product. It’s the basic structure to perform the system or product requirements traceability & to manage their internal interfaces.

**J.2. Expected response**

**Special remarks.** The specification tree **is part of the Design Definition File** (as per ECSS‑E‑ST‑10 Ann.G). The specification tree shall be coherent with the product tree (see ECSS‑M‑ST‑10) & the business agreement structure (see ECSS‑M‑ST‑60).

**Scope & content:**

1. **Introduction.** The purpose, objective & the reason prompting its preparation.
1. **Applicable & reference documents.** The applicable & reference documents, used in support to the generation of the document.
1. **Project summary & user’s need presentation.** A brief description of the project & the key user’s needs.
1. **Tree structure**
   1. The specification tree document shall provide the complete list of specifications defining the system or product, & contain a graphical representation where the system or product specification (i.e. at the top level of the tree) is decomposed into lower level product specifications.
   1. When recurrent products from previous space projects are used, their specification shall be identified in the tree structure, & in addition, for every necessary product that is not under the supplier’s responsibility, their specification shall be identified in the tree structure.



### System concept report (SCR) — DRD

**C.1. DRD identification.** **Requirement ID & source** — ECSS‑E‑ST‑10 §5.3.3c. **Purpose & objective.** Describes the principal technical characteristics of alternative system concepts, relating to performance, architectures, driving technologies, interfaces, risk, their evaluation & classification, & later addresses the selected concept.

**C.2. Expected response**

**Special remarks** — None.

**Scope & content:**

1. The system concept report (SCR) shall be an instantiation of the trade‑off report at system level in Phase 0 & Phase A of a project, conforming to ECSS‑E‑ST‑10 Ann.L. (The SCR can be extended to Phase B where needed (e.g. late trade‑offs))
1. The SCR shall address all technical (e.g. engineering disciplines), programmatic & related aspects relevant to the system.
1. Where relevant, specific e.g. discipline trade‑off’s shall be performed, contributing to the system trade‑off, each one being reported in a document conforming to ECSS‑E‑ST‑10 Ann.L.



### System engineering plan (SEP) — DRD

**D.1. DRD identification.** **Requirement ID & source** — ECSS‑E‑ST‑10, requirements 5.1a & 5.3.4a. **Purpose & objective** — to define the approach, methods, procedures, resources & organization to co‑ordinate & manage all technical activities necessary to specify, design, verify, operate & maintain a system or product in conformance with the customer’s requirements. In particular the SEP is established to fulfil the major technical project objectives, taking into account the defined project phases & milestones (as per ECSS‑M‑ST‑10). The SEP covers the full project lifecycle according to the scope of the business agreement. It’s established for each item of the product tree (as per ECSS‑M‑ST‑10). It highlights the risks, the critical elements, the specified technologies, as well as potential commonalities, possibilities of reuse & standardization, & provides means for handling these issues. The SEP is an element of the project management plan (as per ECSS‑M‑ST‑10). NOTE It’s important to adapt the SEP content to the phase of the project, with more information on risk analysis & new technologies in early phases 0/A/B, & more information on verification & validation aspects in phases C/D.

**D.2. Expected response**

**Special remarks** — None.

**Scope & content:**

1. **Introduction.** The purpose, objective, content & the reason prompting its preparation (e.g. programme or project reference & phase).
1. **Applicable & reference documents.** The applicable & reference documents in support to the generation of the document. A list of the references to the following applicable documents: ➀ Business agreement, ➁ Project management plan, as per ECSS‑M‑ST‑10 Ann.A, ➂ Product assurance plan, as per ECSS‑Q‑ST‑10 Ann.A, ➃ Configuration management plan, as per ECSS‑M‑ST‑40 Ann.A, ➄ Production plan, ➅ Mission operations plan, as per ECSS‑E‑ST‑70 Ann.G, ➆ ILS plan.
1. **Project overview**
   1. **Project objectives & constraints**
      1. The project objective & the main elements that characterize the user’s need.
      1. The objective of the system or product as established by the TS (as per ECSS‑E‑ST‑10‑06 Ann.A).
      1. The main elements of the system architecture (i.e. first level elements of the architecture adopted for the system & identification of their reuse constraints).
      1. The principal characteristics of the project lifecycle & the incremental development of the system (e.g. successive versions, progressive implementation of the functions of the system).
      1. The main elements supporting the project lifecycle (e.g. ground support equipment, & facilities).
      1. The organizational constraints impacting SE activities (e.g. the external & internal industrial organization (e.g. contractors, partners, suppliers, own company) constraints).
      1. List the critical issues identified at the beginning of the project phase(s).
      1. List the national & international regulations.
      1. The capacity for verification & validation of the product, taking into account the means available, e.g. for tests, analysis, or simulation.
   1. **Product evolution logic.** Detail the incremental development of the system:
      1. progressive implementation of system functionalities,
      1. identification of possible successive versions,
      1. objectives & strategy for the implementation of the successive versions.
   1. **Project phase(s), reviews & planning**
      1. Provide an implementation & schedule of the SE activities & identify for the considered phase(s), as a minimum: ➀ the main project milestones driving the SE process, ➁ the phase(s) of the project lifecycle & the main reviews in accordance to project management plan.
      1. Provide dates of milestones or the duration of phases & the critical path according to the project master schedule.
   1. **Procurement approach.** Describe the strategy for acquisition of the items of the system or products defined in the product tree (e.g. make or buy, product line, incremental development).
   1. **Initial critical issues.** List the critical issues identified at the beginning of the project phase(s) covered in the SEP (e.g. any specific issues, problems, critical subjects, which require dedicated attention, investigation, action & planning).
1. **System design approach**
   1. **System engineering inputs**
      1. List the driving inputs for the SE activities described & defined by the: ➀ business agreement, ➁ outputs from previous phase(s) or expected under the heading of activities which are not controlled within the context of this SEP (e.g. data provided by the customer, data coming from other projects, upstream or predevelopment studies, product lines), ➂ project management plan, product assurance plan, risk management plan, & configuration & documentation management plans.
      1. List the external means & facilities (e.g. equipment, software, & premises) made available by the customer or by any other entity external to the supplier that is responsible of this SEP, and, for each identified mean or facility, identify the applicable interface requirements (e.g. interface control documentation) as well as the authority in charge of it.
      1. List the internal means & facilities (e.g. equipment, software, & premises) made available by the organization in charge of the development of the system or product.
      1. Contain the Coordinate System Document (as per ECSS‑E‑ST‑10‑09 Ann.A).
      1. Define the units system to be used in the project.
   1. **System engineering outputs**
      1. List the specified SE outputs as per ECSS‑E‑ST‑10 §6 for the specific project phase(s) covered in the SEP. (NOTE. An overview of document delivery is given in Ann.A.)
      1. Describe:
         1. The strategy for the SE activities in line with the guidelines addressed by the management plan. In particular, identifying intermediate technical events for each phase in compliance with the master program schedule.
         1. The system design activities, with their objectives & major outputs according to the phase.
         1. The major engineering activities for each intermediate technical events, showing their mutual interactions & their relationships with the principal milestones (i.e. internal or contractual) of the project.
         1. The model philosophy (as per ECSS‑E‑ST‑10‑02 §4.2.5) in terms of number & characterization of models, from system to the requested lower level, necessary to achieve a high confidence in the product verification.
         1. The margin policy according to project phase, product category & maturity level.
         1. the method(s) & process(es) considered for the engineering activities (e.g. concurrent engineering, value analysis, or iteration cycle),
         1. the interrelation between the different engineering disciplines & other project activities (e.g. production, quality assurance, & operations & logistics),
         1. the interaction with other actors (e.g. customer & suppliers),
         1. the consistency & coherency of simultaneous activities (e.g. performed in parallel),
         1. which & how, control activities are implemented,
         1. Assessment of potential COTS usage
      1. In the case of a system incremental evolution, describe the design strategy for the:
         1. development of the initial release of the product,
         1. development, the verification of subsequent releases & their deployment,
         1. introduction of new technologies,
         1. tools & methods used for analysis,
         1. control of the evolutions for each release.
   1. **System engineering team responsibilities & organization**
      1. Definition of the entities participating in the SE activities & the corresponding functions according to the project management plan.
      1. Identification of key engineering roles & responsibilities (e.g. system engineers, disciplines engineers, & technical managers).
      1. Description of the co‑operative work amongst the different teams participating in the system design.
   1. **System engineering coordination.** Describe the external & internal coordination in line with the project management plan.
1. **Implementation & related plans**
   1. **System engineering tasks description**
      1. System engineering process description.
         1. Describe the SE process tailored to the specifics of the considered project, & identify all the SE tasks to be implemented from the starting conditions (e.g. kick‑off) to the closing event (e.g. review), their relationship, & their interfaces with other actors of the project, & identify & describe any existing iteration within the process.
         1. For each task, the input information & their origin, the document(s) delivered (i.e. expected output) & their destination, the SE function(s) performed & the contribution of other actors shall be identified.
      1. Engineering disciplines integration.
         1. Address the following activities that concern the different engineering disciplines, recalling the relevant applicable standards & ancillary dedicated plans that are considered integral part of this SEP.
         1. Define the process & control to be put in place to meet requirements for the thermal, structures, mechanisms, environmental control & life support, propulsion, pyrotechnics, mechanical parts, & materials functions & interfaces. (NOTE. These requirements refer to Mechanical engineering as per ECSS‑E‑ST‑3x series of standards)
         1. Define the process & control to be put in place to meet requirements for electrical & electronic engineering, covering all electrical & electronic aspects of the relevant space product, including functions such as power generation, storage, conversion & distribution, & optical, avionics & microwave domains, electromagnetic compatibility, & electrical interfaces. (NOTE. These requirements refer to Electrical & electronic engineering as per ECSS‑E‑ST‑20.)
         1. Define process & control to be put in place to meet requirements for software engineering, covering, amongst others, flight & ground software, checkout software & simulation software. (NOTE. These requirements refer to Software engineering as per ECSS‑E‑ST‑40.)
         1. Define process & control to be put in place to meet requirements for communication engineering, covering, amongst others, spacecraft‑to‑ground, spacecraft‑to‑spacecraft, ground‑to‑ground & on‑board communications links. (NOTE 1 These requirements refer for Communications engineering as per ECSS‑E‑ST‑50. NOTE 2 It includes aspects such link budgets, data management, RF, audio & video communications & protocols)
         1. Define process & control to be put in place to meet requirements for control engineering, covering, amongst others, AOCS, robotics, rendez‑vous & docking.
         1. Define the process & control to be put in place to meet requirements specifying natural environment for all space regimes (e.g. debris regulations, or planetary contamination protection) & general models & rules for determining the local induced environment. (NOTE. These requirements refer to Space environment as per ECSS‑E‑ST‑10‑04)
         1. Define the process & control to be put in place to meet requirements for the approach, methods, procedures, organization & resources to be implemented to ensure proper technical interfaces between SE & production.
         1. Define the process & control to be put in place to meet requirements of operations of the space segment, covering, amongst others: 1. mission operation definition & preparation, 2. mission & trajectory analysis, 3. operability analysis (e.g. autonomy, operational scenario, nominal & non‑nominal modes, failure detection isolation & recovery). (NOTE. These requirements refer to Operations engineering as per ECSS‑E‑ST‑70)
         1. Define the process & control to be put in place to meet requirements for ground & in‑orbit logistics & maintenance, addressing, amongst others, technical activities, related engineering standards, methods & analyses to be performed to ensure that the development of space systems (i.e. manned & unmanned) properly takes into account & integrates the supportability & support aspects for the whole life cycle.
         1. Define the process & control to be put in place to meet requirements for human activities & environments associated with space systems. (NOTE. These requirements refer to Human factors engineering as per ECSS‑E‑ST‑10‑11)
         1. Define process & control to be put in place to meet requirements for implementation of design selections relating to humans for any item with associated human interface, including computer based system & equipment.
      1. Work package. Define & describe the work package(s) for the relevant engineering tasks, which are maintained in the work breakdown structure.
   1. **Related plans**
      1. When the SEP includes sub‑plans covering parts of SE activities, these sub‑plans shall be annexed to the SEP.
      1. Identify the other plans relevant to SE function activity belonging to the following categories:
         1. Programmatic plans (NOTE. Examples of programmatic plans are: the SEP plans of sub‑products constituting the system or product, Industrial procurement plan, risk management plan, off‑the‑shelf plan (see ECSS‑Q‑ST‑20‑10))
         1. Verification plans (NOTE. Examples of verification plans are: verification plan (VP), AIT plan, AIV plan & technology plan, system calibration plan, Security Aspects Verification Plan. Some of those DRDs are defined in this document, in ECSS‑E‑ST‑10‑02 or ECSS‑E‑ST‑10‑03. ・ VP & AIT plans can be integral parts of the SEP, or rolled out separately (without overlap), or combined as the AIV Plan which can also be rolled out separately. However, the existence of the AIV Plan excludes independent VP & AIT plans.)
         1. Engineering discipline plans (NOTE. Examples of engineering discipline plans are: Fracture Control Plan (see ECSS‑E‑ST‑32), Micro‑gravity Control Plan, Electro‑Magnetic Compatibility Plan (see ECSS‑E‑ST‑20), Audible Noise Control Plan, Radio Frequency Plan, Alignment Requirements & Control Plan, System Performance Simulations Plan, Software Development Plan, Orbital Debris Mitigation Plan & Disposal Plan (as per ISO 24113:2011), Planetary protection Plan, Cleanliness & Contamination Control Plan.)
         1. Operations plans (NOTE. Examples of operation plans are: launch site operations & logistics plan, system commissioning & operation support plan)
         1. (NOTE. Some of these plans can be integrated in the SEP in the early phases of a project.)
      1. Describe the constraints & the interactions impacting the SE activities derived from the analysis of the plans identified as relevant in D.2.1<5.2>b.
   1. **System engineering methods, tools & models**
      1. List & briefly describe the methods, tools, & data models that the SE team uses in performing their tasks.
      1. In relation to requirements traceability & demonstration of verification (compliance with requirements, VCD), the specific methods & tools shall be described (including interfaces to next lower level suppliers), & reuse of elements (e.g. COTS) identified.
   1. **Critical issues.** Describe any specific issues, problems requiring dedicated attention, investigations or actions during the current phase & identify risks, & risk mitigation measures.
1. **System engineering for the following phase(s).** Introduce the SE activities, to be conducted during subsequent phase(s) of the project, & as a minimum, list any identified critical issue & risk to be mitigated during the subsequent phase(s).



### Technical budget — DRD

**I.1. DRD identification.** **Requirement ID & source** — ECSS‑E‑ST‑10 §5.4.1.2a. **Purpose & objective** — the **technical budget (TB)** defines for each key engineering parameter of a system or product, the nature of this parameter, its measure, specified value, metrics requirements & current actual or computed value & assessed value. It’s the basic document for providing adequate control of the key engineering parameter properties to meet the system or product technical requirements.

**I.2. Expected response**

**Special remarks.** The TB **is part of the Design Definition File** (as per ECSS‑E‑ST‑10 Ann.G).

**Scope & content:**

1. **Introduction.** The purpose, objective, content & the reason prompting its preparation.
1. **Applicable & reference documents.** The applicable & reference documents in support to the generation of the document.
1. **List of selected key engineering parameters**
   1. list the selected key engineering parameters (those specified by the customer & those selected by the supplier; e.g. mass, communication links, power, & OBC memory capacity)
   1. present the reason for their selection
   1. identify for each key engineering parameters the stages of maturity of the design
   1. present the related margin policy for these parameters
1. **Assessment of key engineering parameters.** For each key engineering parameter:
   1. provide the specified value of the parameter
   1. provide the supplier’s margin resulting of the allocation of the parameter to the lower level products
   1. provide the specified values with the reference to the relevant technical requirement of the lower level products
   1. propose a specific program to conform to the specified value in case of nonconformance
   1. contain a chart of parameter history that presents the evolution of the parameter’s value at the different design maturity steps for which the evaluation of the parameter is performed
   1. list the documentation sources (e.g. analysis report & verification report)
1. **Conclusion.** Identify the key engineering parameters having a negative margin, & identify for each of those:
   1. the impact on the technical requirements & the associated risk for the project
   1. the specific program to conform to the specified value & for project risk mitigation



### Technical requirements specification (TS) — DRD

**A.1 DRD identification.** **Requirement ID & source** — ECSS‑E‑ST‑10 §5.2.3.1.b & 5.6.4.a for all technical requirements specifications. **Purpose & objective** — the **technical requirements specification (TS)** establishes the intended purpose of a product, its associated constraints & environment, the operational & performance features for each relevant situation of its life profile, & the permissible boundaries in terms of technical requirements. The TS expresses frozen technical requirements for designing & developing the proposed solution to be implemented. These technical requirements, to be met by the future solution, are compatible with the intended purpose of a product, its associated constraints & environment, & the operational & performance features for each relevant situation of its life profile.

**A.2 Expected response**

**Special remarks** — None.

**Scope & content:**

1. **Introduction.** The purpose, objective, content & the reason prompting its preparation.
2. **Applicable & reference documents.** The applicable & reference documents in support of the generation of the document.
3. **User’s need presentation**
    1. Present the main elements that characterize the user’s need for developing the product as a background for those requirements that are defined in detail in the dedicated section.
    2. Put the product into perspective with other related products.
    3. If the product is independent & totally self‑contained, i.e. able to match the final user’s need, it should be so stated here.
    4. If the TS defines a product that is a component of a higher tier system, the TS shall recall the related needs of that larger system & shall describe the identified interfaces between that system & the product.
    5. A non­exhaustive checklist of general questions that should be answered at the early stages of the TS is: What is the product supposed to do? It’s fundamental but critically important to make sure that every actor has a complete understanding of what the product has to do. Who is going to use this product? It’s important to indicate who & why is going to use the product, & for what it’s going to be used.
4. **Selected concept/product presentation.** Describe the concept, the expected product architecture & the functioning principles on which it’s based.
5. **Life profile description.** List & describe the different chronological situations of the product’s life profile. (1) For a spacecraft, the life profile includes: AIT related life events; transportation to launching area; conditioning & tests; installation on launcher; pre­launch phase; launching phase; self transfer to its operating position; in­orbit functioning; end­of­life (e.g. de­orbitation). (2) An identifier can be associated with each situation in order to be able to link each requirement to at least one situation in which it applies. Such an approach enables sorting & filtering of the requirements per situation.
6. **Environment & constraints description.** Describe the different environments & constraints for each situation in the life profile that the product is expected to encounter. An identifier can be associated with each product environment in order to be able to link each requirement to at least the worst environment to which it applies. Such an approach enables sorting & filtering the requirements per environment.
7. **Requirements.**
    1. List all the technical requirements necessary for the product to satisfy the user’s needs. Interfaces requirements can be rolled‑out of the TS in form an interface requirement document (IRD), see ECSS‑E‑ST‑10 Annex M.
    2. The technical requirements shall be expressed according to ECSS‑E‑ST‑10‑06 §7 & 8. For instance, for all TS & for each requirement, the following characteristics have been selected: identifiability; performance & methods used to determine it; configuration management; traceability; tolerance; verification.



### Technology matrix (TM) — DRD

**F.1. DRD identification.** **Requirement ID & source** — ECSS‑E‑ST‑10 §5.6.7.1a. **Purpose & objective** — to present, for each technical requirement/function, the list of technologies or technological elements, which have the potential to meet this requirement. It summarizes candidate technologies per individual requirement. It’s the basic document for presenting all identified potential technologies for the product.

**F.2. Expected response**

**Special remarks.** The TM **is part of the Design Justification File** (as per ECSS‑E‑ST‑10 Ann.K).

**Scope & content:**

1. **Introduction.** The purpose, objective, content & the reason prompting its preparation.
1. **Applicable & reference documents.** The applicable & reference documents in support to the generation of the document, incl. the reference to the following applicable documents: Function tree, Preliminary technical requirements specifications, Specification tree.
1. **List of technical requirements/functions.** List the system technical requirements/functions as per the functional architecture & its corresponding function tree, & their associated preliminary TS.
1. **List of potential technologies for each technical requirement/function.** List the system technical requirements/functions & for each, a potential technology or technological element. (NOTE Sources to identify potential technologies are technology watch, corporate technology plan, or research & development programme.) For each technology or technological element, the following information shall be listed:
   1. index of technology readiness & maturity as per Table 4‑2 of ECSS‑E‑AS‑11
   1. proof of company's maturity concerning the knowledge & expertise of the technology, including a description of the necessary technology acquisition activities
   1. identification of potential risks, e.g. technology availability, programmatic & financial aspects
1. **Ranking of the potential technologies for each function.** Propose a ranking of the potential technology or technological element for each system requirement/function.
1. **Conclusion.** Provide, for each system requirement/function, the selected technology or technological element, a list of the identified project risks & critical aspects, & an identified back‑up technological solution.



### Technology plan (TP) + Technology readiness status list — DRD

**E.1. DRD identification.** **Requirement ID & source** — ECSS‑E‑ST‑10 §5.6.7b. **Purpose & objective** — the objective of the **technology plan (TP)** is to define the approach, methods, procedures, resources & organization to evaluate the ability of a critical technology to meet the intended requirements. Also, the objective of this plan is to ensure effective preparation of the technologies necessary for a timely implementation of the system, in accordance to the requirements imposed by the specific characteristics of the relevant product. It’s established for each item of the function tree (as per ECSS‑E‑ST‑10 Ann.H), & highlights the technical requirements, & the critical technology of each item.

**E.2. Expected response**

**Special remarks.** The content of the TP **may be merged with the SEP** (as per ECSS‑E‑ST‑10 Ann.D). The TP shall introduce the related activities, to be conducted during all phase(s) of the project.

**Scope & content:**

1. **Introduction.** The purpose, objective, content & the reason prompting its preparation (e.g. programme or project reference & phase).
1. **Applicable & reference documents.** The applicable & reference documents in support to the generation of the document & include the reference to the following applicable documents: Function tree, Specification tree, System engineering plan, Technology matrix (as per ECSS‑E‑ST‑10 Ann.F)
1. **Project overview.** A summary of the main aspects of:
   1. project objectives & constraints (i.e. §3.1 of ECSS‑E‑ST‑10 Ann.D);
   1. product evolution logic (i.e. §3.2 of ECSS‑E‑ST‑10 Ann.D);
   1. project phase(s), reviews & planning (i.e. §3.3 of ECSS‑E‑ST‑10 Ann.D),
   1. Procurement approach (i.e. §3.4 of ECSS‑E‑ST‑10 Ann.D)
1. **Tasks description**
   1. **TP expected outputs.** The expected output shall be an answer concerning the possibility for using the identified or needed technology to perform a function.
   1. **TP inputs.** For each system function, the TP input shall be:
      1. technical requirements,
      1. the selected technology or technological element & its TRL,
      1. the list of the identified project risks & critical aspects,
      1. the schedule for Engineering activities.
   1. **TP tasks**
      1. Establish & describe the necessary activities to complete the acquisition of each technology or technological element, including verification strategies & methods, & the link to product assurance aspects.
      1. Define the model philosophy for each technology or technological element, based on an assessment on the maturity status & on the criticality of the technology with respect to functions' requirements.
      1. Describe the technology development activities, their required or possible interrelations & timings, as necessary for the satisfactory acquisition of the technologies & procurement of the technological elements.
      1. Identify technical milestones, showing their interactions & relationships with the SEP milestones.
   1. **Responsibilities & organization**
      1. definition of the entities participating in the engineering activities & the corresponding functions according to the SEP;
      1. identification of key engineering roles & responsibilities for each technology or technological element.
   1. **TP interfaces.** Describe the external & internal interfaces in conformance to the SEP.
1. **Technology issues.** Describe, for any identified technology risk & related critical aspects for the project, the specific actions taken for risk mitigation based on identified technology readiness level (TRL). Include the TSL by using the template in Figure E‑1, & listing:
   1. the critical function, with reference to the Function tree,
   1. the name of the technology or element(s) implementing such a function,
   1. current declared & verified TRL, as per ECSS‑E‑AS‑11,
   1. reference to the TRA report confirming the declared TRL,
   1. date of the report,
   1. key points to support TRL declared in column [3],
   1. forward plan for TRL evaluation, indicating the target TRL, the phase or date at which such target TRL is expected, & status of the planning to achieve the target TRL,
   1. during phases A & B, indication whether or not technology/element is a candidate for the CIL.
   1. (NOTE. The key point in column [6] are normally few lines summarizing the TRL assessment report referenced in column [4]). (NOTE. At the end of Phase B, the TSL is introduced as part of the CIL)

【**Figure E‑1:** TSL template】

|**(1) Critical Function**|**(2) Tech&shy;nology / Element**|**(3) TRL**|**(4) TRA Report**|**(5) TRA Date**|**(6) Rationale for TRL evaluation**|**(7) Target TRL & Tech&shy;nology Plan Evolu&shy;tion**|**(8) TIL Candi&shy;date (Y/N)**|
|:--|:--|:--|:--|:--|:--|:--|:--|
|TBD|✔|—|✔|—|✔|—|✔|



### Test procedure (TPRO) — DRD

**C.1 DRD identification.** **Requirement ID & source** — ECSS‑E‑ST‑10‑03 §4.3.3.4. **Purpose & objective** — the **Test Procedure (TPRO)** gives directions for conducting a test activity in terms of description, resources, constraints & step‑by‑step procedure, & provides detailed step‑by‑step instructions for conducting test activities with the selected test facility & set‑up in agreement with the relevant AIT Plan & the test requirements. It contains the activity objective, the applicable documents, the references to the relevant test specification & the test facility configuration, the participants required, the list of configured items under test & tools & the step‑by‑step activities. The TPRO is used & filled‑in as appropriate during the execution & becomes the “as‑run” procedure. The TPRO is prepared for each test to be conducted at each verification level. The same procedure can be used in case of recurring tests. It incorporates the requirements of the test specification (Annex B) & uses detailed information contained in other project documentation (e.g. drawings, ICDs). Several procedures often originate from a single test specification. In certain circumstances involving a test facility (for example during environmental tests) several test procedures can be combined in an overall integrated test procedure. The “as‑run” procedure becomes part of the relevant test report (see ECSS‑E‑ST‑10‑02). Overlaps with the test specification are minimized (see Ann.B).

**C.2 Expected response**

**Special remarks** — None.

**Scope & content:**

1. **Introduction.** The purpose, objective, content & the reason prompting its preparation. State & describe open issues, assumptions & constraints relevant to this document.
1. **Applicable & reference documents, Definitions & abbreviations.** The applicable & reference documents in support to the generation of the document. List the applicable dictionary or glossary & the meaning of specific terms or abbreviations utilized in the document.
1. **Requirements mapping w.r.t. the TSPE.** Provide a mapping matrix to the TSPE giving traceability towards the test requirement.
1. **Item under test.** Describe the item under test configuration, including any reference to the relevant test configuration list, & any deviation from the specified standard. Identify the software version of the item under test.
1. **Test set‑up.** Describe the test set‑up to be used.
1. **GSE & test tools required.** Identify the GSE & test tools to be used in the test activity including test script(s), test software & database(s) versioning number.
1. **Test instrumentation.** Identify the test instrumentation, with measurement uncertainties, to be used, including fixtures.
1. **Test facility.** Identify the applicable test facility & any data handling system.
1. **Test conditions.** List the applicable standards, the applicable test conditions, in terms of levels, duration & tolerances, & the test data acquisition & reduction.
1. **Documentation.** Describe how the applicable documentation is used to support the test activity.
1. **Participants.** List the allocation of responsibilities & resources.
1. **Test constraints & operations.** Identify special, safety & hazard conditions, operational constraints, rules for test management relating to changes in procedure, failures, reporting & signing off procedure. Describe QA & PA aspects applicable to the test. Contain a placeholder for identifying: procedure variations, together with justification, & anomalies.
1. **Step‑by‑step procedure**
   1. Provide detailed instructions, including expected results, with tolerances, pass/fail criteria, & identification of specific steps to be witnessed by QA personnel.
   1. The step‑by‑step instructions may be organized in specific tables.
   1. When the procedure is automated, documents the listing of the automated procedure to a level allowing consistency check with the TPRO & the TPSE.



### Test report (TR) — DRD

**C.1 DRD identification.** **Requirement ID & source** — ECSS‑E‑ST‑10‑02 §5.3.2.1b. **Purpose & objective** — the **Test Report (TR)** describes test execution, test & engineering assessment of results & conclusions in the light of the test requirements (including pass‑fail criteria). The test report contains the scope of the test, the test description, the test article & set‑up configuration, & the test results including the as­run test procedures, the considerations & conclusions with particular emphasis on the close­out of the relevant verification requirements including deviations.

**C.2 Expected response**

**Special remarks** — None.

**Scope & content:**

1. **Introduction.** The purpose, objective, content & the reason prompting its preparation. State & describe open issues, assumptions & constraints relevant to this document.
1. **Applicable & reference documents, Definitions & abbreviations.** The applicable & reference documents in support to the generation of the document. Include as applicable reference documents the corresponding test procedure & test specification as specified in the DRDs in ECSS‑E‑ST‑10‑03. The applicable dictionary or glossary & the meaning of specific terms or abbreviations utilized in the document.
1. **Test results.**
   1. The test results with supporting data (including the test execution dates, the as run procedure, & the test facility results).
   1. The analysis of test data & the relevant assessment.
   1. Provide a synthesis of the test results.
1. **Anomalies.** Include the list of deviations to the test procedure, the nonconformance including failures & the problems.
1. **Conclusions**
   1. Summarize:
      1. the test results, including: ➀ the list of the requirements to be verified (in correlation with the VCD), ➁ traceability to used documentation, ➂ conformance or deviation including references & signature & date)
      1. the comparison with the requirements
      1. the verification close‑out judgment
   1. Clearly state & describe open issues
   1. Cross‑conference aeparate test analyses



### Test specification (TSPE) — DRD

**B.1 DRD identification.** **Requirement ID & source** — ECSS‑E‑ST‑10‑03 §4.3.3.3. **Purpose & objective** — the **test specification (TSPE)** describes in detail the test requirements applicable to any major test activity. In particular, it defines the purpose of the test, the test approach, the item under test & the set‑up, the required GSE, test tools, test instrumentation & measurement uncertainties, test conditions with tolerances, test sequence, test facility, pass/fail criteria, required documentation, participants & test schedule. Since major test activities often cover multiple activity sheets, the structure of the TSPE is adapted accordingly. The TSPE is used as an input to the test procedures, as a requirements document for booking the environmental test facility & to provide evidence to the customer on certain details of the test activity in advance of the activity itself. The TSPE is used at each level of the space system decomposition (i.e. equipment, space segment element). The TSPE provides the requirements for the activities identified in the AIT Plan (as per ECSS‑E‑ST‑10‑03 Annex A). The TSPE is used as a basis for writing the relevant test procedures (as per ECSS‑E‑ST‑10‑03 Annex C) & test report (as per ECSS‑E‑ST‑10‑02 Annex C). In writing the test specification potential overlaps with the test procedure is minimized (i.e. the test specification gives emphasis on requirements, the test procedure on operative step by step instructions). For simple tests, merging TSPE & TPRO is acceptable.

**B.2 Expected response**

**Special remarks** — None.

**Scope & content:**

1. **Introduction.** The purpose, objective, content & the reason prompting its preparation. State & describe open issues, assumptions & constraints relevant to this document.
1. **Applicable & reference documents, Definitions & abbreviations.** The applicable & reference documents in support to the generation of the document. List the applicable dictionary or glossary & the meaning of specific terms or abbreviations utilized in the document.
1. **Requirements to be verified.** List the requirements to be verified (extracted from the VCD) in the specific test & provides traceability where in the test the requirement is covered.
1. **Test approach & test requirements.** Summarize the approach to the test activity & the associated requirements as well as the prerequisites to start the test.
1. **Test description.** Summarize the configuration of the item under test, the test set‑up, the necessary GSE, the test tools, the test conditions & the applicable constraints.
1. **Test facility.** Describe the applicable test facility requirements together with the instrumentation & measurement uncertainties, data acquisition & test space segment equipment to be used.
1. **Test sequence.** Describe the test activity flow & the associated requirements. When constraints are identified on activities sequence, specify them including necessary timely information between test steps.
1. **Pass/fail criteria.** List the test pass/fail criteria in relation to the inputs & output.
1. **Test documentation.** List the requirements for the involved documentation, including test procedure, test report & PA & QA records.
1. **Test organization.** Describe the overall test responsibilities, participants to be involved & the schedule outline (NOTE Participation list is often limited to organisation & not individual name).



### Trade‑off report — DRD

**L.1. DRD identification.** **Requirement ID & source** — ECSS‑E‑ST‑10 §5.3.3.1b. **Purpose & objective** — to provide the system‑engineering point of view on alternative design solutions, an evaluation & a classification of the alternative design solutions, & the justification of their ranking.

**L.2. Expected response**

**Special remarks** — None.

**Scope & content:**

1. **Introduction.** The purpose, objective, content, & the reason prompting its preparation.
1. **Applicable & reference documents.** A list of the documents in support to the generation of the document, incl. the reference to the following applicable documents: ➀ Mission description document, if relevant, ➁ Project phasing & planning requirement document, ➂ System engineering plan, ➃ Technical requirements specification (TS).
1. **Objective & context of the trade‑off study.** The purpose of the trade‑off study & its context (e.g. logic, organization, process or procedure).
1. **Key technical requirements**
   1. A list of the key technical requirements from the TS (as per ECSS‑E‑ST‑10‑06) to be satisfied by the possible alternative design solutions to conform to the needs or requirements of the user.
   1. The research of possible alternative design solutions should not preclude the identification of design solutions which are not currently mature enough, but which can be potential design solution for future similar applications. (NOTE A possible design solution is a technical answer that has the capability to meet a set of technical requirements)
   1. Identify & present the sources of information used to identify the possible design solution, e.g. R&D results, lessons learned, or similar applications.
1. **Evaluation criteria**
   1. A list of the selected evaluation criteria & precise the justification for selecting those criteria, & provide the weighting of criteria & their justifications. (NOTE The criteria are selected theme by theme from the Technical Specification (as per ECSS‑E‑ST‑10‑06 Ann.A), the programmatic aspects (including e.g. budget, schedule, etc… for development, manufacturing, as well as target cost for operations & recurrent items), & the technical risks).
   1. Identify the entity responsible for the evaluation of the design solutions for any criteria, as well as the source & agreement regarding weighting factors (e.g. with management).
1. **Presentation of the alternative design solutions**
   1. Present every different alternative design solutions proposed by the organization in charge of the development of the system or product, the proposals from the customer & supplier if any, & emphasize the technical description that is correlated to the criteria of evaluation.
   1. Characterize each alternative design solution in terms of technology status or maturity, performances capability, & risks.
1. **Evaluation of the alternative design solutions.** Present the result of the evaluation of every identified alternative design solution with regard to the key technical requirements. For each alternative design solution the following shall be performed; present, in a table, the result of the evaluation per criteria: ➀ assessment of all the key technical requirements/evaluation criteria, ➁ presentation of the pros/cons of the design solution, ➂ identification of the technical/programmatic risks.
1. **Classification of the alternative design solutions.** Based on the proposed scheme for weighting the evaluation criteria, provide a classification of the different alternative design solutions.
1. **Analysis of the robustness of the classification.** Provide the result of a sensitivity analysis of the criteria that provide advantage to the solution ranked first, e.g. when changing the weighting of the evaluation criteria.
1. **Conclusion.** Recommend one solution & explain the reason for this choice (e.g. evaluation criteria where the selected solution take advantage), & precise the condition for the application of the recommended solution. Present the identified technical & programmatic risks induced by the choice of the recommended solution, & any additional activity necessary to be performed for risk mitigation.



### Verification control document (VCD) — DRD

**B.1 DRD identification.** **Requirement ID & source** — ECSS‑ST‑E‑10‑02 §5.2.8.2b. & 5.4.4.1b. **Purpose & objective** — to list the requirements to be verified with the selected methods in the applicable stages at the defined levels. It includes the **Verification Matrix (VM)**. The VCD is a living document & provides traceability during the phase C, D & E, how & when each requirement is planned to be verified & is actually verified. The VCD becomes part of the EIDP, as detailed in ECSS‑Q‑ST‑20.

**B.2 Expected response**

**Special remarks** — None.

**Scope & content:**

1. **Introduction.** The purpose, objective, content & the reason prompting its preparation. State & describe open issues, assumptions & constraints relevant to this document. Phase the VCD content with the product life‑cycle such that the initial issue contains the verification matrix, intermediate issues cover the planned on‑ground verifications & their executions evidence (in particular for qualification & acceptance completion), the in‑orbit & post landing activities; final issue provides evidence of the close‑out of the overall verification process.
1. **Applicable & reference documents, Definitions & abbreviations.** The applicable & reference documents in support to the generation of the document. List the applicable dictionary or glossary & the meaning of specific terms or abbreviations utilized in the document.
1. **Verification subject.** Describe the verification control approach applied to the product, the involved documentation & the computerized tool used to support the process. Include the requirements to be verified (with reference to the specifications involved), call up the verification methods, levels & stages definitions & explain the verification close­out criteria.
1. **Verification summary status.** Summarize the current Verification Close‑out status for each issue of the VCD.
1. **Verification control data**
   1. Collect in the form of a matrix, for each requirement, the following  verification information:
      1. Requirement identifier
      1. Requirement text
      1. Traceability between requirement
      1. Levels & stages of verification
      1. Methods
      1. Link to the relevant section of the verification plan & any planning document (For example, test specification)
      1. References to any documentation that demonstrates compliance to the requirements (For example, report, analysis, waivers, RFD, NCR, NRB, customer closeout records
      1. Status of Compliance (yes, no, partial)
      1. Close‑out status (open/closed)
      1. Reasons of the close‑out status
   1. The initial issue of the VCD shall contain a verification matrix limited to:
      1. Requirement identifier
      1. Requirement text
      1. Traceability between requirement
      1. Levels & stages of verification
      1. Methods
      1. Link to the relevant section of the verification plan



### Verification plan (VP) — DRD

**A.1 DRD identification.** **Requirement ID & source** — ECSS‑E‑ST‑10‑02 §5.2.8.1b. **Purpose & objective** — it contains the overall verification approach, the model philosophy, the product matrix, the verification strategies for the requirements (the interrelation between different methods/levels/stages of verification to be used to demonstrate status of compliance to requirements), the test, inspection, analysis & review‑of‑design programme with the relevant activity sheets & planning, the verification tools, the verification control methodology, the involved documentation, the verification management & organization.

**A.2 Expected response**

**Special remarks.** The Verification Plan **may be combined with the AIT Plan** in one single AIV Plan; in this case VP & AIT plans do not exist anymore as single entities.

**Scope & content:**

1. **Introduction.** The purpose, objective, content & the reason prompting its preparation. State & describe open issues, assumptions & constraints relevant to this document.
1. **Applicable & reference documents, Definitions & abbreviations.** The applicable & reference documents in support to the generation of the document. The applicable dictionary or glossary & the meaning of specific terms or abbreviations utilized in the document.
1. **Verification subject.** Briefly describe the subject of the verification process.
1. **Verification approach.** Describe the basic verification concepts & definitions (methods, levels & stages).
1. **Model philosophy.** Describe the selected models & the associated model philosophy, product matrix.
1. **Verification Strategy.** Describe the selected combination of the different verification methods at the applicable verification levels & stages, in general & for each requirement type/group (including software). Provide the allocation of the requirements to the specific verification tasks.
1. **Verification programme.** Document the verification activities & associated planning in the applicable verification stages. Detail the analysis, review ­of ­design, inspection & test programmes through dedicated activity sheets, or through reference to the AIT Plan.
1. **Verification tools.** Describe high level definitions of the verification tools to be used, such as S/W facilities, special tools, simulators, analytical tools.
1. **Verification control methodology.** Describe the proposed methodology to be utilized for verification monitoring & control including the use of a verification data base.
1. **Documentation.** List the involved verification documents & describe their content.
1. **Organization & management**
   1. Describe the responsibility & management tools applicable to the described verification process.
   1. Describe the responsibilities within the project team, the relation to product assurance, quality control & configuration control (including anomaly handling & change control) as well as the responsibility sharing with external partners.
   1. Describe & plan the relevant reviews & responsibilities.



### Verification report (VR) — DRD

**F.1 DRD identification.** **Requirement ID & source** — ECSS‑E‑ST‑10‑02 §5.3.2.5b. **Purpose & objective** — the **Verification Report (VR)** is prepared when more than one of the defined verification methods are utilized to verify a requirement or a specific set of requirements. It reports the approach followed & how the verification methods were combined to achieve the verification objectives. The positive achievement constitutes the completion of verification for the particular requirement.

**F.2 Expected response**

**Special remarks** — None.

**Scope & content:**

1. **Introduction.** The purpose, objective, content & the reason prompting its preparation. State & describe open issues, assumptions & constraints relevant to this document.
1. **Applicable & reference documents, Definitions & Abbreviations.** The applicable & reference documents in support to the generation of the document. The applicable dictionary or glossary & the meaning of specific terms or abbreviations utilized in the document with the relevant meaning Verification subject.
1. **Verification results.** Describe the verification approach, the associated problems & results with reference to the relevant test, analysis, review­ of­ design & inspection reports. Identify the deviations from the verification plan.
1. **Conclusions.** List the requirements to be verified (in correlation with the VCD). Summarize verification results, the comparison with the requirements & the verification close­out judgement. Clearly state & describe open issues.



### Work breakdown structure (WBS) — DRD

**C.1 DRD identification.** **Requirement ID & source** — ECSS‑M‑ST‑10 §5.3h. **Purpose & objective** — To provide a framework for project in cost & schedule management activities (as per ECSS‑M‑ST‑60) & for managing technical content. It assists projectʹs actors in: conducting tender comparisons & business agreement negotiations; optimizing the distribution of work amongst the different suppliers; monitoring the schedule of the project. The WBS divides the project into manageable work packages, organized by nature of work. It identifies the total work to be performed down to a level of detail agreed between the customer & supplier. Information concerning the determination of the appropriate WBS level of detail is provided in ECSS‑M‑ST‑10 Annex H.

**C.2 Expected response**

**Special remarks** — None.

**Scope & contents**:

1. **Introduction.** The purpose, objective & the reason prompting its preparation (e.g. program or project reference & phase).
1. **Applicable & reference documents.** The applicable & reference documents used in support of the generation of the document.
1. **Tree structure**
   1. Provide a logical & exhaustive breakdown of the product tree elements, that includes the customer’s defined support functions (e.g. project management, engineering, product assurance support) necessary to produce the end item deliverables (development & flight models) & the necessary services as appropriate for the project
   1. A coding scheme for WBS elements that represents the hierarchical structure when viewed in text format shall be used. (NOTE 1 A common coding system facilitates communications among all project actors. NOTE 2 E.g.: to each WBS element is assigned a code used for its identification throughout the life of the project. It can be a simple decimal or alphanumeric coding system that logically indicates the level of an element & related lower‑level subordinate elements)
   1. Identify all control work‑packages
   1. The control work‑packages may be further broken down by the supplier in several more detailed work‑packages
   1. All defined work‑packages together shall cover the total work scope
   1. Present the WBS either as a graphical diagram or an indentured structure

**Determination of the appropriate WBS level of detail**

The main challenge associated with developing the **work breakdown structure (WBS)** is to determine the balancing between the project definition aspects of the WBS & the requirements for data collecting & reporting. One has to keep in mind that the WBS is a tool designed to assist the project manager when decomposing the project only to the levels necessary to meet the needs of the project, the nature of the work, & the confidence of the team.

An excessive WBS levels can lead to unrealistic levels of maintenance & reporting, & consequently to an inefficient & over costly project. The theory that more management data equates to better management control has been proven false many times over in the last decades when assessing systems performance. On the other hand, if not detailed enough it makes the element difficult to manage or the risk unacceptable.

Among the different questions arising when developing a WBS, an important one is: should the WBS be decomposed further?

To help answering this question, we propose the following list of questions. If most of the questions can be answered YES, then the WBS element analyzed should be decomposed. On the contrary, if most of the questions can be answered NO, then this is not necessary. If the answers are approximately 50/50, then additional judgment is needed.

1. Is there a need to improve the assessment of the cost estimates or progress measuring of the WBS element?
1. Is there more than one individual responsible for the WBS element? Often a variety of resources are assigned to a WBS element, a unique individual is assigned the overall responsibility for the deliverable created during the completion of the WBS element.
1. Does the WBS element content include more than one type of work process or produces more than one deliverable at completion?
1. Is there a need to assess the timing of work processes that are internal to the WBS element?
1. Is there a need to assess the cost of work processes or deliverables that are internal to the WBS element?
1. Are there interactions between deliverables within a WBS element to another WBS element?
1. Are there significant time gaps in the execution of the work processes that are internal to the WBS element?
1. Do resource requirements change over time within a WBS element?
1. Are there acceptance criteria, leading to intermediate deliverable(s), applicable before the completion of the entire WBS element?
1. Are there identified risks that require specific attention to a subset of the WBS element?
1. Can a subset of the work to be performed within the WBS element be organized as a separate unit?



### Work package (WP) description — DRD

**D.1 DRD identification.** **Requirement ID & source** — ECSS‑M‑ST‑10 §5.3n. **Purpose & objective** — to describe the detailed content of each element of the WBS as per ECSS‑M‑ST‑10 Annex C.

**D.2 Expected response**

**Special remarks** — None.

**Scope & content:**

The WP description shall contain the following elements:

1. Project name & project phase
1. WP title
1. Unique identification of each WP & issue number
1. Supplier or entity in charge of the WP performance
1. WP manager’s name & organization
1. Supplier’s country
1. Product to which the tasks of the WP are allocated (link to the product tree)
1. Description of the objectives of the WP
1. Description of the tasks
1. List of the inputs necessary to achieve the tasks
1. Interfaces or links with other tasks or WP’s
1. List of constraints, requirements, standards, & regulations
1. List of the expected outputs
1. List of deliverables
1. Location of delivery
1. Start event identification including date
1. End event identification including date
1. Excluded tasks



### ---


GSE Data packages
GSE specifications
Space debris mitigation plan
