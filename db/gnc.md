# Бортовой комплекс управления
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [GNC](gnc.md), [Control](control.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Бортовой комплекс управления (БКУ)** — русскоязычный термин. **Guidance, navigation, control (GNC)** — примерный англоязычный эквивалент.</small>

**Бортовой комплекс управления (БКУ)** — это совокупность систем КА, обеспечивающих управление функционированием КА. БКУ предназначен для:

1. управления движением [центра масс](mic.md) КА и движением вокруг центра масс;
1. наведения [АФС](comms.md) на [НС](sc.md);
1. управления [бортовой аппаратурой](sc.md);
1. контроля работоспособности БА, переключения резервной аппаратуры в программируемых случаях;
1. сверки и коррекции [БШВ](time.md).

**Составные части:**

1. [Автономная система навигации](ans.md) (АСН, САН);
1. [Блоки автоматики и подрыва пиротехники](eas.md) (БАППТ);
1. [Бортовая кабельная сеть БКУ](cable.md) (БКС);
1. [Блок управления](sp.md) (БУ);
1. [Гироскоп](iu.md);
1. [Дальномер](doppler.md) (ИСР, измеритель скорости и расстояния);
1. [Двигатель‑маховик](iu.md) (ДМ);
1. [Звёздный датчик](sensor.md) (ЗД);
1. [Компьютер](obc.md) (ЦВМ, БЦВМ);
1. [Магнитометр](sensor.md);
1. [Программное обеспечение](soft.md);
1. [Привод направленной антенны](devd.md) (ПНА, ПОНА, ПСНА);
1. [Солнечный датчик](sensor.md) (СД);
1. [Система ориентации солнечных батарей](devd.md) (СОСБ);
1. [Система электроавтоматики](eas.md) (СЭА);

**Стандарты и [интерфейсы](interface.md):**

|**Name**|**Purpose**|**Bitrate**|
|:--|:--|:--|
|[CAN](can.md)|data exchange, only data‑link layer|1 Mbit/s|
|[JTAG](jtag.md)|testing|—|
|[LVDS](lvds.md)|data exchange, physical layer only|655 Mbit/s|
|[MIL-STD-1553](mil_std_1553.md)|data exchange|1 Mbit/s|
|[RS-232](rs_xxx.md)|data exchange|0.1 Mbit/s|
|[RS-422](rs_xxx.md)|data exchange|10 Mbit/s|
|[RS-485](rs_xxx.md)|data exchange|10 Mbit/s |
|[SpaceWire](spacewire.md)|data exchange, currently most perspective|400 Mbit/s|


## TMP

### AAC CS GNSS-701

**GNSS-701** — satellite GNSS receiver. Designed by [AAC ClydeSpace](aac_cs.md) in ….

|**Characteristics**|**(GNSS-701-A)**|**(GNSS-701-B)**|**(GNSS-701-C)**|
|:--|:--|:--|:--|
|Composition|2 units: receiver, antenna|Same|Same|
|Consumption, W|A: 0.9 for 3.3V, 1.0 for 7.5V, 1.2 for 28V (incl. Active Antenna)<br> B: 1.3 for 3.3V, 1.4 for 7.5V, 1.6 for 28V (incl. Active Antenna)<br> C: 1.8 for 3.3V, 2.0 for 7.5V, 2.2 for 28V (incl. Active Antenna)|Same|Same|
|Dimensions, ㎜|94 × 56 × 27|Same|Same|
|[Interfaces](interface.md)|2 Serial Ports (LVCMOS or RS-422) with Binary & ASCII Messages ≤460 kbps, 1 USB2.0 Port|Same|Same|
|[Lifetime](lifetime.md), h(y)| | | |
|Mass, ㎏|0.16|Same|Same|
|[Overload](vibration.md), Grms| | | |
|[Radiation](ion_rad.md), ㏉(㎭)|100 (10 000)|Same|Same|
|[Reliability](qm.md)| | | |
|[Thermal](tcs.md), ℃|−40 ‑ +85 (−55 ‑ +95 storage)|Same|Same|
|[TRL](trl.md)|9|9|9|
|[Voltage](sps.md), V|3.3 Regulated, 4.5 ‑ 20 Unregulated, 8 ‑ 42 Unregulated (Build Time Option)|Same|Same|
|**【Specific】**|~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |~ ~ ~ ~ ~ |
|Accuracy (Time)|20 ηsec RMS| | |
|Accuracy (Position)|<5 meters RMS| | |
|Accuracy (Velocity)|<0.10 ㎧ RMS| | |
|Constellations|GPS, GPS+GLO, GPS+GLO+GAL|GPS, GPS+GLO, GPS+GLO+BDS|GPS, GPS+GLO, GPS+GLO+BDS, GPS+GLO+GAL+BDS|
|Frequencies|L1, E1, B1|+ L2, E5b, B2|+ L5, E5a, AltBOC, B3|
|I/O Messages|Output: >150 output message types (position, velocity, time, etc.)<br> Input: >100 input command types|Same|Same|
|RF Inputs|1 SMA Female for Active Antenna|Same|Same|
|Signals|LVCMOS Outputs: pulse/s, position valid, variable frequency<br> LVCMOS Inputs: reset & 2 edge‑trigger|Same|Same|
|Time to 1st fix, s|90 cold start, 45 warm start, 30 hot start| | |
| |[![](f/gnc/a/aaccs_gnss_701_pic1t.webp)](f/gnc/a/aaccs_gnss_701_pic1.webp)| | |



## Designers, manufacturers
1. **РФ:**
   1. БКУ КА производят все, кто производит КА ([ВНИИЭМ](vniiem.md), [ИСС](iss_r.md), [LAV](lav.md) и т.д.)
   1. СЧ БКУ — см. соответствующую СЧ из списка выше

【**Table.** Manufacturers】

| | |
|:--|:--|
|**AE**|…|
|**AU**|…|
|**CA**|…|
|**CN**|…|
|**EU**|…|
|**IL**|…|
|**IN**|…|
|**JP**|…|
|**KR**|…|
|**RU**|…|
|**SA**|…|
|**SG**|…|
|**US**|…|
|**VN**|…|



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Guidance, Navigation & Control (GNC)](gnc.md)】**<br> [CAN](can.md) ~~ [LVDS](lvds.md) ~~ [MIL‑STD‑1553](mil_std_1553.md) (МКО) ~~ [OS](os.md) ~~ [RS‑232, 422, 485](rs_xxx.md) ~~ [SpaceWire](spacewire.md) ~~ [АСН, САН](ans.md) ~~ [БНО](nnb.md)[MIL‑STD‑1553](mil_std_1553.md) (МКО)[БАППТ](eas.md) ~~ [БКС](cable.md) ~~ [БУ](eas.md) ~~ [БШВ](time.md) ~~ [Гироскоп](iu.md) ~~ [Дальномер](doppler.md) (ИСР) ~~ [ДМ](iu.md) ~~ [ЗД](sensor.md) ~~ [Компьютер](obc.md) (ЦВМ, БЦВМ) ~~ [Магнитометр](sensor.md) ~~ [МИХ](mic.md) ~~ [МКО](mil_std_1553.md) ~~ [ПО](soft.md) ~~ [ПНА, ПОНА, ПСНА](devd.md) ~~ [СД](sensor.md) ~~ [Система координат](coord_sys.md) ~~ [СОСБ](devd.md)|
|**【[Control](Control.md)】**<br> [Ad hoc](ad_hoc.md) ~~ [Business travel](business_travel.md) ~~ [Chief designers council](cocd.md) ~~ [CML](trl.md) ~~ [Competence](competence.md) ~~ [Confident](confident.md) ~~ [Consp.theory](consp_theory.md) ~~ [Control sys. (CS)](cs.md) ~~ [Coordinate system](coord_sys.md) ~~ [Curator](curator.md) ~~ [Designer’s supervision](des_spv.md) ~~ [E‑sig](esig.md) ~~ [Engineer](se.md) ~~ [Errand](errand.md) ~~ [Federal law](fed_law.md) ~~ [Federal TP](fed_tp.md) ~~ [Federal SP](fed_sp.md) ~~ [GNC](gnc.md) ~~ [Gravity assist](gravass.md) ~~ [Industrial archaeology](ind_arch.md) ~~ [Instruction](instruction.md) ~~ [Lean manuf.](lean_man.md) ~~ [Lifetime](lifetime.md) ~~ [Manager](manager.md) ~~ [MBSE](se.md) ~~ [Meeting](meeting.md) ~~ [MCC](sc.md) ~~ [MIC](mic.md) ~~ [MML](mml.md) ~~ [MoU](contract.md) ~~ [Nav. & ballistics (NB)](nnb.md) ~~ [Nonprofit org.](nonprof_org.md) ~~ [NX](nx.md) ~~ [Oberth effect](oberth_eff.md) ~~ [Org.structure](orgstruct.md) ~~ [Outcomes commission](outccom.md) ~~ [Patent](patent.md) ~~ [Peter prin.](peter_principle.md) ~~ [Plan](plan.md) ~~ [PMBok](pmbok.md) ~~ [Quorum](quorum.md) ~~ [R&D management](mgmt.md) ~~ [R&D support](rnd_support.md) ~~ [Recursion](recurs.md) ~~ [Schulze_method](schulze_method.md) ~~ [Sci'N'Tech activities](st_act.md) ~~ [Sci'N'Tech council](satc.md) ~~ [Single-window system](sw_sys.md) ~~ [Situ.leadership](situ_leadership.md) ~~ [Skunk works](se.md) ~~ [State arm. plan](plan_sa.md) ~~ [Swamp](swamp.md) ~~ [Teamcenter](teamcenter.md) ~~ [Tennis racket theorem](tr_theorem.md) ~~ [TRIZ](triz.md) ~~ [TRL](trl.md) ~~ [V‑model](v_model.md) ~~ [Veto](veto.md) ~~ [Workflow](workflow.md) ~~ [Workgroup](wg.md)|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Guidance,_navigation,_and_control>


## The End

end of file
