# Система координат
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [GNC](gnc.md), [Control](control.md), [SE](se.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Система координат (СКТ)** — русскоязычный термин. **Coordinate system** — англоязычный эквивалент.</small>

**Систе́ма координа́т (СКТ)** — комплекс определений, реализующий метод координат, то есть способ определять положение и перемещение точки или тела с помощью чисел или других символов. Совокупность чисел, определяющих положение конкретной точки, называется координатами этой точки.
1. **В математике** координаты — совокупность чисел, сопоставленных точкам многообразия в некоторой карте определённого атласа.
1. **В элементарной геометрии** координаты — величины, определяющие положение точки на плоскости и в пространстве. На плоскости положение точки чаще всего определяется расстояниями от двух прямых (координатных осей), пересекающихся в одной точке (начале координат) под прямым углом; одна из координат называется ординатой, а другая — абсциссой. В пространстве по системе Декарта положение точки определяется расстояниями от трёх плоскостей координат, пересекающихся в одной точке под прямыми углами друг к другу, или сферическими координатами, где начало координат находится в центре сферы.
1. **В географии** координаты выбираются как (приближённо) сферическая система координат — широта, долгота и высота над известным общим уровнем (например, океана).
1. **В астрономии** небесные координаты — упорядоченная пара угловых величин (например, прямое восхождение и склонение), с помощью которых определяют положение светил и вспомогательных точек на небесной сфере. В астрономии употребляют различные системы небесных координат. Каждая из них по существу представляет собой сферическую систему координат (без радиальной координаты) с соответствующим образом выбранной фундаментальной плоскостью и началом отсчёта. В зависимости от выбора фундаментальной плоскости система небесных координат называется горизонтальной (плоскость горизонта), экваториальной (плоскость экватора), эклиптической (плоскость эклиптики) или галактической (галактическая плоскость).

Наиболее используемая система координат — прямоугольная система координат (также известная как декартова система координат).

Координаты на плоскости и в пространстве можно вводить бесконечным числом разных способов. Решая ту или иную математическую или физическую задачу методом координат, можно использовать различные координатные системы, выбирая ту из них, в которой задача решается проще или удобнее в данном конкретном случае. Известным обобщением системы координат являются системы отсчёта и системы референции.



## Основные системы
1. Декартовы координаты
1. Полярные координаты
1. Цилиндрические координаты
1. Сферические координаты



## Небесные системы
**Система небесных координат (СНК)** используется в астрономии для описания положения светил на небе или точек на воображаемой небесной сфере. Координаты светил или точек задаются двумя угловыми величинами (или дугами), однозначно определяющими положение объектов на небесной сфере. Таким образом, система небесных координат является сферической системой координат, в которой третья координата — расстояние — часто неизвестна и не играет роли.

Системы небесных координат отличаются друг от друга выбором основной плоскости (см. Фундаментальная плоскость) и началом отсчёта. В зависимости от стоя́щей задачи, может быть более удобным использовать ту или иную систему. Наиболее часто используются горизонтная и экваториальная системы координат. Реже — эклиптическая, галактическая и другие.

1. Горизонтальная топоцентрическая система координат
1. Первая экваториальная система координат
1. Вторая экваториальная система координат
1. Эклиптическая система координат
1. Галактическая система координат



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Guidance, Navigation & Control (GNC)](gnc.md)】**<br> [CAN](can.md) ~~ [LVDS](lvds.md) ~~ [MIL‑STD‑1553](mil_std_1553.md) (МКО) ~~ [OS](os.md) ~~ [RS‑232, 422, 485](rs_xxx.md) ~~ [SpaceWire](spacewire.md) ~~ [АСН, САН](ans.md) ~~ [БНО](nnb.md)[MIL‑STD‑1553](mil_std_1553.md) (МКО)[БАППТ](eas.md) ~~ [БКС](cable.md) ~~ [БУ](eas.md) ~~ [БШВ](time.md) ~~ [Гироскоп](iu.md) ~~ [Дальномер](doppler.md) (ИСР) ~~ [ДМ](iu.md) ~~ [ЗД](sensor.md) ~~ [Компьютер](obc.md) (ЦВМ, БЦВМ) ~~ [Магнитометр](sensor.md) ~~ [МИХ](mic.md) ~~ [МКО](mil_std_1553.md) ~~ [ПО](soft.md) ~~ [ПНА, ПОНА, ПСНА](devd.md) ~~ [СД](sensor.md) ~~ [Система координат](coord_sys.md) ~~ [СОСБ](devd.md)|
|**【[Control](Control.md)】**<br> [Ad hoc](ad_hoc.md) ~~ [Business travel](business_travel.md) ~~ [Chief designers council](cocd.md) ~~ [CML](trl.md) ~~ [Competence](competence.md) ~~ [Confident](confident.md) ~~ [Consp.theory](consp_theory.md) ~~ [Control sys. (CS)](cs.md) ~~ [Coordinate system](coord_sys.md) ~~ [Curator](curator.md) ~~ [Designer’s supervision](des_spv.md) ~~ [E‑sig](esig.md) ~~ [Engineer](se.md) ~~ [Errand](errand.md) ~~ [Federal law](fed_law.md) ~~ [Federal TP](fed_tp.md) ~~ [Federal SP](fed_sp.md) ~~ [GNC](gnc.md) ~~ [Gravity assist](gravass.md) ~~ [Industrial archaeology](ind_arch.md) ~~ [Instruction](instruction.md) ~~ [Lean manuf.](lean_man.md) ~~ [Lifetime](lifetime.md) ~~ [Manager](manager.md) ~~ [MBSE](se.md) ~~ [Meeting](meeting.md) ~~ [MCC](sc.md) ~~ [MIC](mic.md) ~~ [MML](mml.md) ~~ [MoU](contract.md) ~~ [Nav. & ballistics (NB)](nnb.md) ~~ [Nonprofit org.](nonprof_org.md) ~~ [NX](nx.md) ~~ [Oberth effect](oberth_eff.md) ~~ [Org.structure](orgstruct.md) ~~ [Outcomes commission](outccom.md) ~~ [Patent](patent.md) ~~ [Peter prin.](peter_principle.md) ~~ [Plan](plan.md) ~~ [PMBok](pmbok.md) ~~ [Quorum](quorum.md) ~~ [R&D management](mgmt.md) ~~ [R&D support](rnd_support.md) ~~ [Recursion](recurs.md) ~~ [Schulze_method](schulze_method.md) ~~ [Sci'N'Tech activities](st_act.md) ~~ [Sci'N'Tech council](satc.md) ~~ [Single-window system](sw_sys.md) ~~ [Situ.leadership](situ_leadership.md) ~~ [Skunk works](se.md) ~~ [State arm. plan](plan_sa.md) ~~ [Swamp](swamp.md) ~~ [Teamcenter](teamcenter.md) ~~ [Tennis racket theorem](tr_theorem.md) ~~ [TRIZ](triz.md) ~~ [TRL](trl.md) ~~ [V‑model](v_model.md) ~~ [Veto](veto.md) ~~ [Workflow](workflow.md) ~~ [Workgroup](wg.md)|
|**【[Systems engineering](se.md)】**<br> [Competence](competence.md) ~~ [Coordinate system](coord_sys.md) ~~ [Designer’s supervision](des_spv.md) ~~ [Industrial archaeology](ind_arch.md) ~~ [Instruction](instruction.md) ~~ [Lean manuf.](lean_man.md) ~~ [Lifetime](lifetime.md) ~~ [MBSE](se.md) ~~ [MML](mml.md) ~~ [Nav. & ballistics (NB)](nnb.md) ~~ [NASA SEH](book_nasa_seh.md) ~~ [Oberth effect](oberth_eff.md) ~~ [PMBok](pmbok.md) ~~ [Quorum](quorum.md) ~~ [R&D management](mgmt.md) ~~ [R&D support](rnd_support.md) ~~ [Recursion](recurs.md) ~~ [Schulze_method](schulze_method.md) ~~ [Sci'N'Tech activities](st_act.md) ~~ [Sci'N'Tech council](satc.md) ~~ [Skunk works](se.md) ~~ [SysML](sysml.md) ~~ [Tennis racket theorem](tr_theorem.md) ~~ [TRIZ](triz.md) ~~ [TRL](trl.md) ~~ [V‑model](v_model.md) ~~ [Workflow](workflow.md) ~~ [Workgroup](wg.md)|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Coordinate_system>
1. <https://en.wikipedia.org/wiki/Astronomical_coordinate_systems>
1. <https://en.wikipedia.org/wiki/Equatorial_coordinate_system>


## The End

end of file
