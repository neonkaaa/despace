# Buffalo Univ.
> 2019.08.08 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/b/buffalo_univ_logo1t.webp)](f/c/b/buffalo_univ_logo1.webp)|<mark>noemail</mark>, +1(716)645-20-00, Fax …;<br> *12 Capen Hall, Buffalo, New York 14260-1660, USA*<br> <http://www.buffalo.edu> ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/University_at_Buffalo)|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Университет штата Нью‑Йорк в Буффало**. It is commonly referred to as **SUNY Buffalo** or the **University at Buffalo (UB)**, & was formerly known as the **University of Buffalo**. Государственный научно‑исследовательский университет в США, являющийся флагманом системы университетов штата Нью‑Йорк (SUNY). Университет Буффало имеет несколько кампусов — в Буффало и Амхерсте.



## The End

end of file
