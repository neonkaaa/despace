# Infostellar
> 2020.07.17 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/i/infostellar_logo1t.webp)](f/c/i/infostellar_logo1.webp)|<info@istellar.jp>, <mark>nophone</mark>, Fax …;<br> *Kearney Place 3F, 8 Chome-8-15 Nishigotanda, Shinagawa City, Tokyo 141-0031, Japan*<br> <https://infostellar.net>・ <https://www.stellarstation.com>・ <https://makesat.com> ~~ [LI ⎆](https://www.linkedin.com/company/infostellar)|
|:--|:--|
|**Business**|Sat Ground Segment as a Service provider, cubesat components|
|**Mission**|…|
|**Vision**|…|
|**Values**|**Customer Success.** We are passionate about serving our customers. We express our passion by being focused on ensuring their success as well as our own.・ **One Infostellar.** We are one team, each member is vital to our mission.  As a team we seek what is best for Infostellar as a whole, rather than what is best for the individual.・ **Ownership.** We are responsible for the company's success. Being proactive improves our company, our product & our customer experience.・ **Diversity & Inclusion.** We are committed to making Infostellar a place where people from all backgrounds feel like they belong & can contribute.  We care about each team member. We embrace different perspectives with respect allowing us to be a more effective team.|
|**[MGMT](mgmt.md)**|・CEO, Founder — Naomi Kurahara|

**Infostellar** is a Japanese space communication infrastructure firm making space communications infrastructure, & are now developing our cloud‑based satellite [antenna sharing platform](scs.md), StellarStation. By lowering costs & increasing transmission time, Infostellar empowers satellite operators to improve the quality of their service & expand potential business. Founded 2016.01.04.

Team’s experience:

1. **Space & telecommunications.** Our small team boasts over 80 years of combined experience in the space industry, from the nuts & bolts of RF engineering to the transnational impact of regulatory affairs & government policy.
1. **Engineering.** With resumes spanning MongoDB, Google, Kymeta, Microsoft & beyond, our team possesses both breadth & depth of engineering experience.
1. **International partnerships.** Our team has professional experience spanning over 23 countries. Whether it’s navigating regulatory regimes or cultural nuances, we are well equipped to tackle the complexities of an inherently global industry.

Working towards a world where all LEO antennas are part of one vast network:

- The best of space & software engineering. We bring together agile software development & RF & satellite systems engineering experience to create innovative, forward‑thinking products for modern space businesses.
- Expertise & experience in radio regulations. Our dedicated Regulatory Affairs team navigates the world’s regulatory regimes  to create a streamlined onboarding experience, regardless of how many countries you’re operating in.
- Protocol & hardware‑agnostic platform. Our single API & partnerships with hardware manufacturers revolutionise scaling for ground segments. Simply integrate with our network once — & get access to every compatible ground station in it.
- Our product: StellarStation. A cloud‑based ground station platform offering ground station owners an opportunity to monetise their surplus capacity, & enabling satellite operators to communicate with their satellites from a varied range of antennas around the world, all through one common interface.
- 10+ UHF/S/X antennas — & more on the way.



## The End

end of file
