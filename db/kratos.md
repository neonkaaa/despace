# Kratos IS
> 2020.07.17 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/k/kratos_is_logo1t.webp)](f/c/k/kratos_is_logo1.webp)|<support@integ-japan.com>, <mark>nophone</mark>, Fax …;<br> *3-chōme-7-26 Ariake, Koto City, Tōkyō-to 135-0063, Japan*<br> <http://www.integ-japan.com>・ <https://www.kratosdefense.com/products/space> ~~ [LI ⎆](https://www.linkedin.com/company/integral-systems-inc.)|
|:--|:--|
|**Business**|Ground antennas, optimizing / managing satellites, signals|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Kratos Integral Systems** is a company aimed for R&D [ground antennas](scs.md), optimizing/managing satelllites, signals.

- Systems. Design & install systems for:
   - Ground systems/gateways
   - Spectrum regulation
   - Telemetry Tracking & Command
- Networks:
   - Assure uptime & health
   - Protect against cyber-attacks
   - Transport data reliably
- Satellites:
   - Command & control satellites
   - Hardware/software for TT&C
   - Manufacture & integrate antennas
- Signals:
   - Monitor the RF signal quality
   - Process signals reliably
   - Test & simulate scenarios
- Antennas:
   - Greenfield to Turnkey System Upgrades
   - Multiband Solutions for L-,S-,C-,X-,Ku-,K-,Ka-,Q/V
   - Sizes Ranging from 2.4M to 18M



## The End

end of file
