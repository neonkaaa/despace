# ecss_est31

[TOC]

---


## Annex C (normative) TCS analysis report — DRD

**C.1 DRD identification.** *Requirement ID & source* — ECSS‑E‑ST‑31, requirement 1.1.1.1.1 & 1.1.1.1.1. *Purpose & objective* — the **TCS analysis report (TCSAR)** is produced by the thermal control subsystem supplier to gather & document the results of analysis. The first issue is produced for the Phase A & updated throughout the project as further thermal control subsystem analysis & testing is carried out. The present DRD is also used to present test prediction & correlation with respect to major subsystem tests, such as Thermal Balance Test as well as for thermal control system flight prediction. The results of all analysis including test prediction & correlation carried out at subsystem level are reported in TCS analysis report. The documents are therefore critical for tracking the development of the thermal control subsystem throughout the project, ensuring that the TCS continues to meet the functional & performance requirements as the design & implementation are elaborated.

**C.2 Expected response**

*Special remarks* — None.

*Scope & content:*

1. **Introduction.** The purpose, objective, content, assumption & constraint & the reason prompting its preparation. State & describe any open issue relevant to the TCSAR.
1. **Applicable & reference documents, Definitions & abbreviations.** The applicable & reference documents in support to the generation of the document. List the applicable directory or glossary & the meaning of specific terms or abbreviations utilized in this document.
1. **Subsystem status.** Each issue of the TCSAR shall define the underlying design status of the TCS (development‑, test‑, qualification‑, protoflight‑, flight‑ model). Each issue of the TCSAR shall identify/evaluate analysis constraints with respect to in‑orbit configuration.
1. **Requirements review.** A list of subsystem requirements to be met by analysis.
1. **Overview of analysis approach.** An overview of the analysis approach applied to the TCS. Describe the different analysis techniques used. Include a description of tools used to carry out the analysis. Refer to GMM & TMM used for each analysis case.
1. **Description of thermal analysis cases.** Include a list of assumptions made concerning the thermal control subsystem & its environment during the analysis. Include a list of all input data including starting conditions for the analysis cases. Include a justification of assumed uncertainties.
1. **Results of analysis.** Include the results of the analyses represented as diagrams, temperature mapping, detailed comparison to requirements, heat balance. Include a compliance matrix for applicable requirements. Present the results of the analysis as an electronic file as agreed with the system authority.
1. **Assessment & conclusions**
   1. Summarize the analysis results & the comparison with the requirements.
   1. Provide an overall assessment.
   1. The requirement closeout may be summarized in dedicated tables to be prepared for each involved requirement or a group of requirements. (For examples see ECSS‑E‑HB‑10‑02)
   1. Clearly state & describe any open issue.
   1. Recommend the design changes & adaptations at subsystem & system level based on analysis results.
