# International Space University
> 2021.04.01 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c//_logo1t.webp)](f/c//_logo1.webp)|<mark>noemail</mark>, +33(0388)65-54-30, Fax …;<br> *…*<br> <https://www.isunet.edu> ~~ [LI ⎆](https://www.linkedin.com/school/international-space-university)|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

The **International Space University** provides graduate‑level training to the future leaders of the emerging global space community at its Central Campus in Strasbourg, France, and at locations around the world.

In its two‑month Space Studies Program and one‑year Masters program, ISU offers its students a unique Core Curriculum covering all disciplines related to

- space programs and enterprises,
- space science,
- space engineering,
- systems engineering,
- space policy and law,
- business and management,
- space and society.

Both programs also involve an intense student research Team Project providing international graduate students and young space professionals the opportunity to solve complex problems by working together in an intercultural environment.



## The End

end of file
