# Штатный образец
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [Test](test.md), [ЛИ](rnd_e.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Штатный образец** — русскоязычный термин, не имеющий аналога в английском языке. **Flight unit** — дословный перевод с русского на английский.</small>

**Штатный образец (ШО)**, также **штатное изделие (ШИ)** — [изделие](unit.md), полностью изготовленное по [конструкторской документации](doc.md) с литерой 「О」 или 「О₁」 и предназначенное для [лётных испытаний](rnd_e.md) или эксплуатации, соответственно.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Test](test.md)】**<br> [JTAG](jtag.md) ~~ [Proto fligt model](pfm.md) ~~ [Безэховая камера](ach.md) ~~ [Валидация](vnv.md) ~~ [Класс чистоты](clean_lvl.md) ~~ [КПЭО](ctpr.md) ~~ [Перечень методик испытаний](list_tp.md) ~~ [Программа и методика испытаний](pmot.md) ~~ [Опытный образец](pilot_sample.md) ~~ [Циклограмма](obc.md) ~~ [Штатный образец](flight_unit.md) ~~ [ЭО](test.md) ~~ [Экспериментально‑теоретический метод](etetm.md)|

1. Docs:
   1. [РК‑11](const_rk.md).
1. <…>


## The End

end of file
