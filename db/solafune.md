# Solafune
> 2021.12.08 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/s/solafune_logo1t.webp)](f/c/s/solafune_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *3-6-16, Uechi, Okinawa Shi, Okinawa Ken, 904-0031, Japan*<br> <https://solafune.com>|
|:--|:--|
|**Business**|Sat data analysis, software development, related services|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・Founder — Ren Uechi|

**Solafune Inc.** is a Data Science Competition platform that specializes in satellite & geospatial data to solve social and corporative issues. Founded 2020.

We provide a place to challenge data science. Through competition, you can access satellite data & geospatial data. Analysis results are evaluated in real time.

<p style="page-break-after:always"> </p>

## The End

end of file
