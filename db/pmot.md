# Программа и методика испытаний
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [Doc](doc.md), **[Test](test.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Программа и методика испытаний (ПИМС)** — русскоязычный термин, не имеющий аналога в английском языке. **Procedure & methods of tests** — дословный перевод с русского на английский.</small>

**Программа и методика испытаний (ПИМС)** — по [ГОСТ 2.102](гост_2_102.md) — документ, содержащий технические данные, подлежащие проверке при [испытании](test.md) [изделий](unit.md), а также порядок и методы их контроля.

В соответствии с ГОСТ 2.102 входит в состав [конструкторской документации](doc.md).



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Test](test.md)】**<br> [JTAG](jtag.md) ~~ [Proto fligt model](pfm.md) ~~ [Безэховая камера](ach.md) ~~ [Валидация](vnv.md) ~~ [Класс чистоты](clean_lvl.md) ~~ [КПЭО](ctpr.md) ~~ [Перечень методик испытаний](list_tp.md) ~~ [Программа и методика испытаний](pmot.md) ~~ [Опытный образец](pilot_sample.md) ~~ [Циклограмма](obc.md) ~~ [Штатный образец](flight_unit.md) ~~ [ЭО](test.md) ~~ [Экспериментально‑теоретический метод](etetm.md)|

1. Docs:
   1. [ГОСТ 2.102](гост_2_102.md)
1. <…>


## The End

end of file
