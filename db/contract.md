# Договор
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [Doc](doc.md), [ТЭО](fs.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small> ・**Договор** — русскоязычный термин. **Contract** — англоязычный эквивалент.<br> ・**Трудовой договор (ТДД)** — русскоязычный термин. **Employment contract** — англоязычный эквивалент.<br> ・**Государственный контракт (ГК, ГСК)** — русскоязычный термин. **Public contract** — англоязычный эквивалент.</small>  
>> <small>Согласие есть продукт при полном непротивлении сторон.<br> *「12 стульев」*</small>  
>>> <small>Бабушка поспорила с Сёмой, что он не съест 25 её пельменей на то, что он уберёт в квартире… И вот Сёма доедает 24‑й пельмень и понимает, что 25‑го в тарелке нет… Это всё, что надо знать о составлении договоров.</small>

**Догово́р** *(мн. ч. — догово́ры)* — 「соглашение двух или более лиц об установлении, изменении или прекращении гражданских прав и обязанностей」 (ст. 420 Гражданского кодекса РФ).

**Догово́р** *(мн. ч. — догово́ры)* — 「соглашение двух или более лиц об установлении, изменении или прекращении гражданских прав и обязанностей」 (ст. 420 Гражданского кодекса РФ).

При договоре от каждой стороны, как правило, требуется встречное удовлетворение. Сторонами договора могут выступать как физические, так и юридические лица, включая различные публично‑правовые образования (международные организации, государство, муниципальные образования и пр.).

Используется в трёх значениях: договор как правоотношение; как юридический факт, порождающий обязательства; как документ, фиксирующий факт возникновения обязательств по воле его участников.

Синонимы: контракт, государственный контракт.



## Трудовой договор
**Трудово́й догово́р** — в трудовом праве письменный документ — соглашение между работником и работодателем, которое устанавливает их взаимные права и обязанности. В соответствии с трудовым договором работник обязуется лично выполнять работу по определённой должности, соответствующей его квалификации, а работодатель обязуется предоставлять работнику работу, обеспечивать условия труда и своевременно выплачивать заработную плату.

Подчинение внутреннему трудовому распорядку является одной из основных характеризующих черт трудового договора, отделяющих его от различных гражданско‑правовых договоров (подряда, оказания услуг и пр.).

Служебные обязанности и иные особенности работы на определённой должности регулируются [должностной инструкцией](дин.md), с которой работника обязаны ознакомить при подписании договора, если они не были перечислены в трудовом договоре.

Перед заключением трудового договора на работодателе лежит обязанность по ознакомлению работника также с иными [локальными нормативными актами организации](doc.md).

Обязательные нормы трудового договора в основном регулируются [Трудовым кодексом](labour_code_ru.md) и иными правовыми актами трудового законодательства, а для отдельных организаций, отраслей хозяйства или административно‑территориальных единиц могут устанавливаться также коллективными договорами.

Действиям работодателя и работника, направленным на приём на работу по определённой специальности либо должности, присущ следующий алгоритм:

1. ознакомление будущего работника с правилами внутреннего распорядка, должностной инструкцией и выдача их копий работнику (по требованию);
1. подписание (заключение) трудового договора;
1. издание (на основании трудового договора) приказа о принятии работника на работу;
1. внесение записи (на основании приказа) о приёме на работу в трудовую книжку работника.



## Memorandum of Understanding
> <small>**Меморандум о взаимопонимании (МОВ)** — русскоязычный термин. **Memorandum of understanding (MoU)** — англоязычный эквивалент.</small>

A **memorandum of understanding** *(MoU, Меморандум о взаимопонимании, МОВ)* — is a type of agreement between two (bilateral) or more (multilateral) parties. It expresses a convergence of will between the parties, indicating an intended common line of action. It is often used in cases where parties either do not imply a legal commitment or in situations where the parties cannot create a legally enforceable agreement. It is a more formal alternative to a gentlemen’s agreement.

Whether a document constitutes a binding contract depends only on the presence or absence of well‑defined legal elements in the text proper of the document (the so‑called 「four corners」). The required elements are: offer & acceptance, consideration, & the intention to be legally bound (animus contrahendi). In the U.S., the specifics can differ slightly depending on whether the contract is for goods (falls under the Uniform Commercial Code, UCC) or services (falls under the common law of the state).



## Letter of intent (LOI)
A **letter of intent** (**LOI** or **LoI**, or **Letter of Intent**) is a document outlining the understanding between two or more parties which they intend to formalize in a legally binding agreement. The concept is similar to a heads of agreement, term sheet or memorandum of understanding. Such outlined agreements may be merger & acquisition transaction agreements, joint venture agreements, real property lease agreements & several other categories of agreements that may govern material transactions.

The capitalized form Letter of Intent may be used in legal writing, but only when referring to a specific document under discussion.

LOIs resemble short, written contracts, often in tabular form. They are not binding on the parties in their entirety. Many LOIs, however, contain provisions that are binding, such as those governing non‑disclosure, governing law, exclusivity or covenants to negotiate in good faith. A LOI may sometimes be interpreted by a court of law as binding the parties to it if it too‑closely resembles a formal contract & does not contain clear disclaimers.

A letter of intent may be presented by one party to another party & subsequently negotiated before execution (or signature). If carefully negotiated, a LOI may serve to protect both parties to a transaction. For example, a seller of a business may incorporate what is known as a non‑solicitation provision, which would restrict the buyer’s ability to hire an employee of the seller’s business should the two parties not be able to close the transaction. On the other hand, a LOI may protect the buyer of a business by expressly conditioning its obligation to complete the transaction if it is unable to secure financing for the transaction.

Common purposes of a LOI are:

1. To allow parties to sketch out fundamental terms quickly before expending substantial resources on negotiating definitive agreements, finalizing due diligence, pursuing third‑party approvals & other matters
1. To declare officially that the parties are currently negotiating, as in a merger or joint venture proposal
1. To provide safeguards in case a deal collapses during negotiation
1. To verify certain issues regarding payments made for someone else (e.g. credit card payments)

Potential downsides to using a LOI may include:

1. The parties may engage in protracted negotiations on only a subset of a deal’s terms
1. Management time & focus may be diverted
1. Alternative opportunities may be missed & markets may move against the parties during negotiations
1. Parties may reduce their lack of a workable deal framework into a LOI, with a hope of making progress later
1. Public disclosure obligations may be inadvertently triggered
1. The risk of leaks, exacerbated by the desire of some to tout the LOI to the world, or shop it to other parties

In the UK construction industry, it has been noted that 「a significant element」 within the industry appears to be 「content to have their commercial & legal relationships defined on the basis of a letter of intent rather than by clear & definite contracts」.



### (RU) Письмо о намерениях
**Письмо о намерениях** *(англ. Letter of intent, LOI)* — письмо, где автор (например, покупатель) формально сообщает о своих намерениях что‑то совершить, например: закупить партию товара на тех или иных условиях, подписать тот или иной контракт, осуществить куплю‑продажу акций, войти в полное или частичное управление предприятием и т. д.

Целями письма о намерениях могут быть:

1. Обозначение поставщику покупателем условий, в которые могут входить, например: количество поставляемой продукции, её качество, условий доставки, а иногда и цены, при которых он согласен на осуществление сделки.
1. Предоставления поставщику более полной информации (включающей, например реквизиты) о покупателе.
1. Обеспечение гарантии в случае, если сделка 「провалилась」 в ходе переговоров.
1. Предварительное уведомление о дальнейших намерениях, после исполнения договоров или выполнения некоторых условий (например, намерение заключить трудовой контракт после окончания обучения).

В общем случае LOI не обязывает ни одну из сторон к последующим действиям и не накладывает штрафов на сторону, которая изменила своё решение до заключения конкретного договора.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[](.md)】**<br> <mark>NOCAT</mark>|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Government_procurement>
1. <https://en.wikipedia.org/wiki/Contract>
1. <https://en.wikipedia.org/wiki/Employment_contract>
1. <https://en.wikipedia.org/wiki/Memorandum_of_understanding>
1. <https://en.wikipedia.org/wiki/Letter_of_intent>


## The End

end of file
