# NASA Deep Space Network
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [JPL](jpl.md), [NASA](nasa.md), **[НС](sc.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**NASA Deep Space Network** — англоязычный термин, не имеющий аналога в русском языке. **Сеть дальней космической связи НАСА** — дословный перевод с английского на русский.</small>

**Сеть дальней космической связи НАСА** — международная сеть радиотелескопов и средств связи, используемых как для радиоастрономического исследования Солнечной системы и Вселенной, так и для управления межпланетными космическими аппаратами.

DSN является частью [Лаборатории реактивного движения НАСА](jpl.md). Аналогами этой сети можно назвать сеть ESTRACK Европейского центра управления космическими полётами (ЕКА), Восточный центр дальней космической связи в России, а также китайский и индийский центры дальней космической связи.

|**Расположение станций**|**Охват сферы**|
|:--|:--|
|[![](f/gs/dsn_networkt.webp)](f/gs/dsn_network.webp)|[![](f/gs/dsn_antennat.webp)](f/gs/dsn_antenna.webp)|

**Инструменты:**

1. **Голдстоун** (Калифорния, США) — радиообсерватория, планетный радар. [Bands](comms.md): <mark>TBD</mark>
1. **Мадридский комплекс дальней космической связи** (Робледо‑де‑Чавела, Испания) — радиообсерватория. [Bands](comms.md): **L** (🚀↘), **S** (♁↗ 🚀↘), **X** (♁↗ 🚀↘).
1. **Комплекс дальней космической связи в Канберре** (Австралия) — радиообсерватория. [Bands](comms.md): <mark>TBD</mark>



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**`Наземная станция (НС):`**<br> … <br><br> [CDSN](cdsn.md) ~~ [DSN](dsn.md) ~~ [ESTRACK](estrack.md) ~~ [IDSN](idsn.md) ~~ [SSC_GGSN](ssc_ggsn.md) ~~ [UDSC](udsc.md)|

1. Docs: …
1. <https://en.wikipedia.org/wiki/NASA_Deep_Space_Network>
1. <https://deepspace.jpl.nasa.gov>
1. <https://eyes.nasa.gov/dsn/dsn.html>


## The End

end of file
