# ОКБ Пятое Поколение
> 2019.08.06 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/o/okb_5_logo1t.webp)](f/c/o/okb_5_logo1.webp)|<tereshkova@5okb.ru>, +7(495)542-64-55, Fax …;<br> *Россия, 630090, Новосибирск, ул. Николаева, д. 11*<br> <https://vk.com/space_okb>・ <http://egrinf.com/4202500>・ <https://sbis.ru/contragents/5408305449/540801001>|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Общество с ограниченной ответственностью ОКБ Пятое Поколение.** Создано 19.11.2013.  
Создание и производство блоков управления и схем.



## The End

end of file
