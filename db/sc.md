# Spacecraft system
> 2019.05.05 [🚀](../../index/index.md) [despace](index.md) → [SCS](sc.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---



> <small>**・ Spacecraft system (SCS)** — EN term. **Космический комплекс (КК)** — RU analogue.<br> **・ Многоразовая космическая система (МКС)** — русскоязычный термин. **Reusable space system** — англоязычный эквивалент.</small>

**Spacecraft system (SCS)** — combination of space & ground segments.

**Космический комплекс (КК)** — совокупность функционально взаимосвязанных орбитальных и наземных технических средств, обеспечивающих как самостоятельное решение целевых задач на основе использования космического пространства, так и в составе космической системы.

1. **Космическая система (КС)** — [ГОСТ 53802](гост_53802.md) п.1‑7 — совокупность одного или нескольких КК и специальных комплексов, предназначенных для решения целевых задач.
1. **Космическая система (КС)** — [РК‑11‑КТ](const_rk.md) стр.17 — совокупность согласованно действующих и взаимосвязанных [КА](sc.md) и других технических средств КК и наземного специального комплекса, предназначенных для решения целевых задач. В КС могут входить несколько КК.
1. **Многоразовая космическая система (МКС)** — КС с орбитальными средствами многократного использования.

Common dividing SCS into parts for the US‑EU‑JP ecosystem:

**Spacecraft system** (SCS)  
░║  
░╟ **Ground segment** (GS)  
░║░╟ Ground networks  
░║░╟ [Ground (or Earth) stations](sc.md)  
░║░╟ Integration & test facilities  
░║░╟ Launch facilities  
░║░║░╟ [Launch vehicle](lv.md)  
░║░║░╟ [Spaceport](spaceport.md)  
░║░║░╙ Transport & supporting facilities  
░║░╟ [Mission (or flight) control (or operations) centers](sc.md) (MCC)  
░║░╟ Remote terminals  
░║░╙ [Search & rescue complex](sarc.md) (SARC)  
░║  
░╟ **User segment** (US)  
░║░╟ Customer terminals  
░║░╙ Network & data storage  
░║  
░╙ **Space segment** (SS)  
░░╙ **[Spacecraft](sc.md)** (SC) / **[Manned spacecraft](sc.md)** (MSC, ПКА) / constellation of those  
░░░╟ [Attitude control system](acs.md)  
░░░╟ [Command & Data Handling](c_n_dh.md) *(C&DH)*  
░░░║░╟ [Data storage system](ds.md)  
░░░║░╟ [Flight software](soft.md)  
░░░║░╟ [On-board computer](obc.md) *(OBC)*  
░░░║░╟ Software  
░░░║░╙ [Telemetry system](obc.md) *(TMS)*  
░░░╟ [Communication system](comms.md)  
░░░║░╟ Amplifiers  
░░░║░╟ Antennas  
░░░║░╙ Transmitters, receivers  
░░░╟ [Guidance, navigation, & control](gnc.md) *(GNC, БКУ)*  
░░░║░╟ Altimeter  
░░░║░╟ IMU  
░░░║░╟ Magnetometer  
░░░║░╟ Reaction wheel (RW, ДМ)  
░░░║░╟ Sensor for electrization  
░░░║░╟ Sensor for relative navigation  
░░░║░╟ Star trackers  
░░░║░╙ Sun sensors  
░░░╟ [Life support](ls.md)  
░░░╟ **[Payload](sc.md) (OE)**  
░░░║░╟ Mounting elements (Payload↔Spacecraft)  
░░░║░╙ Payload objects  
░░░╟ [Power system](sps.md) *(SPS)*  
░░░║░╟ Automation & stabilization unit  
░░░║░╟ [Cables](cable.md), connectors  
░░░║░╟ Electroautomatics  
░░░║░╟ Generator / converter  
░░░║░╙ Storage device  
░░░╟ [Propulsion system](ps.md) *(PS)*  
░░░║░╟ [Engines](engine_lst.md)  
░░░║░╟ Gears  
░░░║░╟ Heaters, sensors  
░░░║░╟ Pipes  
░░░║░╟ Protection, drainage  
░░░║░╟ Structures  
░░░║░╙ Tanks  
░░░╟ [Structures, gears, materials](sc.md) *(SGM)*  
░░░║░╟ Mounting gears, structures  
░░░║░╟ On-board gears  
░░░║░╙ On-board structures  
░░░╙ [Thermal system](tcs.md) *(TCS)*

Common dividing SCS into parts for the ex‑USSR ecosystem:

**Космическая система** (КС, может заменяться КК, если он один)  
░║  
░╙ **Космический комплекс** (КК)  
░░░║  
░░░╟ Наземный комплекс управления КА (НКУ)  
░░░║░╟ Баллистические центры (БЦ)  
░░░║░╟ Наземные станции (НС)  
░░░║░╟ Сектор главного конструктора КК (СГК)  
░░░║░╟ Средства связи и передачи данных (ССПД)  
░░░║░╙ Центр управления полётом (ЦУП)  
░░░║  
░░░╟ Наземный научный комплекс (ННК)  
░░░║░╟ Аппаратно‑программные средства (АПС) научного архива  
░░░║░╟ АПС обработки научных даннных  
░░░║░╟ АПС планирования работы КНА  
░░░║░╟ АПС приёма и анализа научных данных  
░░░║░╙ АПС связи и коммуникации  
░░░║  
░░░╙ Ракетно‑космический комплекс (РКК)  
░░░░╟ Комплекс разгонного блока (КРБ)  
░░░░╟ Комплекс ракеты‑носителя (КРН)  
░░░░╟ Комплекс средств измерения, сбора и обработки (КСИСО)  
░░░░╟ Технический комплекс КА (ТК КА)  
░░░░╟ Технический комплекс космической головной части (ТК КГЧ)  
░░░░╟ Технический комплекс РКН (ТК РКН)  
░░░░╙ Ракета космического назначения (РКН)  
░░░░░╟ Ракета‑носитель  
░░░░░╙ Космическая головная часть (КГЧ)  
░░░░░░╟ Головной обтекатель (ГО)  
░░░░░░╟ Переходной отсек (ПхО)  
░░░░░░╟ Разгонный блок (РБ)  
░░░░░░╙ **Космический аппарат** (КА)  
░░░░░░░╟ Адаптер с системой отделения  
░░░░░░░╟ Антенно‑фидерная система (АФС)  
░░░░░░░╟ Бортовая кабельная сеть (БКС)  
░░░░░░░╟ Бортовой комплекс управления (БКУ)  
░░░░░░░╟ Двигательная установка (ДУ)  
░░░░░░░╟ Комплекс научной аппаратуры (КНА)  
░░░░░░░╟ Конструкция и механизмы  
░░░░░░░╟ Радиокомплекс (РК)  
░░░░░░░╟ Система электроавтоматики (СЭА)  
░░░░░░░╟ Система электроснабжения (СЭС)  
░░░░░░░╟ Средства обеспечения теплового режима (СОТР)  
░░░░░░░╙ Телеметрическая система (ТМС)



**And logics:**

1. [Communications](comms.md)
   1. Ground & onboard antennas
   1. [RF agreements](comms.md)
1. [Control](control.md)
   1. [Management](mgmt.md)
1. [Documents](doc.md), incl. major ones:
   1. [Drafts, models](draw.md)
   1. [Report](report.md)
   1. [Terms of reference](tor.md) (TOR)
1. [Ecology](ecology.md)
1. [Electronic components](elc.md) (ElC)
1. [External factors](ef.md)
1. [Errors](faq.md)
1. [Human factors & ergonomics](hfe.md) (HF&E)
1. [Interfaces](interface.md)
1. [International System of Units](si.md) (SI)
1. [Key technologies](kt.md) (KT)
1. [Model](draw.md)
1. [Navigation & ballistics](nnb.md) (NB)
1. [Research & Development](rnd.md) (R&D)
1. [Safety, Reliability, Risk, Quality](qm.md) (SRRQ)
1. [Software](soft.md)
1. [Systems engineering](se.md)
1. [Tests](test.md)
1. [Timeline](timeline.md)
1. [TRL](trl.md) & [CML](trl.md)
1. **[Ecology](ecology.md)**·Экология
1. **[Ground station (GS)](sc.md)**·НС
1. **[Launch vehicle (LV)](lv.md)**·РН
1. **[Manned spacecraft (MSC)](sc.md)**·ПКА
1. **[Mission control center (MCC)](mcc.md)**·ЦУП
1. **[Search & rescue complex (SARC)](sarc.md)**·ПСК
1. **[Systems engineering (SE)](se.md)**·СЭ
1. **[Software](soft.md)**·ПО
1. **[Spaceport](spaceport.md)**·Космодром
1. Smth:
   1. [Класс чистоты](clean_lvl.md)
   1. [Контейнеры для транспортировки](ship_contain.md)
   1. [О выводимых массах](throw_weight.md)



## ➀ Ground segment
A **ground segment (GS)** consists of all the ground‑based control elements of a spacecraft system, as opposed to the space segment & user segment. The GS serves to enable control of a spacecraft, & distribution of payload data & telemetry among interested parties on the ground.

The ground segment, though not technically part of the spacecraft, is vital to the operation of the spacecraft. Typical components of a GS in use during normal operations include a mission operations facility where the flight operations team conducts the operations of the spacecraft, a data processing & storage facility, ground stations to radiate signals to & receive signals from the spacecraft, & a voice & data communications network to connect all mission elements.

The **[launch vehicle](lv.md)** propels the spacecraft from Earth’s surface, through the atmosphere, & into an orbit, the exact orbit being dependent on the mission configuration. The launch vehicle may be expendable or reusable.

| | |
|:--|:--|
|**AE**|…|
|**AU**|…|
|**CA**|・[ADGA](adga.md) — GS engineering (incl. software)<br> ・[BRASS](brass.md) — satellite operations<br> ・[Calian AT](calian_at.md) — Composite carbon fiber antenna, Comms systems, TT&C systems, Earth observation systems, Monitor & reconfiguration systems, Mission operations systems, Satellite gateway systems<br> ・[Honeywell Aerospace](honeywell_as.md) — signal & data processing, flight & ground operations<br> ・[MDA](mda.md)|
|**CN**|…|
|**EU**|…|
|**IL**|…|
|**IN**|…|
|**JP**|…|
|**KR**|…|
|**RU**|…|
|**SA**|…|
|**SG**|…|
|**US**|…|
|**VN**|…|



### Mission control center (MCC)
> <small>**Mission control center (MCC), Launch control center (LCC)** — EN term. **Центр управления полётами (ЦУП)** — RU analogue.</small>

A **mission control center** (**MCC**, sometimes called a **flight control center** or **operations center**) — a part of a [SCS](sc.md) — is a facility that manages space flights, usually from the point of launch until landing or the end of the mission. It is part of the ground segment of spacecraft operations. A staff of flight controllers & other support personnel monitor all aspects of the mission using telemetry, & send commands to the vehicle using ground stations. Personnel supporting the mission from an MCC can include representatives of the attitude control system, power, propulsion, thermal, attitude dynamics, orbital operations & other subsystem disciplines. The training for these missions usually falls under the responsibility of the flight controllers, typically including extensive rehearsals in the MCC.

![](f/control/mcc_global.webp)

**(RU) ЦУП**

**Центр управле́ния полётами (ЦУП)** — сооружения с техническими системами и технологическими средствами командно‑программного, телеметрического и [баллистико‑навигационного обеспечения](nnb.md), вешних информационных обменов, магистральных и специальных связей, отображения, предназначенными для обеспечения деятельности обслуживающего персонала по формированию, передаче, приёму, обработке, хранению, документированию информации при непрерывном процессе управления полётами [космических аппаратов](sc.md) в период проведения [лётных испытаний](rnd_e.md) и эксплуатации [космических систем](sc.md). Согласно типовой [схеме деления](draw.md) ЦУП входит в состав [НКУ](sc.md). Часто в состав ЦУП входят также [баллистические центры](sc.md).

【**Таблица.** Крупнейшие ЦУП】

|**#**|**List**|
|:--|:--|
|**[CN](cnsa.md)**|➀ Пекинский центр управления космическими полётами|
|**[EU](esa.md)**|➀ Европейский центр управления космическими объектами — Дармштадт, Германия<br> ➁ ATV Control Centre — Тулуза, Франция<br> ➂ Columbus Control Center — Оберпфаффенхофен, Германия|
|**[JP](jaxa.md)**|➀ NEC Satellite Operation Center ([NEC](nec.md))|
|**[RU](roskosmos.md)**|➀ Центр управления полётами [ЦНИИмаш](tsniimash.md) — Королёв, Московская область<br> ➁ Главный испытательный центр испытаний и управления космическими средствами имени Г.С. Титова — Краснознаменск, Московская область<br> ➂ Центр управления спутниками народно‑хозяйственного назначения (ЦУП-НХ) — [ОАО ИСС](iss_r.md), Железногорск<br> ➃ ЦУП‑Л — [LAV](lav.md), Химки, Московская область<br> ➄ ЦУП Бонум (ЦКС Сколково) — ФГУП 「Космическая связь」, Сколково, Московская область|
|**[US](nasa.md)**|➀ Центр управления полётами (НАСА) — Хьюстон, Техас<br> ➁ [JPL](jpl.md) — Пасадина, Калифорния|



## ➁ Space segment (Spacecraft)
> <small>**Космический аппарат (КА)** — русскоязычный термин. **Spacecraft (SC)** — англоязычный эквивалент.</small>  
> <small>**Типичные формы КА** — русскоязычный термин, не имеющий аналога в английском языке. **Typical spacecrafts shapes** — дословный перевод с русского на английский.</small>

**Космический аппарат (КА)**, нар. **Махарайка, пепелац** — техническое устройство, предназначенное для функционирования в космическом пространстве с целью решения задач в соответствии с назначением [КК (КС)](sc.md).

1. Docs:
   1. [ГОСТ 53802](гост_53802.md), п. 102‑103
1. <https://en.wikipedia.org/wiki/Payload>
1. <https://www.spacematdb.com>
1. <https://en.wikipedia.org/wiki/Reusable_launch_system>
1. <https://en.wikipedia.org/wiki/Spacecraft>
1. <https://en.wikipedia.org/wiki/Reusable_spacecraft>
1. <https://en.wikipedia.org/wiki/List_of_crewed_spacecraft>
1. <https://ru.wikipedia.org/wiki/Пилотируемый_космический_аппарат>
1. <https://ru.wikipedia.org/wiki/Пилотируемый_космический_корабль>
1. <https://www.nasa.gov/audience/forstudents/postsecondary/features/F_Spacecraft_Classification.html>

**Major units:**

1. **Attitude control.** A Spacecraft needs an attitude control subsystem to be correctly oriented in space & respond to external torques & forces properly. The attitude control subsystem consists of sensors & actuators, together with controlling algorithms. The attitude‑control subsystem permits proper pointing for the science objective, sun pointing for power to the solar arrays & earth pointing for communications.
1. **Command & data handling.** The CDH subsystem receives commands from the communications subsystem, performs validation & decoding of the commands, & distributes the commands to the appropriate spacecraft subsystems & components. The CDH also receives housekeeping data & science data from the other spacecraft subsystems & components, & packages the data for storage on a data recorder or transmission to the ground via the communications subsystem. Other functions of the CDH include maintaining the spacecraft clock & state‑of‑health monitoring.
1. **Communications.** SC, both robotic & crewed, utilize various communications systems for communication with terrestrial stations as well as for communication between spacecraft in space. Technologies utilized include RF & optical communication. In addition, some spacecraft payloads are explicitly for the purpose of ground‑ground communication using receiver/retransmitter electronic technologies.
1. **GNC.** Guidance refers to the calculation of the commands (usually done by the CDH subsystem) needed to steer the spacecraft where it is desired to be. Navigation means determining a spacecraft’s orbital elements or position. Control means adjusting the path of the spacecraft to meet mission requirements.
1. **Life support.** SC intended for human spaceflight must also include a life support system for the crew.
1. **Payload.** The payload depends on the mission of the SC, & is typically regarded as the part of the SC 「that pays the bills」. Typical payloads could include scientific instruments (cameras, telescopes, or particle detectors, for example), cargo, or a human crew.
1. **Power.** SC need an electrical power generation & distribution subsystem for powering the various SC subsystems. For spacecraft near the Sun, solar panels are frequently used to generate electrical power. SC designed to operate in more distant locations, for example Jupiter, might employ a radioisotope thermoelectric generator (RTG) to generate electrical power. Electrical power is sent through power conditioning equipment before it passes through a power distribution unit over an electrical bus to other spacecraft components. Batteries are typically connected to the bus via a battery charge regulator, & the batteries are used to provide electrical power during periods when primary power is not available, for example when a low Earth orbit spacecraft is eclipsed by Earth.
1. **Thermal control.** SC must be engineered to withstand transit through Earth’s atmosphere & the space environment. They must operate in a vacuum with temperatures potentially ranging across hundreds of ℃ as well as (if subject to reentry) in the presence of plasmas. Material requirements are such that either high melting temperature, low density materials such as beryllium & reinforced carbon‑carbon or (possibly due to the lower thickness requirements despite its high density) tungsten or ablative carbon‑carbon composites are used. Depending on mission profile, spacecraft may also need to operate on the surface of another planetary body. The thermal control subsystem (TCS) can be passive, dependent on the selection of materials with specific radiative properties. Active TCS makes use of electrical heaters & certain actuators such as louvers to control temperature of equipments within specific ranges.
1. **Spacecraft propulsion.** SC may or may not have a propulsion subsystem, depending on whether or not the mission profile calls for propulsion. The Swift spacecraft is an example of a spacecraft that does not have a propulsion subsystem. Typically though, LEO spacecraft include a propulsion subsystem for altitude adjustments (drag make‑up maneuvers) & inclination adjustment maneuvers. A propulsion system is also needed for spacecraft that perform momentum management maneuvers. Components of a conventional propulsion subsystem include fuel, tankage, valves, pipes, & thrusters. The thermal control system interfaces with the propulsion subsystem by monitoring the temperature of those components, & by preheating tanks & thrusters in preparation for a spacecraft maneuver.
1. **Structures.** SC must be engineered to withstand launch loads imparted by a [LV](lv.md), & must have a point of attachment for all the other subsystems. Depending on mission profile, the structural subsystem might need to withstand loads imparted by entry into the atmosphere of another planetary body, & landing on the surface of another planetary body.

Любой КА прост как смартфон среднего уровня. Осталось решить вопросы температур, перегрузок, радиации и вперёд. Что же общего ~~у ворона и письменного стола~~ у КА и смартфона 2015 года выпуска.

|**СЧ**|**КА**|**Смартфон**|
|:--|:--|:--|
|[Аккумулятор](eb.md)|+|+|
|[Акселерометр](.md)|(гироскоп)|+|
|[Антенны](antenna.md)|+|+|
|[Батарея солнечная](sp.md)|+|опция|
|[Блок автоматики и стабилизации](eas.md)|+|+|
|[Дальномер](doppler.md)|+|+|
|[Двигатель](ps.md)|+|–|
|[Двигатель‑маховик](iu.md)|+|–|
|[Звёздный датчик](sensor.md)|+|–|
|[Конструкция, механизмы, материалы](sc.md)|+|+|
|[Пилот](manned_sf.md)|+|+|
|[Привод антенны](devd.md)|+|опция|
|[Привод батареи солнечной](devd.md)|+|опция|
|[Провода](cable.md)|+|+|
|[Процессор](obc.md)|+|+|
|[Радиаторы, нагреватели](tcs.md)|+|+|
|[Радиомодуль](comms.md)|+|+|
|[Солнечный датчик](sensor.md)|+|+|
|[Телеметрия](tms.md)|+|+|

**Varieties:**

1. **Гайка** — [Фобос‑Грунт](фобос_грунт.md)
1. **Гнездо** — [Луна‑Грунт](luna_28.md)
1. **Голубятня** — [Luna‑25](луна_25.md), [Luna‑27](луна_27.md)
1. **Куб** — …
1. **Скворечник** — [Luna‑26](луна_26.md)

**Разновидности:**

1. **Авиационно‑космическая система (АКС)** — *Aerospace system* — РН с КА стартует с летящего самолёта.
1. **Автоматический космический аппарат (АКА)**.
1. **Композитный космический аппарат (ККА)** — КА, включающий в себя другие КА.
1. **Кубсат**.
1. **Многоразовая космическая транспортная система (МКТС, МТКС)** — *Reusable space transport system* — разновидность МКА.
1. **Многоразовый космический аппарат (МКА)**, иногда *「космический аппарат многоразового использования」* — *Re‑entry space vehicle* — КА, конструкция которого предусматривает повторное использование КА или его СЧ после возвращения из космического полёта.
1. **Модуль** — в некоторой степени автономная составная часть КА.
1. **Пилотируемый космический аппарат (ПКА)** — manned spacecraft (MSC) — КА для живых существ в космосе.
1. **Спутник**.

|**#**|**Class**|**Description**|
|:--|:--|:--|
|1|**Atmospheric**<br> (атмосферный КА) |1. Balloon — ➀ high‑height, ➁ low‑height, ➂ powered, ➃ simple, ➄ variable altitude<br> 2. Communications atmospheric spacecraft (КА для связи)<br> 3. Plane — ➀ glider, ➁ powered, ➂ maneuverable|
|2|**Base**<br> (база)|1. Ground<br> 2. Orbital|
|3|**Lander**<br> (посадочный КА)|1. Communications lander (КА для связи)<br> 2. Penetrator (КА‑пенетратор)<br> 3. Plane — ➀ glider, ➁ powered, ➂ maneuverable<br> 4. Rover (планетоход)|
|4|**Orbiter / Satellite**<br> (орбитальный КА)|1. [Communications orbiter](sc.md) (КА для связи) — ➀ high‑height, ➁ L1/L2, ➂ low‑height<br> 2. Fly‑by spacecraft (пролётный КА)<br> 3. Observatory (КА‑обсерватория) — ➀ high‑height, ➁ L1/L2, ➂ low‑height|
|5|**Robot**|1. Arm<br> 2. Antropomorphic<br> 3. Digger|
|6|**Other** (applicable<br> to almost any of the<br> above mentioned)|1. Manned — ➀ manned, ➁ unmanned<br> 2. Size — ➀ large, ➁ medium, ➂ micro, ➃ nano, ➄ small<br> 3. Swarm — ➀ net, ➁ single|

【**Table.** Manufacturers】

| | |
|:--|:--|
|**AE**|…|
|**AU**|…|
|**CA**|・[ADGA](adga.md) — space systems engineering<br> ・[AQST Canada](qstc.md)<br> ・[BRASS](brass.md) — design consultancy<br> ・[C-CORE](c_core.md) — technology consultancy<br> ・[Canadensys](canadensys.md)<br> ・[Magellan Aerospace](magellan_as.md)<br> ・[MDA](mda.md) — Earth sats<br> ・[MSCI](msci.md)<br> ・[SatCan](satcan.md) — engineering & business consultancy for sat telecom & space tech sectors<br> ・[Space Concordia](space_concordia.md) — (Student community) CubeSats<br> ・[UTIAS](utias.md) — Research for space mechatronics, robotics, microsats, fluid dynamics|
|**CN**|…|
|**EU**|…|
|**IL**|…|
|**IN**|…|
|**JP**|・[ALE](ale.md) — small Earth satellites<br> ・[Astroflash](astroflash.md) — small sats<br> ・[Astroscale](astroscale.md) — space debris<br> ・[Axelspace](axelspace.md) — microsats<br> ・[CE Space](ce_space.md) — microsats<br> ・[ispace](ispace.md)<br> ・[JAMSS](jamss.md) — Earth sats<br> ・JAXA [ISAS](isas.md)<br> ・JAXA [Tsukuba Space Center](tsukuba_sc.md)<br> ・[Kawasaki HVI](kawasaki_hvi.md)<br> ・[Meisei](meisei.md) — microsats<br> ・[Mitsubishi Electric](mitsubishi.md)<br> ・[NEC](nec.md)<br> ・[PDAS](pd_aerospace.md) — suborbital spaceplane for tourism<br> ・[SKY Perfect JSAT](sky_perfect_jsat.md)<br> ・[Space Walker](space_walker.md) — suborbital spaceplane for tourism<br> ・[Synspective](synspective.md) — small Earth sats<br> ・[Warpspace](warpspace.md)|
|**KR**|・[KAI](kai.md)<br> ・[KARI](kari.md)<br> ・[Satrec Initiative](satreci.md)|
|**RU**|・Lavochkin Association<br> ・[VNIIEM](vniiem.md)<br> ・[РКК Энергия](ркк_энергия.md)<br> ・[AvantSpace](avantspace.md)|
|**SA**|…|
|**SG**|…|
|**US**|・[Ball A&T](ball_at.md)<br> ・[Xplore](xplore.md)|
|**VN**|…|



### Gears, Materials, Structures
**Structures, gears, materials (SGM)** *(ru. Конструктивные элементы, механизмы, материалы, КММ)* — elements of SC or their parts.

**Gears & structures**

1. [Containers for transportation](ship_contain.md)
1. [Nominal](nominal.md)
1. **Gears:**
   1. [Directional Antenna Drive](devd.md)
   1. [Landing gear (PUS)](lag.md)
   1. [Mechanical gears](mech_gear.md)
   1. [Pusher](толкатель.md)
   1. [Robotics](robot.md)
1. **Structures:**
   1. [Capillar intake unit (CINU)](cinu.md)
   1. [Capillary gas storage (CGS)](cgs.md)
   1. [Hermetic container](гермоконтейнер.md)
   1. [Sensor](sensor.md)
   1. [Slice](слайс.md)
   1. [Thermal honeycomb](tsp.md)
   1. [Typical forms of SC](sc.md)

**Materials**

1. The use of the following materials in a SC (especially, in parts interacting with an atmosphere or soil or affecting SC actions) shall be deliberately limited:<br> Diamond, Platinoids, Silicon carbide, Ag, As, Au, Bi, [Cu](copper.md), Ga, Ge, In, [Ni](nickel.md), [Sn](tin.md), Sb, Se, Te, Zn
1. [Fuel](ps.md)
1. [Materials characteristics](matc.md)
1. **Gas:**
   1. [Helium](helium.md)
   1. [Hydrogen](hydrogen.md)
1. **Liquid:**
   1. …
1. **Metal:**
   1. [Aluminium](aluminium.md) (01570, AMg6)
   1. [Copper](copper.md)
   1. [Magnesium](magnesium.md)
   1. [Nickel](nickel.md) (Inconel)
   1. [Niobium](niobium.md)
   1. [Tin](tin.md)
   1. [Titanium](titanium.md) (VT23)
1. **Non‑metal:**
   1. [Asbotextolite](asc_lam.md)
   1. [Bakelite](bakelite.md)
   1. [Carbon fiber](cfrp.md)
   1. [Gravimol](gravimol.md)
   1. [MLI](mli.md)
   1. [Nitron](acryl_fiber.md)
   1. [Polyformaldehyde](polyoxymethylene.md)

【**Table.** Manufacturers】

| | |
|:--|:--|
|**AE**|…|
|**AU**|…|
|**CA**|・[Canadensys](canadensys.md) — mechanisms, structures<br> ・[Macfab](macfab.md) —  housings & covers for mission-critical electronics, separation systems, structures machined from magnesium & composite honeycomb panels<br> ・[UTIAS](utias.md) — Research for space mechatronics, robotics, microsats, fluid dynamics|
|**CN**|…|
|**EU**|…|
|**IL**|…|
|**IN**|…|
|**JP**|・[Mitsubishi Elecric](mitsubishi.md) — structural panels<br> ・[Tamagawa Seiki](tamagawa_seiki.md) — servo components|
|**KR**|…|
|**RU**|・SGM of SC manufactures everyone who manufacture SC ([VNIIEM](vniiem.md), [ISS](iss_r.md), [LAV](lav.md), etc.)<br> ・[NIICOM](niicom.md) — rotary devices, drives;<br> ・[RSC AC](rsc_ac.md) — chemicals, materials, coatings|
|**SA**|…|
|**SG**|…|
|**US**|…|
|**VN**|…|



### Onboard equipment
**Onboard equipment (OE)** *(ru. Бортовая аппаратура, БА)* — the general name of the equipment installed on the [SC](sc.md). Includes service equipment & payload.

<small>

|**Service equipment* (ru. Служебная аппаратура, СА) — the general name of the OE that ensures the functioning of the SC & the implementation of the target task by the SC|**Payload* (ru. Научная аппаратура, НА, Комплекс научной аппаратуры, КНА, Полезная нагрузка, ПН) — the general name of the OE used to implement the target task for a [SC](sc.md) & [SCS](sc.md)|
|:--|:--|
|**[Aimed antenna drive (AIAD)](devd.md)**·ПНА|—|
|—|**Analyzer of gas**·Газоанализатор|
|**[Automatic control unit (ACU)](eas.md)**·БАППТ|—|
|**[Autonomous navigation system (ANS)](ans.md)**·САН|—|
|**Beacon**·Маяк|**Beacon**·Маяк|
|**[Cable](cable.md)**·БКС|+|
|**[Camera (gen., 3D, diff. spectra)](cam.md)**·Kамера (обыч., 3D, разн. спектров)|**[Camera (gen., 3D, diff. spectra)](cam.md)**·Kамера (обыч., 3D, разн. спектров)|
|**[Comms (transmitter, receiver)](comms.md)**·Радио (приёмник, передатчик)|—|
|**[Control module (CM)](eas.md)**·БУ|—|
|**[Data storage (DS)](ds.md)**·ЗУ|—|
|**[Doppler](doppler.md)**·ИСР|**Radar**·Радар|
|—|**Radar for subsoil**·Радар подпочвенный|
|**[Ecology](ecology.md)**·Экология|+|
|**[Electronic components](elc.md)**·ЭКБ|—|
|**[Electric battery (EB)](eb.md)**·ХИТ|—|
|**[Electro-automatic system (EAS)](eas.md)**·СЭА|—|
|**[EMC](emc.md)**·ЭМС|+|
|**[Fuel](ps.md)**·Топливо|—|
|**[GNC](gnc.md)**·БКУ|—|
|**[Inertial unit (UI)](iu.md)**·Гироскоп|—|
|**[Landing gear (LAG)](lag.md)**·ПУC|—|
|**[Launch escape system (LES)](les.md)**·САСП|—|
|**[Life support (LS)](ls.md)**·СЖО|—|
|**[Manned spacecraft (MSC)](sc.md)**·ПКА|—|
|—|**Meter of energetic particles**·Измеритель энергетичных частиц|
|**[Meter of magnetic fields (magnetometer)](sensor.md)**·Магнитометр|**[Meter of magnetic fields (magnetometer)](sensor.md)**·Магнитометр|
|—|**Meter of plasma**·Измеритель плазмы|
|—|**Meter of seismic activity (seismometer)**·Сейсмометр|
|—|**Meter of spectra (spectrometer: IR, UV, Fourier, etc.)**·Спектрометр|
|—|**Meter of stellar wind**·Измеритель звёздного ветра|
|**[Nuclear reactor (NR)](nr.md)**·ЯР|—|
|**[Onboard computer (OBC)](obc.md)**·ЦВМ|—|
|**[Patent](patent.md)**·Патент|+|
|**[Propulsion system (PS)](ps.md)**·ДУ|—|
|**[Reaction wheel (RW)](iu.md)**·ДМ|—|
|—|**Reflector**·Отражатель|
|**[Robot](robot.md)**·Робот|—|
|**[Rotor](iu.md)**·Ротор|—|
|**[Rover](robot.md)**·Ровер|—|
|**[RTG, RHU](rtg.md)**·РИТЭГ, РИТ|—|
|**[Sensor (general)](sensor.md)**·Датчик (общий)|**[Sensor of dust/gas/temp./wind](sensor.md)**·Датчик пыли/газа/темп./ветра|
|**[Structures, gears, materials (SGM)](sc.md)**·KММ|+|
|**[Software](soft.md)**·ПО|+|
|**[Soil sample system (SSS)](sss.md)**·ГЗУ|**[Soil collector](sss.md)**·Грунтозаборник|
|**[Solar panel (SP)](sp.md)**·БС|—|
|**[Solar panels orientation system (SPOS)](devd.md)**·СОСБ|—|
|**[Spacecraft power system (SPS)](sps.md)**·СЭС|—|
|**[Star tracker](sensor.md)**·Звёздный датчик|—|
|**[Sun sensor](sensor.md)**·Солнечный датчик|—|
|**[Systems engineering (SE)](se.md)**·СЭ|+|
|**[Telemetry system (TMS)](tms.md)**·ТМС|+|
|**[Thermal control system (TCS)](tcs.md)**·СОТР|+|
|**[Timeline](timeline.md)**·Циклограмма|+|
|**[Wind turbine (WT)](wt.md)**·Ветрогенератор|—|

</small>

In common any onboard equipment can be basically described using the following table.

|**Characteristics**|**Value**|
|:--|:--|
|Composition| 【TBD: single unit, wires, number of units, pressurization level, etc.】 |
|Consumption, W| 【TBD: typical, basic timeline, etc.】 |
|Dimensions, ㎜| 【TBD: general dimensions, not volume】 |
|Interfaces| 【TBD: connectors, mounting points, orientation】 |
|Lifetime/Resource, h(y)| 【TBD: lifetime for total lifetime, resource for active state】 |
|Mass, ㎏| 【TBD: mass of the unit(s) in space】 |
|[Overload](vibration.md), Grms| 【TBD: acceptable loads & their direction】 |
|Rad.resist, ㏉(㎭)| 【TBD: total ionising dose】 |
|Reliability per lifetime| 【TBD: a calculated/proven reliability】 |
|Thermal range, ℃| 【TBD: for an active condition & for transportation】 |
|TRL| 【TBD: current TRL】 |
|Voltage, V| 【TBD: nominal, acceptable range for steady work & transition periods】 |
|**【Specific】**|~ ~ ~ ~ ~ |
|Specific req. #1| |
|Specific req. #…| |
| | 【TBD: photo, render, scheme, sketch, etc.】 |

【**Table.** Manufacturers】

| | |
|:--|:--|
|**AE**|…|
|**AU**|…|
|**CA**|・[MDA](mda.md)<br> ・[Thoth Tech.](thoth_tech.md) — cameras, spectrometers|
|**CN**|…|
|**EU**|…|
|**IL**|…|
|**IN**|…|
|**JP**|…|
|**KR**|…|
|**RU**|・[IKI RAS](iki_ras.md) — payload|
|**SA**|…|
|**SG**|…|
|**US**|…|
|**VN**|…|



### Var. — CubeSat
> <small>**Кубсат** — русскоязычный термин. **Cubesat** — англоязычный эквивалент.</small>

**Кубсат, CubeSat**, также **вошь**, **горох** — формат малых (сверхмалых) искусственных спутников, имеющих объём 1 литр и массу не более 1.33 ㎏ или несколько (кратно) более. Имеется ещё более малый формат покетсат (буквально карманный) в несколько сотен или десятков грамм и несколько сантиметров.

1. <https://www.nanosats.eu/cubesat>
1. <http://www.cubesat.org>
1. <https://en.wikipedia.org/wiki/CubeSat>
1. <http://directory.eoportal.org/web/eoportal/satellite-missions/c-missions/cubesat-concept> — [archived ❐](f/scs/cubesat_concept_seor.pdf) 2016.05.06
1. <http://www.cubesatshop.com> — магазин оборудования для кубсатов.

![](f/scs/cubesat-ncube2.webp)

**Кубсат** — спутник размером 10×10×10 ㎝ (1U) и массой не более 1.33 ㎏. Допускается объединение 2 или 3 стандартных кубов в составе одного спутника (обозначаются 2U и 3U и имеют размер 10×10×20 или 10×10×30 ㎝).

Кубсаты обычно используют шасси‑каркас спецификации CubeSat и покупные стандартные комплектующие — COTS‑электронику и прочие узлы. Спецификации CubeSat были разработаны в 1999 году Калифорнийским политехническим и Стэнфордским университетами, чтобы упростить создание сверхмалых спутников. Формат кубсат сделал широким распространение университетских спутников; для унификации и координации существует всемирная межуниверситетская программа запуска кубсатов.

Кубсаты имеют стоимость выведения до нескольких десятков тысяч долларов, а покетсаты — до нескольких тысяч долларов.

Кубсаты выводятся, как правило, сразу по несколько единиц либо посредством [ракет‑носителей](lv.md), либо с борта космических кораблей и орбитальных станций. Несколько компаний предоставляет услуги по выводу кубсатов на орбиту, в частности ISC Kosmotras и Eurokot. Для размещения на РН или КА, запуска и разведения кубсатов разработаны многоместные контейнеры‑платформы, в т.ч. с револьверным выводом на орбиту. Также для вывода кубсатов разрабатываются сверхмалые РН — наноносители.

Термином 「CubeSat」 обозначаются наноспутники (Nano‑satellite), удовлетворяющие спецификациям стандарта, созданному под руководством профессора Bob Twiggs (факультет аэронавтики и астронавтики, Стэнфорд), Спутники имеют размер 10×10×10 ㎝ и запускаются при помощи Poly‑PicoSatellite Orbital Deployer (P‑POD). Стандарт допускает объединение 2 или 3 стандартных кубов в составе одного спутника (обозначаются 2U и 3U и имеют размер 10×10×20 или 10×10×30 ㎝). Один P‑POD имеет размеры, достаточные для запуска трёх спутников 10×10×10 ㎝ или меньшего количества, общим размером не более 3U.

На 2004 год, спутники в формате CubeSats могли быть изготовлены и запущены на околоземную орбиту за $ 65 000 ‑ 80 000. На 2012 год типичная стоимость запуска CubeSat оценивалась в $ 40 000 (иногда доходя также до 80 000, хотя NASA заявило и о возможности запуска за 20 000). Несколько покетсатов могут компоноваться и запускаться в контейнерном месте и по цене одного кубсата, т.е. за несколько тысяч долларов каждый. Столь низкая стоимость и унификация платформ и комплектующих позволяет разрабатывать и запускать кубсаты университетам и даже школам, небольшим частным компаниям и любительским объединениям, а покетсаты — частным лицам.

Большинство CubeSat имеют один или два научных прибора, некоторые имеют небольшие выдвижные антенны и поверхностные или распахивающиеся солнечные батареи.

【**Table.** Manufacturers】

| | |
|:--|:--|
|**AU**|…|
|**CA**|・[AlbertaSat](albertasat.md)<br> ・[Kepler Communications](kepler_comms.md)<br> ・[DSS](dsslabs.md)<br> ・[SFL](utias.md)|
|**CN**|…|
|**EU**|…|
|**IN**|…|
|**IL**|…|
|**JP**|…|
|**KR**|…|
|**RU**|・[AvantSpace](avantspace.md)<br> ・[Спутникс](sputnix.md)|
|**SA**|…|
|**SG**|…|
|**US**|…|
|**AE**|…|
|**VN**|…|



### Var. — FSS (Comms spacecraft)
> <small>**Fixed-satellite service (FSS)** — EN term. **Фиксированная спутниковая служба (ФСС)** — RU analogue.</small>  
> <small>**Communication spacecraft** — EN term. **Космический аппарат связи** — RU analogue.</small>

1. <https://en.wikipedia.org/wiki/Fixed-satellite_service>



### Var. — HTS (Comms spacecraft)
> <small>**High-throughput satellite (HTS)** — EN term. **Высокопроизводительный спутник (ВПС)** — literal RU translation.</small>  
> <small>**Communication spacecraft** — EN term. **Космический аппарат связи** — RU analogue.</small>

**High-throughput satellite (HTS)** is a communications satellite that provides more throughput than a classic FSS satellite (at least twice, though usually by a factor of 20 or more) for the same amount of allocated orbital spectrum, thus significantly reducing cost-per-bit. ViaSat-1 & EchoStar XVII (also known as Jupiter-1) do provide more than 100 Gbit/s of capacity, which is more than 100 times the capacity offered by a conventional FSS satellite. When it was launched in October 2011 ViaSat-1 had more capacity (140 Gbit/s) than all other commercial communications satellites over North America combined.

1. <https://en.wikipedia.org/wiki/High-throughput_satellite>

The significant increase in capacity is achieved by a high level frequency re-use & spot beam technology which enables frequency re-use across multiple narrowly focused spot beams (usually in the order of hundreds of kilometers), as in cellular networks, which both are defining technical features of high-throughput satellites. By contrast traditional satellite technology utilizes a broad single beam (usually in the order of thousands of kilometers) to cover wide regions or even entire continents. In addition to a large amount of bandwidth capacity HTS are defined by the fact that they often, but not solely, target the consumer market. In the last 10 years, the majority of high-throughput satellites operated in the Ka band, however this is not a defining criterion, & at the beginning of 2017 there was at least 10 Ku band HTS satellites projects, of which 3 were already launched & 7 were in construction.

Initially, HTS systems used satellites in the same geosynchronous orbit (at an altitude of 35 786 km) as satellite TV craft (with satellites such as KA-SAT, Yahsat 1A & Astra 2E sharing TV & HTS functionality) but the propagation delay for a round-trip internet protocol transmission via a geosynchronous satellite can exceed 550 ms which is detrimental to many digital connectivity applications, such as automated stock trades, hardcore gaming & Skype video chats. & the focus for HTS is increasingly shifting to the lower Medium Earth orbit (MEO) & Low Earth orbit (LEO), with altitudes as low as 600 km & delays as short as 40 ms. Also, the lower path losses of MEO & LEO orbits reduces ground station & satellite power requirements & costs, & so vastly increased throughput & global coverage is achieved by using constellations of many smaller, cheaper high-throughput satellites. SES's O3b constellation was the 1st MEO high-throughput satellite system, launched in 2013, & by 2018 more than 18 000 new LEO satellites had been proposed to launch by 2025.

Despite the higher costs associated with spot beam technology, the overall cost per circuit is considerably lower as compared to shaped beam technology. While Ku band FSS bandwidth can cost well over $ 100 million per gigabit per second in space, HTS like ViaSat-1 can supply a gigabit of throughput in space for less than $ 3 million. While a reduced cost per bit is often cited as a substantial advantage of high-throughput satellites, the lowest cost per bit is not always the main driver behind the design of an HTS system, depending on the industry it will be serving.

HTS are primarily deployed to provide broadband Internet access service (point-to-point) to regions unserved or underserved by terrestrial technologies where they can deliver services comparable to terrestrial services in terms of pricing & bandwidth. While many current HTS platforms were designed to serve the consumer broadband market, some are also offering services to government & enterprise markets, as well as to terrestrial cellular network operators who face growing demand for broadband backhaul to rural cell sites. For cellular backhaul, the reduced cost per bit of many HTS platforms creates a significantly more favorable economic model for wireless operators to use satellite for cellular voice & data backhaul. Some HTS platforms are designed primarily for the enterprise, telecom or maritime sectors. HTS can furthermore support point-to-multipoint applications & even broadcast services such as DTH distribution to relatively small geographic areas served by a single spot beam.

A fundamental difference between HTS satellites is the fact that certain HTS are linked to ground infrastructure through a feeder link using a regional spot beam dictating the location of possible teleports while other HTS satellites allow the use of any spot beam for the location of the teleports. In the latter case, the teleports can be set up in a wider area as their spotbeams' footprints cover entire continents & regions like it is the case for traditional satellites .

Industry analysts at Northern Sky Research believe that high-throughput satellites will supply at least 1.34 TB/s of capacity by 2020 & thus will be a driving power for the global satellite backhaul market which is expected to triple in value – jumping from the 2012 annual revenue of about US $ 800 million to $ 2.3 billion by 2021.

【**Picture.** KA-SAT coverage over Europe showing frequency reuse by different colors】  
![](f/scs/hts_spot_beams_coverage.webp)

~ ~ ~ ~ ~ 

**(RU) HTS**

**HTS (англ. high-throughput satellite)** — класс спутников связи с высокой пропускной способностью, которые обеспечивают увеличение общей пропускной способности по сравнению с традиционными спутниками от двух до 20 и более раз при том же самом орбитальном спектре частот.

Большая пропускная способность позволяет уменьшать стоимость использования спутникового канала. Наиболее известные спутники этой категории — ViaSat-1 и EchoStar XVII (известный также как Jupiter-1), они обеспечивают общую скорость передачи данных более 100 Гбит/с, что более чем в 100 раз превышает ёмкость традиционных спутниковых каналов. Спутник ViaSat-1 был запущен в октябре 2011 года и имел  общую скорость передачи информации 140 Гбит/с, что больше, чем у всех остальных коммерческих спутников связи в Северной Америке.

Принципиальная разница между HTS и традиционными спутниками состоит в наличии у первых множества лучей, что позволяет повторно использовать их частотный ресурс.

Основным элементом таких спутников является антенная система. Её параметры определяют потенциальные возможности всей системы. Выбор необходимых параметров антенной системы, рабочей зоны, ориентации лучей и др. влияет на окупаемость спутниковой системы. В настоящее время используются многолучевые зеркальные антенны, выполненные по типу 「один рупор — один луч」. Возможна также реализация бортовых многолучевых антенн с кластерными облучателями, так как это решение, несмотря на то, что проигрывает по антенным техническим параметрам, однако позволяет существенно сократить массу антенной системы HTS за счёт сокращения числа облучателей антенн в её составе.

**Запуски.** Запуск спутников класса HTS производится с 2004 года, среди них:

1. Anik F2 (июль 2004)
1. Thaicom 4 (IPSTAR) (август 2005)
1. Spaceway-3 (август 2007)
1. WINDS (февраль 2008)
1. KA-SAT (декабрь 2010)
1. Yahsat 1A (апрель 2011)
1. ViaSat-1 (октябрь 2011)
1. Yahsat 1B (апрель 2012)
1. EchoStar XVII (июль 2012)
1. HYLAS 2 (июль 2012)
1. Astra 2E (сентябрь 2013)
1. Inmarsat Global Xpress constellation (2013-2015)
1. Sky Muster (NBN Co-1A) (октябрь 2015)
1. Badr-7 for TRIO Connect (ноябрь 2015)
1. Intelsat 29e (2016)
1. Intelsat 33e (2016)
1. Intelsat 32e (2017)
1. Intelsat 37e (2017)
1. SES-14 (Q4, 2017)
1. Eutelsat 172B (2017)
1. Fibersat-1 (Q4, 2018)
1. На 2019 год намечен запуск спутника ViaSat-3, ориентированного на американский рынок. Антенная система этого спутника будет формировать 5 000 узких лучей. Заявленная пропускная способность составит около 1 Тбит/с. В России проводится работа над HTS 「Энергия-100」. Его антенная система будет формировать 1 500 лучей.



### Var. — Manned spacecraft
> <small>**Пилотируемый космический аппарат (ПКА)** — русскоязычный термин. **Manned spacecraft** — англоязычный эквивалент.</small>

**Пилотируемый космический аппарат (ПКА)** — космический аппарат, снабжённый [системами жизнеобеспечения](ls.md) и управления полётом, и предназначенный для жизни, работы или иной деятельности одного или нескольких человек в космическом пространстве. Встречаются также наименования *пилотируемый космический корабль (ПКК)* и *пилотируемая орбитальная станция (ПОС)*.

Управление ПКА может осуществляться экипажем, операторами наземных [ЦУП](sc.md), системами автоматики или комбинацией этих методов.

В состав ПКА, в дополнение к [обычным системам](draw.md) [КА](sc.md), входят:

1. [Система аварийного спасения](les.md) (САС)
1. [Система жизнеобеспечения](ls.md) (СЖО)
1. **Спускаемый аппарат** или **Космоплан** для возврата экипажа на Землю

【**Table.** Manufacturers】

| | |
|:--|:--|
|**AU**|…|
|**CA**|…|
|**CN**|・[CNSA](cnsa.md)|
|**EU**|・[ESA](esa.md)|
|**IN**|…|
|**IL**|…|
|**JP**|・[JAXA](jaxa.md)|
|**KR**|…|
|**RU**|・[РКК Энергия](ркк_энергия.md)|
|**SA**|…|
|**SG**|…|
|**US**|・[Blue Origin](blue_origin.md)<br> ・[Boeing](boeing.md)<br> ・[Lockheed Martin](lockheed_martin.md)<br> ・[SpaceX](spacex.md) — [Dragon](dragon.md)|
|**AE**|…|
|**VN**|…|



### Var. — Satellite
> <small>**Спутник** — русскоязычный термин. **Satellite** — англоязычный эквивалент.</small>

**Satellite mass classification**  
Классификация спутников по массе

1. ≥ 1 000 ㎏ — Large satellites — Тяжёлые спутники
1. 500 ‑ 1 000 ㎏ — Medium satellites — Средние спутники
1. ≤ 500 ㎏ — Small satellites — Малые спутники
   1. 100 ‑ 500 ㎏ — Minisatellites — Миниспутники
   1. 10 ‑ 100 ㎏ — Microsatellites — Микроспутники
   1. 1 ‑ 10 ㎏ — Nanosatellites — Наноспутники (all Cube‑, Pocket‑, Tube‑, Sun‑, Thin‑Sats & non‑standard picosats)
   1. 0.1 ‑ 1 ㎏ — Picosatellites — Пикоспутники
   1. 0.01 ‑ 0.1 ㎏ — Femtosatellites — Фемтоспутники
   1. 0.001 ‑ 0.01 ㎏ — Attosatellites — Аттоспутники
   1. 0.0001 ‑ 0.001 ㎏ — Zeptosatellites — Зептоспутники
1. CubeSat parametres — Параметры кубсатов:
   1. 0.25 ‑ 27U
   1. 0.2 ‑ 40 ㎏



## ➂ User segment
…



## ➃ (RU) КК и СЧ КК
Стандартная схема деления КК:

**Космический комплекс** (КК)  
░║  
░╟ **Наземный комплекс управления** (НКУ)  
░║░╟ Баллистический центр (БЦ)  
░║░╟ Наземные станции слежения (НСС)  
░║░╟ Наземные станции управления (НСУ)  
░║░╟ Сектор главного конструктора (СГК)  
░║░╟ Средства связи и передачи данных (ССПД)  
░║░╙ Центр управления полётом (ЦУП)  
░║  
░╟ **Наземный научный комплекс** (ННК)  
░║░╟ Лаборатория  
░║░╟ Комплекс долговременного хранения и обработки научных данных (КДХ)  
░║░╟ Комплекс планирования работы научной аппаратуры (КПР)  
░║░╟ Комплекс приёма и анализа научных и телеметрических данных (КПНД)  
░║░╙ Комплекс связи и коммуникации (КСК)  
░║  
░╙ **Ракетно‑космический комплекс** (РКК)  
░░╟ Комплекс разгонного блока (КРБ)  
░░╟ Комплекс ракеты‑носителя (ТК РН)  
░░╟ Комплес средств измерений, сбора и обработки (КСИСО)  
░░╟ Технический комплекс космического аппарата (ТК КА)  
░░╟ Технический комплекс космической головной части (ТК КГЧ)  
░░╟ Технический комплекс ракеты космического назначения (ТК РКН)  
░░╙ Ракета космического назначения (РКН)  
░░░╟ [Ракета‑носитель](lv.md) (РН)  
░░░╙ Космическая головная часть (КГЧ)  
░░░░╟ [Головной обтекатель](lv.md) (ГО)  
░░░░╟ Разгонный блок (РБ)  
░░░░╟ [Переходной отсек](lv.md) (ПхО)  
░░░░╙ **[Космический аппарат](sc.md)** (КА)  
░░░░░╟ Адаптер с устройством отделения  
░░░░░╟ Бортовой комплекс управления (БКУ)  
░░░░░║░╟ Бортовая кабельная сеть (БКС)  
░░░░░║░╟ Высотомер  
░░░░░║░╟ Гироскоп  
░░░░░║░╟ Датчик звёздный (ЗД)  
░░░░░║░╟ Датчик контроля электризации  
░░░░░║░╟ Датчик относительной ориентации (GPS, GLONASS)  
░░░░░║░╟ Датчик солнечный (СД)  
░░░░░║░╟ Двигатель‑маховик (ДМ)  
░░░░░║░╟ Запоминающее устройство (ЗУ)  
░░░░░║░╟ Привод антенны  
░░░░░║░╟ Привод солнечных панелей (ПСП)  
░░░░░║░╟ Программное обеспечение (ПО)  
░░░░░║░╙ Цифровая вычислительная машина (ЦВМ)  
░░░░░╟ Бортовая кабельная сеть (БКС)  
░░░░░╟ Двигательная установка (ДУ)  
░░░░░║░╟ Агрегаты  
░░░░░║░╟ Баки  
░░░░░║░╟ Двигатели  
░░░░░║░╟ Защита, дренаж  
░░░░░║░╟ Конструкция  
░░░░░║░╟ Нагреватели, тепловые датчики  
░░░░░║░╙ Трубопроводы  
░░░░░╟ Комплекс научной аппаратуры (КНА)  
░░░░░╟ Конструкция и механизмы (КММ)  
░░░░░╟ Радиокомплекс (РК)  
░░░░░║░╟ Антенно‑фидерная система (АФС)  
░░░░░║░╟ Бортовая кабельная сеть (БКС)  
░░░░░║░╙ Приёмо‑передатчик  
░░░░░╟ Система электроавтоматики (СЭА)  
░░░░░╟ Система электроснабжения (СЭС)  
░░░░░║░╟ Аккумуляторная батарея (АБ)  
░░░░░║░╟ Бортовая кабельная сеть (БКС)  
░░░░░║░╟ Комплекс автоматики и стабилизации  
░░░░░║░╙ Солнечные панели  
░░░░░╟ Средства обеспечения теплового режима (СОТР)  
░░░░░║░╟ Нагреватели, тепловые датчики  
░░░░░║░╟ Покрытия  
░░░░░║░╟ Радиаторы, коллекторы, тепловые трубы  
░░░░░║░╙ Экранно‑вакуумная теплоизоляция (ЭВТИ)  
░░░░░╙ Телеметрическая система (ТМС)



### БЦ (Баллистический центр)
> <small>**Баллистический центр (БЦ)** — русскоязычный термин. **Ballistic analysis center (BAC)** — англоязычный эквивалент.</small>

**Баллистический центр (БЦ)** — комплекс программно‑аппаратных средств для определения орбит КА и расчётов траектории его движения, необходимых манёвров и для прочего баллистико‑навигационного обеспечения. Часто входит в состав [ЦУП](sc.md).

Известные БЦ РФ:

1. БЦ [ИПМ Келдыша](keldysh_ipm.md);
1. БЦ [ЦНИИмаш](tsniimash.md).



### КИК (Командно‑измерительный комплекс)
> <small>**Командно‑измерительный комплекс** — русскоязычный термин, не имеющий аналога в английском языке. **Сommand & measurement complex (CAMC)** — дословный перевод с русского на английский.</small>

**Командно‑измерительный комплекс (КИК)** — совокупность Земных средств и служб, с помощью которых осуществляется управление полётом [космических аппаратов](sc.md), [ракет‑носителей](lv.md) и космических объектов.

В состав КИК входят командно‑измерительные пункты, расположенные на суше, плавучие (корабельные) и самолётные измерительные пункты. Количество и местоположение стационарных командно‑измерительных пунктов определяются задачами обеспечения непрерывности управления различными КА и требованиями дублирования и резервирования. Состав и размещение стационарных и подвижных средств КИК, используемых для управления конкретными типами КА, определяются их орбитой, типом установленной на борту аппаратуры и программой полёта.

Основными средствами управления в КИК являются: аппаратура траекторных измерений (для определения параметров орбиты); телеметрическая аппаратура (для контроля и диагностики состояния КА); командно‑программная аппаратура (для выдачи на борт управляющих команд, программ и контроля их исполнения). В состав КИК входят также: вычислительные комплексы (ЭВМ), аппаратура автоматического ввода данных траекторных измерений в ЭВМ, системы автоматической обработки результатов телеизмерений, аппаратура приёма и передачи телевизионной информации, телефонной, телеграфной связи с космонавтами, служба единого точного времени, каналы и средства космической и наземной связи, средства контроля и отображения хода полёта, системы моделирования процессов управления и др.

Информация, поступающая с КА, обрабатывается координационно‑вычислительными центрами, которые выдают необходимые данные в Центр управления полётом).



### КИС (Командно‑измерительная система)
> <small>**Командно‑измерительная система (КИС)** — русскоязычный термин, не имеющий аналога в английском языке. **Command measurement system (COMES)** — дословный перевод с русского на английский.</small>

**Командно‑измерительная система (КИС)** — радиотехническое средство [НКУ](sc.md) и [НАКУ](sc.md) в совокупности с бортовой аппаратурой [КА](sc.md) или [РБ](lv.md), предназначенное для измерения параметров движения КА и РБ, приёма и передачи различных видов информации, формирования и передачи на КА и РБ команд и программ управления, стандартных частот и сигналов [времени](time.md) для синхронизации работы [GNC](gnc.md).

Известные КИС:

1. <mark>TBD</mark>

Согласно типовой [схеме деления](draw.md) КИС не входит в состав КК (КС), однако функционально входит в состав [НКУ](sc.md).



### КСИСО (Комплекс средств измерения, сбора и обработки информации)
> <small>**Комплекс средств измерения, сбора и обработки информации (КСИСО)** — русскоязычный термин, не имеющий аналога в английском языке. **System of measuring instruments, data acquisition & processing (SMIDAP)** — дословный перевод с русского на английский.</small>

**Комплекс средств измерения, сбора и обработки информации (КСИСО)** — совокупность сооружений, взаимосвязанных между собой технических средств и программного обеспечения [НКУ](sc.md) и [НАКУ](sc.md) [космическими аппаратами](sc.md) и измерений, предназначенных для автоматизированного контроля за функционированием [РКН](lv.md) в процессе предстартовой подготовки и на участке выведения, обеспечивающих обработку, документирование и распределение результатов измерений между потребителями.

Привлекается на этапе выведения [КА](sc.md) на орбиту перелёта и осуществляет контроль за выполнением [полётных заданий](fp.md) [LV](lv.md) и [РБ](lv.md) совместно с [НИК РБ](lm_sys.md). После выведения КА на орбиту перелёта функции контроля передаются в [НКУ](sc.md).



### НАКУ (Наземный автоматизированный комплекс управления)
> <small>**Наземный автоматизированный комплекс управления (НАКУ)** — русскоязычный термин. **Ground automated control complex** — англоязычный эквивалент.</small>

**Наземный автоматизированный комплекс управления (НАКУ)** — по [ГОСТ 53802](гост_53802.md), п. 30‑32 — совокупность необходимой инфраструктуры, технических систем, средств из состава командно‑измерительных и измерительных пунктов, центров и пунктов управления орбитальными средствами, центров обработки измерительной информации, предназначенных для формирования наземных комплексов, обеспечивающих реализацию автоматизированных процессов контроля параметров полёта [изделий](unit.md) ракетно‑космической техники, состояния бортовой аппаратуры и управления их функционированием.

Согласно типовой [схеме деления](draw.md) НАКУ является самостоятельной структурой и не входит в состав [КК (КС)](sc.md).



### НИП (Наземный измерительный пункт)
> <small>**Наземный измерительный пункт (НИП)** — русскоязычный термин. **Ground telemetry station** — англоязычный эквивалент.</small>

**Наземный измерительный пункт (НИП)**, также **Научно‑измерительный пункт** (Отдельный командно‑измерительный комплекс) — пункт контроля и управления [космическими аппаратами](sc.md).

Разделяют на:

1. наземные измерительные пункты (НИП);
1. плавучие измерительные пункты;
1. самолётные измерительные пункты (СИП);
1. отдельные измерительные пункты (ОИП).

Обычно функционально входит в состав [НКУ](sc.md).



### НК (Наземный комплекс)
> <small>**Наземный комплекс (НК)** — русскоязычный термин, не имеющий аналога в английском языке. **Ground‑based complex** — дословный перевод с русского на английский.</small>

**Наземный комплекс** ─ общее негостированное название всех наземных элементов, предназначенных для управления и обмена информацией с космическим аппаратом.

Может включать в свой состав:

1. **Наземный комплекс управления (НКУ)**:
   1. баллистические центры;
   1. наземные станции слежения;
   1. наземные станции управления
   1. телекоммуникации и средства связи;
   1. сектор главного конструктора;
   1. средства обработки и хранения информации;
   1. прочие средства управления и обеспечения управления.
   2. **Наземный научный комплекс (ННК)**:
   1. аппаратно‑программные средства приёма, передачи, обработки и хранения информации.



### НКПОР (Наземный комплекс приёма, обработки и распределения информации)
> <small>**Наземный комплекс приёма, обработки и распределения информации (НКПОР)** — русскоязычный термин, не имеющий аналога в английском языке. **Ground-based complex for data receiving, processing & distribution (GCDRPD)** — дословный перевод с русского на английский.</small>

**Наземный комплекс приёма, обработки и распределения информации (НКПОР)** — совокупность взаимосвязанных технических средств с программным обеспечением, расположенных на [Земле](earth.md) и предназначенных для обеспечения заказчика и его потребителей целевой информацией, полученной на основе космических данных.

Согласно типовой [схеме деления](draw.md) НКПОР не входит никуда.



### НКУ (Наземный комплекс управления)
> <small>**Наземный комплекс управления (НКУ)** — русскоязычный термин. **Ground segment (GS)** — примерный англоязычный эквивалент.</small>

**Наземный комплекс управления (НКУ)** — совокупность взаимосвязанных технических средств с информационным и математическим обеспечением, сооружений, [центра управления полётом](sc.md) и отдельных командно‑измерительных комплексов, предназначенных для автоматизированного управления КА на всех этапах полёта КА после его отделения от [разгонного блока](lv.md).

**НКУ** обычно включает в свой [состав](draw.md):

1. [Баллистический центр](sc.md) (БЦ)
1. Наземная станция (НС)
1. [Сектор главного конструктора](cd_segm.md) (СГК)
1. [Средства связи и передачи данных](mcntd.md) (ССПД)
1. [Центр управления полётом](sc.md) (ЦУП)

На этапе выведения на орбиту перелёта НКУ не привлекается. На этом этапе полёта контроль за выполнением [полётных заданий](fp.md) [LV](lv.md) и [РБ](lv.md) осуществляется наземными средствами [КСИСО](sc.md) и [НИК](lm_sys.md) РБ. Также может включать в свой состав [КИС](sc.md) и [НИП](sc.md).



### ННК (Наземный научный комплекс)
> <small>**Наземный научный комплекс (ННК)** — русскоязычный термин. **[User segment (US)](us.md)** или **[Payload data ground segment (PDGS)](pdgs.md)** — примерный англоязычный эквивалент.</small>

**Наземный научный комплекс (ННК)** — совокупность [аппаратно‑программных средства (АПС)](hns.md) предназначенных для осуществления полного цикла сбора, обработки, анализа и долговременного хранения всех типов данных (калибровочной, научной, вспомогательной и служебной информации), принимаемых с борта [КА](sc.md), подготовки программ проведения наблюдений, а также распространения научной информации среди участников проекта.

По факту ННК представляет собой группу серверов со специализированным ПО и каналы связи.

| | |
|:--|:--|
|**AE**|…|
|**AU**|…|
|**CA**|…|
|**CN**|…|
|**EU**|…|
|**IL**|…|
|**IN**|…|
|**JP**|・[NEC](nec.md)|
|**Korea South**|…|
|**RU**|…|
|**Saudi Arabia**|…|
|**SG**|…|
|**US**|…|
|**VN**|…|



### НС (Наземная станция)
> <small>**Наземная станция (НС)** — русскоязычный термин. **Ground station (GS) / Earth station (ES) / Earth terminal (ET)** — англоязычные эквиваленты.</small>

**Наземная станция (НС)** — земная [радиостанция](comms.md), предназначенная для связи с [космическим аппаратом](sc.md), находящимся вне Земли, или для приёма‑передачи радиоволн от (к) астрономическим источникам радиоизлучения. НС входит в состав НКУ.

**Разновидности:**

1. **Наземная станция приёма информации (НСПИ)** — наземная станция, предназначенная для приёма информации от космического аппарата.  **Ground station for information reception** — дословный перевод с русского на английский.
1. **Наземная станция приёма научной информации (НСПНИ)** — наземная станция, предназначенная для приёма научной информации от космического аппарата. **Ground station for scientific information reception** — дословный перевод с русского на английский.
1. **Наземная станция управления (НСУ)** — наземная станция, предназначенная для управления КА. **Ground control station** — англоязычный эквивалент.

**Производители, операторы:**

| | |
|:--|:--|
|**AU**|…|
|**CA**|・[MDA](mda.md)|
|**CN**|…|
|**EU**|…|
|**IL**|…|
|**IN**|…|
|**JP**|・[Infostellar](infostellar.md) — space communication infrastructure, 10+ UHF/S/X antennas<br> ・[Kratos IS](kratos.md) — R&D ground antennas, optimizing/managing satelllites, signals<br> ・[Mitsubishi Elecric](mitsubishi.md) — ground control stations for satellite tracking, & optical/radio telescopes for astronomical observation, antennas, transmitters & receivers<br> ・[RESTEC](restec.md)|
|**KR**|…|
|**RU**|・[ОКБ МЭИ](okbmei.md)|
|**SA**|…|
|**SG**|…|
|**US**|…|
|**AE**|…|
|**VN**|…|

|**Изображение**|**Описание**|
|:--|:--|
|[![](f/gs/cdsn_pic1t.webp)](f/gs/cdsn_pic1.webp)|**[Chinese Deep Space Network](cdsn.md)**<br> (Китай)|
|[![](f/gs/estrack_pic1t.webp)](f/gs/estrack_pic1.webp)|**[ESTRACK](estrack.md)**<br> (Европа)|
|[![](f/gs/idsn_pic1t.webp)](f/gs/idsn_pic1.webp)|**[Indian Deep Space Network](idsn.md)**<br> (Индия)|
|[![](f/gs/dsn_antennat2.webp)](f/gs/dsn_antenna.webp)|**[NASA Deep Space Network](dsn.md)**<br> (США)|
|[![](f/gs/ssc_ggsnt2.webp)](f/gs/ssc_ggsn.webp)|**[SSC’s Global Ground Station Network](ssc_ggsn.md)**<br> (Европа, Швеция)|
|[![](f/gs/udsc_pic1t.webp)](f/gs/udsc_pic1.webp)|**[Usuda Deep Space Center](udsc.md)**<br> (Япония)|
|[![](f/gs/map_world_ground_stationst2.webp)](f/gs/map_world_ground_stations.webp)|**Мировые НС**|
|[![](f/gs/map_world_ground_stations_nkudkat2.webp)](f/gs/map_world_ground_stations_nkudka.webp)|**НС в рамках НКУ‑ДКА**<br> (примерное расположение)|

【**Таблица.** Известные НС】

| |**Описание**|**Изобр.**|
|:--|:--|:--|
|**EU**|**Raisting Satellite Earth Station (Германия)**, [Maspalomas Station](maspalomas_station.md)|
|**RU**|**Калязинская радиоастрономическая обсерватория**<br> Владелец — [ОКБ МЭИ](okbmei.md).<br> ・Радиотелескоп ТНА‑1500 или РТ‑64: D = 64 м, F/0.37, полноповоротный параболический рефлектор, мин. раб. длина волны = 1 ㎝, M общая = 3 800 т, M зеркала = 800 т, вторичное зеркало D = 6 м.<br> ・Наблюдаемая часть небесной сферы: A = ± 300° H = 0 ‑ 90°. Класс наблюдений: B; Выделенные [полосы частот](comms.md) для наблюдений, ㎓.: 0.608 ‑ 0.614, 1.66 ‑ 1.67, 4.8 ‑ 4.99, 4.99 ‑ 5.0, 22.21 ‑ 22.50. Шумовая температура радиотелескопа, К: 80, 22, 22, 22, 65.|[![](f/gs/20081011_tna-1500_radio_telescope_in_kalyazin_wikipedia_rut.webp)](f/gs/20081011_tna-1500_radio_telescope_in_kalyazin_wikipedia_ru.webp)|
| |**Центр космической связи 「Медвежьи озёра」**<br> Владелец — [ОКБ МЭИ](okbmei.md).<br> ・Радиотелескоп ТНА‑1500 или РТ‑64: D = 64 м, F/0.37, полноповоротный параболический рефлектор, мин. раб. длина волны = 1 ㎝, M общая = 3 800 т, M зеркала = 800 т, вторичное зеркало D = 6 м. Собирающая площадь 1 500 m².<br> ・Работает с 1979 г. До 2010 г была только принимающей антенной, теперь она и передающая. Система облучения Грегори.|[![](f/gs/20160819_mosoblast_schyolkovo_district_radio_telescope_wikipedia_rut.webp)](f/gs/20160819_mosoblast_schyolkovo_district_radio_telescope_wikipedia_ru.webp)|
| |**Восточный центр дальней космической связи (Уссурийск)**<br> 44°00′57″ с.ш. 131°45′25″ в.д. <sup>[H](https://tools.wmflabs.org/geohack/geohack.php?:language=ru&pagename=%D0 %A0 %D0 %A2-70&params=44_0_57.90_N_131_45_25.13_E) [G](https://maps.google.com/maps?:ll=44.0160833,131.7569806&q=44.0160833,131.7569806&spn=0.03,0.03&t=h&hl=ru) [Я](https://yandex.ru/maps/?:ll=131.7569806,44.0160833&pt=131.7569806,44.0160833&spn=0.03,0.03&l=sat,skl) [O](https://www.openstreetmap.org/?:mlat=44.0160833&mlon=131.7569806&zoom=14)</sup><br> ・Радиотелескоп П‑2500 или РТ‑70.|[![](f/gs/20150412_ussuriisk_c538a_9da9691a_orig_livejournal_rut.webp)](f/gs/20150412_ussuriisk_c538a_9da9691a_orig_livejournal_ru.webp)|



## ➄ Docs & links
|**Sections & pages**|
|:--|
|**【[Spacecraft (SC)](sc.md)】**<br> [Cleanliness level](clean_lvl.md) ~~ [Communication SC](sc.md) ~~ [Cubesat](sc.md) ~~ [FSS](sc.md) ~~ [HTS](sc.md) ~~ [Interface](interface.md) ~~ [Manned SC](sc.md) ~~ [Satellite](sc.md) ~~ [Sub-item](sui.md) ~~ [Typical forms](sc.md)|
|**`Космический комплекс (КК):`**<br> [Выводимая масса](throw_weight.md) ~~ [ГО и ПхО](lv.md) ~~ [Класс чистоты](clean_lvl.md) ~~ [Контейнеры для транспортировки](ship_contain.md) ~~ [СЧ](sui.md)|
|**`Наземный комплекс управления (НКУ):`**<br> [БЦ](sc.md) ~~ [КИС](sc.md) ~~ [КСИСО](sc.md) ~~ [НИК](lm_sys.md) ~~ [НИП](sc.md) ~~ [НС](sc.md) ~~ [ПОЗ](fp.md) ~~ [СГК](cd_segm.md) ~~ [ССПД](mcntd.md) ~~ [ЦУП](sc.md)|
|**`Наземная станция (НС):`**<br> … <br><br> [CDSN](cdsn.md) ~~ [DSN](dsn.md) ~~ [ESTRACK](estrack.md) ~~ [IDSN](idsn.md) ~~ [SSC_GGSN](ssc_ggsn.md) ~~ [UDSC](udsc.md)|
|**`Наземный научный комплекс (ННК):`**<br> [АПС](hns.md)|

1. Docs:
   1. SCS:
      1. [ГОСТ 53802](гост_53802.md), п. 1‑7
      1. [РК‑11](const_rk.md), стр. 17
   1. НКУ: [ГОСТ 53802](гост_53802.md), п.30‑32
   1. БЦ: [ГОСТ 53802](гост_53802.md), п.43
   1. НКПОР: [ГОСТ 53802](гост_53802.md), п.50
   1. НАКУ: [ГОСТ 53802](гост_53802.md), п. 30‑32
   1. КСИСО: [ГОСТ 53802](гост_53802.md), п. 51
   1. КИК: <https://ru.wikipedia.org/wiki/Категория:Командно‑измерительный_комплекс>
   1. ЦУП: [ГОСТ 53802](гост_53802.md), п. 41‑42
1. SCS:
   1. <https://en.wikipedia.org/wiki/Attitude_control>
   1. <https://en.wikipedia.org/wiki/Ground_segment>
   1. <https://en.wikipedia.org/wiki/Guidance,_navigation,_and_control>
   1. <https://en.wikipedia.org/wiki/Space_segment>
   1. <https://en.wikipedia.org/wiki/Fixed-satellite_service>
   1. <https://ru.wikipedia.org/wiki/Научно-измерительный_пункт>
1. НС:
   1. <https://en.wikipedia.org/wiki/Ground_station>
   1. <https://ru.wikipedia.org/wiki/Восточный_центр_дальней_космической_связи>
   1. <https://ru.wikipedia.org/wiki/Калязинская_радиоастрономическая_обсерватория>
   1. <https://ru.wikipedia.org/wiki/РТ-70>
   1. <https://ru.wikipedia.org/wiki/Центр_космической_связи_「Медвежьи_озёра」>
1. БЦ:
   1. <http://www.ngpedia.ru/id584007p1.html>
   1. <http://www.keldysh.ru/httpd/kiam-info_fr.html>
   1. <http://www.kiam1.rssi.ru>
1. ЦУП:
   1. <https://en.wikipedia.org/wiki/Mission_control_center>
   1. <https://ru.wikipedia.org/wiki/Центр_управления_полётами_(организация)>


## The End

end of file
