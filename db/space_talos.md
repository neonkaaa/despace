# Space Talos
> 2022.01.27 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/s/space_talos_logo1t.webp)](f/c/s/space_talos_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *35 Kingsland Road, Hackney, London E2 8AA*<br> <https://spacetalos.com> ~~ [LI ⎆](https://www.linkedin.com/company/space-talos-ltd) ~~ [X ⎆](https://twitter.com/LtdTalos)|
|:--|:--|
|**Business**|Radiation shielding technologies, testing|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO — Alexandros Christou<br> ・CTO — Majed Jawad|

**Space Talos LTD** is a UK company aimed for radiation shielding & testing services for spacecraft components. Founded 2018.

<p style="page-break-after:always"> </p>

## The End

end of file
