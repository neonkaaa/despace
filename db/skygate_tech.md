# Skygate Technologies
> 2020.11.18 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/s/skygate_tech_logo1t.webp)](f/c/s/skygate_tech_logo1.webp)|<skygate@skygate-tech.com>, <mark>nophone</mark>, Fax …; *東京都江東区青海二丁目5番10号 テレコムセンタービル東棟14F. Koto, Tokyo, Japan*<br> <https://www.skygate-tech.com> ~~ [FB ⎆](https://www.facebook.com/skygatetech) ~~ [LI ⎆](https://www.linkedin.com/company/skygate-tech) ~~ [X ⎆](https://twitter.com/skygate_tech)|
|:--|:--|
|**Business**|…|
|**Mission**|To give every developer the power to change the world by earth observation data|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Skygate Technologies Inc.** affords Ground Station as a Service for satellites. Founded 2020.02.12.

Founded in 2020, Skygate Technologies is building a network to deliver a large volume of data from observation satellites around the Earth to the ground & integrating the network into the web architecture. 「Make Space accessible on the Web」 is its motto, & the ability to develop both hardware & software is its strength.

**Skygate Ground Station as a Service.** Skygate is a cloud ground station for satellite operators & constellation players. There is no need to build a ground station to operate the satellite. Once set up, you can use it whenever you need it.Skygate is optimized for low earth orbit satellites & automatically acquires & tracks satellites. Each communication path is automatically scheduled & can also be reserved.

- Full-managed. You do not need to maintain your antenna system.
- Security. Protect your data by achieving a high level of security compliance with international standards & regulations.
- Scaling by software. Skygate has a unified antenna specification & a software radio. The communication point can be scaled in any way.
- Support your setup. We can help you with licensing & communications testing. No need to bother with compatibility testing & frequency licensing.



## The End

end of file
