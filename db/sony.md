# Sony
> 2023.01.06 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/s/sony_logo1t.webp)](f/c/s/sony_logo1.svg)|<noemail>, <noworkphone>, Fax …;<br> *…, Tokyo, Japan*<br> <https://sony.com/>|
|:--|:--|
|**Business**|CubeSats|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・…|

**Sony**

Space projects & subsidiaries:

| | |
|:--|:--|
|[![](f/c/s/sony_star_sphere_logo1t.webp)](f/c/s/sony_star_sphere_logo1.webp)|**Star Sphere**<br> <https://starsphere.sony.com/en/>|
| |**Sony Space Communications Corporation**|



## The End

end of file
