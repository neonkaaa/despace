# Ad hoc
> 2019.07.25 [🚀](../../index/index.md) [despace](index.md) → [Control](control.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Ad hoc** — англоязычный термин, не имеющий аналога в русском языке. **Ad hoc** — дословный перевод с английского на русский.</small>

**Ad hoc** — латинская фраза, означающая 「специально для этого」, 「по особому случаю」.



## Описание
Как правило, фраза обозначает способ решения специфической проблемы или задачи, который невозможно приспособить для решения других задач и который не вписывается в общую стратегию решений, составляет некоторое исключение. Например, 「закон ad hoc」 — это закон, принятый в связи с каким‑то конкретным инцидентом или для решения какой‑то особой задачи, который не вписывается в законодательную практику и не решает других схожих проблем; 「отдел ad hoc」 — это подразделение в организации, созданное для решения какой‑то узкой задачи, не попадающей в сферу [компетенции](competence.md) ни одной из постоянных структур. В некоторых случаях выражение 「ad hoc」 может иметь негативный подтекст, предполагая отсутствие стратегического планирования и реакционные непродуманные действия.

1. В менеджменте 「управление ad hoc」 — это ситуационное управление (в противовес, или как дополнение к стратегическому управлению).
1. В международном праве термин 「ad hoc」 также используется для обозначения формы международно‑правового признания при необходимости установления разовых контактов между сторонами, которые категорически не желают признавать друг друга.
1. В науке и философии имеется понятие 「гипотезы ad hoc」 — гипотезы, выдвинутой для объяснения какого‑то особого явления или результатов конкретного эксперимента, не объясняющая при этом другие явления или результаты других экспериментов. При этом ученые зачастую скептически относятся к научным теориям, которые опираются на 「гипотезы ad hoc」 . Специальные гипотезы часто характерны для псевдонаучных предметов, таких как гомеопатия.
1. В армии специальные подразделения создаются в непредсказуемых ситуациях, когда сотрудничество между различными подразделениями внезапно необходимо для быстрых действий или из остатков предыдущих подразделений, которые были захвачены или иным образом сокращены.
1. В компьютерной технике имеется понятие 「беспроводные ad‑hoc‑сети」 — это сети, не имеющие постоянной структуры, в которых клиентские устройства соединяются 「на лету」, образуя собой сеть.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Control](Control.md)】**<br> [Ad hoc](ad_hoc.md) ~~ [Business travel](business_travel.md) ~~ [Chief designers council](cocd.md) ~~ [CML](trl.md) ~~ [Competence](competence.md) ~~ [Confident](confident.md) ~~ [Consp.theory](consp_theory.md) ~~ [Control sys. (CS)](cs.md) ~~ [Coordinate system](coord_sys.md) ~~ [Curator](curator.md) ~~ [Designer’s supervision](des_spv.md) ~~ [E‑sig](esig.md) ~~ [Engineer](se.md) ~~ [Errand](errand.md) ~~ [Federal law](fed_law.md) ~~ [Federal TP](fed_tp.md) ~~ [Federal SP](fed_sp.md) ~~ [GNC](gnc.md) ~~ [Gravity assist](gravass.md) ~~ [Industrial archaeology](ind_arch.md) ~~ [Instruction](instruction.md) ~~ [Lean manuf.](lean_man.md) ~~ [Lifetime](lifetime.md) ~~ [Manager](manager.md) ~~ [MBSE](se.md) ~~ [Meeting](meeting.md) ~~ [MCC](sc.md) ~~ [MIC](mic.md) ~~ [MML](mml.md) ~~ [MoU](contract.md) ~~ [Nav. & ballistics (NB)](nnb.md) ~~ [Nonprofit org.](nonprof_org.md) ~~ [NX](nx.md) ~~ [Oberth effect](oberth_eff.md) ~~ [Org.structure](orgstruct.md) ~~ [Outcomes commission](outccom.md) ~~ [Patent](patent.md) ~~ [Peter prin.](peter_principle.md) ~~ [Plan](plan.md) ~~ [PMBok](pmbok.md) ~~ [Quorum](quorum.md) ~~ [R&D management](mgmt.md) ~~ [R&D support](rnd_support.md) ~~ [Recursion](recurs.md) ~~ [Schulze_method](schulze_method.md) ~~ [Sci'N'Tech activities](st_act.md) ~~ [Sci'N'Tech council](satc.md) ~~ [Single-window system](sw_sys.md) ~~ [Situ.leadership](situ_leadership.md) ~~ [Skunk works](se.md) ~~ [State arm. plan](plan_sa.md) ~~ [Swamp](swamp.md) ~~ [Teamcenter](teamcenter.md) ~~ [Tennis racket theorem](tr_theorem.md) ~~ [TRIZ](triz.md) ~~ [TRL](trl.md) ~~ [V‑model](v_model.md) ~~ [Veto](veto.md) ~~ [Workflow](workflow.md) ~~ [Workgroup](wg.md)|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Ad_hoc>



## The End

end of file
