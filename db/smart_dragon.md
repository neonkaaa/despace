# Smart Dragon
> 2019.07.04 [🚀](../../index/index.md) [despace](index.md) → [LV](lv.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Jielong / Smart Dragon** — англоязычный термин, не имеющий аналога в русском языке. **Умный Дракон** — дословный перевод с английского на русский.</small>

**Jielong (Smart Dragon)** — китайская одноразовая твердотопливная ракета‑носитель лёгкого класса.

|**Version**|**Description**|**Activity**|
|:--|:--|:--|
|Smart Dragon 1|Базовый вариант.|**Активен** (2019 ‑  …)|
[![](f/lv/smart_dragon/jielong1_01t.webp)](f/lv/smart_dragon/jielong1_01.webp)



---



## Smart Dragon-1
**Jielong-1 (Smart Dragon-1)** — китайская одноразовая твердотопливная ракета‑носитель лёгкого класса.

|**Characteristic**|**[Value](si.md)**|
|:--|:--|
|Активность|**Активен** (2019.07.15 ‑ …)|
|[Аналоги](analogue.md)|[Electron](electron.md) (США) ~~ [Shavit](shavit.md) (Израиль) ~~ [Unha](unha.md) (Корея сев.)|
|Длина/диаметр|19.5 м / 1.2 м|
|[Космодромы](spaceport.md)|[Jiuquan](spaceport.md)|
|Масса старт./сух.|23 100 ㎏ / … ㎏|
|Разраб./изготов.|CASC group (Китай) / CASC group (Китай)|
|Ступени|4|
|[Fuel](ps.md)|[HTPB](ps.md)|
| |[![](f/lv/smart_dragon/jielong1_01t.webp)](f/lv/smart_dragon/jielong1_01.webp) [![](f/lv/smart_dragon/jielong1_02t.webp)](f/lv/smart_dragon/jielong1_02.webp)|

**Выводимые массы.**

|**Космодром**|**РН**|<small>*Масса,<br> [НОО](nnb.md), т*</small>|<small>*Масса,<br> [ГСО](nnb.md), т*</small>|<small>*Масса к<br> [Луне](moon.md), т*</small>|<small>*Масса к<br> [Венере](venus.md), т*</small>|<small>*Масса к<br> [Марсу](mars.md), т*</small>|**Примечания**|
|:--|:--|:--|:--|:--|:--|:--|:--|
|[Jiuquan](spaceport.md)|Smart Dragon-1|0.25|…|…|…|…|Пуск — $ … млн (… г);<br> ПН 1.08 % от ст.массы;<br> На [ССО](nnb.md) — 150 кг|

<small>Примечания:<br> **1)** Указана масса для наихудших условий старта.<br> **2)** В скобках указана масса для наилучших условий старта.</small>



## Архивные

…



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[](.md)】**<br> <mark>NOCAT</mark>|

1. Docs: …
1. <https://space.skyrocket.de/doc_lau/jielong-1.htm>


## The End

end of file
