# IRF
> 2019.08.04 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/i/irf_logo1t.webp)](f/c/i/irf_logo1.webp)|<irf@irf.se>, +46(980)790-00, Fax +46(980)790-50;<br> *Box 812, SE‑981 28 Kiruna, Sweden*<br> <http://www.irf.se>|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Swedish Institute of Space Physics (Institutet för rymdfysik, IRF)**, рус. **Институт космической физики (ИКФ)** — государственный научно‑исследовательский институт. Основные задачи — проведение фундаментальных исследований в области космической физики, космической техники и физики атмосферы.



## The End

end of file
