# Bascule
> 2021.12.10 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/b/bascule_logo1t.webp)](f/c/b/bascule_logo1.webp)|<mark>noemail</mark>, +81(03)5797-7110, Fax …;<br> *Azabu Kaisei Bldg 6F 1-8-10 Azabudai, Minato-ku, Tokyo 106-0041, Japan*<br> <https://bascule.co.jp> ~~ [FB ⎆](https://www.facebook.com/BasculeInc) ~~ [X ⎆](https://twitter.com/bascule_inc)|
|:--|:--|
|**Business**|Advertising, media, products, sports, education, mobility, urban development, space|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO, Representative Director — Masayoshi Boku|

**Bascule Co., Ltd.** is a project design studio that creates an exciting future with 「PLAY WITH FUTURE」. Founded 2000.07.28.

Advertising, media, products, sports, education, mobility, urban development, & space. With the curiosity of wanting to reach such a future & the penetration of digital technology into society, our creative field is expanding year by year. What we are trying to do is to create a future standard that goes beyond the existing framework by connecting things that were not previously connected, driven by a vision that everyone can be excited about. We will continue to take on unprecedented projects with co‑creation partners that connect across fields & industries.

<p style="page-break-after:always"> </p>

## The End

end of file
