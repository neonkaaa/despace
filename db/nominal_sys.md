# Nominal Systems
> 2022.04.06 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/n/nominal_sys_logo1t.webp)](f/c/n/nominal_sys_logo1.webp)|<info@nominalsys.com>, +614 4925 6635, Fax …;<br> *147 Limestone Avenue, Braddon, Canberra, 2612 - Australia*<br> <https://www.nominalsys.com>・[GitHub ⎆](https://github.com/Space-Services-Australia) ・ [LI ⎆](https://au.linkedin.com/company/nominal-systems)|
|:--|:--|
|**Business**|Software for simulation, system analysis|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO, Founder — Christopher Capon<br> ・CTO, Founder — Brenton Smith|

**Nominal Systems** (formerly **Space Services Australia Pty Ltd**) is an Australian provider of digital engineering tools in space. Founded 2019.11.11.

Nominal Systems assists customers (ranging from Universities to defence organizations) with R&D, mission design, testing & evaluation or operations.



## The End

end of file
