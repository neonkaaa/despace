# Sagri
> 2021.12.10 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/s/sagri_logo1t.webp)](f/c/s/sagri_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *725-1 Joraku, Hikamicho, Tamba City, Hyogo Prefecture, 〒669-3602, Japan*<br> <https://sagri.tokyo> ~~ [LI ⎆](https://www.linkedin.com/company/sagri)|
|:--|:--|
|**Business**|Space data utilization, machine learning, software|
|**Mission**|Utilizing satellite data & ground data to optimize the activities of humankind living on the Earth|
|**Vision**|Realizing the coexistence of humankind & the Earth|
|**Values**|…|
|**[MGMT](mgmt.md)**|・Representative Director, CEO — Shunsuke Hirai<br> ・COO — Shu Masuda<br> ・CTO — Takashi Tanaka|

**Sagri Co., Ltd** (Satellite × AI × Grid) is a Japanese company aimed to build algorithms from data obtained from satellites & data from on‑site data on the ground to improve the activities of humankind living on the Earth, incl. agriculture. Founded 2018.06.14.

- Tokyo Headquarters — Room 802, Wakore Shinjuku Daiichi Building, 7-7-26 Nishishinjuku, Shinjuku-ku, Tokyo
- Office — 5 Tomei, Higashimiyukicho, Toyohashi City, Aichi Prefecture Satelli-con valley booth A

**Technology:**

- **Advances in AI technology. Satellite data that is big data.** Since satellite data can be obtained over a wide area & uniformly on the ground, its usefulness is expected. Since it is possible to collect infrared & microwave data in the invisible light region that is invisible to the human eye, research in various applications is progressing. However, on the other hand, satellite data has a limited resolution (image resolution), & while there are various types of sensors, it is difficult for end users to directly utilize it, so it is necessary to have a player who can provide it appropriately. It is the situation that is being done. Satellite data has a long history, starting from the purpose of military use & has been used for research purposes for a long time. Until now, features & changes have been visually captured by analyzing the combination of wavelengths in each sensor & coloring satellite images according to the analysis results. Many satellites orbit the same place on a regular basis & can capture changes over time, so it is so‑called big data, & there were limits to the analysis methods used so far, but AI technology has advanced in recent years. As a result, the situation has changed significantly.
- **Let AI learn. AI predicts farmland conditions.** We analyze satellite data using AI technology. For satellite data, which is the reflection value of the wavelength of each sensor, it is converted into useful data by giving a meaning to what kind of state the reflection value indicates on the ground. So to speak, by letting AI learn the characteristics of satellite data in various states on the ground, we will create a state in which AI can make predictions. For example, our agricultural land situation grasping app 「ACTABA」 provides a solution to the big agricultural problem in Japan, which is the increase of abandoned cultivated land. ACTABA analyzes satellite data & uses the AI ​​developed by our company to learn whether the farmland is cultivated or abandoned for each field to capture the characteristics of the farmland. The constructed algorithm presents a prediction of the probability of abandoned cultivated land.
- **Utilizing machine learning (AI). Agricultural land automatic polygon technology.** Currently, we will automate the development by AI image recognition technology using the plot information (brush polygon) of domestic agricultural land, which was completed by the Ministry of Agriculture, Forestry & Fisheries in March 2019 & began to be provided as open data in April, as teacher data. By doing so, we are aiming to convert agricultural land around the world into polygons. Agricultural land polygons are expected to be the basis for holding all the data digitized by smart agriculture, & as a data infrastructure, both hard & soft in smart agriculture. It is supposed to work with various tools of.

**Message from CEO**

> 「Realizing the coexistence of the Earth & humankind」.<br> The Earth is enormously influenced by humankind. Once upon a time, the land, which was full of nature, was greatly transformed by humankind. We must be aware that it is humankind that is leading the global environment to ruin. The ancestors created the technology & made improvements so that people of the next era could use it in their daily lives. Tabata was cultivated during the Yayoi period & gained the wisdom of producing food with their own hands. By making full use of the technology created in modern times, we have enriched our lives. Thanks to this, humankind can live without any inconvenience. However, some have been left behind. That is the disparity. There are disparities between central & rural areas, & there are also disparities between developed & developing countries. Through Sagri, I want to create an optimal environment & mechanism for the coexistence of the Earth & humankind. And I want to realize the coexistence of the Earth & humankind.

<p style="page-break-after:always"> </p>

## The End

end of file
