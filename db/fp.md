# Полётное задание
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → **[НКУ](sc.md)**, [ПОЗ](fp.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Полётное задание (ПОЗ, ПЗ)** — русскоязычный термин. **Flight profile (FP)** — англоязычный эквивалент.</small>

**Полётное задание (ПОЗ, ПЗ)** — информация для настройки системы управления [LV](lv.md) и [РБ](lv.md), обеспечивающая предполётную подготовку, полёт ракеты и элементов её оснащения по требуемым траекториям и осуществления необходимых манёвров.



## Описание



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**`Наземный комплекс управления (НКУ):`**<br> [БЦ](sc.md) ~~ [КИС](sc.md) ~~ [КСИСО](sc.md) ~~ [НИК](lm_sys.md) ~~ [НИП](sc.md) ~~ [НС](sc.md) ~~ [ПОЗ](fp.md) ~~ [СГК](cd_segm.md) ~~ [ССПД](mcntd.md) ~~ [ЦУП](sc.md)|

1. Docs: …
1. <http://encyclopedia.mil.ru/encyclopedia/dictionary/details_rvsn.htm?:id=14607@morfDictionary>



## The End

end of file
