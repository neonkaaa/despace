# Spartan Space
> 2022.03.05 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/s/spartan_space_logo1t.webp)](f/c/s/spartan_space_logo1.webp)|<info@spartan-space.com.>, <mark>nophone</mark>, Fax …;<br> *41 Traverse Parangon Bat1, 13008 Marseille, France*<br> <https://spartan-space.com> ~~ [FB ⎆](https://www.facebook.com/spartanspace) ~~ [IG ⎆](https://www.instagram.com/spartan__space) ~~ [LI ⎆](https://www.linkedin.com/company/spartanspace) ~~ [X ⎆](https://twitter.com/Spartan__space)|
|:--|:--|
|**Business**|Space habitats|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO, Founder — Peter Weiss<br> ・CSO — Jean-Jacques Favier|

**Spartan Space** is a startup developing smart habitats for space & underwater. Founded in 2021 by a group of Senior Space & Underwater Engineers, Astronauts & Architects.

<p style="page-break-after:always"> </p>

## The End

end of file
