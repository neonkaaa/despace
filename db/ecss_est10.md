# ECSS‑E‑ST‑10C Rev.1 (15‑Feb‑2017)
> 2017.02.15 [🚀](../../index/index.md) [despace](index.md) → [Doc](doc.md), [ECSS](ecss.md), [SC](sc.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

## 1. Introduction & Scope

**「System engineering general requirements」**

<small>

This Standard is one of the series of ECSS Standards intended to be applied together for the management, engineering & product assurance in space projects & applications. ECSS is a cooperative effort of the European Space Agency, national space agencies & European industry associations for the purpose of developing & maintaining common standards. Requirements in this Standard are defined in terms of what shall be accomplished, rather than in terms of how to organize & perform the necessary work. This allows existing organizational structures & methods to be applied where they are effective, & for the structures & methods to evolve as necessary without rewriting the standards.

This Standard has been prepared by the ECSS‑E‑ST‑10C Rev.1 Working Group, reviewed by the ECSS Executive Secretariat & approved by the ECSS Technical Authority.

ECSS does not provide any warranty whatsoever, whether expressed, implied, or statutory, including, but not limited to, any warranty of merchantability or fitness for a particular purpose or any warranty that the contents of the item are error‑free. In no respect shall ECSS incur any liability for any damages, including, but not limited to, direct, indirect, special, or consequential damages arising out of, resulting from, or in any way connected to the use of this Standard, whether or not based upon warranty, business agreement, tort, or otherwise; whether or not injury was sustained by persons or property or otherwise; & whether or not loss was sustained from, or arose out of, the results of, the item, or any services that may be provided by ECSS.

</small>

This standard specifies the **system engineering (SE)** implementation requirements for space systems & space products development. Specific objectives of this standard are:

1. to implement the SE requirements to establish a firm technical basis & to minimize technical risk & cost for space systems & space products development;
1. to specify the essential SE tasks, their objectives & outputs;
1. to implement integration & control of engineering disciplines & lower level SE work;
1. to implement the 「customer‑system‑supplier model」 through the development of systems & products for space applications.

Depending of the product category, the application of this standard needs to be checked & if needed tailored. The pre‑tailoring table in clause 7 contains the applicability of the requirements of this document & its annexes according to product type. Specific requirements related to SE, like technical specification, verification, & testing are specified in dedicated documents & standards within the set of ECSS SE standards ECSS‑E‑ST‑10‑XX. Discipline or element specific engineering implementation requirements are covered in dedicated ECSS standards. These standards are based on the same principles, process & documentation model. The applicability of each these standards can therefore not be considered in isolation from the others.

The term 「Discipline」 is defined in ECSS‑M‑ST‑10, as 「a specific area of expertise within a general subject」. The name of the discipline normally indicates the type of expertise, e.g. in the ECSS system mechanical engineering, software & communications are disciplines within the engineering domain. The requirements on the SE process are gathered in this standard; specific aspects of the SE process are further elaborated in dedicated standards.

For engineering process both for SW & for Ground Segment & Operations the following standards are considered fully sufficient for development of these items:

1. ECSS‑E‑ST‑70 Space engineering — Ground systems & operations
1. ECSS‑E‑ST‑40 Space engineering — Software
1. ECSS‑Q‑ST‑80 Space product assurance — Software product assurance

This standard may be tailored for the specific characteristic & constrains of a space project in conformance with ECSS‑S‑ST‑00.



## 2. Normative references & Bibliography

The following normative documents contain provisions which, through reference in this text, constitute provisions of this ECSS Standard. For dated references, subsequent amendments to, or revision of any of these publications do not apply, However, parties to agreements based on this ECSS Standard are encouraged to investigate the possibility of applying the more recent editions of the normative documents indicated below. For undated references, the latest edition of the publication referred to applies.

1. ECSS‑S‑ST‑00‑01 — ECSS system — Glossary of terms
1. ECSS‑E‑AS‑11 — Adoption Notice of ISO 16290, Space systems — Definition of the Technology Readiness Levels (TRLs) & their criteria of assessment
1. ECSS‑E‑ST‑10‑02 — Space engineering — Verification
1. ECSS‑E‑ST‑10‑06 — Space engineering — Technical requirements specification
1. ECSS‑E‑ST‑10‑09 — Space engineering — Reference coordinate system
1. ECSS‑E‑ST‑10‑24 — Space engineering — Interface control
1. ECSS‑M‑ST‑10 — Space project management — Project planning & implementation
1. ECSS‑M‑ST‑40 — Space project management — Configuration & information management
1. [ECSS‑Q‑ST‑10](ecss_qst10.md) — Space product assurance ‑ Product assurance management
1. ECSS‑Q‑ST‑10‑09 — Space product assurance ‑ Nonconformance control system
1. ECSS‑Q‑ST‑20‑10 — Off‑the‑shelf items utilization in space systems
1. [Margin philosophy for science assessment studies](ecss_mpsas.md)

**Bibliography:**

1. ECSS‑S‑ST‑00 — ECSS system — Description, implementation & general requirements
1. ECSS‑E‑ST‑10‑03 — Space engineering — Testing
1. ECSS‑E‑ST‑10‑04 — Space engineering — Space environment
1. ECSS‑E‑ST‑10‑11 — Space engineering — Human factors engineering
1. ECSS‑E‑ST‑20 — Space engineering — Electrical & electronic
1. ECSS‑E‑ST‑3x series — Space engineering — Materials series
1. ECSS‑E‑ST‑40 — Space engineering — Software general requirements
1. ECSS‑E‑ST‑50 — Space engineering — Communications
1. ECSS‑E‑ST‑6x series — Space engineering — Control engineering series
1. ECSS‑E‑ST‑70 — Space engineering — Ground systems & operations
1. ECSS‑E‑ST‑70‑41 — Space engineering — Telemetry & telecommand packet utilization
1. ECSS‑E‑TM‑10‑10 — Space engineering — Logistic engineering
1. ECSS‑E‑TM‑10‑20 — Space engineering — Product data exchange
1. ECSS‑E‑TM‑10‑21 — Space engineering — System modelling & simulation
1. ECSS‑M‑ST‑60 — Space project management — Cost & schedule management
1. ECSS‑M‑ST‑70 — Space project management — Integrated logistic support
1. ECSS‑M‑ST‑80 — Space project management — Risk management
1. ECSS‑Q‑ST‑10 — Space product assurance — Product assurance management
1. IEEE P1220 — Standard for application & management of the systems engineering process
1. ISO 24113:2011 — Space systems — Space debris mitigation


## 3. Terms, definitions & abbreviated terms

**Terms from other standards.** The terms & definitions from ECSS‑S‑ST‑00‑01 apply, in particular:

1. acceptance
1. approval
1. configuration baseline
1. critical
1. development
1. equipment
1. inspection
1. integration
1. mission statement
1. product tree
1. requirement
1. specification
1. subsystem
1. system
1. test
1. verification

The terms & definitions from ECSS‑E‑AS‑11 apply, in particular:

1. technology readiness level

**Terms specific to the present standard**

1. **requirement traceability** — requirement attribute that links each single requirement to its higher level requirements inside the requirement set. This enables the derivation of a requirement tree, which demonstrates the coherent flow‑down of the requirements.
1. **recurring product** — product which conforms to a qualified design & is produced according to the corresponding production master file.
1. **SE** — interdisciplinary approach governing the total technical effort required to transform requirements into a system solution. From IEEE P1220.
1. **verification matrix** — initial issue of the VCD which contains for each requirement to be verified the methods, levels & stages of product verification. See ECSS‑E‑ST‑10‑02 for a more detailed definition of the VCD.

**Abbreviated terms.** The abbreviations from ECSS‑S‑ST‑00‑01 & the following apply:

1. **AIT** — assembly, integration & test
1. **AIV plan** — assembly, integration & verification plan
1. **AOCS** — attitude & orbit control sub‑system
1. **AR** — acceptance review
1. **CDR** — critical design review
1. **COTS** — commercial off‑the‑shelf
1. **CRR** — commissioning results review. For space vehicles (e.g. launcher, transfer vehicle, crew transport vehicle) the CRR can be replaced or complemented by a flight qualification review (FQR)
1. **DDF** — design definition file
1. **DDP** — design development plan
1. **DJF** — design justification file
1. **DRD** — document requirements definition
1. **ECSS** — European Cooperation for Space Standardization
1. **ELR** — end‑of‑life review
1. **FDIR** — failure, detection, isolation, recovery
1. **FM** — flight model
1. **FMECA** — failure modes, effects, & criticality analysis
1. **FOM** — flight operations manual
1. **FRR** — flight readiness review
1. **FTA** — fault tree analysis
1. **GSE** — ground support equipment
1. **HITL** — human‑in‑the‑loop
1. **ICD** — interface control document
1. **ILS** — integrated logistic support
1. **IRD** — interface requirement document
1. **LRR** — launch readiness review
1. **MCR** — mission closed‑out review
1. **MDD** — mission description document
1. **MDR** — mission definition review
1. **MOP** — mission operations plan
1. **MS** — mission statement
1. **ORR** — operational readiness review
1. **PDR** — preliminary design review
1. **PM&P** — parts materials & processes
1. **PMP** — project management plan
1. **PRR** — preliminary requirement review
1. **PUM** — product user manual
1. **QR** — qualification review
1. **RAMS** — reliability, availability, maintainability, safety
1. **RAR** — risk assessment report
1. **RF** — radio frequency
1. **RJF** — requirement justification file
1. **ROD** — review of design
1. **ROM/RAM** — read only memory / random access memory
1. **RTM** — requirement traceability matrix
1. **R&D** — research & development
1. **SE** — SE
1. **SEP** — SE plan
1. **SFT** — system functional test
1. **SRR** — system requirement review
1. **SVT** — system validation test
1. **SW** — software
1. **TP** — technology plan
1. **TRA** — technology readiness assessment
1. **TRL** — technology readiness level
1. **TRSL** — technology readiness status list
1. **TS** — technical requirements specification
1. **UM** — user manual
1. **VCD** — verification control document
1. **VP** — verification plan
1. **w.r.t.** — with respect to


## 4. Overview of SE

### 4.1. The SE discipline

System engineering is an interdisciplinary approach governing the total technical effort to transform requirements into a system solution.

A system is an integrated set of elements to accomplish a defined objective. These elements include hardware, software, firmware, human resources, information, techniques, facilities services, & other support elements.

In this standard the concept of 「system」 is used in a wide sense. The highest level, often called 「mission level」 or 「space system」, consists usually of one (or more) space segment(s), of a ground segment, & of a user segment. Elements of system decomposition are also considered a system. For the purpose of this standard the system can be any element at any level of decomposition as defined by the function tree (see Annex H) or the product tree (see ECSS‑M‑ST‑10 Ann.B). The scope of an element can include hardware, software, procedures, man‑in‑the‑loop, facilities & services.

From the perspective of the considered system, requirements originate from the next upper level (the customer) & elements are procured from the next lower level (the suppliers). (1) The customer‑supplier model is described in ECSS‑S‑ST‑00. (2) Through this standard the notion of customer refers to several actors ,in relation to the project phase. In fact a customer can be e.g. a scientific community in phase 0, a commercial user in phase A or an Agency in phase B. A supplier can on the other hand be an Agency in both phase 0 & phase A.

Figure 4‑1 shows the boundaries of SE (for which the indicated interactions between the identified major disciplines is not exhaustive but indicative), its relationship with production, operations, product assurance & management disciplines (and their cross‑interaction is indicated as 「interface areas」 in the figure) & its internal partition into the following SE sub‑functions:

1. requirement engineering, which consist on requirement analysis & validation, requirement allocation, & requirement maintenance;
1. analysis, which is performed for the purpose of resolving requirements conflicts, decomposing & allocating requirements during functional analysis, assessing system effectiveness (including analysing risk factors); & complementing testing evaluation & providing trade studies for assessing effectiveness, risk, cost & planning;
1. design & configuration which results in a physical architecture, & its complete system functional, physical & software characteristics;
1. verification, which objective is to demonstrate that the deliverables conform to the specified requirements, including qualification & acceptance;
1. SE integration & control, coordinating the various engineering disciplines & participants throughout all the project phases.

Figure 4‑2 shows SE sub‑functions, their inter‑relationships & their main activities during the SE process.

System engineering sub‑functions are applied in an iterative mode during implementation of the SE process described in clause 4.2.

Within the frame of a project, the SE function is generally implemented by a SE organisation of the supplier which is in charge of transforming the requirements of the customer into a system solution delivered by the supplier. For the purpose of this standard, the ‘SE function’ only is referred to in the normative statements, independent of whether the supplier has a formal ‘SE organisation’ or not. With respect to the next lower level, the supplier plays the role of the customer.

【**Fig.4‑1:** System engineering, sub‑functions & boundaries】  
![](f/doc/ecss/ecss_est10/41.png)

【**Fig.4‑2:** System engineering sub‑functions inter‑relationships】  
![](f/doc/ecss/ecss_est10/42.png)



### 4.2. The SE process

The SE activities of a project are conducted by an entity or resources within the project team of a supplier. This entity or the resources that perform this function is called in this document 「SE function」. The SE process is in turn applied by each SE function of each supplier of the elements of the product decomposition.

The SE process consists of activities to be performed by the SE function within each project phase according to the designated lifecycle model. The objective is to obtain a product which satisfies the customer technical requirements within pre‑established objectives of cost time & quality. The requirements for these activities are described in clause 5.

The SE process is intrinsically iterative across the whole life of a project, in particular in the initial phases (i.e. 0, A, & B) of the development of a complex system (e.g. a spacecraft), procured through a multi‑layered set of suppliers.

During these phases, the SE function derives design oriented technical solutions starting from the design‑independent customer requirements contained in a technical requirements specification (TS). This is achieved through an iterative top‑down process by trading off several design solutions at an increasing level of detail. For definition & requirements for a technical requirements specification see ECSS‑E‑ST‑10‑06.

Through this process the SE function performs a multidisciplinary functional decomposition to obtain logical lower level products (both hardware & software). At the same time the SE function decides on balanced allocations, throughout the system, of resources allocated by the customer & respects agreed [margin philosophies](ecss_mpsas.md) as a function of the relevant technology readiness levels.

The functional decomposition defines, for each level of the system, the technical requirements for the procurement of subassemblies or lower level products as well as the requirements for the verification of the final characteristics of each product.

The SE process uses the results of these lower level verification activities to build a bottom‑up multi‑layered evidence that the customer requirements have been met.

The SE process is applied with various degrees of depth depending on the level of maturity of the product (e.g. new development or off‑the‑shelf).

The SE process can be applied with different level of tailoring as agreed between customer & supplier in their business agreement.

The SE function has interfaces with those other functions in charge of management, product assurance, engineering disciplines, production, & operations & logistics. The project phases are defined in ECSS‑M‑ST‑10.

### 4.3. Overview of SE tasks per project phase

**Overview.** The allocation of specific SE requirements per phase depends strongly on the type of business agreement between customer & supplier & the nature & level of complexity of the system subject of the agreement. The breakdown & the details of the tasks are defined in the business agreement specific documents (Some projects define them in a Statement of work (SoW)). The actors in the customer‑supplier relationship change between phases & across levels. In the following clauses each SE function is meant to be the supplier’s SE function during that phase.

**General**

1. The SE function plans its activities in conformance with the project phases as per the Project Management Plan in accordance with ECSS‑M‑ST‑10 & document it in the SEP as per Annex D. (The phases or combination thereof to be implemented for a project are specified in the business agreement)
1. The SE function monitors the execution of all SE activities including lower levels.
1. The SE function identifies the critical items in cooperation with the Product Assurance (PA) according to ECSS‑Q‑ST‑10 (PA being tasked to manage the critical items throughout the project life).
1. The SE function ensures that for critical items the technical requirements specification, the design definition file & the design justification file are available at latest by end of Phase B. (Information regarding the expected delivery of SE documents for each project review is provided in 7. The documents to be approved by the customer as well as the time of their approval are listed in the business agreement.)
1. The SE function activities need to be aligned with Product Assurance (PA) activities according to ECSS‑Q‑ST‑10 throughout the project life. This includes definition & execution of tasks in which PA activities are involved (detailed in the Product Assurance Plan).

**Phase 0: Mission analysis‑need identification.** For Phase 0, the SE function:

1. support the identification of customer needs.
1. propose possible system concepts.
1. support the Mission Definition Review (MDR) & ensures implementation of the MDR actions.
1. perform an analysis of the Mission Statement document, & integrates this analysis & any relevant contribution from lower level suppliers in to a Mission Description document(s) in conformance with Annex B, & maintains this latter document for the final selected concept.
1. propose the requirements against the expressed user needs for agreement with the customer. (Mission Statement captures the declared 「user needs」.)

**Phase A: Feasibility.** For Phase A, the SE function:

1. finalise the expression of the needs identified in Phase 0.
1. propose system solutions (including identification of critical items & risks) to meet the customer needs.
1. support the Preliminary Requirement Review (PRR) & ensure implementation of PRR actions.
1. finalise the validation of the requirements against the expressed needs together with the customer. (Mission Statement captures the declared 「user needs」)

**Phase B: Preliminary definition.** For Phase B, the SE function:

1. establish the system preliminary definition for the system solution selected at Phase A.
1. demonstrate that the solution meets the technical requirements according to the schedule, the target cost & the customer requirements.
1. support the System Requirements Review (SRR) & Preliminary Design Review (PDR), & ensuring implementation of the SRR & PDR actions.
1. define development approach & plan of engineering activities.

**Phase C: Detailed definition.** For Phase C, the SE function:

1. establish the system detailed definition.
1. demonstratesthe capability to meet the technical requirements of the system technical requirements specification.
1. support the Critical Design Review (CDR) & ensures implementation of the CDR actions.

**Phase D: Qualification & production.** For Phase D, the SE function:

1. finalize the development of the system by qualification & acceptance.
1. finalize the preparation for operations & utilization.
1. support Qualification Review (QR) & Acceptance Review (AR) & ensures implementation of the QR & AR actions.

**Phase E: Operations / utilization.** For Phase E, the SE function:

1. support the launch campaign.
1. support the entity in charge of the operations & utilization following the terms of a business agreement.
1. support the Flight Readiness Review (FRR), Operations Readiness Review (ORR), Launch Readiness Review (LRR), Commissioning Results Review (CRR), End‑of‑Life Review (ELR), & recurring products AR, & ensure implementation of the actions of those reviews.
1. support the execution of all SE activities & provision of documents in support to anomaly investigations & resolutions. (Phase E & its reviews as presented in A.1.1.1.<1>Table A‑1 refer only to mission level. In case of lower level product, activities to be considered by the SE function are only related to maintenance & anomaly investigations.)

**Phase F: Disposal.** For Phase F, the SE function:

1. support the entity in charge of the disposal following the terms of a business agreement.
1. support the Mission Close‑out Review (MCR) & ensure implementation of the actions of the MCR. (Phase F & its review as presented in A.1.1.1.<1>Table A‑1 refer only to mission level. In case of lower level product, activities to be considered by the SE function are only related to disposal.)


## 5. General requirements

### 5.1. System engineering plan

1. The SE function shall produce & maintain a SE plan (SEP) in conformance with Annex D. (The SE function establishes the SEP with the contributions & constraints of management, product assurance, engineering disciplines, production, & operations & logistics.)
1. [deleted & moved modified as Note to 5.1.1.1a.]
1. The SEP shall take consideration of the lower level plans, ensure consistency between these plans, and, be consistent with these plans.
   1. 1. The early version of the Project Management Plan (PMP), which includes the early version of the SEP, contains all the information which was traditionally contained in the Design & Development Plan (DDP). See Annex R which illustrates the mapping between a typical DDP & ECSS DRDs.
   1. 2. The SEP content evolves with the phase of the project, with more information on risk analysis & new technologies in early phases 0, A & B, & more information on verification & validation aspects in phases C, D.
   1. 3. The SEP can be considered a collection of documents delivered over the life‑cycle as illustrated in Table A‑1.
   1. 4. No SEP lower than equipment/unit.
1. The SE function shall support the project manager in the project reviews as per the Project Management Plan in accordance with ECSS‑M‑ST‑10.

### 5.2. Requirement engineering

**General**

1. The SE function shall analyse the requirements for the system issued by the customer. ((1) This analysis enables the transformation of customer requirements into the supplier’s system solution. (2) The level of the required analysis & form of any deliverable is expressed in the business agreement.)
1. The SE function shall derive, generate, control & maintain the set of requirements for the lower level elements, defining their design & operational constraints & the parameters of functionality, performance, & verification necessary to meet the system requirements issued by the customer.
1. The SE function shall ensure consistency of the requirements at system level, at lower levels, as well as amongst levels. (Consistency of requirements of different SE sub‑functions at the same level is the responsibility of the higher level SE function.)
1. The SE function shall ensure requirements generated in 5.2.1.1b.. are in conformance with characteristics specified in ECSS‑E‑ST‑10‑06 clause 8.
1. The SE function shall ensure that each requirement for the lower level elements has a justification reflected in the requirement justification file in conformance with Annex O. (Tailoring of a standard in a list of applicable standards, or of a requirement in an applicable standard, is possible where each tailoring measure is duly justified.)

**Requirement traceability**

1. The SE function shall ensure forward & backward traceability of all requirements:
   1. to their sources;
   1. to the lower level requirements, if existing;
   1. to changes in the design inducing modifications of the requirements;
   1. to their verification close‑out. (to item 1: Examples for sources: a higher level requirement, an imposed management constraint, an applicable standard or an accepted lower level constraint)
1. The SE function shall establish & maintain the requirements traceability matrix in conformance with Annex N.
1. The SE function shall ensure that the requirement close‑out traceability is documented in the VCD in conformance with ECSS‑E‑ST‑10‑02 Annex B.

**Requirement engineering process**

1. **Technical requirements specifications**
   1. The SE function shall establish technical requirements specifications of the next lower level products consistent among them & with the technical specification received from the customer.
   1. The SE function shall ensure that the technical requirements specifications it establishes conform to ECSS‑E‑ST‑10‑06 & its DRD in Annex A.
   1. The SE function shall establish a specification tree in conformance with Annex J. ((1) Requirements common to more than one lower level product can be gathered in 「common」 technical specifications called 「support specifications」 (e.g. GDIR 「General Design & Interface Requirements」, environmental, test, EMC requirements specifications). (2) Requirements for equipment level products (or lower level products) can be issued in self‑contained specifications.)
   1. [deleted]
1. **Requirement consolidation**
   1. The SE function shall involve the customer in the consolidation of requirements by identifying & resolving incomplete, duplicate, ambiguous, & contradictory requirements for customer‑issued requirements.
   1. The SE function shall reflect the consolidated requirements in the release of the technical specifications.
1. **Requirement risk analysis**
   1. [deleted]
   1. The SE function shall perform the requirements analysis to identify impacts on system risks.
   1. As part of the risk management process implemented on the project, the SE function shall report the requirement impacts on the risk. (ECSS‐M‐ST‐80 Annex E describes the risk reporting process)
1. **Requirements verification methods**
   1. The SE function shall ensure that for each requirement contained in the technical requirements specification, one or a combination of verification methods are identified. (Technical requirements specification is defined in ECSS‑E‑ST‑10‑06 Ann.A)
   1. The SE function shall ensure that for each requirement contained in the technical requirements specification, the verification methods are reflected in the verification matrix. (Technical requirements specification is defined in ECSS‑E‑ST‑10‑06 Ann.A)
1. **Requirement allocation**
   1. The SE function shall ensure that the system requirements & their verification methods are allocated to lower levels & included in the specifications of the related products.
1. **Requirements consistency**
   1. [deleted]
   1. [deleted]
1. **Requirements agreement**
   1. In phase 0 the SE function shall propose   requirements in response to  the expressed user needs, for agreement with the customer. (」User needs」 are expressed in the Mission Statement document. In some contexts this activity is called 「requirements validation」. This terminology has not been used to avoid confusion with the term 「validation」 as per ECSS‑S‑ST‑00‑01.
1. **Requirements maintenance**
   1. The SE function shall ensure that agreed changes to requirements are applied to & maintained in system & lower level specifications.
   1. [deleted].
1. **Requirements baseline**
   1. The SE function shall establish the list of documents constituting the system requirements baselines in contribution to the configuration baselines. (Details on configuration baselines are provided in ECSS‑M‑ST‑40)

### 5.3. Analysis

1. **System analysis**
   1. In phase 0 the SE function shall perform an analysis of the Mission Statement document, produce Mission Description document(s) in conformance with Annex B, & maintain this latter document for the final selected concept.
   1. The SE function shall perform a functional analysis, produce the functional architecture, & produce the function tree which satisfy the customer technical requirements specification, in conformance with Annex H.
   1. The SE function shall document the functional architecture in the design definition file (DDF) in conformance with Annex G.
   1. The SE function shall justify the functional architecture in the DJF in conformance with Annex K.
   1. The SE function shall perform a physical analysis, produce the physical architecture & produce the product tree in conformance with ECSS‑M‑ST‑10 Annex B.
   1. The SE function shall document the physical architecture in the DDF in conformance with Annex G.
   1. The SE function shall justify the physical architecture in the DJF in conformance with Annex K.
   1. The SE function shall analyse the performance of the system, including end‑to‑end evaluation, documenting the results of the analysis in the Design Justification File in conformance with Annex K.
   1. The SE function shall analyse the influence of mission, design, development, operations & constraints on cost & schedule as an input to the project cost & schedule consolidation & to the utilisation recurring cost
   1. The SE function shall document analysis that it performs in an analysis report. (An example of an analysis report is given in Annex S)
1. **System environments & design & test factors**
   1. The SE function shall establish the influence of all types of environments applied during each life profile event on system & its elements in terms of nominal & extreme environmental conditions including all applicable operational phases.
   1. The SE function shall establish the criteria for qualification & acceptance in conformance with Annex K of system & system elements for all types of environment.
   1. The SE function shall ensure that analyses include design induced effects between system components or the system & its external environment & account for analysis uncertainties, in conformance with Annex K.
   1. The SE function shall establish the design & test factors & [margins applicable](ecss_mpsas.md) for design in conformance with Annex K. ((1) Design factors are factors applied to specified loads to ensure robustness of the design. (2) Test factors are factors applied to specified loads to demonstrate margins w.r.t. these loads (e.g. qualification / acceptance factors).)
   1. The SE function shall establish test environment conditions for product verification in test specifications.
1. **Trade‑off analyses**
   1. The SE function shall conduct or consolidate trade‑off analyses to:
      1. assist in selecting system concepts, designs & solutions (including people, parts & materials availability);
      1. support material selection & process decisions;
      1. support make‑or‑buy & supplier selection;
      1. examine alternative technologies to satisfy functional & design requirements;
      1. evaluate environmental & cost impacts of materials & processes;
      1. evaluate alternative physical architectures to select preferred products & processes;
      1. establish the system & its configuration items;
      1. analyze planning critical paths & propose alternatives;
      1. select standard components, techniques, services & facilities that reduce system life‑cycle cost;
      1. establish model & product verification philosophy for achieving qualification & acceptance objectives while considering testability;
      1. assess design capacity to evolve.
   1. The SE function shall evaluate alternative concepts, designs & solutions against each other in a Trade‑off report in conformance with Annex L.
   1. The SE function shall document alternative system concepts considered during system trade‑off studies in a System Concept Report in conformance with Annex C.
1. **Analysis methods, tools & models**
   1. The SE function shall define the analysis methods & tools to be used during the product life cycle, as well as the related models & data exchanges between the tools, & document these in the SEP in conformance with Annex D.
   1. The SE function shall ensure that analysis tools are validated.
   1. The SE function shall ensure that analysis tools are maintained.
   1. The SE function shall ensure that analysis tools are capable of exchanging & using models & data where agreed by customer & supplier. ((1) For exchange of models & data, see ECSS‑E‑TM‑10‑20 & ECSS‑E‑TM‑10‑21. (2) Exchange can be either direct or via interfaces.)
   1. The SE function shall ensure that analysis tools are capable of transferring models & data for multi‑disciplinary analysis where agreed by customer & supplier. ((1) Details on product data exchange & system modelling & simulation are provided in ECSS‑E‑TM‑10‑20 & ECSS‑E‑TM‑10‑21. (2) Exchange can be either direct or via interfaces.)
   1. The SE function shall ensure that models produced by analysis tools are validated based on documented procedures & results.
   1. The SE function shall ensure that modelling & test accuracy as well as limitations are considered (as part of Annex K) when establishing the performances & specifying environmental conditions for product verification.
   1. The SE function shall ensure that models are kept operational in accordance with the terms of the business agreement.


### 5.4. Design & configuration

**Design**

1. **General**
   1. The SE function shall establish a design of the system from its functional architecture, requirement allocation, & technology selection. (This includes definition of the interfaces & corresponding ICDs)
   1. The SE function shall ensure that the design addresses system aspects, covering its whole lifecycle, producing the physical architecture documented in conformance with Annex G & the product tree in conformance with ECSS‑M‑ST‑10 Annex B.
   1. The SE function shall take into account the outcome of the design & verification activities of the lower level products.
   1. The SE function shall ensure that the design covers hardware, software, & human‑in‑the‑loop (HITL).
   1. The SE function shall ensure that the design is supported by analyses consistent with the level of maturity of the design.
   1. The SE function shall coordinate with all entities for design data exchange. (This relates to coordination between the various functions given in Figure 4‑1)
1. **Technical budgets & margin policy**
   1. The SE function shall define, control & maintain all technical budgets of the system in conformance with Annex I in terms of target, current status & their trends.
   1. The SE function shall apportion & control budget requirements to all levels of system decomposition.
   1. The SE function shall define & apply the margin policy agreed between customer & supplier in compliance with Annex D.2.1.1.<4.2>b..5.. (The system margin policy is defined in the SEP.)
1. **Design methods, tools & models**
   1. The SE function shall define the design methods, tools & related models to be used during the product life cycle & document them in the SEP.
   1. The SE function shall ensure that design tools are validated & maintained.
   1. The SE function shall ensure that design tools are capable of exchanging & using design models & data where agreed by the customer & supplier.
   1. The SE function shall ensure that models  are kept operational in accordance with the business agreements.
   1. The SE function shall ensure that design models produced by design tools are validated.
   1. The SE function shall ensure that all design models are defined in accordance with the Coordinate System Document  with transformation methods as per ECSS‑E‑ST‑10‑09. (Coordinate system document is delivered as part of the SEP)
1. **Design files**
   1. The SE function shall establish & maintain a design definition file in conformance with Annex G.
   1. The SE function shall establish & maintain a design justification file in conformance with Annex K.
   1. The SE function shall establish & maintain a product user’s manual (PUM) or user’s manual (UM) in conformance with Annex P. (In case the product considered is a space segment, the Space Segment User Manual defined in ECSS‑E‑ST‑70 Annex E is generated with support of the SE function)

**Configuration**

1. **Configuration content**
   1. The SE function shall ensure that the configuration includes the system functional, physical & software characteristics, budgets, and, internal & external interfaces.
   1. The SE function shall ensure that the configuration includes lower decomposition levels.
   1. The SE function shall ensure that the configuration is documented in the DDF & in configuration definition documents. (Details on configuration definition documents are provided in ECSS‑M‑ST‑40)
1. **Configuration baselines**
   1. The SE function shall establish the system configuration baselines to be placed under control at defined project milestones. (Details on system configuration baselines are provided in ECSS‑M‑ST‑40)
1. **Configuration assembly constraints**
   1. The SE function shall define & document the hierarchy & assembly sequence of the system elements in line with the physical architecture.
   1. [deleted & incorporated in 5.4.2.3a..].



### 5.5. Verification

1. **General**
   1. The SE function shall define the Verification Plan in accordance with ECSS‑E‑ST‑10‑02 Annex A & coordinate the product verification accordingly.
1. **Product verification**
   1. [deleted, covered by 5.5.2.1c..].
   1. The SE function shall assign the product to a verification product category, as per ECSS‑E‑ST‑10‑02 Table 5‑1.
   1. The SE function shall specify the configuration & environment conditions for product verification & the criteria for its qualification & acceptance.
   1. The SE function shall confirm that all product verification objectives are achieved by analysing the results of the verification activities. (This includes analysis of Verification Control Document & its closeout documents, as per ECSS‑E‑ST‑10‑02 Annex B.)
   1. [deleted]
   1. The SE function shall ensure that the verification covers the complete product including hardware, software, human‑in‑the‑loop (HITL), operations & representative mission scenarios (including pre‑launch, launch & early orbit, in‑orbit, post‑landing, or other mission scenario). ((1) The testing performed during & at the end of the integration of a system is defined as System Functional Test (SFT). (2) For system composed of different segments (e.g. space segment, ground segment), the testing performed to ensure operability & functionality of the complete system is defined as System Validation Test (SVT). (3) System validation is performed against user needs & is typically performed under customer authority, with supplier involvement as detailed in the business agreement.)



### 5.6 System engineering integration & control

1. **Management of SE activities**
   1. The SE function  shall implement the SEP including quantification of this effort as input to management.
   1. [deleted]
   1. [deleted]
   1. [deleted]
   1. [deleted, covered by 5.6.1.1a..].
   1. The SE function shall ensure that all engineering changes, dispositions & decisions are provided to project configuration control in accordance with the Configuration Management Plan.
   1. The SE function shall ensure that the experience gained in past & in parallel activities is systematically considered & support project management in the collection of experience gained for future use.
1. **Planning**
   1. The SE function shall ensure that the SEP is consistent with the project schedule. (Details on the project schedule content are provided in ECSS‑M‑ST‑60, in particular the DRD Ann.B)
1. **Engineering data**
   1. The SE function shall define the engineering data to be stored in a data repository.
   1. The SE function shall ensure that engineering data can be exchanged in electronic form between the different organizations in charge of the elements of the product decomposition levels via agreed & validated interfaces.
   1. The SE function shall ensure the availability of engineering data to meet the schedule which enable the system to be designed, produced, tested, delivered, operated, maintained, & disposed of.
1. **Interface control**
   1. The SE function shall ensure external & internal interfaces  are controlled in conformance with ECSS‑E‑ST‑10‑24. ((1) The control of the external interfaces is performed in cooperation with the parties involved in the interface. (2) Interface requirements are rolled‑out of the technical specification as interface requirements documents)
   1. [deleted]
   1. [deleted].
1. **Coordinate systems & units**
   1. The SE function shall define the coordinate systems & related coordinate units  in the Coordinate System Document conforming to DRD ECSS‑E‑ST‑10‑09 Annex A.
   1. The SE function shall define the units system to be used during the product life cycle & document it in the SEP. (For example; the unit system being selected as 「metric」)
1. **Technical budgets & margin policy**
   1. [deleted, covered by clause 5.4.1.2].
   1. [deleted, covered by clause 5.4.1.2]
1. **Technology**
   1. The SE function shall identify candidate technologies, & document them in the Technology Matrix in conformance with Annex F.
   1. The technologies proposed shall be assessed & verified in terms of TRL levels, defined in ECSS‑E‑AS‑11, & documented in the Technology Plan in conformance with Annex E. (For assessing & verifying technologies, the process described in ECSS‑E‑HB‑11 「TRL Handbook」 can be used)
   1. The SE function shall demonstrate supportability & feasibility within the defined supplier’s cost & schedule constraints.
   1. The Technology Readiness Status List (TRSL) shall be completed & provided to the customer for Phase 0 in conformance with Annex E.2.1.1.<5>.
   1. The Technology Readiness Status List (TRSL) of the programme technologies shall be maintained & provided to the customer at Phase A & B project reviews
   1. The TRSL specified in Annex E.2.1.1.<5> shall be developed by the SE team with the support of the PA manager.
   1. The items identified in the TRSL shall be assessed at Phase B for inclusion & tracking in the Critical Items List. (The TRSL & the processes of transferring technologies to the Critical Item List is captured in ECSS‑E‑HB‑11)
1. **Risk management**
   1. The SE function shall contribute to the identification of risks & mitigation measures. (Details on risk management are provided in ECSS‑M‑ST‑80)
   1. [deleted, covered in 5.4.1.1f..].
   1. The SE function shall implement & control the content of the risk management plan which is within SE responsibility.
1. **Changes & nonconformances control**
   1. The SE function shall provide a technical assessment on any change proposal to the baseline of the product.
   1. The SE function shall provide a technical assessment on any nonconformance to the status of the product. (Nonconformance treatment is described by ECSS‑Q‑ST‑10‑09)
   1. The SE function shall implement & control agreed actions assigned to it, arising from change proposals & nonconformances. ((1) Change is related to a request for deviation. (2) Nonconformance is related to a request for waiver. (3) The change procedure/control is defined as part of the configuration management as per ECSS‑M‑ST‑40.)



## 6. [deleted]
& added with modifications as new clause 4.3



## 7. Pre‑tailoring matrix per product types

The Matrix of Table 7‑2 presents the pre‑tailoring of this ECSS Standard per space product type.

For the terminology & definitions of the space product types see ECSS‑S‑ST‑00‑01. (「Ground segment equipment」 is not to be confused with 「Ground support equipment」.)

ECSS‑E‑ST‑10 addresses System Engineering processes to be followed throughout the whole Space System development. This includes, along with Space Segment, also Ground Segment & Operations.

Yet, specific standards detail the Engineering process both for SW & for Ground Segment & Operations. The following standards are considered fully sufficient for development of these items:

1. ECSS‑E‑ST‑70 Space engineering ‑ Ground systems & operations
1. ECSS‑E‑ST‑40 Space engineering ‑ Software
1. ECSS‑Q‑ST‑80 Space product assurance ‑ Software product assurance

Thus, in the above applicability table, the columns for SW & Ground are stated as N/A, i.e. 「—」.

【**Table 7‑1:** Definitions of the columns of Table 7‑2】

1. **Applicability status:**
   1. ✘ — applicable
   1. — — N/A
   1. ░ — pre‑tailoring applicability not definable ‑ to be determined during tailoring
   1. » — the requirement is applicable to a lower product type. Responsibility of tailoring (if needed) resides with the customer of this lower product type
   1. ✘# — requirement is applicable except in a specific case ‑ the criteria for being 「N/A」 are defined in the Comments column
   1. ░# — pre‑tailoring applicability not definable – however supplementary indications regarding applicability in the tailoring are given in the Comments column
   1. 「#」 is a number to uniquely identify every comment in the same row.
   1. A requirement is considered applicable for a product type if it is verified on this product type.
1. **Comments.** The column 「Comments」 provides information on the limitation of applicability – it provides clarification on the limited & specific conditions for the applicability of the requirement, is not used to modify a requirement.

【**Table 7‑2:** Pre‑tailoring matrix per 「Space product types」】

<small>

|**ECSS req.#**|**Space system**|**Space segm. element & sub‑system**|**Space segm. equip.**|**Launch segm. element & sub‑system**|**Launch segm. equip.**|**Ground segm. element & sub‑system**|**Ground segm. equip.**|**Ground support equip.**|**SW**|**Comments**|
|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|
|5.1.1.1a|✘|✘1|░2|░2|—|—|—|—|—|(1) ✘ at element lvl: for subsys. lvl ‑ see 2.<br> (2) applicability should be defined/tailored at each level for next lower level, depending on product heritage, engineering complexity & industrialization context|
|5.1.1.1c|✘|✘1|—|░2|—|—|—|—|—|(1) ✘ at element lvl: for subsys. lvl ‑ see 2<br> (2) applicability should be defined/tailored at each level for next lower level, depending on product heritage, engineering complexity & industrialization context|
|5.1.1.1d|✘|✘1|░2|░2|—|—|—|—|—|(1) ✘ at element lvl: for subsys. lvl ‑ see 2<br> (2) applicability should be defined/tailored at each level for next lower level, depending on product heritage, engineering complexity & industrialization context|
|5.2.1.1a|✘|✘|✘|✘|—|—|—|—|—| |
|5.2.1.1b|✘|✘|✘1|✘|—|—|—|—|—|(1) N/A in case of no lower level elements in scope of engineering responsibility of supplier|
|5.2.1.1c|✘|✘|✘1|✘|—|—|—|—|—|(1) N/A in case of no lower level elements in scope of responsibility of supplier|
|5.2.1.1d|✘|░1|░1|░1|—|—|—|—|—|(1) depending on the applicability of ECSS‑E‑ST‑10‑06 clause 8|
|5.2.1.1e|✘|✘|░1|✘|░1|—|—|—|—|(1) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context.|
|5.2.2.1a|✘|✘|✘1|✘|—|—|—|—|—|(1) 5.2.2.1.11. N/A in the case of upper level traceability provided by the upper layer; 5.2.2.1.12. N/A in the case of no lower level requirements|
|5.2.2.1b|✘|✘|✘1|✘|—|—|—|—|—|(1) N/A in case of no lower level components in scope of engineering responsibility of supplier|
|5.2.2.1c|✘|✘|✘|✘|—|—|—|—|—| |
|5.2.3.1a|✘|✘|✘1|✘|—|—|—|—|—|(1) N/A in case of no lower level elements in scope of engineering responsibility of supplier|
|5.2.3.1b|✘|✘|✘1|░|—|—|—|—|—|(1) ✘ for new specifications developed by the equipment supplier in direct response to higher level ones in the contractual chain where the ECSS‑E‑ST‑10‑06 & Annex A are applicable. N/A where existing specifications developed from a different heritage or modifications thereof are deemed adequate|
|5.2.3.1c|✘|✘|✘1|✘|✘1|—|—|—|—|(1) N/A in case of no lower level elements in scope of engineering responsibility of supplier|
|5.2.3.2a|✘|✘|░1|✘|░1|—|—|—|—|(1) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|5.2.3.2b|✘|✘|░1|✘|░1|—|—|—|—|(1) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|5.2.3.3b|✘|✘|░1|✘|░1|—|—|—|—|(1) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|5.2.3.3c|✘|✘|░1|✘|░1|—|—|—|—|(1) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|5.2.3.4a|✘|✘|░1|✘|░1|—|—|—|—|(1) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|5.2.3.4b|✘|✘|░1|✘|░1|—|—|—|—|(1) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|5.2.3.5a|✘|✘|✘1|░|░2|—|—|—|—|(1) N/A in case of no lower level elements in scope of engineering responsibility of supplier<br> (2) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|5.2.3.7a|✘|░|—|░|—| | |—|—| |
|5.2.3.8a|✘|✘|░1|✘|░1|—|—|—|—|(1) lower levels specifications implementation, N/A in case of no lower level elements in scope of engineering responsibility of supplier|
|5.2.3.9a|✘|✘|░1|✘|░1|—|—|—|—|(1) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context. ECSS‑M‑ST‑40 is sufficient|
|5.3.1.1a|✘|—|—|░1|—|—|—|—|—|(1) ✘ at element lvl, for subsys. lvl ‑ & applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|5.3.1.1b|✘|✘1|░2|✘1|░2|—|—|—|—|(1) ✘ at element lvl,: for subsys. lvl ‑ see (2)<br> (2) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|5.3.1.1c|✘|✘|░2|✘1|░2|—|—|—|—|(1) ✘ at element lvl, for subsys. lvl ‑ see (2)<br> (2) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|5.3.1.1d|✘|✘|░2|✘1|░2|—|—|—|—|(1) ✘ at element lvl, for subsys. lvl ‑ see (2)<br> (2) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|5.3.1.1e|✘|░1|░1|░1|░1|—|—|—|—|(1) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|5.3.1.1f|✘|✘|░2|✘1|░2|—|—|—|—|(1) ✘ at element lvl, for subsys. lvl ‑ see (2)<br> (2) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|5.3.1.1g|✘|✘|░2|✘1|░2|—|—|—|—|(1) ✘ at element lvl, // for subsys. lvl‑ see (2)<br> (2) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|5.3.1.1h|✘|✘|░2|✘1|░2|—|—|—|—|(1) ✘ at element lvl, for subsys. lvl ‑ see (2)<br> (2) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|5.3.1.1i|✘|░1|—|░1|—|—|—|—|—|(1) ✘ at element lvl, for subsys. lvl ‑ & applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|5.3.1.1j|✘1|✘1|✘1|✘|—|—|—|—|—|(1) Depending on the scope of the analysis other more detailed standards are used in place of Annex Q|
|5.3.2.1a|✘|✘|░2|✘1|—|—|—|—|—|(1) ✘ at element lvl, // for subsys. lvl ‑ see 2 <br> (2) defined by the upper level & made applicable directly, applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|5.3.2.1b|✘|✘|░1|░|—|—|—|—|—|(1) defined by the upper level & made applicable directly, applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|5.3.2.1c|✘1|✘1|✘1|░|—|—|—|—|—|(1) depending on the scope of the analysis other more detailed standards are used in place of Ann.K|
|5.3.2.1d|✘|✘|░1|░|—|—|—|—|—|(1) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|5.3.2.1e|✘|✘|░2|✘1|—|—|—|—|—|(1) ✘ at element lvl, for subsys. lvl ‑ see (2)<br> (2) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|5.3.3.1a|✘|✘|░2|✘1|—|—|—|—|—|(1) ✘ at element lvl, for subsys. lvl ‑ see (2)<br> (2) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|5.3.3.1b|✘|✘|░2|✘1|░2|—|—|—|—|(1) ✘ at element lvl, for subsys. lvl – see (2)<br> (2) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|5.3.3.1c|✘|✘|—|░1|—|—|—|—|—|(1) ✘ at element lvl, for subsys. lvl ‑ & applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|5.3.4.1a|✘|░1|░1|░1|—|—|—|—|—|(1) applicability should be defined/tailored at each level for next lower level, depending on product heritage, engineering complexity & industrialization context|
|5.3.4.1b|✘|░1|░1|░1|—|—|—|—|—|(1) during tailoring, consider whether other more detailed standards already cover this point for specific domains|
|5.3.4.1c|✘|░1|░1|░1|—|—|—|—|—|(1) during tailoring, consider the specific need for maintenance & whether other more detailed standards already cover this point for specific domains|
|5.3.4.1d|✘|░1|░1|░1|—|—|—|—|—|(1) during tailoring, consider both the specific need for exchange & whether other more detailed standards already cover this point for specific domains|
|5.3.4.1e|✘|░1|░1|░1|—|—|—|—|—|(1) during tailoring, consider both the specific need for exchange & whether other more detailed standards already cover this point for specific domains|
|5.3.4.1f|✘|░1|░1|░1|—|—|—|—|—|(1) during tailoring, consider whether other more detailed standards already cover this point for specific domains|
|5.3.4.1g|✘|✘|░2|✘1|—|—|—|—|—|(1) ✘ at element lvl, for subsys. lvl ‑ see (2)<br> (2) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|5.3.4.1h|✘|✘|░2|✘1|—|—|—|—|—|(1) ✘ at element lvl, for subsys. lvl ‑ see (2)<br> (2) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|5.4.1.1a|✘|✘|░2|✘1|—|—|—|—|—|(1) ✘ at element lvl, for subsys. lvl ‑ see (2)<br> (2) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|5.4.1.1b|✘|✘|░2|✘1|░2|—|—|—|—|(1) ✘ at element lvl, for subsys. lvl ‑ see (2)<br> (2) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|5.4.1.1c|✘|✘|✘1|✘|—|—|—|—|—|(1) N/A in case of no lower level elements in scope of engineering responsibility of supplier|
|5.4.1.1d|✘|░1|░1|░1|—|—|—|—|—|(1) during tailoring, consider whether other more detailed standards already cover this point for specific domains|
|5.4.1.1e|✘|░1|░1|░1|—|—|—|—|—|(1) during tailoring, consider whether other more detailed standards already cover this point for specific domains|
|5.4.1.1f|✘|✘|░2|✘1|—|—|—|—|—|(1) ✘ at element lvl, for subsys. lvl ‑ see (2)<br> (2) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|5.4.1.2a|✘|✘|✘|✘|—|—|—|—|—| |
|5.4.1.2b|✘|✘|✘1|░|—|—|—|—|—|(1) N/A in case of no lower level elements in scope of engineering responsibility of supplier|
|5.4.1.2c|✘|✘|✘|✘|—|—|—|—|—| |
|5.4.1.3a|✘|✘1|░2|░2|—|—|—|—|—|(1) ✘ at element lvl, for subsys. lvl ‑ see (2)<br> (2) applicability should be defined/tailored at each level for next lower level, depending on product heritage, engineering complexity & industrialization context|
|5.4.1.3b|✘|░1|░1|░1|—|—|—|—|—|(1) during tailoring, consider the specific need & whether other more detailed standards already cover this point for specific domains|
|5.4.1.3c|✘|░1|░1|░1|—|—|—|—|—|(1) during tailoring, consider both the specific need for exchange & whether other more detailed standards already cover this point for specific domains|
|5.4.1.3d|✘|✘1|░2|░|—|—|—|—|—|(1) ✘ at element lvl, for subsys. lvl ‑ see (2)<br> (2) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|5.4.1.3e|✘|░1|░1|░1|—|—|—|—|—|(1) during tailoring, consider the specific need & whether other more detailed standards already cover this point for specific domains|
|5.4.1.3f|✘|✘1|░2|░|—|—|—|—|—|(1) ✘ at element lvl, for subsys. lvl ‑ see (2)<br> (2) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|5.4.1.4a|✘|✘|░1|░|—|—|—|—|—|(1) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|5.4.1.4b|✘|✘|░1|░|—|—|—|—|—|(1) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|5.4.1.4c.|—|✘|✘1|░|—|—|—|—|—|(1) during tailoring, Ann.P contents are  also tailored to fit the need|
|5.4.2.1a|✘|✘|░1|✘|—|—|—|—|—|(1) during tailoring, other configuration management standards called by the business agreement when considered fully sufficient to suppress the applicability at this level|
|5.4.2.1b|✘|✘|✘1|✘|—|—|—|—|—|(1) N/A in case of no lower level elements in scope of engineering responsibility of supplier|
|5.4.2.1c|✘|✘|░1|✘|—|—|—|—|—|(1) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|5.4.2.2a|✘|✘|░1|✘|—|—|—|—|—|(1) during tailoring, other configuration management standards called by the business agreement when considered fully sufficient to suppress the applicability at this level|
|5.4.2.3a|✘|✘|░1|✘|—|—|—|—|—|(1) during tailoring, other configuration management standards called by the business agreement when considered fully sufficient to suppress the applicability at this level|
|5.5.1.1a|✘|✘|✘|░|—|—|—|—|—| |
|5.5.2.1b|✘|✘|✘|░|—|—|—|—|—| |
|5.5.2.1c|✘|✘1|░2|░2|—|—|—|—|—|(1) ✘ at element lvl, for subsys. lvl ‑ see (2)<br> (2) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|5.5.2.1d|✘|✘|✘|✘|—|—|—|—|—| |
|5.5.2.1f|✘|✘1|░2|░2|—|—|—|—|—|(1) ✘ at element lvl, for subsys. lvl ‑ see (2)<br> (2) applicability can be suppressed where adequately covered by other requirements e.g. ECSS‑E‑ST‑10‑03 & the TS & the SOW|
|5.6.1.1a|✘|✘1|░2|░2|—|—|—|—|—|(1) ✘ at element lvl, for subsys. lvl ‑ see (2)<br> (2) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|5.6.1.1f|✘|✘|░2|✘1|—|—|—|—|—|(1) ✘ at element lvl,  for subsys. lvl ‑ see (2)<br> (2) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|5.6.1.1g|✘|✘|░2|✘1|—|—|—|—|—|(1) ✘ at element lvl,  for subsys. lvl ‑ see (2)<br> (2) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|5.6.2.1a|✘|✘1|░2|░2|—|—|—|—|—|(1) ✘ at element lvl, for subsys. lvl ‑ see (2)<br> (2) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|5.6.3.1a|✘|✘1|░2|░2|—|—|—|—|—|(1) ✘ at element lvl, for subsys. lvl ‑ see (2)<br> (2) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|5.6.3.1b|✘|✘1|░2|░2|—|—|—|—|—|(1) ✘ at element lvl, for subsys. lvl ‑ see (2)<br> (2) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|5.6.3.1c|✘|✘1|░2|░2|—|—|—|—|—|(1) ✘ at element lvl, for subsys. lvl ‑ see (2)<br> (2) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|5.6.4.1a|✘|✘|✘1|✘|—|—|—|—|—|(1) for ECSS‑E‑ST‑10‑06 Ann.A also see pre‑tailoring of 5.2.3.1b|
|5.6.5.1a|✘|✘1|░2|░2|—|—|—|—|—|(1) ✘ at element lvl, for subsys. lvl ‑ see (2)<br> (2) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|5.6.5.1b|✘|✘1|░2|░2|—|—|—|—|—|(1)applicable at element lvl, for subsys. lvl ‑ see (2)<br> (2) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|5.6.7.1a|✘|✘|✘|✘|✘|—|—|—|—| |
|5.6.7.1b|✘|✘|✘|✘|✘|—|—|—|—| |
|5.6.7.1c|✘|✘|✘|✘|✘|—|—|—|—| |
|5.6.7.1d|✘|✘|✘|✘|✘|—|—|—|—| |
|5.6.7.1e|✘|✘|✘|✘|✘|—|—|—|—| |
|5.6.7.1f|✘|✘|✘|✘|✘|—|—|—|—| |
|5.6.7.1g|✘|✘|✘|✘|✘|—|—|—|—| |
|5.6.8.1a|✘|✘|░1|✘|—|—|—|—|—|(1) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|5.6.8.1c|✘|✘1|░2|░2|—|—|—|—|—|(1) ✘ at element lvl, for subsys. lvl ‑ see (2)<br> (2) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|5.6.9.1a|✘|✘|░1|✘|—|—|—|—|—|(1) during tailoring, consider whether this requirement is superfluous to existing measures e.g. PA plan, RFD/RFW templates|
|5.6.9.1b|✘|✘|░1|✘|—|—|—|—|—|(1) during tailoring, consider whether this requirement is superfluous to existing measures e.g. PA plan, RFD/RFW templates|
|5.6.9.1c|✘|✘|░1|✘|—|—|—|—|—|(1) during tailoring, consider whether this requirement is superfluous to existing measures e.g. PA plan, RFD/RFW templates|
|Ann.B|✘|—|—|—|—|—|—|—|—| |
|Ann.C|✘|░1|—|░1|—|—|—|—|—|(1) ✘ at element lvl, for subsys. lvl ‑ & applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|Ann.D|✘|░1|░1|░1|—|—|—|—|—|(1) applicability should be defined/tailored at each level for next lower level, depending on product heritage, engineering complexity & industrialization context|
|Ann.E|✘|✘|░1|✘|░1|—|—|—|—|(1) depending on product heritage, engineering complexity & industrialization context|
|Ann.F|✘|✘|░1|✘|░1|—|—|—|—|(1) depending on product heritage, engineering complexity & industrialization context|
|Ann.G|✘|✘|░2|✘1|░1|—|—|—|—|(1) ✘ at element lvl,  for subsys. lvl ‑ see (2)<br> (2) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|Ann.H|✘|✘1|░2|✘1|░2|—|—|—|—|(1) ✘ at element lvl,  for subsys. lvl ‑ see (2)<br> (2) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|Ann.I|✘|✘|✘|✘|—|—|—|—|—| |
|Ann.J|✘|✘|✘1|✘|✘1|—|—|—|—|(1) N/A in case of no lower level elements in scope of engineering responsibility of supplier|
|Ann.K|✘|✘|░2|✘1|░2|—|—|—|—|(1) ✘ at element lvl,  for subsys. lvl ‑ see (2)<br> (2) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|Ann.L|✘|✘|░2|✘1|░2|—|—|—|—|(1) ✘ at element lvl,  for subsys. lvl ‑ see (2)<br> (2) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|Ann.N|✘|✘|✘1|✘|—|—|—|—|—|(1) N/A in case of no lower level components in scope of engineering responsibility of supplier|
|Ann.O|✘|✘|░1|✘|—|—|—|—|—|(1) applicability defined/tailored at each level for next lower level depending on product heritage, engineering complexity & industrialisation context|
|Ann.P|—|✘|░|✘|—|—|—|—|—| |

</small>


## Annex A (informative) System engineering documents delivery per review

Scope of the Table A‑1 is to indicate the relationship of documents associated to engineering activities which support project review objectives as specified in [ECSS‑M‑ST‑10](ecss_mst10.md). This table constitutes a first indication for the data package content at various reviews. The full content of such data package is established as part of the business agreement, which also defines the delivery of the document between reviews.

The table lists the documents generated by the engineering organization necessary for the project reviews (identified by 「✘」), except for verification documents, which are identified in Table G‑1 of ECSS‑E‑ST‑10‑02. The various crosses in a row indicate the expected progressive increased levels of maturity versus reviews. The last cross in a row indicates that at that review the document is expected to be completed & finalized. For the SEP, DDF & DJF, the line entries of these documents which are not identified by a 「✘」 in the table are not expected to be delivered for the quoted review.

All documents, even when not marked as deliverables in Table A‑1, are expected to be available & maintained under configuration management as per ECSS‑M‑ST‑40 (e.g. to allow for backtracking in case of changes).

【**Table A‑1:** System engineering deliverable documents】  
【Note (1): Document limited to the verification matrix】

|**#**|Phase →|**0**|**a**|**B**|**B**|**c**|**D**|**D**|**e**|**e**|**e**|**e**|**e**|**F**| |
|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|
| |**Document ↓**|**M**|**P**|**S**|**P**|**C**|**Q**|**A**|**O**|**F**|**L**|**C**|**E**|**M**|**DRD Ref.**|
| | |**D**|**R**|**R**|**D**|**D**|**R**|**R**|**R**|**R**|**R**|**R**|**L**|**C**| |
| | |**R**|**R**|**R**|**R**|**R**| | |**R**|**R**|**R**|**R**|**R**|**R**| |
|1|**Mission description document**|✘|✘| | | | | | | | | | | |ECSS‑E‑ST‑10 Ann.B|
|2|**Preliminary technical requirements specification**|✘|✘| | | | | | | | | | | |[ECSS‑E‑ST‑10‑06](ecss_est1006.md) Ann.A|
|3|Technical requirements specification| | |✘| | | | | | | | | | |[ECSS‑E‑ST‑10‑06](ecss_est1006.md) Ann.A|
|4|Interface requirements document| |✘|✘|✘| | | | | | | | | |[ECSS‑E‑ST‑10‑24](ecss_est1024.md) Ann.A|
|5|**System engineering plan**|✘|✘|✘|✘|✘|✘|✘| | | | | | |ECSS‑E‑ST‑10 Ann.D|
|6|Technology plan| |✘|✘|✘| | | | | | | | | |ECSS‑E‑ST‑10 Ann.E|
|7|Technology readiness status list|✘|✘|✘|✘| | | | | | | | | |ECSS‑E‑ST‑10 Ann.E|
|8|Technology matrix| |✘|✘|✘| | | | | | | | | |ECSS‑E‑ST‑10 Ann.F|
|9|Verification plan| |✘|✘|✘|✘|✘|✘| | | | | | |[ECSS‑E‑ST‑10‑02](ecss_est1002.md) Ann.A|
|10|AIT QM/FM plan| | | |✘|✘|✘|✘| | | | | | |[ECSS‑E‑ST‑10‑03](ecss_est1003.md) Ann.A|
|11|Space debris mitigation plan|✘|✘|✘|✘|✘|✘|✘|✘|✘|✘| |✘|✘|[ECSS‑U‑AS‑10](ecss_uas10.md)|
|12|Other related plans (from ECSS‑E‑ST‑10 Ann.D)| |✘|✘|✘|✘|✘|✘| | | | | | |ECSS‑E‑ST‑10 Ann.D|
|13|Coordinate system document| |✘|✘|✘|✘|✘| | | | | | | |[ECSS‑E‑ST‑10‑09](ecss_est1009.md) Ann.A|
|14|**Design definition file**| |✘|✘|✘|✘|✘| | | | | | | |ECSS‑E‑ST‑10 Ann.G|
|15|Function tree| |✘|✘|✘| | | | | | | | | |ECSS‑E‑ST‑10 Ann.H|
|16|Product tree| |✘|✘|✘| | | | | | | | | |[ECSS‑M‑ST‑10](ecss_mst10.md) Ann.B|
|17|Specification tree| | |✘|✘| | | | | | | | | |ECSS‑E‑ST‑10 Ann.J|
|18|Technical budget| |✘|✘|✘|✘|✘|✘| | | | | | |ECSS‑E‑ST‑10 Ann.I|
|19|Preliminary tech.req.specs for next lower level| |✘|✘| | | | | | | | | | |ECSS‑E‑ST‑10‑06 Ann.A|
|20|Technical req.specifications for next lower level| | |✘|✘| | | | | | | | | |ECSS‑E‑ST‑10‑06 Ann.A|
|21|Design definition file for next lower level| | | |✘|✘|✘|✘| | | | | | |ECSS‑E‑ST‑10 Ann.G|
|22|Interface control document| | |✘|✘|✘|✘|✘|✘|✘|✘| | | |[ECSS‑E‑ST‑10‑24](ecss_est1024.md) Ann.B|
|23|Product user manual / User Manual| | | | |✘|✘|✘|✘|✘|✘|✘|✘|✘|ECSS‑E‑ST‑10 Ann.P|
|24|**Design justification file**| |✘|✘|✘|✘|✘| | | | | | | |ECSS‑E‑ST‑10 Ann.K|
|25|Req. traceability matrix w.r.t. next lower level| |✘|✘|✘| | | | | | | | | |ECSS‑E‑ST‑10 Ann.N|
|26|Requirement justification file|✘|✘|✘|✘| | | | | | | | | |ECSS‑E‑ST‑10 Ann.O|
|27|System concept report|✘|✘| | | | | | | | | | | |ECSS‑E‑ST‑10 Ann.C|
|28|Trade‑off reports|✘|✘|✘|✘|✘| | | | | | | | |ECSS‑E‑ST‑10 Ann.L|
|29|Verification control document| |1|1|1|✘|✘|✘|✘|✘|✘|✘|✘|✘|[ECSS‑E‑ST‑10‑02](ecss_est1002.md) Ann.B|
|30|Test specification| | | | |✘|✘|✘|✘|✘|✘|✘|✘|✘|[ECSS‑E‑ST‑10‑03](ecss_est1003.md) Ann.B|
|31|Mathematical model description| | |✘|✘|✘|✘| | | | | | | |[ECSS‑E‑ST‑32](ecss_est32.md) Ann.I|
|32|Correlation report| | | | |✘|✘| | | | | | | |[ECSS‑E‑ST‑31](ecss_est31.md) Ann.C|
|33|Test procedure| | | | |✘|✘|✘|✘|✘| | | | |[ECSS‑E‑ST‑10‑03](ecss_est1003.md) Ann.C|
|34|Test report| | | | |✘|✘|✘|✘|✘|✘|✘|✘|✘|[ECSS‑E‑ST‑10‑02](ecss_est1002.md) Ann.C|
|35|Verification report| | | | |✘|✘|✘|✘|✘|✘|✘|✘|✘|[ECSS‑E‑ST‑10‑02](ecss_est1002.md) Ann.F|
|36|Design justification file for next lower level| | | | |✘|✘|✘| | | | | | |ECSS‑E‑ST‑10 Ann.K|
|37|Review of design report| | | | |✘|✘| | | | | | | |[ECSS‑E‑ST‑10‑02](ecss_est1002.md) Ann.D|
|38|Inspection report| | | | |✘|✘|✘| | | | | | |[ECSS‑E‑ST‑10‑02](ecss_est1002.md) Ann.E|
|39|GSE specifications| | | |✘|✘|✘|✘| | | | | | |—|
|40|GSE Data packages| | | | |✘|✘|✘| | | | | | |—|
|41|**Other documents**| | | | | | | | | | | | | |—|



## Annex B (normative) Mission description document (MDD) — DRD

**B.1. DRD identification.** *Requirement ID & source* — ECSS‑E‑ST‑10, requirement 5.3.1a. *Purpose & objective* — to provide input for the later selection of the best concept meeting the mission statement (MS) in iteration with the preparation of the preliminary technical requirements specification (TS) (as per ECSS‑E‑ST‑10‑06 Ann.A). It’s prepared in Phase 0/A for each possible concept, as per requirement 5.3.1a. Links & chronology amongst the MS, TS, MDD, SE plan, project management plan & system concept report are provided on the Figure B‑1. The MDD is produced by the top‑level customer (as described in ECSS‑S‑ST‑00 clause 6.1 & figure 6‑1) (typically an Agency or other institutional actors) & defines a concept that aims at satisfying the preliminary technical requirements specification, & presents how the objectives, operation profile, major system events & capabilities, contingencies & performance standards are expected to be achieved. For each mission concept, the MDD is a complete description of that concept. And to each MDD a SEP evaluating the related SE effort & a report evaluating the related programmatic aspect are associated. The system concept report assesses the different concepts from a technical, programmatic & risk point of view, includes a trade‑off including weighting factors which bears some management aspects, followed by a system concept selection.

【**Figure B‑1:** Relationship between documents】

1. Step 1: Analyse Mission statement
1. Step 2: Identify Preliminary Technical Requirements
1. Step 3: Identify Mission Concepts
1. Step 4: Prepare for each Concept: MDD, System Engineering Plan, Project Management Plan
1. Step 5: Put the results of the above into the System Concept Report

**B.2. Expected response**

*Special remarks* — None.

*Scope & content:*

1. **Introduction.** The purpose, objective, content & the reason prompting its preparation (e.g. logic, organization, process or procedure).
1. **Applicable & reference documents.** The applicable & reference documents in support to the generation of the document, & include, as a minimum, the current preliminary technical requirements specification.
1. **Preliminary technical requirements specification summary.** A summary of the preliminary technical requirements specification objectives & list the design driving requirements, derived from the current initial specification.
1. **Concept description.**
   1. Overview of the concept
   1. Mission analysis
   1. System description, element by element. (Note. For example: For a spacecraft, its ground control segment, & a user segment, e.g.. Space Segment: Payload, Platform, Launch Vehicle, Orbit related aspects, On‑Board Data Handling, Reference Operation Scenarios / Observation characteristics, Operability / Autonomy Requirements. Ground Segment: Functional Requirements & Major Elements, Monitoring & Control Segment, Data Processing Segment. User segment: Functional Requirements & Major Elements, Monitoring requirements.)
   1. Description of how the system works in each mission phase. (For example: For a spacecraft, the following phase: Launch preparation, Launch & Early Orbit Phase, In Orbit Commissioning, Nominal Operations, Spacecraft Disposal.)
      1. Performance drivers
      1. Constraints
      1. Main events
      1. Operations scenarios
1. **Assessment of the performance.** Assessment against the current preliminary technical requirements specification requirements. Identification of non‑compliances, & their impact on the current preliminary technical requirements specification.
1. **Identification of risk areas.** A list of identified risk related to the concept, including as a minimum technology, contingencies handling, & programmatic aspects.
1. **Conclusion.** Summarize the strengths & weaknesses of the concept.



## Annex C (normative) System concept report (SCR) — DRD

**C.1. DRD identification.** *Requirement ID & source* — ECSS‑E‑ST‑10, requirement 5.3.3c. *Purpose & objective.* Describes the principal technical characteristics of alternative system concepts, relating to performance, architectures, driving technologies, interfaces, risk, their evaluation & classification, & later addresses the selected concept.

**C.2. Expected response**

*Special remarks* — None.

*Scope & content:*

1. The system concept report (SCR) shall be an instantiation of the trade‑off report at system level in Phase 0 & Phase A of a project, conforming to ECSS‑E‑ST‑10 Ann.L. (The SCR can be extended to Phase B where needed (e.g. late trade‑offs))
1. The SCR shall address all technical (e.g. engineering disciplines), programmatic & related aspects relevant to the system.
1. Where relevant, specific e.g. discipline trade‑off’s shall be performed, contributing to the system trade‑off, each one being reported in a document conforming to ECSS‑E‑ST‑10 Ann.L.



## Annex D (normative) System engineering plan (SEP) — DRD

**D.1. DRD identification.** *Requirement ID & source* — ECSS‑E‑ST‑10, requirements 5.1a & 5.3.4a. *Purpose & objective* — to define the approach, methods, procedures, resources & organization to co‑ordinate & manage all technical activities necessary to specify, design, verify, operate & maintain a system or product in conformance with the customer’s requirements. In particular the SEP is established to fulfil the major technical project objectives, taking into account the defined project phases & milestones (as per ECSS‑M‑ST‑10). The SEP covers the full project lifecycle according to the scope of the business agreement. It’s established for each item of the product tree (as per ECSS‑M‑ST‑10). It highlights the risks, the critical elements, the specified technologies, as well as potential commonalities, possibilities of reuse & standardization, & provides means for handling these issues. The SEP is an element of the project management plan (as per ECSS‑M‑ST‑10). It’s important to adapt the SEP content to the phase of the project, with more information on risk analysis & new technologies in early phases 0/A/B, & more information on verification & validation aspects in phases C/D.

**D.2. Expected response**

*Special remarks* — None.

*Scope & content:*

1. **Introduction.** The purpose, objective, content & the reason prompting its preparation (e.g. programme or project reference & phase).
1. **Applicable & reference documents.** The applicable & reference documents in support to the generation of the document. A list of the references to the following applicable documents:
   1. Business agreement
   1. Project management plan, as per ECSS‑M‑ST‑10 Ann.A
   1. Product assurance plan, as per ECSS‑Q‑ST‑10 Ann.A
   1. Configuration management plan, as per ECSS‑M‑ST‑40 Ann.A
   1. Production plan
   1. Mission operations plan, as per ECSS‑E‑ST‑70 Ann.G
   1. ILS plan
1. **Project overview**
   1. *Project objectives & constraints:*
      1. The project objective & the main elements that characterize the user’s need.
      1. The objective of the system or product as established by the TS (as per ECSS‑E‑ST‑10‑06 Ann.A).
      1. The main elements of the system architecture (i.e. first level elements of the architecture adopted for the system & identification of their reuse constraints).
      1. The principal characteristics of the project lifecycle & the incremental development of the system (e.g. successive versions, progressive implementation of the functions of the system).
      1. The main elements supporting the project lifecycle (e.g. ground support equipment, & facilities).
      1. The organizational constraints impacting SE activities (e.g. the external & internal industrial organization (e.g. contractors, partners, suppliers, own company) constraints).
      1. The list of the critical issues identified at the beginning of the project phase(s).
      1. The list of national & international regulations.
      1. The capacity for verification & validation of the product, taking into account the means available, e.g. for tests, analysis, or simulation.
   1. *Product evolution logic.* Detail the incremental development of the system:
      1. progressive implementation of system functionalities,
      1. identification of possible successive versions,
      1. objectives & strategy for the implementation of the successive versions.
   1. *Project phase(s), reviews & planning*
      1. Provide an implementation & schedule of the SE activities & identify for the considered phase(s), as a minimum: 1. the main project milestones driving the SE process, 2. the phase(s) of the project lifecycle & the main reviews in accordance to project management plan.
      1. Provide dates of milestones or the duration of phases & the critical path according to the project master schedule.
   1. *Procurement approach.* Describe the strategy for acquisition of the items of the system or products defined in the product tree (e.g. make or buy, product line, incremental development).
   1. *Initial critical issues.* List the critical issues identified at the beginning of the project phase(s) covered in the SEP (e.g. any specific issues, problems, critical subjects, which require dedicated attention, investigation, action & planning).
1. **System design approach**
   1. *System engineering inputs*
      1. List the driving inputs for the SE activities described & defined by the: 1. business agreement, 2. outputs from previous phase(s) or expected under the heading of activities which are not controlled within the context of this SEP (e.g. data provided by the customer, data coming from other projects, upstream or predevelopment studies, product lines), 3. project management plan, product assurance plan, risk management plan, & configuration & documentation management plans.
      1. List the external means & facilities (e.g. equipment, software, & premises) made available by the customer or by any other entity external to the supplier that is responsible of this SEP, and, for each identified mean or facility, identify the applicable interface requirements (e.g. interface control documentation) as well as the authority in charge of it.
      1. List the internal means & facilities (e.g. equipment, software, & premises) made available by the organization in charge of the development of the system or product.
      1. Contain the Coordinate System Document (as per ECSS‑E‑ST‑10‑09 Ann.A).
      1. Define the units system to be used in the project.
   1. *System engineering outputs*
      1. List the specified SE outputs as per ECSS‑E‑ST‑10 clause 6 for the specific project phase(s) covered in the SEP. (NOTE. An overview of document delivery is given in Ann.A.)
      1. Describe:
         1. The strategy for the SE activities in line with the guidelines addressed by the management plan. In particular, identifying intermediate technical events for each phase in compliance with the master program schedule.
         1. The system design activities, with their objectives & major outputs according to the phase.
         1. The major engineering activities for each intermediate technical events, showing their mutual interactions & their relationships with the principal milestones (i.e. internal or contractual) of the project.
         1. The model philosophy (as per ECSS‑E‑ST‑10‑02 clause 4.2.5) in terms of number & characterization of models, from system to the requested lower level, necessary to achieve a high confidence in the product verification.
         1. The margin policy according to project phase, product category & maturity level.
         1. the method(s) & process(es) considered for the engineering activities (e.g. concurrent engineering, value analysis, or iteration cycle),
         1. the interrelation between the different engineering disciplines & other project activities (e.g. production, quality assurance, & operations & logistics),
         1. the interaction with other actors (e.g. customer & suppliers),
         1. the consistency & coherency of simultaneous activities (e.g. performed in parallel),
         1. which & how, control activities are implemented,
         1. Assessment of potential COTS usage
      1. In the case of a system incremental evolution, describe the design strategy for the:
         1. development of the initial release of the product,
         1. development, the verification of subsequent releases & their deployment,
         1. introduction of new technologies,
         1. tools & methods used for analysis,
         1. control of the evolutions for each release.
   1. *System engineering team responsibilities & organization:*
      1. Definition of the entities participating in the SE activities & the corresponding functions according to the project management plan.
      1. Identification of key engineering roles & responsibilities (e.g. system engineers, disciplines engineers, & technical managers).
      1. Description of the co‑operative work amongst the different teams participating in the system design.
   1. *System engineering coordination.* Describe the external & internal coordination in line with the project management plan.
1. **Implementation & related plans**
   1. *System engineering tasks description*
      1. System engineering process description.
         1. Describe the SE process tailored to the specifics of the considered project, & identify all the SE tasks to be implemented from the starting conditions (e.g. kick‑off) to the closing event (e.g. review), their relationship, & their interfaces with other actors of the project, & identify & describe any existing iteration within the process.
         1. For each task, the input information & their origin, the document(s) delivered (i.e. expected output) & their destination, the SE function(s) performed & the contribution of other actors shall be identified.
      1. Engineering disciplines integration.
         1. Address the following activities that concern the different engineering disciplines, recalling the relevant applicable standards & ancillary dedicated plans that are considered integral part of this SEP.
         1. Define the process & control to be put in place to meet requirements for the thermal, structures, mechanisms, environmental control & life support, propulsion, pyrotechnics, mechanical parts, & materials functions & interfaces. (NOTE. These requirements refer to Mechanical engineering as per ECSS‑E‑ST‑3x series of standards)
         1. Define the process & control to be put in place to meet requirements for electrical & electronic engineering, covering all electrical & electronic aspects of the relevant space product, including functions such as power generation, storage, conversion & distribution, & optical, avionics & microwave domains, electromagnetic compatibility, & electrical interfaces. (NOTE. These requirements refer to Electrical & electronic engineering as per ECSS‑E‑ST‑20.)
         1. Define process & control to be put in place to meet requirements for software engineering, covering, amongst others, flight & ground software, checkout software & simulation software. (NOTE. These requirements refer to Software engineering as per ECSS‑E‑ST‑40.)
         1. Define process & control to be put in place to meet requirements for communication engineering, covering, amongst others, spacecraft‑to‑ground, spacecraft‑to‑spacecraft, ground‑to‑ground & on‑board communications links. ((1) These requirements refer for Communications engineering as per ECSS‑E‑ST‑50. (2) It includes aspects such link budgets, data management, RF, audio & video communications & protocols)
         1. Define process & control to be put in place to meet requirements for control engineering, covering, amongst others, AOCS, robotics, rendez‑vous & docking.
         1. Define the process & control to be put in place to meet requirements specifying natural environment for all space regimes (e.g. debris regulations, or planetary contamination protection) & general models & rules for determining the local induced environment. (NOTE. These requirements refer to Space environment as per ECSS‑E‑ST‑10‑04)
         1. Define the process & control to be put in place to meet requirements for the approach, methods, procedures, organization & resources to be implemented to ensure proper technical interfaces between SE & production.
         1. Define the process & control to be put in place to meet requirements of operations of the space segment, covering, amongst others: 1. mission operation definition & preparation, 2. mission & trajectory analysis, 3. operability analysis (e.g. autonomy, operational scenario, nominal & non‑nominal modes, failure detection isolation & recovery). (NOTE. These requirements refer to Operations engineering as per ECSS‑E‑ST‑70)
         1. Define the process & control to be put in place to meet requirements for ground & in‑orbit logistics & maintenance, addressing, amongst others, technical activities, related engineering standards, methods & analyses to be performed to ensure that the development of space systems (i.e. manned & unmanned) properly takes into account & integrates the supportability & support aspects for the whole life cycle.
         1. Define the process & control to be put in place to meet requirements for human activities & environments associated with space systems. (NOTE. These requirements refer to Human factors engineering as per ECSS‑E‑ST‑10‑11)
         1. Define process & control to be put in place to meet requirements for implementation of design selections relating to humans for any item with associated human interface, including computer based system & equipment.
      1. Work package. Define & describe the work package(s) for the relevant engineering tasks, which are maintained in the work breakdown structure.
   1. *Related plans.*
      1. When the SEP includes sub‑plans covering parts of SE activities, these sub‑plans shall be annexed to the SEP.
      1. Identify the other plans relevant to SE function activity belonging to the following categories:
         1. Programmatic plans (NOTE. Examples of programmatic plans are: the SEP plans of sub‑products constituting the system or product, Industrial procurement plan, risk management plan, off‑the‑shelf plan (see ECSS‑Q‑ST‑20‑10))
         1. Verification plans (NOTE. Examples of verification plans are: verification plan (VP), AIT plan, AIV plan & technology plan, system calibration plan, Security Aspects Verification Plan. Some of those DRDs are defined in this document, in ECSS‑E‑ST‑10‑02 or ECSS‑E‑ST‑10‑03. ・ VP & AIT plans can be integral parts of the SEP, or rolled out separately (without overlap), or combined as the AIV Plan which can also be rolled out separately. However, the existence of the AIV Plan excludes independent VP & AIT plans.)
         1. Engineering discipline plans (NOTE. Examples of engineering discipline plans are: Fracture Control Plan (see ECSS‑E‑ST‑32), Micro‑gravity Control Plan, Electro‑Magnetic Compatibility Plan (see ECSS‑E‑ST‑20), Audible Noise Control Plan, Radio Frequency Plan, Alignment Requirements & Control Plan, System Performance Simulations Plan, Software Development Plan, Orbital Debris Mitigation Plan & Disposal Plan (as per ISO 24113:2011), Planetary protection Plan, Cleanliness & Contamination Control Plan.)
         1. Operations plans (NOTE. Examples of operation plans are: launch site operations & logistics plan, system commissioning & operation support plan)
         1. (NOTE. Some of these plans can be integrated in the SEP in the early phases of a project.)
      1. Describe the constraints & the interactions impacting the SE activities derived from the analysis of the plans identified as relevant in D.2.1<5.2>b.
   1. *System engineering methods, tools & models*
      1. List & briefly describe the methods, tools, & data models that the SE team uses in performing their tasks.
      1. In relation to requirements traceability & demonstration of verification (compliance with requirements, VCD), the specific methods & tools shall be described (including interfaces to next lower level suppliers), & reuse of elements (e.g. COTS) identified.
   1. *Critical issues.* Describe any specific issues, problems requiring dedicated attention, investigations or actions during the current phase & identify risks, & risk mitigation measures.
1. **System engineering for the following phase(s).** Introduce the SE activities, to be conducted during subsequent phase(s) of the project, & as a minimum, list any identified critical issue & risk to be mitigated during the subsequent phase(s).



## Annex E (normative) Technology plan (TP) — DRD

**E.1. DRD identification.** *Requirement ID & source* — ECSS‑E‑ST‑10, requirement 5.6.7b. *Purpose & objective* — the objective of the **technology plan (TP)** is to define the approach, methods, procedures, resources & organization to evaluate the ability of a critical technology to meet the intended requirements. Also, the objective of this plan is to ensure effective preparation of the technologies necessary for a timely implementation of the system, in accordance to the requirements imposed by the specific characteristics of the relevant product. It’s established for each item of the function tree (as per ECSS‑E‑ST‑10 Ann.H), & highlights the technical requirements, & the critical technology of each item.

**E.2. Expected response**

*Special remarks.* The content of the TP **may be merged with the SEP** (as per ECSS‑E‑ST‑10 Ann.D). The TP shall introduce the related activities, to be conducted during all phase(s) of the project.

*Scope & content:*

1. **Introduction.** The purpose, objective, content & the reason prompting its preparation (e.g. programme or project reference & phase).
1. **Applicable & reference documents.** The applicable & reference documents in support to the generation of the document & include the reference to the following applicable documents: Function tree, Specification tree, System engineering plan, Technology matrix (as per ECSS‑E‑ST‑10 Ann.F)
1. **Project overview.** A summary of the main aspects of:
   1. project objectives & constraints (i.e. section 3.1 of ECSS‑E‑ST‑10 Ann.D SEP DRD);
   1. product evolution logic (i.e. section 3.2 of ECSS‑E‑ST‑10 Ann.D SEP DRD);
   1. project phase(s), reviews & planning (i.e. section 3.3 of ECSS‑E‑ST‑10 Ann.D SEP DRD),
   1. Procurement approach (i.e. section 3.4 of ECSS‑E‑ST‑10 Ann.D SEP DRD)
1. **Tasks description**
   1. *TP expected outputs.* The expected output shall be an answer concerning the possibility for using the identified or needed technology to perform a function.
   1. *TP inputs.* For each system function, the TP input shall be:
      1. technical requirements,
      1. the selected technology or technological element & its TRL,
      1. the list of the identified project risks & critical aspects,
      1. the schedule for Engineering activities.
   1. *TP tasks*
      1. Establish & describe the necessary activities to complete the acquisition of each technology or technological element, including verification strategies & methods, & the link to product assurance aspects.
      1. Define the model philosophy for each technology or technological element, based on an assessment on the maturity status & on the criticality of the technology with respect to functions' requirements.
      1. Describe the technology development activities, their required or possible interrelations & timings, as necessary for the satisfactory acquisition of the technologies & procurement of the technological elements.
      1. Identify technical milestones, showing their interactions & relationships with the SEP milestones.
   1. *Responsibilities & organization*
      1. definition of the entities participating in the engineering activities & the corresponding functions according to the SEP;
      1. identification of key engineering roles & responsibilities for each technology or technological element.
   1. *TP interfaces.* Describe the external & internal interfaces in conformance to the SEP.
1. **Technology issues.** Describe, for any identified technology risk & related critical aspects for the project, the specific actions taken for risk mitigation based on identified technology readiness level (TRL). Include the TRSL by using the template in Figure E‑1, & listing:
   1. the critical function, with reference to the Function tree,
   1. the name of the technology or element(s) implementing such a function,
   1. current declared & verified TRL, as per ECSS‑E‑AS‑11,
   1. reference to the **technology readiness assessment (TRA)** report confirming the declared TRL,
   1. date of the report,
   1. key points to support TRL declared in column [3],
   1. forward plan for TRL evaluation, indicating the target TRL, the phase or date at which such target TRL is expected, & status of the planning to achieve the target TRL,
   1. during phases A & B, indication whether or not technology/element is a candidate for the CIL.
   1. (NOTE. The key point in column [6] are normally few lines summarizing the TRL assessment report referenced in column [4]). (NOTE. At the end of Phase B, the TRSL is introduced as part of the CIL)

【**Figure E‑1:** TRSL template】

|**(1) Critical Function**|**(2) Technology / Element**|**(3) TRL**|**(4) TRA Report**|**(5) TRA Date**|**(6) Rationale for TRL evaluation**|**(7) Target TRL & Technology Plan Evolution**|**(8) TIL Candidate (Y/N)**|
|:--|:--|:--|:--|:--|:--|:--|:--|
|TBD| | | | | | | |



## Annex F (normative) Technology matrix — DRD

**F.1. DRD identification.** *Requirement ID & source* — ECSS‑E‑ST‑10, requirement 5.6.7.1a. *Purpose & objective* — the technology matrix presents, for each technical requirement/function, the list of technologies or technological elements, which have the potential to meet this requirement. It summarizes candidate technologies per individual requirement. It’s the basic document for presenting all identified potential technologies for the product.

**F.2. Expected response**

*Special remarks.* The technology matrix **is part of the Design Justification File** (as per ECSS‑E‑ST‑10 Ann.K).

*Scope & content:*

1. **Introduction.** The purpose, objective, content & the reason prompting its preparation.
1. **Applicable & reference documents.** The applicable & reference documents in support to the generation of the document, incl. the reference to the following applicable documents: Function tree, Preliminary technical requirements specifications, Specification tree.
1. **List of technical requirements/functions.** List the system technical requirements/functions as per the functional architecture & its corresponding function tree, & their associated preliminary TS.
1. **List of potential technologies for each technical requirement/function.** List the system technical requirements/functions & for each, a potential technology or technological element. (Sources to identify potential technologies are technology watch, corporate technology plan, or research & development programme.) For each technology or technological element, the following information shall be listed:
   1. index of technology readiness & maturity as per Table 4‑2 of ECSS‑E‑AS‑11
   1. proof of company's maturity concerning the knowledge & expertise of the technology, including a description of the necessary technology acquisition activities
   1. identification of potential risks, e.g. technology availability, programmatic & financial aspects
1. **Ranking of the potential technologies for each function.** Propose a ranking of the potential technology or technological element for each system requirement/function.
1. **Conclusion.** Provide, for each system requirement/function, the selected technology or technological element, a list of the identified project risks & critical aspects, & an identified back‑up technological solution.



## Annex G (normative) Design definition file (DDF) — DRD

**G.1. DRD identification.** *Requirement ID & source* — ECSS‑E‑ST‑10, requirements 5.3.1.1c., 5.3.1.1f., 5.4.1.1b. & 5.4.1.4a. *Purpose & objective* — the objective of the **design definition file (DDF)** is to establish the technical definition of a system or product that complies with its technical requirements specification (as per ECSS‑E‑ST‑10‑06 Ann.A). The DDF is a basic structure referring to all information relative to the functional & physical architectures & characteristics of a system or product, necessary for its identification, manufacturing, utilization, support, configuration management & removal from service. The DDF is a collection of all documentation that establishes the system or product characteristics such as lower level technical specifications, design & interface description, drawings, electrical schematics, specified constraints (e.g. on materials, manufacturing, processes, & logistic). It details the as‑designed configuration baseline (as per ECSS‑M‑ST‑40) of the system or product & is built up & updated under the responsibility of the team in charge of SE. It’s the technical baseline for the production, assembly, integration & test, operations & maintenance of the product. The DDF, the technical requirements specification, & the Design Justification File (as per ECSS‑E‑ST‑10 Ann.K) are the basic documents used for product development. They are interrelated such as: the design (i.e. DDF) is the response to the requirements stated in the TS, the justification (i.e. DJF) demonstrates the conformance of the design (i.e. DDF) to the requirements stated in the TS. (The DDF is a logical file covering all TS disciplines required for the considered system. In general, the elements of the DDF are 「rolled out」 as separate documents.)

**G.2. Expected response**

*Special remarks* — None.

*Scope & content:*

1. **Introduction.** The purpose, objective & the reason prompting its preparation (e.g. programme or project reference & phase).
1. **Applicable & reference documents.** List of applicable & reference documents, used in support to the generation of the document. Refer to DDFs of next higher & lower level products. Include the reference to the following applicable documents: ➀ Business agreement, ➁ System engineering plan as per ECSS‑E‑ST‑10 Ann.D, ➂ Coordinate system document as per ECSS‑E‑ST‑10‑09 Ann.A, ➃ Technical requirements specification, ➄ Product assurance plan, ➅ Configuration management plan as per ECSS‑M‑ST‑40, ➆ Configuration status report as per ECSS‑M‑ST‑40.
1. **Summary of the project & technical requirements**
   1. A brief description of the product & of the main technical requirements throughout its life cycle phases (e.g. launch, deployed, operations, end‑of‑life).
   1. The description of the system or product design documentation, based on the Product Tree (as per ECSS‑M‑ST‑10) & also include, or refer to, the Specifications Tree (as per ECSS‑E‑ST‑10 Ann.J).
   1. A system shall contain at least the technical requirements specifications of the elements in which the system is broken down.
1. **Functional description**
   1. *Functional architecture.* The description of the functional architecture of the system or product i.e. the arrangement of functions, their sub‑functions & interfaces (internal & external), & the performance requirements to satisfy the requirements of the TS. Present the data & their flow interchanged between the different functions, the conditions for control, & the execution sequencing for the different operational modes & states. (The Functional Architecture is an output of the functional analysis process (as per ECSS‑E‑ST‑10 clause 5.3.1.1a.))
   1. *Function tree.* Include or refer to the system or product Function Tree, the latter conforming to ECSS‑E‑ST‑10 Ann.H.
   1. *Description of functional chains.* Describe the functional chains that contribute to the realization of the functional requirements of the TS & their contributing functions, consider the different operational modes & states, & indicate the selected physical implementation for each of the functions.
1. **Physical description**
   1. *Physical architecture.* The description of physical architecture of the system or product i.e. the arrangement of elements, their decomposition, interfaces (internal & external), & physical constraints, which form the basis of a system or product design to satisfy the functional architecture & the technical requirements. (The Physical Architecture is an output of the preliminary design definition activities (as per clause 5.3.1.1f.).)
   1. *Product tree.* Include or refer to the product tree of the system or product, as per ECSS‑M‑ST‑10 Ann.B. (The Product Tree is a breakdown of the Physical Architecture)
   1. *Specification tree.* Include or refer to the specification tree of the system or product, the latter conforming to Ann.J.
   1. *Description of elements of the physical architecture.* Reference any documentation containing detailed technical descriptions & associated matrices to ensure overall consistency & completeness. Provide:
      1. the nomenclature of the system or product,
      1. the overall system or product drawings,
      1. for each element of the system, the description of the different constituents of the physical architecture,
      1. the characteristics of the respective elements,
      1. their configuration management identifier (e.g. hardware part number, software version number, drawings number, electrical schematics numbers).
   1. *Description of interfaces.* Describe the physical & functional characteristics of the internal & external interfaces of the system & refer to the relevant IRD & ICD, conforming to ECSS‑E‑ST‑10‑24 Ann.A.
1. **System technical budget, margins & deviations.** Present the budget allocation of the technical parameters of the system & provide the actual status of the system margins, & deviations. Contain or refer to the system or product technical budget, the latter conforming to Ann.I.
1. **System design constraints**
   1. *Constraints for production.* Present the constraints induced by the system or product design definition on the production activities e.g. operational allowable envelopes, restrictions on assembling sequences, procedures & testing modes, exclusion zones, manufacturing environmental conditions, & conditions for procurement.
   1. *Constraints for operation.* Present the constraints induced by the system or product design definition on the implementation of the operations e.g. operational allowable envelopes, restrictions on operating modes, & exclusion zones.
   1. *Constraints for transportation & storage.* Present the constraints induced by the system or product design definition on the transportation activities & during the periods of storage of the product e.g. allowable envelopes, restrictions on transportation & storage, exclusion zones, packaging, shock levels, temperature environments, humidity, cleanliness, regulations, & dangerous materials.
   1. *Constraints for maintainability.* Present the constraints induced by the system or product design definition on the maintenance activities & procedures e.g. operational allowable envelopes, accessibility, tooling, support materials, parts availability, & deliveries.
1. **Engineering data repository.** Provide the information on the system or product engineering data repository that contains the complete set of design parameters, or a reference to it. (Information about the set of design parameters is provided in ECSS‑E‑TM‑10‑10)
1. **Conclusion.** List & summarize all deviations of the design with respect to the technical specifications & constraints induced by the system or product design definition.



## Annex H (normative) Function tree (FT) — DRD

**H.1. DRD identification.** *Requirement ID & source* — ECSS‑E‑ST‑10, requirement 5.3.1.1b. *Purpose & objective* — to describe the hierarchical decomposition of a system/product capabilities into successive level of functions & sub‑functions. It’s the starting point for the establishment of the Product Tree (as per ECSS‑M‑ST‑10) & is a basic structure to establish Preliminary Technical Requirements Specification(s) (as per ECSS‑E‑ST‑10‑06 Ann.A).

**H.2. Expected response**

*Special remarks.* The FT **is part of the Design Definition File**. The FT shall be coherent with other functional descriptions of the system/product (e.g. functional architecture, functional block diagram).

*Scope & content:*

1. **Introduction.** The purpose, objective & the reason prompting its preparation.
1. **Applicable & reference documents.** List of applicable & reference documents, used in support to the generation of the document.
1. **Project summary & user’s need presentation.** A brief description of the project & of the key user’s needs.
1. **Tree structure**
   1. The complete list of functions that the system/product shall perform, & contain a graphical representation where the main specified function(s) (i.e. at the top level of the tree) is/are decomposed into lower level functions.
   1. When recurrent products from previous space projects are used, identify the product’s functions in the tree structure, & addition, every necessary function by the system/product that is not under the supplier’s responsibility identified in the tree structure.



## Annex I (normative) Technical budget — DRD

**I.1. DRD identification.** *Requirement ID & source* — ECSS‑E‑ST‑10, requirement 5.4.1.2a. *Purpose & objective* — the **technical budget (TB)** defines for each key engineering parameter of a system or product, the nature of this parameter, its measure, specified value, metrics requirements & current actual or computed value & assessed value. It’s the basic document for providing adequate control of the key engineering parameter properties to meet the system or product technical requirements.

**I.2. Expected response**

*Special remarks.* The technical budget **is part of the Design Definition File** (as per ECSS‑E‑ST‑10 Ann.G).

*Scope & content:*

1. **Introduction.** The purpose, objective, content & the reason prompting its preparation.
1. **Applicable & reference documents.** The applicable & reference documents in support to the generation of the document.
1. **List of selected key engineering parameters**
   1. list the selected key engineering parameters (those specified by the customer & those selected by the supplier; e.g. mass, communication links, power, & OBC memory capacity)
   1. present the reason for their selection
   1. identify for each key engineering parameters the stages of maturity of the design
   1. present the related margin policy for these parameters
1. **Assessment of key engineering parameters.** For each key engineering parameter:
   1. provide the specified value of the parameter
   1. provide the supplier’s margin resulting of the allocation of the parameter to the lower level products
   1. provide the specified values with the reference to the relevant technical requirement of the lower level products
   1. propose a specific program to conform to the specified value in case of nonconformance
   1. contain a chart of parameter history that presents the evolution of the parameter’s value at the different design maturity steps for which the evaluation of the parameter is performed
   1. list the documentation sources (e.g. analysis report & verification report)
1. **Conclusion.** A conclusion that identifies the key engineering parameters having a negative margin, & identify for each of those:
   1. the impact on the technical requirements & the associated risk for the project
   1. the specific program to conform to the specified value & for project risk mitigation



## Annex J (normative) Specification tree — DRD

**J.1. DRD identification.** *Requirement ID & source* — ECSS‑E‑ST‑10, requirement 5.2.3.1c. *Purpose & objective* — the objective of the specification tree document is to define the hierarchical relationship of all technical requirements specifications for the different elements of a system or product. It’s the basic structure to perform the system or product requirements traceability & to manage their internal interfaces.

**J.2. Expected response**

*Special remarks.* The specification tree **is part of the Design Definition File** (as per ECSS‑E‑ST‑10 Ann.G). The specification tree shall be coherent with the product tree (see ECSS‑M‑ST‑10) & the business agreement structure (see ECSS‑M‑ST‑60).

*Scope & content:*

1. **Introduction.** The purpose, objective & the reason prompting its preparation.
1. **Applicable & reference documents.** The applicable & reference documents, used in support to the generation of the document.
1. **Project summary & user’s need presentation.** A brief description of the project & the key user’s needs.
1. **Tree structure**
   1. The specification tree document shall provide the complete list of specifications defining the system or product, & contain a graphical representation where the system or product specification (i.e. at the top level of the tree) is decomposed into lower level product specifications.
   1. When recurrent products from previous space projects are used, their specification shall be identified in the tree structure, & in addition, for every necessary product that is not under the supplier’s responsibility, their specification shall be identified in the tree structure.



## Annex K (normative) Design justification file (DJF) — DRD

**K.1. DRD identification.** *Requirement ID & source* — ECSS‑E‑ST‑10, requirements 5.3.1.1d, 5.3.1.1g, 5.3.1.1h and 5.4.1.4b. *Purpose & objective* —  to present the rationale for the selection of the design solution, & to demonstrate that the design meets the baseline requirements. The DJF is progressively prepared during the detailed design process & according to the system engineering plan (SEP) (as per ECSS‑E‑ST‑10 Annex D), & serves the following purposes: ➀ it provides access to the necessary justification information, ➁ it presents a review of all acquired justifications, ➂ it constitutes a basic element for decision taking concerning the product definition qualification. The DJF together with the Design Definition File (DDF) (as per Annex G) & the technical requirements specification (TS) (as per ECSS‑E‑ST‑10‑06 Annex A) are the basic documents used for the development of the product. These documents are used to monitor the evolution of the design. The DJF is a collection of all documentation that traces the evolution of the design during the development & maintenance of the product. The DJF is updated according to the evolution of the DDF, in accordance with the above‑mentioned objectives. The DJF provides also access to coherent & substantiated information which can be used to support decision‑making in the analysis of change requests for the management of non conformances. The DJF contains results obtained during the evolution of the design as a consequence of the activities performed along the design process: ➀ analysis & trade‑off reports concerning the evaluation of alternative design solutions & the justification of the choice; ➁ all results obtained during the verification of the design as a consequence of the activities performed along the verification process; ➂ test Reports on engineering model, structural & thermal model & qualification model (e.g. Protoflight Models). The DJF is a logical file covering all technical disciplines required for the considered system. In general, the elements of the DJF are 「rolled out」 as separate documents.

**K.2. Expected response**

*Special remarks* — None.

*Scope & content:*

1. **Introduction.** The purpose, objective, content & the reason prompting its preparation.
1. **Applicable & reference documents.** The applicable & reference documents in support to its generation. Refer to the relevant product specifications, the relevant DDF & SEP, &: ➀ Trade‑Off‑Reports, as defined Ann.L, ➁ Analysis Reports, e.g. requirements allocation analysis, functional analysis, ➂ Requirements Traceability Matrix, as per Ann.N with link to analysis, ➃ Verification Control Document, ➄ All verification documentation, such as: Analysis Reports (e.g. reports w.r.t. qualification aspects), Test/Inspection Reports, ROD Reports, Verification Reports
1. **Design description.** A description of the expected product, its intended mission, architecture & design, & the functioning principles on which it is based. Define the requirement criteria levels for qualification & acceptance verification of the product.
1. **Design Justification File Synthesis.** Present status of the design justification in response to requirements, with emphasis on the driving requirements that have a big impact on the system design, production & maintainability (see also K.2.1<8.2.4>a.). Present an overall system qualification status synthesis, including:
   1. the list of requirements which have not been met (e.g. nonconformances), including proposed actions
   1. the list of all critical points, & how criticalities have been or are intended to be resolved
   1. the identification of requirements which have not been justified yet, & associated risks analysis, with emphasis on those that can have an impact at system level
1. **Justification of the Functional Architecture.** Demonstrate that all requirements of the preliminary technical requirements specification are allocated to functional blocks of the system functional architecture. Where requirements assigned to functional blocks do not have their origin within any of the customer preliminary technical specifications, these requirements shall be justified.
1. **Justification of the Physical Architecture.** Demonstrate that the system design conforms to the requirements of the technical specification, & identify products which are reused (e.g. COTS). Provide the justification for the choice of architectural elements at the next lower level, or lower levels in case of system critical elements. Where requirements do not have their origin within any of the upper level technical specifications, these shall be justified.
1. **Development activities & synthesis of development results**
   1. Present the development activities (e.g. assessments, analyses, tests, & trade‑offs) & the design drivers, which lead to & justify the design as per the DDF, in line with the development approach identified in the SEP.
      1. The justification shall concern all the engineering disciplines contributing to design & development of the product (including its operational modes & scenarios).
      1. Include the status of DJF of lower level products. (Activities related to verification are dealt with in section K.2.1<8>)
   1. For the system & each discipline, produce the following information:
      1. Activity inputs, such as requirements, operational modes, assumptions, analysis cases, boundary conditions, model descriptions & limitations.
      1. Activity results, such as: (a) raw results, (b) evaluation of results, (c) evaluation of margins with respect to the technical requirements contained in the TS, (d) identification of any marginal areas.
      1. Activity synthesis, such as: (a) evidence of compliance to the technical requirements contained in the TS, (b) list of technical requirements which have not been met, including proposed actions, (c) list of all critical points, & how criticalities have been or are intended to be resolved, (d) identification of aspects of the design, which are not yet justified, & assessment of inherent risks.
   1. Refer to the requirements traceability matrix, e.g. w.r.t. building up the justification of a considered system top level requirements in terms of the various elements contributing to it, including where relevant contribution from other disciplines (e.g. pointing as a function of thermal, structures, & AOCS).
1. **Verification activities & synthesis of results**
   1.  *Verification plan.* Integrate or refer to the document that conforms to the verification plan DRD defined in ECSS‑E‑ST‑10‑02 Ann.A. (The verification activities are detailed in the Verification Plan (VP), which also contains the justification of the verification strategy (as per ECSS‑E‑ST‑10‑02).)
   1. *Qualification verification & synthesis of results*
      1. Qualification evidence. Present the evidence of the qualification of the design in conformance to the applicable technical requirements & proper qualification margins. (This is done in line with the qualification approach identified in the VP). Cover the system & all disciplines relevant to the product in all its operational modes & scenarios, addressing all applicable technical requirements & proper qualification margins. ((1) This is done in line with the system verification matrix. (2) The formal compliance with the qualification requirements is recorded in the VCD, together with references to the close‑out documents.)
      1. Implementation of the qualification plan. Present the implementation of the qualification plan & the status thereof, addressing the detailed definition of qualification activities (e.g. analysis, test, ROD, & inspection), including the detailed definition of the tests, the prediction of expected test results, test success criteria, test specifications, & model validations. (Details on test specifications are provided in ECSS‑E‑ST‑10‑03 Ann.B)
   1. *Validation of models.* Provide all evidence (e.g. analyses, test results, & model descriptions & correlations) regarding the suitability & validation of all models used for the analysis of the system.
   1. *Requirements status log.* Include a requirement status log addressing each requirement in turn, & including the:
      1. reference to relevant elements of the verification plan,
      1. synthesis of the justifications acquired, calling up references to the supporting activities & evidence (e.g. Technical Notes listed in section K.2.1<4>),
      1. list of justifications to be acquired & related activities,
      1. conclusion / action flag.
   1. *Manufacturing process status log.* Include a requirement status log, addressing design relevant aspects of manufacturing processes, & recording their characteristics in regard to qualification.
   1. *Acceptance verification.* Present the implementation of the acceptance verification & the status thereof, addressing the detailed definition of acceptance activities (e.g. inspection, test, analysis), including the detailed definition of the tests, the prediction of expected test results, test success criteria, & test specifications. (Details on test specifications are provided in ECSS‑E‑ST‑10‑03 Ann.B). Cover the system & all disciplines relevant to the product, addressing all acceptance verification activities in line with the system verification Plan (VP).
1. **Justification of System Technical Budgets & Margins.** The DJF shall present a synthesis of all technical budgets & margins for specific parameters according to the functional & physical architectures. (For technical budgets & margins, see ECSS‑E‑ST‑10 Ann.I)
1. **Justification of Constraints imposed by the System Design**
   1. *Design constraints on the production.* Present the justification of constraints induced by the system or product design definition on the production activities e.g. operational allowable envelopes, restrictions on assembling sequences, procedures & testing modes, exclusion zones, manufacturing environmental conditions, & conditions for procurement.
   1. *System design constraints for operation.* Present the justification of constraints induced by the system or product design definition on the implementation of the operations e.g. operational allowable envelopes, restrictions on operating modes, & exclusion zones.
   1. *System design constraints for transportation & storage.* Present the justification of constraints induced by the system or product design definition on the transportation activities & during the periods of storage of the product e.g. allowable envelopes, restrictions on transportation & storage, exclusion zones, packaging, shock levels, temperature environments, humidity, cleanliness, regulations, & dangerous materials.
   1. *System design constraints for maintainability.* Present the justification of constraints induced by the system or product design definition on the maintenance activities & procedures e.g. operational allowable envelopes, accessibility, tooling, support materials, parts availability, & deliveries.
1. **Constituent documents.** Include or refer to the DJF of lower level elements of the product. Integrate or refer to the documents that conform to the:
   1. ECSS‑E‑ST‑10 Ann.L, Trade‑Off‑Report — DRD
   1. ECSS‑E‑ST‑10 Ann.Q, Analysis Report — DRD
   1. ECSS‑E‑ST‑10 Ann.O, Requirement Justification File — DRD
   1. ECSS‑E‑ST‑10 Ann.N, Requirements Traceability Matrix — DRD
   1. ECSS‑E‑ST‑10‑02 Ann.B, Verification Control Document — DRD
   1. ECSS‑E‑ST‑10‑02 Ann.C, Test Report — DRD
   1. ECSS‑E‑ST‑10‑02 Ann.D, Review Of Design Report — DRD
   1. ECSS‑E‑ST‑10‑02 Ann.E, Inspection Report — DRD
   1. ECSS‑E‑ST‑10‑02 Ann.F, Verification Report — DRD



## Annex L (normative) Trade‑off report — DRD

**L.1. DRD identification.** *Requirement ID & source* — ECSS‑E‑ST‑10, requirement 5.3.3.1b. *Purpose & objective* — to provide the system‑engineering point of view on alternative design solutions, an evaluation & a classification of the alternative design solutions, & the justification of their ranking.

**L.2. Expected response**

*Special remarks* — None.

*Scope & content:*

1. **Introduction.** The purpose, objective, content, & the reason prompting its preparation.
1. **Applicable & reference documents.** A list of the documents in support to the generation of the document, incl. the reference to the following applicable documents:
   1. Mission description document, if relevant
   1. Project phasing & planning requirement document
   1. System engineering plan
   1. Technical requirements specification (TS)
1. **Objective & context of the trade‑off study.** The purpose of the trade‑off study & its context (e.g. logic, organization, process or procedure).
1. **Key technical requirements**
   1. A list of the key technical requirements from the TS (as per ECSS‑E‑ST‑10‑06) to be satisfied by the possible alternative design solutions to conform to the needs or requirements of the user.
   1. The research of possible alternative design solutions should not preclude the identification of design solutions which are not currently mature enough, but which can be potential design solution for future similar applications. (A possible design solution is a technical answer that has the capability to meet a set of technical requirements)
   1. Identify & present the sources of information used to identify the possible design solution, e.g. R&D results, lessons learned, or similar applications.
1. **Evaluation criteria**
   1. A list of the selected evaluation criteria & precise the justification for selecting those criteria, & provide the weighting of criteria & their justifications. (The criteria are selected theme by theme from the Technical Specification (as per ECSS‑E‑ST‑10‑06 Ann.A), the programmatic aspects (including e.g. budget, schedule, etc… for development, manufacturing, as well as target cost for operations & recurrent items), & the technical risks).
   1. Identify the entity responsible for the evaluation of the design solutions for any criteria, as well as the source & agreement regarding weighting factors (e.g. with management).
1. **Presentation of the alternative design solutions**
   1. Present every different alternative design solutions proposed by the organization in charge of the development of the system or product, the proposals from the customer & supplier if any, & emphasize the technical description that is correlated to the criteria of evaluation.
   1. Characterize each alternative design solution in terms of technology status or maturity, performances capability, & risks.
1. **Evaluation of the alternative design solutions.** Present the result of the evaluation of every identified alternative design solution with regard to the key technical requirements. For each alternative design solution the following shall be performed; present, in a table, the result of the evaluation per criteria:
   1. assessment of all the key technical requirements/evaluation criteria
   1. presentation of the pros/cons of the design solution
   1. identification of the technical/programmatic risks
1. **Classification of the alternative design solutions.** Based on the proposed scheme for weighting the evaluation criteria, provide a classification of the different alternative design solutions.
1. **Analysis of the robustness of the classification.** Provide the result of a sensitivity analysis of the criteria that provide advantage to the solution ranked first, e.g. when changing the weighting of the evaluation criteria.
1. **Conclusion**
   1. Recommend one solution & explain the reason for this choice (e.g. evaluation criteria where the selected solution take advantage), & precise the condition for the application of the recommended solution.
   1. Present the identified technical & programmatic risks induced by the choice of the recommended solution, & any additional activity necessary to be performed for risk mitigation.



## Annex M (deleted)



## Annex N (normative) Requirements traceability matrix (RTM) — DRD

**N.1. DRD identification.** *Requirement ID & source* — ECSS‑E‑ST‑10, requirement 5.2.2.1b. *Purpose & objective* — the **requirement traceability matrix (RTM)** defines the relationships between the requirements of a deliverable product defined by a technical requirements specification & the apportioned requirements of the product's lower level elements. The purpose of the RTM is to help verify that all stated & derived requirements are allocated to system components & other deliverables (forward trace). The matrix is also used to determine the source of requirements (backward trace). Requirements traceability includes tracing any information that satisfy the requirements such as capabilities, design elements, & tests. The RTM is also used to ensure that all requirements are met & to locate affected system components when there is a requirements change. The ability to locate affected components allows the impact of requirements changes on the system to be determined, facilitating cost, benefit, & schedule determinations.

**N.2. Expected response**

*Special remarks* — None.

*Scope & content:*

1. **Introduction.** The purpose, objective, content & the reason prompting its preparation.
1. **Applicable & reference documents.** The applicable & reference documents in support to the generation of the document. The RTM shall include the following:
   1. Technical requirements specification (as per ECSS‑E‑ST‑10‑06 Ann.A) of the product & its lower level elements
   1. Product tree (as per ECSS‑M‑ST‑10 Ann.B)
   1. Specification tree (as per ECSS‑E‑ST‑10 Ann.J).
1. **Requirement traceability**
   1. List all the technical requirement of the product TS.
   1. List all the lower level elements constituting the product & their technical requirements (contained in the lower‑level element TS).
   1. The requirement identification shall be identical in the RTM & the different TS.
   1. Each technical requirement of the product shall be linked to at least one requirement of a lower level element. (The required visibility of the traceability down the elements of the product tree is depending on the criticality of some lower level elements w.r.t. the product requirements)
   1. Each technical requirement of a lower level element should be linked to a technical requirement of the product.
   1. When a technical requirement of a lower level element is not linked to a technical requirement of the product, this requirement shall be justified & an evaluation of its existence or removal on the product shall be agreed between the customer & the supplier.



## Annex O (normative) Requirements justification file (RJF) — DRD

**O.1. DRD identification.** *Requirement ID & source* — ECSS‑E‑ST‑10, requirement 5.2.1.1e. *Purpose & objective* — the **requirement justification file (RJF)** is a generic title referring to all documentation which: records & describes the needs & the associated constrains resulting from the different trade‑offs; demonstrates how the requirements of the technical requirements specification (TS) (as per ECSS‑E‑ST‑10‑06 Ann.A) at each level can satisfy the needs & the constraints of the TS of the level above.

**O.2. Expected response**

*Special remarks.* A top level RJF document is established at the upper level of the project structure that is, according to ECSS‑M‑ST‑10, the first‑level customer situated at level 0 of the customer supplier network. For other levels, the RJF **is part of the design justification file (DJF)** (as per Ann.K).

*Scope & content:*

1. **Introduction.** The purpose, objective, content & the reason prompting its preparation
1. **Applicable & reference documents.** The applicable & reference documents in support to the generation of the document. Include the reference to the technical requirements specification.
1. **Selected concept/solution justification.** Present the rationale for the selection of a concept for the technical requirements specification. (For justification, reference can be made to the system concept report of the considered project)
1. **Life profile justification for the selected concept/solution.** Present & justify the life profile situations for the concept presented in the technical requirements specification. (For justification, reference can be made to the system concept report of the considered project where relevant)
1. **Environments & constraints justification.** Present & justify the different environments & constraints for each life profile situations for the concept presented in the technical requirements specification. (For justification, reference can be made to the system concept report of the considered project where relevant)
1. **Technical requirements justification.** List all the technical requirements, & their identifier, expressed by the corresponding TS as they are organized in these documents. For each technical requirement the following information shall be provided:
   1. the justification of the requirement (i.e. justified source),
   1. the Entity or owner responsible for the requirement,
   1. if one technical requirement is induced by several sources, the reason of the specified performance,
   1. the justification of the quantified performance (such as the range, the approach used to determine the level, e.g. measure, estimation),
   1. the justification of the selected verification method.
1. **Requirement traceability.** Present the requirement traceability between the technical requirements of the TS, & their justified source.
1. **Compliance matrix for COTS.** For COTS, the RJF shall contain a compliance matrix between the technical specification/characteristics of the COTS & the technical requirement expressed by the TS.



## Annex P (normative) Product user manual (PUM or UM) — DRD

**P.1. DRD identification.** *Requirement ID & source* — ECSS‑E‑ST‑10 requirement 5.4.1.4c. *Purpose & objective* — the objective of the **product user manual (PUM)** is to provide information on design, operations & data of the product that is required by the user to handle, install, operate, maintain & dispose the product during its life time.

**P.2. Expected response**

*Special remarks* — Where the objective is to allow for the accommodation of equipment designed a posteriori w.r.t an existing platform or vehicle, the following docs shall be part of the UM: the accommodation handbook describing the location, mounting, all interfaces & clearances of equipment in a platform or vehicle; the installation plan describing the approach, methods, procedures, resources & organization to install, commission, & check the operation of the equipment in its fixed operational environment.

*Scope & content:*

1. **Introduction.** Describe the purpose & objective of the PUM.
1. **Applicable & reference documents.** The applicable & reference documents in support of the generation of the document.
1. **Product function definition**
   1. *Product expected functions.* Provide a general description of the expected functions of the product during its lifetime in expected operational context & environment.
   1. *Product functional constraints.* Describe all product functional constraints.
   1. *Life time phases & purposes*
      1. Address the whole product life cycle & all its modes: Handling, Storage, Installation, Operations (nominal & contingency), Maintenance, Disposal
      1. Consider potential consequences of the environment on those sequences (e.g. sensor blinding, eclipses)
1. **Product description**
   1. *Design summary.* Include the following:
      1. summary of the product design, showing the definition of the product, its constituents, the distribution of functions & the major interfaces;
      1. block diagram of the product;
      1. top‑level description of the product software architecture;
      1. description of nominal product operations scenarios & constraints e.g. mutually exclusive modes of operation, power or resource sharing.
   1. *Product level autonomy.* Include the following:
      1. description of product‑level autonomy provisions in the areas of fault management (FDIR);
      1. definition, for each autonomous function, of the logic or rules used & of its internal (product constituents) & external interfaces.
   1. *Product configurations.* Include the following:
      1. drawings of the overall product configuration in all product modes;
      1. definition of the product reference axes system(s);
      1. drawings of the product layouts.
   1. *Product budgets.* Provide the distribution (or allocation) of the following budgets, per product constituent, or per operating mode, as appropriate:
      1. mass properties;
      1. alignment;
      1. power consumption for all operational modes;
      1. thermal budget & constraints & predictions;
      1. Description of interfaces & related budgets. (e.g. RF links);
      1. telemetry & telecommand date rates;
      1. memory;
      1. timing.
   1. *Interface specifications.* Provide a cross‑reference to the applicable version of the ICD.
   1. *Handling.*
      1. Describe the conditions & procedures for the handling of the product, be it integrated or stand‑alone.
      1. Describe the specific design features, transport & environmental conditions, required GSE, & limitations for the handling of the product.
   1. *Storage*
      1. Describe the conditions & procedures for the storage of the product, be it integrated or stand‑alone.
      1. Describe the specific design features, environmental conditions, required GSE, monitoring requirements, life‑limited items, health maintenance procedures (activation, monitoring) & limitations for the storage of the product.
   1. *Installation*
      1. Describe the conditions & procedures for the installation of the product, be it integrated or stand‑alone.
      1. Describe the specific design features, required GSE, modes, environmental conditions, & limitations for the installation of the product.
   1. *Product operations*
      1. General. Include timelines, modes & procedures, constraints to operate the product during its life cycle in nominal & contingency conditions, & highlight critical operations. ((1) When the product is a space segment, the product operations aspects are included in a specific part of the UM called Flight Operations Manual (FOM). (2) The implementation of the FOM by the ground segment responsible organisation is contained in the Mission Operations Plan (MOP, as per ECSS‑E‑ST‑70 Ann.G).)
      1. Timelines.
         1. Include: Baseline event timelines for all nominal & contingency modes & phases; Related constraints.
         1. Each timeline shall contain a detailed description (i.e. down to the level of each single operational action) of the complete sequence of operations to be carried out, including a description of the rationale behind the chosen sequence of events, a definition of any constraints (e.g. absolute timing, relative timing) & the interrelationships between operations in the sequence.
      1. Product modes
         1. Describe all nominal & contingency modes, including:
            1. their purpose (i.e. circumstances under which they are used),
            1. the related procedures,
            1. operational constraints,
            1. resource utilization,
            1. the definition of the associated modes, and
            1. monitoring requirements.
         1. Describe the allowable mode transitions & the operations procedure corresponding to each such transition.
         1. Appropriate cross‑reference shall be made to product constituent modes & procedures.
      1. Product failure analysis
         1. Provide the results of the product failure modes, effects & criticality analysis (FMECA) & the resulting list of single point failures.
         1. Potential product failures shall be identified by means of a fault‑tree analysis (FTA).
   1. *Maintenance.* Describe the conditions, procedures & logistics for the maintenance of the product, be it integrated or stand‑alone. The description can refer to the document that conforms to the Integrated Logistic Support Plan in conformance with ECSS‑M‑ST‑70.
   1. *Disposal*
      1. Describe the conditions & procedures for the disposal of the product, be it integrated or stand‑alone.
      1. The procedures shall include passivation, as relevant.
      1. Identify the risks during & after disposal.
1. **Products constituents description**
   1. *General.* The information specified in P.2.1<5.2> to P.2.1<5.9> shall be provided for each product constituent.
   1. *Product constituent design summary.* Describe the product constituent including:
      1. the overall functions of the product constituent & the definition of its operational modes during the different mission phases;
      1. description of any product constituent management functions, fault management concept & redundancy provisions;
      1. a summary description of the component units/equipment & software including the functions which each supports;
      1. product constituent functional block diagrams & a diagram showing the source of telemetry outputs & the sink of telecommand inputs;
      1. interfaces;
      1. budgets.
   1. *Product constituent design definition.* The following shall be provided for each product constituent:
      1. a detailed design description, including block diagrams, functional diagrams, logic & circuit diagrams;
      1. physical characteristics including location & connections to the support structure, axes definition & alignment where relevant, dimensions & mass properties;
      1. principle of operation & operational constraints of the product constituent;
      1. lower level of breakdown for products composed of many complex elements.
   1. *Software*
      1. Include:
         1. description of software design,
         1. product constituent software,
         1. application process service software, and
         1. memory map.
      1. Describe the organization of the software & its physical mapping onto hardware.
      1. Describe the details of each software component i.e. scheduler, interrupt handler, I/O system, telecommand packet handling system, telemetry packet handling system, including for each component its functions, component routines, input/output interfaces, timing & performance characteristics, flowcharts & details of any operational constraints.
      1. For the application process service software, the PUM shall:
         1. describe the services implemented making cross‑reference to ECSS‑E‑ST‑70‑41 「Telemetry & telecommand packet utilization」, as tailored for the mission;
         1. summarize all telemetry & telecommand structures (e.g. packets) including the conditions under which they are generated, the generation frequency, content & interpretation.
      1. For each memory block, a map shall be provided showing RAM & ROM address areas, areas allocated for program code, buffer space & working parameters (e.g. content of protected memory).
   1. *Product component performance.* Describe all relevant product constituent performance characteristics, define the expected performance degradation as a function of time during the mission, & identify the resulting impact in terms of modifications to operational requirements or constraints.
   1. *Product component telemetry & telecommand lists*
      1. For each product constituent, the following lists shall be provided: a list of the housekeeping telemetry parameters; a list of the telecommands.
      1. Each housekeeping telemetry shall have a functional description with validity conditions, telecommand relationship, & all technical information necessary for using it.
      1. Each telecommand shall have a functional description with utilization conditions (e.g. pre‑transmission validity, criticality level), command parameters (syntax & semantics) & execution verification in telemetry.
   1. *Product component failure analysis.* Describe:
      1. Identification of potential product constituent failures by means of a systematic failure analysis (including a subsystem FMECA & FTA).
      1. Identification of the methods by which the higher levels can identify a failure condition from analysis of the telemetry data & isolate the source of the failure.
   1. *Product components operations*
      1. Describe:
         1. product constituent modes;
         1. nominal operational procedures;
         1. contingency procedures.
      1. product constituent modes shall be defined for all distinct nominal & back‑up modes of the subsystem including:
         1. purpose (i.e. conditions under which each is used);
         1. operational constraints;
         1. resource utilization;
         1. the definition of the associated modes for each product constituent & its software functions;
         1. higher level monitoring requirements;
         1. identification of the allowable mode transitions & any product constituent operational constraints.
      1. Nominal operational procedures shall be defined for each nominal mode transition identified under P.2.1<5.8>6..
      1. For each procedure described in P.2.1<5.8>c.., the following shall be provided:
         1. an introduction describing the purpose of the procedure & the phase(s) or conditions when applicable;
         1. the body of the procedure, structured according to operational steps, including:
            1. pre‑conditions for the start of the step defining, where applicable: product or product constituent level pre‑requisites (e.g. configuration & resource requirements, such as power, fuel); external interfacing products pre‑requisites.
            1. telecommands to be sent;
            1. telemetry data to be monitored to verify correct execution of the step;
            1. interrelationships between steps (e.g. conditional branching within the procedure, timing requirements or constraints, hold & check points);
            1. conditions for completion of the step.
         1. Contingency procedures shall be defined for each failure case identified in the product constituent failure analysis (FMECA/FTA). This can utilize a nominal operational procedure already identified in P.2.1<5.8>c.. above.
         1. For contingency procedures, the same details shall be provided as for nominal operational procedures in P.2.1<5.8>d.. above.
         1. Where the recovery method for a failure or group of failures is mode, mission, or phase dependent, separate procedures shall be described for each mode/mission phase.
      1. *Product component data definition*
         1. For each operational mode of the product constituent, sensor output data, conditions under which they are generated, their contents, & data rate shall be described.
         1. Required on‑board processing performed on sensor data & algorithms used for this shall be described.



## Annex Q (deleted)

Deleted & replaced by informative Annex S



## Annex R (informative) Mapping of typical DDP to ECSS documents

|**Design Development Plan (DDP) content**|**ECSS coverage**|
|:--|:--|
|**Introduction**<br> ・General summary of the scope, objective & constraints<br> ・Definition of responsibilities & pre‑requisites|ECSS‑M‑ST‑10‑PMP <3><br> ECSS‑E‑ST‑10‑SEP <3.1><br> ECSS‑M‑ST‑10‑PMP <4>|
|**System definition including high level product tree**<br> ・Identification of customer furnished products<br> ・Availability of & need to reuse existing products<br> ・Specification tree|ECSS‑M‑ST‑10‑PT<br> ECSS‑M‑ST‑60‑9.6.2 req.<br> ECSS‑E‑ST‑10‑SEP <4.2>.6 & <5.2.13><br> ECSS‑M‑ST‑10‑PT <3>.e<br> ECSS‑E‑ST‑10 Ann.J|
|**Technology assessment**<br> Availability of & need to develop new technologies|ECSS‑E‑ST‑10‑SEP <5.2.5><br> ECSS‑E‑ST‑10‑TP <4.3>|
|**System design flow & philosophy of models**<br> ・Model philosophy<br> ・Design margin philosophy for budgets<br> ・・Margin philosophy for requirements as flown down from system to subsystem‑ assembly — & equipment level<br> ・・Management of system resource allocation over the entire project phases & resource reporting to the next higher level<br> ・・Rationale|ECSS‑E‑ST‑10‑SEP <4.2>.4<br> ECSS‑E‑ST‑10‑SEP <4.2>.5|
|**System control & verification**<br> ・Qualification & acceptance philosophy<br> ・Budget allocation philosophy<br> ・Compliance to requirements demonstration philosophy<br> ・High‑level Assembly, Integration & Verification Plan, incl.: end to end test, mission simulation & Dress Rehearsals|ECSS‑E‑ST‑10‑SEP <5.2.2><br> ‑> ECSS‑E‑ST‑10‑02‑VP<br> ‑> ECSS‑E‑ST‑10‑02‑VCD (inc. VM)|
|**Planning & definition of milestones & reviews** including the analysis of the critical path(s)<br> ・definition & scope of reviews in terms of baselining of requirements & requirements control, design releases & control of released design including external & internal interfaces|ECSS‑M‑ST‑10‑PMP <7><br> ‑> ECSS‑M‑ST‑60‑S<br> ECSS‑M‑ST‑10‑01<br> ECSS‑E‑ST‑10‑SEP <3.3>|
|**Definition of required Ground Support Equipment** & schedule of development & delivery| |
|**Risk assessment** linked to development choices|ECSS‑E‑ST‑10‑SEP <5.4> <3.5><br> ECSS‑M‑ST‑80‑RAR|
|ECSS‑M‑ST‑10‑PMP = ECSS‑M‑ST‑10, Project management plan DRD Ann.A| |
|ECSS‑M‑ST‑10‑PT = ECSS‑M‑ST‑10, Product tree DRD Ann.B| |
|ECSS‑M‑ST‑60‑S = ECSS‑M‑ST‑60, Schedule DRD Ann.B| |
|ECSS‑M‑ST‑80‑RAR = ECSS‑M‑ST‑80, Risk assessment report DRD Ann.C| |
|ECSS‑E‑ST‑10‑SEP = ECSS‑E‑ST‑10, System engineering plan DRD Ann.D| |
|ECSS‑E‑ST‑10‑TP = ECSS‑E‑ST‑10, Technology plan DRD Ann.E| |
|ECSS‑E‑ST‑10‑02‑VP = ECSS‑E‑ST‑10‑02, Verification plan DRD Ann.A| |
|ECSS‑E‑ST‑10‑02‑VCD = ECSS‑E‑ST‑10‑02, Verification control document DRD Ann.B (Note: Verification matrix = 1st issue of VCD)|



## Annex S (informative) Guideline content of Analysis Report

**S.1 DRD identification.** *Requirement ID & source* — the Note of ECSS‑E‑ST‑10, requirement 5.3.1.1j, & is provided as a guideline. *Purpose & objective* — the analysis report describes, for each analysis, the relevant assumptions, utilized methods, techniques & results.

**S.2 Expected response**

*Special remarks* — None.

*Scope & content:*

1. **Introduction.** The purpose, objective, content & the reason prompting its preparation. Any open issue, assumption & constraint relevant to this document are stated & described.
1. **Applicable & reference documents.** Lists the applicable & reference documents in support to the generation of the document, & make a clear reference to the configuration baseline of the product considered for the analysis.
1. **Definitions & abbreviations.** Lists the applicable dictionary or glossary & the meaning of specific terms or abbreviations utilized in the document with the relevant meaning.
1. **Analysis approach.** Summarize the analysis content & the method utilized.
1. **Assumptions & limitations.** Describes the basic assumptions, the boundary conditions, validity of the analysis, life profile aspects, & all other related limitations.
1. **Analysis description.** Describe & justify the analysis methods used including software, tools & associated models.
1. **Analysis results.** Present the main calculations, associated results, accuracies, sensitivities, margins where relevant.
1. **Conclusions**
   1. The analysis report: 1. summarizes the analysis results, 2. summarizes the conditions of validity of this analysis, 3. clearly states & describes any open issue.
   1. Where the analysis report is used for the verification of requirements it lists the requirements to be verified (in correlation with the VCD), summarizes the analysis results, presents comparison with the requirements, & indicates the verification closeout judgements (e.g. requirement met / not met).


## The End

end of file
