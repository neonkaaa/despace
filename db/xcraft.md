# Xcraft
> 2019.01.21 [🚀](../../index/index.md) [despace](index.md) → **[](.md)** <mark>NOCAT</mark>  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Xcraft** — EN term. **Икскрафт** — rough RU analogue.</small>

**Xcraft** — the basic [Xplore](xplore.md) spacecraft, offered to any customer for work near the Earth, Moon, Venus, Mars and near asteroids.

![](f/project/x/xcraft/pic01.webp)

|**Type**|**[Param.](si.md)**|
|:--|:--|
|**【Mission】**|~ ~ ~ ~ ~ |
|Cost|… or … ㎏ of [gold](sc_price.md) in … prices|
|[CML](trl.md) / [TRL](trl.md)|CML: <mark>TBD</mark>, TRL: <mark>TBD</mark>|
|Development|2017 ‑ ┊|
|Duration|…|
|Launch|2021 (planned), …, …|
|Operator|…|
|Programme|…|
|Similar to|・Proposed: …<br> ・Current: …<br> ・Past: …|
|Target|…|
|[Type](sc.md)|…|
|**【Spacecraft】**|~ ~ ~ ~ ~ |
|Composition|…|
|Contractor|…|
|Manufacturer|…|
| |**`…`**|
|Comms|Large HGA, multi‑band relay communications|
|[ID](spaceid.md)|NSSDC ID (COSPAR ID): <mark>TBD</mark>, SCN: <mark>TBD</mark>|
|Mass|… ([…satellite](sc.md))|
|Orbit / Site|…|
|Power|550 W (at 1.0 AU), expandable to 1.1 kW|
|Payload|The payload depends on the order. Up to 70 ㎏.|

Targets & investigations:

1. **T** — technical; **C** — contact research; **D** — distant research; **F** — fly‑by; **H** — manned; **S** — soil sample return; **X** — technology demonstration
1. **Sections of measurement and observation:**
   1. Atmospheric/climate — **Ac** composition, **Ai** imaging, **Am** mapping, **Ap** pressure, **As** samples, **At** temperature, **Aw** wind speed/direction.
   1. General — **Gi** planet’s interactions with outer space.
   1. Soil/surface — **Sc** composition, **Si** imaging, **Sm** mapping, **Ss** samples.

<small>

|**EVN‑XXX**|**T**|**EN**|**Section of m&o**|**D**|**C**|**F**|**H**|**S**|
|:--|:--|:--|:--|:--|:--|:--|:--|:--|
|…| |…| | | | | | |


</small>



## Mission
<mark>TBD</mark>



## Science goals & payload
The spacecraft is universal and is currently offered for the following missions:

1. Delivery of Arch Libraries materials to the planets of the Solar system. Agreement reached 2019.06.11.
1. Mission (any) at the request of the customer.
1. Operation of an artificial satellite of Venus in orbit (polar orbit, 550 × 550 ㎞, period 1.6 h, speed 7 ㎞/s) with launches in 2022, 2024, 2026, 2028.
1. Operation of an artificial moon satellite in orbit (orbit with an inclination of 86°, 200 × 200 ㎞, period 2.46 h, speed 1.6 ㎞/s) with launches in 2022, 2024, 2026, 2028.
1. Operation in orbit of an artificial satellite of Mars (orbit with an inclination of 93°, 250 × 250 ㎞, period 1.87 h, speed 3.4 ㎞/s) with launches in the fourth quarters of 2021, 2022, 2023, 2024.
1. Functioning of asteroids with launches in 2023, 2024, 2025, 2026.



## Spacecraft

The standard XCRAFT is perfect for most missions.

1. Enormous payload bay that can accommodate 30 ㎏ — 70 ㎏ of payload in 50 U of volume
1. Electric propulsion providing between 1500 ㎧ — 3500 ㎧ delta-V to fit mission needs
1. 550 W of power generation at 1.0 AU, expandable to 1.1 kW
1. Large high‑gain antenna for high speed communications from interplanetary distances
1. Multi‑band relay communications from other spacecraft, landers, rovers and ground terminals.
1. Optically stable platform with precision pointing for high performance sensors


|**Venus**|**Mars**|**Moon**|**Asteroid**|**Custom**|
|:--|:--|:--|:--|:--|
|[![](f/project/x/xcraft/pic04t.webp)](f/project/x/xcraft/pic04.webp)|[![](f/project/x/xcraft/pic02t.webp)](f/project/x/xcraft/pic02.webp)|[![](f/project/x/xcraft/pic03t.webp)](f/project/x/xcraft/pic03.webp)|[![](f/project/x/xcraft/pic05t.webp)](f/project/x/xcraft/pic05.webp)|[![](f/project/x/xcraft/pic06t.webp)](f/project/x/xcraft/pic06.webp)|



## Community, library, links

**PEOPLE:**

1. <mark>TBD</mark>

**COMMUNITY:**

<mark>TBD</mark>



## Docs/Links
|**Sections & pages**|
|:--|
|**【[](.md)】**<br> <mark>NOCAT</mark>|

1. Docs: …
1. <https://www.xplore.com/xcraft.html>


## The End

end of file
