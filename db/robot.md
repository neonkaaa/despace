# Robotics
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [Robotics](robot.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Robotics** — EN term. **Робототехника** — RU analogue.</small>

**Robotics** is the study of robots. **Robotics** *(from 「robot」 & 「technology」; ru. роботика, роботехника)* is an applied science engaged in the development of automated technical systems & is the most important technical basis for the development of production. Robotics relies on disciplines such as electronics, mechanics, cybernetics, telemechanics, mechatronics informatics, radio & electrical engineering. There’re construction, industrial, household, medical, aviation & extreme (military, space, underwater) robotics.

**Major classes of robots.**  
Several approaches to the classification of robots can be used — for example, by scope, by purpose, by mode of movement, etc. According to the scope of the main application, industrial robots, research robots, robots used in training, special robots can be distinguished. The most important classes of general‑purpose robots are manipulation & mobile robots.

1. **Manipulation robot** — an automatic machine (stationary or mobile), consisting of an executive device in the form of a manipulator, which has several degrees of mobility, & a program control device, which serves to perform motor & control functions in the production process. Such robots are manufactured in floor-standing, suspended & portal versions. They are most widespread in the machine-building & instrument-making industries.
1. **Mobile robot** — automatic machine with a moving chassis with automatically controlled drives. Such robots can be wheeled, walking & tracked (there are also crawling, floating & flying mobile robotic systems).


| | |
|:--|:--|
|**AE**|…|
|**AU**|…|
|**CA**|・[Deltion](deltion.md)<br> ・[MDA](mda.md)<br> ・[Space Concordia](space_concordia.md) — (Student community) Robots<br> ・[UTIAS](utias.md) — Research for space mechatronics, robotics, microsats, fluid dynamics|
|**CN**|…|
|**EU**|…|
|**IL**|…|
|**IN**|…|
|**JP**|・[Meltin](meltin.md) — avatar robots|
|**KR**|…|
|**RU**|…|
|**SA**|…|
|**SG**|…|
|**US**|…|
|**VN**|…|



## Robotic arm

| |**[Company](contact.md)**|**Units (mass, ㎏)**|
|:--|:--|:--|
|**EU**|[PIAP](piap_space.md) (PL)|[Titan](ra_lst.md) (55)|
| |[Redwire](redwire.md) (LU)|[Staark](ra_lst.md) (35)|



## Rover
> <small>**Rover** — EN term. **Планетоход** — RU analogue.</small>

 **Rover** — a device designed to move on the surface of another planet. Varieties of the term:

1. **Lunar rover** *(ru. Луноход)* — rover designed to move on the surface of the moon.
1. **Mars rover** *(ru. Марсоход)* — rover designed to move on the surface of Mars. 

|**Planet**| |**Active**|**Archived**|
|:--|:--|:--|:--|
|**[Venus](venus.md)**|**RU**||[HM-VD-2](хм‑вд‑2.md)|
| |**US**|[AREE](aree.md) (2027)| |
| | | | |
|**[Луна](moon.md)**|**CN**|[Yutu-2](yutu_2.md) (2018)|[Yutu](yutu.md)|
| |**IN**|[Chandrayaan-2-rover](chandrayaan_2_rover.md) (2018)| |
| |**RU**|[Robot-geolog](робот_геолог.md) (2020)|[Lunokhod‑2](луноход_2.md) ~~ [Lunokhod‑1](луноход_1.md)|
| |**US**||[LRV](lrv.md)|
| | | | |
|**[Марс](mars.md)**|**EU**|[MSRM](msrm.md) (2022) ~~ [ExoMars Rover](exomars_rover.md) (2020)| |
| |**RU**|[ExoMars Rover](exomars_rover.md) (2020)|[Prop_M](проп_м.md)|
| |**US**|[MSRM](msrm.md) (2022) ~~ [Mars 2020 RV](mars_2020_rv.md) (2020) ~~ [Opportunity](opportunity.md)|[Curiosity](curiosity.md) ~~ [Spirit](spirit.md) ~~ [Sojourner](sojourner.md)|

【**Table.** Manufacturers】

| | |
|:--|:--|
|**AU**|…|
|**CA**|・[CSA](csa.md)<br> ・[Deltion](deltion.md)|
|**CN**|…|
|**EU**|…|
|**IN**|…|
|**IL**|…|
|**JP**|・[Harada Seiki](harada_seiki.md)<br> ・[ispace](ispace.md)|
|**KR**|…|
|**RU**|…|
|**SA**|…|
|**SG**|…|
|**US**|…|
|**AE**|…|
|**VN**|…|



## Description

### From NASA Knows! (Grades 5-8) series.
Robotics is the study of robots. Robots are machines that can be used to do jobs. Some robots can do work by themselves. Other robots must always have a person telling them what to do.

How Does NASA Use Robots?
NASA uses robots in many different ways. Robotic arms on spacecraft are used to move very large objects in space. Spacecraft that visit other worlds are robots that can do work by themselves. People send them commands. The robots then follow those commands. This type of robot includes the rovers that explore the surface of Mars. Robotic airplanes can fly without a pilot aboard. NASA is researching new types of robots that will work with people & help them.


What Are Robotic Arms?
NASA uses robotic arms to move large objects in space. The space shuttle’s "Canadarm" robot arm 1st flew on the shuttle’s second mission in 1981. The International Space Station is home to the larger Canadarm2. The space shuttle has used its arm for many jobs. It could be used to release or recover satellites. For example, the arm has been used to grab the Hubble Space Telescope on five different repair missions. The shuttle & space station arms work together to help build the station. The robotic arms have been used to move new parts of the station into place. The arms also can be used to move astronauts around the station on spacewalks. The space station’s arm can move to different parts of the station. It moves along the outside of the station like an inchworm, attached at one end at a time. It also has a robotic "hand" named Dextre that can do smaller jobs. An astronaut or someone in Mission Control must control these robotic arms. The astronaut uses controllers that look like joysticks used to play video games to move the arm around.


How Do Robots Explore Other Worlds?
Robots help NASA explore the solar system & the universe. Spacecraft that explore other worlds, like the moon or Mars, are all robotic. These robots include rovers & landers on the surface of other planets. The Mars rovers Spirit & Opportunity are examples of this kind of robot. Other robotic spacecraft fly by or orbit other worlds & study them from space. Cassini, the spacecraft that studies Saturn & its moons & rings, is this type of robot. The Voyager & Pioneer spacecraft now traveling outside Earth’s solar system are also robots.

Unlike the robotic arm on the space station, these robots are autonomous. That means they can work by themselves. They follow the commands people send. People use computers & powerful antennas to send messages to the spacecraft. The robots have antennas that receive the messages & transfer the commands telling them what to do into their computers. Then the robot will follow the commands.


How Does NASA Use Robotic Airplanes?
NASA uses many airplanes called UAVs. UAV stands for unmanned aerial vehicle. These planes do not carry pilots aboard them. Some UAVs are flown by remote control by pilots on the ground. Others can fly themselves, with only simple directions. UAVs provide many benefits. The planes can study dangerous places without risking human life. For example, UAVs might be used to take pictures of a volcano. A UAV also could fly for a very long time without the need to land. Since they do not carry a pilot, UAVs also can be smaller or more lightweight than they would with a person aboard.


How Can Robots Help Astronauts?
NASA is developing new robots that could help people in space. For example, one of these ideas is called Robonaut. Robonaut looks like the upper body of a person. It has a chest, head & arms. Robonaut could work outside a spacecraft, performing tasks like an astronaut on a spacewalk. With wheels or another way of moving, Robonaut also could work on the moon, or another world. Robonaut could work alongside astronauts & help them.

Another robot idea is called SPHERES. These are small robots that look a little like soccer balls. The current SPHERES are being used on the space station to test how well they can move in microgravity. Someday, similar robots could fly around inside the station helping astronauts.

NASA also is studying the possibility of other robots. For example, a small version of the station’s robotic arm could be used inside the station. A robot like that might help in an emergency. If an astronaut were seriously hurt, a doctor on Earth could control the robotic arm to perform surgery. This technology could help on Earth, as well. Doctors could use their expertise to help people in remote locations.

Robots also can be used as scouts to check out new areas to be explored. Scout robots can take photographs & measure the terrain. This helps scientists & engineers make better plans for exploring. Scout robots can be used to look for dangers & to find the best places to walk, drive or stop. This helps astronauts work more safely & quickly. Having humans & robots work together makes it easier to study other worlds.



## Docs/Links
|**Sections & pages**|
|:--|
|**【[Robot](robot.md)】**<br> [Rover](robot.md)|
|**【[Rover](robot.md)】**<br> **Mars:** … ┆ **Moon:** … ┆ **Venus:** [AREE](aree.md), [Zephyr](zephyr.md)|

1. Docs:
   1. **Rover:**
      1. [Концепция самоходного шасси марсохода ExoMars. ❐](f/rover/20180329_iki_exomars_rover_concept.djvu) (Москва, ИКИ РАН, 2018)
1. <https://en.wikipedia.org/wiki/Robotics>
1. **Rover:**
   1. <https://en.wikipedia.org/wiki/Rover_(space_exploration)>
   1. 2017.10.04 Хабр: Роботы стремятся к звёздам (<https://habr.com/ru/company/mailru/blog/407165>) — [archived ❐](f/archive/20171004_1.pdf) 2019.02.07


## The End

end of file
