# Солнечная постоянная
> 2019.07.20 [🚀](../../index/index.md) [despace](index.md) → **[Space](index.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Солнечная постоянная** — русскоязычный термин. **Solar constant** — англоязычный эквивалент.</small>

**Со́лнечная постоя́нная** — суммарная мощность солнечного излучения, проходящего через единичную площадку, ориентированную перпендикулярно потоку, на расстоянии одной астрономической единицы от Солнца вне земной атмосферы. По данным внеатмосферных измерений солнечная постоянная для Земли составляет 1 367 W/m², или 1.959 кал/㎝²·мин.



## Описание
Солнечная постоянная не является неизменной во времени величиной. На неё влияют 2 основных фактора: расстояние между планетой и Солнцем, изменяющееся в течение года по причине эллиптичности орбит (годичная вариация для Земли 6.9 % — от 1 412 W/m² в начале января до 1 321 W/m² в начале июля) и изменения солнечной активности. В соответствии с современными моделями развития Солнца, его светимость будет возрастать на ~1 % за 110 млн лет.


|**Планета**|**Солнечная<br> постоянная (W/m²)**|
|:--|:--|
|[Меркурий](mercury.md)|5 930 ‑ 13 680|
|[Venus](venus.md)|2 437 ‑ 2 505|
|[Земля](earth.md)|1 321 ‑ 1 412|
|[Луна](moon.md)|1 321 ‑ 1 412|
|[Марс](mars.md)|465 ‑ 678|
|[Юпитер](jupiter.md)|50|
|[Сатурн](saturn.md)|15|
|[Уран](uranus.md)|4|
|[Нептун](neptune.md)|2|
|[Плутон](pluto.md)|1|



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Space](index.md)】**<br> [Apparent magnitude](app_mag.md) ~~ [Astro.object](aob.md) ~~ [Blue Marble](earth.md) ~~ [Cosmic rays](ion_rad.md) ~~ [Ecliptic](ecliptic.md) ~~ [Escape velocity](esc_vel.md) ~~ [Health](health.md) ~~ [Hill sphere](hill_sphere.md) ~~ [Information](info.md) ~~ [Lagrangian points](l_points.md) ~~ [Near space](near_space.md) ~~ [Pale Blue Dot](earth.md) ~~ [Parallax](parallax.md) ~~ [Point Nemo](earth.md) ~~ [Silver Snoopy award](silver_snoopy_award.md) ~~ [Solar constant](solar_const.md) ~~ [Terminator](terminator.md) ~~ [Time](time.md) ~~ [Wormhole](wormhole.md) ┊ ··•·· **Solar system:** [Ariel](ariel.md) ~~ [Callisto](callisto.md) ~~ [Ceres](ceres.md) ~~ [Deimos](deimos.md) ~~ [Earth](earth.md) ~~ [Enceladus](enceladus.md) ~~ [Eris](eris.md) ~~ [Europa](europa.md) ~~ [Ganymede](ganymede.md) ~~ [Haumea](haumea.md) ~~ [Iapetus](iapetus.md) ~~ [Io](io.md) ~~ [Jupiter](jupiter.md) ~~ [Makemake](makemake.md) ~~ [Mars](mars.md) ~~ [Mercury](mercury.md) ~~ [Moon](moon.md) ~~ [Neptune](neptune.md) ~~ [Nereid](nereid.md) ~~ [Nibiru](nibiru.md) ~~ [Oberon](oberon.md) ~~ [Phobos](phobos.md) ~~ [Pluto](pluto.md) ~~ [Proteus](proteus.md) ~~ [Rhea](rhea.md) ~~ [Saturn](saturn.md) ~~ [Sedna](sedna.md) ~~ [Solar day](solar_day.md) ~~ [Sun](sun.md) ~~ [Titan](titan.md) ~~ [Titania](titania.md) ~~ [Triton](triton.md) ~~ [Umbriel](umbriel.md) ~~ [Uranus](uranus.md) ~~ [Venus](venus.md)|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Solar_constant>
1. <http://www.astronet.ru/db/msg/1188666>
1. <https://pvcdrom.pveducation.org/RU/SUNLIGHT/SPACE.HTM>


## The End

end of file
