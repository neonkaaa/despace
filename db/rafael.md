# Rafael
> 2019.08.13 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/r/rafael_logo1t.webp)](f/c/r/rafael_logo1.webp)|<intl-mkt@rafael.co.il>, +972(73)335-4444, Fax …;<br> *POB 2250, Haifa, 3102102 Israel*<br> <https://www.rafael.co.il> ~~ [FB ⎆](https://www.facebook.com/Rafael-Advanced-Defense-Systems-250896539197350) ~~ [IG ⎆](https://www.instagram.com/rafaeldefense) ~~ [LI ⎆](https://www.linkedin.com/company/rafael-advanced-defense-systems-official) ~~ [X ⎆](https://twitter.com/RAFAELdefense) ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/Rafael_Advanced_Defense_Systems)|
|:--|:--|
|**Business**|LV|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Rafael Advanced Defense Systems Ltd.** (Hebrew: רפאל - מערכות לחימה מתקדמות בע"מ), (「Rafael」 from Hebrew acronym of 「Authority for the Development of Armaments」 — רשות לפיתוח אמצעי לחימה‎) is an Israeli defense technology company. It was founded in 1948 as Israel’s National R&D Defense Laboratory for the development of weapons & military technology within the Israeli Ministry of Defense; in 2002 it was incorporated as a limited company. Rafael develops & produces weapons, military, & defense technologies for the Israel Defense Forces & for export abroad. All current projects are classified.



## The End

end of file
