# Выводимая масса
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [КК](sc.md), [РБ](lv.md), **[LV](lv.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Выводимая масса** — русскоязычный термин. **Throw-weight** — англоязычный эквивалент.</small>

Выведение с 「[Восточного](spaceport.md)」 при РН 「[Ангара‑А5](angara.md)」 и РБ [КВТК](квтк.md) с параметрами:  
h<sub>A</sub> = 1 325 000 ㎞,  
h<sub>П</sub> = 1 614 ㎞,  
i = 51.5°,  
ω = 0°,  
выводимая масса = 7 300 ㎏, включая массу переходной системы

|**Место**|**РН**|**РБ**|**ГО**|**[НОО](nnb.md), ㎏**|**ГСО, ㎏**|**Перелёт<br> к Луне, ㎏**|
|:--|:--|:--|:--|:--|:--|:--|
|[Байконур](spaceport.md)|[Союз‑2.1б](soyuz.md)|[Фрегат](fregat.md)|1.750|6 500 ‑ 8 250| |2 200|
| | | |14С737|6 500 ‑ 8 250| |2 140|
| | | |81 КС|6 500 ‑ 8 250| |2 130|
|[Восточный](spaceport.md)|[Союз‑2.1б](soyuz.md)|[Фрегат](fregat.md)|81 КС| | | |



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**`Космический комплекс (КК):`**<br> [Выводимая масса](throw_weight.md) ~~ [ГО и ПхО](lv.md) ~~ [Класс чистоты](clean_lvl.md) ~~ [Контейнеры для транспортировки](ship_contain.md) ~~ [СЧ](sui.md)|
|**`Разгонный блок (РБ):`**<br> [Выводимая масса](throw_weight.md) ~~ [Космическая головная часть](lv.md) <br>~ ~ ~ ~ ~<br> **РФ, СНГ:** [Бриз](бриз.md) ~~ [Блок Д](блок_д.md) ~~ [Волга](волга.md) ~~ [КВТК](квтк.md) ~~ [Фрегат](fregat.md)|
|**【[Launch vehicle (LV)](lv.md)】**<br> [Безракетный космический запуск](nrs.md) ~~ [Выводимая масса](throw_weight.md) ~~ [Космическая головная часть](lv.md) ~~ [㎆Р](icbm.md) ~~ [РКН](lv.md)<br>~ ~ ~ ~ ~<br> **Европа:**  [Arian](arian.md) ~~ [Vega](vega.md) / **Израиль:** [Shavit](shavit.md) / **Индия:** [GSLV](gslv.md) ~~ [PSLV](pslv.md) / **Китай:** [Long March](long_march.md) / **Корея с.:** [Unha](unha.md) / **Корея ю.:** *([Naro-1](kslv.md))* / **РФ, СНГ:** [Ангара](angara.md) ~~ [Протон](proton.md) ~~ [Союз](soyuz.md) ~~ [СТК](yenisei.md) ~~ [Зенит](zenit.md) *([Корона](korona.md) ~~ [Н-1](n_1.md) ~~ [Р-1](r_7.md) ~~ [Энергия](energia.md))* / **США:** [Antares](antares.md) ~~ [Atlas](atlas.md) ~~ [BFR](bfr.md) ~~ [Delta](delta.md) ~~ [Electron](electron.md) ~~ [Falcon](falcon.md) ~~ [Firefly Alpha](firefly_alpha.md) ~~ [LauncherOne](launcherone.md) ~~ [New Armstrong](new_armstrong.md) ~~ [New Glenn](new_glenn.md) ~~ [Minotaur](minotaur.md) ~~ [Pegasus](pegasus.md) ~~ [Shuttle](shuttle.md) ~~ [SLS](sls.md) ~~ [Vulcan](vulcan.md) *([Saturn](saturn_lv.md) ~~ [Sea Dragon](sea_dragon.md))* / **Япония:** [Epsilon](epsilon.md) ~~ [H2](h2.md) ~~ [H3](h3.md)|

1. Docs: …
1. <…>


## The End

end of file
