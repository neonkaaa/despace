# WeSpace Technologies Ltd.
> 20211.11.15 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/w/wespace_tech_logo1t.webp)](f/c/w/wespace_tech_logo1.webp)|<info@wespacetech.com>, <mark>nophone</mark>, Fax …;<br> *HaNofar St 1, Ra'anana, Israel*<br> <http://www.wespacetech.com>|
|:--|:--|
|**Business**|R&D robots (drones, rovers), operation, data|
|**Mission**|To develop a leading‑edge profitable company for space exploration vehicles|
|**Vision**|Our goal is to position WeSpace Technologies Ltd. as a leading innovator of space technology, enabling exploration, fostering deep‑space discoveries|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**WeSpace Technologies Ltd.** is a company comprised of an innovative engineering team & creative business thinking staff. The founder, Yigal Harel, was served as the program director of the 「Beresheet」 program — the 1st Israeli spacecraft to the moon at SpaceIL.

We specialize in Autonomous Mobility Space Platforms to retrieve valuable information about in‑situ resources (water, metals, minerals, etc.) as well as signs of life & habitation potential in deep space, from hard‑to‑reach areas that are currently inaccessible to any robotic systems (e.g. PSRs). Our vehicles will carry scientific & engineering payloads, as a service for its customers, with the aim of taking part in the new emerging huge space economy.

Mining the Moon has become a topic of interest to the entire space community as well as to commercial companies. The Moon will become 「an intergalactic petrol station」 as it has the in‑situ resources needed for rocket fuel, i.e. hydrogen & oxygen, meaning, spacecraft, rockets, & space vehicles will be able to travel further into space, with an unlimited supply of fuel, as well as restarting satellites which are currently hibernating (for running out of fuel). This deep exploration will allow human beings access to space’s many resources that can be used to benefit our planet.

The   company’s   core   technology   comprises   two robotic  platforms — a  spaceborne  drone (「HopLa「) designed  for  flight  &  hoveringabove  the  surface, &  a  four‑wheeled  SubTerranean  Rover  (STAR) engineered for versatile exploration.

<p style="page-break-after:always"> </p>

## The End

end of file
