# MSSS
> 2019.06.14 [🚀](../../index/index.md) [despace](index.md) → [MSSS](msss.md), **[Events](event.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Московский симпозиум по солнечной системе** — русскоязычный термин, не имеющий аналога в английском языке. **The Moscow Solar System Symposium** — дословный перевод с русского на английский.</small>

Starting from 2010, the [Space Research Institute](iki_ras.md) holds annual international symposia on Solar system exploration. Main topics of these symposia include wide range of problems related to formation and evolution of Solar system, planetary systems of other stars; exploration of Solar system planets, their moons, small bodies; interplanetary environment, astrobiology problems. Experimental planetary studies, science instruments and preparation for space missions are also considered at these symposia.

<small>

|**Event**|**Dates**|**Comments**|
|:--|:--|:--|
|[11th](msss_11.md)|2020.10.05 ‑ 09|<https://ms2020.cosmos.ru>|
|[10th](msss_10.md)|2019.10.07 ‑ 11|<https://ms2019.cosmos.ru>|
|[9th](msss_9.md)|2018.10.08 ‑ 12|<https://ms2018.cosmos.ru>|
|[8th](msss_8.md)|2017.10.09 ‑ 13|<https://ms2017.cosmos.ru>|
|[7th](msss_7.md)|2016.10.10 ‑ 14|<https://ms2016.cosmos.ru>|
|[6th](msss_6.md)|2015.10.05 ‑ 09|<https://ms2015.cosmos.ru>|
|[5th](msss_5.md)|2014.10.13 ‑ 18|<https://ms2014.cosmos.ru>|
|[4th](msss_4.md)|2013.10.14 ‑ 18|<https://ms2013.cosmos.ru>|
|[3rd](msss_3.md)|2012.10.08 ‑ 12|<https://ms2012.cosmos.ru>|
|[2nd](msss_2.md)|2011.10.10 ‑ 14|<https://ms2011.cosmos.ru>|
|[1st](msss_1.md)|2010.10.11 ‑ 15|<https://ms2010.cosmos.ru>|

</small>



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Events](event.md)】**<br> **Meetings:** [AGU](agu.md) ~~ [CGMS](cgms.md) ~~ [COSPAR](cospar.md) ~~ [DPS](dps.md) ~~ [EGU](egu.md) ~~ [EPSC](epsc.md) ~~ [FHS](fhs.md) ~~ [IPDW](ipdw.md) ~~ [IVC](ivc.md) ~~ [JpGU](jpgu.md) ~~ [LPSC](lpsc.md) ~~ [MAKS](maks.md) ~~ [MSSS](msss.md) ~~ [NIAC](niac_program.md) ~~ [VEXAG](vexag.md) ~~ [WSI](wsi.md) ┊ ··•·· **Contests:** [Google Lunar X Prize](google_lunar_x_prize.md)|

1. Docs: …
1. <…>


## The End

end of file
