# Задел
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [SGM](sc.md), [Проекты](project.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Задел** — русскоязычный термин. **Margin** — англоязычный эквивалент.</small>

**Задел** — это:

1. запас сырья, деталей, полуфабрикатов, комплектующих, необходимых для обеспечения бесперебойного производственного процесса или строительства. (Словарь бизнес‑терминов. Академик.ру. 2001.)
1. наработка; часть работы, выполненная заранее, в расчёте на её использование в будущем. (<https://ru.wiktionary.org/wiki/задел>)

Не путайте с [унификацией](commonality.md).



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Structures, gears, materials (SGM)](sc.md)】**<br> [Гермоконтейнер](гермоконтейнер.md) ~~ [Датчик](sensor.md) ~~ [Задел](margin.md) ~~ [Изделие](unit.md) ~~ [Испарение материалов](matc.md) ~~ [Кавитация](cavitation.md) ~~ [КЗУ](cinu.md) (ВБУ КТ) ~~ [КХГ](cgs.md) ~~ [Контейнеры для транспортировки](ship_contain.md) ~~ [Крейцкопф](crosshead.md) ~~ [Номинал](nominal.md) ~~ [ПУС](lag.md) ~~ [ПНА, ПОНА, ПСНА](devd.md) ~~ [Резерв](reserve.md) ~~ [Слайс](слайс.md) ~~ [ТСП](tsp.md) ~~ [Типичные формы КА](sc.md) ~~ [Толкатель](толкатель.md) ~~ [Унификация](commonality.md)|

1. Docs: …
1. <…>


## The End

end of file
