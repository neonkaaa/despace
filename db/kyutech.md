# Kyutech
> 2020.07.18 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/k/kyutech_logo1t.webp)](f/c/k/kyutech_logo1.webp)|<kok-ryugaku@jimu.kyutech.ac.jp>, +81-(0)93-884-3061, Fax +81-(0)93-884-3059;<br> *1-1 Sensui-cho, Tobata-ku, Kitakyushu-shi, Fukuoka, 804-8550, Japan*<br> <https://www.kyutech.ac.jp> ~~ [LI ⎆](https://www.linkedin.com/company/kyutech-institute-of-technology)|
|:--|:--|
|**Business**|Scientific & technological institute|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Kyushu Institute of Technology** (九州工業大学, Kyūshū Kōgyō Daigaku) is one of the 87 national universities in Japan. Located in Fukuoka Prefecture on the island of Kyushu, it is dedicated to education & research in the fields of science & technology. It’s often abbreviated to **KIT** & sometimes to **Kyutech**.

Notable space activities:

- <https://kyutech-cent.net/seic/about.html> Space engineering international course (SEIC)
- <https://www.kyutech.ac.jp/english/academics/e/k03.html> Department of Space Systems Engineering



## The End

end of file
