# KASI
> 2020.07.24 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/k/kasi_logo1t.webp)](f/c/k/kasi_logo1.webp)|<mark>noemail</mark>, +82-42-865-3332, Fax +82-42-861-5610;<br> *Daedeokdae-ro 776, Yuseong-gu, Daejeon 34055, Korea*<br> <https://www.kasi.re.kr> ~~ [LI ⎆](https://www.linkedin.com/company/korea-astronomy-and-space-science-institute) ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/Korea_Astronomy_and_Space_Science_Institute)|
|:--|:--|
|**Business**|Research institute in astronomy & space science|
|**Mission**|…|
|**Vision**|World’s Leading National Astronomy & Space Science Research Institute|
|**Values**|…|
|**[MGMT](mgmt.md)**|・Vice-president — Wonyong Han|

The **Korea Astronomy & Space Science Institute (KASI)** is the national research institute in astronomy & space science of South Korea funded by the South Korean Government. Its headquarters are located in Daejeon, in the Daedeok Science Town. Research at KASI covers main areas of modern astronomy, incl. Optical Astronomy, Radio Astronomy, Space Science, & Theoretical Astronomy. Founded 1974.09.

Function:

- Performing a key role in Astronomy & Space Science
- Developing & operating research facilities for Astronomy & Space Science
- Developing technologies related to Space Situational Awareness
- Managing national astronomical almanac & Korean Standard Time
- Performing public outreach & citizen science for Astronomy & Space Science
- Collaborating R&D projects with public & private sectors
- Fostering R&D manpower & establishing policy for Astronomy & Space Science



## The End

end of file
