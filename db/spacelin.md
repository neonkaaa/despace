# Лин Индастриал
> 2019.04.01 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/l/spacelin_logo1t.webp)](f/c/l/spacelin_logo1.webp)|<mail@spacelin.ru>, <mark>nophone</mark>, Fax …;<br> *Россия, 115035, Москва, ул. Садовническая, д. 76/71, стр. 5*<br> <http://spacelin.ru>・ <https://vk.com/lin_industrial>・ <https://users.livejournal.com/---lin--->|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Лин Индастриал** — разработчик ракет‑носителей и суборбитальных кораблей. Создано 14.01.2014. Нынешняя продукция:

1. Сверхлёгкая РН 「Таймыр」



## The End

end of file
