# Болото
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md), [Control](control.md), [Экология](ecology.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Болото** — русскоязычный термин. **Swamp** — англоязычный эквивалент.</small>

**Боло́то** — участок ландшафта, характеризующийся избыточным увлажнением, влаголюбивым живым напочвенным покровом. Для болота характерно отложение на поверхности почвы неполностью разложившегося органического вещества, превращающегося в дальнейшем в торф.



## Описание
В России распространены на севере и в центре Европейской части (в том числе в районе Москвы и Подмосковья), в Западной Сибири, на Камчатке.

Болота возникают двумя основными путями: из‑за заболачивания почвы или же из‑за зарастания водоёмов.

|Болото на Валдае близ озера Селигер|Болото на южной окраине Санкт‑Петербурга (бассейн Невы)|[РКС](rss.md)|
|:--|:--|:--|
|[![](f/c/boloto_pic01t.webp)](f/c/boloto_pic01.webp)|[![](f/c/boloto_pic02t.webp)](f/c/boloto_pic02.webp)|[![](f/c/r/rks_officet.webp)](f/c/r/rks_office.webp)|



## В ноосфере
В ноосфере болото склонно к образованию при любом из следующих условий:

1. в государственных [предприятиях](contact.md), институтах и университетах;
1. при большом количестве [управленцев](manager.md) в организации;
1. при нечётком [разграничении обязанностей](orgstruct.md) в организации;
1. при завязывании вопросов на [конкретных персонах](sw_sys.md).

Результатом образования болота в организации являются:

1. увеличенное время рассмотрения и согласования документации;
1. потеря документации;
1. несвоевременное выполнение работ;
1. увеличение количества живности.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[Control](Control.md)】**<br> [Ad hoc](ad_hoc.md) ~~ [Business travel](business_travel.md) ~~ [Chief designers council](cocd.md) ~~ [CML](trl.md) ~~ [Competence](competence.md) ~~ [Confident](confident.md) ~~ [Consp.theory](consp_theory.md) ~~ [Control sys. (CS)](cs.md) ~~ [Coordinate system](coord_sys.md) ~~ [Curator](curator.md) ~~ [Designer’s supervision](des_spv.md) ~~ [E‑sig](esig.md) ~~ [Engineer](se.md) ~~ [Errand](errand.md) ~~ [Federal law](fed_law.md) ~~ [Federal TP](fed_tp.md) ~~ [Federal SP](fed_sp.md) ~~ [GNC](gnc.md) ~~ [Gravity assist](gravass.md) ~~ [Industrial archaeology](ind_arch.md) ~~ [Instruction](instruction.md) ~~ [Lean manuf.](lean_man.md) ~~ [Lifetime](lifetime.md) ~~ [Manager](manager.md) ~~ [MBSE](se.md) ~~ [Meeting](meeting.md) ~~ [MCC](sc.md) ~~ [MIC](mic.md) ~~ [MML](mml.md) ~~ [MoU](contract.md) ~~ [Nav. & ballistics (NB)](nnb.md) ~~ [Nonprofit org.](nonprof_org.md) ~~ [NX](nx.md) ~~ [Oberth effect](oberth_eff.md) ~~ [Org.structure](orgstruct.md) ~~ [Outcomes commission](outccom.md) ~~ [Patent](patent.md) ~~ [Peter prin.](peter_principle.md) ~~ [Plan](plan.md) ~~ [PMBok](pmbok.md) ~~ [Quorum](quorum.md) ~~ [R&D management](mgmt.md) ~~ [R&D support](rnd_support.md) ~~ [Recursion](recurs.md) ~~ [Schulze_method](schulze_method.md) ~~ [Sci'N'Tech activities](st_act.md) ~~ [Sci'N'Tech council](satc.md) ~~ [Single-window system](sw_sys.md) ~~ [Situ.leadership](situ_leadership.md) ~~ [Skunk works](se.md) ~~ [State arm. plan](plan_sa.md) ~~ [Swamp](swamp.md) ~~ [Teamcenter](teamcenter.md) ~~ [Tennis racket theorem](tr_theorem.md) ~~ [TRIZ](triz.md) ~~ [TRL](trl.md) ~~ [V‑model](v_model.md) ~~ [Veto](veto.md) ~~ [Workflow](workflow.md) ~~ [Workgroup](wg.md)|

1. Docs: …
1. <https://ru.wikipedia.org/wiki/Болото>
1. <https://ru.wikipedia.org/wiki/Ноосфера>
1. <http://lurkmore.to/Армейский_способ>
1. <https://masterok.livejournal.com/2667699.html>


## The End

end of file
