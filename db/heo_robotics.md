# HEO Robotics
> 2022.01.19 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/h/heo_robotics_logo1t.webp)](f/c/h/heo_robotics_logo1.webp)|<info@heo-robotics.com>, <mark>nophone</mark>, Fax …;<br> *…*<br> <https://www.heo-robotics.com> ~~ [FB ⎆](http://www.facebook.com/heorobotics) ~~ [LI ⎆](https://www.linkedin.com/company/high-earth-orbit-robotics) ~~ [X ⎆](http://www.twitter.com/heorobotics)|
|:--|:--|
|**Business**|Acquire imagery of sats, space‑debris, resource‑rich asteroids with sats|
|**Mission**|…|
|**Vision**|…|
|**Values**|We value good space citizenship. We’re passionate about responsible custodianship, sustainability & trust. We believe that space should be easy & transparent.|
|**[MGMT](mgmt.md)**|…|

**High Earth Orbit Robotics (HEO Robotics)** helps satellite operators & governments visually monitor their spacecraft & other space objects they care about. HEO Robotics monitors these space objects using a series of Earth observation satellites, which are transformed into in‑orbit inspection platforms using HEO’s proprietary software.

Product: **HEO Inspect** is the world’s 1st in‑orbit satellite inspection service. Using satellites equipped with high quality panchromatic, multispectral & hyperspectral imagers, we can capture sub 0.5 m resolution imagery of your LEO assets. High resolution imagery allows us to verify a satellite’s identity, deploy status, among many other things.

<p style="page-break-after:always"> </p>

## The End

end of file
