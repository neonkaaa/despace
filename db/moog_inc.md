# Moog Inc.
> 2019.08.05 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/m/moog_inc_logo1t.webp)](f/c/m/moog_inc_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *East Aurora, New York, USA*<br> <http://www.moog.com> ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/Moog_Inc.)|
|:--|:--|
|**Business**|…|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Moog Inc.** — американский разработчик и производитель подвижных и жидкостных компонентов и систем управления в области авиации, космоса, наземной индустрии и медицинского оборудования. Штаб‑квартира расположена в East Aurora, Нью‑Йорк, США. Подразделения и фабрики расположены в 26 странах. Компания основана в апреле 1950 года Биллом Мугом. Нынешняя продукция:

1. ракетные двигатели [LEROS](leros.md);
1. системы управления для турбин, самолётов, ракет‑носителей, космических аппаратов;
1. медицинские насосы.



## The End

end of file
