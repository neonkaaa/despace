# Proposal
> 2020.04.16 [🚀](../../index/index.md) [despace](index.md) → [Doc](doc.md), [R&D](rnd.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---
> <small>**Proposal** — EN term. **Предложение** — rough RU analogue.</small>

<mark>TBD</mark>

NASA proposals must provide for a complete mission, except the launch. It is a competitive process. Only typically for or five out of two dozen are selected for phase A study. That study requires a report, a site visit & after evaluation by a large scientific & technical panel, one or two are selected for launch.

ESA follows a comparable path. The missions are selected according to scientific priorities & by agency’s vision, so the 1st selection is heavy on mission science objectives but also implementation to a lesser degree & the emphasis is switched for the second round.

Your proposal may look quite different based on what agency wants & if it is competed or directed.


## ESA

A Proposal is a reply for ESA’s call for proposals stating how exactly you would like to fulfil the call. In common, a call for proposals contains brief description of a mission, and a set of needs, goals, objectives, tasks, & requirements. A Bidder should perform a rapid design, define several options, & confirm & describe (in a set of documents) that he can perform such activity better than others.


### Checklist — Technical proposal
> 2014.10.17 This info is tailored for the establishment of proposals for R&D Contracts. It doesn’t take into account specific requirements & constraints applicable to flight hardware proposals.  
> <https://www.esa.int/About_Us/Business_with_ESA/How_to_do/Preparing_a_technical_proposal>

The technical part of the proposal (the *Technical Proposal (TP)*) is essential to support the technical evaluation & addresses the 2 following *Invitations to Tender (ITT)* criteria:

1. Understanding of the requirements & objectives & discussion of problem areas
1. Quality & suitability of proposed programme of work & adequacy of engineering approach

Bidders might be unclear between what should already be available with the proposal & what is to be done as part of the activity itself. Preparing the TP requires a thorough analysis of the objectives, requirements, solutions, problem areas & this leads to a proposed 「best」 baseline concept & to the detailed proposed programme of work to meet all objectives from the SOW.

**The typical technical elements to be provided in the proposal.**

1. Special conditions of tender or the cover letter may indicate specific elements to be provided. E.g., it may be a request to provide a detailed list of the testing facilities: 「ESA requires this info because doing this type of testing is obviously not straightforward work & they want this info because of the impact on the activity」. Pay attention to such special conditions, they are there for a reason.
1. Understanding of the objectives (what is the matter to be developed, to which maturity level shall it be developed/tested, what are the final deliverables, etc.)
1. Current state of the art (what is the tech. context, are there existing solutions that may be used as a starting point, are there any similar matters in other areas, etc.)
1. Discussion of possible concepts & trade‑off (at least 2 ‑ 3 solutions to be elaborated during the activity & the technical state of the art, what are the criteria to compare these solutions & how do they actually compare when performing a 1st trade‑off)
1. Critical assessment of requirements & possible limitations with corresponding justifications (analyse requirements & confirm compliance, propose limitations & new requirements, justify in detail all non compliances or partial non compliances)
1. Discussion of problem areas (what are the technical challenges that  are expected to be addressed during the R&D, what are the mitigation approaches to address them)
1. Identification of the baseline (what is the 「best」 solution from the ones explored)
1. Detailed description of the baseline (what are in the detail the specification, work logic, work breakdown structure & work packages)
1. Compliance matrix w/ comments (what is for each requirement the declared status of compliance to be able to judge how the baseline address each of the SOW requirements)
1. Elements to allow the assessment of the credibility of the proposed programme of work (e.g., is there a back‑up plan in case the selected baseline is not retained during the activity, are risks & problem areas well tackled, are all constraints built‑in in the overall programme of work e.g. a constraint for availability of a specific facility)

**A good technical part of a proposal:**

1. addresses the technical discussion & the review of requirements & does not jump directly to a proposed baseline concept without explaining the 「why this concept」
1. defines very clearly what ESA gets out of the activity
1. has all WP’s well identified in the planning & costing
1. is consistent & balanced in all chapters
1. is consistent with the Work Breakdown Structure (WBS) & Work Package (WP) descriptions
1. is easy to read & complete, is catching the eye, within limited number of pages
1. is well structured & shows a logical development
1. reflects the preliminary tech.work (1st iteration) based of the SOW
1. reflects the technical knowledge of the proposed team & key personnel

**Avoid:**

1. inconsistency between TP & management/cost (e.g. work proposed doesn’t fit man‑hours)
1. inconsistency between WBS/WP & compliance matrix & technical description
1. inconsistent subcontractor involvement
1. lengthy philosophical considerations
1. narrow single solution approach
1. pure repetition of the SOW (but should reflect this technical elaboration)



### Process

A short step‑by‑step process to perform. Created out of ESA’s standards, guides, etc.

1. Obtain documents from ITT
1. Define number of books to deliver & their structure
1. Define expectations, incl.:
   1. statement of work
   1. objectives
   1. requirements + special conditions
   1. limitations
   1. tasks
   1. persons/companies in charge
1. Define schedule, incl.:
   1. timeline
   1. key points
   1. deliverables
   1. work packages + work breakdown structure
   1. cost
1. Make a brief analysis to meet schedule and expectations, incl.:
   1. current state of the art
   1. what solutions can be proposed & why
   1. how can you meet the schedule & expectations + complience matrix
   1. expected risks and problems
   1. what is the baseline & why
1. Describe the results
   1. laconically
   1. structured
   1. within limited number of pages



## Docs/Links
|**Sections & pages**|
|:--|
|**【[Documents](doc.md)】**<br> **Схема:** [КСС](ксс.md) ~~ [ПГС](пгс.md) ~~ [ПЛИС](плис.md) ~~ [СхД](draw.md) ~~ [СхО](draw.md) ~~ [СхПЗ](draw.md) ~~ [СхЧ](draw.md) ~~ [СхЭ](draw.md)<br> [Interface](interface.md) ~~ [Mission proposal](proposal.md)|
|**【[R&D](rnd.md)】**<br> [Design review](design_review.md) ~~ [Management](mgmt.md) ~~ [MBSE](se.md) ~~ [Proposal](proposal.md) ~~ [Test](test.md) ~~ [V‑model](v_model.md) ~~ [Validation, Verification](vnv.md)<br> [АП](rnd_ap.md) ~~ [ЛИ](rnd_e.md) ~~ [Макеты, НЭО](test.md) ~~ [НИР](rnd_0.md) ~~ [РКД (РРД)](rnd_rkd.md) ~~ [ТП](rnd_tp.md) ~~ [ЭП](rnd_ep.md)|

1. Docs:
   1. [How to write a good proposal for ESA PECS/NMS programmes ⎆](f/doc/20200101_how_to_write_a_good_proposal_pecs_nms_2.pdf)
1. <https://earth.esa.int/web/guest/pi-community>


## The End

end of file
