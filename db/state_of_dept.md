# Положение о структурном подразделении
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → **[Док.](doc.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Положение о структурном подразделении (ПОСП)** — русскоязычный термин, не имеющий аналога в английском языке. **State of department** — дословный перевод с русского на английский.</small>

**Положение о структурном подразделении (ПОСП)** — документ, в котором определяются: порядок создания (образования) подразделения; правовое положение подразделения в структуре организации; структура подразделения; задачи, функции, права и ответственность подразделения; порядок взаимодействия подразделения с иными структурными единицами организации.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[](.md)】**<br> <mark>NOCAT</mark>|

1. Docs: …
1. <http://www.hr-portal.ru/pages/poloj/psp.php>


## The End

end of file
