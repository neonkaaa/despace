# RTG (a list)
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [RTG](rtg.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

A list of [Soil sampling systems](sss.md).

## Current



### RHU — TB-8,5 (RU)

**TB‑8,5** *(ru. ТБ‑8,5)* — [radioisotope heater unit](rtg.md) (RHU). Manufacturer [RFYATS-VNIIEF](vniief.md). Designed in 2010.

|**Characteristics**|**(TB-8,5)**|
|:--|:--|
|Composition| |
|Consumption, W|—|
|Dimensions, ㎜|60×⌀40|
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|…(1)) / 28 032 (3.2)|
|Mass, ㎏|0.195|
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)|0.998|
|[Thermal](tcs.md), ℃|−150 ‑ +150|
|[TRL](trl.md)|9|
|[Voltage](sps.md), V| |
|**【Specific】**|~ ~ ~ ~ ~ |
|Degradation of el. power in the EoL, %|—|
|Degradation of thermal power in the EoL, %|5|
|Equivalent dose of [radiation](ion_rad.md)/1 m, m㏜/h (㏉/s)|0.0055 (0.0914)|
|Efficiency, %| |
|Power (electrical), W|—|
|Power (thermal), W|8.5±0.5|
|Radioactive element|²³⁸Pu|
|Specific energy, W·h/㎏| |

**Notes:**

1. …
1. **Applicability:** [Luna‑25](луна_25.md) ~~ [Luna‑27](луна_27.md)



### RTG — RITEG-238-6,5/3 (RU)

**RITEG-238-6,5/3** *(ru. РИТЭГ-238-6,5/3)* — [radioisotope thermoelectric generator](rtg.md) (RTG). Manufacturer [RFYATS-VNIIEF](vniief.md). Designed in 2010. Price 10⁹ ₽ (2018.01.10).

|**Characteristics**|**(RITEG-238-6,5/3)**|
|:--|:--|
|Composition| |
|Consumption, W| |
|Dimensions, ㎜|245×⌀180|
|[Interfaces](interface.md)| |
|[Lifetime](lifetime.md), h(y)|… (1) / 45 552 (5.2)|
|Mass, ㎏|6.7 |
|[Overload](vibration.md), Grms| |
|[Radiation](ion_rad.md), ㏉(㎭)| |
|[Reliability](qm.md)|0.98 |
|[Thermal](tcs.md), ℃|−50 ‑ +80|
|[TRL](trl.md)|9|
|[Voltage](sps.md), V|3.5 ± 0.5|
|**【Specific】**|~ ~ ~ ~ ~ |
|Degradation of el. power in the EoL, %|15|
|Degradation of thermal power in the EoL, %|5|
|Efficiency, %|3.8 ‑ 5.2|
|Equivalent dose of [radiation](ion_rad.md)/1 m, m㏜/h (㏉/s)|0.08 (1.33)|
|Power (electrical), W|5 ‑ 6.5|
|Power (thermal), W|125 ‑ 145|
|Radioactive element|²³⁸Pu|
|Specific energy, W·h/㎏|0.97|

**Notes:**

1. …
1. **Applicability:** [Luna‑25](луна_25.md) ~~ [Luna‑27](луна_27.md)



## Archive



## Docs/Links
|**Sections & pages**|
|:--|
|**【[Radioisotope thermoelectric generator (RTG)](rtg.md)】**<br> … <br>~ ~ ~ ~ ~<br> **RU:** [RITEG-238-6,5/3](rtg_lst.md) ~~ [TB-8,5](rtg_lst.md)|

1. Docs: …
1. <…>


## The End

end of file
