# Hokkaido Satellite Co., Ltd.
> 2021.12.02 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c//_logo1t.webp)](f/c//_logo1.webp)|<mark>noemail</mark>, <mark>nophone</mark>, Fax …;<br> *…*<br> <https://www.campuscreate.com/global/seeds-list/global-15>|
|:--|:--|
|**Business**|Cameras|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

**Hokkaido Satellite Co., Ltd.** is a venture company in university. Founded in 2004.

We have developed a multispectral camera to extract and analyze the reflection spectrum of any (6 bands) (+ RGB: 3 bands) in a wide range of wavelengths from visible to near‑infrared (350 ‑ 1 050 ㎚). We can see the material properties that we couldn’t see from our own spectral functions. It’s cheaper than a hyper‑spectral camera or an overseas multi‑spectral camera. It can be applied to various uses such as inspection and analysis. We offer flexible suggestions to customers, such as equipment sales, customization, etc.

<p style="page-break-after:always"> </p>

## The End

end of file
