# ГОГУ
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → [Control](control.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---


> <small>**Главная оперативная группа управления (ГОГУ)** — русскоязычный термин, не имеющий аналога в английском языке. **Head operational task group (HOTG)** — дословный перевод с русского на английский.</small>

**Главная оперативная группа управления (ГОГУ)** — общее название исполнительного органа Госкомиссии при проведении [лётных испытаний](rnd_e.md) космического [комплекса (системы)](sc.md).

Представляет собой назначаемую отдельным Положением по каждому проекту группу специалистов из различных организаций. В состав ГОГУ входят представители Главного конструктора системы, головного НИИ государственного заказчика, потребителя космической продукции, главных конструкторов отдельных элементов системы, главных конструкторов привлекаемых средств [НКПОР](sc.md), участвующих в совместной разработке системы и проведении лётных испытаний.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[](.md)】**<br> <mark>NOCAT</mark>|

1. Docs: …
1. 2009 Научные чтения Циолковского (<http://readings.gmik.ru/lecture/2009-GLAVNAYA-OPERATIVNAYA-GRUPPA-UPRAVLENIYA-GOGU-ISTORIYA-SOZDANIYA-I-DEYATELNOSTI>) — [archived ❐](f/archive/20090101_1.pdf) 2019.02.18


## The End

end of file
