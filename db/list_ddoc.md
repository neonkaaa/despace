# Перечень (комплектность) конструкторской документации
> 2019.05.12 [🚀](../../index/index.md) [despace](index.md) → **[КД](doc.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Перечень (комплектность) конструкторской документации** — русскоязычный термин, не имеющий аналога в английском языке. **List of design documentation** — дословный перевод с русского на английский.</small>

**Перечень (комплектность) конструкторской документации** — документ, включающий в себя список планируемой к выпуску [конструкторской документации](doc.md) в рамках разработки ОКР.

Не путать с [Перечнем документации](list_doc.md)!



## Разработка и согласование
|**Этап**|**Наименование документа**|**Разраб.**|**Согласует**|**Утверждает**|
|:--|:--|:--|:--|:--|
|[НИР](rnd_0.md)|—|—|—|—|
|[АП](rnd_ap.md)|—|—|—|—|
|[ЭП](rnd_ep.md)|—|—|—|—|
|[ТП](rnd_tp.md)|Перечень (комплектность) КД|Исполнитель.|+|+|
|[РКД](ркд.md)|Перечень (комплектность) КД|Исполнитель.|+|+|
|[Макеты, НЭО](test.md)|—|—|—|—|
|[ЛИ](rnd_e.md)|—|—|—|—|

В соответствии с [ГОСТ 15.203](гост_15_203.md) п.5.3.7 и [ГОСТ 2.902](гост_2_902.md) разработка Перечня производится на этапе Технического проекта (ТП) и представляется в составе ТП Заказчику. В случае отсутствия этапа ТП, разработка производится на [этапе РРД](rnd_rkd.md).



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[](.md)】**<br> <mark>NOCAT</mark>|

1. Docs:
   1. [ГОСТ 15.203](гост_15_203.md)
   1. [ГОСТ 2.902](гост_2_902.md)
1. <…>


## The End

end of file
