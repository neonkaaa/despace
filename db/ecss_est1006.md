# ECSS‑E‑ST‑10‑06C (06‑Mar‑2009)
> 2009.03.06 [🚀](../../index/index.md) [despace](index.md) → [Doc](doc.md), [ECSS](ecss.md), [SC](sc.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

## 1. Introduction & Scope

**「Technical requirements specification.」**

This Standard is intended to be applied together for the management, engineering & product assurance in space projects & applications. ECSS is a cooperative effort of the European Space Agency, national space agencies & European industry associations for the purpose of developing & maintaining common standards. Requirements in this Standard are defined in terms of what shall be accomplished, rather than in terms of how to organize & perform the necessary work. This allows existing organizational structures & methods to be applied where they are effective, & for the structures & methods to evolve as necessary without rewriting the standards.

This Standard has been prepared by the ECSS‑E‑ST‑10‑06 Working Group, reviewed by the ECSS Executive Secretariat & approved by the ECSS Technical Authority.

ECSS does not provide any warranty whatsoever, whether expressed, implied, or statutory, including, but not limited to, any warranty of merchantability or fitness for a particular purpose or any warranty that the contents of the item are error‑free. In no respect shall ECSS incur any liability for any damages, including, but not limited to, direct, indirect, special, or consequential damages arising out of, resulting from, or in any way connected to the use of this Standard, whether or not based upon warranty, business agreement, tort, or otherwise; whether or not injury was sustained by persons or property or otherwise; & whether or not loss was sustained from, or arose out of, the results of, the item, or any services that may be provided by ECSS.

This Standard addresses the process for & the content of the **Technical requirements specification (TS)**.

This document is an adaptation of ISO 21351 「Space systems — Functional & technical specifications」 to the ECSS context.

This Standard provides an overview of the purposes & positions of the technical requirements specification, defines the different types of requirements, & defines requirements on the TS & on its requirements.

This Standard is applicable to all types of space systems, all product elements, & projects.

This standard may be tailored for the specific characteristic & constraints of a space project in conformance with ECSS‑S‑ST‑00.


## 2. Normative references & Bibliography
The following normative documents contain provisions which, through reference in this text, constitute provisions of this ECSS Standard. For dated references, subsequent amendments to, or revision of any of these publications, do not apply. However, parties to agreements based on this ECSS Standard are encouraged to investigate the possibility of applying the more recent editions of the normative documents indicated below. For undated references, the latest edition of the publication referred to applies.

1. [ECSS‑S‑ST‑00‑01](ecss_sst0001.md) ECSS system — Glossary of terms
2. [ECSS‑E‑ST‑10‑02](ecss_est1002.md) Space engineering — Verification

**Bibliography:**

1. ECSS‑S‑ST‑00 — ECSS system — Description, implementation & general requirements
2. [ECSS‑E‑ST‑10](ecss_est10.md) — Space engineering — System engineering general requirements
3. [ECSS‑M‑ST‑10](ecss_mst10.md) — Space project management — Project planning & implementation
4. EN 1325‑1:1996 — Value management, value analysis, functional analysis vocabulary — Part 1: Value analysis & functional analysis
5. EN 12973:2000 — Value management
6. ISO 9000:2000 — Quality management systems — Fundamentals & vocabulary



## 3. Terms, definitions & abbreviated terms

**Terms from other standards**

For the purposes of this Standard, the terms & definitions from [ECSS‑S‑ST‑00‑01](ecss_sst0001.md) apply.

**Terms specific to the present standard**

1. **Constraint** — Characteristic, result or design feature which is made compulsory or has been prohibited for any reason. (Adapted from EN 1325‑1) Constraints are generally restrictions on the choice of solutions in a system. Two kinds of constraints are considered, those which concern solutions, & those which concern the use of the system. For example constraints can come from environmental & operational conditions, law, standards, market demand, investments & means availability, or the organization’s policy.
2. **Environment** — (Product) natural conditions & induced conditions that constrain the design definitions for end products & their enabling products. Examples of natural conditions are weather, climate, ocean conditions, terrain, vegetation, dust, light & radiation. Example of induced conditions are electromagnetic interference, heat, vibration, pollution & contamination.
3. **Environment** — (Project) external factors affecting an enterprise or project.
4. **Environment** — (Development) external factors affecting development tools, methods, or processes.
5. **Function** — Intended effect of a system, subsystem, product or part. (Adapted from EN 1325‑1) Functions should have a single definite purpose. Function names should have a declarative structure (e.g. 「Validate Telecommands」), & say 「what」 is to be done rather than 「how」. Good naming allows design components with strong cohesion to be easily derived.
6. **Functional analysis** — Technique of identifying & describing all functions of a system. (Adapted from EN 1325‑1).
7. **Life cycle** — Time interval between the conceptual exploration of the product introduction to its withdrawal from service.
8. **Mission** — A possible instantiation of the mission statement in a mission concept. Each mission is described in an MDD. The implementation in time is called mission scenario.
9. **Need** — What is necessary for, or desired by, the user. (Adapted from EN 1325‑1) A need can be declared or undeclared; it can be an existing or a potential one. The user is a person or an organization for which the product is designed & which exploits at least one of its functions at any time during its life cycle. For the space community, the needs are often called mission statement.
10. **Specification** — Document stating requirements. (Adapted from ISO 9000:2000) A specification can be related to activities (e.g. procedure document, process specification & test specification), or products (e.g. technical requirements specification).
11. **Technical requirements specification** — Document by which the customer establishes the intended purpose of a product, its associated constraints & environment, the operational & performances features. The TS is the baseline of the business agreement to develop or purchase the selected solution. This specification is called in some projects System Requirements Document (SRD).

**Abbreviated terms.** The abbreviations from [ECSS‑S‑ST‑00‑01](ecss_sst0001.md) & the following apply:

1. **IEC** — International Electrotechnical Commission
2. **TS** — Technical requirements specification
3. **MDD** — Mission definition document


## 4. TS purpose & description

The technical requirements specification is a document through which a customer expresses his needs (or those that he is responsible for expressing) & the related environment & constraints in terms of technical requirements.

The technical requirements contained in the TS allow for potential suppliers to propose the best technical & programmatic solutions.

The intention of the technical requirements specification is **not to assume or refer to specific solutions**.

1. The TS is the technical reference for the qualification of the design & for the acceptance of the end product.
2. In that scope, the technical requirements contained in the TS are subject to the agreed change process defined in the business agreement. They are attainable & verifiable.

The change process itself can change in between project phases (Phase 0, A, B, C/D).

**TS content**

A technical requirements specification is typically composed of three major sets of information:

1. General information related to the context of the document (e.g. administrative information, normative documents & informative documents);
2. General information related to the context of the project, the product or system;
3. Technical requirements (described in clauses 6 & 8).

The specification provides the general information related to its context:

1. Administrative information: to provide all the information regarding, for example, the owner, status, identification, distribution list, & management rule;
2. Scope: to define without ambiguity the subject of the TS & aspects covered, thereby indicating limits of applicability;
3. References: to list all the normative (applicable) documents & standards, with titles, issue revision, & dates that are referred to in the TS;
4. Terms, definitions & abbreviated terms: to list the specific terms & abbreviated terms used in the TS.

It also provides general information related to the context of the project, product or system:

1. To provide a clear & rapid understanding of the project & the main needs or mission statements;
2. To give indications of the market as additional information, as well as information about the context of the project & the objectives (situation of the project in a larger programme, further developments);
3. To provide information on the environment & its constraints;
4. To detail the different situations of the product or system life cycle.


## 5. Process for establishing a technical requirements specification

The management of a programme necessitates the establishment of a set of successive states of a product & a network of customer & supplier relationships.

The successive states of a product are characterised by initially a 「high level」 (e.g. rather of functional type) definition of needs / requirements (e.g. at Phase 0), evolving progressively to a more precise (e.g. at phase B) or frozen (e.g. Phase C, or procurement of an equipment) definition of all requirements.

The procurement of products is governed by business agreements between two parties ‑ the customer & the supplier. At any intermediate level, the supplier of an item acts as customer in specifying components towards its suppliers.

A business agreement results from a process between a customer with a problem to solve, & a supplier with potential solutions. This results in a set of requirements that engages both parties. The list of technical requirements constitutes an important part of the business agreement & is adapted to the nature of the expected outcome. This list is contained in the technical requirements specification.

**Process for establishing a technical requirements specification**

The process to establish the technical requirements specification during Phase 0 of a project starts with the identification & evaluation of the different possible concepts to establish the TS. This step is needed in phase 0 for space projects with low heritage. It can also be required in Phase A.

A functional analysis can be performed to capture the technical requirements (see EN 12973).

It consists of an initial assessment of the project & results in the preliminary TS, as illustrated in Figure 51. The purpose of this preliminary TS is to express the customer’s need, mission statement, associated environmental constraint & programmatic element in terms of technical requirements (i.e. the problem to solve). This document serves as a basis to initiate the next step.

【**Fig.51:** Process to establish the preliminary TS in Phase 0】  
![](f/doc/ecss/ecss_est1006/51.png)

Where:

1. The F1.1 task: The customer identifies & captures the user’s needs or mission statements, associated environments & constraints. He expresses these in terms of technical requirements;
2. The F1.2 task: The customer structures, classifies & justifies (see 8.1.1) individual technical requirements;
3. The F1.3 task: The customer assesses the entire set of technical requirements for correctness, consistency & suitability for the intended use;
4. The F1.4 task: The customer establishes the preliminary TS & releases it.

The second step consists of the exploration among the different possible concepts ensuring the conformity to the defined needs, then the selection of one concept, & results in the TS. This version is progressively drafted from the preliminary TS & takes into account the induced constraints from the possible concepts. Figure 52 illustrates this process.

【**Fig.52:** Process to establish the TS in phase A】  
![](f/doc/ecss/ecss_est1006/52.png)

Where:

1. The F1.5 task: The customer reviews the preliminary TS, identifies & proposes possible concepts;
2. The F1.6 task: The customer evaluates & selects preferred concepts;
3. The F1.7 task: The customer identifies the need for changes to the preliminary TS taking into account the limitations & possibilities induced by the selected preferred concepts. Then, he expresses the adjusted or new individual technical requirements;
4. The F1.8 task: The customer structures, classifies & justifies (see 8.2.1) the individual technical requirements;
5. The F1.9 task: The customer assesses the entire set of technical requirements for correctness, consistency & suitability for the intended use;
6. The F1.10 task: The customer establishes the TS & releases it.

The process described is applicable at each decomposition level where the solution to be developed is chosen (e.g. for establishing a system level specification, or a lower level specification).

The outcome of this process, the technical requirements specification (TS), is a set of technical requirements to be issued by the customer & to be included in the business agreement for the development.

The customer, as a result of the negotiation of the business agreement with the supplier, can decide to update a few elements of his TS (as of other requirements specifications attached to the business agreement). This updated TS is then included in the business agreement for the next phase. In conformance with [ECSS‑M‑ST‑10](ecss_mst10.md), this update is typically done as a result of the SRR.



## 6. Technical requirements types

**6.1. General.** The management of the technical requirements is based upon recognition of the attributes of these technical requirements.

**6.2. Identification of types of technical requirements**

The differing types of technical requirements contained in the TS are as follows. These different technical requirements are called 「user related functions」 & constraints in EN 1325‑1.

1. **Functional requirements** — Requirements that define what the product shall perform, in order to conform to the needs/mission statement or requirements of the user. For example: 「The product shall analyse the surface of Mars & transmit the data so that it’s at the disposal of the scientific community」.
2. **Mission requirements** — Requirements related to a task, a function, a constraint, or an action induced by the mission scenario. For example: 「The product shall be designed to be put in its final position after a transfer duration shorter than 90 days」.
3. **Interface requirements** — Requirements related to the interconnection or relationship characteristics between the product & other items. This includes different types of interfaces (e.g. physical, thermal, electrical, & protocol). For example: 「The product shall dialogue with the ground segment using telemetry」.
4. **Environmental requirements** — Requirements related to a product or the system environment during its life cycle; this includes the natural environments (e.g. planet interactions, free space & dust) & induced environments (e.g. radiation, electromagnetic, heat, vibration & contamination). For example: 「The product shall operate within the temperature range from +30 ℃ to +50 ℃」.
5. **Operational requirements** — Requirements related to the system operability. This includes operational profiles & the utilization environment & events to which the product shall respond (e.g. autonomy, control & contingency) for each operational profile. For example: 「The product shall be designed to accept control of the viewing function from the ground segment」.
6. **Human factor requirements** — Requirements related to a product or a process adapted to human capabilities considering basic human characteristics. This includes the following basic human capability characteristics: decision making, muscular strength, coordination & craftsmanship, body dimensions, perception & judgement, workload, & comfort & freedom from environmental stress. For example: 「The product shall display the information with no more than two windows on the screen at the same time」.
7. **(Integrated) logistics support requirements** — Requirements related to the (integrated) logistics support considerations to ensure the effective & economical support of a system for its life cycle. This includes the following subjects: the constraints concerning the maintenance (e.g. minimum periodicity, intervention duration, infrastructure, tooling, intervention modes), packaging, transportation, handling & storage, training of product users, user documentation, implementation of the product at the user’s site, & reuse of the product or its elements. For example: 「The product shall be designed to be installed at the customer’s site within 2 days」.
8. **Physical requirements** — Requirements that establish the boundary conditions to ensure physical compatibility & that are not defined by the interface requirements, design & construction requirements, or referenced drawings. This includes requirements related to mechanical characteristics, electrical isolation & chemical composition (e.g. weight & dimensional limits). For example: 「The product shall have a mass of (30 ± 0,1) ㎏」.
9. **Product assurance (PA) induced requirements** — Requirements related to the relevant activities covered by the product assurance. This can include the following subjects: Reliability, availability, maintainability, Safety, & Quality assurance.
10. **Configuration requirements** — Requirements related to the composition of the product or its organization. For example: 「The product shall have 7 power modules with 2 power outlets per engine」.
11. **Design requirements** — Requirements related to the imposed design & construction standards such as design standards, selection list of components or materials, interchangeability, safety or margins. For example 「The receiver shall use a phase­lock loop (PLL)」.
12. **Verification requirements.** — Requirements related to the imposed verification methods, such as compliance to verification standards, usage of test methods or facilities. For example: 「The thermal balance test shall be performed using solar illumination」.



## 7. Overall Requirements for technical requirements specifications

**7.1. Overview.** In the perspective of the customer‑supplier relationship, these requirements are imposed by the customer on the supplier of the product for the production of lower level specifications. However, it’s recommended that the customer also applies them in its internal process of producing technical requirements specifications.

**7.2. Requirements for technical requirements specifications**

**7.2.1. General** — Technical requirements shall be formulated as defined in clause 8. The DRD for the TS is shown in Annex A. The specification shall be identifiable, referable & related to a product or a system.

**7.2.2. Responsibility** — An entity shall be identified to be responsible for the specification. The responsible entity of the specification shall define the content & format of the attributes listed in clause 8.

**7.2.3. Technical requirements organisation** — The technical requirements shall be grouped. Grouping can be either by type or in accordance with the different situations of the product life cycle.

1. Each technical requirement shall be separately stated.
2. Abbreviated terms used in requirements shall be defined in a dedicated section of the specification.
3. The technical requirements shall be consistent (e.g. not in conflict with the other requirements within the specification).
4. The technical requirements shall not be in conflict with the other requirements contained in business agreement documents.

**7.2.4. Technical reference** — The specification shall be complete in terms of applicable requirements & reference to applicable documents.

1. A technical requirement shall not call for more than one technical requirement in an applicable referred document.
2. The link to an applicable document shall be stated in the technical requirements.
3. The reference number of the applicable documents cited in the specification shall contain the revision identifier.

**7.2.5. Configuration management** — The specification shall be under configuration management.

**7.2.6. Format** — The specification shall be established to be readily exchanged according to the established access policy & rights.

**7.2.7. Supplementary information** — If a clause is stated to be informative or descriptive, then this clause shall not contain any requirement or recommendation.

**7.2.8. Restrictions** — Technical requirements specifications shall only include technical requirements & exclude requirements such as cost, methods of payment, quantity required, time or place of delivery.



## 8. Requirements for formulating technical requirements

### 8.1. General

In the perspective of the customer‑supplier relationship, these requirements are imposed by the customer on the supplier of the product for the production of lower level specifications. However, it’s recommended that the customer also applies them in its internal process of producing technical requirements specifications.


### 8.2. Requirements for the characteristics of a technical requirement of a TS

**8.2.1. Performance**

1. Each technical requirement shall be described in quantifiable terms.
2. If necessary to remove possible ambiguities of a given performance requirement the method used to determine the required performance shall be indicated in the requirement itself.

**8.2.2. Justification**

1. Each technical requirement should be justified.
2. The entity responsible of the technical requirement shall be identified.
3. The entity responsible of the specification shall define what part of the justification shall be included in the specification as informative material.
4. The justification of every technical requirement, as well as the entity responsible of the technical requirement, can be collected & recorded in a requirement justification file (see [ECSS‑E‑ST‑10](ecss_est10.md) Annex O).

**8.2.3. Configuration management & traceability**

1. Each technical requirement shall be under configuration management.
2. All technical requirements shall be backwards­traceable.
3. All technical requirements shall be forward­traceable.
4. A technical requirement is traceable when it’s possible to trace the history, application, or location of a requirement by means of recorded identification.
5. The backward traceability is the process to trace back the source of each requirement to the requirement from which it derives.
6. The forward traceability is the process to establish that each level requirement is implemented at the appropriate phase of the design & that all requirements are implemented.

**8.2.4. Ambiguity** — The technical requirements shall be unambiguous.

**8.2.5. Uniqueness** — Each technical requirement shall be unique.

**8.2.6. Identifiability**

1. A technical requirement shall be identified in relation to the relevant function, product or system.
2. A unique identifier shall be assigned to each technical requirement.
3. The unique identifier should reflect the type of the technical requirement.
4. The unique identifier should reflect the life profile situation.
5. In general a technical requirement is identified by, for example, a character or a string of characters, a number, or a name tag or hypertext.

**8.2.7. Singularity**

1. Each technical requirement shall be separately stated.
2. Technical requirements are single or separately stated when they are not the combination of two or more technical requirements.

**8.2.8. Completeness**

1. A technical requirement shall be self­contained.
2. A technical requirement is self­contained when it’s complete & does not require additional data or explanation to express the need.

**8.2.9. Verification**

1. A technical requirement shall be verifiable using one or more approved verification methods.
2. A technical requirement is verifiable when the means to evaluate if the proposed solution meets the requirement are known.
3. Verification of technical requirements shall be performed in conformance with [ECSS‑E‑ST‑10‑02](ecss_est1002.md).

**8.2.10. Tolerance**

1. The tolerance shall be specified for each parameter/variable.
2. The technical requirement tolerance is a range of values within which the conformity to the requirement is accepted.



### 8.3. Recommendations for the wording of requirements

**8.3.1. General format**

1. Technical requirements should be stated in performance or 「what­ is­ necessary」 terms, as opposed to telling a supplier 「how to」 perform a task, unless the exact steps in performance of the task are essential to ensure the proper functioning of the product.
2. Technical requirements should be expressed in a positive way, as a complete sentence (with a verb & a noun).

**8.3.2. Required verbal form**

1. **「Shall」** shall be used whenever a provision is a requirement.
2. **「Should」** shall be used whenever a provision is a recommendation.
3. **「May」** shall be used whenever a provision is a permission.
4. **「Can」** shall be used to indicate possibility or capability.

**8.3.3. Format restrictions** — Terms that **shall NOT** be used in a TS requirement:

1. adequate
2. and/or
3. appropriate
4. as far as possible
5. best possible
6. easy
7. enough
8. etc.
9. first rate
10. goal
11. great
12. large
13. maximize
14. minimize
15. necessary
16. optimize
17. quick
18. rapid
19. relevant
20. satisfactory
21. shall be included but not limited to
22. small
23. state of the art
24. suitable
25. sufficient
26. typical
27. user­ friendly



## Annex A (normative) Technical requirements specification (TS) — DRD

**A.1 DRD identification.** *Requirement ID & source* — ECSS‑E‑ST‑10, requirement 5.2.3.1.b & 5.6.4.a for all technical requirements specifications. *Purpose & objective* — the **technical requirements specification (TS)** establishes the intended purpose of a product, its associated constraints & environment, the operational & performance features for each relevant situation of its life profile, & the permissible boundaries in terms of technical requirements. The TS expresses frozen technical requirements for designing & developing the proposed solution to be implemented. These technical requirements, to be met by the future solution, are compatible with the intended purpose of a product, its associated constraints & environment, & the operational & performance features for each relevant situation of its life profile.

**A.2 Expected response**

*Special remarks* — None.

*Scope & content:*

1. **Introduction.** The purpose, objective, content & the reason prompting its preparation.
2. **Applicable & reference documents.** The applicable & reference documents in support of the generation of the document.
3. **User’s need presentation**
    1. Present the main elements that characterize the user’s need for developing the product as a background for those requirements that are defined in detail in the dedicated section.
    2. Put the product into perspective with other related products.
    3. If the product is independent & totally self‑contained, i.e. able to match the final user’s need, it should be so stated here.
    4. If the TS defines a product that is a component of a higher tier system, the TS shall recall the related needs of that larger system & shall describe the identified interfaces between that system & the product.
    5. A non­exhaustive checklist of general questions that should be answered at the early stages of the TS is: What is the product supposed to do? It’s fundamental but critically important to make sure that every actor has a complete understanding of what the product has to do. Who is going to use this product? It’s important to indicate who & why is going to use the product, & for what it’s going to be used.
4. **Selected concept/product presentation.** Describe the concept, the expected product architecture & the functioning principles on which it’s based.
5. **Life profile description.** List & describe the different chronological situations of the product’s life profile. (1) For a spacecraft, the life profile includes: AIT related life events; transportation to launching area; conditioning & tests; installation on launcher; pre­launch phase; launching phase; self transfer to its operating position; in­orbit functioning; end­of­life (e.g. de­orbitation). (2) An identifier can be associated with each situation in order to be able to link each requirement to at least one situation in which it applies. Such an approach enables sorting & filtering of the requirements per situation.
6. **Environment & constraints description.** Describe the different environments & constraints for each situation in the life profile that the product is expected to encounter. An identifier can be associated with each product environment in order to be able to link each requirement to at least the worst environment to which it applies. Such an approach enables sorting & filtering the requirements per environment.
7. **Requirements.**
    1. List all the technical requirements necessary for the product to satisfy the user’s needs. Interfaces requirements can be rolled‑out of the TS in form an interface requirement document (IRD), see [ECSS‑E‑ST‑10](ecss_est10.md) Annex M.
    2. The technical requirements shall be expressed according to [ECSS‑E‑ST‑10‑06](ecss_est1006.md) clauses 7 & 8. For instance, for all TS & for each requirement, the following characteristics have been selected: identifiability; performance & methods used to determine it; configuration management; traceability; tolerance; verification.


## The End

end of file
