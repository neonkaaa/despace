# NTU
> 2022.02.22 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/n/ntu_logo1t.webp)](f/c/n/ntu_logo1.svg)|<qsmanager@ntu.edu.sg>, +65( 679)117-44, Fax …;<br> *50 Nanyang Avenue, Singapore 639798*<br> <http://www.ntu.edu.sg> ~~ [FB ⎆](https://www.facebook.com/NTUsg) ~~ [IG ⎆](https://www.instagram.com/ntu_sg) ~~ [LI ⎆](https://www.linkedin.com/company/ntusg) ~~ [X ⎆](https://www.twitter.com/ntusg) ~~ [Wiki ⎆](https://en.wikipedia.org/wiki/Nanyang_Technological_University)|
|:--|:--|
|**Business**|Higher education, small sats development|
|**Mission**|…|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|…|

The **Nanyang Technological University (NTU)** is a national research university based in Singapore. It is the second oldest autonomous university in Singapore & is considered to be one of the top universities in the world.

- **College of Engineering.** The College of Engineering is NTU’s largest subdivision. It claimed to be the world’s largest engineering college, with a student population of more than 10 500 undergraduates & 3 500 graduates. It consists of six schools (Chemical & Biomedical, Civil & Environmental, Computer Science & Engineering, Electrical & Electronic, Materials Science, Mechanical & Aerospace) focused on technology & innovation. The college offers a rich array of multidisciplinary programmes & specialisations in traditional engineering disciplines & beyond. In addition to the 12 single degree programmes, the college also offers double degrees, double majors & integrated programmes as well as the only aerospace engineering programme in Singapore.
   - **Satellite Research Centre (SaRC)**

<p style="page-break-after:always"> </p>

## The End

end of file
