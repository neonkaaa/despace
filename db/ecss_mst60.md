# ECSS‑M‑ST‑60C Rev.1 (31 July 2008)
> 2023.07.07 [🚀](../../index/index.md) [despace](index.md) → [Doc](doc.md), [ECSS](ecss.md), [SC](sc.md)  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---


## Annex B (normative) Schedule — DRD

**B.1 DRD identification.** *Requirement ID & source* — ECSS‑M‑ST‑60 requirement 1.1.1.1.1 for baseline & current working schedule. *Purpose & objective* — the objective of the schedule is twofold as follows: ➀ For the baseline schedule, establish a reference data base agreed between both contractual parties (the baseline schedule contains all reference data for the purpose of schedule control); ➁ For the current working schedule, provide an overview of the on‑going activities & of the milestone status (for activities not yet completed, the current working schedule reflects the most probable prediction given by the supplier to the customer).

**B.2 Expected response**

*Special remarks.* The schedule should be presented in the form of a Gantt‑Chart.

*Scope & content:*

1. Provide the following information, for both the baseline & current working schedules (when defining the schedule, the total float for each activity with respect to the agreed milestones shall be available):
   1. identification of the activities
   1. identification of the critical path activities
   1. duration of activities
   1. flow of the activities (logical links between activities)
   1. key milestones
   1. lower suppliers planned end dates
   1. main & key inspection points (MIP/KIP)
   1. start/finish date of activities



## Annex G (normative) Cost estimate report — DRD

**G.1 DRD identification.** *Requirement ID & source* — ECSS‑M‑ST‑60, requirement 1.1.1.1.1cc. *Purpose & objective* — the objective of the **cost estimate report (CER)** is to provide the project or programme cost estimate, along with the details how the estimates were determined, what are the main cost & risk drivers & a list of recommendations that need to be taken into consideration at project/programme management level.

**G.2 Expected response**

*Special remarks.* Sections [4 ‑ 8] shall be repeated for each option or alternates. Section [9] shall synthesize findings for baseline & all options & alternates.

*Scope & content:*

1. **Introduction.** The purpose & objective of the CER; a brief description of what was done during the cost estimate exercise, & its outcome; identification of organizations that contributed to the preparation of the document.
1. **Applicable & reference documents.** The applicable & reference documents, used in support to the generation of the document.
1. **Quality of estimate.** Characterize the expected quality level & features of the estimate. This may be done using references to codes & standards. (For example: AACE International recommended best practice No 17R‑97)
1. **Hypothesis used.** Describe what the hypothesis were made to perform the cost estimating analysis, including:
   1. Economic conditions
   1. Currency
   1. Inclusions & exclusions
   1. Heritage/Technology Readiness Level
   1. Design status
   1. Engineering difficulty
   1. Hardware matrix
   1. Spares philosophy
   1. Identification of main cost drivers
   1. Market situation (e.g. monopoly, & open competition)
   1. Cost sharing
1. **Method & Cost model(s) description.** The supplier shall indicate for each of the cost items which method was used.
1. **Cost breakdown tables.** The customer & the supplier shall mutually agree in advance on the template format & the level of details to be produced.
1. **Cost sensitivity analysis.** Identify, rank & describe how the main cost drivers influence the global cost.
1. **Cost Risk assessment.** This assessment shall be based on the risk register. If absent at the time of the assessment, a register shall be constituted at this stage & for this purpose.
   1. Identify the main cost risk items & the associated mitigations proposed.
   1. The supplier shall refer to its cost estimating plan (as per ECSS‑M‑ST‑60 Ann.F) & to ECSS‑M‑ST‑80.
   1. The supplier shall provide cost figures associated with confidence level. Unless otherwise agreed with the customer, the supplier shall present ranges at agreed confidence level with the customer.
   1. In case of stochastic analysis, the supplier shall explain how the distribution profiles are chosen & parameters fixed for the different cost items.
1. **Recommendations.** Summarize the findings, restate the accuracy & confidence level to be given to the analysis, & make all necessary recommendations for the sake of the proper execution of the project/programme. Based on the cost sensitivity analysis & the identified risk drivers, the cost estimating report shall identify any recommendation that need to be taken into consideration at project/programme management level.
