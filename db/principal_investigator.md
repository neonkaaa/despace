# Principal investigator
> 2019.10.18 [🚀](../../index/index.md) [despace](index.md) → **[](.md)**  
> Nav.:  
> [FAQ](faq.md)［[SC+OE+SGM](sc.md)］ ~~ [CON](contact.md)·[Pers](person.md) ~~ [Ctrl](control.md) ~~ [Doc](doc.md) ~~ [Drawing](draw.md) ~~ [EF](ef.md) ~~ [Event](event.md) ~~ [FS](fs.md) ~~ [HF&E](hfe.md) ~~ [KT](kt.md) ~~ [N&B](nnb.md) ~~ [Project](project.md) ~~ [QM](qm.md) ~~ [R&D](rnd.md) ~~ [SI](si.md) ~~ [Test](test.md) ~~ [TRL](trl.md)

[TOC]

---

> <small>**Principal investigator (PI)** — англоязычный термин, не имеющий аналога в русском языке. **Ответственный исследователь** — дословный перевод с английского на русский.</small>

In Canada & the United States, the term **principal investigator (PI)** refers to the holder of an independent grant & the lead researcher for the grant project, usually in the sciences, such as a laboratory study or a clinical trial. The phrase is also often used as a synonym for 「head of the laboratory」 or 「research group leader」. While the expression is common in the sciences, it is used widely for the person or persons who make final decisions & supervise funding & expenditures on a given research project.

A co‑investigator (Co‑I) assists the principal investigator in the management & leadership of the research project. There may be a number of co‑investigators supporting a PI.



## Federal funding
In the context of US federal funding from agencies such as the National Institutes of Health (NIH) or the National Science Foundation (NSF), the PI is the person who takes direct responsibility for completion of a funded project, directing the research & reporting directly to the funding agency. For small projects (which might involve 1 ‑ 5 people) the PI is typically the person who conceived of the investigation, but for larger projects the PI may be selected by a team to obtain the best strategic advantage for the project.

In the context of a clinical trial a PI may be an academic working with grants from NIH or other funding agencies, or may be effectively a contractor for a pharmaceutical company working on testing the safety & efficacy of new medicines.

There were 20 458 PIs on NIH R01 grants in US biomedical research in 2000. In 2013, this number has grown to 21 511. At the same time the success rate for an applicant to receive an R01 grant has gone down from 32 % in 2000 to 17 % in 2013.



## Docs/Links (TRANSLATEME ALREADY)
|**Sections & pages**|
|:--|
|**【[](.md)】**<br> <mark>NOCAT</mark>|

1. Docs: …
1. <https://en.wikipedia.org/wiki/Principal_investigator>


## The End

end of file
