# Digital Blast
> 2021.12.06 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/d/digital_blast_logo1t.webp)](f/c/d/digital_blast_logo1.webp)|<info@digitalblast.co.jp>, <mark>nophone</mark>, Fax …;<br> *Kanda Tsukasacho PREX 7F, 2-7-9 Kanda Tsukasacho, Chiyoda-ku, Tokyo, 101-0048, Japan*<br> <http://digitalblast.co.jp> ~~ [FB ⎆](https://www.facebook.com/DigitalBlastInc) ~~ [LI ⎆](https://www.linkedin.com/company/digitalblastjp) ~~ [X ⎆](https://twitter.com/digitalblastinc)|
|:--|:--|
|**Business**|Business & technology colsulting|
|**Mission**|…|
|**Vision**|To become the 「World's No.1 Space Business Platformer」|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO — Shingo Horiguchi<br> ・COO — Hironobu Inagaki|

**Digital Blast Co., Ltd.** is a Japanese space‑related consulting company. Founded 2018.12. Our Business:

- **Space Business Consulting Division.** From a global perspective, the industrial structure has changed compared to 10 years ago, with a shift in space technology from the public to the private sector & the accompanying rise of space ventures. As a result, attractive business opportunities are opening up for companies that were not previously involved in space. We support our clients to develop the best space business by teaming up with experts who have rich industry knowledge, experience, & networks in space & consultants who have market leading knowledge in new business development.
- **Business Consulting Division.** In a wide range of fields from business development & product development to social innovation, we provide consistent support from the discovery of issues to the implementation of solutions, based on our outstanding creative ability to create innovative solutions & our solid know‑how to implement them.
- **Incubation Division.** The Incubation Division aims to bring about social change by developing unprecedented cutting‑edge technologies, such as the development of space media & community businesses based on video, & solutions that integrate space & ground technologies, utilizing the knowledge & experience gained through consulting.

In the **Space Business Consulting Division**, we support our clients to develop the best practice of space business by teaming up with experts who have market leading knowledge, experience & connection in the space industry. As for **Business Consulting Division**, we provide consistent support from the discovery of issues to the implementation of solutions, based on our outstanding creative ability to create innovative solutions & implementing them based on our solid background. Furthermore, we aim to bring about social change in the space industry by developing cutting‑edge technologies through our activities in the **Incubation Division**. By utilizing the knowledge & experience gained through consulting, we create solutions that integrate scientific knowledge on Earth & space technologies.

<p style="page-break-after:always"> </p>

## The End

end of file
