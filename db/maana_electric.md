# Maana Electric
> 2022.04.04 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/m/maana_electric_logo1t.webp)](f/c/m/maana_electric_logo1.webp)|<lux@maanaelectric.com>, +352 20 28 58, Fax …;<br> *12 rue de l’Industrie, L-3895, Foetz, Luxembourg*<br> <https://maanaelectric.com> ~~ [FB ⎆](https://www.facebook.com/MaanaElectric) ~~ [IG ⎆](https://www.instagram.com/maanaelectric) ~~ [LI ⎆](https://www.linkedin.com/company/maanaelectric) ~~ [X ⎆](https://twitter.com/maanaelectric)|
|:--|:--|
|**Business**|Electric systems, in‑situ el.systems production|
|**Mission**|…|
|**Vision**|Supporting human exploration|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO, Founder — Joost van Oorschot<br> ・COO, Founder — Luca Celiento<br> ・CTO, Founder — Pablo Alberto Calla Galleguillos|

**Maana Electric SA** is a Luxembourg company aimed to create electronic systems and in‑situ producted electric generators. Founded in 2018.

**Offices:**

- **Maana Electric in Luxembourg** — Maana Electric’s head offices are located in Luxembourg, with 2 000 m² facilities. Here Maana Electric develops many of it’s proprietary technologies for applications both on Earth & in Space.
- **Maana Electric in Netherlands** — Glass & Robotics Development. Maana Electric’s offices in the Netherlands are located in Delft & are focussed on the development of its glass products & the robotic aspects of its systems. For enquiries to Maana Electric’s office in the Netherlands, feel free to reach out to us at: <nl@maanaelectric.com> Our offices are located at:<br> *Molengraaffsingel 12, 2629 JD, Delft, Netherlands*
- **Maana Electric in UAE** — ISRU Technologies Development. Maana Electric’s offices in the United Arab Emirates are located in Dubai within the Dubai Silicon Oasis & are focussed on the development of terrestrial & space disruptive ISRU technologies. For enquiries to Maana Electric’s office in Dubai, feel free to reach out to us at: <info@maanaelectric.com> Our offices are located at:<br> *Dubai Silicon Oasis DDP, Building A2, Dubai, UAE*

<p style="page-break-after:always"> </p>

## The End

end of file
