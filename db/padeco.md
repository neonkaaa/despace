# PADECO
> 2023.01.04 [🚀](../../index/index.md) [despace](index.md) → [Contact](contact.md)

|[![](f/c/p/padeco_logo1t.webp)](f/c/p/padeco_logo1.webp)|<admin@padeco.co.jp>, +81-3-5733-0855, Fax +81-3-5733-0856;<br> *Shin-Onarimon Bldg. 6-17-19 Shinbashi, Minato-ku Tokyo 105-0004 Japan*<br> <http://www.padeco.jp> ~~ [LI ⎆](https://www.linkedin.com/company/padeco-co.-ltd.)|
|:--|:--|
|**Business**|Space consultations|
|**Mission**|The lynchpin to our work for client’s satisfaction & helps us to make our world more livable|
|**Vision**|…|
|**Values**|…|
|**[MGMT](mgmt.md)**|・CEO — Takashi Soma|

**PADECO Co., Ltd** is a Japanese consulting company with a space division. Founded 1983.

While an individual is capable of providing results, the results of a team of capable multi‑disciplinary individuals are far beyond the sum of each. Our multi‑cultured international staff strive to satisfy clients by not only relying on our specialties & backgrounds but also those of others in the world whenever appropriate. Our open culture encourages staff to seek answers in order to leverage the knowledge of our entire organization & beyond.

Project list: <https://www.padeco.co.jp/en/projects/list.html?catid=35>



## The End

end of file
